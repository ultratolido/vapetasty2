﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using Microsoft.AspNet.SignalR;
using POS.Models;
using System.Data;
using System.Configuration;
using MySql.Data.MySqlClient;
using System.IO;
using POS.Fn;

namespace Tienda_Online.Hubs
{
    public class HubProductosExcel : Hub
    {
        public string msg = "Iniciando...";
        public int count = 100;


        public void AplicarCambiosExcel()
        {
            Producto_Actualizar_DESCRIPCION_FAMILIA_Excel();

        }

        public void Producto_Actualizar_DESCRIPCION_FAMILIA_Excel()
        {
            //SUB-PROCESOS
            //1.- CREAR SCRIPT
            //2.- CORRER SCRIPT


            try
            {
                //****************************
                //INICIALIZANDO
                //****************************                
                object[] sendVariable = new object[2];
                sendVariable = new object[3];
                sendVariable[0] = " Inicializando Descripcion - Familia...";
                sendVariable[1] = 0;
                sendVariable[2] = "Registros procesados: (0/0)";// string.Format(" ({0}/{1})", subProcesoActual, totalSubProcesos);
                Clients.Caller.sendMessage(sendVariable);

                //****************************
                //GENERANDO QUERY SQL
                //****************************                

                sendVariable = new object[3];
                sendVariable[0] = " (1/2) Generando Query Descripcion - Familia...";
                sendVariable[1] = 0;
                sendVariable[2] = "Registros procesados: (0/0)";
                Clients.Caller.sendMessage(sendVariable);



                moragas_pruebasEntities1 db = new moragas_pruebasEntities1();
                configuracion_general conf = db.configuracion_general.First();

                List<nuevo_producto> lista_productos = db.nuevo_producto.Where(x => x.copiadoCatolgoDescripcion != 1).ToList();
                //List<producto> ListaProductosRegistrados = db.producto.ToList();

                bool success = false;
                string _msg = "";

                //******************
                //DB CONEXION*********
                //******************
                //string path = System.Web.HttpContext.Current.Server.MapPath("").Split('\\').ElementAt(System.Web.HttpContext.Current.Server.MapPath("").Split('\\').Length - 2);
                //MySqlConnection dbConn = new MySqlConnection(Tienda_Online.Areas.POS.Fn.ConnDB.getConnString(path));
                string path = System.Web.HttpContext.Current.Server.MapPath("").Split('\\').ElementAt(System.Web.HttpContext.Current.Server.MapPath("").Split('\\').Length - 2);
                MySqlConnection dbConn = new MySqlConnection(POS.Fn.ConnDB.getConnString(path));


                ////******************
                //MARCA*********
                //******************

                string query_insert_marca = "";
                List<string> MarcaDistinct = lista_productos.Where(x => x.marca != "").Select(x => x.marca).Distinct().ToList();
                foreach (string marca in MarcaDistinct)
                {
                    query_insert_marca +=
                        //"SET @marca = '" +  marca + "';"
                        " INSERT INTO marca (nombre)"
                        + " SELECT * FROM (SELECT '" + marca + "') AS tmp"
                        + " WHERE NOT EXISTS ("
                        + "    SELECT nombre FROM marca WHERE nombre = '" + marca + "'" /*COLLATE utf8_general_ci"*/
                        + ") LIMIT 1;";
                }
                //APLICAR QUERY MARCA 
                if (query_insert_marca != "")
                {
                    using (MySqlCommand com = new MySqlCommand(query_insert_marca, dbConn))
                    {
                        dbConn.Open();
                        com.ExecuteNonQuery();
                        dbConn.Close();
                    }
                }

                ////******************
                //PROVEEDOR*********
                //******************

                string query_insert_proveedor = "";
                List<string> proveedorDistinct = lista_productos.Where(x => x.proveedor != "").Select(x => x.proveedor).Distinct().ToList();
                foreach (string proveedor in proveedorDistinct)
                {
                    query_insert_proveedor +=
                        "INSERT INTO proveedor (nombre_proveedor)"
                        + " SELECT * FROM (SELECT '" + proveedor + "') AS tmp"
                        + " WHERE NOT EXISTS ("
                        + "    SELECT nombre_proveedor FROM proveedor WHERE nombre_proveedor = '" + proveedor + "'" /*"' COLLATE utf8_general_ci"*/
                        + ") LIMIT 1;";
                }
                //APLICAR QUERY PROVEEDOR 
                if (query_insert_proveedor != "")
                {
                    using (MySqlCommand com = new MySqlCommand(query_insert_proveedor, dbConn))
                    {
                        dbConn.Open();
                        com.ExecuteNonQuery();
                        dbConn.Close();
                    }
                }


                //******************
                //FAMILIA-SUBFAMILIA
                //******************

                // FAMILIA -> ARMAR QUERY INSERT
                List<string> distinctFamilias = lista_productos.Where(x => x.familia != "").Select(x => x.familia.TrimEnd().TrimStart().ToUpper()).Distinct().ToList();
                String query_insert_familia = "";// "INSERT INTO FAMILIA(NOMBRE) VALUES";
                foreach (string f in distinctFamilias)
                {
                    //query_insert_familia += "('" + f + "'),";

                    query_insert_familia +=
                        "INSERT INTO familia (nombre)"
                        + " SELECT * FROM (SELECT '" + f + "') AS tmp"
                        + " WHERE NOT EXISTS ("
                        + "    SELECT nombre FROM familia WHERE nombre = '" + f + "'"/*"' COLLATE utf8_general_ci"*/
                        + ") LIMIT 1;";

                }
                //SUBFAMLIAS
                string query_insert_subfamilia = "";
                foreach (string familia in distinctFamilias)
                {
                    List<string> subfamilia_distinct_from_familia = lista_productos.Where(x => x.subfamilia != "" && x.familia == familia).Select(x => x.subfamilia).Distinct().ToList();
                    foreach (string subfamilia in subfamilia_distinct_from_familia)
                    {

                        query_insert_subfamilia +=
                            "INSERT INTO subfamilia (nombre,familia_id) "
                                + " SELECT * FROM (SELECT '" + subfamilia + "' as _NombreSubFamilia,(select id from familia where nombre='" + familia + "') as _idFamilia) AS tmp "
                                + " WHERE NOT EXISTS (SELECT *FROM subfamilia WHERE nombre = '" + subfamilia + "' and familia_id= (select id from familia where nombre='" + familia + "')) LIMIT 1;";
                    }
                }


                //APLICAR QUERY FAMILIA
                if (query_insert_familia != "")
                {
                    using (MySqlCommand com = new MySqlCommand(query_insert_familia, dbConn))
                    {
                        dbConn.Open();
                        com.ExecuteNonQuery();
                        dbConn.Close();
                    }
                }
                //APLICAR QUERY SUBFAMILIA
                if (query_insert_subfamilia != "")
                {
                    using (MySqlCommand com = new MySqlCommand(query_insert_subfamilia, dbConn))
                    {
                        dbConn.Open();
                        com.ExecuteNonQuery();
                        dbConn.Close();
                    }
                }
                List<string> distinctProductoPadre = lista_productos.Where(x => x.productoPadre != "").Select(x => x.productoPadre.TrimEnd().TrimStart().ToUpper()).Distinct().ToList();
                String query_insert__codigo_producto_padre = "";// "INSERT INTO PRODUCTOPADRE(CODIGO) VALUES";
                foreach (string padre in distinctProductoPadre)
                {
                    List<string> distinctCodigoProductoPadre = lista_productos.Where(x => x.codigoProductoPadre != "" && x.productoPadre == padre).Select(x => x.codigoProductoPadre).Distinct().ToList();

                    foreach (string codigo in distinctCodigoProductoPadre)
                    {
                        query_insert__codigo_producto_padre +=
                                "INSERT INTO productopadre (descripcion, codigo)"
                            + " SELECT * FROM (SELECT '" + padre + "' as _DescripcionProductoPadre, (SELECT '" + codigo + "' as _CodigoProductoPadre)) AS tmp"
                            + " WHERE NOT EXISTS(SELECT * FROM productopadre WHERE descripcion = '" + padre + "' and codigo = '" + codigo + "') LIMIT 1; ";
                    }
                }

                //if (query_insert_producto_padre != "")
                //{
                //    using (MySqlCommand com = new MySqlCommand(query_insert_producto_padre, dbConn))
                //    {
                //        dbConn.Open();
                //        com.ExecuteNonQuery();
                //        dbConn.Close();
                //    }
                //}
                if (query_insert__codigo_producto_padre != "")
                {
                    using (MySqlCommand com = new MySqlCommand(query_insert__codigo_producto_padre, dbConn))
                    {
                        dbConn.Open();
                        com.ExecuteNonQuery();
                        dbConn.Close();
                    }
                }
                //******************
                //PRODUCTOS*********
                //******************

                // PRODUCTOS -> ELIMINAR CODIGOS DUPLICADOS LISTA EXCEL
                List<nuevo_producto> distinctProductos = lista_productos
                          .GroupBy(x => x.codigo)
                          .Select(x => x.First())
                          .ToList();

                String query_productos = "";
                int actualizado = 0;
                int totales = distinctProductos.Count();
                int contar100 = 0;
                foreach (nuevo_producto p in distinctProductos)
                {
                    p.descripcion = p.descripcion.Replace("'", "\\'").Replace('"', '\"');
                    p.descripcion_detallada = p.descripcion_detallada.Replace("'", "\\'").Replace('"', '\"');
                    p.marca = p.marca.Replace("'", "\\'").Replace('"', '\"');
                    p.subfamilia = p.subfamilia.Replace("'", "\\'").Replace('"', '\"');
                    p.proveedor = p.proveedor.Replace("'", "\\'").Replace('"', '\"');
                    p.productoPadre = p.productoPadre.Replace("'", "\\'").Replace('"', '\"');
                    p.codigoProductoPadre = p.codigoProductoPadre.Replace("'", "\\'").Replace('"', '\"');

                    query_productos +=
                        //ACTUALIZA SI EXISTE
                        "UPDATE PRODUCTO SET ";
                    //SI DESCRIPCION TIENE UN VALOR ACTUALIZALA
                    if (p.descripcion != "") { query_productos += " DESCRIPCION=" + "'" + p.descripcion.Replace("'", "\'") + "',"; }
                    //SI DESCRIPCION DETALLADA TIENE UN VALOR ACTUALIZALA
                    if (p.descripcion_detallada != "") { query_productos += " DESCRIPCION_DETALLADA=" + "'" + p.descripcion_detallada.Replace("'", "\'") + "',"; }
                    //SI MARCA TIENE UN VALOR ACTUALIZALO
                    if (p.marca != "") { query_productos += " ID_MARCA=(select id from marca where nombre='" + p.marca + "'),"; }
                    //SI SUBFAMILIA TIENE UN VALOR ACTUALIZALA
                    if (p.subfamilia != "") { query_productos += " SUBFAMILIA_ID=(select id from subfamilia where nombre='" + p.subfamilia + "' and familia_id=(select id from familia where nombre='" + p.familia + "')),"; }
                    //SI PROVEEDOR TIENE UN VALOR ACTUALIZALO
                    if (p.proveedor != "") { query_productos += " PROVEEDOR_ID=(select id from proveedor where nombre_proveedor='" + p.proveedor + "'),"; }
                    //COSTO
                    if (p.costo != null) { query_productos += " COSTO ='" + p.costo + "',"; }
                    //IVA
                    if (p.iva != null) { query_productos += " IVA ='" + p.iva + "',"; }
                    //MEDIDA
                    if (p.medida != "") { query_productos += " MEDIDA ='" + p.medida + "',"; }
                    //MODELO
                    if (p.modelo != "") { query_productos += " MODELO ='" + p.modelo + "',"; }
                    //MODELO
                    if (p.productoPadre != "") { query_productos += " MODELO ='" + p.productoPadre + "',"; }
                    //MODELO
                    if (p.codigoProductoPadre != "") { query_productos += " MODELO ='" + p.codigoProductoPadre + "',"; }
                    ////PRECIO DOLAR
                    //query_productos += " PRECIO_DOLAR =" + p.precio_dolar;
                    //PUBLICAR
                    if (p.publicar.ToUpper() == "1" || p.publicar.ToUpper() == "SI" || p.publicar.ToUpper() == "S") { query_productos += " PUBLICAR =1,"; }
                    if (p.publicar.ToUpper() == "0" || p.publicar.ToUpper() == "NO" || p.publicar.ToUpper() == "N") { query_productos += " PUBLICAR =0,"; }
                    //CARRUSEL             
                    if (p.carrusel.ToUpper() == "1" || p.carrusel.ToUpper() == "SI" || p.carrusel.ToUpper() == "S") { query_productos += " CARRUSEL =1,"; }
                    if (p.carrusel.ToUpper() == "0" || p.carrusel.ToUpper() == "NO" || p.carrusel.ToUpper() == "N") { query_productos += " CARRUSEL =0,"; }
                    //CONSIGNACIÓN
                    if (conf.venta_consignacion.Value)
                    {
                        if (p.consignacion.ToUpper() == "1" || p.consignacion.ToUpper() == "SI" || p.consignacion.ToUpper() == "S") { query_productos += " CONSIGNACION =1,"; }
                    }


                    query_productos = query_productos.Remove(query_productos.Length - 1);
                    // W H E R E    - - - -  APLICAR AL PRODUCTO CON CODIGO==
                    query_productos += " WHERE CODIGO= '" + p.codigo + "';"

                    //INSERTA SI NO EXISTE
                    //+ " INSERT INTO PRODUCTO(CODIGO,DESCRIPCION,DESCRIPCION_DETALLADA,ID_MARCA,SUBFAMILIA_ID,PROVEEDOR_ID,COSTO,MEDIDA,MODELO,PRECIO_DOLAR,PUBLICAR,CARRUSEL)"
                    + " INSERT INTO PRODUCTO(CODIGO,DESCRIPCION,DESCRIPCION_DETALLADA,ID_MARCA,SUBFAMILIA_ID,PROVEEDOR_ID,COSTO,IVA,MEDIDA,MODELO,PUBLICAR,CARRUSEL,CONSIGNACION,PRODUCTOPADRE_ID)"
                    + " SELECT * FROM ( SELECT "
                        + "'" + p.codigo + "' as _codigo,"
                        + "'" + p.descripcion.Replace("'", "\'") + "'  as _descripcion,"
                        + "'" + p.descripcion_detallada.Replace("'", "\'") + "'  as _descripcion_detallada,";
                    //SI MARCA TIENE UN VALOR INGRESALO
                    if (p.marca != "") { query_productos += "(select id from marca where nombre='" + p.marca + "') as _idMarca,"; }
                    else { query_productos += "(SELECT id from marca LIMIT 1) as _idMarca,"; }
                    //SI SUBFAMILIA TIENE UN VALOR INGRESALO
                    if (p.subfamilia != "") { query_productos += "(select id from subfamilia where nombre='" + p.subfamilia + "' and familia_id=(select id from familia where nombre='" + p.familia + "')) as _idSubfamilia,"; }
                    else { query_productos += "(SELECT id from subfamilia LIMIT 1) as _idSubfamilia,"; }
                    //SI PROVEEDOR TIENE UN VALOR INGRESALO
                    if (p.proveedor != "") { query_productos += "(select id from proveedor where nombre_proveedor='" + p.proveedor + "') as _idProveedor,"; }
                    else { query_productos += "(SELECT id from proveedor LIMIT 1) as _idProveedor,"; }
                    //SI COSTO DIFERENTE DE NULL INGRESALO
                    if (p.costo != null) { query_productos += "'" + p.costo + "' as _costo,"; } else { query_productos += "'' as _costo,"; }
                    //SI IVA DIFERENTE DE NULL INGRESALO
                    decimal iva_default = Configuracion.TraerConfiguracionGeneral().iva != null ? Configuracion.TraerConfiguracionGeneral().iva.Value : 0;
                    if (p.iva != null) { query_productos += "'" + p.iva + "' as _iva,"; } else { query_productos += "'' as _iva,"; }
                    //SI MEDIDA TIENE UN VALOR INGRESALO
                    if (p.medida != "") { query_productos += "'" + p.medida + "' as _medida,"; } else { query_productos += "'" + iva_default + "' as _medida,"; }
                    //SI MODELO TIENE UN VALOR INGRESALO
                    if (p.modelo != "") { query_productos += "'" + p.modelo + "' as _modelo,"; } else { query_productos += "'' as _modelo,"; }
                    ////PRECIO DOLAR
                    //query_productos += p.precio_dolar + " as _precioDolar,";
                    //SI PUBLICAR !=1 INSERTA 0.
                    if (p.publicar.ToUpper() == "1" || p.publicar.ToUpper() == "SI" || p.publicar.ToUpper() == "S") { query_productos += " 1  as _publicar,"; } else { query_productos += " 0  as _publicar,"; }
                    //SI CARRUSEL !=1 INSERTA 0.
                    if (p.carrusel.ToUpper() == "1" || p.carrusel.ToUpper() == "SI" || p.carrusel.ToUpper() == "S") { query_productos += " 1  as _carrusel"; } else { query_productos += " 0  as _carrusel"; }
                    //CONSIGNACIÓN
                    if (conf.venta_consignacion.Value)
                    {
                        if (p.consignacion.ToUpper() == "1" || p.consignacion.ToUpper() == "SI" || p.consignacion.ToUpper() == "S") { query_productos += " ,1  as _consignacion"; } else { query_productos += " ,0  as _consignacion"; }
                    }
                    else { query_productos += " ,0  as _consignacion,"; }
                    //SI PRODUCTOPADRE TIENE UN VALOR INGRESALO
                    if (p.productoPadre != "") { query_productos += "(select id from productoPadre where descripcion ='" + p.productoPadre + "') as _idProdutoPadre"; }
                    else { query_productos += "(SELECT id from productoPadre LIMIT 1) as _idProdutoPadre"; }

                    query_productos += ") AS tmp"
                   + " WHERE NOT EXISTS ("
                   + "    SELECT codigo FROM producto WHERE codigo = '" + p.codigo + "'"
                   + ") LIMIT 1;"
                   //ACTUALIZA STATUS COPIADO DE NUEVO PRODUCTO
                   + " UPDATE nuevo_producto set copiadoCatolgoDescripcion=1,fecha=NOW() where id='" + p.id + "';";


                    //calcular porcentaje
                    actualizado++;
                    int porcentaje = (actualizado * 50) / totales;
                    sendVariable = new object[3];
                    sendVariable[0] = " (2/2) Aplicando Cambios...";
                    sendVariable[1] = porcentaje;
                    sendVariable[2] = "Registros procesados: (" + actualizado + "/" + totales + ")";
                    Clients.Caller.sendMessage(sendVariable);

                    //EJECUTAR CONSULTA CADA 100 REGISTROS
                    if (contar100 == 100)
                    {
                        if (query_productos != "")
                        {
                            using (MySqlCommand com = new MySqlCommand(query_productos, dbConn))
                            {
                                dbConn.Open();
                                com.ExecuteNonQuery();
                                dbConn.Close();
                            }
                        }
                        //REINICIAR QUERY
                        query_productos = "";
                        //reiniciar contador
                        contar100 = 0;
                    }
                    //AUMENTAR CONTADOR 1
                    contar100++;
                }
                //SI QUEDAN PENDIENTES EJECUTALOS
                if (query_productos != "")
                {
                    using (MySqlCommand com = new MySqlCommand(query_productos, dbConn))
                    {
                        dbConn.Open();
                        com.ExecuteNonQuery();
                        dbConn.Close();
                        //calcular porcentaje
                        sendVariable = new object[3];
                        sendVariable[0] = " (2/2) Aplicando Cambios...";
                        sendVariable[1] = 100;
                        sendVariable[2] = "Registros procesados: (" + totales + "/" + totales + ")";
                        Clients.Caller.sendMessage(sendVariable);

                    }
                }
                Clients.Caller.UpdateProductos_OK();
            }
            catch (Exception e)
            {
                object[] sendVariable = new object[2];
                sendVariable[0] = "Error:" + e.Message;
                if (e.InnerException != null)
                {
                    sendVariable[0] += "Error:" + e.Message + "Inner" + e.InnerException.ToString();
                }

                sendVariable[1] = 0;
                Clients.Caller.sendMessage(sendVariable);
                Clients.Caller.error(e.Message.ToString());
            }
        }


        public void Producto_CargarArchivoProductos_DB(string fileName)
        {
            //VARIABLES

            //SUB-PROCESOS
            //1.- CARGAR EXCEL
            //2.- TRUNCAR TABLA NUEVO PRODUCTO
            //3.- CARGAR REGISTROS A TABLA NUEVO PRODUCTO
            //4.- ANALIZAR REGISTROS


            int totalSubProcesos = 4;
            int subProcesoActual = 0;

            decimal suma_porcentaje_total = 0;
            decimal porcentaje_por_subproceso = 100 / totalSubProcesos;

            bool existearchivo = false;


            moragas_pruebasEntities1 db = new moragas_pruebasEntities1();
            //traer configuración
            //traer configuración
            configuracion_general conf = db.configuracion_general.First();


            try
            {

                //****************************
                //INICIALIZANDO
                //****************************

                subProcesoActual = 0;
                object[] sendVariable = new object[2];
                sendVariable = new object[3];
                sendVariable[0] = " (" + subProcesoActual + "/" + totalSubProcesos + ")" + " Inicializando...";
                sendVariable[1] = 0;
                sendVariable[2] = "Registros procesados: (0/0)";// string.Format(" ({0}/{1})", subProcesoActual, totalSubProcesos);
                Clients.Caller.sendMessage(sendVariable);

                string filePath = AppDomain.CurrentDomain.BaseDirectory + "UploadFolder//ArchivosExcel/" + fileName;


                if (File.Exists(filePath))
                { existearchivo = true; }
                var connectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1\"", filePath);
                System.Data.OleDb.OleDbConnection connEXCEL = new System.Data.OleDb.OleDbConnection();
                connEXCEL.ConnectionString = connectionString;
                connEXCEL.Open();

                System.Data.DataTable dbSchema = connEXCEL.GetOleDbSchemaTable(System.Data.OleDb.OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
                string Sheet1 = dbSchema.Rows[0].Field<string>("TABLE_NAME");
                var adapter = new System.Data.OleDb.OleDbDataAdapter("SELECT * FROM [" + Sheet1 + "]", connectionString);
                var ds = new DataSet();
                adapter.Fill(ds, "results");
                System.Data.DataTable data = ds.Tables["results"];



                //PORCENAJE 0% - 25%
                suma_porcentaje_total = +porcentaje_por_subproceso;
                subProcesoActual = 1;
                sendVariable = new object[3];
                sendVariable[0] = " (" + subProcesoActual + "/" + totalSubProcesos + ")" + " Cargando Archivo Excel...";
                sendVariable[1] = suma_porcentaje_total;
                sendVariable[2] = "Registros procesados: (0/0)";
                Clients.Caller.sendMessage(sendVariable);

                //CERRAR CONEXION ARCHIVO EXCEL
                connEXCEL.Close();
                //SI EXISTE ELIMINALO
                if (File.Exists(filePath))
                {
                    File.Delete(filePath);
                }

                //****************************
                //SUBPROCESO REINICIAR TABLA NUEVO PRODUCTO
                //****************************


                //string path = System.Web.HttpContext.Current.Server.MapPath("").Split('\\').ElementAt(System.Web.HttpContext.Current.Server.MapPath("").Split('\\').Length - 2);
                //MySqlConnection dbConn = new MySqlConnection(Tienda_Online.Areas.POS.Fn.ConnDB.getConnString(path));
                string path = System.Web.HttpContext.Current.Server.MapPath("").Split('\\').ElementAt(System.Web.HttpContext.Current.Server.MapPath("").Split('\\').Length - 2);
                MySqlConnection dbConn = new MySqlConnection(POS.Fn.ConnDB.getConnString(path));

                int idusuarioLoggeado = usuario_logeado.id();
                string queryTruncateNuevoProducto = "DELETE FROM nuevo_producto where idUsuarioActualiza=" + idusuarioLoggeado + ";";
                dbConn.Open();
                MySqlCommand cmd = new MySqlCommand(queryTruncateNuevoProducto, dbConn);
                cmd.ExecuteNonQuery();
                dbConn.Close();

                //ENVIAR AVANCE ACTUAL -> TABLA TRUNCADA CORRECTAMENTE
                //PORCENAJE 25% - 50%
                suma_porcentaje_total += porcentaje_por_subproceso;
                subProcesoActual = 2;
                sendVariable = new object[3];
                sendVariable[0] = " (" + subProcesoActual + "/" + totalSubProcesos + ")" + " Reiniciando DB...";
                sendVariable[1] = suma_porcentaje_total;
                sendVariable[2] = sendVariable[2] = "Registros procesados: (0/0)";
                Clients.Caller.sendMessage(sendVariable);



                //***********************************************
                //SUBPROCESO AGREGAR REGISTROS A LA BASE DE DATOS
                //***********************************************

                //CONTAR REGISTROS EXCEL
                int totalContarRegistros = data.Rows.Count;
                int Agregado = 0;
                decimal porcentaje_por_registro = totalContarRegistros != 0 ? porcentaje_por_subproceso / totalContarRegistros : 0;
                int Contar100 = 0;
                string queryAgregarNuevoProducto = "";
                int contarRegistrosProcesados = 0;
                foreach (System.Data.DataRow row in data.Rows)
                {
                    var values = row.ItemArray;
                    nuevo_producto ex = new nuevo_producto();
                    ex.codigo = Convert.ToString(values[0]).Trim().Replace('"', '\"').Replace("'", "\'").ToUpper();
                    if (ex.codigo != "")
                    {
                        ex.descripcion = Convert.ToString(values[1]).Replace('"', '\"').Replace("'", "\'").TrimEnd().TrimStart().ToUpper();
                        ex.descripcion_detallada = Convert.ToString(values[2]).Replace('"', '\"').Replace("'", "\'").TrimEnd().TrimStart().ToUpper();
                        ex.marca = Convert.ToString(Convert.ToString(values[3]).Trim().Replace('"', '\"').Replace("'", "\'").ToUpper());
                        ex.familia = Convert.ToString(values[4]).Trim().Replace('"', '\"').Replace("'", "\'").ToUpper();
                        ex.subfamilia = Convert.ToString(values[5]).Trim().Replace('"', '\"').Replace("'", "\'").ToUpper();
                        ex.proveedor = Convert.ToString(values[6]).Trim().Replace('"', '\"').Replace("'", "\'").ToUpper();
                        ex.costo = values[7].ToString() == "" ? 0 : Convert.ToDecimal(Convert.ToString(values[7]).Trim().Replace('"', '\"').Replace("'", "\'").ToUpper());
                        //IVA  INICIO
                        decimal iva_default = Configuracion.TraerConfiguracionGeneral().iva != null ? Configuracion.TraerConfiguracionGeneral().iva.Value : 0;
                        decimal aux;
                        ex.iva = values[8].ToString() == "" ? iva_default.ToString() : Convert.ToString(values[8]).Trim().Replace('"', '\"').Replace("'", "\'").ToUpper();
                        if (!Decimal.TryParse(ex.iva, out aux))
                            ex.iva = iva_default.ToString();
                        //IVA   FIN
                        ex.medida = Convert.ToString(Convert.ToString(values[9]).Trim().Replace('"', '\"').Replace("'", "\'").ToUpper());
                        ex.modelo = Convert.ToString(Convert.ToString(values[10]).Trim().Replace('"', '\"').Replace("'", "\'").ToUpper());
                        //ex.precio_dolar = Convert.ToInt16(values[11]);
                        //ex.publicar = Convert.ToInt16(values[12]);
                        //ex.carrusel = Convert.ToInt16(values[13]);
                        ex.publicar = Convert.ToString(values[11]).Trim();
                        ex.carrusel = Convert.ToString(values[12]).Trim();
                        ex.productoPadre = Convert.ToString(values[13]).Trim().Replace('"', '\"').Replace("'", "\'").ToUpper();
                        ex.codigoProductoPadre = Convert.ToString(values[14]).Trim().Replace('"', '\"').Replace("'", "\'").ToUpper();
                        if (conf.venta_consignacion.Value)
                        {
                            ex.consignacion = Convert.ToString(values[15]).Trim();
                        }

                        ex.copiadoCatolgoDescripcion = 0;


                        queryAgregarNuevoProducto +=
                            "INSERT INTO nuevo_producto"
                                + "("
                                + "codigo,"
                                + "descripcion,"
                                + "descripcion_detallada,"
                                + "marca,"
                                + "familia,"
                                + "subfamilia,"
                                + "proveedor,"
                                + "costo,"
                                + "iva,"
                                + "medida,"
                                + "modelo,"
                                //+ "precio_dolar,"
                                + "publicar,"
                                + "carrusel,"
                                + "consignacion,"
                                + "copiadoCatolgoDescripcion,"
                                + "idUsuarioActualiza,"
                                + "productoPadre,"
                                + "codigoProductoPadre)"
                                + "VALUES"
                                + "("
                                + "'" + ex.codigo.Replace("'", "\\'") + "'" + ","
                                + "'" + ex.descripcion.Replace("'", "\\'") + "'" + ","
                                + "'" + ex.descripcion_detallada.Replace("'", "\\'") + "'" + ","
                                + "'" + ex.marca.Replace("'", "\\'") + "'" + ","
                                + "'" + ex.familia.Replace("'", "\\'") + "'" + ","
                                + "'" + ex.subfamilia.Replace("'", "\\'") + "'" + ","
                                + "'" + ex.proveedor.Replace("'", "\\'") + "'" + ","
                                + "'" + ex.costo + "'" + ","
                                + "'" + ex.iva + "'" + ","
                                + "'" + ex.medida + "'" + ","
                                + "'" + ex.modelo + "'" + ","
                                //+ ex.precio_dolar+ ","
                                + "'" + ex.publicar + "',"
                                + "'" + ex.carrusel + "',"
                                + "'" + ex.consignacion + "',"
                                + "0,"
                                + idusuarioLoggeado + ","
                                + "'" + ex.productoPadre.Replace("'", "\\'") + "'" + ","
                                + "'" + ex.codigoProductoPadre.Replace("'", "\\'") + "'"
                                + ");";

                        contarRegistrosProcesados++;
                        Contar100++;
                        if (Contar100 == 100)
                        {
                            if (queryAgregarNuevoProducto != "")
                            {
                                using (MySqlCommand com = new MySqlCommand(queryAgregarNuevoProducto, dbConn))
                                {
                                    Agregado += 100;
                                    Contar100 = 0;
                                    dbConn.Open();
                                    com.ExecuteNonQuery();
                                    dbConn.Close();
                                    //calcular porcentaje
                                    msg = "Agregando Registros A DB...";
                                    subProcesoActual = 3;
                                    sendVariable = new object[3];
                                    sendVariable[0] = " (" + subProcesoActual + "/" + totalSubProcesos + ") " + msg;
                                    sendVariable[1] = suma_porcentaje_total;
                                    sendVariable[2] = "Registros procesados: (" + Agregado + "/" + totalContarRegistros + ")";
                                    Clients.Caller.sendMessage(sendVariable);
                                }
                                queryAgregarNuevoProducto = "";
                            }
                        }
                    }

                }

                //SI QUEDA UN PRODUCTO POR AGREGAR
                if (queryAgregarNuevoProducto != "")
                {
                    using (MySqlCommand com = new MySqlCommand(queryAgregarNuevoProducto, dbConn))
                    {
                        dbConn.Open();
                        com.ExecuteNonQuery();
                        dbConn.Close();
                        //calcular porcentaje
                        msg = "Agregando Registros A DB...";
                        subProcesoActual = 3;
                        sendVariable = new object[3];
                        sendVariable[0] = " (" + subProcesoActual + "/" + totalSubProcesos + ") " + msg;
                        sendVariable[1] = 100;
                        sendVariable[2] = "Registros procesados: (" + totalContarRegistros + "/" + totalContarRegistros + ")";
                        Clients.Caller.sendMessage(sendVariable);
                    }
                }



                //****************************
                //SUBPROCESO REVISAR ARCHIVO
                //****************************

                int Analizado = 0;
                List<nuevo_producto> ListaNuevosProductos = db.nuevo_producto.Where(x => x.idUsuarioActualiza == idusuarioLoggeado && x.copiadoCatolgoDescripcion != 1).ToList();
                List<producto> ListaProductos = db.producto.ToList();
                List<ModeloNuevoProducto> ListaModeloNuevosPreoductos = new List<ModeloNuevoProducto>();

                porcentaje_por_registro = porcentaje_por_subproceso / ListaNuevosProductos.Count();

                List<string> ProductosExistentes = db.producto.Select(x => x.Codigo).Distinct().ToList();
                foreach (nuevo_producto nuevo_producto in ListaNuevosProductos)
                {
                    bool existe = ProductosExistentes.Contains(nuevo_producto.codigo);
                    int nuevo = 0;
                    if (!existe)
                    {
                        nuevo = 1;
                        string queryNP = "UPDATE NUEVO_PRODUCTO SET NUEVO=" + nuevo;
                        // I N I C I O  -> si esta vacio pon N/A
                        if (nuevo_producto.marca == "" || nuevo_producto.familia == "" || nuevo_producto.subfamilia == "" || nuevo_producto.proveedor == "")
                        {
                            queryNP += ",";
                            queryNP += nuevo_producto.marca == "" ? "marca='NO APLICA'," : "";
                            queryNP += nuevo_producto.familia == "" ? "familia='NO APLICA'," : "";
                            queryNP += nuevo_producto.subfamilia == "" ? "subfamilia='NO APLICA'," : "";
                            queryNP += nuevo_producto.proveedor == "" ? "proveedor='NO APLICA'," : "";
                            queryNP = queryNP.Remove(queryNP.Length - 1);
                        }
                        queryNP += " WHERE id=" + nuevo_producto.id + " AND idUsuarioActualiza=" + idusuarioLoggeado + " ;";
                        //       F I N  -> si esta vacio pon N/A


                        dbConn.Open();
                        cmd = new MySqlCommand(queryNP, dbConn);
                        cmd.ExecuteNonQuery();
                        dbConn.Close();
                    }
                    Analizado++;

                    //CALCULAR PORCENTAJE
                    //PORCENAJE 75% - 100%
                    suma_porcentaje_total += porcentaje_por_registro;
                    // int porc = Analizado * 100 / totalContarRegistros;
                    //ENVIAR AVANCE ACTUAL
                    msg = "Analizando Registros...";
                    subProcesoActual = 4;
                    sendVariable = new object[3];
                    sendVariable[0] = " (" + subProcesoActual + "/" + totalSubProcesos + ") " + msg;
                    sendVariable[1] = suma_porcentaje_total;
                    sendVariable[2] = "Registros procesados: (" + Analizado + "/" + totalContarRegistros + ")";// string.Format(" ({0}/{1})", subProcesoActual, totalSubProcesos);
                    Clients.Caller.sendMessage(sendVariable);
                }

                int subidos = db.nuevo_producto.Where(x => x.idUsuarioActualiza == idusuarioLoggeado).Count();
                if (totalContarRegistros == subidos)
                {
                    Clients.Caller.ExcelSubidoDB_OK();
                    if (subidos == Analizado)
                    {
                        Clients.Caller.Analizado_OK();
                    }
                    else
                    {
                        Clients.Caller.error("No Analizaron " + Analizado + " de " + subidos + ". Favor de revisar tu archivo y volver a intertarlo.");

                    }
                }
                else
                {
                    Clients.Caller.error("Se subieron " + subidos + " de " + totalContarRegistros + ". Favor de revisar tu archivo y volver a intertarlo.");

                }

            }
            catch (Exception e)
            {
                object[] sendVariable = new object[2];
                sendVariable[0] = "Error:" + e.Message;
                sendVariable[0] += e.InnerException != null ? ".      Inner:" + e.InnerException.ToString() : "";
                sendVariable[0] += ".           El Archivo Existe?: " + existearchivo.ToString();
                sendVariable[1] = 0;
                Clients.Caller.sendMessage(sendVariable);
                Clients.Caller.error(e.Message.ToString());
            }
        }

        public void Producto_RevisarArchivo_DB()
        {

            //VAR
            moragas_pruebasEntities1 db = new moragas_pruebasEntities1();
            int idusuarioLoggeado = usuario_logeado.id();
            List<nuevo_producto> ListaNuevosProductos = db.nuevo_producto.Where(x => x.idUsuarioActualiza == idusuarioLoggeado && x.copiadoCatolgoDescripcion != 1).ToList();
            List<producto> ListaProductos = db.producto.ToList();
            List<ModeloNuevoProducto> ListaModeloNuevosPreoductos = new List<ModeloNuevoProducto>();

            int totalContarRegistros = ListaNuevosProductos.Count();
            int Agregado = 0;


            List<string> ProductosExistentes = db.producto.Select(x => x.Codigo).Distinct().ToList();
            foreach (nuevo_producto nuevo_producto in ListaNuevosProductos)
            {


                ModeloNuevoProducto modelo_nuevo_producto = new ModeloNuevoProducto();
                int nuevo = 1;
                bool existe = ProductosExistentes.Contains(nuevo_producto.codigo);
                if (existe)
                    nuevo = 0;
                modelo_nuevo_producto.proveedor = nuevo_producto.proveedor;
                modelo_nuevo_producto.codigo = nuevo_producto.codigo;
                modelo_nuevo_producto.descripcion = nuevo_producto.descripcion;
                modelo_nuevo_producto.familia = nuevo_producto.familia;
                modelo_nuevo_producto.subfamilia = nuevo_producto.subfamilia;
                if (nuevo == 1)
                {
                    modelo_nuevo_producto.estatus = "Nuevo";
                }
                else
                {
                    modelo_nuevo_producto.estatus = "Actualizacion";
                }
                ListaModeloNuevosPreoductos.Add(modelo_nuevo_producto);
                //AUMENTAR REVISADOS 1
                Agregado++;
                //CALCULAR PORCENTAJE
                int porc = Agregado * 100 / totalContarRegistros;
                //ENVIAR AVANCE ACTUAL
                msg = "Cargando Registros En DB...";
                Clients.Caller.sendMessage(string.Format
                        (msg + " {0}% de {1}%", porc, count));

            }
        }
    }
}