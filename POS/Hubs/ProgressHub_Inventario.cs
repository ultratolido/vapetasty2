﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using Microsoft.AspNet.SignalR;
using POS.Models;
using System.Data;
using System.Configuration;
using MySql.Data.MySqlClient;
using System.IO;
using System.Web.Script.Serialization;

namespace POS.Hubs
{
    public class ProgressHub_inventario : Hub
    {
        public string msg = "Iniciando...";
        JavaScriptSerializer respuesta = new JavaScriptSerializer();
        public int count = 100;

        public void AplicarCambiosExcel()
        {
            //VARIABLES

            ////SUB-PROCESOS
            Inventario_Actualizar_inventario();
            //Inventario_insertar_Temp_actualiza(id_sucursal);
            //Inventario_Actualizar_inventario(id_sucursal);
        }
        public void Inventario_Actualizar_inventario()
        {
            //SUB-PROCESOS
            //1.- CREAR SCRIPT
            //2.- CORRER SCRIPT
            try
            {
                //****************************
                //INICIALIZANDO
                //****************************                
                object[] sendVariable = new object[2];
                sendVariable = new object[3];
                sendVariable[0] = " Inicializando actualiza inventario...";
                sendVariable[1] = 0;
                sendVariable[2] = "Registros procesados: (0/0)";// string.Format(" ({0}/{1})", subProcesoActual, totalSubProcesos);
                Clients.Caller.sendMessage(sendVariable);

                //****************************
                //GENERANDO QUERY SQL
                //****************************                

                sendVariable = new object[3];
                sendVariable[0] = " (1/2) Generando Query Inventario...";
                sendVariable[1] = 0;
                sendVariable[2] = "Registros procesados: (0/0)";
                Clients.Caller.sendMessage(sendVariable);


                int idUsuarioLoggeado = Fn.usuario_logeado.id();
                moragas_pruebasEntities1 db = new moragas_pruebasEntities1();
                List<C_subirinvenarioexcel> actualiaInventario = db.C_subirinvenarioexcel.Where(x => x.Producto_id != "" && x.idUsuarioActualiza == idUsuarioLoggeado).ToList();
                //List<producto> ListaProductosRegistrados = db.producto.ToList();

                bool success = false;
                string _msg = "";

                //******************
                //DB CONEXION*********
                //******************
                string path = System.Web.HttpContext.Current.Server.MapPath("").Split('\\').ElementAt(System.Web.HttpContext.Current.Server.MapPath("").Split('\\').Length - 2);
                MySqlConnection dbConn = new MySqlConnection(Fn.ConnDB.getConnString(path));

                //******************
                //PRODUCTOS*********
                //******************

                // PRODUCTOS -> ELIMINAR CODIGOS DUPLICADOS LISTA EXCEL
                //List<temp_movimientos_actualiza_inventario> distinctProductos = lista_productos
                //          .GroupBy(x => x.Codigo)
                //          .Select(x => x.First())
                //          .ToList();

                String query_inventario = "";
                decimal actualizado = 0;
                decimal totales = actualiaInventario.Count();
                int contar100 = 0;
                foreach (C_subirinvenarioexcel i in actualiaInventario)
                {

                    query_inventario +=
                        //SI EXISTE EL INVENTARIO PARA ESE PRODUCTO EN ESA SUCURSAL Y ESA UBICACION ACTUALIZALO 
                        "UPDATE INVENTARIO SET cantidad='" + i.CantidadNueva + "' WHERE sucursal_id=" + i.sucursal_id + " AND Producto_id=" + i.Producto_id + ";"
                        //SI NO EXISTE EL INVENTARIO PARA ESE PRODUCTO EN ESA SUCURSAL Y ESA UBICACION INSERTALO 
                         + " INSERT INTO INVENTARIO(Producto_id,sucursal_id,cantidad)"
                                                + " SELECT * FROM ( SELECT "
                                                + "" + i.Producto_id + " as _Producto_id,"
                                                + i.sucursal_id + "  as _SUCURSAL_ID,"
                                                + "'" + i.CantidadNueva + "'  as _cantidad"
                                                + ") AS tmp"
                                                + " WHERE NOT EXISTS ("
                                                + "    SELECT * FROM INVENTARIO WHERE sucursal_id=" + i.sucursal_id + " AND Producto_id=" + i.Producto_id
                                                + ") LIMIT 1;";



                    //+" INSERT INTO INVENTARIO(PRODUCTO_ID,SUCURSAL_ID,CANTIDAD,UBICACION)"
                    //+ " SELECT * FROM "
                    //+ "(SELECT "
                    //+ i.Producto_id + "  as _producto_id,"
                    //+ i.sucursal_id + " as _SUCURSAL_ID,"
                    //+ "'" + i.CantidadNueva + "'  as _cantidad,"
                    //+ "'" + i.ubicacion + "'  as _ubicacion"
                    //+ ") AS tmp"
                    //+ " WHERE NOT EXISTS ("
                    //+ "    SELECT * FROM INVENTARIO WHERE SUCURSAL_ID=" + i.sucursal_id + " AND PRODUCTO_ID=" + i.Producto_id
                    //+ ") LIMIT 1;";

                    //ACTUALIZA STATUS COPIADO DE NUEVO PRODUCTO

                    //query_inventario += " UPDATE _tempdetallesajusteinventario set ajusteEjecutado=1 where id='" + ajuste.id + "';";
                    ////calcular porcentaje
                    //actualizado++;
                    //int porcentaje = (actualizado * 50) / totales;
                    //sendVariable = new object[3];
                    //sendVariable[0] = " (2/2) Aplicando Cambios...";
                    //sendVariable[1] = porcentaje;
                    //sendVariable[2] = "Registros procesados: (" + actualizado + "/" + totales + ")";
                    //Clients.Caller.sendMessage(sendVariable);

                    //EJECUTAR CONSULTA CADA 100 REGISTROS
                    if (contar100 == 100)
                    {
                        actualizado = +100;
                        if (query_inventario != "")
                        {
                            using (MySqlCommand com = new MySqlCommand(query_inventario, dbConn))
                            {
                                dbConn.Open();
                                com.ExecuteNonQuery();
                                dbConn.Close();

                                sendVariable = new object[3];
                                sendVariable[0] = " (2/2) Aplicando Cambios...";
                                sendVariable[1] = actualizado == totales ? 100 : Math.Round(actualizado * 100 / totales, 2) - 1;
                                sendVariable[2] = "Registros procesados: (" + actualizado + "/" + totales + ")";
                                Clients.Caller.sendMessage(sendVariable);
                            }
                        }
                        //REINICIAR QUERY
                        query_inventario = "";
                        //reiniciar contador
                        contar100 = 0;
                    }
                    //AUMENTAR CONTADOR 1
                    contar100++;
                }
                //SI QUEDAN PENDIENTES EJECUTALOS
                if (query_inventario != "")
                {
                    using (MySqlCommand com = new MySqlCommand(query_inventario, dbConn))
                    {
                        dbConn.Open();
                        com.ExecuteNonQuery();
                        dbConn.Close();
                        //calcular porcentaje
                        sendVariable = new object[3];
                        sendVariable[0] = " (2/2) Aplicando Cambios...";
                        sendVariable[1] = 100;
                        sendVariable[2] = "Registros procesados: (" + totales + "/" + totales + ")";
                        Clients.Caller.sendMessage(sendVariable);
                    }
                }
            }
            catch (Exception e)
            {
                object[] sendVariable = new object[2];
                sendVariable[0] = "ERROR:" + e.Message;
                sendVariable[1] = 0;
                Clients.Caller.sendMessage(sendVariable);
                Clients.Caller.error(e.Message.ToString());
            }
        }

        //public void Producto_Actualizar_inventario(int idSucursal)
        //{
        //    //VARIABLES
        //    // actualizar_idTipoPrecio : 0     - NO ACTUALIZAR
        //    // actualizar_descripcion  : FALSE - NO ACTUALIZAR


        //    //SUB-PROCESOS
        //    //1.- CREAR SCRIPT
        //    //2.- CORRER SCRIPT

        //    try
        //    {
        //        //****************************
        //        //INICIALIZANDO
        //        //****************************                
        //        object[] sendVariable = new object[2];
        //        sendVariable = new object[3];
        //        sendVariable[0] = " Inicializando Actualizar Inventario...";
        //        sendVariable[1] = 0;
        //        sendVariable[2] = "Registros procesados: (0/0)";// string.Format(" ({0}/{1})", subProcesoActual, totalSubProcesos);
        //        Clients.Caller.sendMessage(sendVariable);

        //        //****************************
        //        //GENERANDO QUERY SQL
        //        //****************************                

        //        sendVariable = new object[3];
        //        sendVariable[0] = " (1/2) Generando Query Actualizar Inventario...";
        //        sendVariable[1] = 0;
        //        sendVariable[2] = "Registros procesados: (0/0)";
        //        Clients.Caller.sendMessage(sendVariable);



        //        moragas_pruebasEntities1 db = new moragas_pruebasEntities1();
        //        List<temp_movimientos_actualiza_inventario> lista_productos = db.temp_movimientos_actualiza_inventario.Where(x => x.copiado != 1).ToList();
        //        //List<producto> ListaProductosRegistrados = db.producto.ToList();

        //        bool success = false;
        //        string _msg = "";

        //        //******************
        //        //DB CONEXION*********
        //        //******************
        //        MySqlConnection dbConn = new MySqlConnection(POS.Properties.Resources.ConnectionStringDB);

        //        //******************
        //        //PRODUCTOS*********
        //        //******************

        //        // PRODUCTOS -> ELIMINAR CODIGOS DUPLICADOS LISTA EXCEL
        //        List<temp_movimientos_actualiza_inventario> distinctProductos = lista_productos
        //                  .GroupBy(x => x.Codigo)
        //                  .Select(x => x.First())
        //                  .ToList();

        //        String query_inventario = "";
        //        int actualizado = 0;
        //        int totales = distinctProductos.Count();
        //        int contar100 = 0;
        //        foreach (temp_movimientos_actualiza_inventario i in distinctProductos)
        //        {
        //            i.Descripcion = i.Descripcion.Replace("'", "\\'");
        //            i.Descripcion = i.Descripcion.Replace('"', '\"');


        //            //SI EL CODIGO ES DIFERENTE A NULL O VACIO
        //            if (i.Codigo != null && i.Codigo != "")
        //            {
        //                Decimal n;
        //                //REVISAR SI CANTIDAD ES NUMERO Y QUE CODIG
        //                if (Decimal.TryParse(i.cantidad.Value.ToString(), out n))
        //                {

        //                    query_inventario +=
        //                        //ACTUALIZA SI EXISTE
        //                       "UPDATE inventario SET "
        //                    + "  cantidad=" + i.cantidad
        //                    + "  ubicacion=" + i.ubicacion
        //                    + " WHERE sucursal_id= " + idSucursal
        //                    + " AND Producto_id= (SELECT id FROM producto WHERE codigo = '" + i.Codigo + "');"

        //                    //INSERTA SI NO EXISTE
        //                    + " INSERT INTO inventario(sucursal_id,Producto_id,cantidad,ubiacion)"
        //                   + " SELECT * FROM ( SELECT "
        //                       + "'" + idSucursal + "' as sucursal_id,"
        //                       + "'" + i.cantidad + "'  as cantidad,"
        //                       + " (SELECT id FROM producto WHERE codigo = '" + p.codigo + "')  as _producto_id,"
        //                       + " (SELECT sucursal_id FROM tipocliente WHERE id = '" + idTipoPrecio + "')  as _id_sucursal"
        //                      + ") AS tmp"
        //                    + " WHERE NOT EXISTS ("
        //                    + "    SELECT id FROM precio_por_tipocliente WHERE TipoCliente_id= " + idTipoPrecio
        //                    + " AND Producto_id= (SELECT id FROM producto WHERE codigo = '" + p.codigo + "')"
        //                    + " AND sucursal_id= (SELECT sucursal_id FROM tipocliente WHERE id = " + idTipoPrecio + ")"
        //                    + ") LIMIT 1;"
        //                        //ACTUALIZA STATUS COPIADO DE NUEVO PRODUCTO
        //                 + " UPDATE nuevo_producto set copiadoPrecio=1 where codigo='" + p.codigo + "';";
        //                }
        //            }

        //            //calcular porcentaje
        //            actualizado++;
        //            int porcentaje = ((actualizado * 50) / totales) + 50;
        //            sendVariable = new object[3];
        //            sendVariable[0] = " (2/2) Aplicando Cambios...";
        //            sendVariable[1] = porcentaje;
        //            sendVariable[2] = "Registros procesados: (" + actualizado + "/" + totales + ")";
        //            Clients.Caller.sendMessage(sendVariable);

        //            //EJECUTAR CONSULTA CADA 100 REGISTROS
        //            if (contar100 == 100)
        //            {
        //                if (query_productos != "")
        //                {
        //                    using (MySqlCommand com = new MySqlCommand(query_productos, dbConn))
        //                    {
        //                        dbConn.Open();
        //                        com.ExecuteNonQuery();
        //                        dbConn.Close();
        //                    }
        //                }
        //                //REINICIAR QUERY
        //                query_productos = "";
        //                //reiniciar contador
        //                contar100 = 0;
        //            }
        //            //AUMENTAR CONTADOR 1
        //            contar100++;
        //        }
        //        //SI QUEDAN PENDIENTES EJECUTALOS
        //        if (query_productos != "")
        //        {
        //            using (MySqlCommand com = new MySqlCommand(query_productos, dbConn))
        //            {
        //                dbConn.Open();
        //                com.ExecuteNonQuery();
        //                dbConn.Close();
        //                //calcular porcentaje
        //                sendVariable = new object[3];
        //                sendVariable[0] = " (2/2) Aplicando Cambios...";
        //                sendVariable[1] = 100;
        //                sendVariable[2] = "Registros procesados: (" + totales + "/" + totales + ")";
        //                Clients.Caller.sendMessage(sendVariable);

        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        object[] sendVariable = new object[2];
        //        sendVariable[0] = "Error:" + e.Message;
        //        sendVariable[1] = 0;
        //        Clients.Caller.sendMessage(sendVariable);
        //    }
        //}


        public void Inventario_insertar_Temp_movimientos(string fileName, int sucursal_id)
        {
            //VARIABLES

            //SUB-PROCESOS
            //1.- CARGAR EXCEL
            //2.- TRUNCAR TABLA NUEVO PRODUCTO
            //3.- CARGAR REGISTROS A TABLA NUEVO PRODUCTO
            //4.- ANALIZAR REGISTROS


            int totalSubProcesos = 3;
            int subProcesoActual = 0;

            decimal suma_porcentaje_total = 0;
            decimal porcentaje_por_subproceso = 100 / totalSubProcesos;

            bool existearchivo = false;
            string querySQL = "";

            try
            {
                //****************************
                //INICIALIZANDO
                //****************************
                {
                    subProcesoActual = 0;
                    var respuestaJson = new
                    {
                        avanceSubProcesos = crearStringAvanceProceso(subProcesoActual, totalSubProcesos, "...Iniciando."),
                        porcentaje = 0,
                        avanceRegistrosLabel = "Registros procesados: (0/0)",
                        avanceRegistros = 0
                    };
                    Clients.Caller.sendMessage(respuesta.Serialize(respuestaJson));
                }
                string filePath = AppDomain.CurrentDomain.BaseDirectory + "/ArchivosExcel/" + fileName;


                if (File.Exists(filePath))
                { existearchivo = true; }
                var connectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1\"", filePath);
                System.Data.OleDb.OleDbConnection connEXCEL = new System.Data.OleDb.OleDbConnection();
                connEXCEL.ConnectionString = connectionString;
                connEXCEL.Open();

                System.Data.DataTable dbSchema = connEXCEL.GetOleDbSchemaTable(System.Data.OleDb.OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
                string Sheet1 = dbSchema.Rows[0].Field<string>("TABLE_NAME");
                var adapter = new System.Data.OleDb.OleDbDataAdapter("SELECT * FROM [" + Sheet1 + "]", connectionString);
                var ds = new DataSet();
                adapter.Fill(ds, "results");
                System.Data.DataTable data = ds.Tables["results"];



                //PORCENAJE 0% - 25%
                suma_porcentaje_total = +porcentaje_por_subproceso;

                {
                    subProcesoActual = 1;
                    var respuestaJson = new
                    {
                        avanceSubProcesos = crearStringAvanceProceso(subProcesoActual, totalSubProcesos, "Cargando Archivo Excel..."),
                        porcentaje = suma_porcentaje_total,
                        avanceRegistrosLabel = "Registros procesados: (0/0)",
                        avanceRegistros = 0
                    };
                    Clients.Caller.sendMessage(respuesta.Serialize(respuestaJson));
                }


                //CERRAR CONEXION ARCHIVO EXCEL
                connEXCEL.Close();
                //SI EXISTE ELIMINALO
                if (File.Exists(filePath))
                {
                    File.Delete(filePath);
                }

                //****************************
                //SUBPROCESO REINICIAR TABLA NUEVO PRODUCTO
                //****************************

                string path = System.Web.HttpContext.Current.Server.MapPath("").Split('\\').ElementAt(System.Web.HttpContext.Current.Server.MapPath("").Split('\\').Length - 2);
                MySqlConnection dbConn = new MySqlConnection(Fn.ConnDB.getConnString(path));

                int idusuarioLoggeado = Fn.usuario_logeado.id();
                string queryTruncateTempActualizaInventario = "DELETE FROM _subirinvenarioexcel WHERE idUsuarioActualiza=" + idusuarioLoggeado + ";";
                dbConn.Open();
                MySqlCommand cmd = new MySqlCommand(queryTruncateTempActualizaInventario, dbConn);
                cmd.ExecuteNonQuery();
                dbConn.Close();

                //ENVIAR AVANCE ACTUAL -> TABLA TRUNCADA CORRECTAMENTE
                //PORCENAJE 25% - 50%
                suma_porcentaje_total += porcentaje_por_subproceso;

                {
                    subProcesoActual = 2;
                    var respuestaJson = new
                    {
                        avanceSubProcesos = crearStringAvanceProceso(subProcesoActual, totalSubProcesos, "Reiniciando DB..."),
                        porcentaje = suma_porcentaje_total,
                        avanceRegistrosLabel = "Registros procesados: (0/0)",
                        avanceRegistros = 0
                    };
                    Clients.Caller.sendMessage(respuesta.Serialize(respuestaJson));
                }


                //***********************************************
                //SUBPROCESO AGREGAR REGISTROS A LA BASE DE DATOS
                //***********************************************

                //CONTAR REGISTROS EXCEL
                int totalContarRegistros = data.Rows.Count;
                int Agregado = 0;
                decimal porcentaje_por_registro = porcentaje_por_subproceso / totalContarRegistros;
                int Contar100 = 0;
                string queryAgregarTempActInventario = "";
                List<string> sucursalNoEncontrada = new List<string>();
                List<string> codigoProdCantidadNoDecimal = new List<string>();
                moragas_pruebasEntities1 db = new moragas_pruebasEntities1();
                List<sucursal> LSucursales = db.sucursal.ToList();
                foreach (System.Data.DataRow row in data.Rows)
                {
                    var values = row.ItemArray;
                    POS.Models.C_subirinvenarioexcel ex = new POS.Models.C_subirinvenarioexcel();
                    ex.Codigo = Convert.ToString(values[0]).Trim().Replace('"', '\"').Replace("'", "\'").ToUpper();
                    ex.sucursal = Convert.ToString(values[1]).Replace('"', '\"').Replace("'", "\'").TrimEnd().TrimStart().ToUpper();
                    ex.CantidadNueva = Convert.ToString(values[2]).Replace('"', '\"').Replace("'", "\'").TrimEnd().TrimStart().ToUpper();
                    ex.Copiado = false;

                    //SI CANTIDAD ES NUMERICO ENTONCES GUARDA LA CONVERSION DE ESA CADENA EN CANTIDAD_DECIMAL
                    //DE LO CONTRARIO GUARDA CANTIDAD VACIO Y GUARDA CantidadesDecimalOK = FALSE
                    Decimal cantInt = 0;
                    if (Decimal.TryParse(ex.CantidadNueva, out cantInt))
                        ex.CantidadNueva = Convert.ToDecimal(ex.CantidadNueva).ToString();
                    else
                        codigoProdCantidadNoDecimal.Add(ex.Codigo);


                    //SI SUCURSAL EXISTE ENTONCES GUARDA EL ID
                    //DE LO CONTRARIO NO GUARDES ID

                    if (LSucursales.Where(x => x.nombre_sucursal.ToUpper() == ex.sucursal.ToUpper().TrimEnd().TrimStart()).Any())
                        ex.sucursal_id = LSucursales.Where(x => x.nombre_sucursal.ToUpper() == ex.sucursal.ToUpper().TrimEnd().TrimStart()).First().id;
                    else
                        sucursalNoEncontrada.Add(ex.sucursal);




                    //string cantidadString = Convert.ToString(Convert.ToString(values[2]).Trim().Replace('"', '\"').Replace("'", "\'").ToUpper());
                    //decimal n;
                    //if (decimal.TryParse(cantidadString, out n)) { ex.cantidad = Convert.ToDecimal(cantidadString); } else { ex.cantidad = 0; }

                    //INSERTAR
                    queryAgregarTempActInventario +=
                        //OBTENER VARIABLES
                         @"SET @Producto_id:=IF((select count(*) from producto where TRIM(codigo)='" + ex.Codigo.Replace("'", "\\'") + "')>=1, (SELECT id from PRODUCTO WHERE TRIM(codigo)='" + ex.Codigo.Replace("'", "\\'") + "') , '');"
                         + "SET @CantidadNueva:='" + ex.CantidadNueva + "';"
                        + "SET @ubicacion:='';"
                        + "SET @Sucursal:='" + ex.sucursal + "';";
                    if (ex.sucursal_id == null)
                    {
                        queryAgregarTempActInventario += "SET @sucursal_id:=NULL;"
                                                 + "SET @CantidadActual:=0;";
                    }
                    else
                    {
                        queryAgregarTempActInventario += "SET @sucursal_id:=" + ex.sucursal_id.Value + ";"
                                                     + "SET @CantidadActual:=(SELECT CANTIDAD FROM INVENTARIO WHERE PRODUCTO_ID=@Producto_id AND SUCURSAL_ID=@sucursal_id);"
                                                    + "SET @Diferencia:=@CantidadNueva-@CantidadActual;";
                    }

                    queryAgregarTempActInventario +=
                    "SET @existeBool:=IF((select count(*) from producto where id=@Producto_id) >=1, 1, 0);"
                        //INSERTAR DETALLE AJUSTE INVENTARIO
                    + "INSERT INTO _subirinvenarioexcel"
                        + "("
                        + "Codigo,"
                        + "Producto_id,"
                        + "Sucursal,"
                        + "sucursal_id,"
                        + "ubicacion,"
                        + "existeBOOL,"
                        + "Copiado,"
                        + "idUsuarioActualiza,"
                        + "Fecha,"
                        + "Duplicado,"
                        + "CantidadActual,"
                        + "CantidadNueva,"
                         + "Diferencia)"

                        + "VALUES"
                        + "("
                        + "'" + ex.Codigo.Replace("'", "\\'") + "'" + ","
                        + " @Producto_id ,"
                        + " @Sucursal ,"
                        + " @sucursal_id,"
                        + " @ubicacion,"
                        + " @existeBool ,"
                        + " 0 ,"
                        + " " + idusuarioLoggeado + ","
                        + " NOW() ,"
                        + " 0 ,"
                        + "@CantidadActual,"
                        + "@CantidadNueva,"
                        + "@Diferencia"
                        + ");";


                    suma_porcentaje_total += porcentaje_por_registro;
                    Contar100++;
                    //CADA 100 REGISTROS CORRE EL SCRIPT
                    if (Contar100 == 100)
                    {

                        if (queryAgregarTempActInventario != "")
                        {
                            querySQL += queryAgregarTempActInventario;
                            using (MySqlCommand com = new MySqlCommand(queryAgregarTempActInventario, dbConn))
                            {
                                Agregado += 100;
                                Contar100 = 0;
                                dbConn.Open();
                                com.ExecuteNonQuery();
                                dbConn.Close();
                                //calcular porcentaje

                                {
                                    subProcesoActual = 3;
                                    var respuestaJson = new
                                    {
                                        avanceSubProcesos = crearStringAvanceProceso(subProcesoActual, totalSubProcesos, "Agregando Registros A DB..."),
                                        porcentaje = suma_porcentaje_total,
                                        avanceRegistrosLabel = "Registros procesados: (" + Agregado + "/" + totalContarRegistros + ")",
                                        avanceRegistros = Agregado
                                    };
                                    Clients.Caller.sendMessage(respuesta.Serialize(respuestaJson));
                                }
                            }
                            queryAgregarTempActInventario = "";
                        }
                    }

                }
                //SI QUEDA ALGÚN  MOVIMIENTO DE INVENTARIO POR AGREGAR
                if (queryAgregarTempActInventario != "")
                {
                    //queryAgregarTempActInventario = @"SET @sucursal_id:='1'; select @sucursal_id";
                    querySQL += queryAgregarTempActInventario;
                    using (MySqlCommand com = new MySqlCommand(queryAgregarTempActInventario, dbConn))
                    {
                        dbConn.Open();
                        com.ExecuteNonQuery();
                        dbConn.Close();
                        //calcular porcentaje
                        //AGREGANDO REGISTRO A DB
                        {
                            subProcesoActual = 3;
                            var respuestaJson = new
                            {
                                avanceSubProcesos = crearStringAvanceProceso(subProcesoActual, totalSubProcesos, "Agregando Registros A DB..."),
                                porcentaje = suma_porcentaje_total,
                                avanceRegistrosLabel = "Registros procesados: (" + Agregado + "/" + totalContarRegistros + ")",
                                avanceRegistros = Agregado
                            };
                            Clients.Caller.sendMessage(respuesta.Serialize(respuestaJson));
                        }
                    }
                }

                if (sucursalNoEncontrada.Count() == 0 && codigoProdCantidadNoDecimal.Count() == 0)
                {
                    //AVISA QUE YA TERMINO DE SUBIR EL ARCHIVO
                    Clients.Caller.ExcelSubidoDB_OK();
                }
                else
                {
                    //ENVIAR ERROR
                    if (sucursalNoEncontrada.Count() != 0)
                    {
                        string msgerror = "<div class=error-box alert style='background-color:#ff9c9c; padding: 15px; width: 100%;'>"
                                            + "<div class=msg style='color: #541a1a; margin-top: 5px;'>"
                                                 + "</div> LAS SIGUIENTES SUCURSALES NO EXISTEN. FAVOR DE PONER EL NOMBRE EXACTAMENTE COMO ESTA EN LA PLATAFORMA.</br> ";
                        foreach (var s in sucursalNoEncontrada)
                        {
                            msgerror += "</br><label style='font-weight:bold'>" + s + "</label>";
                        }
                        msgerror += "</div>"
                                            + "</div>"
                                        + "</div>";
                        Clients.Caller.error(msgerror);
                    }
                    if (codigoProdCantidadNoDecimal.Count() != 0)
                    {
                        string msgerror = "<div class=error-box alert style='background-color:#ff9c9c; padding: 15px; width: 100%;'>"
                                           + "<div class=msg style='color: #541a1a; margin-top: 5px;'>"
                                                 + "</div> LOS SIGUINTES PRODUCTOS CONTIENEN UNA CANTIDAD CON FORMATO INCORRECTO. ESTE CAMPO ADMITE SOLO NUMEROS DEL 1-9 Y PUNTO DECIMAL.</br> ";
                        foreach (var p in codigoProdCantidadNoDecimal)
                        {
                            msgerror += "</br><label style='font-weight:bold'>" + p + "</label>";
                        }
                        msgerror += "</div>"
                                            + "</div>"
                                        + "</div>";
                        Clients.Caller.error(msgerror);

                    }

                }

                //ENVIAR MENSAJE DE CARGA A LA BASE DE DATOS FINALIZADA
                {
                    var respuestaJson = new
                    {
                        avanceSubProcesos = crearStringAvanceProceso(subProcesoActual, totalSubProcesos, "Agregando Registros A DB..."),
                        porcentaje = 100,
                        avanceRegistrosLabel = "Registros procesados: (" + Agregado + "/" + totalContarRegistros + ")",
                        avanceRegistros = Agregado
                    };
                    Clients.Caller.sendMessage(respuesta.Serialize(respuestaJson));
                }
            }
            catch (Exception e)
            {

                var respuestaJson = new
                {
                    avanceSubProcesos = crearStringAvanceProceso(subProcesoActual, totalSubProcesos, " ERROR..." + e.Message.ToString() + "."),
                    porcentaje = 0,
                    avanceRegistrosLabel = "",
                    avanceRegistros = 0
                };
                string ERROR = e.Message.ToString();
                ERROR += e.InnerException != null ? e.InnerException.ToString() : "";
                ERROR += querySQL;
                Clients.Caller.error(ERROR);

                Clients.Caller.sendMessage(respuesta.Serialize(respuestaJson));
            }
        }

        //public void Inventario_insertar_Temp_movimientos(string fileName, int sucursal_id)
        //{
        //    //VARIABLES

        //    //SUB-PROCESOS
        //    //1.- CARGAR EXCEL
        //    //2.- TRUNCAR TABLA NUEVO PRODUCTO
        //    //3.- CARGAR REGISTROS A TABLA NUEVO PRODUCTO
        //    //4.- ANALIZAR REGISTROS


        //    int totalSubProcesos = 3;
        //    int subProcesoActual = 0;

        //    decimal suma_porcentaje_total = 0;
        //    decimal porcentaje_por_subproceso = 100 / totalSubProcesos;

        //    bool existearchivo = false;

        //    try
        //    {
        //        //****************************
        //        //INICIALIZANDO
        //        //****************************
        //        {
        //            subProcesoActual = 0;
        //            var respuestaJson = new
        //            {
        //                avanceSubProcesos = crearStringAvanceProceso(subProcesoActual, totalSubProcesos, "...Iniciando."),
        //                porcentaje = 0,
        //                avanceRegistrosLabel = "Registros procesados: (0/0)",
        //                avanceRegistros = 0
        //            };
        //            Clients.Caller.sendMessage(respuesta.Serialize(respuestaJson));
        //        }
        //        string filePath = AppDomain.CurrentDomain.BaseDirectory + "/ArchivosExcel/" + fileName;


        //        if (File.Exists(filePath))
        //        { existearchivo = true; }
        //        var connectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1\"", filePath);
        //        System.Data.OleDb.OleDbConnection connEXCEL = new System.Data.OleDb.OleDbConnection();
        //        connEXCEL.ConnectionString = connectionString;
        //        connEXCEL.Open();

        //        System.Data.DataTable dbSchema = connEXCEL.GetOleDbSchemaTable(System.Data.OleDb.OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
        //        string Sheet1 = dbSchema.Rows[0].Field<string>("TABLE_NAME");
        //        var adapter = new System.Data.OleDb.OleDbDataAdapter("SELECT * FROM [" + Sheet1 + "]", connectionString);
        //        var ds = new DataSet();
        //        adapter.Fill(ds, "results");
        //        System.Data.DataTable data = ds.Tables["results"];



        //        //PORCENAJE 0% - 25%
        //        suma_porcentaje_total = +porcentaje_por_subproceso;

        //        {
        //            subProcesoActual = 1;
        //            var respuestaJson = new
        //            {
        //                avanceSubProcesos = crearStringAvanceProceso(subProcesoActual, totalSubProcesos, "Cargando Archivo Excel..."),
        //                porcentaje = suma_porcentaje_total,
        //                avanceRegistrosLabel = "Registros procesados: (0/0)",
        //                avanceRegistros = 0
        //            };
        //            Clients.Caller.sendMessage(respuesta.Serialize(respuestaJson));
        //        }


        //        //CERRAR CONEXION ARCHIVO EXCEL
        //        connEXCEL.Close();
        //        //SI EXISTE ELIMINALO
        //        if (File.Exists(filePath))
        //        {
        //            File.Delete(filePath);
        //        }

        //        //****************************
        //        //SUBPROCESO REINICIAR TABLA NUEVO PRODUCTO
        //        //****************************


        //        string ConnectionStringDB = POS.Properties.Resources.ConnectionStringDB;
        //        MySqlConnection dbConn = new MySqlConnection(ConnectionStringDB);
        //        string queryTruncateTempActualizaInventario = "TRUNCATE temp_movimientos_actualiza_inventario;";
        //        dbConn.Open();
        //        MySqlCommand cmd = new MySqlCommand(queryTruncateTempActualizaInventario, dbConn);
        //        cmd.ExecuteNonQuery();
        //        dbConn.Close();

        //        //ENVIAR AVANCE ACTUAL -> TABLA TRUNCADA CORRECTAMENTE
        //        //PORCENAJE 25% - 50%
        //        suma_porcentaje_total += porcentaje_por_subproceso;

        //        {
        //            subProcesoActual = 2;
        //            var respuestaJson = new
        //            {
        //                avanceSubProcesos = crearStringAvanceProceso(subProcesoActual, totalSubProcesos, "Reiniciando DB..."),
        //                porcentaje = suma_porcentaje_total,
        //                avanceRegistrosLabel = "Registros procesados: (0/0)",
        //                avanceRegistros = 0
        //            };
        //            Clients.Caller.sendMessage(respuesta.Serialize(respuestaJson));
        //        }


        //        //***********************************************
        //        //SUBPROCESO AGREGAR REGISTROS A LA BASE DE DATOS
        //        //***********************************************

        //        //CONTAR REGISTROS EXCEL
        //        int totalContarRegistros = data.Rows.Count;
        //        int Agregado = 0;
        //        decimal porcentaje_por_registro = porcentaje_por_subproceso / totalContarRegistros;
        //        int Contar100 = 0;
        //        string queryAgregarTempActInventario = "";
        //        foreach (System.Data.DataRow row in data.Rows)
        //        {
        //            var values = row.ItemArray;
        //            POS.Models.temp_movimientos_actualiza_inventario ex = new POS.Models.temp_movimientos_actualiza_inventario();
        //            ex.Codigo = Convert.ToString(values[0]).Trim().Replace('"', '\"').Replace("'", "\'").ToUpper();
        //            ex.ubicacion = Convert.ToString(values[1]).Replace('"', '\"').Replace("'", "\'").TrimEnd().TrimStart().ToUpper();
        //            string cantidadString = Convert.ToString(Convert.ToString(values[2]).Trim().Replace('"', '\"').Replace("'", "\'").ToUpper());
        //            ex.desc_movimiento = Convert.ToString(values[3]);
        //            ex.sucursal_id = sucursal_id;
        //            ex.copiado = 0;

        //            //SI CANTIDAD_STRING ES NUMERICO ENTONCES GUARDA LA CONVERSION DE ESA CADENA EN CANTIDAD_DECIMAL
        //            //DE LO CONTRARIO GUARDA CANTIDAD_DECIMAL=0
        //            decimal n;
        //            if (decimal.TryParse(cantidadString, out n)) { ex.cantidad = Convert.ToDecimal(cantidadString); } else { ex.cantidad = 0; }

        //            //INSERTAR
        //            queryAgregarTempActInventario +=
        //                "INSERT INTO temp_movimientos_actualiza_inventario"
        //                    + "("
        //                    + "codigo,"
        //                    + "ubicacion,"
        //                    + "cantidad,"
        //                    + "sucursal_id,"
        //                    + "desc_movimiento,"
        //                    + "existeBOOL,"
        //                    + "copiado)"
        //                    + "VALUES"
        //                    + "("
        //                    + "'" + ex.Codigo.Replace("'", "\\'") + "'" + ","
        //                    + "'" + ex.ubicacion.Replace("'", "\\'") + "'" + ","
        //                    + "'" + ex.cantidad + "'" + ","
        //                    + sucursal_id + ","
        //                    + "'" + ex.desc_movimiento + "'" + ","
        //                //SI EXISTE '1', SI NO '0'
        //                    + "IF((select count(*) from producto where codigo=" + "'" + ex.Codigo.Replace("'", "\\'") + "'" + ") >=1, 1, 0),"
        //                    + "0"
        //                    + ");";


        //            suma_porcentaje_total += porcentaje_por_registro;
        //            Contar100++;
        //            //CADA 100 REGISTROS CORRE EL SCRIPT
        //            if (Contar100 == 100)
        //            {

        //                if (queryAgregarTempActInventario != "")
        //                {
        //                    using (MySqlCommand com = new MySqlCommand(queryAgregarTempActInventario, dbConn))
        //                    {
        //                        Agregado += 100;
        //                        Contar100 = 0;
        //                        dbConn.Open();
        //                        com.ExecuteNonQuery();
        //                        dbConn.Close();
        //                        //calcular porcentaje

        //                        {
        //                            subProcesoActual = 3;
        //                            var respuestaJson = new
        //                            {
        //                                avanceSubProcesos = crearStringAvanceProceso(subProcesoActual, totalSubProcesos, "Agregando Registros A DB..."),
        //                                porcentaje = suma_porcentaje_total,
        //                                avanceRegistrosLabel = "Registros procesados: (" + Agregado + "/" + totalContarRegistros + ")",
        //                                avanceRegistros = Agregado
        //                            };
        //                            Clients.Caller.sendMessage(respuesta.Serialize(respuestaJson));
        //                        }
        //                    }
        //                    queryAgregarTempActInventario = "";
        //                }
        //            }

        //        }
        //        //SI QUEDA ALGÚN  MOVIMIENTO DE INVENTARIO POR AGREGAR
        //        if (queryAgregarTempActInventario != "")
        //        {
        //            using (MySqlCommand com = new MySqlCommand(queryAgregarTempActInventario, dbConn))
        //            {
        //                dbConn.Open();
        //                com.ExecuteNonQuery();
        //                dbConn.Close();
        //                //calcular porcentaje
        //                //AGREGANDO REGISTRO A DB
        //                {
        //                    subProcesoActual = 3;
        //                    var respuestaJson = new
        //                    {
        //                        avanceSubProcesos = crearStringAvanceProceso(subProcesoActual, totalSubProcesos, "Agregando Registros A DB..."),
        //                        porcentaje = suma_porcentaje_total,
        //                        avanceRegistrosLabel = "Registros procesados: (" + Agregado + "/" + totalContarRegistros + ")",
        //                        avanceRegistros = Agregado
        //                    };
        //                    Clients.Caller.sendMessage(respuesta.Serialize(respuestaJson));
        //                }
        //            }
        //        }

        //        //ENVIAR MENSAJE DE CARGA A LA BASE DE DATOS FINALIZADA
        //        {
        //            var respuestaJson = new
        //            {
        //                avanceSubProcesos = crearStringAvanceProceso(subProcesoActual, totalSubProcesos, "Agregando Registros A DB..."),
        //                porcentaje = 100,
        //                avanceRegistrosLabel = "Registros procesados: (" + Agregado + "/" + totalContarRegistros + ")",
        //                avanceRegistros = Agregado
        //            };
        //            Clients.Caller.sendMessage(respuesta.Serialize(respuestaJson));
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        object[] sendVariable = new object[2];
        //        sendVariable[0] = "Error:" + e.Message + " El Archivo Existe?: " + existearchivo.ToString();
        //        sendVariable[1] = 0;
        //        Clients.Caller.sendMessage(sendVariable);
        //    }
        //}

        public void Producto_RevisarArchivo_DB()
        {

            //VAR
            POS.Models.moragas_pruebasEntities1 db = new Models.moragas_pruebasEntities1();
            List<nuevo_producto> ListaNuevosProductos = db.nuevo_producto.Where(x => x.copiadoCatolgoDescripcion != 1).ToList();
            List<producto> ListaProductos = db.producto.ToList();
            List<ModeloNuevoProducto> ListaModeloNuevosPreoductos = new List<ModeloNuevoProducto>();

            int totalContarRegistros = ListaNuevosProductos.Count();
            int Agregado = 0;


            List<string> ProductosExistentes = db.producto.Select(x => x.Codigo).Distinct().ToList();
            foreach (nuevo_producto nuevo_producto in ListaNuevosProductos)
            {


                ModeloNuevoProducto modelo_nuevo_producto = new ModeloNuevoProducto();
                int nuevo = 1;
                bool existe = ProductosExistentes.Contains(nuevo_producto.codigo);
                if (existe)
                    nuevo = 0;
                modelo_nuevo_producto.proveedor = nuevo_producto.proveedor;
                modelo_nuevo_producto.codigo = nuevo_producto.codigo;
                modelo_nuevo_producto.descripcion = nuevo_producto.descripcion;
                modelo_nuevo_producto.familia = nuevo_producto.familia;
                modelo_nuevo_producto.subfamilia = nuevo_producto.subfamilia;
                if (nuevo == 1)
                {
                    modelo_nuevo_producto.estatus = "Nuevo";
                }
                else
                {
                    modelo_nuevo_producto.estatus = "Actualizacion";
                }
                ListaModeloNuevosPreoductos.Add(modelo_nuevo_producto);
                //AUMENTAR REVISADOS 1
                Agregado++;
                //CALCULAR PORCENTAJE
                int porc = Agregado * 100 / totalContarRegistros;
                //ENVIAR AVANCE ACTUAL
                msg = "Cargando Registros En DB...";
                Clients.Caller.sendMessage(string.Format
                        (msg + " {0}% de {1}%", porc, count));

            }
        }


        public string crearStringAvanceProceso(int avance, int deTotal, string nombreSubproceso)
        {
            return " (" + avance + "/" + deTotal + ") " + nombreSubproceso;
        }
    }
}