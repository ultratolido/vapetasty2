﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using Microsoft.AspNet.SignalR;
using POS.Models;
using System.Data;
using System.Configuration;
using MySql.Data.MySqlClient;
using System.IO;

namespace POS.Hubs
{
    public class HubPrecioExcel : Hub
    {
        public string msg = "Iniciando...";
        public int count = 100;

        public void AplicarCambiosExcel()
        {
            //VARIABLES
            // actualizar_idTipoPrecio : 0     - NO ACTUALIZAR
            // actualizar_descripcion  : FALSE - NO ACTUALIZAR


            //SUB-PROCESOS
            //1.- CREAR SCRIPT
            //2.- CORRER SCRIPT


            try
            {
                //****************************
                //INICIALIZANDO
                //****************************                
                object[] sendVariable = new object[2];
                sendVariable = new object[3];
                sendVariable[0] = " Inicializando Actualizar Precio...";
                sendVariable[1] = 0;
                sendVariable[2] = "Registros procesados: (0/0)";// string.Format(" ({0}/{1})", subProcesoActual, totalSubProcesos);
                Clients.Caller.sendMessage(sendVariable);

                //****************************
                //GENERANDO QUERY SQL
                //****************************                

                sendVariable = new object[3];
                sendVariable[0] = " (1/2) Generando Query Actualizar Precio...";
                sendVariable[1] = 0;
                sendVariable[2] = "Registros procesados: (0/0)";
                Clients.Caller.sendMessage(sendVariable);



                moragas_pruebasEntities1 db = new moragas_pruebasEntities1();
                int idUsuario = Fn.usuario_logeado.id();
                List<precioactualizacionexcel> lista_productos = db.precioactualizacionexcel.Where(x => !x.copiado && x.idUsuarioActualiza == idUsuario).ToList();
                //List<producto> ListaProductosRegistrados = db.producto.ToList();

                bool success = false;
                string _msg = "";

                //******************
                //DB CONEXION*********
                //******************
                //MySqlConnection dbConn = new MySqlConnection(POS.Properties.Resources.ConnectionStringDB);
                string path = System.Web.HttpContext.Current.Server.MapPath("").Split('\\').ElementAt(System.Web.HttpContext.Current.Server.MapPath("").Split('\\').Length - 2);
                MySqlConnection dbConn = new MySqlConnection(Fn.ConnDB.getConnString(path));

                //******************
                //PRODUCTOS*********
                //******************

                // PRODUCTOS -> ELIMINAR CODIGOS DUPLICADOS LISTA EXCEL

                List<precioactualizacionexcel> distinctProductos = lista_productos
                                 .GroupBy(x => new { x.Producto_id, x.sucursal_id, x.tipoPrecio_id })
                                 .Select(x => x.First())
                                 .ToList();

                //List<precioactualizacionexcel> distinctProductos = lista_productos
                //          .GroupBy(x => x.Codigo)
                //          .Select(x => x.First())
                //          .ToList();

                String query_productos = "";
                int actualizado = 0;
                int totales = distinctProductos.Count();
                int contar100 = 0;
                foreach (precioactualizacionexcel p in distinctProductos)
                {
                    //p.descripcion = p.descripcion.Replace("'", "\\'");
                    //p.descripcion = p.descripcion.Replace('"', '\"');
                    //SI EL PRECIO ES DIFERENTE A NULL
                    if (p.precioNuevo != "")
                    {
                        if (p.Producto_id.Trim() != "" && !p.nuevo)
                        {
                            query_productos +=
                                //ACTUALIZA SI EXISTE
                                                      "UPDATE precio_por_tipocliente SET "
                                                   + " Precio='" + p.precioNuevo + "'"
                                                   + " WHERE TipoCliente_id= " + p.tipoPrecio_id
                                                   + " AND Producto_id= " + p.Producto_id
                                                   + " AND sucursal_id= " + p.sucursal_id + ";"
                                //INSERTA SI NO EXISTE
                            + " INSERT INTO precio_por_tipocliente(TipoCliente_id,Precio,Producto_id,sucursal_id)"
                             + " SELECT * FROM ( SELECT "
                              + "'" + p.tipoPrecio_id + "' as _tipoPrecio_id,"
                              + "'" + p.precioNuevo + "'  as _precio,"
                              + " (SELECT id FROM producto WHERE codigo = '" + p.Codigo + "')  as _producto_id,"
                              + p.sucursal_id + "  as _sucursal_id "
                             + ") AS tmp"
                           + " WHERE NOT EXISTS ("
                           + "    SELECT id FROM precio_por_tipocliente WHERE TipoCliente_id= " + p.tipoPrecio_id
                           + " AND Producto_id=" + p.Producto_id
                           + " AND sucursal_id= " + p.sucursal_id + ");"
                                //ACTUALIZA STATUS COPIADO DE NUEVO PRODUCTO
                           + " UPDATE precioactualizacionexcel set copiado=1 where id='" + p.id + "';";
                        }


                    }

                    //calcular porcentaje
                    actualizado++;
                    int porcentaje = ((actualizado * 50) / totales) + 50;
                    sendVariable = new object[3];
                    sendVariable[0] = " (2/2) Aplicando Cambios...";
                    sendVariable[1] = porcentaje;
                    sendVariable[2] = "Registros procesados: (" + actualizado + "/" + totales + ")";
                    Clients.Caller.sendMessage(sendVariable);

                    //EJECUTAR CONSULTA CADA 100 REGISTROS
                    if (contar100 == 100)
                    {
                        if (query_productos != "")
                        {
                            using (MySqlCommand com = new MySqlCommand(query_productos, dbConn))
                            {
                                dbConn.Open();
                                com.ExecuteNonQuery();
                                dbConn.Close();
                            }
                        }
                        //REINICIAR QUERY
                        query_productos = "";
                        //reiniciar contador
                        contar100 = 0;
                    }
                    //AUMENTAR CONTADOR 1
                    contar100++;
                }
                //SI QUEDAN PENDIENTES EJECUTALOS
                if (query_productos != "")
                {
                    using (MySqlCommand com = new MySqlCommand(query_productos, dbConn))
                    {
                        dbConn.Open();
                        com.ExecuteNonQuery();
                        dbConn.Close();
                        //calcular porcentaje
                        sendVariable = new object[3];
                        sendVariable[0] = " (2/2) Aplicando Cambios...";
                        sendVariable[1] = 100;
                        sendVariable[2] = "Registros procesados: (" + totales + "/" + totales + ")";
                        Clients.Caller.sendMessage(sendVariable);

                    }
                }
                Clients.Caller.UpdateProductos_OK();
            }
            catch (Exception e)
            {
                object[] sendVariable = new object[2];
                sendVariable[0] = "Error:" + e.Message;
                sendVariable[1] = 0;
                Clients.Caller.sendMessage(sendVariable);
                Clients.Caller.error(e.Message.ToString());
            }
        }



        public void AplicarCambiosExcel(string idTipoPrecio, string idSucursal)
        {
            int _idTipoPrecio = idTipoPrecio != "" ? Convert.ToInt16(idTipoPrecio) : 0;
            int _idSucursal = idSucursal != "" ? Convert.ToInt16(idSucursal) : 0;

            //VARIABLES
            // actualizar_idTipoPrecio : 0     - NO ACTUALIZAR
            // actualizar_descripcion  : FALSE - NO ACTUALIZAR

            configuracion_general c = Fn.Configuracion.TraerConfiguracionGeneral();
            if (c.centralizacion_precios_sucursales.Value && c.centralizacion_precios_web.Value)
            {
                //----------------------------------------------------------------------- 

                //               PRECIOS CENTRALIZADOS  (SUCURSALES Y WEB)

                //-----------------------------------------------------------------------
                AplicarCambiosExcel_PRECIOS_TODOS(_idTipoPrecio);

            }
            else if (!c.centralizacion_precios_sucursales.Value || c.centralizacion_precios_web.Value)
            {
                //si sucursal es web
                if (_idSucursal == 1)
                {
                    AplicarCambiosExcel_PRECIOS_UNA_SUCURSAL(_idTipoPrecio, 1);
                }
                //actualiza todas las sucursales menos web
                else
                {
                    AplicarCambiosExcel_PRECIOS_TODOS_MENOS_WEB(_idTipoPrecio);
                }
            }
            else if (!c.centralizacion_precios_sucursales.Value || !c.centralizacion_precios_web.Value)
            {
                //----------------------------------------------------------------------- 

                //               PRECIOS SUCURSALES CENTRALIZADOS
                //                       Y WEB DESCENTRALIZADO
                //                                 Y 
                //               PRECIOS DESCENTRALIZADO SUCURSALES Y WEB

                //-----------------------------------------------------------------------

                //SUB-PROCESOS

                if (_idSucursal != 0)
                {
                    AplicarCambiosExcel_PRECIOS_UNA_SUCURSAL(_idTipoPrecio, _idSucursal);
                }
                else
                {
                    object[] sendVariable = new object[2];
                    sendVariable[0] = "ERROR: NO SE HA SELECECCIONADO NINGUNA SUCURSAL";
                    sendVariable[1] = 0;
                    Clients.Caller.sendMessage(sendVariable);
                }
            }
            else
            {
                object[] sendVariable = new object[2];
                sendVariable[0] = "ERROR: NO SE PUDO ESTABLECER SI SU CONFIGURACIÓN ES CENTRALIZADA O DESCENTRALIZADA";
                sendVariable[1] = 0;
                Clients.Caller.sendMessage(sendVariable);
            }

        }


        public void AplicarCambiosExcel_PRECIOS_UNA_SUCURSAL(int idTipoPrecio, int idSucursal)
        {
            //VARIABLES
            // actualizar_idTipoPrecio : 0     - NO ACTUALIZAR
            // actualizar_descripcion  : FALSE - NO ACTUALIZAR


            //SUB-PROCESOS
            //1.- CREAR SCRIPT
            //2.- CORRER SCRIPT


            try
            {
                //****************************
                //INICIALIZANDO
                //****************************                
                object[] sendVariable = new object[2];
                sendVariable = new object[3];
                sendVariable[0] = " Inicializando Actualizar Precio...";
                sendVariable[1] = 0;
                sendVariable[2] = "Registros procesados: (0/0)";// string.Format(" ({0}/{1})", subProcesoActual, totalSubProcesos);
                Clients.Caller.sendMessage(sendVariable);

                //****************************
                //GENERANDO QUERY SQL
                //****************************                

                sendVariable = new object[3];
                sendVariable[0] = " (1/2) Generando Query Actualizar Precio...";
                sendVariable[1] = 0;
                sendVariable[2] = "Registros procesados: (0/0)";
                Clients.Caller.sendMessage(sendVariable);



                moragas_pruebasEntities1 db = new moragas_pruebasEntities1();
                List<precioactualizacionexcel> lista_productos = db.precioactualizacionexcel.Where(x => !x.copiado && x.idUsuarioActualiza == Fn.usuario_logeado.id()).ToList();
                //List<producto> ListaProductosRegistrados = db.producto.ToList();

                bool success = false;
                string _msg = "";

                //******************
                //DB CONEXION*********
                //******************
                //MySqlConnection dbConn = new MySqlConnection(POS.Properties.Resources.ConnectionStringDB);
                string path = System.Web.HttpContext.Current.Server.MapPath("").Split('\\').ElementAt(System.Web.HttpContext.Current.Server.MapPath("").Split('\\').Length - 2);
                MySqlConnection dbConn = new MySqlConnection(Fn.ConnDB.getConnString(path));

                //******************
                //PRODUCTOS*********
                //******************

                // PRODUCTOS -> ELIMINAR CODIGOS DUPLICADOS LISTA EXCEL
                List<precioactualizacionexcel> distinctProductos = lista_productos
                          .GroupBy(x => x.Codigo)
                          .Select(x => x.First())
                          .ToList();

                String query_productos = "";
                int actualizado = 0;
                int totales = distinctProductos.Count();
                int contar100 = 0;
                foreach (precioactualizacionexcel p in distinctProductos)
                {
                    //p.descripcion = p.descripcion.Replace("'", "\\'");
                    //p.descripcion = p.descripcion.Replace('"', '\"');
                    //SI EL PRECIO ES DIFERENTE A NULL
                    if (p.precioNuevo != "")
                    {
                        if (p.Producto_id.Trim() != "" && !p.nuevo)
                        {
                            query_productos +=
                                //ACTUALIZA SI EXISTE
                               "UPDATE precio_por_tipocliente SET "
                            + " Precio='" + p.precioNuevo + "'"
                            + " WHERE TipoCliente_id= " + idTipoPrecio
                            + " AND Producto_id= (SELECT id FROM producto WHERE codigo = '" + p.Codigo + "')"
                            + " AND sucursal_id= " + idSucursal + ";"
                                //INSERTA SI NO EXISTE
                            + " INSERT INTO precio_por_tipocliente(TipoCliente_id,Precio,Producto_id,sucursal_id)"
                           + " SELECT * FROM ( SELECT "
                               + "'" + idTipoPrecio + "' as _tipoPrecio_id,"
                               + "'" + p.precioNuevo + "'  as _precio,"
                               + " (SELECT id FROM producto WHERE codigo = '" + p.Codigo + "')  as _producto_id,"
                               + idSucursal + "  as _sucursal_id "
                              + ") AS tmp"
                            + " WHERE NOT EXISTS ("
                            + "    SELECT id FROM precio_por_tipocliente WHERE TipoCliente_id= " + idTipoPrecio
                            + " AND Producto_id= (SELECT id FROM producto WHERE codigo = '" + p.Codigo + "')"
                            + " AND sucursal_id= " + idSucursal
                            + ") LIMIT 1;"
                                //ACTUALIZA STATUS COPIADO DE NUEVO PRODUCTO
                         + " UPDATE precioactualizacionexcel set copiado=1,fecha=NOW()  where id='" + p.id + "';";
                        }
                    }

                    //calcular porcentaje
                    actualizado++;
                    int porcentaje = ((actualizado * 50) / totales) + 50;
                    sendVariable = new object[3];
                    sendVariable[0] = " (2/2) Aplicando Cambios...";
                    sendVariable[1] = porcentaje;
                    sendVariable[2] = "Registros procesados: (" + actualizado + "/" + totales + ")";
                    Clients.Caller.sendMessage(sendVariable);

                    //EJECUTAR CONSULTA CADA 100 REGISTROS
                    if (contar100 == 100)
                    {
                        if (query_productos != "")
                        {
                            using (MySqlCommand com = new MySqlCommand(query_productos, dbConn))
                            {
                                dbConn.Open();
                                com.ExecuteNonQuery();
                                dbConn.Close();
                            }
                        }
                        //REINICIAR QUERY
                        query_productos = "";
                        //reiniciar contador
                        contar100 = 0;
                    }
                    //AUMENTAR CONTADOR 1
                    contar100++;
                }
                //SI QUEDAN PENDIENTES EJECUTALOS
                if (query_productos != "")
                {
                    using (MySqlCommand com = new MySqlCommand(query_productos, dbConn))
                    {
                        dbConn.Open();
                        com.ExecuteNonQuery();
                        dbConn.Close();
                        //calcular porcentaje
                        sendVariable = new object[3];
                        sendVariable[0] = " (2/2) Aplicando Cambios...";
                        sendVariable[1] = 100;
                        sendVariable[2] = "Registros procesados: (" + totales + "/" + totales + ")";
                        Clients.Caller.sendMessage(sendVariable);

                    }
                }
                Clients.Caller.UpdateProductos_OK();
            }
            catch (Exception e)
            {
                object[] sendVariable = new object[2];
                sendVariable[0] = "Error:" + e.Message;
                sendVariable[1] = 0;
                Clients.Caller.sendMessage(sendVariable);
                Clients.Caller.error(e.Message.ToString());
            }
        }




        public void AplicarCambiosExcel_PRECIOS_TODOS(int idTipoPrecio)
        {
            //VARIABLES
            // actualizar_idTipoPrecio : 0     - NO ACTUALIZAR
            // actualizar_descripcion  : FALSE - NO ACTUALIZAR


            //SUB-PROCESOS
            //1.- CREAR SCRIPT
            //2.- CORRER SCRIPT


            try
            {
                //****************************
                //INICIALIZANDO
                //****************************                
                object[] sendVariable = new object[2];
                sendVariable = new object[3];
                sendVariable[0] = " Inicializando Actualizar Precio...";
                sendVariable[1] = 0;
                sendVariable[2] = "Registros procesados: (0/0)";// string.Format(" ({0}/{1})", subProcesoActual, totalSubProcesos);
                Clients.Caller.sendMessage(sendVariable);

                //****************************
                //GENERANDO QUERY SQL
                //****************************                

                sendVariable = new object[3];
                sendVariable[0] = " (1/2) Generando Query Actualizar Precio...";
                sendVariable[1] = 0;
                sendVariable[2] = "Registros procesados: (0/0)";
                Clients.Caller.sendMessage(sendVariable);


                moragas_pruebasEntities1 db = new moragas_pruebasEntities1();
                int idUsuarioLoggeado = Fn.usuario_logeado.id();
                List<precioactualizacionexcel> lista_productos = db.precioactualizacionexcel.Where(x => !x.copiado && x.idUsuarioActualiza == idUsuarioLoggeado).ToList();
                //List<producto> ListaProductosRegistrados = db.producto.ToList();

                bool success = false;
                string _msg = "";

                //******************
                //DB CONEXION*********
                //******************
                //MySqlConnection dbConn = new MySqlConnection(POS.Properties.Resources.ConnectionStringDB);
                string path = System.Web.HttpContext.Current.Server.MapPath("").Split('\\').ElementAt(System.Web.HttpContext.Current.Server.MapPath("").Split('\\').Length - 2);
                MySqlConnection dbConn = new MySqlConnection(Fn.ConnDB.getConnString(path));

                //******************
                //PRODUCTOS*********
                //******************

                // PRODUCTOS -> ELIMINAR CODIGOS DUPLICADOS Y NUEVOS EN LISTA EXCEL
                List<precioactualizacionexcel> distinctProductos = lista_productos
                          .GroupBy(x => x.Codigo)
                          .Select(x => x.First())
                          .Where(x => !x.nuevo)
                          .ToList();

                String query_productos = "";
                int actualizado = 0;
                List<sucursal> sucursales = db.sucursal.ToList();
                int totales = distinctProductos.Count();// *sucursales.Count();
                int contar100 = 0;
                foreach (precioactualizacionexcel p in distinctProductos)
                {
                    //p.descripcion = p.descripcion.Replace("'", "\\'");
                    //p.descripcion = p.descripcion.Replace('"', '\"');
                    foreach (var s in sucursales)
                    {
                        //SI EL PRECIO ES DIFERENTE A NULL
                        if (p.precioNuevo != "")
                        {
                            if (p.Producto_id.Trim() != "" && !p.nuevo)
                            {
                                query_productos +=
                                    //ACTUALIZA SI EXISTE
                                   "UPDATE precio_por_tipocliente SET "
                                + " Precio=" + p.precioNuevo
                                + " WHERE TipoCliente_id= " + idTipoPrecio
                                + " AND Producto_id= (SELECT id FROM producto WHERE codigo = '" + p.Codigo + "')"
                                + " AND sucursal_id= " + s.id + ";"
                                    //INSERTA SI NO EXISTE
                                + " INSERT INTO precio_por_tipocliente(TipoCliente_id,Precio,Producto_id,sucursal_id)"
                               + " SELECT * FROM ( SELECT "
                                   + "'" + idTipoPrecio + "' as _tipoPrecio_id,"
                                   + "'" + p.precioNuevo + "'  as _precio,"
                                   + " (SELECT id FROM producto WHERE codigo = '" + p.Codigo + "')  as _producto_id,"
                                   + s.id + " as _sucursal_id "
                                  + ") AS tmp"
                                + " WHERE NOT EXISTS ("
                                + "    SELECT id FROM precio_por_tipocliente WHERE TipoCliente_id= " + idTipoPrecio
                                + " AND Producto_id= (SELECT id FROM producto WHERE codigo = '" + p.Codigo + "')"
                                + " AND sucursal_id= " + s.id
                                + ") LIMIT 1;"
                                    //ACTUALIZA STATUS COPIADO DE NUEVO PRODUCTO
                             + " UPDATE precioactualizacionexcel set copiado=1,fecha=NOW() where codigo='" + p.Codigo + "' AND idUsuarioActualiza=" + idUsuarioLoggeado + "  ;";
                            }
                        }
                    }
                    //calcular porcentaje
                    actualizado++;
                    int porcentaje = ((actualizado * 50) / totales) + 50;
                    sendVariable = new object[3];
                    sendVariable[0] = " (2/2) Aplicando Cambios...";
                    sendVariable[1] = porcentaje;
                    sendVariable[2] = "Registros procesados: (" + actualizado + "/" + totales + ")";
                    Clients.Caller.sendMessage(sendVariable);

                    //EJECUTAR CONSULTA CADA 100 REGISTROS
                    if (contar100 == 100)
                    {
                        if (query_productos != "")
                        {
                            using (MySqlCommand com = new MySqlCommand(query_productos, dbConn))
                            {
                                dbConn.Open();
                                com.ExecuteNonQuery();
                                dbConn.Close();
                            }
                        }
                        //REINICIAR QUERY
                        query_productos = "";
                        //reiniciar contador
                        contar100 = 0;
                    }
                    //AUMENTAR CONTADOR 1
                    contar100++;
                }
                //SI QUEDAN PENDIENTES EJECUTALOS
                if (query_productos != "")
                {
                    using (MySqlCommand com = new MySqlCommand(query_productos, dbConn))
                    {
                        dbConn.Open();
                        com.ExecuteNonQuery();
                        dbConn.Close();
                        //calcular porcentaje
                        sendVariable = new object[3];
                        sendVariable[0] = " (2/2) Aplicando Cambios...";
                        sendVariable[1] = 100;
                        sendVariable[2] = "Registros procesados: (" + totales + "/" + totales + ")";
                        Clients.Caller.sendMessage(sendVariable);

                    }
                }
                Clients.Caller.UpdateProductos_OK();
            }
            catch (Exception e)
            {
                object[] sendVariable = new object[2];
                sendVariable[0] = "Error:" + e.Message;
                sendVariable[1] = 0;
                Clients.Caller.sendMessage(sendVariable);
                Clients.Caller.error(e.Message.ToString());
            }
        }


        public void AplicarCambiosExcel_PRECIOS_TODOS_MENOS_WEB(int idTipoPrecio)
        {
            //VARIABLES
            // actualizar_idTipoPrecio : 0     - NO ACTUALIZAR
            // actualizar_descripcion  : FALSE - NO ACTUALIZAR


            //SUB-PROCESOS
            //1.- CREAR SCRIPT
            //2.- CORRER SCRIPT


            try
            {
                //****************************
                //INICIALIZANDO
                //****************************                
                object[] sendVariable = new object[2];
                sendVariable = new object[3];
                sendVariable[0] = " Inicializando Actualizar Precio...";
                sendVariable[1] = 0;
                sendVariable[2] = "Registros procesados: (0/0)";// string.Format(" ({0}/{1})", subProcesoActual, totalSubProcesos);
                Clients.Caller.sendMessage(sendVariable);

                //****************************
                //GENERANDO QUERY SQL
                //****************************                

                sendVariable = new object[3];
                sendVariable[0] = " (1/2) Generando Query Actualizar Precio...";
                sendVariable[1] = 0;
                sendVariable[2] = "Registros procesados: (0/0)";
                Clients.Caller.sendMessage(sendVariable);


                moragas_pruebasEntities1 db = new moragas_pruebasEntities1();
                int idUsuarioLoggeado = Fn.usuario_logeado.id();
                List<precioactualizacionexcel> lista_productos = db.precioactualizacionexcel.Where(x => !x.copiado && x.idUsuarioActualiza == idUsuarioLoggeado).ToList();
                //List<producto> ListaProductosRegistrados = db.producto.ToList();

                bool success = false;
                string _msg = "";

                //******************
                //DB CONEXION*********
                //******************
                //MySqlConnection dbConn = new MySqlConnection(POS.Properties.Resources.ConnectionStringDB);
                string path = System.Web.HttpContext.Current.Server.MapPath("").Split('\\').ElementAt(System.Web.HttpContext.Current.Server.MapPath("").Split('\\').Length - 2);
                MySqlConnection dbConn = new MySqlConnection(Fn.ConnDB.getConnString(path));

                //******************
                //PRODUCTOS*********
                //******************

                // PRODUCTOS -> ELIMINAR CODIGOS DUPLICADOS Y NUEVOS EN LISTA EXCEL
                List<precioactualizacionexcel> distinctProductos = lista_productos
                          .GroupBy(x => x.Codigo)
                          .Select(x => x.First())
                          .Where(x => !x.nuevo)
                          .ToList();

                String query_productos = "";
                int actualizado = 0;
                List<sucursal> sucursales = db.sucursal.ToList();
                int totales = distinctProductos.Count();// *sucursales.Count();
                int contar100 = 0;
                foreach (precioactualizacionexcel p in distinctProductos)
                {
                    //p.descripcion = p.descripcion.Replace("'", "\\'");
                    //p.descripcion = p.descripcion.Replace('"', '\"');
                    //POR CADA SUCURSAL MENOS WEB
                    foreach (var s in sucursales.Where(x => x.id != 1))
                    {
                        //SI EL PRECIO ES DIFERENTE A NULL
                        if (p.precioNuevo != "")
                        {
                            if (p.Producto_id.Trim() != "" && !p.nuevo)
                            {
                                query_productos +=
                                    //ACTUALIZA SI EXISTE
                                   "UPDATE precio_por_tipocliente SET "
                                + " Precio=" + p.precioNuevo
                                + " WHERE TipoCliente_id= " + idTipoPrecio
                                + " AND Producto_id= (SELECT id FROM producto WHERE codigo = '" + p.Codigo + "')"
                                + " AND sucursal_id= " + s.id + ";"
                                    //INSERTA SI NO EXISTE
                                + " INSERT INTO precio_por_tipocliente(TipoCliente_id,Precio,Producto_id,sucursal_id)"
                               + " SELECT * FROM ( SELECT "
                                   + "'" + idTipoPrecio + "' as _tipoPrecio_id,"
                                   + "'" + p.precioNuevo + "'  as _precio,"
                                   + " (SELECT id FROM producto WHERE codigo = '" + p.Codigo + "')  as _producto_id,"
                                   + s.id + " as _sucursal_id "
                                  + ") AS tmp"
                                + " WHERE NOT EXISTS ("
                                + "    SELECT id FROM precio_por_tipocliente WHERE TipoCliente_id= " + idTipoPrecio
                                + " AND Producto_id= (SELECT id FROM producto WHERE codigo = '" + p.Codigo + "')"
                                + " AND sucursal_id= " + s.id
                                + ") LIMIT 1;"
                                    //ACTUALIZA STATUS COPIADO DE NUEVO PRODUCTO
                             + " UPDATE precioactualizacionexcel set copiadoPrecio=1 where codigo='" + p.Codigo + "' AND idUsuarioActualiza=" + idUsuarioLoggeado + " ;";
                            }
                        }
                    }
                    //calcular porcentaje
                    actualizado++;
                    int porcentaje = ((actualizado * 50) / totales) + 50;
                    sendVariable = new object[3];
                    sendVariable[0] = " (2/2) Aplicando Cambios...";
                    sendVariable[1] = porcentaje;
                    sendVariable[2] = "Registros procesados: (" + actualizado + "/" + totales + ")";
                    Clients.Caller.sendMessage(sendVariable);

                    //EJECUTAR CONSULTA CADA 100 REGISTROS
                    if (contar100 == 100)
                    {
                        if (query_productos != "")
                        {
                            using (MySqlCommand com = new MySqlCommand(query_productos, dbConn))
                            {
                                dbConn.Open();
                                com.ExecuteNonQuery();
                                dbConn.Close();
                            }
                        }
                        //REINICIAR QUERY
                        query_productos = "";
                        //reiniciar contador
                        contar100 = 0;
                    }
                    //AUMENTAR CONTADOR 1
                    contar100++;
                }
                //SI QUEDAN PENDIENTES EJECUTALOS
                if (query_productos != "")
                {
                    using (MySqlCommand com = new MySqlCommand(query_productos, dbConn))
                    {
                        dbConn.Open();
                        com.ExecuteNonQuery();
                        dbConn.Close();
                        //calcular porcentaje
                        sendVariable = new object[3];
                        sendVariable[0] = " (2/2) Aplicando Cambios...";
                        sendVariable[1] = 100;
                        sendVariable[2] = "Registros procesados: (" + totales + "/" + totales + ")";
                        Clients.Caller.sendMessage(sendVariable);

                    }
                }
                Clients.Caller.UpdateProductos_OK();
            }
            catch (Exception e)
            {
                object[] sendVariable = new object[2];
                sendVariable[0] = "Error:" + e.Message;
                sendVariable[1] = 0;
                Clients.Caller.sendMessage(sendVariable);
                Clients.Caller.error(e.Message.ToString());
            }
        }

        public void Producto_CargarArchivoProductos_DB(string fileName)
        {
            //VARIABLES

            //SUB-PROCESOS
            //1.- CARGAR EXCEL
            //2.- TRUNCAR TABLA NUEVO PRODUCTO
            //3.- CARGAR REGISTROS A TABLA NUEVO PRODUCTO
            //4.- ANALIZAR REGISTROS


            int totalSubProcesos = 4;
            int subProcesoActual = 0;

            decimal suma_porcentaje_total = 0;
            decimal porcentaje_por_subproceso = 100 / totalSubProcesos;

            bool existearchivo = false;

            try
            {
                //****************************
                //INICIALIZANDO
                //****************************

                subProcesoActual = 0;
                object[] sendVariable = new object[2];
                sendVariable = new object[3];
                sendVariable[0] = " (" + subProcesoActual + "/" + totalSubProcesos + ")" + " Inicializando...";
                sendVariable[1] = 0;
                sendVariable[2] = "Registros procesados: (0/0)";// string.Format(" ({0}/{1})", subProcesoActual, totalSubProcesos);
                Clients.Caller.sendMessage(sendVariable);

                string filePath = AppDomain.CurrentDomain.BaseDirectory + "/ArchivosExcel/" + fileName;


                if (File.Exists(filePath))
                { existearchivo = true; }
                var connectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1\"", filePath);
                System.Data.OleDb.OleDbConnection connEXCEL = new System.Data.OleDb.OleDbConnection();
                connEXCEL.ConnectionString = connectionString;
                connEXCEL.Open();

                System.Data.DataTable dbSchema = connEXCEL.GetOleDbSchemaTable(System.Data.OleDb.OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
                string Sheet1 = dbSchema.Rows[0].Field<string>("TABLE_NAME");
                var adapter = new System.Data.OleDb.OleDbDataAdapter("SELECT * FROM [" + Sheet1 + "]", connectionString);
                var ds = new DataSet();
                adapter.Fill(ds, "results");
                System.Data.DataTable data = ds.Tables["results"];



                //PORCENAJE 0% - 25%
                suma_porcentaje_total = +porcentaje_por_subproceso;
                subProcesoActual = 1;
                sendVariable = new object[3];
                sendVariable[0] = " (" + subProcesoActual + "/" + totalSubProcesos + ")" + " Cargando Archivo Excel...";
                sendVariable[1] = suma_porcentaje_total;
                sendVariable[2] = "Registros procesados: (0/0)";
                Clients.Caller.sendMessage(sendVariable);

                //CERRAR CONEXION ARCHIVO EXCEL
                connEXCEL.Close();
                //SI EXISTE ELIMINALO
                if (File.Exists(filePath))
                {
                    File.Delete(filePath);
                }

                //****************************
                //SUBPROCESO REINICIAR TABLA NUEVO PRODUCTO
                //****************************


                string path = System.Web.HttpContext.Current.Server.MapPath("").Split('\\').ElementAt(System.Web.HttpContext.Current.Server.MapPath("").Split('\\').Length - 2);
                MySqlConnection dbConn = new MySqlConnection(Fn.ConnDB.getConnString(path));
                int idusuarioLoggeado = Fn.usuario_logeado.id();
                string queryTruncateNuevoProducto = "DELETE FROM precioactualizacionexcel where idUsuarioActualiza=" + idusuarioLoggeado + ";";
                dbConn.Open();
                MySqlCommand cmd = new MySqlCommand(queryTruncateNuevoProducto, dbConn);
                cmd.ExecuteNonQuery();
                dbConn.Close();

                //ENVIAR AVANCE ACTUAL -> TABLA TRUNCADA CORRECTAMENTE
                //PORCENAJE 25% - 50%
                suma_porcentaje_total += porcentaje_por_subproceso;
                subProcesoActual = 2;
                sendVariable = new object[3];
                sendVariable[0] = " (" + subProcesoActual + "/" + totalSubProcesos + ")" + " Reiniciando DB...";
                sendVariable[1] = suma_porcentaje_total;
                sendVariable[2] = sendVariable[2] = "Registros procesados: (0/0)";
                Clients.Caller.sendMessage(sendVariable);



                //***********************************************
                //SUBPROCESO AGREGAR REGISTROS A LA BASE DE DATOS
                //***********************************************


                int Agregado = 0;
                int Contar100 = 0;
                string queryAgregarNuevoProducto = "";
                int contarRegistrosProcesados = 0;
                moragas_pruebasEntities1 db = new moragas_pruebasEntities1();
                List<string> sucursalNoEncontrada = new List<string>();
                List<string> codigoProdCantidadNoDecimal = new List<string>();
                List<string> tipoPrecioNoexiste = new List<string>();
                List<sucursal> LSucursales = db.sucursal.ToList();
                List<tipocliente> LTipoPrecios = db.tipocliente.ToList();
                List<string> LPreciosDuplicados = new List<string>();
                List<sucursal> LSucursalesUsuario = Fn.usuario_logeado.traerUsuario().sucursales_por_usuario.Select(x => x.sucursal).ToList();

                //REVISAR LAS CABECERAS QUE SEAN TIPO DE PRECIO (CONTENGAN 'PRECIO:[nombre_tipo_de_precio]')
                List<tipoPrecioAux> auxTipoPrecioColumna = new List<tipoPrecioAux>();
                int colIndex = 0;
                foreach (System.Data.DataColumn columna in data.Columns)
                {
                    string nombreCompleto = columna.ColumnName;
                    //revisar si contiene TP:
                    if (nombreCompleto.ToUpper().Contains("PRECIO:"))
                    {
                        if (nombreCompleto.ToUpper().Contains('(') || nombreCompleto.ToUpper().Split('(')[1].Contains(')'))
                        {
                            //existe tipo precio
                            string nombre = nombreCompleto.Split('(')[1].Split(')')[0].ToUpper().ToString();
                            if (LTipoPrecios.Where(x => x.Tipo.ToUpper().Trim() == nombre.Trim()).Any())
                            {
                                tipoPrecioAux tpc = new tipoPrecioAux();
                                tpc.index = colIndex;
                                tipocliente tp = db.tipocliente.Where(x => x.Tipo.ToUpper().Trim() == nombre.Trim()).First();
                                tpc.tipoPrecio = tp.Tipo;
                                tpc.tipoprecio_id = tp.id.ToString();
                                auxTipoPrecioColumna.Add(tpc);
                            }
                            else
                            {
                                tipoPrecioNoexiste.Add(nombre);
                            }
                        }
                    }
                    colIndex++;

                }

                foreach (var tp in auxTipoPrecioColumna)
                {
                    if (auxTipoPrecioColumna.Where(x => x.tipoprecio_id == tp.tipoprecio_id).Count() > 1)
                    {
                        LPreciosDuplicados.Add(tp.tipoPrecio);
                    }
                }


                //CONTAR REGISTROS EXCEL
                int totalContarRenglonesExcel = data.Rows.Count;// * auxTipoPrecioColumna.Count();
                int totalRow = totalContarRenglonesExcel;// data.Rows.Count;
                decimal porcentajeRenglon = totalRow != 0 ? porcentaje_por_subproceso / totalRow : porcentaje_por_subproceso;
                decimal CuentaMAX = Math.Round(Convert.ToDecimal(100 / (auxTipoPrecioColumna.Count() * LSucursales.Count())), 0);
                int RenglonesExcelProcesados = 0;
                int RenglonesExcelProcesadosTOTAL = 0;
                int RegistrosSubidos = 0;
                //TRAER CONFIGURACIÓN
                configuracion_general conf = Fn.Configuracion.TraerConfiguracionGeneral();

                //EXISTE AL MENOS UN TIPO DE PRECIO VÁLIDO
                if (auxTipoPrecioColumna.Count() != 0)
                {
                    //NO EXISTEN TIPO DE PRECIO DUPLICADOS
                    if (LPreciosDuplicados.Count() == 0)
                    {
                        //todos los precios existen
                        if (tipoPrecioNoexiste.Count() == 0)
                        {
                            foreach (System.Data.DataRow row in data.Rows)
                            {
                                suma_porcentaje_total += porcentajeRenglon;
                                var values = row.ItemArray;
                                precioactualizacionexcel ex = new precioactualizacionexcel();
                                ex.Codigo = Convert.ToString(values[0]).Trim().Replace('"', '\"').Replace("'", "\'").ToUpper();
                                if (ex.Codigo != "")
                                {
                                    //ex.Precio = Convert.ToDecimal(Convert.ToString(values[1]).Trim().Replace('"', '\"').Replace("'", "\'").ToUpper());
                                    ex.sucursal = Convert.ToString(values[1]).Replace('"', '\"').Replace("'", "\'").TrimEnd().TrimStart().ToUpper();

                                    //SI SUCURSAL EXISTE ENTONCES GUARDA EL ID
                                    //DE LO CONTRARIO NO GUARDES ID


                                    //--------------------------------------
                                    //--------------------------------------
                                    // DOBLE ASTERISCO, TODAS LAS SUCURSALES
                                    //--------------------------------------
                                    if (ex.sucursal.ToUpper().Trim() == "**")
                                    {
                                        //REVISAR SI LA CONFIGURACIÓN TIENE HOMOLOGACIÓN DE PRECIOS SUCURSALES Y WEB
                                        if (conf.centralizacion_precios_sucursales.Value && conf.centralizacion_precios_web.Value)
                                        {
                                            //totalContarRegistros = totalContarRegistros * LSucursales.Count();
                                            foreach (sucursal s in LSucursales)
                                            {
                                                //REVISAR SI EL USUARIO TIENE PERMISO PARA ESTA SUCURSAL
                                                if (LSucursalesUsuario.Where(x => x.id == s.id).Any())
                                                {
                                                    //EL USUARIO SI TIENE PERMISO PARA LA SUCURSAL
                                                    foreach (tipoPrecioAux tp in auxTipoPrecioColumna)
                                                    {
                                                        //totalContarRegistros++;
                                                        //suma_porcentaje_total += porcentaje_por_registro;
                                                        ex.tipoPrecio = tp.tipoPrecio;
                                                        ex.tipoPrecio_id = Convert.ToInt16(tp.tipoprecio_id);

                                                        //SI CANTIDAD ES NUMERICO ENTONCES GUARDA LA CONVERSION DE ESA CADENA EN CANTIDAD_DECIMAL
                                                        //DE LO CONTRARIO GUARDA CANTIDAD VACIO Y GUARDA CantidadesDecimalOK = FALSE
                                                        Decimal cantInt = 0;

                                                        ex.precioNuevo = Convert.ToString(values[tp.index]).Replace('"', '\"').Replace("'", "\'").TrimEnd().TrimStart().ToUpper();
                                                        if (ex.precioNuevo != "") { }
                                                        if (!Decimal.TryParse(ex.precioNuevo, out cantInt))
                                                            codigoProdCantidadNoDecimal.Add(ex.Codigo);



                                                        queryAgregarNuevoProducto +=
                                                            //VARIABLES
                                                                @" SET @Producto_id:=IF((select count(*) from producto where TRIM(codigo)='" + ex.Codigo.Replace("'", "\\'") + "')>=1, (SELECT id from PRODUCTO WHERE TRIM(codigo)='" + ex.Codigo.Replace("'", "\\'") + "') , '');"
                                                                 + " SET @Sucursal:='" + s.nombre_sucursal.ToUpper() + "';"
                                                                + " SET @Sucursal_id:='" + s.id + "';"
                                                                + " SET @TipoCliente_id:='" + ex.tipoPrecio_id + "';"
                                                            //TRAER PRECIO ACTUAL SI EXISTE, SI NO TRAE 0
                                                                + " SET @precioActual:=IF(@Producto_id!='',"
                                                                + "IF((SELECT EXISTS(SELECT * FROM precio_por_tipocliente WHERE Producto_id=@Producto_id AND Sucursal_id=@Sucursal_id AND TipoCliente_id=@TipoCliente_id))=1"
                                                                + ",(SELECT PRECIO FROM precio_por_tipocliente WHERE Producto_id=@Producto_id AND Sucursal_id=@Sucursal_id  AND TipoCliente_id=@TipoCliente_id),0),0);"
                                                                + " SET @diferencia:=" + ex.precioNuevo + "- @precioActual;"
                                                                + " SET @duplicado:=IF((select count(*) from precioactualizacionexcel where Producto_id=@Producto_id AND tipoPrecio_id=@TipoCliente_id  AND Sucursal_id=@Sucursal_id AND idUsuarioActualiza=" + idusuarioLoggeado + " )>=1,1,0);"
                                                                 + " SET @Nuevo:=IF(@Producto_id='',1,0);"
                                                            //INSERTA EN TABLA precioactualizacionexcel
                                                                + " INSERT INTO precioactualizacionexcel"
                                                            + "("
                                                            + "codigo,"
                                                            + "Sucursal,"
                                                            + "Sucursal_id,"
                                                            + "Producto_id,"
                                                            + "precioActual,"
                                                            + "precioNuevo,"
                                                            + "diferencia,"
                                                            + "tipoPrecio,"
                                                            + "tipoPrecio_id,"
                                                            + "copiado,"
                                                            + "nuevo,"
                                                            + " fecha,"
                                                            + " duplicado,"
                                                            + "idUsuarioActualiza)"
                                                            + "VALUES"
                                                            + "("
                                                            + "'" + ex.Codigo.Replace("'", "\\'") + "'" + ","
                                                            + "@Sucursal,"
                                                            + "@Sucursal_id,"
                                                            + "@Producto_id,"
                                                            + "@precioActual,"
                                                            + "'" + ex.precioNuevo + "'" + ","
                                                            + "@diferencia,"
                                                            + "'" + ex.tipoPrecio + "'" + ","
                                                            + "'" + ex.tipoPrecio_id + "'" + ","
                                                            + "0,"
                                                            + "@Nuevo,"
                                                            + "NOW(),"
                                                            + "@duplicado,"
                                                            + idusuarioLoggeado
                                                            + ");";
                                                        RegistrosSubidos++;
                                                        //Contar100++;
                                                        //if (Contar100 == 100)
                                                        //contarRegistrosProcesados++;
                                                        if (RenglonesExcelProcesados >= CuentaMAX)
                                                        {
                                                            if (queryAgregarNuevoProducto != "")
                                                            {
                                                                using (MySqlCommand com = new MySqlCommand(queryAgregarNuevoProducto, dbConn))
                                                                {
                                                                    // Agregado += 100;
                                                                    //Contar100 = 0;
                                                                    RenglonesExcelProcesados = 0;
                                                                    dbConn.Open();
                                                                    com.ExecuteNonQuery();
                                                                    dbConn.Close();
                                                                    //calcular porcentaje
                                                                    msg = "Agregando Registros A DB...";
                                                                    subProcesoActual = 3;
                                                                    sendVariable = new object[3];
                                                                    sendVariable[0] = " (" + subProcesoActual + "/" + totalSubProcesos + ") " + msg;
                                                                    sendVariable[1] = suma_porcentaje_total;
                                                                    sendVariable[2] = "Registros procesados: (" + RenglonesExcelProcesadosTOTAL + "/" + totalContarRenglonesExcel + ")";
                                                                    Clients.Caller.sendMessage(sendVariable);
                                                                }
                                                                queryAgregarNuevoProducto = "";
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    //EL USUARIO  TIENE PERMISO PARA ESTA SUCURSAL
                                                    string msgerror = "<div class=error-box alert style='background-color:#ff9c9c; padding: 15px; width: 100%;'>"
                                                         + "<div class=msg style='color: #541a1a; margin-top: 5px;'>"
                                                              + "</div> NO CUENTAS CON PERMISO PARA ACTUALIZAR LA SUCURSAL:</br><label>" + s.nombre_sucursal + ".</label></br> ";
                                                    foreach (var tp in tipoPrecioNoexiste)
                                                    {
                                                        msgerror += "</br><label style='font-weight:bold'>" + tp + "</label>";
                                                    }
                                                    msgerror += "</div>"
                                                                        + "</div>"
                                                                    + "</div>";
                                                    Clients.Caller.error(msgerror);
                                                }
                                            }

                                        }
                                        else
                                        {
                                            //ERROR, PRECIOS NO HOMOLOGADOS
                                            string msgerror = "<div class=error-box alert style='background-color:#ff9c9c; padding: 15px; width: 100%;'>"
                                                     + "<div class=msg style='color: #541a1a; margin-top: 5px;'>"
                                                          + "</div>TU PLATAFORMA NO PERMITE ACTUALIZAR TODAS LAS SUCURSALES PORQUE MANEJA PRECIOS DIFERENTES.</br> DESCARGAR EL LAYOUT Y INTENTALO DE NUEVO.</br> ";
                                            msgerror += "</div>"
                                                                + "</div>"
                                                            + "</div>";
                                            Clients.Caller.error(msgerror);
                                        }
                                    }

                                    //--------------------------------------
                                    //--------------------------------------
                                    //UN ASTERISCO TODAS LAS SUCURSALES MENOS WEB
                                    //--------------------------------------
                                    else if (ex.sucursal.ToUpper().Trim() == "*")
                                    {
                                        //REVISAR SI LA CONFIGURACIÓN TIENE HOMOLOGACIÓN DE PRECIOS SUCURSALES EXCEPTO WEB
                                        if (conf.centralizacion_precios_sucursales.Value && !conf.centralizacion_precios_web.Value)
                                        {
                                            //totalContarRegistros = totalContarRegistros * LSucursales.Where(x => x.id != 1).Count();
                                            foreach (sucursal s in LSucursales.Where(x => x.id != 1))
                                            {
                                                //REVISAR SI EL USUARIO TIENE PERMISO PARA ESTA SUCURSAL
                                                if (LSucursalesUsuario.Where(x => x.id == s.id).Any())
                                                {
                                                    //EL USUARIO SI TIENE PERMISO PARA LA SUCURSAL
                                                    foreach (tipoPrecioAux tp in auxTipoPrecioColumna)
                                                    {
                                                        //totalContarRegistros++;
                                                        //suma_porcentaje_total += porcentaje_por_registro;
                                                        ex.tipoPrecio = tp.tipoPrecio;
                                                        ex.tipoPrecio_id = Convert.ToInt16(tp.tipoprecio_id);

                                                        //SI CANTIDAD ES NUMERICO ENTONCES GUARDA LA CONVERSION DE ESA CADENA EN CANTIDAD_DECIMAL
                                                        //DE LO CONTRARIO GUARDA CANTIDAD VACIO Y GUARDA CantidadesDecimalOK = FALSE
                                                        Decimal cantInt = 0;

                                                        ex.precioNuevo = Convert.ToString(values[tp.index]).Replace('"', '\"').Replace("'", "\'").TrimEnd().TrimStart().ToUpper();
                                                        if (ex.precioNuevo != "") { }
                                                        if (!Decimal.TryParse(ex.precioNuevo, out cantInt))
                                                            codigoProdCantidadNoDecimal.Add(ex.Codigo);



                                                        queryAgregarNuevoProducto +=
                                                            //VARIABLES
                                                                @" SET @Producto_id:=IF((select count(*) from producto where TRIM(codigo)='" + ex.Codigo.Replace("'", "\\'") + "')>=1, (SELECT id from PRODUCTO WHERE TRIM(codigo)='" + ex.Codigo.Replace("'", "\\'") + "') , '');"
                                                                + " SET @Sucursal:='" + s.nombre_sucursal.ToUpper() + "';"
                                                                + " SET @Sucursal_id:='" + s.id + "';"
                                                                + " SET @TipoCliente_id:='" + ex.tipoPrecio_id + "';"
                                                            //TRAER PRECIO ACTUAL SI EXISTE, SI NO TRAE 0
                                                                + " SET @precioActual:=IF(@Producto_id!='',"
                                                                + "IF((SELECT EXISTS(SELECT * FROM precio_por_tipocliente WHERE Producto_id=@Producto_id AND Sucursal_id=@Sucursal_id AND TipoCliente_id=@TipoCliente_id))=1"
                                                                + ",(SELECT PRECIO FROM precio_por_tipocliente WHERE Producto_id=@Producto_id AND Sucursal_id=@Sucursal_id  AND TipoCliente_id=@TipoCliente_id),0),0);"
                                                                + " SET @diferencia:=" + ex.precioNuevo + "- @precioActual;"
                                                                + " SET @duplicado:=IF((select count(*) from precioactualizacionexcel where Producto_id=@Producto_id AND tipoPrecio_id=@TipoCliente_id  AND Sucursal_id=@Sucursal_id AND idUsuarioActualiza=" + idusuarioLoggeado + " )>=1,1,0);"
                                                                + " SET @Nuevo:=IF(@Producto_id='',1,0);"

                                                            //INSERTA EN TABLA precioactualizacionexcel
                                                                + " INSERT INTO precioactualizacionexcel"
                                                            + "("
                                                            + "codigo,"
                                                            + "Sucursal,"
                                                            + "Sucursal_id,"
                                                            + "Producto_id,"
                                                            + "precioActual,"
                                                            + "precioNuevo,"
                                                            + "diferencia,"
                                                            + "tipoPrecio,"
                                                            + "tipoPrecio_id,"
                                                            + "copiado,"
                                                            + "nuevo,"
                                                            + " fecha,"
                                                            + "duplicado,"
                                                            + "idUsuarioActualiza)"
                                                            + "VALUES"
                                                            + "("
                                                            + "'" + ex.Codigo.Replace("'", "\\'") + "'" + ","
                                                            + "@Sucursal,"
                                                            + "@Sucursal_id,"
                                                            + "@Producto_id,"
                                                            + "@precioActual,"
                                                            + "'" + ex.precioNuevo + "'" + ","
                                                            + "@diferencia,"
                                                            + "'" + ex.tipoPrecio + "'" + ","
                                                            + "'" + ex.tipoPrecio_id + "'" + ","
                                                            + "0,"
                                                            + "@Nuevo,"
                                                            + "NOW(),"
                                                            + "@duplicado,"
                                                            + idusuarioLoggeado
                                                            + ");";

                                                        RegistrosSubidos++;
                                                        //Contar100++;
                                                        //if (Contar100 == 100)
                                                        //contarRegistrosProcesados++;
                                                        if (RenglonesExcelProcesados >= CuentaMAX)
                                                        {
                                                            if (queryAgregarNuevoProducto != "")
                                                            {
                                                                using (MySqlCommand com = new MySqlCommand(queryAgregarNuevoProducto, dbConn))
                                                                {
                                                                    // Agregado += 100;
                                                                    //Contar100 = 0;
                                                                    RenglonesExcelProcesados = 0;
                                                                    dbConn.Open();
                                                                    com.ExecuteNonQuery();
                                                                    dbConn.Close();
                                                                    //calcular porcentaje
                                                                    msg = "Agregando Registros A DB...";
                                                                    subProcesoActual = 3;
                                                                    sendVariable = new object[3];
                                                                    sendVariable[0] = " (" + subProcesoActual + "/" + totalSubProcesos + ") " + msg;
                                                                    sendVariable[1] = suma_porcentaje_total;
                                                                    sendVariable[2] = "Registros procesados: (" + RenglonesExcelProcesadosTOTAL + "/" + totalContarRenglonesExcel + ")";
                                                                    Clients.Caller.sendMessage(sendVariable);
                                                                }
                                                                queryAgregarNuevoProducto = "";
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    //EL USUARIO  TIENE PERMISO PARA ESTA SUCURSAL
                                                    string msgerror = "<div class=error-box alert style='background-color:#ff9c9c; padding: 15px; width: 100%;'>"
                                                         + "<div class=msg style='color: #541a1a; margin-top: 5px;'>"
                                                              + "</div> NO CUENTAS CON PERMISO PARA ACTUALIZAR LA SUCURSAL:</br><label>" + s.nombre_sucursal.ToUpper() + ".</label></br> ";
                                                    foreach (var tp in tipoPrecioNoexiste)
                                                    {
                                                        msgerror += "</br><label style='font-weight:bold'>" + tp + "</label>";
                                                    }
                                                    msgerror += "</div>"
                                                                        + "</div>"
                                                                    + "</div>";
                                                    Clients.Caller.error(msgerror);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            //ERROR, PRECIOS NO HOMOLOGADOS
                                            string msgerror = "<div class=error-box alert style='background-color:#ff9c9c; padding: 15px; width: 100%;'>"
                                                     + "<div class=msg style='color: #541a1a; margin-top: 5px;'>"
                                                          + "</div>TU PLATAFORMA NO PERMITE ACTUALIZAR TODAS LAS SUCURSALES PORQUE MANEJA PRECIOS DIFERENTES.</br> DESCARGAR EL LAYOUT Y INTENTALO DE NUEVO.</br> ";
                                            msgerror += "</div>"
                                                                + "</div>"
                                                            + "</div>";
                                            Clients.Caller.error(msgerror);
                                        }
                                    }

                                    //--------------------------------------
                                    //--------------------------------------
                                    //ACTUALIZACIÓN POR NOMBRE DE SUCURSAL
                                    //--------------------------------------
                                    else
                                    {

                                        string error_actualizar_por_nombre = "";
                                        bool OK_actualiza_por_nombre = false;

                                        //totalContarRegistros++;
                                        //suma_porcentaje_total += porcentaje_por_registro;

                                        if (LSucursales.Where(x => x.nombre_sucursal.ToUpper() == ex.sucursal.ToUpper().TrimEnd().TrimStart()).Any())
                                            ex.sucursal_id = LSucursales.Where(x => x.nombre_sucursal.ToUpper() == ex.sucursal.ToUpper().TrimEnd().TrimStart()).First().id;
                                        else
                                            sucursalNoEncontrada.Add(ex.sucursal);

                                        //REVISAR SI ES WEB QUE NO TENGA HOMOLOGACIÓN DE PRECIOS TODAS SUCURSALES CON WEB
                                        if (ex.sucursal_id == 1)
                                        {
                                            //SUCURSAL ES WEB
                                            //PRECIOS HOMOLOGADOS MENOS WEB?
                                            if (!conf.centralizacion_precios_web.Value)
                                            {
                                                //ACTUALIZA
                                                OK_actualiza_por_nombre = true;
                                            }
                                            else
                                            {
                                                error_actualizar_por_nombre = "NO SE PUEDE ACTUALIZAR WEB SOLAMENTE PORQUE LA CONFIGURACIÓN DE LA PLATAFORMA INDICA QUE LOS PRECIOS ESTAN HOMOLOGADOS PARA TODAS LAS SUCURSALES. DESCARGA EL LAYOUT, LLENALO E INTENTALO DE NUEVO. ";
                                            }

                                        }
                                        //REVISAR SI NO ES  WEB REVISAR QUE NO TENGA HOMOLOGACIÓN DE PRECIOS SUCURSALES
                                        else if (ex.sucursal_id != null)
                                        {
                                            //TRAE UN SUCURSAL QUE NO ES WEB
                                            //PRECIOS DESHOMOLOGADOS?
                                            if (!conf.centralizacion_precios_sucursales.Value)
                                            {
                                                //ACTUALIZA
                                                OK_actualiza_por_nombre = true;
                                            }
                                            else
                                            {
                                                error_actualizar_por_nombre = "NO SE PUEDE ACTUALIZAR SOLO UNA SUCURSAL PORQUE TU CONFIGURACIÓN INDICA QUE TIENES LOS PRECIOS HOMOLOGADOS PARA TODAS LAS SUCURSALES. DESCARGA EL LAYOUT, LLENALO E INTENTALO DE NUEVO.";
                                            }

                                        }
                                        else
                                        {
                                            //NO EXISTE SUCURSAL
                                            OK_actualiza_por_nombre = true;
                                        }

                                        //OK ACTUALIZAR LA SUCURSAL
                                        if (OK_actualiza_por_nombre)
                                        {
                                            //REVISAR SI EL USUARIO TIENE PERMISO PARA ACTUALIAR ESTA SUCURSAL
                                            if (ex.sucursal_id != null)
                                            {
                                                if (LSucursalesUsuario.Where(x => x.id == ex.sucursal_id).Any())
                                                {
                                                    //EL USUARIO SI TIENE PERMISO PARA LA SUCURSAL
                                                    foreach (tipoPrecioAux tp in auxTipoPrecioColumna)
                                                    {
                                                        ex.tipoPrecio = tp.tipoPrecio;
                                                        ex.tipoPrecio_id = Convert.ToInt16(tp.tipoprecio_id);

                                                        //SI CANTIDAD ES NUMERICO ENTONCES GUARDA LA CONVERSION DE ESA CADENA EN CANTIDAD_DECIMAL
                                                        //DE LO CONTRARIO GUARDA CANTIDAD VACIO Y GUARDA CantidadesDecimalOK = FALSE
                                                        Decimal cantInt = 0;

                                                        ex.precioNuevo = Convert.ToString(values[tp.index]).Replace('"', '\"').Replace("'", "\'").TrimEnd().TrimStart().ToUpper();
                                                        if (ex.precioNuevo != "") { }
                                                        if (!Decimal.TryParse(ex.precioNuevo, out cantInt))
                                                            codigoProdCantidadNoDecimal.Add(ex.Codigo);

                                                        queryAgregarNuevoProducto +=
                                                            //VARIABLES
                                                                @" SET @Producto_id:=IF((select count(*) from producto where TRIM(codigo)='" + ex.Codigo.Replace("'", "\\'") + "')>=1, (SELECT id from PRODUCTO WHERE TRIM(codigo)='" + ex.Codigo.Replace("'", "\\'") + "') , '');"
                                                                + " SET @Sucursal:='" + ex.sucursal + "';"
                                                                + " SET @Sucursal_id:='" + ex.sucursal_id + "';"
                                                                + " SET @TipoCliente_id:='" + ex.tipoPrecio_id + "';"
                                                            //TRAER PRECIO ACTUAL SI EXISTE, SI NO TRAE 0
                                                                + " SET @precioActual:=IF(@Producto_id!='',"
                                                                + "IF((SELECT EXISTS(SELECT * FROM precio_por_tipocliente WHERE Producto_id=@Producto_id AND Sucursal_id=@Sucursal_id AND TipoCliente_id=@TipoCliente_id))=1"
                                                                + ",(SELECT PRECIO FROM precio_por_tipocliente WHERE Producto_id=@Producto_id AND Sucursal_id=@Sucursal_id  AND TipoCliente_id=@TipoCliente_id),0),0);"
                                                                + " SET @diferencia:=" + ex.precioNuevo + "- @precioActual;"
                                                                + " SET @duplicado:=IF((select count(*) from precioactualizacionexcel where Producto_id=@Producto_id AND tipoPrecio_id=@TipoCliente_id  AND Sucursal_id=@Sucursal_id AND idUsuarioActualiza=" + idusuarioLoggeado + " )>=1,1,0);"
                                                                + " SET @Nuevo:=IF(@Producto_id='',1,0);"

                                                            //INSERTA EN TABLA precioactualizacionexcel
                                                                + " INSERT INTO precioactualizacionexcel"
                                                            + "("
                                                            + "codigo,"
                                                            + "Sucursal,"
                                                            + "Sucursal_id,"
                                                            + "Producto_id,"
                                                            + "precioActual,"
                                                            + "precioNuevo,"
                                                            + "diferencia,"
                                                            + "tipoPrecio,"
                                                            + "tipoPrecio_id,"
                                                            + "copiado,"
                                                            + "nuevo,"
                                                            + " fecha,"
                                                            + " duplicado,"
                                                            + "idUsuarioActualiza)"
                                                            + "VALUES"
                                                            + "("
                                                            + "'" + ex.Codigo.Replace("'", "\\'") + "'" + ","
                                                            + "'" + ex.sucursal + "'" + ","
                                                            + ex.sucursal_id + ","
                                                            + "@Producto_id,"
                                                            + "@precioActual,"
                                                            + "'" + ex.precioNuevo + "'" + ","
                                                            + "@diferencia,"
                                                            + "'" + ex.tipoPrecio + "'" + ","
                                                            + "'" + ex.tipoPrecio_id + "'" + ","
                                                            + "0,"
                                                            + "@Nuevo,"
                                                            + "NOW(),"
                                                            + "@duplicado,"
                                                            + idusuarioLoggeado
                                                            + ");";

                                                        RegistrosSubidos++;

                                                        //Contar100++;
                                                        // if (Contar100 == 100)
                                                        //contarRegistrosProcesados++;
                                                        if (RenglonesExcelProcesados >= CuentaMAX)
                                                        {
                                                            if (queryAgregarNuevoProducto != "")
                                                            {
                                                                using (MySqlCommand com = new MySqlCommand(queryAgregarNuevoProducto, dbConn))
                                                                {
                                                                    // Agregado += 100;
                                                                    //Contar100 = 0;
                                                                    RenglonesExcelProcesados = 0;
                                                                    dbConn.Open();
                                                                    com.ExecuteNonQuery();
                                                                    dbConn.Close();
                                                                    //calcular porcentaje
                                                                    msg = "Agregando Registros A DB...";
                                                                    subProcesoActual = 3;
                                                                    sendVariable = new object[3];
                                                                    sendVariable[0] = " (" + subProcesoActual + "/" + totalSubProcesos + ") " + msg;
                                                                    sendVariable[1] = suma_porcentaje_total;
                                                                    sendVariable[2] = "Registros procesados: (" + RenglonesExcelProcesadosTOTAL + "/" + totalContarRenglonesExcel + ")";
                                                                    Clients.Caller.sendMessage(sendVariable);
                                                                }
                                                                queryAgregarNuevoProducto = "";
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    //EL USUARIO  TIENE PERMISO PARA ESTA SUCURSAL
                                                    string msgerror = "<div class=error-box alert style='background-color:#ff9c9c; padding: 15px; width: 100%;'>"
                                                         + "<div class=msg style='color: #541a1a; margin-top: 5px;'>"
                                                              + "</div> NO CUENTAS CON PERMISO PARA ACTUALIZAR LA SUCURSAL:</br><label>" + ex.sucursal.ToUpper() + ".</label></br> ";
                                                    foreach (var tp in tipoPrecioNoexiste)
                                                    {
                                                        msgerror += "</br><label style='font-weight:bold'>" + tp + "</label>";
                                                    }
                                                    msgerror += "</div>"
                                                                        + "</div>"
                                                                    + "</div>";
                                                    Clients.Caller.error(msgerror);
                                                }
                                            }

                                        }
                                        else
                                        {
                                            string msgerror = "<div class=error-box alert style='background-color:#ff9c9c; padding: 15px; width: 100%;'>"
                                                + "<div class=msg style='color: #541a1a; margin-top: 5px;'>"
                                                     + "</div>" + error_actualizar_por_nombre + "</br> ";
                                            msgerror += "</div>"
                                                                + "</div>"
                                                            + "</div>";
                                            Clients.Caller.error(msgerror);
                                        }
                                    }
                                }
                                //Contar100++;
                                RenglonesExcelProcesados++;
                                RenglonesExcelProcesadosTOTAL++;
                            }

                            //SI QUEDA UN PRODUCTO POR AGREGAR
                            if (queryAgregarNuevoProducto != "")
                            {
                                using (MySqlCommand com = new MySqlCommand(queryAgregarNuevoProducto, dbConn))
                                {
                                    dbConn.Open();
                                    com.ExecuteNonQuery();
                                    dbConn.Close();
                                    //calcular porcentaje
                                    msg = "Agregando Registros A DB...";
                                    subProcesoActual = 3;
                                    sendVariable = new object[3];
                                    sendVariable[0] = " (" + subProcesoActual + "/" + totalSubProcesos + ") " + msg;
                                    sendVariable[1] = 100;
                                    sendVariable[2] = "Registros procesados: (" + totalContarRenglonesExcel + "/" + totalContarRenglonesExcel + ")";
                                    Clients.Caller.sendMessage(sendVariable);
                                }
                            }
                        }
                        else
                        {
                            //algunos tipos de precio no existen
                            string msgerror = "<div class=error-box alert style='background-color:#ff9c9c; padding: 15px; width: 100%;'>"
                                                 + "<div class=msg style='color: #541a1a; margin-top: 5px;'>"
                                                      + "</div> LAS SIGUIENTES TIPOS DE PRECIO NO EXISTEN. FAVOR DE PONER EL NOMBRE EXACTAMENTE COMO ESTA EN LA PLATAFORMA.</br> ";
                            foreach (var tp in tipoPrecioNoexiste)
                            {
                                msgerror += "</br><label style='font-weight:bold'>" + tp + "</label>";
                            }
                            msgerror += "</div>"
                                                + "</div>"
                                            + "</div>";
                            Clients.Caller.error(msgerror);
                        }
                    }
                    else
                    {
                        //TIPO DE PRECIO DUPLICADOS
                        //algunos tipos de precio no existen
                        string msgerror = "<div class=error-box alert style='background-color:#ff9c9c; padding: 15px; width: 100%;'>"
                                             + "<div class=msg style='color: #541a1a; margin-top: 5px;'>"
                                                  + "</div> EXISTEN TIPOS DE PRECIO DUPLICADOS.</br> ";
                        foreach (var tp in LPreciosDuplicados.Distinct())
                        {
                            msgerror += "</br><label style='font-weight:bold'>" + LPreciosDuplicados.Where(x => x == tp).Count() + "x " + tp + "</label>";
                        }
                        msgerror += "</div>"
                   + "</div>"
               + "</div>";

                        Clients.Caller.error(msgerror);
                    }
                }
                else
                {
                    //NO EXISTE NINGUN TIPO DE PRECIO
                    string msgerror = "<div class=error-box alert style='background-color:#ff9c9c; padding: 15px; width: 100%;'>"
                                 + "<div class=msg style='color: #541a1a; margin-top: 5px;'>"
                                      + "</div>NO EXISTE NINGUN TIPO DE PRECIO EN EL ARCHIVO EXCEL.</br> "
                                     + "</div>"
                                + "</div>"
                            + "</div>";
                    Clients.Caller.error(msgerror);
                }

                //****************************
                //SUBPROCESO REVISAR ARCHIVO
                //****************************

                int Analizado = 0;
                List<precioactualizacionexcel> ListaNuevosProductos = db.precioactualizacionexcel.Where(x => x.idUsuarioActualiza == idusuarioLoggeado && !x.copiado).ToList();
                List<producto> ListaProductos = db.producto.ToList();
                List<ModeloNuevoProducto> ListaModeloNuevosPreoductos = new List<ModeloNuevoProducto>();


                if (ListaNuevosProductos.Count() == 0)
                {
                    //ERROR 
                    Clients.Caller.error("No se subió ningun registro. Revisa tu layout y vuelve a intentarlo.");
                    msg = "ERROR: No se subió ningun registro. Revisa tu layout y vuelve a intentarlo.";
                    subProcesoActual = 4;
                    sendVariable = new object[3];
                    sendVariable[0] = " (0/0) " + msg;
                    sendVariable[1] = 100;
                    sendVariable[2] = "Registros procesados: (0/" + totalContarRenglonesExcel + ")";// string.Format(" ({0}/{1})", subProcesoActual, totalSubProcesos);
                    Clients.Caller.sendMessage(sendVariable);
                }
                else
                {
                    decimal porcentaje_por_registro = porcentaje_por_subproceso / ListaNuevosProductos.Count();
                    //List<string> ProductosExistentes = db.producto.Select(x => x.Codigo.ToUpper().Trim()).Distinct().ToList();
                    foreach (precioactualizacionexcel precioactualizacionexcel in ListaNuevosProductos)
                    {
                        //bool existe = ProductosExistentes.Contains(precioactualizacionexcel.Codigo.ToUpper().Trim());
                        //int nuevo = 0;
                        //if (!existe)
                        //{
                        //    nuevo = 1;
                        //    dbConn.Open();
                        //    cmd = new MySqlCommand("UPDATE precioactualizacionexcel SET NUEVO=" + nuevo + " WHERE id=" + precioactualizacionexcel.id + " AND idUsuarioActualiza=" + idusuarioLoggeado + " ;", dbConn);
                        //    cmd.ExecuteNonQuery();
                        //    dbConn.Close();
                        //}
                        Analizado++;

                        //CALCULAR PORCENTAJE
                        //PORCENAJE 75% - 100%
                        suma_porcentaje_total += porcentaje_por_registro;
                        // int porc = Analizado * 100 / totalContarRegistros;
                        //ENVIAR AVANCE ACTUAL
                        msg = "Analizando Registros...";
                        subProcesoActual = 4;
                        sendVariable = new object[3];
                        sendVariable[0] = " (" + subProcesoActual + "/" + totalSubProcesos + ") " + msg;
                        sendVariable[1] = suma_porcentaje_total;
                        sendVariable[2] = "Registros procesados: (" + Analizado + "/" + totalContarRenglonesExcel + ")";// string.Format(" ({0}/{1})", subProcesoActual, totalSubProcesos);
                        Clients.Caller.sendMessage(sendVariable);
                    }


                    if (sucursalNoEncontrada.Count() == 0 && codigoProdCantidadNoDecimal.Count() == 0)
                    {
                        //AVISA QUE YA TERMINO DE SUBIR EL ARCHIVO
                        int subidos = db.precioactualizacionexcel.Where(x => x.idUsuarioActualiza == idusuarioLoggeado).Count();
                        if (RegistrosSubidos == subidos)
                        {
                            Clients.Caller.ExcelSubidoDB_OK();
                            if (subidos == Analizado)
                            {
                                Clients.Caller.Analizado_OK();
                            }
                            else
                            {
                                Clients.Caller.error("No Analizaron " + RegistrosSubidos + " de " + subidos + ". Favor de revisar tu archivo y volver a intertarlo.");

                            }
                        }
                        else
                        {
                            Clients.Caller.error("Se subieron " + subidos + " de " + totalContarRenglonesExcel + ". Favor de revisar tu archivo y volver a intertarlo.");

                        }
                    }
                    else
                    {
                        //ENVIAR ERROR
                        if (sucursalNoEncontrada.Count() != 0)
                        {
                            string msgerror = "<div class=error-box alert style='background-color:#ff9c9c; padding: 15px; width: 100%;'>"
                                                + "<div class=msg style='color: #541a1a; margin-top: 5px;'>"
                                                     + "</div> LAS SIGUIENTES SUCURSALES NO EXISTEN. FAVOR DE PONER EL NOMBRE EXACTAMENTE COMO ESTA EN LA PLATAFORMA.</br> ";
                            foreach (var s in sucursalNoEncontrada)
                            {
                                msgerror += "</br><label style='font-weight:bold'>" + s + "</label>";
                            }
                            msgerror += "</div>"
                                                + "</div>"
                                            + "</div>";
                            Clients.Caller.error(msgerror);
                        }
                        if (codigoProdCantidadNoDecimal.Count() != 0)
                        {
                            string msgerror = "<div class=error-box alert style='background-color:#ff9c9c; padding: 15px; width: 100%;'>"
                                               + "<div class=msg style='color: #541a1a; margin-top: 5px;'>"
                                                     + "</div> LOS SIGUINTES PRODUCTOS CONTIENEN UNA CANTIDAD CON FORMATO INCORRECTO. ESTE CAMPO ADMITE SOLO NUMEROS DEL 1-9 Y PUNTO DECIMAL.</br> ";
                            foreach (var p in codigoProdCantidadNoDecimal)
                            {
                                msgerror += "</br><label style='font-weight:bold'>" + p + "</label>";
                            }
                            msgerror += "</div>"
                                                + "</div>"
                                            + "</div>";
                            Clients.Caller.error(msgerror);

                        }

                    }
                }
            }
            catch (Exception e)
            {
                object[] sendVariable = new object[2];
                sendVariable[0] = "Error:" + e.Message;
                sendVariable[0] += e.InnerException != null ? ".      Inner:" + e.InnerException.ToString() : "";
                sendVariable[0] += ".           El Archivo Existe?: " + existearchivo.ToString();
                sendVariable[1] = 0;
                Clients.Caller.sendMessage(sendVariable);
                Clients.Caller.error(e.Message.ToString());
            }
        }

        public void Producto_RevisarArchivo_DB()
        {

            //VAR
            POS.Models.moragas_pruebasEntities1 db = new Models.moragas_pruebasEntities1();
            int idusuarioLoggeado = Fn.usuario_logeado.id();
            List<precioactualizacionexcel> ListaNuevosProductos = db.precioactualizacionexcel.Where(x => x.idUsuarioActualiza == idusuarioLoggeado && !x.copiado).ToList();
            List<producto> ListaProductos = db.producto.ToList();
            List<ModeloNuevoProducto> ListaModeloNuevosPreoductos = new List<ModeloNuevoProducto>();

            int totalContarRegistros = ListaNuevosProductos.Count();
            int Agregado = 0;


            List<string> ProductosExistentes = db.producto.Select(x => x.Codigo.ToUpper().Trim()).Distinct().ToList();
            foreach (precioactualizacionexcel precioactualizacionexcel in ListaNuevosProductos)
            {


                ModeloNuevoProducto modelo_nuevo_producto = new ModeloNuevoProducto();
                int nuevo = 1;
                bool existe = ProductosExistentes.Contains(precioactualizacionexcel.Codigo.ToUpper().Trim());
                if (existe)
                    nuevo = 0;
                if (nuevo == 1)
                {
                    modelo_nuevo_producto.estatus = "Nuevo";
                }
                else
                {
                    modelo_nuevo_producto.estatus = "Actualizacion";
                }
                ListaModeloNuevosPreoductos.Add(modelo_nuevo_producto);
                //AUMENTAR REVISADOS 1
                Agregado++;
                //CALCULAR PORCENTAJE
                int porc = Agregado * 100 / totalContarRegistros;
                //ENVIAR AVANCE ACTUAL
                msg = "Cargando Registros En DB...";
                Clients.Caller.sendMessage(string.Format
                        (msg + " {0}% de {1}%", porc, count));

            }
        }
    }
}