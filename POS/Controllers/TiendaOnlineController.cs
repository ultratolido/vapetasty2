﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using POS.Models;
using System.Data;
using POS.Fn;
using System.Globalization;
using System.Web.Script.Serialization;
using System.IO;

namespace POS.Controllers
{
    public class TiendaOnlineController : Controller
    {
        private moragas_pruebasEntities1 db = new moragas_pruebasEntities1();
        private CultureInfo IdiomaFecha = new CultureInfo("es-MX");

        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName = "Ventas En Linea")]
        public ActionResult VentaOnline()
        {
            CultureInfo culture = new CultureInfo("es-MX");
            List<int> idsucursales = Fn.usuario_logeado.Sucursales_id();
            if (idsucursales.Contains(1))
                ViewBag.id_sucursal_online = new SelectList(db.sucursal.Where(x => x.id == 1), "id", "nombre_sucursal");
            ViewBag.ListaTipoPago = db.tipo_pago_venta.ToList();
            ViewBag.HoyInicio = DateTime.Now.ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.HoyFin = DateTime.Now.ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.AyerInicio = DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.AyerFin = DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.AntierInicio = DateTime.Now.AddDays(-2).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.AntierFin = DateTime.Now.AddDays(-2).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.SemanaActualInicio = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.SemanaActualFin = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(6).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.SemanaPasadaInicio = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(-7).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.SemanaPasadaFin = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(-7).AddDays(6).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.SemanaAntepasadaInicio = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(-14).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.SemanaAntepasadaFin = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(-14).AddDays(6).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            DateTime hoy = Convert.ToDateTime(DateTime.Now.ToShortDateString());
            int numero_de_dia = hoy.Day;
            if (numero_de_dia > 15)
            //segunda quincena
            {
                ViewBag.QuincenaActualInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 15).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaActualFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.Month)).ToString("MM/dd/yyyy") + " 11:59:59 PM";
                ViewBag.QuincenaPasadaInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaPasadaFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddDays(15).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
                ViewBag.QuincenaAntepasadaInicio = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, 15).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaAntepasadaFin = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month)).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            }
            else //primera quincena
            {
                ViewBag.QuincenaActualInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaActualFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddDays(15).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
                ViewBag.QuincenaPasadaInicio = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, 15).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaPasadaFin = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month)).ToString("MM/dd/yyyy") + " 11:59:59 PM";
                ViewBag.QuincenaAntepasadaInicio = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaAntepasadaFin = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, 1).AddDays(15).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            }
            ViewBag.MesActualInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.MesActualFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(1).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.MesPasadoInicioFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.MesPasadoFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-1).AddMonths(1).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.MesAntepasadoInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-2).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.MesAntepasadoFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-2).AddMonths(1).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            return View();
        }

        [Authorize]
        public PartialViewResult VistaParcialVentaOnline(int idSucursal, string periodo)
        {
            String[] fecha = Fn.Fechas.CalcularIncioFin(periodo);
            String inicio = fecha[0];
            String fin = fecha[1];
            DateTime _inicio = Convert.ToDateTime(inicio, IdiomaFecha);
            DateTime _fin = Convert.ToDateTime(fin, IdiomaFecha);
            String UploadFolderTiendaOnline = new DirectoryInfo(Server.MapPath("~/")).Parent.FullName + "\\Tienda Online\\UploadFolder";
            CultureInfo culture = new CultureInfo("es-MX");
            List<int> ListaIDVentaOnline = new List<int> { 8, 9, 10, 11 };//12 cancelada
            List<venta> ventas_online = db.venta.Where(x => ListaIDVentaOnline.Contains(x.catalogo_status_id) && x.sucursal_id == idSucursal && 
            ((x.fecha >= _inicio && x.fecha <= _fin) || (x.fechaEnvio >= _inicio && x.fechaEnvio <= _fin) ||
            (x.fecha_liquidacion >= _inicio && x.fecha_liquidacion <= _fin) || (x.Fecha_entrega >= _inicio && x.Fecha_entrega <= _fin))).ToList();
            ViewBag.UploadFolderTiendaOnline = UploadFolderTiendaOnline;
            return PartialView(ventas_online);
        }

        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName = "Cotizaciones En Linea")]
        public ActionResult CotizacionOnline()
        {
            CultureInfo culture = new CultureInfo("es-MX");
            List<int> idsucursales = Fn.usuario_logeado.Sucursales_id();
            if (idsucursales.Contains(1))
                ViewBag.id_sucursal_online = new SelectList(db.sucursal.Where(x => x.id == 1), "id", "nombre_sucursal");
            ViewBag.HoyInicio = DateTime.Now.ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.HoyFin = DateTime.Now.ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.AyerInicio = DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.AyerFin = DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.AntierInicio = DateTime.Now.AddDays(-2).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.AntierFin = DateTime.Now.AddDays(-2).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.SemanaActualInicio = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.SemanaActualFin = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(6).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.SemanaPasadaInicio = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(-7).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.SemanaPasadaFin = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(-7).AddDays(6).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.SemanaAntepasadaInicio = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(-14).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.SemanaAntepasadaFin = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(-14).AddDays(6).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            DateTime hoy = Convert.ToDateTime(DateTime.Now.ToShortDateString());
            int numero_de_dia = hoy.Day;
            if (numero_de_dia > 15)
            //segunda quincena
            {
                ViewBag.QuincenaActualInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 15).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaActualFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.Month)).ToString("MM/dd/yyyy") + " 11:59:59 PM";
                ViewBag.QuincenaPasadaInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaPasadaFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddDays(15).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
                ViewBag.QuincenaAntepasadaInicio = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, 15).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaAntepasadaFin = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month)).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            }
            else //primera quincena
            {
                ViewBag.QuincenaActualInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaActualFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddDays(15).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
                ViewBag.QuincenaPasadaInicio = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, 15).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaPasadaFin = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month)).ToString("MM/dd/yyyy") + " 11:59:59 PM";
                ViewBag.QuincenaAntepasadaInicio = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaAntepasadaFin = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, 1).AddDays(15).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            }
            ViewBag.MesActualInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.MesActualFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(1).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.MesPasadoInicioFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.MesPasadoFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-1).AddMonths(1).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.MesAntepasadoInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-2).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.MesAntepasadoFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-2).AddMonths(1).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            return View();
        }

        public PartialViewResult VistaParcialCotizacionOnline(int idSucursal, String periodo, String filtro)
        {
            CultureInfo culture = new CultureInfo("es-MX");
            String[] fecha = Fn.Fechas.CalcularIncioFin(periodo);
            String inicio = fecha[0];
            String fin = fecha[1];
            DateTime _inicio = Convert.ToDateTime(inicio, IdiomaFecha);
            DateTime _fin = Convert.ToDateTime(fin, IdiomaFecha);
            List<venta> todas_cotizaciones_sucursal = new List<venta>();
            if(periodo != null && !periodo.Equals(""))
                todas_cotizaciones_sucursal = db.venta.Where(x => (x.catalogo_status_id == 7) && x.sucursal_id == idSucursal && x.fecha_cotización >= _inicio && x.fecha_cotización <= _fin).OrderBy(x => x.Folio).ToList();
            else
                todas_cotizaciones_sucursal = db.venta.Where(x => (x.catalogo_status_id == 7) && x.sucursal_id == idSucursal).OrderBy(x => x.Folio).ToList();
            if (filtro == null)
                filtro = "";
            List<venta> cotizaciones_filtradas = new List<venta>();
            foreach (venta c in todas_cotizaciones_sucursal)
            {
                string serie_folio = Fn.Numeros.serie_folio_by_id(c.id);
                if (serie_folio.Contains(filtro))
                    cotizaciones_filtradas.Add(c);
            }
            return PartialView(cotizaciones_filtradas);
        }

        public ActionResult CancelarVentaOnline(int id)
        {
            try
            {
                venta venta = db.venta.Find(id);
                venta.fecha_cancelacion = DateTime.Now;
                venta.usuario_id_cancela = Fn.usuario_logeado.traerUsuario().id;
                venta.catalogo_status_id = 12;
                db.Entry(venta).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                var respuesta = new
                {
                    success = true
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                var respuesta = new
                {
                    success = false
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult cantidadEnInventario(String codigo_producto)
        {
            Decimal cantidad_en_inventario = 0;
            
            try
            {
                List<int> lista_id_sucursales = db.sucursal.Where(x => x.alimenta_tienda_online == true).Select(y => y.id).ToList();
                producto pro = db.producto.FirstOrDefault(x => x.Codigo.Equals(codigo_producto));
                List<inventario> lista_inventario = db.inventario.Where(x => x.Producto_id == pro.id && lista_id_sucursales.Contains(x.sucursal_id)).ToList();
                if (lista_inventario != null && lista_inventario.Count > 0)
                    cantidad_en_inventario = Convert.ToDecimal(lista_inventario.Select(x => x.cantidad).Sum());
                var respuesta = new
                {
                    success = true,
                    cantidad_en_inventario = cantidad_en_inventario
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);

            }
            catch (Exception)
            {
                var respuesta = new
                {
                    success = false,
                    cantidad_en_inventario = 0
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
                throw;
            }
        }

        public JsonResult cantidadEnInventarioProductoSucursal(int id_producto, int id_sucursal)
        {
            Decimal cantidad_en_inventario = 0;

            try
            {
                cantidad_en_inventario = Convert.ToDecimal(db.inventario.FirstOrDefault(x => x.Producto_id == id_producto && x.sucursal_id == id_sucursal).cantidad);
                var respuesta = new
                {
                    success = true,
                    cantidad_en_inventario = cantidad_en_inventario
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);

            }
            catch (Exception)
            {
                var respuesta = new
                {
                    success = false,
                    cantidad_en_inventario = 0
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
                throw;
            }
        }

        public PartialViewResult ModalConceptosCotizacionOnline(int id)
        {
            venta cotizacion = db.venta.Find(id);
            ViewBag.cotizacion = cotizacion;
            return PartialView();
        }

        private UploadedFile RetrieveFileFromRequest()
        {
            string filename = null;
            string fileType = null;
            byte[] fileContents = null;
            string ID;

            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];
                fileContents = new byte[file.ContentLength];
                fileType = file.ContentType;
                filename = file.FileName;

            }
            else if (Request.ContentLength > 0)
            {
                fileContents = new byte[Request.ContentLength];
                Request.InputStream.Read(fileContents, 0, Request.ContentLength);
                filename = Request.Headers["X-File-Name"];
                fileType = Request.Headers["X-File-Type"];
            }
            return new UploadedFile()
            {
                Filename = filename,
                ContentType = fileType,
                FileSize = fileContents != null ? fileContents.Length : 0,
                Contents = fileContents
            };
        }

        [HttpPost]
        public void UploadArchivo(int idVenta)
        {
            String UploadFolderTiendaOnline = new DirectoryInfo(Server.MapPath("~/")).Parent.FullName + "\\Tienda Online\\UploadFolder";
            UploadedFile file = RetrieveFileFromRequest();
            string nombre_archivo = idVenta + "-" + file.Filename.Trim().Replace("&", "");
            //string rootPath = AppDomain.CurrentDomain.BaseDirectory;
            String savedFileIMG = UploadFolderTiendaOnline + "/UploadFolder/adjuntos_cliente_compra/" + nombre_archivo;
            String extension = System.IO.Path.GetExtension(file.Filename).ToLower();

            //si el cliente tiene una imagen registrada buscarla y si existe eliminarla
            if (System.IO.File.Exists(savedFileIMG))
            {
                //si ya existe entonces agrega la fecha antes de la extensión para que tenga un nombre único
                string fecha = DateTime.Now.ToString("yyyyMMddHHmmss");
                nombre_archivo = nombre_archivo.Replace(extension, "") + fecha + extension;
            }
            //guardar archivo
            //string UploadFolderTiendaOnline = new DirectoryInfo(Server.MapPath("~/")).Parent.Parent.FullName + "\\Tienda\\Tienda Online\\UploadFolder";
            System.IO.File.WriteAllBytes(UploadFolderTiendaOnline + "\\adjuntos_cliente_compra\\" + nombre_archivo, file.Contents);
            db.SaveChanges();
        }

    }
}
