﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using POS.Models;
using System.Data;
using POS.Fn;

namespace POS.Controllers
{
    public class ReporteController : Controller
    {

        moragas_pruebasEntities1 db = new moragas_pruebasEntities1();
        CultureInfo IdiomaFecha = new CultureInfo("es-MX");

        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName = "Historial De Ventas")]
        public ActionResult Historial()
        {
            List<int> ListaSucursalesPermitidas = usuario_logeado.Sucursales_id();
            ViewBag.id_sucursal = new SelectList(db.sucursal.Where(x => ListaSucursalesPermitidas.Contains(x.id)).OrderByDescending(y => y.id), "id", "nombre_sucursal");
            ViewBag.HoyInicio = DateTime.Now.ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.HoyFin = DateTime.Now.ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.AyerInicio = DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.AyerFin = DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.AntierInicio = DateTime.Now.AddDays(-2).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.AntierFin = DateTime.Now.AddDays(-2).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.SemanaActualInicio = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.SemanaActualFin = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(6).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.SemanaPasadaInicio = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(-7).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.SemanaPasadaFin = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(-7).AddDays(6).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.SemanaAntepasadaInicio = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(-14).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.SemanaAntepasadaFin = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(-14).AddDays(6).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            DateTime hoy = Convert.ToDateTime(DateTime.Now.ToShortDateString());
            int numero_de_dia = hoy.Day;
            if (numero_de_dia > 15)
            //segunda quincena
            {
                ViewBag.QuincenaActualInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 15).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaActualFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.Month)).ToString("MM/dd/yyyy") + " 11:59:59 PM";
                ViewBag.QuincenaPasadaInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaPasadaFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddDays(15).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
                ViewBag.QuincenaAntepasadaInicio = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, 15).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaAntepasadaFin = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month)).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            }
            else //primera quincena
            {
                ViewBag.QuincenaActualInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaActualFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddDays(15).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
                ViewBag.QuincenaPasadaInicio = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, 15).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaPasadaFin = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month)).ToString("MM/dd/yyyy") + " 11:59:59 PM";
                ViewBag.QuincenaAntepasadaInicio = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaAntepasadaFin = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, 1).AddDays(15).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            }
            ViewBag.MesActualInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.MesActualFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(1).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.MesPasadoInicioFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.MesPasadoFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-1).AddMonths(1).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.MesAntepasadoInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-2).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.MesAntepasadoFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-2).AddMonths(1).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.id_catalogo_status = new SelectList(db.catalogo_status.Where(x => (x.id >= 1 && x.id <= 2) || (x.id >= 9 && x.id <= 12)), "id", "status");
            return View();
        }

        public PartialViewResult VistaParcialHistorial(int idSucursal, int id_catalogo_status, String periodo)
        {
            String[] fecha = Fn.Fechas.CalcularIncioFin(periodo);
            String inicio = fecha[0];
            String fin = fecha[1];
            usuario_punto_venta u = Fn.usuario_logeado.traerUsuario();
            List<venta> ListaVentas = new List<venta>();
            DateTime _inicio = Convert.ToDateTime(inicio, IdiomaFecha);
            DateTime _fin = Convert.ToDateTime(fin, IdiomaFecha);
            if (id_catalogo_status == 0)
            {
                ListaVentas = db.venta.Where(x => ((x.catalogo_status_id == 1 && x.Fecha_entrega >= _inicio && x.Fecha_entrega <= _fin)
                                                  || (x.catalogo_status_id == 9 && x.fecha_liquidacion >= _inicio && x.fecha_liquidacion <= _fin)
                                                  || (x.catalogo_status_id == 10 && x.fecha_liquidacion >= _inicio && x.fecha_liquidacion <= _fin)
                                                  || (x.catalogo_status_id == 11 && x.fecha_liquidacion >= _inicio && x.fecha_liquidacion <= _fin)
                                                  || (x.catalogo_status_id == 2 && x.fecha_cancelacion >= _inicio && x.fecha_cancelacion <= _fin)
                                                  || (x.catalogo_status_id == 12 && x.fecha_cancelacion >= _inicio && x.fecha_cancelacion <= _fin && x.fecha_liquidacion != null))
                                                  && x.sucursal_id == idSucursal).OrderBy(x => x.Folio).ToList();
            }
            else
            {
                if(id_catalogo_status == 1)
                {
                    ListaVentas = db.venta.Where(x =>   ((x.catalogo_status_id == 1 && x.Fecha_entrega >= _inicio && x.Fecha_entrega <= _fin))
                                                        && x.sucursal_id == idSucursal).OrderBy(x => x.Folio).ToList();
                }
                else if(id_catalogo_status == 2)
                {
                    ListaVentas = db.venta.Where(x =>   ((x.catalogo_status_id == 2 && x.fecha_cancelacion >= _inicio && x.fecha_cancelacion <= _fin))
                                                        && x.sucursal_id == idSucursal).OrderBy(x => x.Folio).ToList();
                }
                else if(id_catalogo_status >= 9 && id_catalogo_status <= 11)
                {
                    ListaVentas = db.venta.Where(x =>   ((x.catalogo_status_id == id_catalogo_status && x.fecha_liquidacion >= _inicio && x.fecha_liquidacion <= _fin))
                                                        && x.sucursal_id == idSucursal).OrderBy(x => x.Folio).ToList();
                }
                else if(id_catalogo_status == 12)
                {
                    ListaVentas = db.venta.Where(x =>   ((x.catalogo_status_id == id_catalogo_status && x.fecha_cancelacion >= _inicio 
                                                        && x.fecha_cancelacion <= _fin && x.fecha_liquidacion != null))
                                                        && x.sucursal_id == idSucursal).OrderBy(x => x.Folio).ToList();
                }
                
            }
            Decimal total_facturado = ListaVentas.Select(x => x.total.Value).Sum();
            Decimal total_cancelado = ListaVentas.Where(x => x.catalogo_status_id == 2 || x.catalogo_status_id == 12).Select(x => x.total.Value).Sum();
            Decimal total = total_facturado - total_cancelado;
            List<pdf_venta> ListaPDFs = db.pdf_venta.ToList();
            ViewBag.ListaPDF = ListaPDFs;
            ViewBag.total_facturado = total_facturado;
            ViewBag.total_cancelado = total_cancelado;
            ViewBag.total = total;
            ViewBag.ListaVentas = ListaVentas;
            return PartialView();
        }

        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName = "Ventas Por Producto")]
        public ActionResult VentaTipoPrecio()
        {
            List<int> ListaSucursalesPermitidas = usuario_logeado.Sucursales_id();
            ViewBag.id_sucursal = new SelectList(db.sucursal.Where(x => ListaSucursalesPermitidas.Contains(x.id)).OrderByDescending(y => y.id), "id", "nombre_sucursal");
            ViewBag.HoyInicio = DateTime.Now.ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.HoyFin = DateTime.Now.ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.AyerInicio = DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.AyerFin = DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.AntierInicio = DateTime.Now.AddDays(-2).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.AntierFin = DateTime.Now.AddDays(-2).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.SemanaActualInicio = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.SemanaActualFin = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(6).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.SemanaPasadaInicio = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(-7).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.SemanaPasadaFin = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(-7).AddDays(6).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.SemanaAntepasadaInicio = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(-14).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.SemanaAntepasadaFin = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(-14).AddDays(6).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            DateTime hoy = Convert.ToDateTime(DateTime.Now.ToShortDateString());
            int numero_de_dia = hoy.Day;
            if (numero_de_dia > 15)
            //segunda quincena
            {
                ViewBag.QuincenaActualInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 15).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaActualFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.Month)).ToString("MM/dd/yyyy") + " 11:59:59 PM";
                ViewBag.QuincenaPasadaInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaPasadaFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddDays(15).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
                ViewBag.QuincenaAntepasadaInicio = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, 15).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaAntepasadaFin = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month)).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            }
            else //primera quincena
            {
                ViewBag.QuincenaActualInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaActualFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddDays(15).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
                ViewBag.QuincenaPasadaInicio = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, 15).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaPasadaFin = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month)).ToString("MM/dd/yyyy") + " 11:59:59 PM";
                ViewBag.QuincenaAntepasadaInicio = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaAntepasadaFin = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, 1).AddDays(15).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            }
            ViewBag.MesActualInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.MesActualFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(1).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.MesPasadoInicioFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.MesPasadoFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-1).AddMonths(1).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.MesAntepasadoInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-2).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.MesAntepasadoFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-2).AddMonths(1).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            return View();
        }

        public PartialViewResult VistaParcialVentaTipoPrecio(int idSucursal, String periodo)
        {
            String[] fecha = Fechas.CalcularIncioFin(periodo);
            String inicio = fecha[0];
            String fin = fecha[1];
            DateTime _inicio = Convert.ToDateTime(inicio, IdiomaFecha);
            DateTime _fin = Convert.ToDateTime(fin, IdiomaFecha);
            List<venta> ListaVentas = new List<venta>();
            List<conceptos_por_venta> ListaConceptos = new List<conceptos_por_venta>();
            List<producto> ListaProductos = new List<producto>();
            ListaVentas = db.venta.Where(x => ((x.catalogo_status_id == 1 && x.Fecha_entrega >= _inicio && x.Fecha_entrega <= _fin)
                                              || (x.catalogo_status_id == 9 && x.fecha_liquidacion >= _inicio && x.fecha_liquidacion <= _fin)
                                              || (x.catalogo_status_id == 10 && x.fecha_liquidacion >= _inicio && x.fecha_liquidacion <= _fin)
                                              || (x.catalogo_status_id == 11 && x.fecha_liquidacion >= _inicio && x.fecha_liquidacion <= _fin))
                                              && x.sucursal_id == idSucursal).OrderBy(x => x.Folio).ToList();
            List<int> ListaIdsVentas = ListaVentas.Select(x => x.id).ToList();
            ListaConceptos = db.conceptos_por_venta.Where(x => ListaIdsVentas.Contains(x.Venta_id)).ToList();
            ListaProductos = ListaConceptos.Select(x => x.producto).Distinct().ToList();
            ViewBag.ListaVentas = ListaVentas;
            ViewBag.ListaConceptos = ListaConceptos;
            ViewBag.ListaProductos = ListaProductos;
            return PartialView();
        }

        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName = "Ver ventas tipo precio")]
        public ActionResult ResumenVenta()
        {
            List<int> ListaSucursalesPermitidas = usuario_logeado.Sucursales_id();
            ViewBag.id_sucursal = new SelectList(db.sucursal.Where(x => ListaSucursalesPermitidas.Contains(x.id)).OrderByDescending(y => y.id), "id", "nombre_sucursal");
            ViewBag.HoyInicio = DateTime.Now.ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.HoyFin = DateTime.Now.ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.AyerInicio = DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.AyerFin = DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.AntierInicio = DateTime.Now.AddDays(-2).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.AntierFin = DateTime.Now.AddDays(-2).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.SemanaActualInicio = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.SemanaActualFin = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(6).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.SemanaPasadaInicio = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(-7).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.SemanaPasadaFin = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(-7).AddDays(6).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.SemanaAntepasadaInicio = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(-14).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.SemanaAntepasadaFin = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(-14).AddDays(6).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            DateTime hoy = Convert.ToDateTime(DateTime.Now.ToShortDateString());
            int numero_de_dia = hoy.Day;
            if (numero_de_dia > 15)
            //segunda quincena
            {
                ViewBag.QuincenaActualInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 15).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaActualFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.Month)).ToString("MM/dd/yyyy") + " 11:59:59 PM";
                ViewBag.QuincenaPasadaInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaPasadaFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddDays(15).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
                ViewBag.QuincenaAntepasadaInicio = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, 15).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaAntepasadaFin = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month)).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            }
            else //primera quincena
            {
                ViewBag.QuincenaActualInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaActualFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddDays(15).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
                ViewBag.QuincenaPasadaInicio = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, 15).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaPasadaFin = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month)).ToString("MM/dd/yyyy") + " 11:59:59 PM";
                ViewBag.QuincenaAntepasadaInicio = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaAntepasadaFin = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, 1).AddDays(15).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            }
            ViewBag.MesActualInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.MesActualFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(1).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.MesPasadoInicioFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.MesPasadoFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-1).AddMonths(1).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.MesAntepasadoInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-2).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.MesAntepasadoFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-2).AddMonths(1).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            return View();
        }

        public PartialViewResult VistaParcialResumenVenta(int idSucursal, String periodo)
        {
            String[] fecha = Fechas.CalcularIncioFin(periodo);
            String inicio = fecha[0];
            String fin = fecha[1];
            usuario_punto_venta u = usuario_logeado.traerUsuario();
            List<venta> ventas = new List<venta>();
            DateTime _inicio = Convert.ToDateTime(inicio, IdiomaFecha);
            DateTime _fin = Convert.ToDateTime(fin, IdiomaFecha);
            ventas = db.venta.Where(x => ((x.catalogo_status_id == 1 && x.Fecha_entrega >= _inicio && x.Fecha_entrega <= _fin)
                                              || (x.catalogo_status_id == 9 && x.fecha_liquidacion >= _inicio && x.fecha_liquidacion <= _fin)
                                              || (x.catalogo_status_id == 10 && x.fecha_liquidacion >= _inicio && x.fecha_liquidacion <= _fin)
                                              || (x.catalogo_status_id == 11 && x.fecha_liquidacion >= _inicio && x.fecha_liquidacion <= _fin))
                                              && x.sucursal_id == idSucursal).OrderBy(x => x.Folio).ToList();
            List<int> listaIDventa = ventas.Select(x => x.id).ToList();
            List<conceptos_por_venta> lista_conceptos = db.conceptos_por_venta.Where(x => listaIDventa.Contains(x.Venta_id)).ToList();
            List<producto> productos = lista_conceptos.Select(x => x.producto).Distinct().ToList();
            List<conceptos> ListaConceptos = new List<conceptos>();
            foreach (producto p in productos)
            {
                //tipos de precio por producto
                foreach (tipocliente tprecio in lista_conceptos.Where(x => x.Producto_id == p.id).Select(x => x.tipocliente).Distinct().ToList())
                {
                    //agrupar totales
                    List<conceptos_por_venta> temp_lista_conceptos = lista_conceptos.Where(x => x.Producto_id == p.id && x.TipoCliente_id == tprecio.id).ToList();
                    conceptos c = new conceptos();
                    c.cantidad = temp_lista_conceptos.Select(x => x.cantidad).Sum();
                    c.codigo = p.Codigo;
                    c.descripcion = p.Descripcion;
                    c.precio_unitario_mas_iva = temp_lista_conceptos.First().precio_unitario_mas_iva.Value;
                    c.importe_mas_iva = temp_lista_conceptos.Select(x => x.importe_mas_iva.Value).Sum();
                    ListaConceptos.Add(c);
                }
            }
            ViewBag.ListaConceptos = ListaConceptos;
            return PartialView();
        }

        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName = "Historial De Ventas Por Consignacion")]
        public ActionResult Consignacion()
        {
            List<int> ListaSucursalesPermitidas = usuario_logeado.Sucursales_id();
            ViewBag.id_sucursal = new SelectList(db.sucursal.Where(x => ListaSucursalesPermitidas.Contains(x.id)).OrderByDescending(y => y.id), "id", "nombre_sucursal");
            ViewBag.id_proveedor = new SelectList(db.proveedor.OrderBy(x => x.nombre_proveedor), "id", "nombre_proveedor");
            ViewBag.id_familia = new SelectList(db.familia.OrderBy(x => x.nombre), "id", "nombre");
            ViewBag.HoyInicio = DateTime.Now.ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.HoyFin = DateTime.Now.ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.AyerInicio = DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.AyerFin = DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.AntierInicio = DateTime.Now.AddDays(-2).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.AntierFin = DateTime.Now.AddDays(-2).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.SemanaActualInicio = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.SemanaActualFin = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(6).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.SemanaPasadaInicio = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(-7).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.SemanaPasadaFin = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(-7).AddDays(6).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.SemanaAntepasadaInicio = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(-14).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.SemanaAntepasadaFin = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(-14).AddDays(6).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            DateTime hoy = Convert.ToDateTime(DateTime.Now.ToShortDateString());
            int numero_de_dia = hoy.Day;
            if (numero_de_dia > 15)
            //segunda quincena
            {
                ViewBag.QuincenaActualInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 15).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaActualFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.Month)).ToString("MM/dd/yyyy") + " 11:59:59 PM";
                ViewBag.QuincenaPasadaInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaPasadaFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddDays(15).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
                ViewBag.QuincenaAntepasadaInicio = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, 15).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaAntepasadaFin = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month)).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            }
            else //primera quincena
            {
                ViewBag.QuincenaActualInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaActualFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddDays(15).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
                ViewBag.QuincenaPasadaInicio = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, 15).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaPasadaFin = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month)).ToString("MM/dd/yyyy") + " 11:59:59 PM";
                ViewBag.QuincenaAntepasadaInicio = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaAntepasadaFin = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, 1).AddDays(15).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            }
            ViewBag.MesActualInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.MesActualFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(1).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.MesPasadoInicioFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.MesPasadoFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-1).AddMonths(1).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.MesAntepasadoInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-2).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.MesAntepasadoFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-2).AddMonths(1).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            return View();
        }

        public PartialViewResult VistaParcialConsignacion(int id_sucursal, int id_proveedor, int id_familia, int id_estatus, String periodo)
        {
            configuracion_general configuracion_general = Configuracion.TraerConfiguracionGeneral();
            String[] fecha = Fn.Fechas.CalcularIncioFin(periodo);
            String inicio = fecha[0];
            String fin = fecha[1];
            usuario_punto_venta u = usuario_logeado.traerUsuario();
            List<venta> ListaVentas = new List<venta>();
            List<conceptos_por_venta> ListaConceptos = new List<conceptos_por_venta>();
            DateTime _inicio = Convert.ToDateTime(inicio, IdiomaFecha);
            DateTime _fin = Convert.ToDateTime(fin, IdiomaFecha);
            ListaVentas = db.venta.Where(x => ((x.catalogo_status_id == 1 && x.Fecha_entrega >= _inicio && x.Fecha_entrega <= _fin)
                                              || (x.catalogo_status_id == 9 && x.fecha_liquidacion >= _inicio && x.fecha_liquidacion <= _fin)
                                              || (x.catalogo_status_id == 10 && x.fecha_liquidacion >= _inicio && x.fecha_liquidacion <= _fin)
                                              || (x.catalogo_status_id == 11 && x.fecha_liquidacion >= _inicio && x.fecha_liquidacion <= _fin))).ToList();
            if (id_sucursal != 0)
            {
                ListaVentas = ListaVentas.Where(x => x.sucursal_id == id_sucursal).ToList();
            }
            List<int> ListaIdsVentas = ListaVentas.Select(y => y.id).ToList();
            ListaConceptos = db.conceptos_por_venta.Where(x => ListaIdsVentas.Contains(x.Venta_id)).ToList();
            ListaConceptos = ListaConceptos.Where(x => x.producto.consignacion == 1).ToList();
            if (id_proveedor != 0)
            {
                ListaConceptos = ListaConceptos.Where(x => x.producto.Proveedor_id == id_proveedor).ToList();
            }
            if (id_familia != 0)
            {
                ListaConceptos = ListaConceptos.Where(x => x.producto.subfamilia.Familia_id == id_familia).ToList();
            }
            ListaConceptos = ListaConceptos.Where(x => x.pagada_consignacion != null).ToList();
            if (id_estatus == 1)
            {
                ListaConceptos = ListaConceptos.Where(x => x.pagada_consignacion == false).ToList();
            }
            else if (id_estatus == 2)
            {
                ListaConceptos = ListaConceptos.Where(x => x.pagada_consignacion == true).ToList();
            }
            Decimal TotalPagado = 0;
            Decimal TotalDeuda = 0;
            Decimal TotalVendido = 0;
            foreach (conceptos_por_venta concepto_por_venta in ListaConceptos)
            {
                if (concepto_por_venta.producto.costo != null)
                {
                    Decimal total_concepto = 0;
                    total_concepto = Math.Round(concepto_por_venta.cantidad * concepto_por_venta.producto.costo.Value, 2);
                    TotalVendido += total_concepto;
                    if (concepto_por_venta.pagada_consignacion == true)
                    {
                        TotalPagado += total_concepto;
                    }
                    else
                    {
                        TotalDeuda += total_concepto;
                    }
                }
            }
            ViewBag.TotalPagado = TotalPagado;
            ViewBag.TotalDeuda = TotalDeuda;
            ViewBag.TotalVendido = TotalVendido;
            ViewBag.ListaVentas = ListaVentas;
            ViewBag.ListaConceptos = ListaConceptos;
            return PartialView();
        }

        public JsonResult TraerTotalesFiltroConsignacion(List<int> lista_id_ventas)
       {
            if (lista_id_ventas != null)
            {
                List<conceptos_por_venta> ListaConceptos = new List<conceptos_por_venta>();
                ListaConceptos = db.conceptos_por_venta.Where(x => lista_id_ventas.Contains(x.id)).ToList();
                ListaConceptos = ListaConceptos.Where(x => x.producto.consignacion == 1).ToList();
                ListaConceptos = ListaConceptos.Where(x => x.pagada_consignacion != null).ToList();

                Decimal TotalPagado = 0;
                Decimal TotalDeuda = 0;
                Decimal TotalVendido = 0;
                foreach (conceptos_por_venta concepto_por_venta in ListaConceptos)
                {
                    if (concepto_por_venta.producto.costo != null)
                    {
                        Decimal total_concepto = 0;
                        total_concepto = Math.Round(concepto_por_venta.cantidad * concepto_por_venta.producto.costo.Value, 2);
                        TotalVendido += total_concepto;
                        if (concepto_por_venta.pagada_consignacion == true)
                        {
                            TotalPagado += total_concepto;
                        }
                        else
                        {
                            TotalDeuda += total_concepto;
                        }
                    }
                }
                ViewBag.TotalPagado = TotalPagado;
                ViewBag.TotalDeuda = TotalDeuda;
                ViewBag.TotalVendido = TotalVendido;
                var respuesta = new
                {
                    success = true,
                    TotalPagado = TotalPagado,
                    TotalDeuda = TotalDeuda,
                    TotalVendido = TotalVendido

                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var respuesta = new
                {
                    success = false,
                    TotalPagado = 0,
                    TotalDeuda = 0,
                    TotalVendido = 0

                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
        }


        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName = "Historial De Ventas Por Consignacion Por Proveedor")]
        public ActionResult PoolConsignacion()
        {
            List<int> ListaSucursalesPermitidas = usuario_logeado.Sucursales_id();
            ViewBag.id_sucursal = new SelectList(db.sucursal.Where(x => ListaSucursalesPermitidas.Contains(x.id)).OrderByDescending(y => y.id), "id", "nombre_sucursal");
            ViewBag.id_proveedor = new SelectList(db.proveedor.OrderBy(x => x.nombre_proveedor), "id", "nombre_proveedor");
            ViewBag.id_familia = new SelectList(db.familia.OrderBy(x => x.nombre), "id", "nombre");
            ViewBag.HoyInicio = DateTime.Now.ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.HoyFin = DateTime.Now.ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.AyerInicio = DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.AyerFin = DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.AntierInicio = DateTime.Now.AddDays(-2).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.AntierFin = DateTime.Now.AddDays(-2).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.SemanaActualInicio = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.SemanaActualFin = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(6).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.SemanaPasadaInicio = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(-7).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.SemanaPasadaFin = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(-7).AddDays(6).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.SemanaAntepasadaInicio = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(-14).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.SemanaAntepasadaFin = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(-14).AddDays(6).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            DateTime hoy = Convert.ToDateTime(DateTime.Now.ToShortDateString());
            int numero_de_dia = hoy.Day;
            if (numero_de_dia > 15)
            //segunda quincena
            {
                ViewBag.QuincenaActualInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 15).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaActualFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.Month)).ToString("MM/dd/yyyy") + " 11:59:59 PM";
                ViewBag.QuincenaPasadaInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaPasadaFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddDays(15).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
                ViewBag.QuincenaAntepasadaInicio = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, 15).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaAntepasadaFin = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month)).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            }
            else //primera quincena
            {
                ViewBag.QuincenaActualInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaActualFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddDays(15).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
                ViewBag.QuincenaPasadaInicio = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, 15).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaPasadaFin = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month)).ToString("MM/dd/yyyy") + " 11:59:59 PM";
                ViewBag.QuincenaAntepasadaInicio = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaAntepasadaFin = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, 1).AddDays(15).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            }
            ViewBag.MesActualInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.MesActualFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(1).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.MesPasadoInicioFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.MesPasadoFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-1).AddMonths(1).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.MesAntepasadoInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-2).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.MesAntepasadoFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-2).AddMonths(1).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            return View();
        }

        public PartialViewResult VistaParcialPoolConsignacion(int id_sucursal, int id_proveedor, int id_familia, int id_estatus, String periodo)
        {
            configuracion_general configuracion_general = Configuracion.TraerConfiguracionGeneral();
            String[] fecha = Fn.Fechas.CalcularIncioFin(periodo);
            String inicio = fecha[0];
            String fin = fecha[1];
            usuario_punto_venta u = usuario_logeado.traerUsuario();
            List<venta> ListaVentas = new List<venta>();
            List<conceptos_por_venta> ListaConceptos = new List<conceptos_por_venta>();
            List<proveedor> ListaProveedores = new List<proveedor>();
            DateTime _inicio = Convert.ToDateTime(inicio, IdiomaFecha);
            DateTime _fin = Convert.ToDateTime(fin, IdiomaFecha);
            ListaVentas = db.venta.Where(x => ((x.catalogo_status_id == 1 && x.Fecha_entrega >= _inicio && x.Fecha_entrega <= _fin)
                                              || (x.catalogo_status_id == 9 && x.fecha_liquidacion >= _inicio && x.fecha_liquidacion <= _fin)
                                              || (x.catalogo_status_id == 10 && x.fecha_liquidacion >= _inicio && x.fecha_liquidacion <= _fin)
                                              || (x.catalogo_status_id == 11 && x.fecha_liquidacion >= _inicio && x.fecha_liquidacion <= _fin))).ToList();
            if (id_sucursal != 0)
            {
                ListaVentas = ListaVentas.Where(x => x.sucursal_id == id_sucursal).ToList();
            }
            List<int> ListaIdsVentas = ListaVentas.Select(y => y.id).ToList();
            ListaConceptos = db.conceptos_por_venta.Where(x => ListaIdsVentas.Contains(x.Venta_id)).ToList();
            ListaConceptos = ListaConceptos.Where(x => x.producto.consignacion == 1).ToList();
            if (id_proveedor != 0)
            {
                ListaConceptos = ListaConceptos.Where(x => x.producto.Proveedor_id == id_proveedor).ToList();
            }
            if (id_familia != 0)
            {
                ListaConceptos = ListaConceptos.Where(x => x.producto.subfamilia.Familia_id == id_familia).ToList();
            }
            ListaConceptos = ListaConceptos.Where(x => x.pagada_consignacion != null).ToList();
            if (id_estatus == 1)
            {
                ListaConceptos = ListaConceptos.Where(x => x.pagada_consignacion == false).ToList();
            }
            else if (id_estatus == 2)
            {
                ListaConceptos = ListaConceptos.Where(x => x.pagada_consignacion == true).ToList();
            }
            ListaProveedores = ListaConceptos.Select(x => x.producto.proveedor).Distinct().ToList();
            ViewBag.ListaVentas = ListaVentas;
            ViewBag.ListaConceptos = ListaConceptos;
            ViewBag.ListaProveedores = ListaProveedores;
            return PartialView();
        }

        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName = "Historial De Ventas Por Consignacion")]
        public ActionResult InventarioConsignacion()
        {
            List<int> ListaSucursalesPermitidas = usuario_logeado.Sucursales_id();
            ViewBag.id_sucursal = new SelectList(db.sucursal.Where(x => ListaSucursalesPermitidas.Contains(x.id)).OrderByDescending(y => y.id), "id", "nombre_sucursal");
            ViewBag.id_proveedor = new SelectList(db.proveedor.OrderBy(x => x.nombre_proveedor), "id", "nombre_proveedor");
            ViewBag.id_familia = new SelectList(db.familia.OrderBy(x => x.nombre), "id", "nombre");
            return View();
        }

        public PartialViewResult VistaParcialInventarioConsignacion(int id_sucursal, int id_proveedor, int id_familia, bool con_inventario)
        {
            configuracion_general configuracion_general = Configuracion.TraerConfiguracionGeneral();
            usuario_punto_venta u = usuario_logeado.traerUsuario();
            List<inventario> ListaInventario = new List<inventario>();
            if (con_inventario)
                ListaInventario = db.inventario.Where(x => x.cantidad > 0).ToList();
            else
                ListaInventario = db.inventario.ToList();
            if (id_sucursal != 0)
                ListaInventario = ListaInventario.Where(x => x.sucursal_id == id_sucursal).ToList();
            if (id_proveedor != 0 || id_familia != 0) {
                List<producto> ListaProductos = db.producto.ToList();
                if (id_proveedor != 0)
                {
                    ListaProductos = ListaProductos.Where(x => x.Proveedor_id == id_proveedor).ToList();
                }
                if (id_familia != 0)
                {
                    ListaProductos = ListaProductos.Where(x => x.subfamilia.Familia_id == id_familia).ToList();
                }
                List<int> ListaIdsProductos = ListaProductos.Select(y => y.id).ToList();
                ListaInventario = ListaInventario.Where(x => ListaIdsProductos.Contains(x.Producto_id)).ToList();
            }
            ViewBag.ListaInventario = ListaInventario;
            return PartialView();
        }

        public ActionResult PagarConcepto(int id_concepto_por_venta)
        {
            try
            {
                conceptos_por_venta conceptos_por_venta = db.conceptos_por_venta.Find(id_concepto_por_venta);
                conceptos_por_venta.pagada_consignacion = true;
                conceptos_por_venta.fecha_pagada_consignacion = DateTime.Now;
                conceptos_por_venta.id_usuario_pago_consignacion = usuario_logeado.traerUsuario().id;
                db.Entry(conceptos_por_venta).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                var respuesta = new
                {
                    success = true
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                var respuesta = new
                {
                    success = false
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult PagarListaConceptos(List<int> ListaConceptos)
        {
            try
            {
                foreach (int id_concepto_por_venta in ListaConceptos)
                {
                    conceptos_por_venta conceptos_por_venta = db.conceptos_por_venta.Find(id_concepto_por_venta);
                    conceptos_por_venta.pagada_consignacion = true;
                    conceptos_por_venta.fecha_pagada_consignacion = DateTime.Now;
                    conceptos_por_venta.id_usuario_pago_consignacion = usuario_logeado.traerUsuario().id;
                    db.Entry(conceptos_por_venta).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
                var respuesta = new
                {
                    success = true
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                var respuesta = new
                {
                    success = false
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
        }

        public PartialViewResult ModalReporteVentaConsignacion(int id_sucursal, String periodo, int id_proveedor, int id_estatus, int id_familia)
        {
            configuracion_general configuracion_general = Configuracion.TraerConfiguracionGeneral();
            String[] fecha = Fn.Fechas.CalcularIncioFin(periodo);
            String inicio = fecha[0];
            String fin = fecha[1];
            usuario_punto_venta u = usuario_logeado.traerUsuario();
            List<venta> ListaVentas = new List<venta>();
            List<conceptos_por_venta> ListaConceptos = new List<conceptos_por_venta>();
            List<proveedor> ListaProveedores = new List<proveedor>();
            DateTime _inicio = Convert.ToDateTime(inicio, IdiomaFecha);
            DateTime _fin = Convert.ToDateTime(fin, IdiomaFecha);
            ListaVentas = db.venta.Where(x => ((x.catalogo_status_id == 1 && x.Fecha_entrega >= _inicio && x.Fecha_entrega <= _fin)
                                              || (x.catalogo_status_id == 9 && x.fecha_liquidacion >= _inicio && x.fecha_liquidacion <= _fin)
                                              || (x.catalogo_status_id == 10 && x.fecha_liquidacion >= _inicio && x.fecha_liquidacion <= _fin)
                                              || (x.catalogo_status_id == 11 && x.fecha_liquidacion >= _inicio && x.fecha_liquidacion <= _fin))).ToList();
            if (id_sucursal != 0)
            {
                ListaVentas = ListaVentas.Where(x => x.sucursal_id == id_sucursal).ToList();
            }
            List<int> ListaIdsVentas = ListaVentas.Select(y => y.id).ToList();
            ListaConceptos = db.conceptos_por_venta.Where(x => ListaIdsVentas.Contains(x.Venta_id)).ToList();
            ListaConceptos = ListaConceptos.Where(x => x.producto.consignacion == 1).ToList();
            if (id_proveedor != 0)
            {
                ListaConceptos = ListaConceptos.Where(x => x.producto.Proveedor_id == id_proveedor).ToList();
            }
            if (id_familia != 0)
            {
                ListaConceptos = ListaConceptos.Where(x => x.producto.subfamilia.Familia_id == id_familia).ToList();
            }
            ListaConceptos = ListaConceptos.Where(x => x.pagada_consignacion != null).ToList();
            if (id_estatus == 1)
            {
                ListaConceptos = ListaConceptos.Where(x => x.pagada_consignacion == false).ToList();
            }
            else if (id_estatus == 2)
            {
                ListaConceptos = ListaConceptos.Where(x => x.pagada_consignacion == true).ToList();
            }
            ListaProveedores = ListaConceptos.Select(x => x.producto.proveedor).Distinct().ToList();
            ViewBag.ListaVentas = ListaVentas;
            ViewBag.ListaConceptos = ListaConceptos;
            ViewBag.ListaProveedores = ListaProveedores;
            return PartialView();
        }
        public PartialViewResult ModalReporteInventarioConsignacion(int id_sucursal, int id_proveedor, int id_familia, bool con_inventario)
        {
            configuracion_general configuracion_general = Configuracion.TraerConfiguracionGeneral();
            usuario_punto_venta u = usuario_logeado.traerUsuario();
            List<inventario> ListaInventario = new List<inventario>();
            if (con_inventario)
                ListaInventario = db.inventario.Where(x => x.cantidad > 0).ToList();
            else
                ListaInventario = db.inventario.ToList();
            if (id_sucursal != 0)
                ListaInventario = ListaInventario.Where(x => x.sucursal_id == id_sucursal).ToList();
            if (id_proveedor != 0 || id_familia != 0)
            {
                List<producto> ListaProductos = db.producto.ToList();
                if (id_proveedor != 0)
                {
                    ListaProductos = ListaProductos.Where(x => x.Proveedor_id == id_proveedor).ToList();
                }
                if (id_familia != 0)
                {
                    ListaProductos = ListaProductos.Where(x => x.subfamilia.Familia_id == id_familia).ToList();
                }
                List<int> ListaIdsProductos = ListaProductos.Select(y => y.id).ToList();
                ListaInventario = ListaInventario.Where(x => ListaIdsProductos.Contains(x.Producto_id)).ToList();
            }
            List<proveedor> ListaProveedores = ListaInventario.Select(X => X.producto.proveedor).Distinct().ToList();
            ViewBag.ListaInventario = ListaInventario;
            ViewBag.ListaProveedores = ListaProveedores;
            return PartialView();
        }

        public PartialViewResult ModalArchivosAdjuntos(int id)
        {
            pdf_venta archivo = db.pdf_venta.Find(id);
            ViewBag.id_venta = archivo.id_venta;
            return PartialView(archivo);
        }

        public PartialViewResult ModalCancelarVenta(int id, String mensaje)
        {
            ViewBag.id = id;
            ViewBag.mensaje = mensaje;
            return PartialView();
        }

        public PartialViewResult ModalConceptosVenta(int id_venta)
        {
            int hoy = DateTime.Now.DayOfYear;
            venta venta = db.venta.Find(id_venta);
            int dia_venta = venta.fecha.Value.DayOfYear;
            if (hoy == dia_venta)
                ViewBag.editar = true;
            else
                ViewBag.editar = false;
            Decimal total_descuento = 0;
            Decimal total_porcentaje_descuento = 0;
            List<descuento> lista_descuentos = db.descuento.Where(x => x.id_venta == id_venta).ToList();
            if (lista_descuentos != null)
            {
                foreach (descuento descuento_individual in lista_descuentos)
                {
                    total_descuento += descuento_individual.monto;
                    total_porcentaje_descuento += (Decimal)descuento_individual.porcentaje;
                }
            }

            ViewBag.TiposPago = db.tipo_pago_venta.ToList();
            ViewBag.venta = venta;
            ViewBag.total_descuento = total_descuento;
            ViewBag.total_porcentaje_descuento = total_porcentaje_descuento;
            return PartialView();
        }
        public PartialViewResult ModalDetalleVenta(int id)
        {
            int hoy = DateTime.Now.DayOfYear;
            venta venta = db.venta.Find(id);
            int dia_venta = venta.fecha.Value.DayOfYear;
            if (hoy == dia_venta)
                ViewBag.EditarTiposPago = true;
            else
                ViewBag.EditarTiposPago = false;
            ViewBag.TiposPago = db.tipo_pago_venta.ToList();
            ViewBag.Venta = venta;
            return PartialView();
        }

        public ActionResult CancelarVenta(int id, string motivo)
        {
            bool success = false;
            int[] ListaNotaCredito = new int[1000];
            int index_lista_nota_credito = 0;
            venta venta = db.venta.Find(id);
            try
            {
                if (venta.catalogo_status_id != 2)
                {
                    List<nota_credito> ListaNotasCreditoGeneradasPorVenta = db.nota_credito.Where(x => x.id_venta == id).ToList();
                    List<nota_credito> ListaNotasCreditoAplicadasPorVenta = db.nota_credito.Where(x => x.id_venta_aplicada == id).ToList();
                    int nc_aplicadas_a_esta_venta = ListaNotasCreditoAplicadasPorVenta.Count;
                    int nc_generadas_disponibles = ListaNotasCreditoGeneradasPorVenta.Where(x => x.id_estatus_nota_credito == 1).ToList().Count;
                    int nc_generadas_aplicadas = ListaNotasCreditoGeneradasPorVenta.Where(x => x.id_estatus_nota_credito == 2).ToList().Count;
                    if (nc_aplicadas_a_esta_venta == 0)
                    {
                        if (nc_generadas_aplicadas > 0 && nc_generadas_disponibles > 0)
                        {
                            Decimal cantidad = 0;
                            List<int> ListaIdsNotasCreditos = venta.nota_credito.Where(x => x.id_estatus_nota_credito == 1 || x.id_estatus_nota_credito == 2).Select(x => x.id).ToList();
                            List<conceptos_por_venta> ListaConceptosVentaRestantes = venta.conceptos_por_venta.ToList();
                            List<concepto_por_nota_credito> ListaConceptosNotasCreditos = db.concepto_por_nota_credito.Where(x => ListaIdsNotasCreditos.Contains(x.id_nota_credito)).ToList();
                            movimiento_inventario movimiento_inventario = new movimiento_inventario();
                            movimiento_inventario.fecha = DateTime.Now;
                            movimiento_inventario.recibe = venta.sucursal.nombre_sucursal;// Fn.usuario_logeado.traerUsuario().sucursal.nombre_sucursal;
                            movimiento_inventario.usuario_valida_id = Fn.usuario_logeado.traerUsuario().id;
                            movimiento_inventario.tipo_movimiento_inventario_id = 5;
                            movimiento_inventario.sucursal_id = venta.sucursal_id;
                            db.movimiento_inventario.Add(movimiento_inventario);
                            db.SaveChanges();
                            foreach (conceptos_por_venta conceptos_por_venta in ListaConceptosVentaRestantes)
                            {
                                foreach (concepto_por_nota_credito concepto_por_nota_credito1 in ListaConceptosNotasCreditos)
                                {
                                    if (conceptos_por_venta.id == concepto_por_nota_credito1.id_conceptos_por_venta)
                                        conceptos_por_venta.cantidad -= concepto_por_nota_credito1.cantidad.Value;
                                }
                                inventario inventario = db.inventario.Where(x => x.Producto_id == conceptos_por_venta.Producto_id && x.sucursal_id == venta.sucursal_id).First();
                                inventario.cantidad = inventario.cantidad + conceptos_por_venta.cantidad;
                                db.Entry(inventario).State = System.Data.Entity.EntityState.Modified;
                                conceptos_por_movimiento_inventario conceptos_por_movimiento_inventario = new conceptos_por_movimiento_inventario();
                                conceptos_por_movimiento_inventario.movimiento_inventario_id = movimiento_inventario.id;
                                conceptos_por_movimiento_inventario.Inventario_id = inventario.id;
                                conceptos_por_movimiento_inventario.cantidad = conceptos_por_venta.cantidad;
                                db.conceptos_por_movimiento_inventario.Add(conceptos_por_movimiento_inventario);
                                cantidad = cantidad + ((conceptos_por_venta.cantidad * conceptos_por_venta.precio_unitario_mas_iva.Value));
                                foreach (concepto_por_nota_credito concepto_por_nota_credito1 in ListaConceptosNotasCreditos)
                                {
                                    if (conceptos_por_venta.id == concepto_por_nota_credito1.id_conceptos_por_venta)
                                        conceptos_por_venta.cantidad += concepto_por_nota_credito1.cantidad.Value;
                                }
                                db.SaveChanges();
                            }
                            nota_credito nota_credito = new nota_credito();
                            string Serie_de_nota_de_credito = Config.Serie_de_nota_de_credito();
                            nota_credito.fecha_creacion = DateTime.Now;
                            nota_credito.id_estatus_nota_credito = 1;
                            nota_credito.serie = Config.Serie_de_nota_de_credito();
                            nota_credito.id_sucursal = venta.sucursal_id;
                            nota_credito.id_venta = venta.id;
                            nota_credito.folio = Convert.ToInt32(db.nota_credito.Where(x => x.serie.Equals(Serie_de_nota_de_credito) && x.venta.sucursal_id == nota_credito.id_sucursal).Select(x => x.folio).Max()) + 1;
                            nota_credito.cantidad = cantidad;
                            nota_credito.id_usuario_punto_venta = Fn.usuario_logeado.traerUsuario().id;
                            db.nota_credito.Add(nota_credito);
                            db.SaveChanges();
                            foreach (conceptos_por_venta conceptos_por_venta in ListaConceptosVentaRestantes)
                            {
                                foreach (concepto_por_nota_credito concepto_por_nota_credito1 in ListaConceptosNotasCreditos)
                                {
                                    if (conceptos_por_venta.id == concepto_por_nota_credito1.id_conceptos_por_venta)
                                        conceptos_por_venta.cantidad -= concepto_por_nota_credito1.cantidad.Value;
                                }
                                concepto_por_nota_credito concepto_por_nota_credito = new concepto_por_nota_credito();
                                concepto_por_nota_credito.id_nota_credito = nota_credito.id;
                                concepto_por_nota_credito.id_conceptos_por_venta = conceptos_por_venta.id;
                                concepto_por_nota_credito.cantidad = conceptos_por_venta.cantidad;
                                db.concepto_por_nota_credito.Add(concepto_por_nota_credito);
                                foreach (concepto_por_nota_credito concepto_por_nota_credito1 in ListaConceptosNotasCreditos)
                                {
                                    if (conceptos_por_venta.id == concepto_por_nota_credito1.id_conceptos_por_venta)
                                        conceptos_por_venta.cantidad += concepto_por_nota_credito1.cantidad.Value;
                                }
                            }
                            db.SaveChanges();
                            ListaNotaCredito[index_lista_nota_credito++] = nota_credito.id;
                            //mensaje = "La venta " + serie_folio + " tiene notas de credito que ya han sido aplicadas, por lo que no se puede cancelar la venta. Se generara una nota de credito por el resto de la venta. ¿Quiere continuar?";
                        }
                        else if (nc_generadas_aplicadas > 0)
                        {
                            Decimal cantidad = 0;
                            List<int> ListaIdsNotasCreditos = venta.nota_credito.Where(x => x.id_estatus_nota_credito == 1 || x.id_estatus_nota_credito == 2).Select(x => x.id).ToList();
                            List<conceptos_por_venta> ListaConceptosVentaRestantes = venta.conceptos_por_venta.ToList();
                            List<concepto_por_nota_credito> ListaConceptosNotasCreditos = db.concepto_por_nota_credito.Where(x => ListaIdsNotasCreditos.Contains(x.id_nota_credito)).ToList();
                            movimiento_inventario movimiento_inventario = new movimiento_inventario();
                            movimiento_inventario.fecha = DateTime.Now;
                            movimiento_inventario.recibe = venta.sucursal.nombre_sucursal;
                            movimiento_inventario.usuario_valida_id = Fn.usuario_logeado.traerUsuario().id;
                            movimiento_inventario.tipo_movimiento_inventario_id = 5;
                            movimiento_inventario.sucursal_id = venta.sucursal_id;
                            db.movimiento_inventario.Add(movimiento_inventario);
                            db.SaveChanges();
                            foreach (conceptos_por_venta conceptos_por_venta in ListaConceptosVentaRestantes)
                            {
                                foreach (concepto_por_nota_credito concepto_por_nota_credito1 in ListaConceptosNotasCreditos)
                                {
                                    if (conceptos_por_venta.id == concepto_por_nota_credito1.id_conceptos_por_venta)
                                        conceptos_por_venta.cantidad -= concepto_por_nota_credito1.cantidad.Value;
                                }
                                inventario inventario = db.inventario.Where(x => x.Producto_id == conceptos_por_venta.Producto_id && x.sucursal_id == venta.sucursal_id).First();
                                inventario.cantidad = inventario.cantidad + conceptos_por_venta.cantidad;
                                db.Entry(inventario).State = System.Data.Entity.EntityState.Modified;
                                conceptos_por_movimiento_inventario conceptos_por_movimiento_inventario = new conceptos_por_movimiento_inventario();
                                conceptos_por_movimiento_inventario.movimiento_inventario_id = movimiento_inventario.id;
                                conceptos_por_movimiento_inventario.Inventario_id = inventario.id;
                                conceptos_por_movimiento_inventario.cantidad = conceptos_por_venta.cantidad;
                                db.conceptos_por_movimiento_inventario.Add(conceptos_por_movimiento_inventario);
                                cantidad = cantidad + ((conceptos_por_venta.cantidad * conceptos_por_venta.precio_unitario_mas_iva.Value));
                                db.SaveChanges();
                                foreach (concepto_por_nota_credito concepto_por_nota_credito1 in ListaConceptosNotasCreditos)
                                {
                                    if (conceptos_por_venta.id == concepto_por_nota_credito1.id_conceptos_por_venta)
                                        conceptos_por_venta.cantidad += concepto_por_nota_credito1.cantidad.Value;
                                }
                            }
                            nota_credito nota_credito = new nota_credito();
                            string Serie_de_nota_de_credito = Config.Serie_de_nota_de_credito();
                            nota_credito.fecha_creacion = DateTime.Now;
                            nota_credito.id_estatus_nota_credito = 1;
                            nota_credito.id_venta = venta.id;
                            nota_credito.id_sucursal = venta.sucursal_id;
                            nota_credito.serie = Config.Serie_de_nota_de_credito();
                            nota_credito.folio = Convert.ToInt32(db.nota_credito.Where(x => x.serie.Equals(Serie_de_nota_de_credito) && x.venta.sucursal_id == nota_credito.id_sucursal).Select(x => x.folio).Max()) + 1;
                            nota_credito.cantidad = cantidad;
                            nota_credito.id_usuario_punto_venta = Fn.usuario_logeado.traerUsuario().id;
                            db.nota_credito.Add(nota_credito);
                            db.SaveChanges();
                            foreach (conceptos_por_venta conceptos_por_venta in ListaConceptosVentaRestantes)
                            {
                                foreach (concepto_por_nota_credito concepto_por_nota_credito1 in ListaConceptosNotasCreditos)
                                {
                                    if (conceptos_por_venta.id == concepto_por_nota_credito1.id_conceptos_por_venta)
                                        conceptos_por_venta.cantidad -= concepto_por_nota_credito1.cantidad.Value;
                                }
                                concepto_por_nota_credito concepto_por_nota_credito = new concepto_por_nota_credito();
                                concepto_por_nota_credito.id_nota_credito = nota_credito.id;
                                concepto_por_nota_credito.id_conceptos_por_venta = conceptos_por_venta.id;
                                concepto_por_nota_credito.cantidad = conceptos_por_venta.cantidad;
                                db.concepto_por_nota_credito.Add(concepto_por_nota_credito);
                                foreach (concepto_por_nota_credito concepto_por_nota_credito1 in ListaConceptosNotasCreditos)
                                {
                                    if (conceptos_por_venta.id == concepto_por_nota_credito1.id_conceptos_por_venta)
                                        conceptos_por_venta.cantidad += concepto_por_nota_credito1.cantidad.Value;
                                }
                            }
                            db.SaveChanges();
                            ListaNotaCredito[index_lista_nota_credito++] = nota_credito.id;
                            //mensaje = "La venta " + serie_folio + " tiene notas de credito que ya han sido aplicadas, por lo que no se puede cancelar la venta. Se generara una nota de credito por el resto de la venta. ¿Quiere continuar?";
                        }
                        else if (nc_generadas_disponibles > 0)
                        {
                            foreach (nota_credito nota_credito in venta.nota_credito.Where(x => x.id_estatus_nota_credito == 1).ToList())
                            {
                                nota_credito.fecha_cancelacion = DateTime.Now;
                                nota_credito.id_estatus_nota_credito = 3;
                                db.Entry(nota_credito).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                                ListaNotaCredito[index_lista_nota_credito++] = nota_credito.id;
                                movimiento_inventario movimiento_inventario = new movimiento_inventario();
                                movimiento_inventario.fecha = DateTime.Now;
                                movimiento_inventario.recibe = venta.cliente.Razon_social;
                                movimiento_inventario.usuario_valida_id = Fn.usuario_logeado.traerUsuario().id;
                                movimiento_inventario.tipo_movimiento_inventario_id = 6;
                                movimiento_inventario.sucursal_id = venta.sucursal_id;
                                db.movimiento_inventario.Add(movimiento_inventario);
                                db.SaveChanges();
                                List<concepto_por_nota_credito> ListaConceptosNotaCredito = db.concepto_por_nota_credito.Where(x => x.id_nota_credito == nota_credito.id).ToList();
                                foreach (concepto_por_nota_credito concepto_por_nota_credito in ListaConceptosNotaCredito)
                                {
                                    conceptos_por_venta concepto_venta = db.conceptos_por_venta.Find(concepto_por_nota_credito.id_conceptos_por_venta);
                                    inventario inventario = db.inventario.Where(x => x.Producto_id == concepto_venta.Producto_id && x.sucursal_id == venta.sucursal_id).First();
                                    inventario.cantidad = inventario.cantidad - concepto_por_nota_credito.cantidad;
                                    db.Entry(inventario).State = System.Data.Entity.EntityState.Modified;
                                    conceptos_por_movimiento_inventario conceptos_por_movimiento_inventario = new conceptos_por_movimiento_inventario();
                                    conceptos_por_movimiento_inventario.movimiento_inventario_id = movimiento_inventario.id;
                                    conceptos_por_movimiento_inventario.Inventario_id = inventario.id;
                                    conceptos_por_movimiento_inventario.cantidad = concepto_por_nota_credito.cantidad.Value;
                                    db.conceptos_por_movimiento_inventario.Add(conceptos_por_movimiento_inventario);
                                }
                                db.SaveChanges();
                            }
                            cliente cliente = db.cliente.Find(venta.Cliente_id);
                            List<abono> listaAbonos = venta.abono.Where(x => x.estatus == true).ToList();
                            foreach (abono a in listaAbonos)
                            {
                                a.estatus = false;
                                a.fecha_cancelacion = DateTime.Now;
                                a.usuario_id_cancela = Fn.usuario_logeado.traerUsuario().id;
                                a.usuario_punto_venta1 = db.usuario_punto_venta.Find(a.usuario_id_cancela);
                                a.motivo_cancelacion = "Cancelacion de venta";
                                cliente.Adeudo_credito = cliente.Adeudo_credito + a.importe;
                                db.Entry(a).State = System.Data.Entity.EntityState.Modified;
                            }
                            db.Entry(cliente).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                            Decimal pagos_credito = venta.multiple_pago.Where(x => x.id_tipo_pago_venta == 4).ToList().Select(y => y.cantidad).Sum().Value;
                            venta.catalogo_status_id = 2;
                            venta.fecha_cancelacion = DateTime.Now;
                            venta.usuario_id_cancela = Fn.usuario_logeado.traerUsuario().id;
                            venta.usuario_punto_venta1 = db.usuario_punto_venta.Find(venta.usuario_id_cancela);
                            venta.Motivo_cancelacion = motivo;
                            cliente.Adeudo_credito = cliente.Adeudo_credito - pagos_credito;
                            venta.aduedo = 0;
                            db.Entry(venta).State = System.Data.Entity.EntityState.Modified;
                            db.Entry(cliente).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                            //crear registro movimiento de inventario
                            movimiento_inventario mi = new movimiento_inventario();
                            mi.fecha = DateTime.Now;
                            mi.recibe = venta.cliente.Razon_social;
                            mi.usuario_valida_id = Fn.usuario_logeado.traerUsuario().id;
                            mi.tipo_movimiento_inventario_id = 3;
                            mi.sucursal_id = venta.sucursal_id;
                            db.movimiento_inventario.Add(mi);
                            foreach (conceptos_por_venta concepto in venta.conceptos_por_venta)
                            {
                                //regresar inventario
                                inventario i = db.inventario.Where(x => x.Producto_id == concepto.Producto_id && x.sucursal_id == venta.sucursal_id).First();
                                i.cantidad = i.cantidad + concepto.cantidad;
                                //agregar entrada de inventario
                                conceptos_por_movimiento_inventario cxm = new conceptos_por_movimiento_inventario();
                                cxm.movimiento_inventario_id = mi.id;
                                cxm.Inventario_id = i.id;
                                cxm.cantidad = concepto.cantidad;
                                db.conceptos_por_movimiento_inventario.Add(cxm);
                            }
                            db.SaveChanges();
                            //mensaje = "La venta " + serie_folio + " tiene notas de credito disponibles, si se cancela la venta tendran que cancelarse estas notas de credito. ¿Quiere continuar?";
                        }
                        else
                        {
                            cliente cliente = db.cliente.Find(venta.Cliente_id);
                            List<abono> listaAbonos = venta.abono.Where(x => x.estatus == true).ToList();
                            foreach (abono a in listaAbonos)
                            {
                                a.estatus = false;
                                a.fecha_cancelacion = DateTime.Now;
                                a.usuario_id_cancela = Fn.usuario_logeado.traerUsuario().id;
                                a.usuario_punto_venta1 = db.usuario_punto_venta.Find(a.usuario_id_cancela);
                                a.motivo_cancelacion = "Cancelacion de venta";
                                cliente.Adeudo_credito = cliente.Adeudo_credito + a.importe;
                                db.Entry(a).State = System.Data.Entity.EntityState.Modified;
                            }
                            db.Entry(cliente).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                            Decimal pagos_credito = venta.multiple_pago.Where(x => x.id_tipo_pago_venta == 4).ToList().Select(y => y.cantidad).Sum().Value;
                            venta.catalogo_status_id = 2;
                            venta.fecha_cancelacion = DateTime.Now;
                            venta.usuario_id_cancela = Fn.usuario_logeado.traerUsuario().id;
                            venta.usuario_punto_venta1 = db.usuario_punto_venta.Find(venta.usuario_id_cancela);
                            venta.Motivo_cancelacion = motivo;
                            cliente.Adeudo_credito = cliente.Adeudo_credito - pagos_credito;
                            venta.aduedo = 0;
                            db.Entry(venta).State = System.Data.Entity.EntityState.Modified;
                            db.Entry(cliente).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                            //crear registro movimiento de inventario
                            movimiento_inventario mi = new movimiento_inventario();
                            mi.fecha = DateTime.Now;
                            mi.recibe = venta.cliente.Razon_social;
                            mi.usuario_valida_id = Fn.usuario_logeado.traerUsuario().id;
                            mi.tipo_movimiento_inventario_id = 3;//3- entrada por cancelación
                            mi.sucursal_id = venta.sucursal_id;
                            db.movimiento_inventario.Add(mi);
                            foreach (conceptos_por_venta concepto in venta.conceptos_por_venta)
                            {
                                //regresar inventario
                                inventario i = db.inventario.Where(x => x.Producto_id == concepto.Producto_id && x.sucursal_id == venta.sucursal_id).First();
                                i.cantidad = i.cantidad + concepto.cantidad;
                                //agregar entrada de inventario
                                conceptos_por_movimiento_inventario cxm = new conceptos_por_movimiento_inventario();
                                cxm.movimiento_inventario_id = mi.id;
                                cxm.Inventario_id = i.id;
                                cxm.cantidad = concepto.cantidad;
                                db.conceptos_por_movimiento_inventario.Add(cxm);
                            }
                            db.SaveChanges();
                            //cancelacion normal
                        }
                    }
                    else if (nc_aplicadas_a_esta_venta > 0)
                    {
                        if (nc_generadas_aplicadas > 0 && nc_generadas_disponibles > 0)
                        {
                            Decimal cantidad = 0;
                            List<int> ListaIdsNotasCreditos = venta.nota_credito.Where(x => x.id_estatus_nota_credito == 1 || x.id_estatus_nota_credito == 2).Select(x => x.id).ToList();
                            List<conceptos_por_venta> ListaConceptosVentaRestantes = venta.conceptos_por_venta.ToList();
                            List<concepto_por_nota_credito> ListaConceptosNotasCreditos = db.concepto_por_nota_credito.Where(x => ListaIdsNotasCreditos.Contains(x.id_nota_credito)).ToList();
                            movimiento_inventario movimiento_inventario = new movimiento_inventario();
                            movimiento_inventario.fecha = DateTime.Now;
                            movimiento_inventario.recibe = venta.sucursal.nombre_sucursal;
                            movimiento_inventario.usuario_valida_id = Fn.usuario_logeado.traerUsuario().id;
                            movimiento_inventario.tipo_movimiento_inventario_id = 5;
                            movimiento_inventario.sucursal_id = venta.sucursal_id;
                            db.movimiento_inventario.Add(movimiento_inventario);
                            db.SaveChanges();
                            foreach (conceptos_por_venta conceptos_por_venta in ListaConceptosVentaRestantes)
                            {
                                foreach (concepto_por_nota_credito concepto_por_nota_credito1 in ListaConceptosNotasCreditos)
                                {
                                    if (conceptos_por_venta.id == concepto_por_nota_credito1.id_conceptos_por_venta)
                                        conceptos_por_venta.cantidad -= concepto_por_nota_credito1.cantidad.Value;
                                }
                                inventario inventario = db.inventario.Where(x => x.Producto_id == conceptos_por_venta.Producto_id && x.sucursal_id == venta.sucursal_id).First();
                                inventario.cantidad = inventario.cantidad + conceptos_por_venta.cantidad;
                                db.Entry(inventario).State = System.Data.Entity.EntityState.Modified;
                                conceptos_por_movimiento_inventario conceptos_por_movimiento_inventario = new conceptos_por_movimiento_inventario();
                                conceptos_por_movimiento_inventario.movimiento_inventario_id = movimiento_inventario.id;
                                conceptos_por_movimiento_inventario.Inventario_id = inventario.id;
                                conceptos_por_movimiento_inventario.cantidad = conceptos_por_venta.cantidad;
                                db.conceptos_por_movimiento_inventario.Add(conceptos_por_movimiento_inventario);
                                cantidad = cantidad + ((conceptos_por_venta.cantidad * conceptos_por_venta.precio_unitario_mas_iva.Value));
                                db.SaveChanges();
                                foreach (concepto_por_nota_credito concepto_por_nota_credito1 in ListaConceptosNotasCreditos)
                                {
                                    if (conceptos_por_venta.id == concepto_por_nota_credito1.id_conceptos_por_venta)
                                        conceptos_por_venta.cantidad += concepto_por_nota_credito1.cantidad.Value;
                                }
                            }
                            nota_credito nota_credito = new nota_credito();
                            string Serie_de_nota_de_credito = Config.Serie_de_nota_de_credito();
                            nota_credito.fecha_creacion = DateTime.Now;
                            nota_credito.id_estatus_nota_credito = 1;
                            nota_credito.serie = Config.Serie_de_nota_de_credito();
                            nota_credito.id_sucursal = venta.sucursal_id;
                            nota_credito.id_venta = venta.id;
                            nota_credito.folio = Convert.ToInt32(db.nota_credito.Where(x => x.serie.Equals(Serie_de_nota_de_credito) && x.venta.sucursal_id == nota_credito.id_sucursal).Select(x => x.folio).Max()) + 1;
                            nota_credito.cantidad = cantidad;
                            nota_credito.id_usuario_punto_venta = Fn.usuario_logeado.traerUsuario().id;
                            db.nota_credito.Add(nota_credito);
                            db.SaveChanges();
                            foreach (conceptos_por_venta conceptos_por_venta in ListaConceptosVentaRestantes)
                            {
                                foreach (concepto_por_nota_credito concepto_por_nota_credito1 in ListaConceptosNotasCreditos)
                                {
                                    if (conceptos_por_venta.id == concepto_por_nota_credito1.id_conceptos_por_venta)
                                        conceptos_por_venta.cantidad -= concepto_por_nota_credito1.cantidad.Value;
                                }
                                concepto_por_nota_credito concepto_por_nota_credito = new concepto_por_nota_credito();
                                concepto_por_nota_credito.id_nota_credito = nota_credito.id;
                                concepto_por_nota_credito.id_conceptos_por_venta = conceptos_por_venta.id;
                                concepto_por_nota_credito.cantidad = conceptos_por_venta.cantidad;
                                db.concepto_por_nota_credito.Add(concepto_por_nota_credito);
                                foreach (concepto_por_nota_credito concepto_por_nota_credito1 in ListaConceptosNotasCreditos)
                                {
                                    if (conceptos_por_venta.id == concepto_por_nota_credito1.id_conceptos_por_venta)
                                        conceptos_por_venta.cantidad += concepto_por_nota_credito1.cantidad.Value;
                                }
                            }
                            ListaNotaCredito[index_lista_nota_credito++] = nota_credito.id;
                            db.SaveChanges();
                            //mensaje = "La venta " + serie_folio + " tiene notas de credito que ya han sido aplicadas, por lo que no se puede cancelar la venta. Se generara una nota de credito por el resto de la venta. ¿Quiere continuar?";
                        }
                        else if (nc_generadas_aplicadas > 0)
                        {
                            Decimal cantidad = 0;
                            List<int> ListaIdsNotasCreditos = venta.nota_credito.Where(x => x.id_estatus_nota_credito == 1 || x.id_estatus_nota_credito == 2).Select(x => x.id).ToList();
                            List<conceptos_por_venta> ListaConceptosVentaRestantes = venta.conceptos_por_venta.ToList();
                            List<concepto_por_nota_credito> ListaConceptosNotasCreditos = db.concepto_por_nota_credito.Where(x => ListaIdsNotasCreditos.Contains(x.id_nota_credito)).ToList();
                            movimiento_inventario movimiento_inventario = new movimiento_inventario();
                            movimiento_inventario.fecha = DateTime.Now;
                            movimiento_inventario.recibe = venta.sucursal.nombre_sucursal;
                            movimiento_inventario.usuario_valida_id = Fn.usuario_logeado.traerUsuario().id;
                            movimiento_inventario.tipo_movimiento_inventario_id = 5;
                            movimiento_inventario.sucursal_id = venta.sucursal_id;
                            db.movimiento_inventario.Add(movimiento_inventario);
                            db.SaveChanges();
                            foreach (conceptos_por_venta conceptos_por_venta in ListaConceptosVentaRestantes)
                            {
                                foreach (concepto_por_nota_credito concepto_por_nota_credito1 in ListaConceptosNotasCreditos)
                                {
                                    if (conceptos_por_venta.id == concepto_por_nota_credito1.id_conceptos_por_venta)
                                        conceptos_por_venta.cantidad -= concepto_por_nota_credito1.cantidad.Value;
                                }
                                inventario inventario = db.inventario.Where(x => x.Producto_id == conceptos_por_venta.Producto_id && x.sucursal_id == venta.sucursal_id).First();
                                inventario.cantidad = inventario.cantidad + conceptos_por_venta.cantidad;
                                db.Entry(inventario).State = System.Data.Entity.EntityState.Modified;
                                conceptos_por_movimiento_inventario conceptos_por_movimiento_inventario = new conceptos_por_movimiento_inventario();
                                conceptos_por_movimiento_inventario.movimiento_inventario_id = movimiento_inventario.id;
                                conceptos_por_movimiento_inventario.Inventario_id = inventario.id;
                                conceptos_por_movimiento_inventario.cantidad = conceptos_por_venta.cantidad;
                                db.conceptos_por_movimiento_inventario.Add(conceptos_por_movimiento_inventario);
                                cantidad = cantidad + ((conceptos_por_venta.cantidad * conceptos_por_venta.precio_unitario_mas_iva.Value));
                                db.SaveChanges();
                                foreach (concepto_por_nota_credito concepto_por_nota_credito1 in ListaConceptosNotasCreditos)
                                {
                                    if (conceptos_por_venta.id == concepto_por_nota_credito1.id_conceptos_por_venta)
                                        conceptos_por_venta.cantidad += concepto_por_nota_credito1.cantidad.Value;
                                }
                            }
                            nota_credito nota_credito = new nota_credito();
                            string Serie_de_nota_de_credito = Config.Serie_de_nota_de_credito();
                            nota_credito.fecha_creacion = DateTime.Now;
                            nota_credito.id_estatus_nota_credito = 1;
                            nota_credito.serie = Config.Serie_de_nota_de_credito();
                            nota_credito.id_sucursal = venta.sucursal_id;
                            nota_credito.id_venta = venta.id;
                            nota_credito.folio = Convert.ToInt32(db.nota_credito.Where(x => x.serie.Equals(Serie_de_nota_de_credito) && x.venta.sucursal_id == nota_credito.id_sucursal).Select(x => x.folio).Max()) + 1;
                            nota_credito.cantidad = cantidad;
                            nota_credito.id_usuario_punto_venta = Fn.usuario_logeado.traerUsuario().id;
                            db.nota_credito.Add(nota_credito);
                            db.SaveChanges();
                            foreach (conceptos_por_venta conceptos_por_venta in ListaConceptosVentaRestantes)
                            {
                                foreach (concepto_por_nota_credito concepto_por_nota_credito1 in ListaConceptosNotasCreditos)
                                {
                                    if (conceptos_por_venta.id == concepto_por_nota_credito1.id_conceptos_por_venta)
                                        conceptos_por_venta.cantidad -= concepto_por_nota_credito1.cantidad.Value;
                                }
                                concepto_por_nota_credito concepto_por_nota_credito = new concepto_por_nota_credito();
                                concepto_por_nota_credito.id_nota_credito = nota_credito.id;
                                concepto_por_nota_credito.id_conceptos_por_venta = conceptos_por_venta.id;
                                concepto_por_nota_credito.cantidad = conceptos_por_venta.cantidad;
                                db.concepto_por_nota_credito.Add(concepto_por_nota_credito);
                                foreach (concepto_por_nota_credito concepto_por_nota_credito1 in ListaConceptosNotasCreditos)
                                {
                                    if (conceptos_por_venta.id == concepto_por_nota_credito1.id_conceptos_por_venta)
                                        conceptos_por_venta.cantidad += concepto_por_nota_credito1.cantidad.Value;
                                }
                            }
                            db.SaveChanges();
                            ListaNotaCredito[index_lista_nota_credito++] = nota_credito.id;
                            //mensaje = "La venta " + serie_folio + " tiene notas de credito que ya han sido aplicadas, por lo que no se puede cancelar la venta. Se generara una nota de credito por el resto de la venta. ¿Quiere continuar?";
                        }
                        else if (nc_generadas_disponibles > 0)
                        {
                            foreach (nota_credito nota_credito in venta.nota_credito.Where(x => x.id_estatus_nota_credito == 1).ToList())
                            {
                                nota_credito.fecha_cancelacion = DateTime.Now;
                                nota_credito.id_estatus_nota_credito = 3;
                                db.Entry(nota_credito).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                                ListaNotaCredito[index_lista_nota_credito++] = nota_credito.id;
                                movimiento_inventario movimiento_inventario = new movimiento_inventario();
                                movimiento_inventario.fecha = DateTime.Now;
                                movimiento_inventario.recibe = venta.cliente.Razon_social;
                                movimiento_inventario.usuario_valida_id = Fn.usuario_logeado.traerUsuario().id;
                                movimiento_inventario.tipo_movimiento_inventario_id = 6;
                                movimiento_inventario.sucursal_id = venta.sucursal_id;
                                db.movimiento_inventario.Add(movimiento_inventario);
                                db.SaveChanges();
                                List<concepto_por_nota_credito> ListaConceptosNotaCredito = db.concepto_por_nota_credito.Where(x => x.id_nota_credito == nota_credito.id).ToList();
                                foreach (concepto_por_nota_credito concepto_por_nota_credito in ListaConceptosNotaCredito)
                                {
                                    conceptos_por_venta concepto_venta = db.conceptos_por_venta.Find(concepto_por_nota_credito.id_conceptos_por_venta);
                                    inventario inventario = db.inventario.Where(x => x.Producto_id == concepto_venta.Producto_id && x.sucursal_id == venta.sucursal_id).First();
                                    inventario.cantidad = inventario.cantidad - concepto_por_nota_credito.cantidad;
                                    db.Entry(inventario).State = System.Data.Entity.EntityState.Modified;
                                    conceptos_por_movimiento_inventario conceptos_por_movimiento_inventario = new conceptos_por_movimiento_inventario();
                                    conceptos_por_movimiento_inventario.movimiento_inventario_id = movimiento_inventario.id;
                                    conceptos_por_movimiento_inventario.Inventario_id = inventario.id;
                                    conceptos_por_movimiento_inventario.cantidad = concepto_por_nota_credito.cantidad.Value;
                                    db.conceptos_por_movimiento_inventario.Add(conceptos_por_movimiento_inventario);
                                }
                                db.SaveChanges();
                            }
                            cliente cliente = db.cliente.Find(venta.Cliente_id);
                            List<abono> listaAbonos = venta.abono.Where(x => x.estatus == true).ToList();
                            foreach (abono a in listaAbonos)
                            {
                                a.estatus = false;
                                a.fecha_cancelacion = DateTime.Now;
                                a.usuario_id_cancela = Fn.usuario_logeado.traerUsuario().id;
                                a.usuario_punto_venta1 = db.usuario_punto_venta.Find(a.usuario_id_cancela);
                                a.motivo_cancelacion = "Cancelacion de venta";
                                cliente.Adeudo_credito = cliente.Adeudo_credito + a.importe;
                                db.Entry(a).State = System.Data.Entity.EntityState.Modified;
                            }
                            db.Entry(cliente).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                            Decimal pagos_credito = venta.multiple_pago.Where(x => x.id_tipo_pago_venta == 4).ToList().Select(y => y.cantidad).Sum().Value;
                            venta.catalogo_status_id = 2;
                            venta.fecha_cancelacion = DateTime.Now;
                            venta.usuario_id_cancela = Fn.usuario_logeado.traerUsuario().id;
                            venta.usuario_punto_venta1 = db.usuario_punto_venta.Find(venta.usuario_id_cancela);
                            venta.Motivo_cancelacion = motivo;
                            cliente.Adeudo_credito = cliente.Adeudo_credito - pagos_credito;
                            venta.aduedo = 0;
                            db.Entry(venta).State = System.Data.Entity.EntityState.Modified;
                            db.Entry(cliente).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                            //crear registro movimiento de inventario
                            movimiento_inventario mi = new movimiento_inventario();
                            mi.fecha = DateTime.Now;
                            mi.recibe = venta.cliente.Razon_social;
                            mi.usuario_valida_id = Fn.usuario_logeado.traerUsuario().id;
                            mi.tipo_movimiento_inventario_id = 3;
                            mi.sucursal_id = venta.sucursal_id;
                            db.movimiento_inventario.Add(mi);
                            foreach (conceptos_por_venta concepto in venta.conceptos_por_venta)
                            {
                                //regresar inventario
                                inventario i = db.inventario.Where(x => x.Producto_id == concepto.Producto_id && x.sucursal_id == venta.sucursal_id).First();
                                i.cantidad = i.cantidad + concepto.cantidad;
                                //agregar entrada de inventario
                                conceptos_por_movimiento_inventario cxm = new conceptos_por_movimiento_inventario();
                                cxm.movimiento_inventario_id = mi.id;
                                cxm.Inventario_id = i.id;
                                cxm.cantidad = concepto.cantidad;
                                db.conceptos_por_movimiento_inventario.Add(cxm);
                            }
                            db.SaveChanges();
                            foreach (nota_credito nota_credito in db.nota_credito.Where(x => x.id_venta_aplicada == venta.id).ToList())
                            {
                                nota_credito.fecha_cancelacion = DateTime.Now;
                                nota_credito.id_estatus_nota_credito = 3;
                                db.Entry(nota_credito).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                                ListaNotaCredito[index_lista_nota_credito++] = nota_credito.id;
                                string Serie_de_nota_de_credito = Config.Serie_de_nota_de_credito();
                                nota_credito nueva_nota_credito = new nota_credito();
                                nueva_nota_credito.id_venta = nota_credito.id_venta;
                                nueva_nota_credito.id_estatus_nota_credito = 1;
                                nueva_nota_credito.id_sucursal = venta.sucursal_id;
                                nueva_nota_credito.id_usuario_punto_venta = venta.usuario_id;
                                nueva_nota_credito.fecha_creacion = DateTime.Now;
                                nueva_nota_credito.serie = Config.Serie_de_nota_de_credito();
                                nueva_nota_credito.folio = Convert.ToInt32(db.nota_credito.Where(x => x.serie.Equals(Serie_de_nota_de_credito) && x.venta.sucursal_id == nueva_nota_credito.id_sucursal).Select(x => x.folio).Max()) + 1;
                                nueva_nota_credito.cantidad = nota_credito.cantidad;
                                db.nota_credito.Add(nueva_nota_credito);
                                db.SaveChanges();
                                ListaNotaCredito[index_lista_nota_credito++] = nueva_nota_credito.id;
                                foreach (concepto_por_nota_credito concepto_por_nota_credito in nota_credito.concepto_por_nota_credito)
                                {
                                    concepto_por_nota_credito concepto = new concepto_por_nota_credito();
                                    concepto.id_conceptos_por_venta = concepto_por_nota_credito.id_conceptos_por_venta;
                                    concepto.cantidad = concepto_por_nota_credito.cantidad;
                                    concepto.id_nota_credito = nueva_nota_credito.id;
                                    db.concepto_por_nota_credito.Add(concepto);
                                }
                                db.SaveChanges();
                            }
                            //mensaje = "La venta " + serie_folio + " se pago con una nota de credito, por lo que si se quiere cancelar tendra que quedar disponible la nota de credito con la que se hizo la venta. Y las notas de credito de esta venta se cancelaran. ¿Quiere continuar?";
                        }
                        else
                        {
                            cliente cliente = db.cliente.Find(venta.Cliente_id);
                            List<abono> listaAbonos = venta.abono.Where(x => x.estatus == true).ToList();
                            foreach (abono a in listaAbonos)
                            {
                                a.estatus = false;
                                a.fecha_cancelacion = DateTime.Now;
                                a.usuario_id_cancela = Fn.usuario_logeado.traerUsuario().id;
                                a.usuario_punto_venta1 = db.usuario_punto_venta.Find(a.usuario_id_cancela);
                                a.motivo_cancelacion = "Cancelacion de venta";
                                cliente.Adeudo_credito = cliente.Adeudo_credito + a.importe;
                                db.Entry(a).State = System.Data.Entity.EntityState.Modified;
                            }
                            db.Entry(cliente).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                            Decimal pagos_credito = venta.multiple_pago.Where(x => x.id_tipo_pago_venta == 4).ToList().Select(y => y.cantidad).Sum().Value;
                            venta.catalogo_status_id = 2;
                            venta.fecha_cancelacion = DateTime.Now;
                            venta.usuario_id_cancela = Fn.usuario_logeado.traerUsuario().id;
                            venta.usuario_punto_venta1 = db.usuario_punto_venta.Find(venta.usuario_id_cancela);
                            venta.Motivo_cancelacion = motivo;
                            cliente.Adeudo_credito = cliente.Adeudo_credito - pagos_credito;
                            venta.aduedo = 0;
                            db.Entry(venta).State = System.Data.Entity.EntityState.Modified;
                            db.Entry(cliente).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                            //crear registro movimiento de inventario
                            movimiento_inventario mi = new movimiento_inventario();
                            mi.fecha = DateTime.Now;
                            mi.recibe = venta.cliente.Razon_social;
                            mi.usuario_valida_id = Fn.usuario_logeado.traerUsuario().id;
                            mi.tipo_movimiento_inventario_id = 3;
                            mi.sucursal_id = venta.sucursal_id;
                            db.movimiento_inventario.Add(mi);
                            foreach (conceptos_por_venta concepto in venta.conceptos_por_venta)
                            {
                                //regresar inventario
                                inventario i = db.inventario.Where(x => x.Producto_id == concepto.Producto_id && x.sucursal_id == venta.sucursal_id).First();
                                i.cantidad = i.cantidad + concepto.cantidad;
                                //agregar entrada de inventario
                                conceptos_por_movimiento_inventario cxm = new conceptos_por_movimiento_inventario();
                                cxm.movimiento_inventario_id = mi.id;
                                cxm.Inventario_id = i.id;
                                cxm.cantidad = concepto.cantidad;
                                db.conceptos_por_movimiento_inventario.Add(cxm);
                            }
                            db.SaveChanges();
                            foreach (nota_credito nota_credito in db.nota_credito.Where(x => x.id_venta_aplicada == venta.id).ToList())
                            {
                                nota_credito.fecha_cancelacion = DateTime.Now;
                                nota_credito.id_estatus_nota_credito = 3;
                                db.Entry(nota_credito).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                                ListaNotaCredito[index_lista_nota_credito++] = nota_credito.id;
                                string Serie_de_nota_de_credito = Config.Serie_de_nota_de_credito();
                                nota_credito nueva_nota_credito = new nota_credito();
                                nueva_nota_credito.id_venta = nota_credito.id_venta;
                                nueva_nota_credito.id_estatus_nota_credito = 1;
                                nueva_nota_credito.id_sucursal = venta.sucursal_id;
                                nueva_nota_credito.id_usuario_punto_venta = venta.usuario_id;
                                nueva_nota_credito.fecha_creacion = DateTime.Now;
                                nueva_nota_credito.serie = Config.Serie_de_nota_de_credito();
                                nueva_nota_credito.folio = Convert.ToInt32(db.nota_credito.Where(x => x.serie.Equals(Serie_de_nota_de_credito) && x.venta.sucursal_id == nueva_nota_credito.id_sucursal).Select(x => x.folio).Max()) + 1;
                                nueva_nota_credito.cantidad = nota_credito.cantidad;
                                db.nota_credito.Add(nueva_nota_credito);
                                db.SaveChanges();
                                ListaNotaCredito[index_lista_nota_credito++] = nueva_nota_credito.id;
                                foreach (concepto_por_nota_credito concepto_por_nota_credito in nota_credito.concepto_por_nota_credito)
                                {
                                    concepto_por_nota_credito concepto = new concepto_por_nota_credito();
                                    concepto.id_conceptos_por_venta = concepto_por_nota_credito.id_conceptos_por_venta;
                                    concepto.cantidad = concepto_por_nota_credito.cantidad;
                                    concepto.id_nota_credito = nueva_nota_credito.id;
                                    db.concepto_por_nota_credito.Add(concepto);
                                }
                                db.SaveChanges();
                            }
                            //mensaje = "La venta " + serie_folio + " se pago con una nota de credito, por lo que si se quiere cancelar tendra que quedar disponible la nota de credito con la que se hizo la venta. ¿Quiere continuar?";
                        }
                    }
                    success = true;
                }
            }
            catch (Exception)
            {
                success = false;
                throw;
            }
            var respuesta = new
            {
                success = success,
                ListaNotaCredito = ListaNotaCredito,
                index_lista_nota_credito = index_lista_nota_credito
            };
            return Json(respuesta, JsonRequestBehavior.AllowGet);
        }

        public void ActualizarTiposPago(int id_venta, List<multiple_pago> ListaTiposPagos, String Referencia)
        {
            venta venta = db.venta.Find(id_venta);
            cliente cliente = db.cliente.Find(venta.Cliente_id);
            foreach (multiple_pago multiple_pago in venta.multiple_pago.ToList())
            {
                if (multiple_pago.id_tipo_pago_venta == 4)
                {
                    foreach (abono abono in venta.abono.ToList())
                    {
                        abono.fecha_cancelacion = DateTime.Now;
                        abono.usuario_id_cancela = Fn.usuario_logeado.traerUsuario().id;
                        abono.usuario_punto_venta = db.usuario_punto_venta.Find(abono.usuario_id_cancela);
                        abono.motivo_cancelacion = "Cambio de tipo de pago";
                        abono.estatus = false;
                        db.Entry(abono).State = System.Data.Entity.EntityState.Modified;
                        cliente.Adeudo_credito = cliente.Adeudo_credito + abono.importe;
                    }
                    db.Entry(cliente).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    cliente.Adeudo_credito = cliente.Adeudo_credito - multiple_pago.cantidad;
                    venta.aduedo = 0;
                    db.Entry(cliente).State = System.Data.Entity.EntityState.Modified;
                    db.Entry(venta).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
                db.multiple_pago.Remove(multiple_pago);
            }
            db.SaveChanges();
            foreach (multiple_pago tipo_pago in ListaTiposPagos)
            {
                multiple_pago multiple_pago = new multiple_pago();
                multiple_pago.id_tipo_pago_venta = tipo_pago.id_tipo_pago_venta;
                multiple_pago.cantidad = tipo_pago.cantidad;
                multiple_pago.id_venta = id_venta;
                if (multiple_pago.id_tipo_pago_venta == 5)
                {
                    if (!(Referencia == null || Referencia.Equals("")))
                    {
                        multiple_pago.referencia = Referencia;
                    }
                    else
                    {
                        multiple_pago.referencia = "Falta Referencia!";
                    }
                }
                db.multiple_pago.Add(multiple_pago);
                if (multiple_pago.id_tipo_pago_venta == 4)
                {
                    cliente.Adeudo_credito = cliente.Adeudo_credito + multiple_pago.cantidad;
                    venta.aduedo = multiple_pago.cantidad;
                    db.Entry(cliente).State = System.Data.Entity.EntityState.Modified;
                    db.Entry(venta).State = System.Data.Entity.EntityState.Modified;
                }
            }
            db.SaveChanges();
        }

        public ActionResult VerificarEstatusVenta(int id_venta)
        {
            venta venta = db.venta.Find(id_venta);
            List<nota_credito> ListaNotasCreditoGeneradasPorVenta = db.nota_credito.Where(x => x.id_venta == id_venta).ToList();
            List<nota_credito> ListaNotasCreditoAplicadasPorVenta = db.nota_credito.Where(x => x.id_venta_aplicada == id_venta).ToList();
            var respuesta = new
            {
                nc_aplicadas_a_esta_venta = ListaNotasCreditoAplicadasPorVenta.Count,
                nc_generadas_disponibles = ListaNotasCreditoGeneradasPorVenta.Where(x => x.id_estatus_nota_credito == 1).ToList().Count,
                nc_generadas_aplicadas = ListaNotasCreditoGeneradasPorVenta.Where(x => x.id_estatus_nota_credito == 2).ToList().Count
            };
            return Json(respuesta, JsonRequestBehavior.AllowGet);
        }

    }
}
