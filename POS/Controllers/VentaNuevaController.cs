﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using POS.Models;
using System.Data;
using System.IO;
using POS.Fn;

namespace POS.Controllers
{
    public class VentaNuevaController : Controller
    {

        private moragas_pruebasEntities1 db = new moragas_pruebasEntities1();
        private CultureInfo IdiomaFecha = new CultureInfo("es-MX");

        [Authorize]
        public ActionResult NuevaVenta()
        {

            String query = "SELECT Codigo as codigo, Descripcion as descripcion FROM producto WHERE visible = 1 ORDER BY Codigo;";
            DataSet ds = ConnDB.GetDataSet(query, Server.MapPath("").Split('\\').ElementAt(Server.MapPath("").Split('\\').Length - 2));
            DataTable dt = ds.Tables[0];
            List<ConsultaProducto> ListaProductos = (from DataRow row in dt.Rows
                                                     select new ConsultaProducto
                                                     {
                                                         codigo = row["codigo"].ToString() + row["descripcion"].ToString()
                                                     }).ToList();
            var JsonProductos = new JavaScriptSerializer();
            var JsonListaProductos = JsonProductos.Serialize(ListaProductos.Select(x => x.codigo));
            ViewBag.ListaProductos = JsonListaProductos;
            return View();
        }

    }
}
