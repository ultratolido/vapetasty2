﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using POS.Models;
using POS.Fn;
using System.Web.Security;

namespace POS.Controllers
{
    public class UsuarioController : Controller
    {
        private moragas_pruebasEntities1 db = new moragas_pruebasEntities1();

        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName = "Lista De Usuarios")]
        public ViewResult Index()
        {
            List<int> ListaSucursalesPermitidas = Fn.usuario_logeado.Sucursales_id();
            ViewBag.id_sucursal = new SelectList(db.sucursal.Where(x => ListaSucursalesPermitidas.Contains(x.id)).OrderByDescending(y => y.id), "id", "nombre_sucursal");
            return View();
        }

        public PartialViewResult VistaParcialUsuario(int id_sucursal)
        {
            // nivel usuario
            // 1 - Quianty
            // 2 - Admin    
            // 3 - Usuario
            int nivelUsuario = Fn.usuario_logeado.nivel_id();
            if (nivelUsuario == 1 || nivelUsuario == 2)
            {
                List<usuario_punto_venta> ListaUsuarios = db.usuario_punto_venta.Where(x => x.nivel_usuario_id >= nivelUsuario && x.sucursales_por_usuario.Select(y => y.sucursal_id).ToList().Contains(id_sucursal)).ToList();
                ViewBag.ListaUsuarios = ListaUsuarios;
            }
            else if (nivelUsuario == 3)
            {
                int id_usuario_logeado = Fn.usuario_logeado.traerUsuario().id;
                List<usuario_punto_venta> ListaUsuarios = db.usuario_punto_venta.Where(x => x.id == id_usuario_logeado).ToList();
                ViewBag.ListaUsuarios = ListaUsuarios;
            }
            return PartialView();
        }

        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName = "Crear Usuario")]
        public ActionResult NuevoUsuario()
        {
            usuario_punto_venta usuario_logeado = Fn.usuario_logeado.traerUsuario();
            List<int> ListaPermisosUsuarioLogeado = usuario_logeado.permiso_vista_por_usuario.Select(x => x.permiso_vista_id).ToList();
            int id_nivel_usuario = Fn.usuario_logeado.nivel_id();
            List<int> ListaSucursalesPermitidas = Fn.usuario_logeado.Sucursales_id();
            List<sucursal> ListaSucursales = db.sucursal.Where(x => ListaSucursalesPermitidas.Contains(x.id)).ToList();
            List<permiso_vista> ListaPermisos = db.permiso_vista.Where(x => ListaPermisosUsuarioLogeado.Contains(x.id)).ToList();
            List<usuario_punto_venta> ListaUsuarios = db.usuario_punto_venta.Where(x => x.nivel_usuario_id >= id_nivel_usuario).ToList();
            List<int> ListaUsuariosConPermiso = new List<int>();
            foreach (usuario_punto_venta usuario in ListaUsuarios)
            {
                List<int> ListaSucursalesPorUsuarios = usuario.sucursales_por_usuario.Select(x => x.sucursal_id.Value).ToList();
                if (ListaSucursalesPermitidas.Except(ListaSucursalesPorUsuarios).Count() != ListaSucursalesPermitidas.Count)
                    ListaUsuariosConPermiso.Add(usuario.id);
            }
            ViewBag.ListaSucursales = ListaSucursales;
            ViewBag.ListaPermisos = ListaPermisos;
            ViewBag.id_usuario_seleccionado = new SelectList(db.usuario_punto_venta.Where(x => ListaUsuariosConPermiso.Contains(x.id)), "id", "nombre_usuario");
            ViewBag.nivel_usuario_id = new SelectList(db.nivel_usuario.Where(x => x.id >= id_nivel_usuario), "id", "nombre");
            return View();
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult NuevoUsuario(usuario_punto_venta usuario, string[] sucursalesSeleccionadas, string[] permisosSeleccionados)
        {
            usuario.usuario_facturacion_id = 0;
            db.usuario_punto_venta.Add(usuario);
            db.SaveChanges();
            //ACTUALIZAR SUCURSALES POR USUARIO
            actualizaSucursalesUsuario(sucursalesSeleccionadas, usuario);
            actualizaPermisosUsuario(permisosSeleccionados, usuario);
            return RedirectToAction("Index");
        }

        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName = "Editar Usuario")]
        public ActionResult EditarUsuario(int id)
        {
            // nivel usuario
            // 1 - Quianty
            // 2 - Admin    
            // 3 - Usuario
            usuario_punto_venta usuario = db.usuario_punto_venta.Find(id);
            List<int> ListaPermisosUsuarioEditar = usuario.permiso_vista_por_usuario.Select(x => x.permiso_vista_id).ToList();
            List<int> ListaSucursalesUsuarioEditar = usuario.sucursales_por_usuario.Select(x => x.sucursal_id.Value).ToList();
            int id_nivel_usuario = Fn.usuario_logeado.nivel_id();
            if (id_nivel_usuario <= usuario.nivel_usuario_id)
            {
                usuario_punto_venta usuario_logeado = Fn.usuario_logeado.traerUsuario();
                List<int> ListaPermisosUsuarioLogeado = usuario_logeado.permiso_vista_por_usuario.Select(x => x.permiso_vista_id).ToList();
                List<permiso_vista> ListaPermisos = db.permiso_vista.Where(x => ListaPermisosUsuarioLogeado.Contains(x.id)).ToList();
                List<int> ListaIdsPermisos = ListaPermisos.Select(x => x.id).ToList();
                List<int> ListaIdsPermisosActuales = ListaPermisosUsuarioEditar.Except(ListaIdsPermisos).ToList();
                List<permiso_vista> ListaPermisosActuales = db.permiso_vista.Where(x => ListaIdsPermisosActuales.Contains(x.id)).ToList();
                List<int> ListaSucursalesUsuarioLogeado = usuario_logeado.sucursales_por_usuario.Select(x => x.sucursal_id.Value).ToList();
                List<sucursal> ListaSucursales = db.sucursal.Where(x => ListaSucursalesUsuarioLogeado.Contains(x.id)).ToList();
                List<int> ListaIdsSucursales = ListaSucursales.Select(x => x.id).ToList();
                List<int> ListaIdsSucursalesActuales = ListaSucursalesUsuarioEditar.Except(ListaIdsSucursales).ToList();
                List<sucursal> ListaSucursalesActuales = db.sucursal.Where(x => ListaIdsSucursalesActuales.Contains(x.id)).ToList();
                List<int> ListaSucursalesPermitidas = Fn.usuario_logeado.Sucursales_id();
                List<usuario_punto_venta> ListaUsuarios = db.usuario_punto_venta.Where(x => x.nivel_usuario_id >= id_nivel_usuario).ToList();
                List<int> ListaUsuariosConPermiso = new List<int>();
                foreach (usuario_punto_venta usuario_aux in ListaUsuarios)
                {
                    List<int> ListaSucursalesPorUsuarios = usuario_aux.sucursales_por_usuario.Select(x => x.sucursal_id.Value).ToList();
                    if (ListaSucursalesPermitidas.Except(ListaSucursalesPorUsuarios).Count() != ListaSucursalesPermitidas.Count)
                        ListaUsuariosConPermiso.Add(usuario_aux.id);
                }
                ViewBag.ListaSucursalesActuales = ListaSucursalesActuales;
                ViewBag.ListaSucursales = ListaSucursales;
                ViewBag.ListaPermisosActuales = ListaPermisosActuales;
                ViewBag.ListaPermisos = ListaPermisos;
                ViewBag.id_usuario_seleccionado = new SelectList(db.usuario_punto_venta.Where(x => ListaUsuariosConPermiso.Contains(x.id)), "id", "nombre_usuario", usuario.id);
                ViewBag.nivel_usuario_id = new SelectList(db.nivel_usuario.Where(x => x.id >= id_nivel_usuario), "id", "nombre", usuario.nivel_usuario_id);
                ViewBag.id_estatus_usuario = new SelectList(db.estatus_usuario, "id", "nombre", usuario.id_estatus_usuario);
                return View(usuario);
            }
            else
            {
                return RedirectToAction("AccesoDenegadoNivel", "Account");
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult EditarUsuario(usuario_punto_venta usuario, string[] sucursalesSeleccionadas, string[] permisosSeleccionados)
        {
            db.Entry(usuario).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            //ACTUALIZAR SUCURSALES POR USUARIO
            actualizaSucursalesUsuario(sucursalesSeleccionadas, usuario);
            actualizaPermisosUsuario(permisosSeleccionados, usuario);
            return RedirectToAction("Index");
        }

        public PartialViewResult ModalAdvertenciaUsuario()
        {
            return PartialView();
        }

        private void actualizaPermisosUsuario(string[] permisosSeleccionados, usuario_punto_venta usuarioActualizar)
        {
            //   E L I M I N A   T O D A S   S U C U R S A L E S 
            //**************************************************//
            List<permiso_vista_por_usuario> eliminarPermisosUsuario = db.permiso_vista_por_usuario.Where(x => x.usuario_punto_venta_id == usuarioActualizar.id).ToList();
            if (eliminarPermisosUsuario.Count() != 0)
            {
                foreach (var permisoEliminar in eliminarPermisosUsuario)
                {
                    db.permiso_vista_por_usuario.Remove(permisoEliminar);
                }
            }
            //   A G R E G A    S U C U R S A L E S 
            //***************************************//
            if (permisosSeleccionados != null)
            {
                //POR CADA ID EN LA LISTA 'sucursalesSeleccionadas'
                foreach (var idPermisoAgregar in permisosSeleccionados)
                {
                    permiso_vista_por_usuario pu = new permiso_vista_por_usuario();
                    pu.permiso_vista_id = Convert.ToInt16(idPermisoAgregar);
                    pu.usuario_punto_venta_id = usuarioActualizar.id;
                    db.permiso_vista_por_usuario.Add(pu);
                }
            }
            db.SaveChanges();
        }

        private void actualizaSucursalesUsuario(string[] sucursalesSeleccionadas, usuario_punto_venta usuarioActualizar)
        {
            //   E L I M I N A   T O D A S   S U C U R S A L E S 
            //**************************************************//
            List<sucursales_por_usuario> eliminarSucursalesUsuario = db.sucursales_por_usuario.Where(x => x.usuario_punto_venta_id == usuarioActualizar.id).ToList();
            if (eliminarSucursalesUsuario.Count() != 0)
            {
                foreach (var SucursalEliminar in eliminarSucursalesUsuario)
                {
                    db.sucursales_por_usuario.Remove(SucursalEliminar);
                }
            }
            //   A G R E G A    S U C U R S A L E S 
            //***************************************//
            if (sucursalesSeleccionadas != null)
            {
                //POR CADA ID EN LA LISTA 'sucursalesSeleccionadas'
                foreach (var idSucursalAgregar in sucursalesSeleccionadas)
                {
                    sucursales_por_usuario su = new sucursales_por_usuario();
                    su.sucursal_id = Convert.ToInt16(idSucursalAgregar);
                    su.usuario_punto_venta_id = usuarioActualizar.id;
                    db.sucursales_por_usuario.Add(su);
                }
            }
            db.SaveChanges();
        }

        public string parcial_traerPermisosUsuario(int id)
        {
            string trPermisos = "";
            List<permiso_vista> permisos = db.permiso_vista.ToList();
            foreach (var permisosControlador in permisos.GroupBy(x => x.nombreControlador))
            {
                trPermisos += "<tr>"
                 + "   <td>" + permisosControlador.First().nombreControlador.ToUpper() + "</td>"
                + "</tr>"
                + "<tr>";
                foreach (var permisoVista in permisosControlador)
                {
                    bool permisoVistaUsuario = permisoVista.permiso_vista_por_usuario.Where(x => x.usuario_punto_venta_id == id).Any();
                    string _checked = permisoVistaUsuario ? "checked=checked" : "";
                    trPermisos += "<td>"
                        + "<input type='checkbox' class='switch-permiso' style='margin:5px' name='permisosSeleccionados' value='" + permisoVista.id + "' " + _checked + ")) /><label style='margin: 0 20px 0 5px; font-weight: normal'>" + permisoVista.nombreVistaPublico + "</label>"
                    + "</td>";
                }
                trPermisos += "</tr><tr>"
                    + "<td style='height: 20px;'></td>"
                + "</tr>";
            }
            return trPermisos;
        }

        public bool VerificarUsuario(int id, String usuario)
        {
            bool existe;
            if (id == 0)
                existe = db.usuario_punto_venta.Where(x => x.nombre_usuario.ToLower().Equals(usuario.ToLower())).Any();
            else
                existe = db.usuario_punto_venta.Where(x => x.id != id && x.nombre_usuario.ToLower().Equals(usuario.ToLower())).Any();
            return existe;
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}