﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using POS.Models;
using System.IO;
using System.Reflection;
using System.Data.Entity;
using System.Globalization;
using Pos.Models;
using System.Web.Script.Serialization;
using POS.Fn;

namespace POS.Controllers
{
    public class HomeController : Controller
    {

        moragas_pruebasEntities1 db = new moragas_pruebasEntities1();
        CultureInfo IdiomaFecha = new CultureInfo("es-MX");

        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName = "Pantalla De Inicio")]
        public ActionResult Index()
        {
            List<cliente> ListaClientes = db.cliente.ToList();
            ViewBag.ListaClientes = ListaClientes;
            List<int> ListaSucursalesPermitidas = Fn.usuario_logeado.Sucursales_id();
            ViewBag.id_sucursal = new SelectList(db.sucursal.Where(x => ListaSucursalesPermitidas.Contains(x.id)).OrderByDescending(y => y.id), "id", "nombre_sucursal");
            return View();
        }

        public PartialViewResult ModalResumenCreditoCliente(int id_cliente)
        {
            cliente cliente = db.cliente.Find(id_cliente);
            List<venta> ListaVentas = cliente.venta.Where(x => (x.catalogo_status_id == 1 || (x.catalogo_status_id >= 9 && x.catalogo_status_id <= 11) || x.catalogo_status_id == 10 || x.catalogo_status_id == 11) && x.multiple_pago.Where(y => y.id_tipo_pago_venta == 4).ToList().Count > 0).ToList();
            ViewBag.cliente = cliente;
            ViewBag.ListaVentas = ListaVentas;
            return PartialView();
        }

        public void RecalcularCredito()
        {
            List<cliente> ListaClientes = db.cliente.ToList();
            foreach (cliente cliente in ListaClientes)
            {
                Decimal TotalVentasCreditoCliente = 0;
                Decimal TotalAbonosCliente = 0;
                List<venta> ListaVentas = db.venta.Where(x => x.Cliente_id == cliente.id && (x.catalogo_status_id == 1 || (x.catalogo_status_id >= 9 && x.catalogo_status_id <= 11) || x.catalogo_status_id == 10 || x.catalogo_status_id == 11) && x.multiple_pago.Where(y => y.id_tipo_pago_venta == 4).Count() > 0).ToList();
                foreach (venta venta in ListaVentas)
                {
                    Decimal TotalVentaCredito = 0;
                    Decimal TotalAbonosVenta = 0;
                    TotalVentaCredito = venta.multiple_pago.Where(x => x.id_tipo_pago_venta == 4).ToList().Select(y => y.cantidad).Sum().Value;
                    TotalAbonosVenta = venta.abono.Where(x => x.estatus == true).ToList().Select(y => y.importe).Sum().Value;
                    TotalVentasCreditoCliente += TotalVentaCredito;
                    TotalAbonosCliente += TotalAbonosVenta;
                    venta.aduedo = Math.Round(TotalVentaCredito, 2) - Math.Round(TotalAbonosVenta, 2);
                    db.Entry(venta).State = System.Data.Entity.EntityState.Modified;
                }
                cliente.Adeudo_credito = Math.Round(TotalVentasCreditoCliente, 2) - Math.Round(TotalAbonosCliente, 2);
                db.Entry(cliente).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                List<venta> ListaVentasInactivas = db.venta.Where(x => x.Cliente_id == cliente.id && (x.catalogo_status_id != 1 && x.catalogo_status_id != 9 && x.catalogo_status_id != 10 && x.catalogo_status_id != 11)).ToList();
                foreach (venta venta in ListaVentasInactivas)
                {
                    venta.aduedo = 0;
                    db.Entry(venta).State = System.Data.Entity.EntityState.Modified;
                }
                db.SaveChanges();
            }
        }

        public ActionResult ProductosAgregados()
        {
            List<nuevo_producto> ListaNuevosProductos = db.nuevo_producto.ToList();
            List<String> ListaNuevos = new List<string>();
            foreach (nuevo_producto nuevo_producto in ListaNuevosProductos)
            {
                if (!db.producto.Where(x => x.Codigo.ToLower().Equals(nuevo_producto.codigo.ToLower())).Any())
                {
                    producto producto = new producto();
                    producto.Codigo = nuevo_producto.codigo;
                    producto.Descripcion = "Producto sin descripcion";
                    producto.id_marca = 1;
                    producto.Proveedor_id = 1;
                    producto.SubFamilia_id = 1;
                    producto.costo = 0;
                    producto.precio_dolar = 0;
                    producto.carrusel = 0;
                    producto.publicar = 0;
                    db.producto.Add(producto);
                    ListaNuevos.Add(nuevo_producto.codigo);
                }
            }
            db.SaveChanges();
            ViewBag.ListaNuevos = ListaNuevos;
            return View();
        }

        public PartialViewResult VistaParcialInicio(int id_sucursal)
        {
            Decimal cantidad_ventas_mes_pasado = 0;
            Decimal cantidad_ventas_mes_actual = 0;
            Decimal cantidad_ventas_semana_pasada = 0;
            Decimal cantidad_ventas_semana_actual = 0;
            Decimal cantidad_ventas_ayer = 0;
            Decimal cantidad_ventas_hoy = 0;
            Decimal cantidad_ventas_sucursal = 0;
            Decimal cantidad_ventas_online = 0;
            List<venta> venta;
            List<int> ListaIdsClientes;
            List<cliente> ListaCreditosClientes;

            DateTime hoy_inicio = Convert.ToDateTime(DateTime.Today, IdiomaFecha);
            DateTime hoy_fin = Convert.ToDateTime(hoy_inicio.AddHours(23).AddMinutes(59).AddSeconds(59), IdiomaFecha);
            DateTime hoy_pasado_inicio = Convert.ToDateTime(hoy_inicio.AddDays(-1), IdiomaFecha);
            DateTime hoy_pasado_fin = Convert.ToDateTime(hoy_fin.AddDays(-1), IdiomaFecha);

            DateTime semana_actual_inicio = Convert.ToDateTime(DateTime.Today.AddDays((-1) * Convert.ToInt16(DateTime.Today.DayOfWeek)), IdiomaFecha);
            DateTime semana_actual_fin = Convert.ToDateTime(semana_actual_inicio.AddDays(6).AddHours(23).AddMinutes(59).AddSeconds(59), IdiomaFecha);
            DateTime semana_pasado_inicio = Convert.ToDateTime(DateTime.Today.AddDays((-1) * Convert.ToInt16(DateTime.Today.DayOfWeek)).AddDays(-7), IdiomaFecha);
            DateTime semana_pasado_fin = Convert.ToDateTime(semana_pasado_inicio.AddDays(6).AddHours(23).AddMinutes(59).AddSeconds(59), IdiomaFecha);

            int mes_pasado = 0;
            int ano_pasado = 0;
            if (DateTime.Today.Month == 1)
            {
                mes_pasado = 12;
                ano_pasado = DateTime.Today.Year - 1;
            }
            else
            {
                mes_pasado = DateTime.Today.Month - 1;
                ano_pasado = DateTime.Today.Year;
            }
            DateTime mes_actual_inicio = Convert.ToDateTime(DateTime.Today.AddDays((DateTime.Today.Day - 1) * (-1)), IdiomaFecha);
            DateTime mes_actual_fin = Convert.ToDateTime(mes_actual_inicio.AddDays((DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.Month) - 1)).AddHours(23).AddMinutes(59).AddSeconds(59), IdiomaFecha);
            DateTime mes_pasado_inicio = Convert.ToDateTime(DateTime.Today.AddDays((DateTime.Today.Day - 1) * (-1)).AddDays((-1) * DateTime.DaysInMonth(ano_pasado, mes_pasado)), IdiomaFecha);
            DateTime mes_pasado_fin = Convert.ToDateTime(mes_pasado_inicio.AddDays(DateTime.DaysInMonth(ano_pasado, mes_pasado) - 1).AddHours(23).AddMinutes(59).AddSeconds(59), IdiomaFecha);

            DateTime mes_dia_inicio = Convert.ToDateTime(DateTime.Today.AddDays((DateTime.Today.Day - 1) * (-1)), IdiomaFecha);
            DateTime mes_dia_fin = Convert.ToDateTime(mes_dia_inicio.AddHours(23).AddMinutes(59).AddSeconds(59), IdiomaFecha);
            DateTime mes_pasado_dia_inicio = Convert.ToDateTime(DateTime.Today.AddDays((DateTime.Today.Day - 1) * (-1)).AddMonths(-1), IdiomaFecha);
            DateTime mes_pasado_dia_fin = Convert.ToDateTime(mes_dia_inicio.AddHours(23).AddMinutes(59).AddSeconds(59).AddMonths(-1), IdiomaFecha);
            DateTime mes_antepasado_dia_inicio = Convert.ToDateTime(DateTime.Today.AddDays((DateTime.Today.Day - 1) * (-1)).AddMonths(-2), IdiomaFecha);
            DateTime mes_antepasado_dia_fin = Convert.ToDateTime(mes_dia_inicio.AddHours(23).AddMinutes(59).AddSeconds(59).AddMonths(-2), IdiomaFecha);

            if (id_sucursal == 0)
            {
                venta = db.venta.Where(x => ((x.catalogo_status_id == 1 && x.Fecha_entrega >= mes_pasado_inicio && x.Fecha_entrega <= mes_pasado_fin)
                                                  || (x.catalogo_status_id == 9 && x.fecha_liquidacion >= mes_pasado_inicio && x.fecha_liquidacion <= mes_pasado_fin)
                                                  || (x.catalogo_status_id == 10 && x.fecha_liquidacion >= mes_pasado_inicio && x.fecha_liquidacion <= mes_pasado_fin)
                                                  || (x.catalogo_status_id == 11 && x.fecha_liquidacion >= mes_pasado_inicio && x.fecha_liquidacion <= mes_pasado_fin))).ToList();
                cantidad_ventas_mes_pasado = calcularTotalVenta(venta);
                venta = db.venta.Where(x => ((x.catalogo_status_id == 1 && x.Fecha_entrega >= mes_actual_inicio && x.Fecha_entrega <= mes_actual_fin)
                                                  || (x.catalogo_status_id == 9 && x.fecha_liquidacion >= mes_actual_inicio && x.fecha_liquidacion <= mes_actual_fin)
                                                  || (x.catalogo_status_id == 10 && x.fecha_liquidacion >= mes_actual_inicio && x.fecha_liquidacion <= mes_actual_fin)
                                                  || (x.catalogo_status_id == 11 && x.fecha_liquidacion >= mes_actual_inicio && x.fecha_liquidacion <= mes_actual_fin))).ToList();
                cantidad_ventas_mes_actual = calcularTotalVenta(venta);
            }
            else
            {
                venta = db.venta.Where(x => ((x.catalogo_status_id == 1 && x.Fecha_entrega >= mes_pasado_inicio && x.Fecha_entrega <= mes_pasado_fin)
                                                  || (x.catalogo_status_id == 9 && x.fecha_liquidacion >= mes_pasado_inicio && x.fecha_liquidacion <= mes_pasado_fin)
                                                  || (x.catalogo_status_id == 10 && x.fecha_liquidacion >= mes_pasado_inicio && x.fecha_liquidacion <= mes_pasado_fin)
                                                  || (x.catalogo_status_id == 11 && x.fecha_liquidacion >= mes_pasado_inicio && x.fecha_liquidacion <= mes_pasado_fin))
                                                  && x.sucursal_id == id_sucursal).ToList();
                cantidad_ventas_mes_pasado = calcularTotalVenta(venta);
                venta = db.venta.Where(x => ((x.catalogo_status_id == 1 && x.Fecha_entrega >= mes_actual_inicio && x.Fecha_entrega <= mes_actual_fin)
                                                  || (x.catalogo_status_id == 9 && x.fecha_liquidacion >= mes_actual_inicio && x.fecha_liquidacion <= mes_actual_fin)
                                                  || (x.catalogo_status_id == 10 && x.fecha_liquidacion >= mes_actual_inicio && x.fecha_liquidacion <= mes_actual_fin)
                                                  || (x.catalogo_status_id == 11 && x.fecha_liquidacion >= mes_actual_inicio && x.fecha_liquidacion <= mes_actual_fin))
                                                  && x.sucursal_id == id_sucursal).ToList();
                cantidad_ventas_mes_actual = calcularTotalVenta(venta);
            }
            ViewBag.cantidad_ventas_mes_pasado = cantidad_ventas_mes_pasado;
            ViewBag.cantidad_ventas_mes_actual = cantidad_ventas_mes_actual;

            if (id_sucursal == 0)
            {
                venta = db.venta.Where(x => ((x.catalogo_status_id == 1 && x.Fecha_entrega >= semana_pasado_inicio && x.Fecha_entrega <= semana_pasado_fin)
                                                  || (x.catalogo_status_id == 9 && x.fecha_liquidacion >= semana_pasado_inicio && x.fecha_liquidacion <= semana_pasado_fin)
                                                  || (x.catalogo_status_id == 10 && x.fecha_liquidacion >= semana_pasado_inicio && x.fecha_liquidacion <= semana_pasado_fin)
                                                  || (x.catalogo_status_id == 11 && x.fecha_liquidacion >= semana_pasado_inicio && x.fecha_liquidacion <= semana_pasado_fin))).ToList();
                cantidad_ventas_semana_pasada = calcularTotalVenta(venta);
                venta = db.venta.Where(x => ((x.catalogo_status_id == 1 && x.Fecha_entrega >= semana_actual_inicio && x.Fecha_entrega <= semana_actual_fin)
                                                  || (x.catalogo_status_id == 9 && x.fecha_liquidacion >= semana_actual_inicio && x.fecha_liquidacion <= semana_actual_fin)
                                                  || (x.catalogo_status_id == 10 && x.fecha_liquidacion >= semana_actual_inicio && x.fecha_liquidacion <= semana_actual_fin)
                                                  || (x.catalogo_status_id == 11 && x.fecha_liquidacion >= semana_actual_inicio && x.fecha_liquidacion <= semana_actual_fin))).ToList();
                cantidad_ventas_semana_actual = calcularTotalVenta(venta);
            }
            else
            {
                venta = db.venta.Where(x => ((x.catalogo_status_id == 1 && x.Fecha_entrega >= semana_pasado_inicio && x.Fecha_entrega <= semana_pasado_fin)
                                                  || (x.catalogo_status_id == 9 && x.fecha_liquidacion >= semana_pasado_inicio && x.fecha_liquidacion <= semana_pasado_fin)
                                                  || (x.catalogo_status_id == 10 && x.fecha_liquidacion >= semana_pasado_inicio && x.fecha_liquidacion <= semana_pasado_fin)
                                                  || (x.catalogo_status_id == 11 && x.fecha_liquidacion >= semana_pasado_inicio && x.fecha_liquidacion <= semana_pasado_fin))
                                                  && x.sucursal_id == id_sucursal).ToList();
                cantidad_ventas_semana_pasada = calcularTotalVenta(venta);
                venta = db.venta.Where(x => ((x.catalogo_status_id == 1 && x.Fecha_entrega >= semana_actual_inicio && x.Fecha_entrega <= semana_actual_fin)
                                                  || (x.catalogo_status_id == 9 && x.fecha_liquidacion >= semana_actual_inicio && x.fecha_liquidacion <= semana_actual_fin)
                                                  || (x.catalogo_status_id == 10 && x.fecha_liquidacion >= semana_actual_inicio && x.fecha_liquidacion <= semana_actual_fin)
                                                  || (x.catalogo_status_id == 11 && x.fecha_liquidacion >= semana_actual_inicio && x.fecha_liquidacion <= semana_actual_fin))
                                                  && x.sucursal_id == id_sucursal).ToList();
                cantidad_ventas_semana_actual = calcularTotalVenta(venta);
            }
            ViewBag.cantidad_ventas_semana_pasada = cantidad_ventas_semana_pasada;
            ViewBag.cantidad_ventas_semana_actual = cantidad_ventas_semana_actual;

            if (id_sucursal == 0)
            {
                venta = db.venta.Where(x => ((x.catalogo_status_id == 1 && x.Fecha_entrega >= hoy_pasado_inicio && x.Fecha_entrega <= hoy_pasado_fin)
                                                  || (x.catalogo_status_id == 9 && x.fecha_liquidacion >= hoy_pasado_inicio && x.fecha_liquidacion <= hoy_pasado_fin)
                                                  || (x.catalogo_status_id == 10 && x.fecha_liquidacion >= hoy_pasado_inicio && x.fecha_liquidacion <= hoy_pasado_fin)
                                                  || (x.catalogo_status_id == 11 && x.fecha_liquidacion >= hoy_pasado_inicio && x.fecha_liquidacion <= hoy_pasado_fin))).ToList();
                cantidad_ventas_ayer = calcularTotalVenta(venta);
                venta = db.venta.Where(x => ((x.catalogo_status_id == 1 && x.Fecha_entrega >= hoy_inicio && x.Fecha_entrega <= hoy_fin)
                                                  || (x.catalogo_status_id == 9 && x.fecha_liquidacion >= hoy_inicio && x.fecha_liquidacion <= hoy_fin)
                                                  || (x.catalogo_status_id == 10 && x.fecha_liquidacion >= hoy_inicio && x.fecha_liquidacion <= hoy_fin)
                                                  || (x.catalogo_status_id == 11 && x.fecha_liquidacion >= hoy_inicio && x.fecha_liquidacion <= hoy_fin))).ToList();
                cantidad_ventas_hoy = calcularTotalVenta(venta);
            }
            else
            {
                venta = db.venta.Where(x => ((x.catalogo_status_id == 1 && x.Fecha_entrega >= hoy_pasado_inicio && x.Fecha_entrega <= hoy_pasado_fin)
                                                  || (x.catalogo_status_id == 9 && x.fecha_liquidacion >= hoy_pasado_inicio && x.fecha_liquidacion <= hoy_pasado_fin)
                                                  || (x.catalogo_status_id == 10 && x.fecha_liquidacion >= hoy_pasado_inicio && x.fecha_liquidacion <= hoy_pasado_fin)
                                                  || (x.catalogo_status_id == 11 && x.fecha_liquidacion >= hoy_pasado_inicio && x.fecha_liquidacion <= hoy_pasado_fin))
                                                  && x.sucursal_id == id_sucursal).ToList();
                cantidad_ventas_ayer = calcularTotalVenta(venta);
                venta = db.venta.Where(x => ((x.catalogo_status_id == 1 && x.Fecha_entrega >= hoy_inicio && x.Fecha_entrega <= hoy_fin)
                                                  || (x.catalogo_status_id == 9 && x.fecha_liquidacion >= hoy_inicio && x.fecha_liquidacion <= hoy_fin)
                                                  || (x.catalogo_status_id == 10 && x.fecha_liquidacion >= hoy_inicio && x.fecha_liquidacion <= hoy_fin)
                                                  || (x.catalogo_status_id == 11 && x.fecha_liquidacion >= hoy_inicio && x.fecha_liquidacion <= hoy_fin))
                                                  && x.sucursal_id == id_sucursal).ToList();
                cantidad_ventas_hoy = calcularTotalVenta(venta);
            }
            ViewBag.cantidad_ventas_hoy = cantidad_ventas_hoy;
            ViewBag.cantidad_ventas_ayer = cantidad_ventas_ayer;

            if (id_sucursal == 0)
            {
                venta = db.venta.Where(x => ((x.catalogo_status_id == 1 && x.Fecha_entrega >= hoy_inicio && x.Fecha_entrega <= hoy_fin))).ToList();
                cantidad_ventas_sucursal = calcularTotalVenta(venta);
                venta = db.venta.Where(x => ((x.catalogo_status_id == 9 && x.fecha_liquidacion >= hoy_inicio && x.fecha_liquidacion <= hoy_fin)
                                                  || (x.catalogo_status_id == 10 && x.fecha_liquidacion >= hoy_inicio && x.fecha_liquidacion <= hoy_fin)
                                                  || (x.catalogo_status_id == 11 && x.fecha_liquidacion >= hoy_inicio && x.fecha_liquidacion <= hoy_fin))).ToList();
                cantidad_ventas_online = calcularTotalVenta(venta);
            }
            else
            {
                venta = db.venta.Where(x => ((x.catalogo_status_id == 1 && x.Fecha_entrega >= hoy_inicio && x.Fecha_entrega <= hoy_fin))
                                                  && x.sucursal_id == id_sucursal).ToList();
                cantidad_ventas_sucursal = calcularTotalVenta(venta);
                venta = db.venta.Where(x => ((x.catalogo_status_id == 9 && x.fecha_liquidacion >= hoy_inicio && x.fecha_liquidacion <= hoy_fin)
                                                  || (x.catalogo_status_id == 10 && x.fecha_liquidacion >= hoy_inicio && x.fecha_liquidacion <= hoy_fin)
                                                  || (x.catalogo_status_id == 11 && x.fecha_liquidacion >= hoy_inicio && x.fecha_liquidacion <= hoy_fin))
                                                  && x.sucursal_id == id_sucursal).ToList();
                cantidad_ventas_online = calcularTotalVenta(venta);
            }
            ViewBag.cantidad_ventas_sucursal = cantidad_ventas_sucursal;
            ViewBag.cantidad_ventas_online = cantidad_ventas_online;

            var hora_actual_inicio = Convert.ToDateTime(DateTime.Now.AddMinutes(DateTime.Now.Minute * (-1)).AddSeconds(DateTime.Now.Second * (-1)), IdiomaFecha);
            var hora_actual_fin = Convert.ToDateTime(hora_actual_inicio.AddMinutes(59).AddSeconds(59), IdiomaFecha);
            if (id_sucursal == 0)
            {
                venta = db.venta.Where(x => ((x.catalogo_status_id == 1 && x.Fecha_entrega >= hora_actual_inicio && x.Fecha_entrega <= hora_actual_fin)
                                                  || (x.catalogo_status_id == 9 && x.fecha_liquidacion >= hora_actual_inicio && x.fecha_liquidacion <= hora_actual_fin)
                                                  || (x.catalogo_status_id == 10 && x.fecha_liquidacion >= hora_actual_inicio && x.fecha_liquidacion <= hora_actual_fin)
                                                  || (x.catalogo_status_id == 11 && x.fecha_liquidacion >= hora_actual_inicio && x.fecha_liquidacion <= hora_actual_fin))).ToList();
            }
            else
            {
                venta = db.venta.Where(x => ((x.catalogo_status_id == 1 && x.Fecha_entrega >= hora_actual_inicio && x.Fecha_entrega <= hora_actual_fin)
                                                  || (x.catalogo_status_id == 9 && x.fecha_liquidacion >= hora_actual_inicio && x.fecha_liquidacion <= hora_actual_fin)
                                                  || (x.catalogo_status_id == 10 && x.fecha_liquidacion >= hora_actual_inicio && x.fecha_liquidacion <= hora_actual_fin)
                                                  || (x.catalogo_status_id == 11 && x.fecha_liquidacion >= hora_actual_inicio && x.fecha_liquidacion <= hora_actual_fin))
                                                  && x.sucursal_id == id_sucursal).ToList();
            }
            ViewBag.venta = venta;

            if (id_sucursal == 0)
            {
                venta = db.venta.Where(x => x.multiple_pago.Where(y => y.id_tipo_pago_venta == 4).Count() > 0 && x.aduedo != 0).ToList();
            }
            else
            {
                venta = db.venta.Where(x => x.multiple_pago.Where(y => y.id_tipo_pago_venta == 4).Count() > 0 && x.aduedo != 0 && x.sucursal_id == id_sucursal).ToList();
            }
            ViewBag.creditos = venta;

            if (id_sucursal == 0)
            {
                var grafica = new List<DatoGrafica>();
                for (int y = 1; y <= 31; y++)
                {
                    int nombre;
                    Decimal columna1, columna2, columna3;
                    nombre = y;
                    if (DateTime.DaysInMonth(mes_dia_inicio.Year, mes_dia_inicio.Month) >= y)
                    {
                        if (db.venta.Where(x => ((x.catalogo_status_id == 1 && x.Fecha_entrega >= mes_dia_inicio && x.Fecha_entrega <= mes_dia_fin)
                                                  || (x.catalogo_status_id == 9 && x.fecha_liquidacion >= mes_dia_inicio && x.fecha_liquidacion <= mes_dia_fin)
                                                  || (x.catalogo_status_id == 10 && x.fecha_liquidacion >= mes_dia_inicio && x.fecha_liquidacion <= mes_dia_fin)
                                                  || (x.catalogo_status_id == 11 && x.fecha_liquidacion >= mes_dia_inicio && x.fecha_liquidacion <= mes_dia_fin))).Any())
                        {
                            venta = db.venta.Where(x => ((x.catalogo_status_id == 1 && x.Fecha_entrega >= mes_dia_inicio && x.Fecha_entrega <= mes_dia_fin)
                                                  || (x.catalogo_status_id == 9 && x.fecha_liquidacion >= mes_dia_inicio && x.fecha_liquidacion <= mes_dia_fin)
                                                  || (x.catalogo_status_id == 10 && x.fecha_liquidacion >= mes_dia_inicio && x.fecha_liquidacion <= mes_dia_fin)
                                                  || (x.catalogo_status_id == 11 && x.fecha_liquidacion >= mes_dia_inicio && x.fecha_liquidacion <= mes_dia_fin))).ToList();
                            columna1 = calcularTotalVenta(venta);
                        }
                        else
                        {
                            columna1 = 0;
                        }
                    }
                    else
                    {
                        columna1 = 0;
                    }
                    if (DateTime.DaysInMonth(mes_pasado_dia_inicio.Year, mes_pasado_dia_inicio.Month) >= y)
                    {
                        if (db.venta.Where(x => ((x.catalogo_status_id == 1 && x.Fecha_entrega >= mes_pasado_dia_inicio && x.Fecha_entrega <= mes_pasado_dia_fin)
                                                  || (x.catalogo_status_id == 9 && x.fecha_liquidacion >= mes_pasado_dia_inicio && x.fecha_liquidacion <= mes_pasado_dia_fin)
                                                  || (x.catalogo_status_id == 10 && x.fecha_liquidacion >= mes_pasado_dia_inicio && x.fecha_liquidacion <= mes_pasado_dia_fin)
                                                  || (x.catalogo_status_id == 11 && x.fecha_liquidacion >= mes_pasado_dia_inicio && x.fecha_liquidacion <= mes_pasado_dia_fin))).Any())
                        {
                            venta = db.venta.Where(x => ((x.catalogo_status_id == 1 && x.Fecha_entrega >= mes_pasado_dia_inicio && x.Fecha_entrega <= mes_pasado_dia_fin)
                                                  || (x.catalogo_status_id == 9 && x.fecha_liquidacion >= mes_pasado_dia_inicio && x.fecha_liquidacion <= mes_pasado_dia_fin)
                                                  || (x.catalogo_status_id == 10 && x.fecha_liquidacion >= mes_pasado_dia_inicio && x.fecha_liquidacion <= mes_pasado_dia_fin)
                                                  || (x.catalogo_status_id == 11 && x.fecha_liquidacion >= mes_pasado_dia_inicio && x.fecha_liquidacion <= mes_pasado_dia_fin))).ToList();
                            columna2 = calcularTotalVenta(venta);
                        }
                        else
                        {
                            columna2 = 0;
                        }
                    }
                    else
                    {
                        columna2 = 0;
                    }
                    if (DateTime.DaysInMonth(mes_antepasado_dia_inicio.Year, mes_antepasado_dia_inicio.Month) >= y)
                    {
                        if (db.venta.Where(x => ((x.catalogo_status_id == 1 && x.Fecha_entrega >= mes_antepasado_dia_inicio && x.Fecha_entrega <= mes_antepasado_dia_fin)
                                                  || (x.catalogo_status_id == 9 && x.fecha_liquidacion >= mes_antepasado_dia_inicio && x.fecha_liquidacion <= mes_antepasado_dia_fin)
                                                  || (x.catalogo_status_id == 10 && x.fecha_liquidacion >= mes_antepasado_dia_inicio && x.fecha_liquidacion <= mes_antepasado_dia_fin)
                                                  || (x.catalogo_status_id == 11 && x.fecha_liquidacion >= mes_antepasado_dia_inicio && x.fecha_liquidacion <= mes_antepasado_dia_fin))).Any())
                        {
                            venta = db.venta.Where(x => ((x.catalogo_status_id == 1 && x.Fecha_entrega >= mes_antepasado_dia_inicio && x.Fecha_entrega <= mes_antepasado_dia_fin)
                                                  || (x.catalogo_status_id == 9 && x.fecha_liquidacion >= mes_antepasado_dia_inicio && x.fecha_liquidacion <= mes_antepasado_dia_fin)
                                                  || (x.catalogo_status_id == 10 && x.fecha_liquidacion >= mes_antepasado_dia_inicio && x.fecha_liquidacion <= mes_antepasado_dia_fin)
                                                  || (x.catalogo_status_id == 11 && x.fecha_liquidacion >= mes_antepasado_dia_inicio && x.fecha_liquidacion <= mes_antepasado_dia_fin))).ToList();
                            columna3 = calcularTotalVenta(venta);
                        }
                        else
                        {
                            columna3 = 0;
                        }
                    }
                    else
                    {
                        columna3 = 0;
                    }
                    grafica.Add(new DatoGrafica
                    {
                        nombre = nombre,
                        columna1 = columna1,
                        columna2 = columna2,
                        columna3 = columna3,
                    });
                    mes_dia_inicio = mes_dia_inicio.AddDays(1);
                    mes_dia_fin = mes_dia_fin.AddDays(1);
                    mes_pasado_dia_inicio = mes_pasado_dia_inicio.AddDays(1);
                    mes_pasado_dia_fin = mes_pasado_dia_fin.AddDays(1);
                    mes_antepasado_dia_inicio = mes_antepasado_dia_inicio.AddDays(1);
                    mes_antepasado_dia_fin = mes_antepasado_dia_fin.AddDays(1);
                }
                var jsonSerialiser = new JavaScriptSerializer();
                var json = jsonSerialiser.Serialize(grafica);
                ViewBag.grafica = json;
            }
            else
            {
                var grafica = new List<DatoGrafica>();
                for (int y = 1; y <= 31; y++)
                {
                    int nombre;
                    Decimal columna1, columna2, columna3;
                    nombre = y;
                    if (DateTime.DaysInMonth(mes_dia_inicio.Year, mes_dia_inicio.Month) >= y)
                    {
                        if (db.venta.Where(x => ((x.catalogo_status_id == 1 && x.Fecha_entrega >= mes_dia_inicio && x.Fecha_entrega <= mes_dia_fin)
                                                  || (x.catalogo_status_id == 9 && x.fecha_liquidacion >= mes_dia_inicio && x.fecha_liquidacion <= mes_dia_fin)
                                                  || (x.catalogo_status_id == 10 && x.fecha_liquidacion >= mes_dia_inicio && x.fecha_liquidacion <= mes_dia_fin)
                                                  || (x.catalogo_status_id == 11 && x.fecha_liquidacion >= mes_dia_inicio && x.fecha_liquidacion <= mes_dia_fin)) && x.sucursal_id == id_sucursal).Any())
                        {
                            venta = db.venta.Where(x => ((x.catalogo_status_id == 1 && x.Fecha_entrega >= mes_dia_inicio && x.Fecha_entrega <= mes_dia_fin)
                                                  || (x.catalogo_status_id == 9 && x.fecha_liquidacion >= mes_dia_inicio && x.fecha_liquidacion <= mes_dia_fin)
                                                  || (x.catalogo_status_id == 10 && x.fecha_liquidacion >= mes_dia_inicio && x.fecha_liquidacion <= mes_dia_fin)
                                                  || (x.catalogo_status_id == 11 && x.fecha_liquidacion >= mes_dia_inicio && x.fecha_liquidacion <= mes_dia_fin)) && x.sucursal_id == id_sucursal).ToList();
                            columna1 = calcularTotalVenta(venta);
                        }
                        else
                        {
                            columna1 = 0;
                        }
                    }
                    else
                    {
                        columna1 = 0;
                    }
                    if (DateTime.DaysInMonth(mes_pasado_dia_inicio.Year, mes_pasado_dia_inicio.Month) >= y)
                    {
                        if (db.venta.Where(x => ((x.catalogo_status_id == 1 && x.Fecha_entrega >= mes_pasado_dia_inicio && x.Fecha_entrega <= mes_pasado_dia_fin)
                                                  || (x.catalogo_status_id == 9 && x.fecha_liquidacion >= mes_pasado_dia_inicio && x.fecha_liquidacion <= mes_pasado_dia_fin)
                                                  || (x.catalogo_status_id == 10 && x.fecha_liquidacion >= mes_pasado_dia_inicio && x.fecha_liquidacion <= mes_pasado_dia_fin)
                                                  || (x.catalogo_status_id == 11 && x.fecha_liquidacion >= mes_pasado_dia_inicio && x.fecha_liquidacion <= mes_pasado_dia_fin)) && x.sucursal_id == id_sucursal).Any())
                        {
                            venta = db.venta.Where(x => ((x.catalogo_status_id == 1 && x.Fecha_entrega >= mes_pasado_dia_inicio && x.Fecha_entrega <= mes_pasado_dia_fin)
                                                  || (x.catalogo_status_id == 9 && x.fecha_liquidacion >= mes_pasado_dia_inicio && x.fecha_liquidacion <= mes_pasado_dia_fin)
                                                  || (x.catalogo_status_id == 10 && x.fecha_liquidacion >= mes_pasado_dia_inicio && x.fecha_liquidacion <= mes_pasado_dia_fin)
                                                  || (x.catalogo_status_id == 11 && x.fecha_liquidacion >= mes_pasado_dia_inicio && x.fecha_liquidacion <= mes_pasado_dia_fin)) && x.sucursal_id == id_sucursal).ToList();
                            columna2 = calcularTotalVenta(venta);
                        }
                        else
                        {
                            columna2 = 0;
                        }
                    }
                    else
                    {
                        columna2 = 0;
                    }
                    if (DateTime.DaysInMonth(mes_antepasado_dia_inicio.Year, mes_antepasado_dia_inicio.Month) >= y)
                    {
                        if (db.venta.Where(x => ((x.catalogo_status_id == 1 && x.Fecha_entrega >= mes_antepasado_dia_inicio && x.Fecha_entrega <= mes_antepasado_dia_fin)
                                                  || (x.catalogo_status_id == 9 && x.fecha_liquidacion >= mes_antepasado_dia_inicio && x.fecha_liquidacion <= mes_antepasado_dia_fin)
                                                  || (x.catalogo_status_id == 10 && x.fecha_liquidacion >= mes_antepasado_dia_inicio && x.fecha_liquidacion <= mes_antepasado_dia_fin)
                                                  || (x.catalogo_status_id == 11 && x.fecha_liquidacion >= mes_antepasado_dia_inicio && x.fecha_liquidacion <= mes_antepasado_dia_fin)) && x.sucursal_id == id_sucursal).Any())
                        {
                            venta = db.venta.Where(x => ((x.catalogo_status_id == 1 && x.Fecha_entrega >= mes_antepasado_dia_inicio && x.Fecha_entrega <= mes_antepasado_dia_fin)
                                                  || (x.catalogo_status_id == 9 && x.fecha_liquidacion >= mes_antepasado_dia_inicio && x.fecha_liquidacion <= mes_antepasado_dia_fin)
                                                  || (x.catalogo_status_id == 10 && x.fecha_liquidacion >= mes_antepasado_dia_inicio && x.fecha_liquidacion <= mes_antepasado_dia_fin)
                                                  || (x.catalogo_status_id == 11 && x.fecha_liquidacion >= mes_antepasado_dia_inicio && x.fecha_liquidacion <= mes_antepasado_dia_fin)) && x.sucursal_id == id_sucursal).ToList();
                            columna3 = calcularTotalVenta(venta);
                        }
                        else
                        {
                            columna3 = 0;
                        }
                    }
                    else
                    {
                        columna3 = 0;
                    }
                    grafica.Add(new DatoGrafica
                    {
                        nombre = nombre,
                        columna1 = columna1,
                        columna2 = columna2,
                        columna3 = columna3,
                    });
                    mes_dia_inicio = mes_dia_inicio.AddDays(1);
                    mes_dia_fin = mes_dia_fin.AddDays(1);
                    mes_pasado_dia_inicio = mes_pasado_dia_inicio.AddDays(1);
                    mes_pasado_dia_fin = mes_pasado_dia_fin.AddDays(1);
                    mes_antepasado_dia_inicio = mes_antepasado_dia_inicio.AddDays(1);
                    mes_antepasado_dia_fin = mes_antepasado_dia_fin.AddDays(1);
                }
                var jsonSerialiser = new JavaScriptSerializer();
                var json = jsonSerialiser.Serialize(grafica);
                ViewBag.grafica = json;
            }
            return PartialView();
        }

        public Decimal calcularTotalVenta(List<venta> lista)
        {
            Decimal total = 0;
            try
            {
                foreach (venta v in lista)
                {
                    total += (Decimal)v.total;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return total;
        }

        public Boolean crearInventariosNoExistentes(int id_producto)
        {
            try
            {
                List<int> lista_id_sucursales = db.sucursal.Select(x => x.id).ToList();
                foreach (int x in lista_id_sucursales)
                {
                    inventario inventario = db.inventario.FirstOrDefault(v => v.Producto_id == id_producto && v.sucursal_id == x);
                    // si no existe el inventario entonces crearlo
                    if (inventario == null)
                    {
                        inventario inventario_nuevo = new inventario();
                        inventario_nuevo.Producto_id = id_producto;
                        inventario_nuevo.sucursal_id = x;
                        inventario_nuevo.cantidad = 0;
                        db.inventario.Add(inventario_nuevo);
                        db.SaveChanges();
                    }
                }
                return true;
            }
            catch (Exception)
            {
                return false;
                throw;
            }
        }

        public ViewResult TerminosCondiciones()
        {
            return View();
        }

        public void FallaSistema(String controlador, String funcion, String error)
        {
            string plataforma = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~")).Replace("http://", "");
            CDO.Message message = new CDO.Message();
            CDO.IConfiguration configuration = message.Configuration;
            ADODB.Fields fields = configuration.Fields;
            ADODB.Field field = fields["http://schemas.microsoft.com/cdo/configuration/smtpserver"];
            field.Value = "smtp.zoho.com";
            field = fields["http://schemas.microsoft.com/cdo/configuration/smtpserverport"];
            field.Value = 465;
            field = fields["http://schemas.microsoft.com/cdo/configuration/sendusing"];
            field.Value = CDO.CdoSendUsing.cdoSendUsingPort;
            field = fields["http://schemas.microsoft.com/cdo/configuration/smtpauthenticate"];
            field.Value = CDO.CdoProtocolsAuthentication.cdoBasic;
            field = fields["http://schemas.microsoft.com/cdo/configuration/sendusername"];
            field.Value = "soporte@quianty.com";
            field = fields["http://schemas.microsoft.com/cdo/configuration/sendpassword"];
            field.Value = "soportequianty";
            field = fields["http://schemas.microsoft.com/cdo/configuration/smtpusessl"];
            field.Value = "true";
            fields.Update();
            message.From = "soporte@quianty.com";
            message.To = "soporte@quianty.com";
            message.Subject = plataforma + " - Soporte POS - Quianty Tecnologias ";
            message.HTMLBody = "Controlador: " + controlador + "<br>" + "Funcion: " + funcion + "<br>" + "Error: " + error;
            message.Send();
        }

    }
}
