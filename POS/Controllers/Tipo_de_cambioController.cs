﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using POS.Models;

namespace POS.Controllers
{
    public class Tipo_de_cambioController : Controller
    {

        moragas_pruebasEntities1 db = new moragas_pruebasEntities1();

        public ActionResult Index()
        {
            return View();
        }

        public decimal traerPrecioActualDolar(int idSucursal)
        {
            decimal precio_dolar = db.sucursal.Find(idSucursal).precio_dolar.Value;
            return precio_dolar;
        }

        public PartialViewResult ModalEditarPrecioDolar(int idSucursal)
        {
            decimal precio_dolar = db.sucursal.Find(idSucursal).precio_dolar.Value;
            ViewBag.precio_dolar = precio_dolar;
            return PartialView();
        }

        public ActionResult EditarPrecioDolar(decimal nuevo_precio_dolar, int idSucursal)
        {
            bool success = false;
            string error = "";
            try
            {
                sucursal s = db.sucursal.Find(idSucursal);
                s.precio_dolar = nuevo_precio_dolar;

                db.SaveChanges();
                success = true;
            }
            catch (Exception e)
            {
                error = e.Message;
            }
            var respuesta = new
            {
                success = success,
                error = error
            };
            return Json(respuesta, JsonRequestBehavior.AllowGet);
        }

    }
}
