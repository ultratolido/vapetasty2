﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using POS.Models;
using System.Data.Entity;
using POS.Fn;

namespace POS.Controllers
{
    public class ClienteController : Controller
    {

        moragas_pruebasEntities1 db = new moragas_pruebasEntities1();

        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName = "Catalogo De Clientes")]
        public ActionResult Index()
        {
            List<int> ListaSucursalesPermitidas = Fn.usuario_logeado.Sucursales_id();
            ViewBag.id_sucursal = new SelectList(db.sucursal.Where(x => ListaSucursalesPermitidas.Contains(x.id)).OrderByDescending(y => y.id), "id", "nombre_sucursal");
            return View();
        }

        public PartialViewResult VistaParcialCliente(int id_sucursal)
        {
            List<cliente> ListaClientes = db.cliente.Where(y => y.id_sucursal == id_sucursal).OrderBy(x => x.Razon_social).ToList();
            ViewBag.ListaClientes = ListaClientes;
            return PartialView();
        }

        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName = "Crear Cliente")]
        public ActionResult NuevoCliente()
        {
            ViewBag.id_sucursal = new SelectList(db.sucursal.OrderByDescending(y => y.id), "id", "nombre_sucursal");
            ViewBag.TipoCliente_id = new SelectList(db.tipocliente.OrderBy(x => x.orden), "id", "Tipo");
            return View();
        }

        [HttpPost]
        public ActionResult NuevoCliente(cliente cliente)
        {
            cliente.Adeudo_credito = 0;
            if (cliente.dias_credito == null)
                cliente.dias_credito = 0;
            if (cliente.Limite_credito == null)
                cliente.Limite_credito = 0;
            db.cliente.Add(cliente);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName = "Editar Cliente")]
        public ActionResult EditarCliente(int id)
        {
            cliente cliente = db.cliente.Find(id);
            ViewBag.id_sucursal = new SelectList(db.sucursal.OrderByDescending(y => y.id), "id", "nombre_sucursal", cliente.id_sucursal);
            ViewBag.TipoCliente_id = new SelectList(db.tipocliente.OrderBy(x => x.orden), "id", "Tipo", cliente.TipoCliente_id);
            return View(cliente);
        }

        [HttpPost]
        public ActionResult EditarCliente(cliente cliente)
        {
            if (cliente.dias_credito == null)
                cliente.dias_credito = 0;
            if (cliente.Limite_credito == null)
                cliente.Limite_credito = 0;
            db.Entry(cliente).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public PartialViewResult ModalListaClientes(int idSucursal)
        {
            ViewBag.nombreSucursal = db.sucursal.Find(idSucursal).nombre_sucursal;
            return PartialView(db.cliente);
        }

        public bool VerificarRazonSocial(int id, String Razon_social)
        {
            bool existe;
            if (id == 0)
                existe = db.cliente.Where(x => x.Razon_social.ToLower().Equals(Razon_social.ToLower())).Any();
            else
                existe = db.cliente.Where(x => x.id != id && x.Razon_social.ToLower().Equals(Razon_social.ToLower())).Any();
            return existe;
        }

        public bool VerificarRFC(int id, String RFC)
        {
            bool existe;
            if (id == 0)
                existe = db.cliente.Where(x => x.RFC.ToLower().Equals(RFC.ToLower())).Any();
            else
                existe = db.cliente.Where(x => x.id != id && x.RFC.ToLower().Equals(RFC.ToLower())).Any();
            return existe;
        }

        public bool VerificarCorreo(int id, String correo)
        {
            bool existe;
            if (id == 0)
                existe = db.cliente.Where(x => x.correo.ToLower().Equals(correo.ToLower())).Any();
            else
                existe = db.cliente.Where(x => x.id != id && x.correo.ToLower().Equals(correo.ToLower())).Any();
            return existe;
        }

        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName = "Activar Cliente")]
        public ActionResult ActivarClienteTiendaOnline(int id)
        {
            try
            {
                cliente cliente = db.cliente.Find(id);
                configuracion_general configuracion_general = Configuracion.TraerConfiguracionGeneral();
                configuracion_sucursal_correo configuracion_sucursal_correo = Configuracion.TraerConfiguracionSucursales().Where(x => x.id == cliente.id_sucursal).Single().configuracion_sucursal_correo.ToList().First();
                cliente.id_estatus_cliente = 1;
                db.Entry(cliente).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                CDO.Message message = new CDO.Message();
                CDO.IConfiguration configuration = message.Configuration;
                ADODB.Fields fields = configuration.Fields;
                ADODB.Field field = fields["http://schemas.microsoft.com/cdo/configuration/smtpserver"];
                field.Value = configuracion_sucursal_correo.smtp;
                field = fields["http://schemas.microsoft.com/cdo/configuration/smtpserverport"];
                field.Value = configuracion_sucursal_correo.puerto;
                field = fields["http://schemas.microsoft.com/cdo/configuration/sendusing"];
                field.Value = CDO.CdoSendUsing.cdoSendUsingPort;
                field = fields["http://schemas.microsoft.com/cdo/configuration/smtpauthenticate"];
                field.Value = CDO.CdoProtocolsAuthentication.cdoBasic;
                field = fields["http://schemas.microsoft.com/cdo/configuration/sendusername"];
                field.Value = configuracion_sucursal_correo.correo;
                field = fields["http://schemas.microsoft.com/cdo/configuration/sendpassword"];
                field.Value = configuracion_sucursal_correo.contrasena;
                field = fields["http://schemas.microsoft.com/cdo/configuration/smtpusessl"];
                field.Value = configuracion_sucursal_correo.ssl;
                fields.Update();
                message.From = configuracion_sucursal_correo.correo;
                message.To = cliente.correo;
                message.Subject = "Tienda Online " + configuracion_general.marca;
                message.HTMLBody = "Hola " + cliente.Razon_social + "<br><br>" +
                                   "¡Bienvenido a la Tienda Online de " + configuracion_general.marca + "!<br><br>" +
                                   "A partir de ahora su cuenta esta activa, podrá realizar sus pedidos a partir de este momento, recuerda leer nuestras clausulas y términos.<br>" +
                                   "<span><a href='" + configuracion_general.pagina_web + "'>Visitar " + configuracion_general.pagina_web + "</a></span><br><br>" +
                                   "En caso de que no hayas solicitado esta cuenta o no reconozcas este mensaje, favor de ignorarlo.<br>" +
                                   "Saludos!<br><br>" +
                                   configuracion_general.marca;
                message.Send();
                var respuesta = new
                {
                    success = true
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                var respuesta = new
                {
                    success = false
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
        }

        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName = "Editar Tipo De Precio")]
        public PartialViewResult ModalEditarTipoPrecio(int id)
        {
            cliente cliente = db.cliente.Find(id);
            ViewBag.id_tipo_precio = new SelectList(db.tipocliente, "id", "Tipo", cliente.TipoCliente_id);
            ViewBag.id_cliente = cliente.id;
            return PartialView();
        }

        public ActionResult EditarTipoPrecio(int id, int id_tipo_precio)
        {
            try
            {
                cliente cliente = db.cliente.Find(id);
                cliente.TipoCliente_id = id_tipo_precio;
                db.Entry(cliente).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                var respuesta = new
                {
                    success = true
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                var respuesta = new
                {
                    success = false
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
