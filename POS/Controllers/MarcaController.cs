﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using POS.Models;
using System.Data;
using System.Data.Entity;
using POS.Fn;
using System.IO;

namespace POS.Controllers
{
    public class MarcaController : Controller
    {

        moragas_pruebasEntities1 db = new moragas_pruebasEntities1();

        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName = "Catalogo De Marcas")]
        public ActionResult Index()
        {
            ViewBag.marcas = db.marca.ToList();
            return View();
        }

        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName = "Nueva Marca")]
        public ViewResult NuevaMarca()
        {
            return View();
        }

        [HttpPost]
        public ActionResult NuevaMarca(marca marca, HttpPostedFileBase imagen_marca)
        {
            String UploadFolderTiendaOnline = new DirectoryInfo(Server.MapPath("~/")).Parent.FullName + "\\Tienda Online\\UploadFolder";
            db.marca.Add(marca);
            db.SaveChanges();
            if (imagen_marca != null)
            {
                String savedFileName = UploadFolderTiendaOnline + "\\imagen_marca\\" + marca.id + "-" + imagen_marca.FileName;
                imagen_marca.SaveAs(savedFileName);
                marca.imagen = marca.id + "-" + imagen_marca.FileName;
                db.Entry(marca).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName = "Editar Marca")]
        public ViewResult EditarMarca(int id)
        {
            marca marca = db.marca.Find(id);
            return View(marca);
        }

        [HttpPost]
        public ActionResult EditarMarca(marca marca, HttpPostedFileBase imagen_marca, int? id_imagen_eliminada)
        {
            String UploadFolderTiendaOnline = new DirectoryInfo(Server.MapPath("~/")).Parent.FullName + "\\Tienda Online\\UploadFolder";
            db.Entry(marca).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            if (imagen_marca != null)
            {
                if (marca.imagen != null)
                {
                    System.IO.File.Delete(UploadFolderTiendaOnline + "\\imagen_marca\\" + marca.imagen);

                }
                String savedFileName = UploadFolderTiendaOnline + "\\imagen_marca\\" + marca.id + "-" + imagen_marca.FileName;
                imagen_marca.SaveAs(savedFileName);
                marca.imagen = marca.id + "-" + imagen_marca.FileName;
                db.Entry(marca).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            if (id_imagen_eliminada != null && imagen_marca == null)
            {
                System.IO.File.Delete(UploadFolderTiendaOnline + "\\imagen_marca\\" + marca.imagen);
                marca.imagen = null;
                db.Entry(marca).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        public bool VerificarMarca(int id, String marca)
        {
            bool existe;
            if (id == 0)
                existe = db.marca.Where(x => x.nombre.ToLower().Equals(marca.ToLower())).Any();
            else
                existe = db.marca.Where(x => x.id != id && x.nombre.ToLower().Equals(marca.ToLower())).Any();
            return existe;
        }

    }
}
