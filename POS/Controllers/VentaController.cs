﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using POS.Models;
using System.Data;
using System.IO;
using POS.Fn;

namespace POS.Controllers
{
    public class VentaController : Controller
    {

        private moragas_pruebasEntities1 db = new moragas_pruebasEntities1();
        private CultureInfo IdiomaFecha = new CultureInfo("es-MX");

        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName = "Nueva Venta")]
        public ActionResult NuevaVenta()
        {
            ViewBag.ListaTipoPago = db.tipo_pago_venta.ToList();
            return View();
        }

        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName = "Nueva Venta Nueva")]
        public ActionResult NuevaVentaNueva()
        {
            List<int> ListaSucursalesPermitidas = usuario_logeado.Sucursales_id().Where(x => x != 1).ToList();
            ViewBag.id_sucursal = new SelectList(db.sucursal.Where(x => ListaSucursalesPermitidas.Contains(x.id)).OrderByDescending(y => y.id), "id", "nombre_sucursal");
            ViewBag.ListaTipoPago = db.tipo_pago_venta.ToList();
            return View();
        }

        public PartialViewResult VistaParcialNuevaVenta()
        {
            List<int> ListaSucursalesPermitidas = usuario_logeado.Sucursales_id().Where(x => x != 1).ToList();
            ViewBag.id_sucursal = new SelectList(db.sucursal.Where(x => ListaSucursalesPermitidas.Contains(x.id)).OrderByDescending(y => y.id), "id", "nombre_sucursal");
            return PartialView();
        }

        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName = "Entregar Ventas")]
        public ActionResult EntregaVenta()
        {
            List<int> ListaSucursalesPermitidas = Fn.usuario_logeado.Sucursales_id().Where(x => x != 1).ToList();
            ViewBag.id_sucursal = new SelectList(db.sucursal.Where(x => ListaSucursalesPermitidas.Contains(x.id)).OrderByDescending(y => y.id), "id", "nombre_sucursal");
            ViewBag.ListaTipoPago = db.tipo_pago_venta.ToList();
            return View();
        }

        public PartialViewResult VistaParcialEntregaVenta(int idSucursal)
        {
            CultureInfo culture = new CultureInfo("es-MX");
            DateTime hoy = Convert.ToDateTime(DateTime.Now.ToString("dd/MM/yyyy"), culture);
            List<venta> ListaVentas = db.venta.Where(x => x.catalogo_status_id == 3 && x.fecha >= hoy && x.sucursal_id == idSucursal).ToList();
            ViewBag.ListaVentas = ListaVentas;
            return PartialView();
        }

        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName = "Cotizaciones")]
        public ActionResult Cotizacion()
        {
            CultureInfo culture = new CultureInfo("es-MX");
            List<int> ListaSucursalesPermitidas = Fn.usuario_logeado.Sucursales_id().Where(x => x != 1).ToList(); ;
            ViewBag.id_sucursal = new SelectList(db.sucursal.Where(x => ListaSucursalesPermitidas.Contains(x.id)).OrderByDescending(y => y.id), "id", "nombre_sucursal");
            ViewBag.HoyInicio = DateTime.Now.ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.HoyFin = DateTime.Now.ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.AyerInicio = DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.AyerFin = DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.AntierInicio = DateTime.Now.AddDays(-2).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.AntierFin = DateTime.Now.AddDays(-2).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.SemanaActualInicio = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.SemanaActualFin = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(6).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.SemanaPasadaInicio = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(-7).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.SemanaPasadaFin = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(-7).AddDays(6).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.SemanaAntepasadaInicio = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(-14).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.SemanaAntepasadaFin = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(-14).AddDays(6).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            DateTime hoy = Convert.ToDateTime(DateTime.Now.ToShortDateString());
            int numero_de_dia = hoy.Day;
            if (numero_de_dia > 15)
            //segunda quincena
            {
                ViewBag.QuincenaActualInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 15).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaActualFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.Month)).ToString("MM/dd/yyyy") + " 11:59:59 PM";
                ViewBag.QuincenaPasadaInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaPasadaFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddDays(15).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
                ViewBag.QuincenaAntepasadaInicio = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, 15).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaAntepasadaFin = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month)).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            }
            else //primera quincena
            {
                ViewBag.QuincenaActualInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaActualFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddDays(15).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
                ViewBag.QuincenaPasadaInicio = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, 15).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaPasadaFin = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month)).ToString("MM/dd/yyyy") + " 11:59:59 PM";
                ViewBag.QuincenaAntepasadaInicio = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaAntepasadaFin = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, 1).AddDays(15).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            }
            ViewBag.MesActualInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.MesActualFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(1).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.MesPasadoInicioFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.MesPasadoFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-1).AddMonths(1).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.MesAntepasadoInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-2).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.MesAntepasadoFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-2).AddMonths(1).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            return View();
        }

        public PartialViewResult VistaParcialCotizacion(int idSucursal, String periodo, String filtro)
        {
            CultureInfo culture = new CultureInfo("es-MX");
            List<venta> todas_cotizaciones_sucursal = db.venta.Where(x => x.catalogo_status_id == 4 && x.sucursal_id == idSucursal).OrderBy(x => x.Folio).ToList();
            List<venta> ListaCotizaciones = new List<venta>();
            if (filtro == "") //filtro por fecha
            {
                String[] fecha = Fn.Fechas.CalcularIncioFin(periodo);
                String inicio = fecha[0];
                String fin = fecha[1];

                DateTime _inicio = Convert.ToDateTime(inicio, culture);
                DateTime _fin = Convert.ToDateTime(fin, culture);
                ListaCotizaciones = todas_cotizaciones_sucursal.Where(x => x.fecha > _inicio && x.fecha < _fin).ToList();
            }
            else//filtro por id_cotizacion
            {
                foreach (venta c in todas_cotizaciones_sucursal)
                {
                    string serie_folio = Fn.Numeros.serie_folio_by_id(c.id);
                    if (serie_folio.Contains(filtro))
                        ListaCotizaciones.Add(c);
                }
            }
            ViewBag.ListaCotizaciones = ListaCotizaciones;
            return PartialView();
        }

        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName = "Editar Venta")]
        public ActionResult EditarVenta(int id)
        {
            venta venta = db.venta.Find(id);
            ViewBag.id_sucursal = new SelectList(db.sucursal.Where(x => x.id == venta.sucursal_id).OrderByDescending(y => y.id), "id", "nombre_sucursal");
            return View(venta);
        }

        public PartialViewResult ModalConfirmarPagoOnline(int idVenta)
        {
            configuracion_general configuracion_general = Fn.Configuracion.TraerConfiguracionGeneral();
            ViewBag.ListaTipoPago = db.tipo_pago_venta.ToList();
            ViewBag.ListaNotasCredito = db.nota_credito.Where(x => x.id_venta_aplicada == idVenta).ToList();
            ViewBag.ListaSucursales = db.sucursal.Where(x => x.alimenta_tienda_online == true).ToList();
            ViewBag.porcentaje_comision = configuracion_general.porcentaje_comision_pago_tarjeta;
            ViewBag.iva_producto_comision = db.producto.FirstOrDefault(x => x.Codigo.Equals("TPV_COMISION")).iva;
            return PartialView(db.venta.Find(idVenta));
        }

        public PartialViewResult ModalConfirmarEnvioOnline(int idVenta)
        {
            return PartialView(db.venta.Find(idVenta));
        }

        public ActionResult confirmar_envio_online(int idVenta, string descripcionEnvio)
        {
            try
            {
                venta v = db.venta.Find(idVenta);
                v.fechaEnvio = DateTime.Now;
                v.catalogo_status_id = 10;
                v.guiaEnvio = descripcionEnvio;
                db.Entry(v).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                var respuesta = new
                {
                    success = true
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                var respuesta = new
                {
                    success = false
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult confirmar_pago_online(int idVenta)
        {
            venta v = db.venta.Find(idVenta);
            v.fecha_liquidacion = DateTime.Now;
            v.catalogo_status_id = 9;
            return PartialView();
        }

        private UploadedFile RetrieveFileFromRequest()
        {
            string filename = null;
            string fileType = null;
            byte[] fileContents = null;
            string ID;

            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];
                fileContents = new byte[file.ContentLength];
                fileType = file.ContentType;
                filename = file.FileName;

            }
            else if (Request.ContentLength > 0)
            {
                fileContents = new byte[Request.ContentLength];
                Request.InputStream.Read(fileContents, 0, Request.ContentLength);
                filename = Request.Headers["X-File-Name"];
                fileType = Request.Headers["X-File-Type"];
            }
            return new UploadedFile()
            {
                Filename = filename,
                ContentType = fileType,
                FileSize = fileContents != null ? fileContents.Length : 0,
                Contents = fileContents
            };
        }

        [HttpPost]
        public void UploadArchivo(int idVenta)
        {
            String UploadFolderTiendaOnline = new DirectoryInfo(Server.MapPath("~/")).Parent.FullName + "\\Tienda Online\\UploadFolder";
            UploadedFile file = RetrieveFileFromRequest();
            string nombre_archivo = idVenta + "-" + file.Filename.Trim().Replace("&", "");
            string rootPath = AppDomain.CurrentDomain.BaseDirectory;
            //String savedFileIMG = rootPath + "/UploadFolder/adjuntos_cliente_compra/" + nombre_archivo;
            String savedFileIMG = UploadFolderTiendaOnline + "\\logo\\" + nombre_archivo;
            String extension = System.IO.Path.GetExtension(file.Filename).ToLower();

            //si el cliente tiene una imagen registrada buscarla y si existe eliminarla
            if (System.IO.File.Exists(savedFileIMG))
            {
                //si ya existe entonces agrega la fecha antes de la extensión para que tenga un nombre único
                string fecha = DateTime.Now.ToString("yyyyMMddHHmmss");
                nombre_archivo = nombre_archivo.Replace(extension, "") + fecha + extension;
            }
            //guardar archivo
            //string UploadFolderTiendaOnline = new DirectoryInfo(Server.MapPath("~/")).Parent.Parent.FullName + "\\Tienda\\Tienda Online\\UploadFolder";
            System.IO.File.WriteAllBytes(UploadFolderTiendaOnline + "\\adjuntos_cliente_compra\\" + nombre_archivo, file.Contents);
            db.SaveChanges();
        }
        
        public PartialViewResult VistaParcialTraerAnexosVenta(string idVenta)
        {
            String UploadFolderTiendaOnline = new DirectoryInfo(Server.MapPath("~/")).Parent.FullName + "\\Tienda Online\\UploadFolder";
            ViewBag.idVenta = idVenta;
            //String UploadFolderTiendaOnline = new DirectoryInfo(Server.MapPath("~/")).Parent.FullName + "\\Tienda Online\\UploadFolder";
            return PartialView(FnFiles.BuscarCoincidenciasArchivos(UploadFolderTiendaOnline + "\\adjuntos_cliente_compra\\", idVenta + "-"));
        }

        public PartialViewResult VistaParcialRevisarCambioPrecio(List<conceptos> lista_conceptos, int idVenta)
        {
            List<cambio_precio> lista_cambios_precio = new List<cambio_precio>();
            venta v = db.venta.Find(idVenta);
            foreach (conceptos_por_venta c in v.conceptos_por_venta)
            {
                Decimal precio_actual = Math.Round(lista_conceptos.Where(x => x.id_producto == c.Producto_id).First().precio_unitario, 2);
                if (c.precio_unitario != precio_actual)
                {
                    c.importe = precio_actual * c.cantidad;
                    cambio_precio cp = new cambio_precio();
                    cp.codigo = c.producto.Codigo;
                    cp.precio_anterior = c.precio_unitario_mas_iva.Value;
                    cp.precio_actual = precio_actual;
                    cp.diferencia = Math.Round(precio_actual - c.precio_unitario.Value, 2);
                    lista_cambios_precio.Add(cp);
                    c.precio_unitario = precio_actual;
                }
            }
            ViewBag.lista_cambios_precio = lista_cambios_precio;
            return PartialView();
        }

        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName = "Recuperar Cotizacion")]
        public ActionResult RecuperarVentaCotizacion(int id)
        {
            venta v = db.venta.Find(id);
            configuracion_general configuracion_general = Fn.Configuracion.TraerConfiguracionGeneral();
            if (v.catalogo_status_id != 4 && v.catalogo_status_id != 7)
            {//esta venta no es cotizacion
                return View("RecuperarVentaCotizacionError");
            }
            else
            {
                ViewBag.id_sucursal = new SelectList(db.sucursal.Where(x => x.id == v.sucursal_id).OrderByDescending(y => y.id), "id", "nombre_sucursal");
                //revisar si cambio el precio de algun concepto
                List<cambio_precio> lista_cambios_precio = new List<cambio_precio>();
                foreach (conceptos_por_venta c in v.conceptos_por_venta)
                {
                    Decimal precio = db.precio_por_tipocliente.Where(x => x.Producto_id == c.Producto_id && x.TipoCliente_id == c.TipoCliente_id && x.sucursal_id == v.sucursal_id).SingleOrDefault().Precio.Value;
                    if (configuracion_general.precios_netos == true)
                    {
                        Decimal iva_producto = 0;
                        Decimal cantidad_restar = 0;
                        if (c.producto.iva == null)
                        {
                            iva_producto = Convert.ToDecimal(configuracion_general.iva);
                        }
                        else
                        {
                            iva_producto = Convert.ToDecimal(c.producto.iva);
                        }
                        if (iva_producto > 0)
                        {
                            cantidad_restar = ((precio / (100 + iva_producto)) * iva_producto);
                        }
                        precio -= cantidad_restar;
                        //precio = Math.Round(precio, 2);
                        //precio = precio;
                    }
                    bool precioDolar = false;
                    Decimal precio_dolar_actual = v.sucursal.precio_dolar.Value;
                    if (c.producto.precio_dolar == null)
                    {
                        precioDolar = true;
                    }
                    else
                    {
                        if (c.producto.precio_dolar.Value == 1)
                        {
                            precioDolar = true;
                        }
                    }
                    if (precioDolar)
                    {
                        //precio = Math.Round(precio * precio_dolar_actual, 2);
                        precio = precio * precio_dolar_actual;
                    }
                    if (c.precio_unitario != Math.Round(precio, 2))
                    {
                        c.importe = precio * c.cantidad;
                        cambio_precio cp = new cambio_precio();
                        cp.codigo = c.producto.Codigo;
                        cp.precio_anterior = c.precio_unitario.Value;
                        cp.precio_actual = precio;
                        cp.diferencia = Math.Round(precio - c.precio_unitario.Value, 2);
                        lista_cambios_precio.Add(cp);
                        c.precio_unitario = precio;
                    }
                }
                ViewBag.lista_cambios_precio = lista_cambios_precio;
                if (v.catalogo_status_id == 7)
                    ViewBag.tipo_entrega = 8;
                else
                    ViewBag.tipo_entrega = 3;
                return View(v);
            }
        }

        public PartialViewResult ModalAnexosVenta(int id)
        {
            String UploadFolderTiendaOnline = new DirectoryInfo(Server.MapPath("~/")).Parent.FullName + "\\Tienda Online\\UploadFolder";
            ViewBag.venta = db.venta.Find(id);
            //string UploadFolderTiendaOnline = new DirectoryInfo(Server.MapPath("~/")).Parent.Parent.FullName + "\\Tienda\\Tienda Online\\UploadFolder";
            return PartialView(Fn.FnFiles.BuscarCoincidenciasArchivos(UploadFolderTiendaOnline + "\\adjuntos_cliente_compra\\", id + "-"));
        }

        public int TraerCountArchivosVenta(int idVenta)
        {
            String UploadFolderTiendaOnline = new DirectoryInfo(Server.MapPath("~/")).Parent.FullName + "\\Tienda Online\\UploadFolder";
            //string UploadFolderTiendaOnline = new DirectoryInfo(Server.MapPath("~/")).Parent.Parent.FullName + "\\Tienda\\Tienda Online\\UploadFolder";
            return Fn.FnFiles.BuscarCoincidenciasArchivos(UploadFolderTiendaOnline + "\\adjuntos_cliente_compra\\", idVenta + "-").Count();
        }

        public PartialViewResult VistaParcialTraerArchivosOrdenesCompra(int id_cotizacion)
        {
            String UploadFolderTiendaOnline = new DirectoryInfo(Server.MapPath("~/")).Parent.FullName + "\\Tienda Online\\UploadFolder";
            ViewBag.id_cotizacion = id_cotizacion;
            //string UploadFolderTiendaOnline = new DirectoryInfo(Server.MapPath("~/")).Parent.Parent.FullName + "\\Tienda\\Tienda Online\\UploadFolder";
            return PartialView(Fn.FnFiles.BuscarCoincidenciasArchivos(UploadFolderTiendaOnline + "\\adjuntos_cliente_compra\\", id_cotizacion + "-"));
        }
        
        public ActionResult ExisteDocumento(string FileName, string carpeta)
        {
            String UploadFolderTiendaOnline = new DirectoryInfo(Server.MapPath("~/")).Parent.FullName + "\\Tienda Online\\UploadFolder";
            //string UploadFolderTiendaOnline = new DirectoryInfo(Server.MapPath("~/")).Parent.Parent.FullName + "\\Tienda\\Tienda Online\\UploadFolder";
            string absolutePath = UploadFolderTiendaOnline + "\\" + carpeta + "\\";
            bool success = Fn.FnFiles.existeAbsolutePath(absolutePath, FileName);
            var respuesta = new
            {
                success = success
            };
            return Json(respuesta, JsonRequestBehavior.AllowGet);
        }

        public ActionResult eliminarArchivoAbsolutePath(string FileName, string carpeta)
        {
            bool success = false;
            try
            {
                String UploadFolderTiendaOnline = new DirectoryInfo(Server.MapPath("~/")).Parent.FullName + "\\Tienda Online\\UploadFolder";
                //string UploadFolderTiendaOnline = new DirectoryInfo(Server.MapPath("~/")).Parent.Parent.FullName + "\\Tienda\\Tienda Online\\UploadFolder";
                string absolutePath = UploadFolderTiendaOnline + "\\" + carpeta + "\\";
                if (Fn.FnFiles.existeAbsolutePath(absolutePath, FileName))
                    Fn.FnFiles.eliminaAbsolutePath(absolutePath, FileName);
                success = true;
            }
            catch (Exception)
            {
                success = false;
            }
            var respuesta = new
            {
                success = success
            };
            return Json(respuesta, JsonRequestBehavior.AllowGet);
        }
        
        public void descargarDocumento(string FileName, string carpeta)
        {
            String UploadFolderTiendaOnline = new DirectoryInfo(Server.MapPath("~/")).Parent.FullName + "\\Tienda Online\\UploadFolder";
            //string UploadFolderTiendaOnline = new DirectoryInfo(Server.MapPath("~/")).Parent.Parent.FullName + "\\Tienda\\Tienda Online\\UploadFolder";
            UploadFolderTiendaOnline += "\\" + carpeta + "\\";
            Fn.FnFiles.DescargarArchivoAbsolutePath(Response, UploadFolderTiendaOnline, FileName);
        }
        
        public PartialViewResult ModalConceptosCotizacion(int id_venta)
        {
            venta cotizacion = db.venta.Find(id_venta);
            ViewBag.cotizacion = cotizacion;
            return PartialView();
        }

        public PartialViewResult VistaParcialGenerarTicketVenta(int idVenta, String cambio)
        {
            List<descuento> ListaDescuentos;
            List<nota_credito> ListaNotasCredito;
            Decimal total_descuentos = 0;
            Decimal total_porcentaje_descuentos = 0;
            ListaNotasCredito = db.nota_credito.Where(x => x.id_venta_aplicada == idVenta).ToList();
            ListaDescuentos = db.descuento.Where(x => x.id_venta == idVenta).ToList();
            total_descuentos = (Decimal)ListaDescuentos.Select(x => x.monto).Sum();
            total_porcentaje_descuentos = (Decimal)ListaDescuentos.Select(x => x.porcentaje).Sum();

            ViewBag.ListaNotasCredito = ListaNotasCredito;
            ViewBag.ListaDescuentos = ListaDescuentos;
            ViewBag.total_descuentos = total_descuentos;
            ViewBag.total_porcentaje_descuentos = total_porcentaje_descuentos;
            ViewBag.cambio = cambio;
            venta v = db.venta.Find(idVenta);
            //COTIZACION
            if (v.catalogo_status_id == 4 || v.catalogo_status_id == 7)
            {
                return PartialView("VistaParcialGenerarTicketVentaCOTIZACION", db.venta.Find(idVenta));
            }
            //VENTA
            else
            {
                return PartialView(db.venta.Find(idVenta));
            }
            
        }

        public ActionResult confirmar_cambios_venta(int idVenta, int id_cliente, List<conceptos> lista_conceptos, String observaciones)
        {
            Decimal total = 0;
            Decimal iva = 0;
            Decimal subtotal = 0;
            bool success = false;
            try
            {
                venta v = db.venta.Find(idVenta);
                List<conceptos_por_venta> lista_concepto_por_venta = v.conceptos_por_venta.ToList();
                foreach (conceptos_por_venta concepto in lista_concepto_por_venta)
                {
                    db.conceptos_por_venta.Remove(concepto);
                }
                db.SaveChanges();
                //para agrugar por los concepos con el mismo codigo
                List<conceptos> conceptos_por_codigo = new List<conceptos>();
                //DISTINTOS CODIGOS POR VENTA
                foreach (var codigo in lista_conceptos.Select(x => x.codigo).Distinct())
                {
                    //DISTINTOS TIPOS PRECIO POR CODIGO
                    foreach (var idTipoCliente in lista_conceptos.Where(x => x.codigo == codigo).Select(x => x.tipoCliente_id).Distinct())
                    {
                        List<conceptos> temp_lista_conceptos = lista_conceptos.Where(x => x.codigo == codigo && x.tipoCliente_id == idTipoCliente).ToList();
                        conceptos c = new conceptos();
                        c.cantidad = temp_lista_conceptos.Select(x => x.cantidad).Sum();
                        c.codigo = codigo;
                        c.precio_unitario = temp_lista_conceptos.First().precio_unitario;
                        c.precio_unitario_original = temp_lista_conceptos.First().precio_unitario_original;
                        c.iva_unitario = temp_lista_conceptos.First().iva_unitario;
                        c.precio_unitario_mas_iva = temp_lista_conceptos.First().precio_unitario_mas_iva;
                        c.importe = temp_lista_conceptos.First().importe;
                        c.iva_importe = temp_lista_conceptos.First().iva_importe;
                        c.importe_mas_iva = temp_lista_conceptos.First().importe_mas_iva;
                        c.id_producto = temp_lista_conceptos.First().id_producto;
                        c.tipoCliente_id = idTipoCliente;
                        c.precio_en_dolares = temp_lista_conceptos.First().precio_en_dolares;
                        c.precio_en_dolares_original = temp_lista_conceptos.First().precio_en_dolares_original;
                        conceptos_por_codigo.Add(c);
                    }
                }
                //guardar conceptos de venta
                foreach (conceptos concepto in conceptos_por_codigo)
                {
                    conceptos_por_venta cpv = new conceptos_por_venta();
                    cpv.cantidad = concepto.cantidad;
                    cpv.precio_unitario = concepto.precio_unitario;
                    cpv.precio_unitario_original = concepto.precio_unitario_original;
                    cpv.iva_unitario = concepto.iva_unitario;
                    cpv.precio_unitario_mas_iva = concepto.precio_unitario_mas_iva;
                    cpv.importe = concepto.importe;
                    cpv.iva_importe = concepto.iva_importe;
                    cpv.importe_mas_iva = concepto.importe_mas_iva;
                    cpv.Producto_id = concepto.id_producto;
                    cpv.TipoCliente_id = concepto.tipoCliente_id;
                    cpv.precio_en_dolares = concepto.precio_en_dolares;
                    cpv.precio_en_dolares_original = concepto.precio_en_dolares_original;
                    cpv.Venta_id = v.id;
                    db.conceptos_por_venta.Add(cpv);
                    subtotal = subtotal + (cpv.cantidad * cpv.precio_unitario_original.Value);
                    iva = iva + ((cpv.cantidad * cpv.precio_unitario_original.Value) * (Config.iva(cpv.Producto_id) - 1));
                    total = total + ((cpv.cantidad * cpv.precio_unitario_original.Value) * Config.iva(cpv.Producto_id));
                }
                v.Cliente_id = id_cliente;
                v.subtotal = subtotal;
                v.iva = iva;
                v.total = total;
                v.observaciones = observaciones;
                db.Entry(v).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                success = true;
            }
            catch (Exception)
            {
                success = false;
                throw;
            }
            var respuesta = new
            {
                success = success
            };
            return Json(respuesta, JsonRequestBehavior.AllowGet);
        }

        public ActionResult eliminar_venta_por_entregar(int id)
        {
            bool success = false;
            string error = "";
            try
            {
                venta v = db.venta.Find(id);
                v.catalogo_status_id = 6;
                db.SaveChanges();
                success = true;
            }
            catch (Exception e)
            {
                error = e.Message;
                throw;
            }
            var respuesta = new
            {
                success = success,
                error = error
            };
            return Json(respuesta, JsonRequestBehavior.AllowGet);

        }

        public ActionResult NuevaCotizacion(int id_cliente, List<conceptos> lista_conceptos, decimal precio_dolar_venta, String observaciones, int id_sucursal)
        {
            bool success = false;
            int idventa = 0;
            string serie_folio = "";
            Decimal total = 0;
            Decimal iva = 0;
            Decimal subtotal = 0;
            //para agrugar por los concepos con el mismo codigo
            List<conceptos> conceptos_por_codigo = new List<conceptos>();
            //DISTINTOS CODIGOS POR VENTA
            foreach (var codigo in lista_conceptos.Select(x => x.codigo).Distinct())
            {
                //DISTINTOS TIPOS PRECIO POR CODIGO
                foreach (var idTipoCliente in lista_conceptos.Where(x => x.codigo == codigo).Select(x => x.tipoCliente_id).Distinct())
                {
                    List<conceptos> temp_lista_conceptos = lista_conceptos.Where(x => x.codigo == codigo && x.tipoCliente_id == idTipoCliente).ToList();
                    conceptos c = new conceptos();
                    c.cantidad = temp_lista_conceptos.Select(x => x.cantidad).Sum();
                    c.codigo = codigo;
                    c.precio_unitario = temp_lista_conceptos.First().precio_unitario;
                    c.precio_unitario_original = temp_lista_conceptos.First().precio_unitario_original;
                    c.iva_unitario = temp_lista_conceptos.First().iva_unitario;
                    c.precio_unitario_mas_iva = temp_lista_conceptos.First().precio_unitario_mas_iva;
                    c.importe = temp_lista_conceptos.First().importe;
                    c.iva_importe = temp_lista_conceptos.First().iva_importe;
                    c.importe_mas_iva = temp_lista_conceptos.First().importe_mas_iva;
                    c.id_producto = temp_lista_conceptos.First().id_producto;
                    c.tipoCliente_id = idTipoCliente;
                    c.precio_en_dolares = temp_lista_conceptos.First().precio_en_dolares;
                    c.precio_en_dolares_original = temp_lista_conceptos.First().precio_en_dolares_original;
                    conceptos_por_codigo.Add(c);
                }
            }
            //guardar venta
            venta v = new venta();
            try
            {
                string serie_cotizacion = Config.Serie_de_cotizacion();
                v.Serie = serie_cotizacion;  // "TC"
                v.Cliente_id = id_cliente;
                v.catalogo_status_id = 4;
                v.sucursal_id = id_sucursal;
                v.Folio = Convert.ToInt16(db.venta.Where(x => x.Serie == serie_cotizacion && x.sucursal_id == v.sucursal_id).Select(x => x.Folio).Max()) + 1;
                v.usuario_id = Fn.usuario_logeado.traerUsuario().id;
                v.fecha = DateTime.Now;
                v.subtotal = 0;
                v.iva = 0;
                v.total = 0;
                v.aduedo = 0;
                v.precio_dolar_venta = precio_dolar_venta;
                v.observaciones = observaciones;
                db.venta.Add(v);
                db.SaveChanges();
                //guardar conceptos de venta
                foreach (conceptos concepto in conceptos_por_codigo)
                {
                    conceptos_por_venta cpv = new conceptos_por_venta();
                    cpv.cantidad = concepto.cantidad;
                    cpv.precio_unitario = concepto.precio_unitario;
                    cpv.precio_unitario_original = concepto.precio_unitario_original;
                    cpv.iva_unitario = concepto.iva_unitario;
                    cpv.precio_unitario_mas_iva = concepto.precio_unitario_mas_iva;
                    cpv.importe = concepto.importe;
                    cpv.iva_importe = concepto.iva_importe;
                    cpv.importe_mas_iva = concepto.importe_mas_iva;
                    cpv.Producto_id = concepto.id_producto;//db.producto.Where(x => x.Codigo == concepto.codigo).First().id;
                    cpv.TipoCliente_id = concepto.tipoCliente_id;
                    cpv.Venta_id = v.id;
                    cpv.precio_en_dolares = concepto.precio_en_dolares;
                    cpv.precio_en_dolares_original = concepto.precio_en_dolares_original;
                    db.conceptos_por_venta.Add(cpv);
                    subtotal = subtotal + (cpv.cantidad * cpv.precio_unitario_original.Value);
                    iva = iva + ((cpv.cantidad * cpv.precio_unitario_original.Value) * (Config.iva(cpv.Producto_id) - 1));
                    total = total + ((cpv.cantidad * cpv.precio_unitario_original.Value) * Config.iva(cpv.Producto_id));
                }
                venta vTemp = db.venta.Find(v.id);
                vTemp.subtotal = subtotal;
                vTemp.iva = iva;
                vTemp.total = total;
                db.Entry(vTemp).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                success = true;
                idventa = v.id;
                serie_folio = Fn.Numeros.serie_folio_by_id(idventa);
            }
            catch (Exception)
            {
                success = false;
                db.SaveChanges();
                if (v.id != 0)
                {
                    List<conceptos_por_venta> conceptos_p_venta = db.conceptos_por_venta.Where(x => x.Venta_id == v.id).ToList();
                    foreach (conceptos_por_venta c in conceptos_p_venta)
                    {
                        //ELIMINAR CONCEPTOS
                        db.conceptos_por_venta.Remove(c);
                    }
                    db.SaveChanges();
                    db.venta.Remove(v);
                    db.SaveChanges();
                }
            }
            var respuesta = new
            {
                success = success,
                idventa = idventa,
                serie_folio = serie_folio
            };
            return Json(respuesta, JsonRequestBehavior.AllowGet);
        }

        public ActionResult confirmar_Generar_Venta_desde_cotización(int idCotizacion, int id_cliente, List<conceptos> lista_conceptos, decimal precio_dolar_venta, int tipo_entrega, String observaciones, int id_sucursal)
        {
            bool success = false;
            int idventa = 0;
            string serie_folio = "";
            Decimal total = 0;
            Decimal iva = 0;
            Decimal subtotal = 0;
            //para agrugar por los concepos con el mismo codigo
            List<conceptos> conceptos_por_codigo = new List<conceptos>();
            //DISTINTOS CODIGOS POR VENTA
            foreach (var codigo in lista_conceptos.Select(x => x.codigo).Distinct())
            {
                //DISTINTOS TIPOS PRECIO POR CODIGO
                foreach (var idTipoCliente in lista_conceptos.Where(x => x.codigo == codigo).Select(x => x.tipoCliente_id).Distinct())
                {
                    List<conceptos> temp_lista_conceptos = lista_conceptos.Where(x => x.codigo == codigo && x.tipoCliente_id == idTipoCliente).ToList();
                    conceptos c = new conceptos();
                    c.cantidad = temp_lista_conceptos.Select(x => x.cantidad).Sum();
                    c.codigo = codigo;
                    c.precio_unitario = temp_lista_conceptos.First().precio_unitario;
                    c.precio_unitario_original = temp_lista_conceptos.First().precio_unitario_original;
                    c.iva_unitario = temp_lista_conceptos.First().iva_unitario;
                    c.precio_unitario_mas_iva = temp_lista_conceptos.First().precio_unitario_mas_iva;
                    c.importe = temp_lista_conceptos.First().importe;
                    c.iva_importe = temp_lista_conceptos.First().iva_importe;
                    c.importe_mas_iva = temp_lista_conceptos.First().importe_mas_iva;
                    c.id_producto = temp_lista_conceptos.First().id_producto;
                    c.tipoCliente_id = idTipoCliente;
                    c.precio_en_dolares = temp_lista_conceptos.First().precio_en_dolares;
                    c.precio_en_dolares_original = temp_lista_conceptos.First().precio_en_dolares_original;
                    conceptos_por_codigo.Add(c);
                }
            }
            venta v = new venta();
            try
            {
                //guardar venta
                v.Serie = Config.Serie_de_venta_por_entregar();// "TN"
                v.Cliente_id = id_cliente;
                v.catalogo_status_id = tipo_entrega;
                v.sucursal_id = id_sucursal;
                v.usuario_id = Fn.usuario_logeado.traerUsuario().id;
                v.fecha = DateTime.Now;
                v.subtotal = 0;
                v.iva = 0;
                v.total = 0;
                v.aduedo = 0;
                v.precio_dolar_venta = precio_dolar_venta;
                v.observaciones = observaciones;
                db.venta.Add(v);
                db.SaveChanges();
                //guardar conceptos de venta
                foreach (conceptos concepto in conceptos_por_codigo)
                {
                    conceptos_por_venta cpv = new conceptos_por_venta();
                    cpv.cantidad = concepto.cantidad;
                    cpv.precio_unitario = concepto.precio_unitario;
                    cpv.precio_unitario_original = concepto.precio_unitario_original;
                    cpv.iva_unitario = concepto.iva_unitario;
                    cpv.precio_unitario_mas_iva = concepto.precio_unitario_mas_iva;
                    cpv.importe = concepto.importe;
                    cpv.iva_importe = concepto.iva_importe;
                    cpv.importe_mas_iva = concepto.importe_mas_iva;
                    cpv.Producto_id = concepto.id_producto;//db.producto.Where(x => x.Codigo == concepto.codigo).First().id;
                    cpv.TipoCliente_id = concepto.tipoCliente_id;
                    cpv.Venta_id = v.id;
                    cpv.precio_en_dolares = concepto.precio_en_dolares;
                    cpv.precio_en_dolares_original = concepto.precio_en_dolares_original;
                    db.conceptos_por_venta.Add(cpv);
                    subtotal = subtotal + (cpv.cantidad * cpv.precio_unitario_original.Value);
                    iva = iva + ((cpv.cantidad * cpv.precio_unitario_original.Value) * (Config.iva(cpv.Producto_id) - 1));
                    total = total + ((cpv.cantidad * cpv.precio_unitario_original.Value) * Config.iva(cpv.Producto_id));
                }
                venta vTemp = db.venta.Find(v.id);
                vTemp.Folio = v.id;
                vTemp.subtotal = subtotal;
                vTemp.iva = iva;
                vTemp.total = total;
                db.Entry(vTemp).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                //cambiar status de la venta-cotizacion a venta-desde-cotizacion
                db.venta.Find(idCotizacion).catalogo_status_id = 5;
                db.venta.Find(idCotizacion).observaciones = observaciones;
                db.SaveChanges();
                success = true;
                idventa = v.id;
                serie_folio = Fn.Numeros.serie_folio_by_id(idventa);
            }
            catch (Exception)
            {
                success = false;
                db.SaveChanges();
                if (v.id != 0)
                {
                    List<conceptos_por_venta> conceptos_p_venta = db.conceptos_por_venta.Where(x => x.Venta_id == v.id).ToList();
                    foreach (conceptos_por_venta c in conceptos_p_venta)
                    {
                        //ELIMINAR CONCEPTOS
                        db.conceptos_por_venta.Remove(c);
                    }
                    db.SaveChanges();
                    db.venta.Remove(v);
                    db.SaveChanges();
                }
            }
            var respuesta = new
            {
                success = success,
                idventa = idventa,
                serie_folio = serie_folio
            };
            return Json(respuesta, JsonRequestBehavior.AllowGet);
        }

        public ActionResult confirmar_venta(int id_cliente, decimal precio_dolar_venta, List<conceptos> lista_conceptos, String observaciones, int id_sucursal)
        {
            bool success = false;
            int idventa = 0;
            string serie_folio = "";
            //para agrugar por los concepos con el mismo codigo
            List<conceptos> conceptos_por_codigo = new List<conceptos>();
            //DISTINTOS CODIGOS POR VENTA
            foreach (var codigo in lista_conceptos.Select(x => x.codigo).Distinct())
            {
                //DISTINTOS TIPOS PRECIO POR CODIGO
                foreach (var idTipoCliente in lista_conceptos.Where(x => x.codigo == codigo).Select(x => x.tipoCliente_id).Distinct())
                {
                    List<conceptos> temp_lista_conceptos = lista_conceptos.Where(x => x.codigo == codigo && x.tipoCliente_id == idTipoCliente).ToList();
                    conceptos c = new conceptos();
                    c.cantidad = temp_lista_conceptos.Select(x => x.cantidad).Sum();
                    c.codigo = codigo;
                    c.precio_unitario = temp_lista_conceptos.First().precio_unitario;
                    c.precio_unitario_original = temp_lista_conceptos.First().precio_unitario_original;
                    c.iva_unitario = temp_lista_conceptos.First().iva_unitario;
                    c.precio_unitario_mas_iva = temp_lista_conceptos.First().precio_unitario_mas_iva;
                    c.importe = temp_lista_conceptos.First().importe;
                    c.iva_importe = temp_lista_conceptos.First().iva_importe;
                    c.importe_mas_iva = temp_lista_conceptos.First().importe_mas_iva;
                    c.id_producto = temp_lista_conceptos.First().id_producto;
                    c.tipoCliente_id = idTipoCliente;
                    c.precio_en_dolares = temp_lista_conceptos.First().precio_en_dolares;
                    c.precio_en_dolares_original = temp_lista_conceptos.First().precio_en_dolares_original;
                    conceptos_por_codigo.Add(c);
                }
            }
            //guardar venta
            venta v = new venta();
            try
            {
                v.Serie = Config.Serie_de_venta_por_entregar();// "TN";
                v.Cliente_id = id_cliente;
                v.catalogo_status_id = 3;
                v.sucursal_id = id_sucursal;
                v.usuario_id = Fn.usuario_logeado.traerUsuario().id;
                v.fecha = DateTime.Now;
                v.subtotal = 0;
                v.iva = 0;
                v.total = 0;
                v.aduedo = 0;
                v.precio_dolar_venta = precio_dolar_venta;
                v.observaciones = observaciones;
                v.guiaEnvio = "";
                db.venta.Add(v);
                db.SaveChanges();
                venta ventaPorEntregar = db.venta.Find(v.id);
                ventaPorEntregar.Folio = v.id;
                Decimal total = 0;
                Decimal iva = 0;
                Decimal subtotal = 0;
                //guardar conceptos de venta
                foreach (conceptos concepto in conceptos_por_codigo)
                {
                    conceptos_por_venta cpv = new conceptos_por_venta();
                    cpv.cantidad = concepto.cantidad;
                    cpv.precio_unitario = concepto.precio_unitario;
                    cpv.precio_unitario_original = concepto.precio_unitario_original;
                    cpv.iva_unitario = concepto.iva_unitario;
                    cpv.precio_unitario_mas_iva = concepto.precio_unitario_mas_iva;
                    cpv.importe = concepto.importe;
                    cpv.iva_importe = concepto.iva_importe;
                    cpv.importe_mas_iva = concepto.importe_mas_iva;
                    cpv.Producto_id = concepto.id_producto;//db.producto.Where(x => x.Codigo == concepto.codigo).First().id;
                    cpv.TipoCliente_id = concepto.tipoCliente_id;
                    cpv.precio_en_dolares = concepto.precio_en_dolares;
                    cpv.precio_en_dolares_original = concepto.precio_en_dolares_original;
                    cpv.Venta_id = v.id;
                    db.conceptos_por_venta.Add(cpv);
                    subtotal = subtotal + (cpv.cantidad * cpv.precio_unitario_original.Value);
                    iva = iva + ((cpv.cantidad * cpv.precio_unitario_original.Value) * (Config.iva(cpv.Producto_id) - 1));
                    total = total + ((cpv.cantidad * cpv.precio_unitario_original.Value) * Config.iva(cpv.Producto_id));
                }
                venta vTemp = db.venta.Find(v.id);
                vTemp.subtotal = subtotal;
                vTemp.iva = iva;
                vTemp.total = total;
                db.Entry(vTemp).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                success = true;
                idventa = v.id;
                serie_folio = Fn.Numeros.serie_folio_by_id(idventa);
            }
            catch (Exception)
            {
                success = false;
                db.SaveChanges();
                if (v.id != 0)
                {
                    List<conceptos_por_venta> conceptos_p_venta = db.conceptos_por_venta.Where(x => x.Venta_id == v.id).ToList();
                    foreach (conceptos_por_venta c in conceptos_p_venta)
                    {
                        //ELIMINAR CONCEPTOS
                        db.conceptos_por_venta.Remove(c);
                    }
                    db.SaveChanges();
                    db.venta.Remove(v);
                    db.SaveChanges();
                }
            }
            var respuesta = new
            {
                success = success,
                idventa = idventa,
                serie_folio = serie_folio
            };
            return Json(respuesta, JsonRequestBehavior.AllowGet);
        }

        public ActionResult confirmar_venta_id(int id, List<multiple_pago> ListaTiposPagos, List<int> ListaNotasCredito, List<Decimal> ListaDescuentos, decimal porcentaje_comision_pago_tarjeta, decimal monto_comision, String Referencia)
        {
            Decimal total_nc = 0;
            int id_nueva_nota_credito = 0;
            bool success = false;
            string error = "";
            int nuevo_id_movimiento_inventario = 0;
            venta v = db.venta.Find(id);

            configuracion_general configuracion_general = Configuracion.TraerConfiguracionGeneral();
            if (configuracion_general.respetar_inventario == true)
            {
                Decimal cantidad_en_inventario = 0;

                List<conceptos_por_venta> lista_conceptos_por_venta = db.conceptos_por_venta.Where(x => x.Venta_id == v.id).ToList();
                List<int> lista_cpv_borrar = new List<int>();
                List<int> lista_cpv_actualizar = new List<int>();
                Decimal total = 0;
                Decimal iva = 0;
                Decimal subtotal = 0;
                foreach (conceptos_por_venta cpv in lista_conceptos_por_venta)
                {
                    producto pro = db.producto.Find(cpv.Producto_id);
                    cantidad_en_inventario = Convert.ToDecimal(db.inventario.FirstOrDefault(x => x.Producto_id == pro.id && x.sucursal_id == v.sucursal_id).cantidad);

                    //Si no existe inventario procedemos a eliminra el concepto por venta
                    if (cantidad_en_inventario <= 0)
                    {
                        lista_cpv_borrar.Add(cpv.id);
                    }
                    //si la cantidad en inventario es menor que la cantidad guardada procedemos a descontar la cantidad 
                    else if (cantidad_en_inventario < cpv.cantidad)
                    {
                        lista_cpv_actualizar.Add(cpv.id);
                    }

                }
                if (lista_cpv_borrar != null && lista_cpv_borrar.Count > 0)
                {
                    foreach (int x in lista_cpv_borrar)
                    {
                        db.conceptos_por_venta.Remove(db.conceptos_por_venta.Find(x));
                    }
                    db.SaveChanges();
                }
                if (lista_cpv_actualizar != null && lista_cpv_actualizar.Count > 0)
                {
                    foreach (int x in lista_cpv_actualizar)
                    {
                        conceptos_por_venta cpv = db.conceptos_por_venta.Find(x);
                        producto pro = db.producto.Find(cpv.Producto_id);
                        cantidad_en_inventario = Convert.ToDecimal(db.inventario.FirstOrDefault(z => z.Producto_id == pro.id && z.sucursal_id == v.sucursal_id).cantidad);

                        cpv.cantidad = cantidad_en_inventario;
                        cpv.importe = (cpv.cantidad * cpv.precio_unitario_original.Value);
                        cpv.iva_importe = ((cpv.cantidad * cpv.precio_unitario_original.Value) * (Config.iva(cpv.Producto_id) - 1));
                        cpv.importe_mas_iva = ((cpv.cantidad * cpv.precio_unitario_original.Value) * Config.iva(cpv.Producto_id));
                        db.Entry(cpv).State = System.Data.Entity.EntityState.Modified;
                    }
                    db.SaveChanges();
                }
                if (monto_comision > 0)
                {
                    /* Agregar El concepto por comision por pago con tarjeta en caso de que se exista */
                    /* Tambien agregar los campos a venta  (porcentaje y monto) */
                    /* Esto adentro y fuera de respetar inventario */
                    conceptos_por_venta concepto_por_venta_comision = new conceptos_por_venta();
                    concepto_por_venta_comision.Venta_id = v.id;
                    //En 64000 es reservado para el producto TPV_COMISION
                    producto producto_comision = db.producto.FirstOrDefault(x => x.Codigo.Equals("TPV_COMISION"));
                    concepto_por_venta_comision.Producto_id = producto_comision.id;
                    concepto_por_venta_comision.TipoCliente_id = v.cliente.TipoCliente_id;
                    concepto_por_venta_comision.cantidad = 1;

                    // Sacar precio unitario e iva unitario de comision 

                    decimal uno_por_ciento_comision = monto_comision / (100 + (decimal)producto_comision.iva);
                    decimal precio_unitario_comision = uno_por_ciento_comision * 100;
                    decimal iva_unitario_comision = uno_por_ciento_comision * (decimal)producto_comision.iva;

                    concepto_por_venta_comision.precio_unitario = precio_unitario_comision;
                    concepto_por_venta_comision.precio_unitario_original = precio_unitario_comision;
                    concepto_por_venta_comision.iva_unitario = iva_unitario_comision;
                    concepto_por_venta_comision.precio_unitario_mas_iva = monto_comision;
                    concepto_por_venta_comision.importe = precio_unitario_comision;
                    concepto_por_venta_comision.iva_importe = iva_unitario_comision;
                    concepto_por_venta_comision.importe_mas_iva = monto_comision;
                    concepto_por_venta_comision.precio_en_dolares = 0;
                    concepto_por_venta_comision.precio_en_dolares_original = 0;
                    db.conceptos_por_venta.Add(concepto_por_venta_comision);
                    db.SaveChanges();

                    v.porcentaje_comision_pago_tarjeta = porcentaje_comision_pago_tarjeta;
                    v.monto_comision = monto_comision; 
                }

                lista_conceptos_por_venta = db.conceptos_por_venta.Where(x => x.Venta_id == v.id).ToList();
                foreach (conceptos_por_venta cpv in lista_conceptos_por_venta)
                {
                    subtotal = subtotal + (cpv.cantidad * cpv.precio_unitario_original.Value);
                    iva = iva + ((cpv.cantidad * cpv.precio_unitario_original.Value) * (Config.iva(cpv.Producto_id) - 1));
                    total = total + ((cpv.cantidad * cpv.precio_unitario_original.Value) * Config.iva(cpv.Producto_id));
                }

                v.subtotal = subtotal;
                v.iva = iva;
                v.total = total;
                db.Entry(v).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            else
            {
                if (monto_comision > 0)
                {
                    /* Agregar El concepto por comision por pago con tarjeta en caso de que se exista */
                    /* Tambien agregar los campos a venta  (porcentaje y monto) */
                    /* Esto adentro y fuera de respetar inventario */
                    conceptos_por_venta concepto_por_venta_comision = new conceptos_por_venta();
                    concepto_por_venta_comision.Venta_id = v.id;
                    //En 64000 es reservado para el producto TPV_COMISION
                    producto producto_comision = db.producto.FirstOrDefault(x => x.Codigo.Equals("TPV_COMISION"));
                    concepto_por_venta_comision.Producto_id = producto_comision.id;
                    concepto_por_venta_comision.TipoCliente_id = v.cliente.TipoCliente_id;
                    concepto_por_venta_comision.cantidad = 1;

                    // Sacar precio unitario e iva unitario de comision 

                    decimal uno_por_ciento_comision = monto_comision / (100 + (decimal)producto_comision.iva);
                    decimal precio_unitario_comision = uno_por_ciento_comision * 100;
                    decimal iva_unitario_comision = uno_por_ciento_comision * (decimal)producto_comision.iva;

                    concepto_por_venta_comision.precio_unitario = precio_unitario_comision;
                    concepto_por_venta_comision.precio_unitario_original = precio_unitario_comision;
                    concepto_por_venta_comision.iva_unitario = iva_unitario_comision;
                    concepto_por_venta_comision.precio_unitario_mas_iva = monto_comision;
                    concepto_por_venta_comision.importe = precio_unitario_comision;
                    concepto_por_venta_comision.iva_importe = iva_unitario_comision;
                    concepto_por_venta_comision.importe_mas_iva = monto_comision;
                    concepto_por_venta_comision.precio_en_dolares = 0;
                    concepto_por_venta_comision.precio_en_dolares_original = 0;
                    db.conceptos_por_venta.Add(concepto_por_venta_comision);
                    db.SaveChanges();

                    v.porcentaje_comision_pago_tarjeta = porcentaje_comision_pago_tarjeta;
                    v.monto_comision = monto_comision;

                    List<conceptos_por_venta> lista_conceptos_por_venta = db.conceptos_por_venta.Where(x => x.Venta_id == v.id).ToList();
                    decimal subtotal = 0;
                    decimal iva = 0;
                    decimal total = 0;
                    foreach (conceptos_por_venta cpv in lista_conceptos_por_venta)
                    {
                        subtotal = subtotal + (cpv.cantidad * cpv.precio_unitario_original.Value);
                        iva = iva + ((cpv.cantidad * cpv.precio_unitario_original.Value) * (Config.iva(cpv.Producto_id) - 1));
                        total = total + ((cpv.cantidad * cpv.precio_unitario_original.Value) * Config.iva(cpv.Producto_id));
                    }
                    
                    v.subtotal = subtotal;
                    v.iva = iva;
                    v.total = total;
                    db.Entry(v).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
            }

            Decimal total_aux = (Decimal)v.total;
            Decimal total_real = (Decimal)v.total;
            List<nota_credito> ListaNotasCreditoReal = new List<nota_credito>();
            string folio_serie_nueva = "";
            try
            {
                if (ListaNotasCredito != null)
                {
                    foreach (int id_nota_credito in ListaNotasCredito)
                    {
                        nota_credito nota_credito = db.nota_credito.Find(id_nota_credito);
                        ListaNotasCreditoReal.Add(nota_credito);
                    }
                }
                total_aux -= (Decimal)ListaNotasCreditoReal.Select(x => x.cantidad).Sum();
                if (ListaDescuentos != null && ListaDescuentos.Count >= 1)
                {
                    foreach (Decimal x in ListaDescuentos)
                    {
                        total_real -= x;
                    }
                }
                string serie_venta_confirmada = Config.Serie_de_venta_confirmada();
                if (v.Serie != serie_venta_confirmada)//  "T"=esta venta ya se ha entregado
                {
                    v.total = total_real;
                    v.Folio = Convert.ToInt16(db.venta.Where(x => x.Serie == serie_venta_confirmada && x.sucursal_id == v.sucursal_id).Select(x => x.Folio).Max()) + 1;
                    v.Serie = Config.Serie_de_venta_confirmada();//"T"
                    v.catalogo_status_id = 1;
                    v.Fecha_entrega = DateTime.Now;
                    db.Entry(v).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    if (ListaNotasCreditoReal != null)
                    {
                        foreach (nota_credito nota_credito in ListaNotasCreditoReal)
                        {
                            nota_credito.id_venta_aplicada = v.id;
                            nota_credito.id_estatus_nota_credito = 2;
                            nota_credito.fecha_uso = DateTime.Now;
                            db.Entry(nota_credito).State = System.Data.Entity.EntityState.Modified;
                            total_nc += nota_credito.cantidad.Value;
                        }
                        db.SaveChanges();
                    }
                    if (ListaTiposPagos != null)
                    {
                        foreach (multiple_pago tipo_pago in ListaTiposPagos)
                        {
                            multiple_pago multiple_pago = new multiple_pago();
                            multiple_pago.id_venta = v.id;
                            multiple_pago.id_tipo_pago_venta = tipo_pago.id_tipo_pago_venta;
                            multiple_pago.cantidad = tipo_pago.cantidad;
                            if (multiple_pago.id_tipo_pago_venta == 5)
                            {
                                if (!(Referencia == null || Referencia.Equals("")))
                                {
                                    multiple_pago.referencia = Referencia;
                                }
                                else
                                {
                                    multiple_pago.referencia = "Falta Referencia!";
                                }
                            }
                            db.multiple_pago.Add(multiple_pago);
                            if (tipo_pago.id_tipo_pago_venta == 4)
                            {
                                v.aduedo = tipo_pago.cantidad;
                                cliente c = db.cliente.Find(v.Cliente_id);
                                c.Adeudo_credito = c.Adeudo_credito + tipo_pago.cantidad;
                                db.Entry(v).State = System.Data.Entity.EntityState.Modified;
                                db.Entry(c).State = System.Data.Entity.EntityState.Modified;
                            }
                        }
                    }
                    //crear registro movimiento de inventario
                    //*********************************
                    movimiento_inventario mi = new movimiento_inventario();
                    mi.fecha = DateTime.Now;
                    mi.recibe = v.cliente.Razon_social;
                    mi.usuario_valida_id = Fn.usuario_logeado.traerUsuario().id;
                    mi.tipo_movimiento_inventario_id = 4;//4- salida por venta
                    mi.sucursal_id = v.sucursal_id;
                    db.movimiento_inventario.Add(mi);
                    db.SaveChanges();
                    nuevo_id_movimiento_inventario = mi.id;
                    foreach (conceptos_por_venta c in v.conceptos_por_venta)
                    {
                        inventario i = new inventario();
                        //Aplicar cambios de inventario
                        bool existe_inventario = db.inventario.Where(x => x.Producto_id == c.Producto_id && x.sucursal_id == v.sucursal_id).Any();
                        if (existe_inventario)
                        {
                            //bajar de inventario
                            i = db.inventario.Where(x => x.Producto_id == c.Producto_id && x.sucursal_id == v.sucursal_id).Single();
                            i.cantidad = i.cantidad - c.cantidad;
                        }
                        else
                        {
                            //crear y bajar de inventario
                            i.Producto_id = c.Producto_id;
                            i.sucursal_id = v.sucursal_id;
                            i.cantidad = 0;
                            db.inventario.Add(i);
                            i.cantidad = i.cantidad - c.cantidad;
                        }
                        db.SaveChanges();
                        conceptos_por_movimiento_inventario cxm = new conceptos_por_movimiento_inventario();
                        cxm.movimiento_inventario_id = mi.id;
                        cxm.Inventario_id = i.id;
                        cxm.cantidad = c.cantidad;
                        db.conceptos_por_movimiento_inventario.Add(cxm);
                        db.SaveChanges();
                        folio_serie_nueva = Fn.Numeros.serie_folio_by_id(v.id);
                    }
                    if (ListaNotasCredito != null)
                    {
                        Decimal restante = Convert.ToDecimal(total_nc - v.total);
                        if (restante > 0)
                        {
                            foreach (nota_credito nota_credito in ListaNotasCreditoReal)
                            {
                                if (restante > 0)
                                {
                                    if ((restante - nota_credito.cantidad.Value) >= 0)
                                        restante -= nota_credito.cantidad.Value;
                                    else
                                    {
                                        nota_credito.cantidad -= restante;
                                        restante = 0;
                                        String Serie_de_nota_de_credito = Config.Serie_de_nota_de_credito();
                                        nota_credito nc = new nota_credito();
                                        nc.id_venta = nota_credito.id_venta;
                                        nc.id_estatus_nota_credito = 1;
                                        nc.id_sucursal = v.sucursal_id;
                                        nc.id_usuario_punto_venta = v.usuario_id;
                                        nc.serie = Config.Serie_de_nota_de_credito();
                                        nc.folio = Convert.ToInt32(db.nota_credito.Where(x => x.serie.Equals(Serie_de_nota_de_credito) && x.venta.sucursal_id == nc.id_sucursal).Select(x => x.folio).Max()) + 1;
                                        nc.cantidad = Convert.ToDecimal(total_nc - v.total);
                                        nc.fecha_creacion = DateTime.Now;
                                        db.nota_credito.Add(nc);
                                        db.SaveChanges();
                                        id_nueva_nota_credito = nc.id;
                                    }
                                    db.Entry(nota_credito).State = System.Data.Entity.EntityState.Modified;
                                }
                            }
                        }
                        db.SaveChanges();
                    }
                    if (ListaDescuentos != null)
                    {
                        Decimal uno_porciento = total_aux / 100;
                        foreach (Decimal descuento_individual in ListaDescuentos)
                        {
                            Decimal porcentaje_descuento = descuento_individual / uno_porciento;
                            descuento descuento = new descuento();
                            descuento.id_venta = v.id;
                            descuento.monto = descuento_individual;
                            descuento.porcentaje = porcentaje_descuento;
                            db.descuento.Add(descuento);
                            db.SaveChanges();
                        }
                    }

                    venta venta_2 = db.venta.Find(id);
                    Decimal total_consignacion = 0;
                    foreach (conceptos_por_venta cpv in venta_2.conceptos_por_venta.ToList())
                    {
                        if (configuracion_general.venta_consignacion == true)
                        {
                            if (cpv.producto.consignacion == 1)
                            {
                                total_consignacion += ((cpv.cantidad * cpv.precio_unitario_original.Value) * Config.iva(cpv.Producto_id));
                                cpv.pagada_consignacion = false;
                                db.Entry(cpv).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                            }
                        }
                    }
                    //if (configuracion_general.venta_consignacion == true)
                    //{
                    //    venta_2.total_consignacion = total_consignacion;
                    //    venta_2.pagada_consignacion = false;
                    //    db.Entry(venta_2).State = System.Data.Entity.EntityState.Modified;
                    //    db.SaveChanges();
                    //}

                    success = true;
                }
                else
                {
                    error = "Esta no es una venta por entregar (serie:TN)";
                    success = false;
                }
            }
            catch (Exception e)
            {
                success = false;
                error = e.Message;
                //eliminar movimiento de invnetario agregado
                db.movimiento_inventario.Remove(db.movimiento_inventario.Find(nuevo_id_movimiento_inventario));
                db.SaveChanges();
                throw;
            }
            var respuesta = new
            {
                success = success,
                error = error,
                folio_serie_nueva = folio_serie_nueva,
                idventa = v.id,
                id_nueva_nota_credito = id_nueva_nota_credito
            };
            return Json(respuesta, JsonRequestBehavior.AllowGet);
        }

        public ActionResult confirmar_pago_venta_online(int id, List<multiple_pago> ListaTiposPagos, List<int> ListaNotasCredito, List<Decimal> ListaDescuentos, List<inventario> ListaInventarios, decimal porcentaje_comision_pago_tarjeta, decimal monto_comision, String Referencia)
        {
            Decimal total_nc = 0;
            int id_nueva_nota_credito = 0;
            bool success = false;
            string error = "";
            int nuevo_id_movimiento_inventario = 0;
            venta v = db.venta.Find(id);
            configuracion_general configuracion_general = Fn.Configuracion.TraerConfiguracionGeneral();
            if (configuracion_general.respetar_inventario == true)
            {
                Decimal cantidad_en_inventario = 0;
                List<int> lista_id_sucursales = db.sucursal.Where(x => x.alimenta_tienda_online == true).Select(y => y.id).ToList();
                List<conceptos_por_venta> lista_conceptos_por_venta = db.conceptos_por_venta.Where(x => x.Venta_id == v.id).ToList();
                List<int> lista_cpv_borrar = new List<int>();
                List<int> lista_cpv_actualizar = new List<int>();
                Decimal total = 0;
                Decimal iva = 0;
                Decimal subtotal = 0;
                foreach (conceptos_por_venta cpv in lista_conceptos_por_venta)
                {
                    producto pro = db.producto.Find(cpv.Producto_id);
                    List<inventario> lista_inventario = db.inventario.Where(x => x.Producto_id == pro.id && lista_id_sucursales.Contains(x.sucursal_id)).ToList();

                    if (lista_inventario != null && lista_inventario.Count > 0)
                        cantidad_en_inventario = Convert.ToDecimal(lista_inventario.Select(x => x.cantidad).Sum());
                    //Si no existe inventario procedemos a eliminra el concepto por venta
                    if (cantidad_en_inventario <= 0)
                    {
                        lista_cpv_borrar.Add(cpv.id);
                    }
                    //si la cantidad en inventario es menor que la cantidad guardada procedemos a descontar la cantidad 
                    else if (cantidad_en_inventario < cpv.cantidad)
                    {
                        lista_cpv_actualizar.Add(cpv.id);
                    }
                }
                if (lista_cpv_borrar != null && lista_cpv_borrar.Count > 0)
                {
                    foreach (int x in lista_cpv_borrar)
                    {
                        db.conceptos_por_venta.Remove(db.conceptos_por_venta.Find(x));
                    }
                    db.SaveChanges();
                }
                if (lista_cpv_actualizar != null && lista_cpv_actualizar.Count > 0)
                {
                    foreach (int x in lista_cpv_actualizar)
                    {
                        conceptos_por_venta cpv = db.conceptos_por_venta.Find(x);
                        producto pro = db.producto.Find(cpv.Producto_id);
                        List<inventario> lista_inventario = db.inventario.Where(a => a.Producto_id == pro.id && lista_id_sucursales.Contains(a.sucursal_id)).ToList();

                        if (lista_inventario != null && lista_inventario.Count > 0)
                            cantidad_en_inventario = Convert.ToDecimal(lista_inventario.Select(y => y.cantidad).Sum());
                        cpv.cantidad = cantidad_en_inventario;
                        cpv.importe = cpv.cantidad * cpv.precio_unitario;

                        cpv.importe = (cpv.cantidad * cpv.precio_unitario_original.Value);
                        cpv.iva_importe = ((cpv.cantidad * cpv.precio_unitario_original.Value) * (Config.iva(cpv.Producto_id) - 1));
                        cpv.importe_mas_iva = ((cpv.cantidad * cpv.precio_unitario_original.Value) * Config.iva(cpv.Producto_id));
                        db.Entry(cpv).State = System.Data.Entity.EntityState.Modified;
                    }
                    db.SaveChanges();
                }
                if (monto_comision > 0)
                {
                    /* Agregar El concepto por comision por pago con tarjeta en caso de que se exista */
                    /* Tambien agregar los campos a venta  (porcentaje y monto) */
                    /* Esto adentro y fuera de respetar inventario */
                    conceptos_por_venta concepto_por_venta_comision = new conceptos_por_venta();
                    concepto_por_venta_comision.Venta_id = v.id;
                    //En 64000 es reservado para el producto TPV_COMISION
                    producto producto_comision = db.producto.FirstOrDefault(x => x.Codigo.Equals("TPV_COMISION"));
                    concepto_por_venta_comision.Producto_id = producto_comision.id;
                    concepto_por_venta_comision.TipoCliente_id = v.cliente.TipoCliente_id;
                    concepto_por_venta_comision.cantidad = 1;

                    // Sacar precio unitario e iva unitario de comision 

                    decimal uno_por_ciento_comision = monto_comision / (100 + (decimal)producto_comision.iva);
                    decimal precio_unitario_comision = uno_por_ciento_comision * 100;
                    decimal iva_unitario_comision = uno_por_ciento_comision * (decimal)producto_comision.iva;

                    concepto_por_venta_comision.precio_unitario = precio_unitario_comision;
                    concepto_por_venta_comision.precio_unitario_original = precio_unitario_comision;
                    concepto_por_venta_comision.iva_unitario = iva_unitario_comision;
                    concepto_por_venta_comision.precio_unitario_mas_iva = monto_comision;
                    concepto_por_venta_comision.importe = precio_unitario_comision;
                    concepto_por_venta_comision.iva_importe = iva_unitario_comision;
                    concepto_por_venta_comision.importe_mas_iva = monto_comision;
                    concepto_por_venta_comision.precio_en_dolares = 0;
                    concepto_por_venta_comision.precio_en_dolares_original = 0;
                    db.conceptos_por_venta.Add(concepto_por_venta_comision);
                    db.SaveChanges();

                    v.porcentaje_comision_pago_tarjeta = porcentaje_comision_pago_tarjeta;
                    v.monto_comision = monto_comision;
                }
                lista_conceptos_por_venta = db.conceptos_por_venta.Where(x => x.Venta_id == v.id).ToList();
                foreach (conceptos_por_venta cpv in lista_conceptos_por_venta)
                {
                    subtotal = subtotal + (cpv.cantidad * cpv.precio_unitario_original.Value);
                    iva = iva + ((cpv.cantidad * cpv.precio_unitario_original.Value) * (Config.iva(cpv.Producto_id) - 1));
                    total = total + ((cpv.cantidad * cpv.precio_unitario_original.Value) * Config.iva(cpv.Producto_id));
                }

                v.subtotal = subtotal;
                v.iva = iva;
                v.total = total;
                db.Entry(v).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            else
            {
                if (monto_comision > 0)
                {
                    /* Agregar El concepto por comision por pago con tarjeta en caso de que se exista */
                    /* Tambien agregar los campos a venta  (porcentaje y monto) */
                    /* Esto adentro y fuera de respetar inventario */
                    conceptos_por_venta concepto_por_venta_comision = new conceptos_por_venta();
                    concepto_por_venta_comision.Venta_id = v.id;
                    //En 64000 es reservado para el producto TPV_COMISION
                    producto producto_comision = db.producto.FirstOrDefault(x => x.Codigo.Equals("TPV_COMISION"));
                    concepto_por_venta_comision.Producto_id = producto_comision.id;
                    concepto_por_venta_comision.TipoCliente_id = v.cliente.TipoCliente_id;
                    concepto_por_venta_comision.cantidad = 1;

                    // Sacar precio unitario e iva unitario de comision 

                    decimal uno_por_ciento_comision = monto_comision / (100 + (decimal)producto_comision.iva);
                    decimal precio_unitario_comision = uno_por_ciento_comision * 100;
                    decimal iva_unitario_comision = uno_por_ciento_comision * (decimal)producto_comision.iva;

                    concepto_por_venta_comision.precio_unitario = precio_unitario_comision;
                    concepto_por_venta_comision.precio_unitario_original = precio_unitario_comision;
                    concepto_por_venta_comision.iva_unitario = iva_unitario_comision;
                    concepto_por_venta_comision.precio_unitario_mas_iva = monto_comision;
                    concepto_por_venta_comision.importe = precio_unitario_comision;
                    concepto_por_venta_comision.iva_importe = iva_unitario_comision;
                    concepto_por_venta_comision.importe_mas_iva = monto_comision;
                    concepto_por_venta_comision.precio_en_dolares = 0;
                    concepto_por_venta_comision.precio_en_dolares_original = 0;
                    db.conceptos_por_venta.Add(concepto_por_venta_comision);
                    db.SaveChanges();

                    v.porcentaje_comision_pago_tarjeta = porcentaje_comision_pago_tarjeta;
                    v.monto_comision = monto_comision;

                    List<conceptos_por_venta> lista_conceptos_por_venta = db.conceptos_por_venta.Where(x => x.Venta_id == v.id).ToList();
                    decimal subtotal = 0;
                    decimal iva = 0;
                    decimal total = 0;
                    foreach (conceptos_por_venta cpv in lista_conceptos_por_venta)
                    {
                        subtotal = subtotal + (cpv.cantidad * cpv.precio_unitario_original.Value);
                        iva = iva + ((cpv.cantidad * cpv.precio_unitario_original.Value) * (Config.iva(cpv.Producto_id) - 1));
                        total = total + ((cpv.cantidad * cpv.precio_unitario_original.Value) * Config.iva(cpv.Producto_id));
                    }

                    v.subtotal = subtotal;
                    v.iva = iva;
                    v.total = total;
                    db.Entry(v).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
            }

            Decimal total_aux = (Decimal)v.total;
            Decimal total_real = (Decimal)v.total;
            List<nota_credito> ListaNotasCreditoReal = new List<nota_credito>();
            string folio_serie_nueva = "";
            try
            {
                if (ListaNotasCredito != null)
                {
                    foreach (int id_nota_credito in ListaNotasCredito)
                    {
                        nota_credito nota_credito = db.nota_credito.Find(id_nota_credito);
                        ListaNotasCreditoReal.Add(nota_credito);
                    }
                }
                total_aux -= (Decimal)ListaNotasCreditoReal.Select(x => x.cantidad).Sum();
                if (ListaDescuentos != null && ListaDescuentos.Count >= 1)
                {
                    foreach (Decimal x in ListaDescuentos)
                    {
                        total_real -= x;
                    }
                }
                //string serie_venta_confirmada = Config.Serie_de_venta_confirmada();
                //if (v.Serie != serie_venta_confirmada)//  "T"=esta venta ya se ha entregado
                //{
                //v.Folio = Convert.ToInt16(db.venta.Where(x => x.Serie == serie_venta_confirmada && x.sucursal_id == v.sucursal_id).Select(x => x.Folio).Max()) + 1;
                //v.Serie = Config.Serie_de_venta_confirmada();//"T"
                v.total = total_real;
                v.catalogo_status_id = 9;
                v.fecha_liquidacion = DateTime.Now;
                db.Entry(v).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                if (ListaNotasCreditoReal != null)
                {
                    foreach (nota_credito nota_credito in ListaNotasCreditoReal)
                    {
                        nota_credito.id_venta_aplicada = v.id;
                        nota_credito.id_estatus_nota_credito = 2;
                        nota_credito.fecha_uso = DateTime.Now;
                        db.Entry(nota_credito).State = System.Data.Entity.EntityState.Modified;
                        total_nc += nota_credito.cantidad.Value;
                    }
                    db.SaveChanges();
                }
                if (ListaTiposPagos != null)
                {
                    foreach (multiple_pago tipo_pago in ListaTiposPagos)
                    {
                        multiple_pago multiple_pago = new multiple_pago();
                        multiple_pago.id_venta = v.id;
                        multiple_pago.id_tipo_pago_venta = tipo_pago.id_tipo_pago_venta;
                        multiple_pago.cantidad = tipo_pago.cantidad;
                        if (multiple_pago.id_tipo_pago_venta == 5)
                        {
                            if (!(Referencia == null || Referencia.Equals("")))
                            {
                                multiple_pago.referencia = Referencia;
                            }
                            else
                            {
                                multiple_pago.referencia = "Falta Referencia!";
                            }
                        }
                        db.multiple_pago.Add(multiple_pago);
                        if (tipo_pago.id_tipo_pago_venta == 4)
                        {
                            v.aduedo = tipo_pago.cantidad;
                            cliente c = db.cliente.Find(v.Cliente_id);
                            c.Adeudo_credito = c.Adeudo_credito + tipo_pago.cantidad;
                            db.Entry(v).State = System.Data.Entity.EntityState.Modified;
                            db.Entry(c).State = System.Data.Entity.EntityState.Modified;
                        }
                        //deposito
                    }
                }
                //crear registro movimiento de inventario
                //*********************************
                movimiento_inventario mi = new movimiento_inventario();
                mi.fecha = DateTime.Now;
                mi.recibe = v.cliente.Razon_social;
                mi.usuario_valida_id = Fn.usuario_logeado.traerUsuario().id;
                mi.tipo_movimiento_inventario_id = 4;//4- salida por venta
                mi.sucursal_id = v.sucursal_id;
                db.movimiento_inventario.Add(mi);
                db.SaveChanges();
                nuevo_id_movimiento_inventario = mi.id;

                if (configuracion_general.respetar_inventario == false || ListaInventarios == null || ListaInventarios.Count == 0)
                {
                    foreach (conceptos_por_venta c in v.conceptos_por_venta)
                    {
                        inventario i = new inventario();
                        //Aplicar cambios de inventario
                        bool existe_inventario = db.inventario.Where(x => x.Producto_id == c.Producto_id && x.sucursal_id == v.sucursal_id).Any();
                        if (existe_inventario)
                        {
                            //bajar de inventario
                            i = db.inventario.Where(x => x.Producto_id == c.Producto_id && x.sucursal_id == v.sucursal_id).Single();
                            i.cantidad = i.cantidad - c.cantidad;
                        }
                        else
                        {
                            //crear y bajar de inventario
                            i.Producto_id = c.Producto_id;
                            i.sucursal_id = v.sucursal_id;
                            i.cantidad = 0;
                            db.inventario.Add(i);
                            i.cantidad = i.cantidad - c.cantidad;
                        }
                        db.SaveChanges();
                        conceptos_por_movimiento_inventario cxm = new conceptos_por_movimiento_inventario();
                        cxm.movimiento_inventario_id = mi.id;
                        cxm.Inventario_id = i.id;
                        cxm.cantidad = c.cantidad;
                        db.conceptos_por_movimiento_inventario.Add(cxm);
                        db.SaveChanges();
                        //folio_serie_nueva = Fn.Numeros.serie_folio_by_id(v.id);
                    }
                }
                else
                {
                    foreach (conceptos_por_venta c in v.conceptos_por_venta)
                    {
                        List<inventario> lista_inventario_por_concepto_venta = ListaInventarios.Where(y => y.Producto_id == c.Producto_id).ToList();
                        foreach (inventario inv in lista_inventario_por_concepto_venta)
                        {
                            if (inv.cantidad > 0)
                            {
                                inventario i = db.inventario.FirstOrDefault(x => x.Producto_id == inv.Producto_id && x.sucursal_id == inv.sucursal_id);
                                i.cantidad = i.cantidad - inv.cantidad;
                                db.Entry(i).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                                conceptos_por_movimiento_inventario cxm = new conceptos_por_movimiento_inventario();
                                cxm.movimiento_inventario_id = mi.id;
                                cxm.Inventario_id = i.id;
                                cxm.cantidad = Convert.ToDecimal(inv.cantidad);
                                db.conceptos_por_movimiento_inventario.Add(cxm);
                                db.SaveChanges();
                            }
                        }

                        //folio_serie_nueva = Fn.Numeros.serie_folio_by_id(v.id);
                    }
                }
                if (ListaNotasCreditoReal != null)
                {
                    Decimal restante = Convert.ToDecimal(total_nc - v.total);
                    if (restante > 0)
                    {
                        foreach (nota_credito nota_credito in ListaNotasCreditoReal)
                        {
                            if (restante > 0)
                            {
                                if ((restante - nota_credito.cantidad.Value) >= 0)
                                    restante -= nota_credito.cantidad.Value;
                                else
                                {
                                    nota_credito.cantidad -= restante;
                                    restante = 0;
                                    String Serie_de_nota_de_credito = Config.Serie_de_nota_de_credito();
                                    nota_credito nc = new nota_credito();
                                    nc.id_venta = nota_credito.id_venta;
                                    nc.id_estatus_nota_credito = 1;
                                    nc.id_sucursal = v.sucursal_id;
                                    nc.id_usuario_punto_venta = v.usuario_id;
                                    nc.serie = Config.Serie_de_nota_de_credito();
                                    nc.folio = Convert.ToInt32(db.nota_credito.Where(x => x.serie.Equals(Serie_de_nota_de_credito) && x.venta.sucursal_id == nc.id_sucursal).Select(x => x.folio).Max()) + 1;
                                    nc.cantidad = Convert.ToDecimal(total_nc - v.total);
                                    nc.fecha_creacion = DateTime.Now;
                                    db.nota_credito.Add(nc);
                                    db.SaveChanges();
                                    id_nueva_nota_credito = nc.id;
                                }
                                db.Entry(nota_credito).State = System.Data.Entity.EntityState.Modified;
                            }
                        }
                    }
                    db.SaveChanges();
                }
                if (ListaDescuentos != null)
                {
                    Decimal uno_porciento = total_aux / 100;
                    foreach (Decimal descuento_individual in ListaDescuentos)
                    {
                        Decimal porcentaje_descuento = descuento_individual / uno_porciento;
                        descuento descuento = new descuento();
                        descuento.id_venta = v.id;
                        descuento.monto = descuento_individual;
                        descuento.porcentaje = porcentaje_descuento;
                        db.descuento.Add(descuento);
                        db.SaveChanges();
                    }
                }
                folio_serie_nueva = Fn.Numeros.serie_folio_by_id(v.id);

                venta venta_2 = db.venta.Find(id);
                Decimal total_consignacion = 0;
                foreach (conceptos_por_venta cpv in venta_2.conceptos_por_venta.ToList())
                {
                    if (configuracion_general.venta_consignacion == true)
                    {
                        if (cpv.producto.consignacion == 1)
                        {
                            total_consignacion += ((cpv.cantidad * cpv.precio_unitario_original.Value) * Config.iva(cpv.Producto_id));
                            cpv.pagada_consignacion = false;
                            db.Entry(cpv).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                        }
                    }
                }
                //if (configuracion_general.venta_consignacion == true)
                //{
                //    venta_2.total_consignacion = total_consignacion;
                //    venta_2.pagada_consignacion = false;
                //    db.Entry(venta_2).State = System.Data.Entity.EntityState.Modified;
                //    db.SaveChanges();
                //}

                success = true;
                //}
                //else
                //{
                //    error = "Esta no es una venta por entregar (serie:TN)";
                //    success = false;
                //}
            }
            catch (Exception e)
            {
                success = false;
                error = e.Message;
                //eliminar movimiento de invnetario agregado
                db.movimiento_inventario.Remove(db.movimiento_inventario.Find(nuevo_id_movimiento_inventario));
                db.SaveChanges();
                throw;
            }
            var respuesta = new
            {
                success = success,
                error = error,
                folio_serie_nueva = folio_serie_nueva,
                idventa = v.id,
                id_nueva_nota_credito = id_nueva_nota_credito
            };
            return Json(respuesta, JsonRequestBehavior.AllowGet);
        }
        
        public PartialViewResult ModalConfirmarVentaCentralizada(int idVenta)
        {
            configuracion_general configuracion_general = Fn.Configuracion.TraerConfiguracionGeneral();
            ViewBag.ListaNotasCredito = db.nota_credito.Where(x => x.id_venta_aplicada == idVenta).ToList();
            ViewBag.ListaTipoPago = db.tipo_pago_venta.ToList();
            ViewBag.porcentaje_comision = configuracion_general.porcentaje_comision_pago_tarjeta;
            ViewBag.iva_producto_comision = db.producto.FirstOrDefault(x => x.Codigo.Equals("TPV_COMISION")).iva;
            return PartialView(db.venta.Find(idVenta));
        }

        public PartialViewResult VistaParcialAutocompleteCliente(int id_sucursal, String id_cliente)
        {
            var jsonSerialiser = new JavaScriptSerializer();
            var jsonLista = jsonSerialiser.Serialize(db.cliente.Select(x => x.Razon_social));
            ViewBag.autocomplete = jsonLista;
            if (db.cliente.Count() != 0)
            {
                if (id_cliente != null)
                {
                    int cliente = Convert.ToInt32(id_cliente);
                    ViewBag.cliente_default = db.cliente.Where(x => x.id == cliente).Single().Razon_social;
                }
                else
                {
                    int idcliente = db.cliente.Select(x => x.id).Min();
                    ViewBag.cliente_default = db.cliente.Find(idcliente).Razon_social;
                }
            }
            else
            {
                ViewBag.cliente_default = "";
            }
            return PartialView();
        }

        public PartialViewResult VistaParcialAutocompleteProducto(int id_sucursal)
        {
            var jsonSerialiser = new JavaScriptSerializer();
            var jsonLista = jsonSerialiser.Serialize(db.producto.Where(y => y.visible == 1).Select(x => string.Concat(x.Codigo, " | ", x.Descripcion + " | " + x.medida)));
            ViewBag.codigos_autocomplete = jsonLista;
            return PartialView();
        }

        public ActionResult parcial_cbx_tipo_precio(int id_sucursal)
        {
            return Json(
                db.tipocliente.Select(x => new { value = x.id, text = x.Tipo }),
                JsonRequestBehavior.AllowGet
            );
        }

        public ActionResult parcial_cbx_tipo_precio_cotizacion(int idSucursal, String id_tipo_precio)
        {
            int tipo_precio = Convert.ToInt32(id_tipo_precio);
            String opciones = "";
            foreach (tipocliente precio in db.tipocliente.ToList())
            {
                if (tipo_precio == precio.id)
                {
                    opciones += "<option selected='selected' value='" + precio.id + "'>" + precio.Tipo + "</option>";
                }
                else
                {
                    opciones += "<option value='" + precio.id + "'>" + precio.Tipo + "</option>";
                }
            }
            var respuesta = new
            {
                opciones = opciones
            };
            return Json(respuesta, JsonRequestBehavior.AllowGet);
        }

        public ActionResult traer_precio(string codigoProducto, int idtipoPrecio, int idSucursal)
        {
            string precio = "";
            string descripcion = "";
            bool success = false;
            bool existe = false;
            int id_Producto = 0;
            bool precioDolar = false;
            Decimal iva = 0;
            if (db.producto.Where(x => x.Codigo == codigoProducto).Count() != 0)
            {
                producto p = db.producto.Where(x => x.Codigo == codigoProducto).SingleOrDefault();
                id_Producto = p.id;
                descripcion = p.Descripcion;
                if (p.precio_dolar == null)
                {
                    precioDolar = true;
                }
                else
                {
                    if (p.precio_dolar.Value == 1)
                    {
                        precioDolar = true;
                    }
                }
                if (db.precio_por_tipocliente.Where(x => x.TipoCliente_id == idtipoPrecio && x.Producto_id == id_Producto && x.sucursal_id == idSucursal).Count() != 0)
                {
                    precio = db.precio_por_tipocliente.Where(x => x.TipoCliente_id == idtipoPrecio && x.Producto_id == id_Producto && x.sucursal_id == idSucursal).SingleOrDefault().Precio.Value.ToString();

                }
                if (p.iva == null)
                {
                    configuracion_general configuracion_general = Fn.Configuracion.TraerConfiguracionGeneral();
                    if (configuracion_general.iva == null)
                        iva = 0;
                    else
                        iva = Convert.ToDecimal(configuracion_general.iva);
                }
                else
                {
                    iva = Convert.ToDecimal(p.iva);
                }
                success = true;
                existe = true;
            }
            else
            {
                existe = false;
            }
            var respuesta = new
            {
                success = success,
                precio = precio,
                id_producto = id_Producto,
                descripcion = descripcion,
                existe = existe,
                precioDolar = precioDolar,
                iva = iva
            };
            return Json(respuesta, JsonRequestBehavior.AllowGet);
        }

        public ActionResult traer_datos_cliente(string nombre, int idSucursal)
        {
            int id_tipo_precio = 0;
            int id_cliente = 0;
            bool success = false;
            try
            {
                if (db.cliente.Where(x => x.Razon_social == nombre).Count() != 0)
                {
                    id_tipo_precio = db.cliente.Where(x => x.Razon_social == nombre).SingleOrDefault().TipoCliente_id;
                    id_cliente = db.cliente.Where(x => x.Razon_social == nombre).SingleOrDefault().id;
                    success = true;
                }
            }
            catch (Exception)
            {
                success = false;
                throw;
            }
            var respuesta = new
            {
                success = success,
                id_tipo_precio = id_tipo_precio,
                id_cliente = id_cliente
            };
            return Json(respuesta, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult ModalConfirmarRecuperarCotiazacion(int tipo_entrega)
        {
            ViewBag.tipo_entrega = tipo_entrega;
            return PartialView();
        }

        public PartialViewResult ModalCancelarRecuperarCotizacion()
        {
            return PartialView();
        }

        public PartialViewResult ModalFalloRecuperarCotizacion(String folio, int tipo_entrega)
        {
            ViewBag.folio = folio;
            ViewBag.tipo_entrega = tipo_entrega;
            return PartialView();
        }

        public PartialViewResult ModalVentaVacia()
        {
            return PartialView();
        }

        public PartialViewResult ModalAdvertenciaCambioTipoPago()
        {
            return PartialView();
        }

        public PartialViewResult ModalExitoCambioTipoPago(String folio_serie_venta, String TipoPago)
        {
            ViewBag.folio = folio_serie_venta;
            ViewBag.tipo_pago = TipoPago;
            return PartialView();
        }

        public PartialViewResult ModalFalloCambioTipoPago(String error)
        {
            ViewBag.error = error;
            return PartialView();
        }

        public PartialViewResult ModalCancelarVenta(String mensaje)
        {
            ViewBag.mensaje = mensaje;
            return PartialView();
        }

        public PartialViewResult ModalCancelarEditarVenta()
        {
            return PartialView();
        }

        public PartialViewResult ModalConfirmarEditarVenta()
        {
            return PartialView();
        }

        public PartialViewResult ModalCancelarEntregaVenta(int id, String folio)
        {
            ViewBag.id = id;
            ViewBag.folio = folio;
            return PartialView();
        }

        public PartialViewResult ModalVentaEntregadaExitosamente(String folio, String folio_nuevo, String id_venta, String id_nota_credito, String cambio)
        {
            ViewBag.folio = folio;
            ViewBag.folio_nuevo = folio_nuevo;
            ViewBag.id_venta = id_venta;
            ViewBag.id_nota_credito = id_nota_credito;
            ViewBag.cambio = cambio;
            return PartialView();
        }

        public PartialViewResult ModalFalloEntregaVenta(String folio, String error)
        {
            ViewBag.folio = folio;
            ViewBag.error = error;
            return PartialView();
        }

        public PartialViewResult ModalLimpiarVenta()
        {
            return PartialView();
        }

        public PartialViewResult ModalGuardarCotizacion()
        {
            return PartialView();
        }

        public PartialViewResult ModalConfirmarVenta()
        {
            ViewBag.ListaTipoPago = db.tipo_pago_venta.ToList();
            return PartialView();
        }

        public PartialViewResult ModalFolioVenta(String folio)
        {
            ViewBag.folio = folio;
            return PartialView();
        }

        public PartialViewResult ModalAdvertenciaNoMozilla()
        {
            return PartialView();
        }

        public PartialViewResult ModalAdvertenciaImpresionNoMozilla()
        {
            return PartialView();
        }

        public PartialViewResult ModalConfirmarVentaDescentralizada(int idVenta)
        {
            configuracion_general configuracion_general = Fn.Configuracion.TraerConfiguracionGeneral();
            ViewBag.ListaNotasCredito = db.nota_credito.Where(x => x.id_venta_aplicada == idVenta).ToList();
            ViewBag.ListaTipoPago = db.tipo_pago_venta.ToList();
            ViewBag.porcentaje_comision = configuracion_general.porcentaje_comision_pago_tarjeta;
            ViewBag.iva_producto_comision = db.producto.FirstOrDefault(x => x.Codigo.Equals("TPV_COMISION")).iva;
            return PartialView(db.venta.Find(idVenta));
        }

        public PartialViewResult ModalCargandoPDF()
        {
            return PartialView();
        }

        public PartialViewResult ModalEditarPrecioProducto(String codigo, int id_tipo_precio, int id_sucursal)
        {
            configuracion_general configuracion_general = Fn.Configuracion.TraerConfiguracionGeneral();
            producto producto = db.producto.FirstOrDefault(x => x.Codigo.Equals(codigo));
            tipocliente tipo_cliente = db.tipocliente.Find(id_tipo_precio);
            sucursal sucursal = db.sucursal.Find(id_sucursal);
            decimal precio = Convert.ToDecimal(db.precio_por_tipocliente.Where(x => x.tipocliente.id == id_tipo_precio && x.Producto_id == producto.id && x.sucursal_id == id_sucursal).SingleOrDefault().Precio);
            
            if(!Convert.ToBoolean(configuracion_general.precios_netos))
                precio = precio * (1 + (Convert.ToDecimal(producto.iva) / 100));

            ViewBag.codigo = codigo;
            ViewBag.tipoprecio = tipo_cliente.Tipo.ToUpper();
            ViewBag.sucursal = sucursal.nombre_sucursal.ToUpper();
            ViewBag.precio = precio;
            ViewBag.precio_neto = configuracion_general.precios_netos;
            ViewBag.porcentaje_iva_producto = Convert.ToDecimal(producto.iva);
            return PartialView();
        }

        public ActionResult VerificarNotaCredito(String id_nota_credito)
        {
            try
            {
                String[] datos = id_nota_credito.Split('-');
                String serie = datos[0];
                String folio_con_ceros = datos[1];
                String folio_sin_ceros = "";
                Boolean ignorar = false;
                for (int i = 0; i < 5; i++)
                {
                    if (!ignorar)
                    {
                        if (folio_con_ceros.ElementAt(i) != '0')
                        {
                            folio_sin_ceros = folio_sin_ceros + folio_con_ceros.ElementAt(i);
                            ignorar = true;
                        }
                    }
                    else
                        folio_sin_ceros = folio_sin_ceros + folio_con_ceros.ElementAt(i);
                }
                int folio = Convert.ToInt32(folio_sin_ceros);
                nota_credito nota_credito = db.nota_credito.Where(x => x.folio == folio && x.serie.ToLower().Equals(serie.ToLower())).Single();
                var respuesta = new
                {
                    correcto = true,
                    id_nota_credito = nota_credito.id,
                    folio_nota_credito = Fn.Numeros.serie_folio_by_id_nota_credito(nota_credito.id),
                    sucursal = nota_credito.sucursal.nombre_sucursal,
                    estatus = nota_credito.estatus_nota_credito.nombre,
                    fecha_creacion = nota_credito.fecha_creacion.Value.ToString("dd/MM/yyyy HH:mm:ss"),
                    importe = nota_credito.cantidad,
                    id_estatus_nota_credito = nota_credito.id_estatus_nota_credito
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                var respuesta = new
                {
                    correcto = false
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult VerificarDescuento(Decimal monto, String token, int id_sucursal)
        {
            String token_admin;
            Boolean es_igual = true;
            try
            {
                usuario_punto_venta usuario = Fn.usuario_logeado.traerUsuario();
                configuracion_general configuracion_general = Fn.Configuracion.TraerConfiguracionGeneral();
                //configuracion configuracion = db.configuracion.FirstOrDefault(x => x.id_sucursal == id_sucursal);
                //token_admin = configuracion.token_descuento.ToUpper();

                //if (usuario.nivel_usuario_id > 2)
                //{
                //    if (token.ToUpper().Equals(token_admin))
                //es_igual = true;
                //    else
                //        es_igual = false;
                //}
                var respuesta = new
                {
                    correcto = es_igual,
                    monto = monto
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                var respuesta = new
                {
                    correcto = false
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
        }

        public PartialViewResult ModalNotaCreditoUsada()
        {
            return PartialView();
        }

        public PartialViewResult ModalNotaCreditoInexistente()
        {
            return PartialView();
        }

        public PartialViewResult ModalNotaCreditoCancelada()
        {
            return PartialView();
        }

        public PartialViewResult ModalAgregarNotaCredito()
        {
            return PartialView();
        }

        public PartialViewResult ModalAgregarDescuento()
        {
            return PartialView();
        }

        public PartialViewResult ModalFalloEntregaDescuento()
        {
            return PartialView();
        }

        public ActionResult VerificarEstatusVenta(int id_venta)
        {
            venta venta = db.venta.Find(id_venta);
            List<nota_credito> ListaNotasCreditoGeneradasPorVenta = db.nota_credito.Where(x => x.id_venta == id_venta).ToList();
            List<nota_credito> ListaNotasCreditoAplicadasPorVenta = db.nota_credito.Where(x => x.id_venta_aplicada == id_venta).ToList();
            var respuesta = new
            {
                nc_aplicadas_a_esta_venta = ListaNotasCreditoAplicadasPorVenta.Count,
                nc_generadas_disponibles = ListaNotasCreditoGeneradasPorVenta.Where(x => x.id_estatus_nota_credito == 1).ToList().Count,
                nc_generadas_aplicadas = ListaNotasCreditoGeneradasPorVenta.Where(x => x.id_estatus_nota_credito == 2).ToList().Count
            };
            return Json(respuesta, JsonRequestBehavior.AllowGet);
        }

        public JsonResult cantidadEnInventario(String codigo_producto, int id_sucursal)
        {
            Decimal cantidad_en_inventario = 0;
            try
            {
                producto pro = db.producto.FirstOrDefault(x => x.Codigo.Equals(codigo_producto));
                inventario inv = db.inventario.FirstOrDefault(x => x.Producto_id == pro.id && x.sucursal_id == id_sucursal);
                if (inv != null)
                    cantidad_en_inventario = Convert.ToDecimal(inv.cantidad);
                var respuesta = new
                {
                    success = true,
                    cantidad_en_inventario = cantidad_en_inventario
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);

            }
            catch (Exception)
            {
                var respuesta = new
                {
                    success = false,
                    cantidad_en_inventario = 0
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
                throw;
            }
        }

        public ActionResult VistaParcialMostrarAdjunto(int id)
        {
            pdf_venta archivo = db.pdf_venta.Find(id);
            ViewBag.id_venta = archivo.id_venta;
            return PartialView(archivo);
        }

        public FileStreamResult ObtenerPDF(int id)
        {
            pdf_venta archivo = db.pdf_venta.Find(id);
            //FileStream fs = new FileStream(Server.MapPath("/UploadFolder/adjuntos_cliente_compra/") + archivo.nombre, FileMode.Open, FileAccess.Read);
            string UploadFolderTiendaOnline = new DirectoryInfo(Server.MapPath("~/")).Parent.Parent.FullName + "\\Tienda\\Tienda Online\\UploadFolder";
            FileStream fs = new FileStream(UploadFolderTiendaOnline + "\\adjuntos_cliente_compra\\" + archivo.nombre, FileMode.Open, FileAccess.Read);
            return File(fs, "application/pdf");
        }

        public FileResult DescargarAdjunto(String nombre)
        {
            string UploadFolderTiendaOnline = new DirectoryInfo(Server.MapPath("~/")).Parent.Parent.FullName + "\\Tienda\\Tienda Online\\UploadFolder";
            return File(UploadFolderTiendaOnline + "\\adjuntos_cliente_compra\\" + nombre, System.Net.Mime.MediaTypeNames.Application.Octet, nombre);
        }

        public String SiguienteAdjunto(int id, int IdVenta)
        {
            List<pdf_venta> la = db.pdf_venta.Where(x => x.id_venta == IdVenta && (x.extension.ToLower().Equals("pdf") || x.extension.ToLower().Equals("png") || x.extension.ToLower().Equals("bmp") || x.extension.ToLower().Equals("jpg") || x.extension.ToLower().Equals("jpeg"))).ToList();
            int actual = -1;
            int siguiente = -1;
            for (int i = 0; i < la.Count; i++)
            {
                if (la.ElementAt(i).id == id)
                {
                    actual = i;
                    break;
                }
            }
            if ((actual + 1) >= la.Count)
            {
                siguiente = 0;
            }
            else
            {
                siguiente = actual + 1;
            }
            pdf_venta a = la.ElementAt(siguiente);
            return a.nombre + "|" + a.id + "|" + a.extension;
        }

        public String AnteriorAdjunto(int id, int IdVenta)
        {
            List<pdf_venta> la = db.pdf_venta.Where(x => x.id_venta == IdVenta && (x.extension.ToLower().Equals("pdf") || x.extension.ToLower().Equals("png") || x.extension.ToLower().Equals("bmp") || x.extension.ToLower().Equals("jpg") || x.extension.ToLower().Equals("jpeg"))).ToList();
            int actual = -1;
            for (int i = 0; i < la.Count; i++)
            {
                if (la.ElementAt(i).id == id)
                {
                    actual = i;
                    break;
                }
            }
            if ((actual - 1) < 0)
            {
                actual = la.Count - 1;
            }
            else
            {
                actual = actual - 1;
            }
            pdf_venta a = la.ElementAt(actual);
            return a.nombre + "|" + a.id + "|" + a.extension;
        }

public PartialViewResult ModalProductos()
        {
            return PartialView();
        }

        public PartialViewResult _ModalProductos(int pagina, int numeroProductos, String filtro, int ordenamiento, int productos_ocultos)
        {
            if (productos_ocultos == 1)
                productos_ocultos = 0;
            else
                productos_ocultos = 1;
            String query = "SELECT producto.id AS id_producto, "
                            + "(SELECT nombre FROM familia WHERE familia.id = subfamilia.Familia_id) AS nombre_familia, "
                            + "subfamilia.nombre AS nombre_subfamilia, "
                            + "producto.Codigo AS codigo_producto, "
                            + "producto.Descripcion AS descripcion_producto, "
                            + "producto.descripcion_detallada AS descripcion_detallada, "
                            + "proveedor.nombre_proveedor as proveedor_producto, "
                            + "marca.nombre as marca_producto, "
                            + "producto.precio_dolar as precio_dolar_producto, "
                            + "producto.publicar as publicado_producto, "
                            + "producto.carrusel as destacado_producto, "
                            + "producto.visible as producto_visible, "
                            + "producto.consignacion as producto_consignacion, "
                            + "(SELECT COUNT(*) FROM imagen WHERE imagen.id_producto = producto.id) as imagenes, "
                            + "(SELECT COUNT(*) FROM pdf WHERE pdf.id_producto = producto.id) as pdfs, "
                            + "producto.costo as costo, "
                            + "producto.medida as medida, "
                            + "producto.modelo as modelo "
                            + "FROM producto producto, "
                            + "subfamilia, "
                            + "proveedor, "
                            + "marca "
                            + "WHERE "
                            + "producto.visible = " + productos_ocultos + " and "
                            + "producto.SubFamilia_id = subfamilia.id and "
                            + "producto.Proveedor_id = proveedor.id and "
                            + "producto.id_marca = marca.id "
                            + "AND (producto.codigo like '%" + filtro + "%' OR producto.descripcion like '%" + filtro + "%' OR proveedor.nombre_proveedor like '%" + filtro + "%' OR subfamilia.nombre like '%" + filtro + "%' OR (SELECT nombre FROM familia WHERE familia.id = subfamilia.Familia_id) like '%" + filtro + "%')";
            if (ordenamiento == 1)
                query += " ORDER BY producto.Codigo ASC;";
            else if (ordenamiento == 2)
                query += " ORDER BY producto.Descripcion ASC;";
            else if (ordenamiento == 3)
                query += " (SELECT nombre FROM familia WHERE familia.id = subfamilia.id_familia) ASC;";
            else if (ordenamiento == 4)
                query += " ORDER BY subfamilia.nombre ASC;";
            else if (ordenamiento == 5)
                query += " ORDER BY marca.nombre ASC;";
            else if (ordenamiento == 6)
                query += " ORDER BY proveedor.nombre_proveedor ASC;";
            else if (ordenamiento == 7)
                query += " ORDER BY producto.medida ASC;";
            else if (ordenamiento == 8)
                query += " ORDER BY producto.modelo ASC;";
            DataSet ds = Fn.ConnDB.GetDataSet(query, Server.MapPath("").Split('\\').ElementAt(Server.MapPath("").Split('\\').Length - 2));
            DataTable dt = ds.Tables[0];
            List<ConsultaProducto> ListaProductos = (from DataRow row in dt.Rows
                                                     select new ConsultaProducto
                                                     {
                                                         id = Convert.ToInt32(row["id_producto"]),
                                                         proveedor = row["proveedor_producto"].ToString(),
                                                         marca = row["marca_producto"].ToString(),
                                                         familia = row["nombre_familia"].ToString(),
                                                         subfamilia = row["nombre_subfamilia"].ToString(),
                                                         codigo = row["codigo_producto"].ToString(),
                                                         descripcion = row["descripcion_producto"].ToString(),
                                                         descripcion_detallada = row["descripcion_detallada"].ToString(),
                                                         publicado = Convert.ToInt32(row["publicado_producto"]),
                                                         carrusel = Convert.ToInt32(row["destacado_producto"]),
                                                         visible = Convert.ToInt32(row["producto_visible"]),
                                                         consignacion = Convert.ToInt32(row["producto_consignacion"]),
                                                         precio_dolar = Convert.ToInt32(row["precio_dolar_producto"]),
                                                         imagenes = Convert.ToInt32(row["imagenes"]),
                                                         pdfs = Convert.ToInt32(row["pdfs"]),
                                                         costo = row["costo"] != null ? row["costo"].ToString() : "",
                                                         medida = row["medida"] != null ? row["medida"].ToString() : "",
                                                         modelo = row["modelo"] != null ? row["modelo"].ToString() : ""
                                                     }).ToList();
            int totalProductos = ListaProductos.Count();
            if (numeroProductos != 0)
            {
                ListaProductos = ListaProductos.Skip(((pagina - 1) * numeroProductos)).Take(numeroProductos).ToList();
            }
            else
            {
                numeroProductos = ListaProductos.Count();
            }

            ViewBag.ListaProductos = ListaProductos;
            ViewBag.totalProductos = totalProductos;
            if (totalProductos == 0)
                ViewBag.desde = 0;
            else
                ViewBag.desde = ((pagina - 1) * numeroProductos) + 1;
            if (totalProductos < ((pagina - 1) * numeroProductos) + numeroProductos)
                ViewBag.hasta = totalProductos;
            else
                ViewBag.hasta = ((pagina - 1) * numeroProductos) + numeroProductos;
            ViewBag.pagina = pagina;
            ViewBag.numeroProductos = numeroProductos;
            return PartialView();
        }

    }
}
