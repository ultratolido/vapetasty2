﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using POS.Models;
using System.Data.Entity;
using POS.Fn;
using MySql.Data.MySqlClient;

namespace POS.Controllers
{
    public class SucursalController : Controller
    {

        moragas_pruebasEntities1 db = new moragas_pruebasEntities1();

        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName = "Lista De Sucursales")]
        public ActionResult Index()
        {
            return View();
        }

        public PartialViewResult VistaParcialSucursal()
        {
            List<sucursal> ListaSucursales = db.sucursal.OrderBy(x => x.id).ToList();
            ViewBag.ListaSucursales = ListaSucursales;
            return PartialView();
        }

        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName = "Crear Sucursal")]
        public ActionResult NuevaSucursal()
        {
            return View();
        }

        [HttpPost]
        public ActionResult NuevaSucursal(sucursal sucursal)
        {
            db.sucursal.Add(sucursal);
            db.SaveChanges();
            crearInventarioSucursalNueva();
            return RedirectToAction("Index");
        }

        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName = "Editar Sucursal")]
        public ActionResult EditarSucursal(int id)
        {
            sucursal sucursal = db.sucursal.Find(id);
            return View(sucursal);
        }

        [HttpPost]
        public ActionResult EditarSucursal(sucursal sucursal)
        {
            db.Entry(sucursal).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public bool VerificarSucursal(int id, String sucursal)
        {
            bool existe;
            if (id == 0)
                existe = db.sucursal.Where(x => x.nombre_sucursal.ToLower().Equals(sucursal.ToLower())).Any();
            else
                existe = db.sucursal.Where(x => x.id != id && x.nombre_sucursal.ToLower().Equals(sucursal.ToLower())).Any();
            return existe;
        }

        public Boolean crearInventariosNoExistentesSucursal(int id_sucursal)
        {
            try
            {
                List<int> lista_id_productos = db.producto.Select(x => x.id).ToList();
                foreach (int x in lista_id_productos)
                {
                    inventario inventario = db.inventario.FirstOrDefault(v => v.Producto_id == x && v.sucursal_id == id_sucursal);
                    // si no existe el inventario entonces crearlo
                    if (inventario == null)
                    {
                        inventario inventario_nuevo = new inventario();
                        inventario_nuevo.Producto_id = x;
                        inventario_nuevo.sucursal_id = id_sucursal;
                        inventario_nuevo.cantidad = 0;
                        db.inventario.Add(inventario_nuevo);
                        db.SaveChanges();
                    }
                }
                return true;
            }
            catch (Exception)
            {
                return false;
                throw;
            }
        }

        public bool crearInventarioSucursalNueva()
        {
            string path = System.Web.HttpContext.Current.Server.MapPath("").Split('\\').ElementAt(System.Web.HttpContext.Current.Server.MapPath("").Split('\\').Length - 2);
            MySqlConnection dbConn = new MySqlConnection(Fn.ConnDB.getConnString(path));
            String query = "insert into inventario(producto_id,sucursal_id,cantidad)" +
                            "( " +
                            "SELECT p.id as _idProducto, s.id as _idSucursal, 0 " +
                            "FROM producto p, sucursal s " +
                            "WHERE NOT EXISTS(SELECT null " +
                            "FROM inventario i " +
                            "WHERE i.Producto_id = p.id and i.sucursal_id = s.id));";
            using (MySqlCommand com = new MySqlCommand(query, dbConn))
            {
                dbConn.Open();
                com.ExecuteNonQuery();
                dbConn.Close();
            }

            return true;
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
