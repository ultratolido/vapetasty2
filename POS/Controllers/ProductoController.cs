﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using POS.Models;
using System.Data;
using System.Data.Entity.Validation;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Data.Entity;
using POS.Fn;
using System.Web.Script.Serialization;
using MySql.Data.MySqlClient;

namespace POS.Controllers
{
    public class ProductoController : Controller
    {

        moragas_pruebasEntities1 db = new moragas_pruebasEntities1();
        
        [HttpPost]
        public ActionResult cambiarPublicado(int idProducto, int valor)
        {
            try
            {
                int _publicar = valor == 1 ? 0 : 1;
                db.producto.Find(idProducto).publicar = _publicar;
                db.SaveChanges();
                var respuesta = new
                {
                    valor = _publicar,
                    success = true
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                var respuesta = new
                {
                    success = false
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult cambiarDestacado(int idProducto, int valor)
        {
            try
            {
                int _destacado = valor == 1 ? 0 : 1;
                db.producto.Find(idProducto).carrusel = _destacado;
                db.SaveChanges();
                var respuesta = new
                {
                    valor = _destacado,
                    success = true
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                var respuesta = new
                {
                    success = false
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult cambiarDolar(int idProducto, int valor)
        {
            try
            {
                int _dolar = valor == 1 ? 0 : 1;
                db.producto.Find(idProducto).precio_dolar = _dolar;
                db.SaveChanges();
                var respuesta = new
                {
                    valor = _dolar,
                    success = true
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                var respuesta = new
                {
                    success = false
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult cambiarVisibilidad(int idProducto, int valor)
        {
            try
            {
                int _visible = valor == 1 ? 0 : 1;
                db.producto.Find(idProducto).visible = _visible;
                db.SaveChanges();
                var respuesta = new
                {
                    valor = _visible,
                    success = true
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                var respuesta = new
                {
                    success = false
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult cambiarConsignacion(int idProducto, int valor)
        {
            try
            {
                int _consignacion = valor == 1 ? 0 : 1;
                db.producto.Find(idProducto).consignacion = _consignacion;
                db.SaveChanges();
                var respuesta = new
                {
                    valor = _consignacion,
                    success = true
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                var respuesta = new
                {
                    success = false
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
        }

        public PartialViewResult parcial_txt_autocomplete_codigo()
        {
            var jsonSerialiser = new JavaScriptSerializer();
            var jsonLista = jsonSerialiser.Serialize(db.producto.Where(x => x.imagen.Count() != 0).Select(x => string.Concat(x.Codigo, " | ", x.Descripcion)));
            ViewBag.codigos_autocomplete = jsonLista;
            return PartialView();
        }

        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName = "Catalogo De Productos")]
        public ActionResult Index()
        {
            ViewBag.sucursales = Fn.usuario_logeado.traerUsuario().sucursales_por_usuario.Select(x => x.sucursal).ToList();
            ViewBag.tipoPrecio = db.tipocliente.ToList();
            return View(db.producto.ToList());
        }

        public PartialViewResult VistaParcialProductos(int pagina, int numeroProductos, String filtro, int ordenamiento, int productos_ocultos)
        {
            if (productos_ocultos == 1)
                productos_ocultos = 0;
            else
                productos_ocultos = 1;
            String query = "SELECT producto.id AS id_producto, "
                            + "(SELECT nombre FROM familia WHERE familia.id = subfamilia.Familia_id) AS nombre_familia, "
                            + "subfamilia.nombre AS nombre_subfamilia, "
                            + "producto.Codigo AS codigo_producto, "
                            + "producto.Descripcion AS descripcion_producto, "
                            + "producto.descripcion_detallada AS descripcion_detallada, "
                            + "proveedor.nombre_proveedor as proveedor_producto, "
                            + "marca.nombre as marca_producto, "
                            + "producto.precio_dolar as precio_dolar_producto, "
                            + "producto.publicar as publicado_producto, "
                            + "producto.carrusel as destacado_producto, "
                            + "producto.visible as producto_visible, "
                            + "producto.consignacion as producto_consignacion, "
                            + "(SELECT COUNT(*) FROM imagen WHERE imagen.id_producto = producto.id) as imagenes, "
                            + "(SELECT COUNT(*) FROM pdf WHERE pdf.id_producto = producto.id) as pdfs, "
                            + "producto.costo as costo, "
                            + "producto.medida as medida, "
                            + "producto.modelo as modelo "
                            + "FROM producto producto, "
                            + "subfamilia, "
                            + "proveedor, "
                            + "marca "
                            + "WHERE "
                            + "producto.visible = " + productos_ocultos + " and "
                            + "producto.SubFamilia_id = subfamilia.id and "
                            + "producto.Proveedor_id = proveedor.id and "
                            + "producto.id_marca = marca.id "
                            + "AND (producto.codigo like '%" + filtro + "%' OR producto.descripcion like '%" + filtro + "%' OR proveedor.nombre_proveedor like '%" + filtro + "%' OR subfamilia.nombre like '%" + filtro + "%' OR (SELECT nombre FROM familia WHERE familia.id = subfamilia.Familia_id) like '%" + filtro + "%')";
            if (ordenamiento == 1)
                query += " ORDER BY producto.Codigo ASC;";
            else if (ordenamiento == 2)
                query += " ORDER BY producto.Descripcion ASC;";
            else if (ordenamiento == 3)
                query += " (SELECT nombre FROM familia WHERE familia.id = subfamilia.id_familia) ASC;";
            else if (ordenamiento == 4)
                query += " ORDER BY subfamilia.nombre ASC;";
            else if (ordenamiento == 5)
                query += " ORDER BY marca.nombre ASC;";
            else if (ordenamiento == 6)
                query += " ORDER BY producto.precio_dolar DESC;";
            else if (ordenamiento == 7)
                query += " ORDER BY producto.publicar DESC;";
            else if (ordenamiento == 8)
                query += " ORDER BY producto.carrusel DESC;";
            else if (ordenamiento == 9)
                query += " ORDER BY (SELECT COUNT(*) FROM imagen WHERE imagen.id_producto = producto.id) DESC;";
            else if (ordenamiento == 10)
                query += " ORDER BY (SELECT COUNT(*) FROM pdf WHERE pdf.id_producto = producto.id) DESC;";
            DataSet ds = Fn.ConnDB.GetDataSet(query, Server.MapPath("").Split('\\').ElementAt(Server.MapPath("").Split('\\').Length - 2));
            DataTable dt = ds.Tables[0];
            List<ConsultaProducto> ListaProductos = (from DataRow row in dt.Rows
                                                     select new ConsultaProducto
                                                     {
                                                         id = Convert.ToInt32(row["id_producto"]),
                                                         proveedor = row["proveedor_producto"].ToString(),
                                                         marca = row["marca_producto"].ToString(),
                                                         familia = row["nombre_familia"].ToString(),
                                                         subfamilia = row["nombre_subfamilia"].ToString(),
                                                         codigo = row["codigo_producto"].ToString(),
                                                         descripcion = row["descripcion_producto"].ToString(),
                                                         descripcion_detallada = row["descripcion_detallada"].ToString(),
                                                         publicado = Convert.ToInt32(row["publicado_producto"]),
                                                         carrusel = Convert.ToInt32(row["destacado_producto"]),
                                                         visible = Convert.ToInt32(row["producto_visible"]),
                                                         consignacion = Convert.ToInt32(row["producto_consignacion"]),
                                                         precio_dolar = Convert.ToInt32(row["precio_dolar_producto"]),
                                                         imagenes = Convert.ToInt32(row["imagenes"]),
                                                         pdfs = Convert.ToInt32(row["pdfs"]),
                                                         costo = row["costo"] != null ? row["costo"].ToString() : "",
                                                         medida = row["medida"] != null ? row["medida"].ToString() : "",
                                                         modelo = row["modelo"] != null ? row["modelo"].ToString() : ""
                                                     }).ToList();
            int totalProductos = ListaProductos.Count();
            if (numeroProductos != 0)
            {
                ListaProductos = ListaProductos.Skip(((pagina - 1) * numeroProductos)).Take(numeroProductos).ToList();
            }
            else
            {
                numeroProductos = ListaProductos.Count();
            }

            ViewBag.ListaProductos = ListaProductos;
            ViewBag.totalProductos = totalProductos;
            if (totalProductos == 0)
                ViewBag.desde = 0;
            else
                ViewBag.desde = ((pagina - 1) * numeroProductos) + 1;
            if (totalProductos < ((pagina - 1) * numeroProductos) + numeroProductos)
                ViewBag.hasta = totalProductos;
            else
                ViewBag.hasta = ((pagina - 1) * numeroProductos) + numeroProductos;
            ViewBag.pagina = pagina;
            ViewBag.numeroProductos = numeroProductos;
            return PartialView();
        }

        public ActionResult ProductosRepetidos()
        {
            List<String> ListaRepetidos = new List<String>();
            List<producto> productos = db.producto.ToList();
            foreach (producto producto in productos)
            {
                if (db.producto.Where(x => x.Codigo.ToLower().Equals(producto.Codigo)).Count() > 1)
                    ListaRepetidos.Add(producto.Codigo);
            }
            ViewBag.ListaRepetidos = ListaRepetidos;
            return View();
        }

        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName = "Lista De Precios")]
        public ActionResult Lista_de_precios_producto(int idProducto)
        {
            return View(db.producto.Find(idProducto));
        }

        [Authorize]
        public ActionResult agregar_tipo_cliente(int idProducto)
        {
            ViewBag.id_sucursal = new SelectList(db.sucursal.OrderByDescending(y => y.id), "id", "nombre_sucursal");
            ViewBag.idProducto = idProducto;
            return View();
        }

        [HttpPost]
        public ActionResult agregar_tipo_cliente(tipocliente tc, int idProducto)
        {
            db.tipocliente.Add(tc);
            db.SaveChanges();
            return RedirectToAction("agregar_nuevo_precio", new { idProducto = idProducto });
        }

        [Authorize]
        public ActionResult agregar_nuevo_precio(int idProducto)
        {
            ViewBag.sucursales = db.sucursal.ToList();
            ViewBag.producto = db.producto.Find(idProducto);
            return View();
        }

        [HttpPost]
        public ActionResult agregar_nuevo_precio(precio_por_tipocliente pptc)
        {
            db.precio_por_tipocliente.Add(pptc);
            db.SaveChanges();
            return RedirectToAction("Lista_de_precios_producto", new { idProducto = pptc.Producto_id });
        }

        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName = "Crear Producto")]
        public PartialViewResult ModalNuevoProducto()
        {
            ViewBag.Proveedor_id = new SelectList(db.proveedor, "id", "nombre_proveedor");
            ViewBag.id_marca = new SelectList(db.marca, "id", "nombre");
            ViewBag.Familia = new SelectList(db.familia, "id", "nombre");
            ViewBag.id_tipo_precio = new SelectList(db.tipocliente, "id", "Tipo");
            ViewBag.tipoPrecio = db.tipocliente.OrderBy(x => x.orden).ToList();
            ViewBag.totalSucursales = db.sucursal.Count();
            ViewBag.sucursales = Fn.usuario_logeado.traerUsuario().sucursales_por_usuario.Select(x => x.sucursal).ToList();
            return PartialView();
        }

        [HttpPost]
        public ActionResult NuevoProducto(producto producto, List<precio_por_tipocliente> ListaPrecios)
        {
            try
            {
                configuracion_general configuracion_general = Configuracion.TraerConfiguracionGeneral();
                producto.Codigo = producto.Codigo.ToUpper();
                if (producto.Descripcion == null)
                    producto.Descripcion = "Producto sin descripcion";
                if (producto.medida == null)
                    producto.medida = "N/A";
                if (producto.precio_dolar == null)
                    producto.precio_dolar = 0;
                if (producto.publicar == null)
                    producto.publicar = 0;
                if (producto.carrusel == null)
                    producto.carrusel = 0;
                if (producto.visible == null)
                    producto.visible = 1;
                if (producto.consignacion == null)
                    producto.consignacion = 0;
                db.producto.Add(producto);
                db.SaveChanges();
                if (ListaPrecios != null)
                {
                    if (configuracion_general.centralizacion_precios_sucursales == true && configuracion_general.centralizacion_precios_web == true)
                    {
                        foreach (precio_por_tipocliente precio in ListaPrecios)
                        {
                            foreach (sucursal sucursal in db.sucursal.ToList())
                            {
                                if (precio.Precio != null)
                                {
                                    precio_por_tipocliente precio_nuevo = new precio_por_tipocliente();
                                    precio_nuevo.TipoCliente_id = precio.TipoCliente_id;
                                    precio_nuevo.Precio = precio.Precio;
                                    precio_nuevo.sucursal_id = sucursal.id;
                                    precio_nuevo.Producto_id = producto.id;
                                    db.precio_por_tipocliente.Add(precio_nuevo);
                                }
                            }
                        }
                        db.SaveChanges();
                    }
                    else if (configuracion_general.centralizacion_precios_sucursales == true && configuracion_general.centralizacion_precios_web == false)
                    {
                        foreach (precio_por_tipocliente precio in ListaPrecios.Where(x => x.sucursal_id == 1).ToList())
                        {
                            if (precio.Precio != null)
                            {
                                precio_por_tipocliente precio_nuevo = new precio_por_tipocliente();
                                precio_nuevo.TipoCliente_id = precio.TipoCliente_id;
                                precio_nuevo.Precio = precio.Precio;
                                precio_nuevo.sucursal_id = precio.sucursal_id;
                                precio_nuevo.Producto_id = producto.id;
                                db.precio_por_tipocliente.Add(precio_nuevo);
                            }
                        }
                        foreach (precio_por_tipocliente precio in ListaPrecios.Where(x => x.sucursal_id != 1).ToList())
                        {
                            foreach (sucursal sucursal in db.sucursal.Where(x => x.id != 1).ToList())
                            {
                                if (precio.Precio != null)
                                {
                                    precio_por_tipocliente precio_nuevo = new precio_por_tipocliente();
                                    precio_nuevo.TipoCliente_id = precio.TipoCliente_id;
                                    precio_nuevo.Precio = precio.Precio;
                                    precio_nuevo.sucursal_id = sucursal.id;
                                    precio_nuevo.Producto_id = producto.id;
                                    db.precio_por_tipocliente.Add(precio_nuevo);
                                }
                            }
                        }
                        db.SaveChanges();
                    }
                    else
                    {
                        foreach (precio_por_tipocliente precio in ListaPrecios)
                        {
                            if (precio.Precio != null)
                            {
                                precio_por_tipocliente precio_nuevo = new precio_por_tipocliente();
                                precio_nuevo.TipoCliente_id = precio.TipoCliente_id;
                                precio_nuevo.Precio = precio.Precio;
                                precio_nuevo.sucursal_id = precio.sucursal_id;
                                precio_nuevo.Producto_id = producto.id;
                                db.precio_por_tipocliente.Add(precio_nuevo);
                            }
                        }
                        db.SaveChanges();
                    }
                }
                foreach (sucursal sucursal in db.sucursal)
                {
                    inventario inventario = new inventario();
                    inventario.Producto_id = producto.id;
                    inventario.sucursal_id = sucursal.id;
                    inventario.cantidad = 0;
                    db.inventario.Add(inventario);
                }
                db.SaveChanges();
                var respuesta = new
                {
                    success = true,
                    id_producto = producto.id
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                var respuesta = new
                {
                    success = false
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
        }

        [Authorize]
        public ActionResult NuevoPrecioProducto(int idSucursal, string codigoProducto, int idtipoPrecio, decimal precio)
        {
            bool success = false;
            string error = "";
            try
            {
                bool existe = db.precio_por_tipocliente.Where(x => x.producto.Codigo == codigoProducto && x.TipoCliente_id == idtipoPrecio && x.sucursal_id == idSucursal).Any();
                if (!existe)
                {
                    precio_por_tipocliente pptc = new precio_por_tipocliente();
                    pptc.Producto_id = db.producto.Where(x => x.Codigo == codigoProducto).Single().id;
                    pptc.TipoCliente_id = idtipoPrecio;
                    pptc.sucursal_id = idSucursal;
                    pptc.Precio = precio;
                    db.precio_por_tipocliente.Add(pptc);
                    db.SaveChanges();
                    success = true;
                }
                else
                {
                    error = "Ya ha sido registrado este precio";
                }
            }
            catch (Exception e)
            {
                error = e.Message;
            }
            var respuesta = new
            {
                success = success,
                error = error
            };
            return Json(respuesta, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName = "Editar Producto")]
        public PartialViewResult ModalEditarProducto(int id)
        {
            ViewBag.UploadFolderPath = new DirectoryInfo(Server.MapPath("~/")).Parent.Parent.FullName + "\\Tienda\\Tienda Online\\UploadFolder";
            var rd = HttpContext.Request.RequestContext.RouteData;
            string nombreControlador = rd.GetRequiredString("controller");
            string nombreAction = rd.GetRequiredString("action");

            if (Fn.usuario_logeado.revisarPermisoAccesoVista(nombreControlador, nombreAction))
            {
                producto producto = db.producto.Find(id);
                List<precio_por_tipocliente> ListaPrecios = db.precio_por_tipocliente.Where(x => x.Producto_id == producto.id).ToList();
                List<int> ListaIdTipoPrecio = ListaPrecios.Select(x => x.TipoCliente_id).ToList();
                ViewBag.Proveedor_id = new SelectList(db.proveedor, "id", "nombre_proveedor", producto.Proveedor_id);
                ViewBag.id_marca = new SelectList(db.marca, "id", "nombre", producto.id_marca);
                ViewBag.Familia = new SelectList(db.familia, "id", "nombre", producto.subfamilia.Familia_id);
                ViewBag.SubFamilia_id = new SelectList(db.subfamilia.Where(x => x.Familia_id == producto.subfamilia.Familia_id), "id", "nombre", producto.SubFamilia_id);
                if (db.tipocliente.Where(x => !ListaIdTipoPrecio.Contains(x.id)).ToList().Count() > 0)
                    ViewBag.id_tipo_precio = new SelectList(db.tipocliente.Where(x => !ListaIdTipoPrecio.Contains(x.id)), "id", "Tipo");
                else
                    ViewBag.id_tipo_precio = null;
                ViewBag.ListaPrecios = ListaPrecios;
                ViewBag.producto = producto;
                ViewBag.sucursales = Fn.usuario_logeado.traerUsuario().sucursales_por_usuario.Select(x => x.sucursal).ToList();
                ViewBag.totalSucursales = db.sucursal.Count();
                ViewBag.tipoPrecio = db.tipocliente.OrderBy(x => x.orden).ToList();
                return PartialView();
            }
            else
            {
                ViewBag.ControllerName = nombreControlador;
                ViewBag.ActionName = nombreAction;
                return PartialView("~/Views/Account/ParcialAccesoDenegadoVista.cshtml");
            }
        }

        [HttpPost]
        public ActionResult EditarProducto(producto producto, List<precio_por_tipocliente> ListaPrecios, List<int> ImagenesEliminadas, List<int> PDFsEliminadas)
        {
            try
            {
                String UploadFolderTiendaOnline = new DirectoryInfo(Server.MapPath("~/")).Parent.FullName + "\\Tienda Online\\UploadFolder";
                configuracion_general configuracion_general = Configuracion.TraerConfiguracionGeneral();
                producto producto_guardado = db.producto.Find(producto.id);
                producto_guardado.Codigo = producto.Codigo.ToUpper();
                producto_guardado.Descripcion = producto.Descripcion;
                producto_guardado.descripcion_detallada = producto.descripcion_detallada;
                producto_guardado.id_marca = producto.id_marca;
                producto_guardado.SubFamilia_id = producto.SubFamilia_id;
                producto_guardado.Proveedor_id = producto.Proveedor_id;
                producto_guardado.costo = producto.costo;
                producto_guardado.medida = producto.medida;
                producto_guardado.modelo = producto.modelo;
                producto_guardado.precio_dolar = producto.precio_dolar;
                producto_guardado.publicar = producto.publicar;
                producto_guardado.carrusel = producto.carrusel;
                producto_guardado.iva = producto.iva;
                producto_guardado.visible = producto.visible;
                producto_guardado.consignacion = producto.consignacion;
                if (producto.Descripcion == null)
                    producto_guardado.Descripcion = "Producto sin descripcion";
                if (producto.medida == null)
                    producto_guardado.medida = "N/A";
                if (producto.precio_dolar == null)
                    producto_guardado.precio_dolar = 0;
                if (producto.publicar == null)
                    producto_guardado.publicar = 0;
                if (producto.carrusel == null)
                    producto_guardado.carrusel = 0;
                if (producto.visible == null)
                    producto_guardado.visible = 1;
                if (producto.consignacion == null)
                    producto_guardado.consignacion = 0;
                db.Entry(producto_guardado).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                if (ListaPrecios != null)
                {
                    if (configuracion_general.centralizacion_precios_sucursales == true && configuracion_general.centralizacion_precios_web == true)
                    {
                        foreach (precio_por_tipocliente precio in ListaPrecios)
                        {
                            foreach (sucursal sucursal in db.sucursal.ToList())
                            {
                                if (precio.Precio == null)
                                {
                                    //SI EXISTE, ELIMINALO, SI NO EXISTE, NO HAGAS NADA
                                    if (db.precio_por_tipocliente.Where(x => x.Producto_id == producto.id && x.TipoCliente_id == precio.TipoCliente_id && x.sucursal_id == sucursal.id).Any())
                                    {
                                        precio_por_tipocliente precio_eliminar = db.precio_por_tipocliente.Where(x => x.Producto_id == producto.id && x.TipoCliente_id == precio.TipoCliente_id && x.sucursal_id == sucursal.id).First();
                                        db.precio_por_tipocliente.Remove(precio_eliminar);
                                        db.SaveChanges();
                                    }
                                }
                                else
                                {
                                    //SI EXISTE EDITALO
                                    if (db.precio_por_tipocliente.Where(x => x.Producto_id == producto.id && x.TipoCliente_id == precio.TipoCliente_id && x.sucursal_id == sucursal.id).Any())
                                    {
                                        precio_por_tipocliente precio_nuevo = db.precio_por_tipocliente.Where(x => x.Producto_id == producto.id && x.TipoCliente_id == precio.TipoCliente_id && x.sucursal_id == sucursal.id).First();
                                        precio_nuevo.Precio = precio.Precio;
                                        db.Entry(precio_nuevo).State = System.Data.Entity.EntityState.Modified;
                                        db.SaveChanges();
                                    }
                                    //SI NO EXISTE CREALO
                                    else
                                    {
                                        precio_por_tipocliente precio_nuevo = new precio_por_tipocliente();
                                        precio_nuevo.TipoCliente_id = precio.TipoCliente_id;
                                        precio_nuevo.Precio = precio.Precio;
                                        precio_nuevo.sucursal_id = sucursal.id;
                                        precio_nuevo.Producto_id = producto.id;
                                        db.precio_por_tipocliente.Add(precio_nuevo);
                                        db.SaveChanges();
                                    }
                                }
                            }
                        }
                        db.SaveChanges();
                    }
                    else if (configuracion_general.centralizacion_precios_sucursales == true && configuracion_general.centralizacion_precios_web == false)
                    {
                        foreach (precio_por_tipocliente precio in ListaPrecios.Where(x => x.sucursal_id == 1).ToList())
                        {
                            if (precio.Precio == null)
                            {
                                //SI EXISTE, ELIMINALO, SI NO EXISTE, NO HAGAS NADA
                                if (db.precio_por_tipocliente.Where(x => x.Producto_id == producto.id && x.TipoCliente_id == precio.TipoCliente_id && x.sucursal_id == precio.sucursal_id).Any())
                                {
                                    precio_por_tipocliente precio_eliminar = db.precio_por_tipocliente.Where(x => x.Producto_id == producto.id && x.TipoCliente_id == precio.TipoCliente_id && x.sucursal_id == precio.sucursal_id).First();
                                    db.precio_por_tipocliente.Remove(precio_eliminar);
                                    db.SaveChanges();
                                }
                            }
                            else
                            {
                                //SI EXISTE EDITALO
                                if (db.precio_por_tipocliente.Where(x => x.Producto_id == producto.id && x.TipoCliente_id == precio.TipoCliente_id && x.sucursal_id == precio.sucursal_id).Any())
                                {
                                    precio_por_tipocliente precio_nuevo = db.precio_por_tipocliente.Where(x => x.Producto_id == producto.id && x.TipoCliente_id == precio.TipoCliente_id && x.sucursal_id == precio.sucursal_id).First();
                                    precio_nuevo.Precio = precio.Precio;
                                    db.Entry(precio_nuevo).State = System.Data.Entity.EntityState.Modified;
                                    db.SaveChanges();
                                }
                                //SI NO EXISTE CREALO
                                else
                                {
                                    precio_por_tipocliente precio_nuevo = new precio_por_tipocliente();
                                    precio_nuevo.TipoCliente_id = precio.TipoCliente_id;
                                    precio_nuevo.Precio = precio.Precio;
                                    precio_nuevo.sucursal_id = precio.sucursal_id;
                                    precio_nuevo.Producto_id = producto.id;
                                    db.precio_por_tipocliente.Add(precio_nuevo);
                                    db.SaveChanges();
                                }
                            }
                        }
                        foreach (precio_por_tipocliente precio in ListaPrecios.Where(x => x.sucursal_id != 1).ToList())
                        {
                            foreach (sucursal sucursal in db.sucursal.Where(x => x.id != 1).ToList())
                            {
                                if (precio.Precio == null)
                                {
                                    //SI EXISTE, ELIMINALO, SI NO EXISTE, NO HAGAS NADA
                                    if (db.precio_por_tipocliente.Where(x => x.Producto_id == producto.id && x.TipoCliente_id == precio.TipoCliente_id && x.sucursal_id == sucursal.id).Any())
                                    {
                                        precio_por_tipocliente precio_eliminar = db.precio_por_tipocliente.Where(x => x.Producto_id == producto.id && x.TipoCliente_id == precio.TipoCliente_id && x.sucursal_id == sucursal.id).First();
                                        db.precio_por_tipocliente.Remove(precio_eliminar);
                                        db.SaveChanges();
                                    }
                                }
                                else
                                {
                                    //SI EXISTE EDITALO
                                    if (db.precio_por_tipocliente.Where(x => x.Producto_id == producto.id && x.TipoCliente_id == precio.TipoCliente_id && x.sucursal_id == sucursal.id).Any())
                                    {
                                        precio_por_tipocliente precio_nuevo = db.precio_por_tipocliente.Where(x => x.Producto_id == producto.id && x.TipoCliente_id == precio.TipoCliente_id && x.sucursal_id == sucursal.id).First();
                                        precio_nuevo.Precio = precio.Precio;
                                        db.Entry(precio_nuevo).State = System.Data.Entity.EntityState.Modified;
                                        db.SaveChanges();
                                    }
                                    //SI NO EXISTE CREALO
                                    else
                                    {
                                        precio_por_tipocliente precio_nuevo = new precio_por_tipocliente();
                                        precio_nuevo.TipoCliente_id = precio.TipoCliente_id;
                                        precio_nuevo.Precio = precio.Precio;
                                        precio_nuevo.sucursal_id = sucursal.id;
                                        precio_nuevo.Producto_id = producto.id;
                                        db.precio_por_tipocliente.Add(precio_nuevo);
                                        db.SaveChanges();
                                    }
                                }
                            }
                        }
                        db.SaveChanges();
                    }
                    else
                    {
                        foreach (precio_por_tipocliente precio in ListaPrecios)
                        {
                            if (precio.Precio == null)
                            {
                                //SI EXISTE, ELIMINALO, SI NO EXISTE, NO HAGAS NADA
                                if (db.precio_por_tipocliente.Where(x => x.Producto_id == producto.id && x.TipoCliente_id == precio.TipoCliente_id && x.sucursal_id == precio.sucursal_id).Any())
                                {
                                    precio_por_tipocliente precio_eliminar = db.precio_por_tipocliente.Where(x => x.Producto_id == producto.id && x.TipoCliente_id == precio.TipoCliente_id && x.sucursal_id == precio.sucursal_id).First();
                                    db.precio_por_tipocliente.Remove(precio_eliminar);
                                    db.SaveChanges();
                                }
                            }
                            else
                            {
                                //SI EXISTE EDITALO
                                if (db.precio_por_tipocliente.Where(x => x.Producto_id == producto.id && x.TipoCliente_id == precio.TipoCliente_id && x.sucursal_id == precio.sucursal_id).Any())
                                {
                                    precio_por_tipocliente precio_nuevo = db.precio_por_tipocliente.Where(x => x.Producto_id == producto.id && x.TipoCliente_id == precio.TipoCliente_id && x.sucursal_id == precio.sucursal_id).First();
                                    precio_nuevo.Precio = precio.Precio;
                                    db.Entry(precio_nuevo).State = System.Data.Entity.EntityState.Modified;
                                    db.SaveChanges();
                                }
                                //SI NO EXISTE CREALO
                                else
                                {
                                    precio_por_tipocliente precio_nuevo = new precio_por_tipocliente();
                                    precio_nuevo.TipoCliente_id = precio.TipoCliente_id;
                                    precio_nuevo.Precio = precio.Precio;
                                    precio_nuevo.sucursal_id = precio.sucursal_id;
                                    precio_nuevo.Producto_id = producto.id;
                                    db.precio_por_tipocliente.Add(precio_nuevo);
                                    db.SaveChanges();
                                }
                            }
                        }
                        db.SaveChanges();
                    }
                }
                if (ImagenesEliminadas != null)
                {
                    foreach (int id_imagen in ImagenesEliminadas)
                    {
                        imagen imagen = db.imagen.Find(id_imagen);
                        System.IO.File.Delete(UploadFolderTiendaOnline + "\\imagen_producto\\" + imagen.nombre);
                        db.imagen.Remove(imagen);
                        db.SaveChanges();
                    }
                }
                if (PDFsEliminadas != null)
                {
                    foreach (int id_pdf in PDFsEliminadas)
                    {
                        pdf pdf = db.pdf.Find(id_pdf);
                        System.IO.File.Delete(UploadFolderTiendaOnline + "\\descripcion_detalle_producto\\" + pdf.nombre);
                        db.pdf.Remove(pdf);
                        db.SaveChanges();
                    }
                }
                var respuesta = new
                {
                    success = true,
                    id_producto = producto.id
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                var respuesta = new
                {
                    success = false
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
        }

        public ViewResult NuevoProveedor(int id_producto)
        {
            ViewBag.id_producto = id_producto;
            return View();
        }

        [HttpPost]
        public ActionResult NuevoProveedor(proveedor proveedor, int id_producto)
        {
            db.proveedor.Add(proveedor);
            db.SaveChanges();
            if (id_producto == 0)
                return RedirectToAction("agregar_nuevo_producto");
            else
                return RedirectToAction("editarProducto", new { id = id_producto });
        }

        public ViewResult NuevaSubfamilia(int id_producto)
        {
            ViewBag.id_producto = id_producto;
            return View();
        }

        [HttpPost]
        public ActionResult NuevaSubfamilia(subfamilia subfamilia, int id_producto)
        {
            db.subfamilia.Add(subfamilia);
            db.SaveChanges();
            if (id_producto == 0)
                return RedirectToAction("agregar_nuevo_producto");
            else
                return RedirectToAction("editarProducto", new { id = id_producto });
        }

        public ViewResult NuevaMarca(int id_producto)
        {
            ViewBag.id_producto = id_producto;
            return View();
        }

        [HttpPost]
        public ActionResult NuevaMarca(marca marca, int id_producto)
        {
            db.marca.Add(marca);
            db.SaveChanges();
            if (id_producto == 0)
                return RedirectToAction("agregar_nuevo_producto");
            else
                return RedirectToAction("editarProducto", new { id = id_producto });
        }

        [HttpPost]
        public ActionResult agregar_nueva_marca(string nombre_marca)
        {
            marca m = new marca();
            m.nombre = nombre_marca;
            db.marca.Add(m);
            try
            {
                db.SaveChanges();
                var respuesta = new
                {
                    success = true,
                    idMarca = m.id,
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                var respuesta = new
                {
                    success = false,
                    idMarca = 1
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult agregar_nuevo_proveedor(string nombre_proveedor, String correo, String telefono)
        {
            proveedor p = new proveedor();
            p.nombre_proveedor = nombre_proveedor;
            p.correo = correo;
            p.telefono = telefono;
            db.proveedor.Add(p);
            try
            {
                db.SaveChanges();
                var respuesta = new
                {
                    success = true,
                    idProveedor = p.id,
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                var respuesta = new
                {
                    success = false,
                    idMarca = 1
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult guardarNuevaFamilia(string nombre)
        {
            familia f = new Models.familia();
            f.nombre = nombre;
            db.familia.Add(f);
            try
            {
                db.SaveChanges();
                var respuesta = new
                {
                    success = true,
                    idFamilia = f.id,
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                var respuesta = new
                {
                    success = false,
                    idFamilia = 1,
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult guardarNuevaSubFamilia(string nombre, int idFamilia)
        {
            subfamilia sf = new Models.subfamilia();
            sf.nombre = nombre;
            sf.Familia_id = idFamilia;
            db.subfamilia.Add(sf);
            try
            {
                db.SaveChanges();
                var respuesta = new
                {
                    success = true,
                    idFamilia = sf.Familia_id,
                    idSubfamilia = sf.id,
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                var respuesta = new
                {
                    success = false,
                    idFamilia = 1,
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
        }

        [Authorize]
        public ActionResult Editar_precio_tipo_cliente(int id)
        {
            return View(db.precio_por_tipocliente.Find(id));
        }

        [HttpPost]
        public ActionResult Editar_precio_tipo_cliente(precio_por_tipocliente model)
        {
            precio_por_tipocliente pptc = db.precio_por_tipocliente.Find(model.id);
            pptc.Precio = model.Precio;
            db.SaveChanges();
            return RedirectToAction("Lista_de_precios_producto", new { idProducto = pptc.Producto_id });
        }

        [Authorize]
        public ActionResult traer_Tipo_precio_sucursal(int idSucursal, int idProducto)
        {
            //List<int> tipo_cliente_sucursal = db.precio_por_tipocliente.Where(x => x.sucursal_id == idSucursal && x.Producto_id == idProducto).Select(x => x.TipoCliente_id).ToList();
            //return Json(
            //    db.tipocliente.Where(x => x.sucursal_id == idSucursal && !tipo_cliente_sucursal.Contains(x.id)).Select(x => new { value = x.id, text = x.Tipo }),
            //    JsonRequestBehavior.AllowGet
            //);
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult modal_lista_productos(int idSucursal)
        {
            List<inventario> inventario = db.inventario.Where(x => x.sucursal_id == idSucursal).ToList();
            ViewBag.idSucursal = idSucursal;
            ViewBag.nombreSucursal = db.sucursal.Find(idSucursal).nombre_sucursal;
            ViewBag.inventario = inventario;
            return PartialView("modal_lista_productos", db.producto.ToList());
        }

        public String traer_familia_select()
        {
            string opciones = "";
            foreach (familia f in db.familia.OrderBy(x => x.nombre))
            {
                opciones += "<option value='" + f.id + "'>" + f.nombre + "</option>";
            }
            return opciones;
        }

        public String traer_subfamilia_select(int idFamilia)
        {
            string opciones = "";
            foreach (subfamilia s in db.subfamilia.Where(x => x.Familia_id == idFamilia).OrderBy(x => x.nombre))
            {
                opciones += "<option value='" + s.id + "'>" + s.nombre + "</option>";
            }
            return opciones;
        }

        public String traer_marcas_select()
        {
            string opciones = "";
            foreach (marca s in db.marca.OrderBy(x => x.nombre))
            {
                opciones += "<option value='" + s.id + "'>" + s.nombre + "</option>";
            }
            return opciones;
        }

        public String traer_proveedores_select()
        {
            string opciones = "";
            foreach (proveedor s in db.proveedor.OrderBy(x => x.nombre_proveedor))
            {
                opciones += "<option value='" + s.id + "'>" + s.nombre_proveedor + "</option>";
            }
            return opciones;
        }
        
        public ActionResult modalCargaArchivoExcel()// CargandoProductosVistaParcial()
        {
            return PartialView();
        }

        public void SubirArchivoProductos(string new_fileName)
        {

            //RECUPERAR ARCHIVO A SUBIR
            UploadedFile file = RetrieveFileFromRequest();
            file.Filename = new_fileName;
            string rootPath = AppDomain.CurrentDomain.BaseDirectory;
            String savedFilePath = Server.MapPath("~/ArchivosExcel/") + new_fileName;
            System.IO.File.WriteAllBytes(savedFilePath, file.Contents);
        }

        public ActionResult modalResultadoAnalizarArchivoExcelPRECIOS()
        {
            int idUsuarioLoggeado = Fn.usuario_logeado.id();
            List<precioactualizacionexcel> Lproductos = db.precioactualizacionexcel.Where(x => x.idUsuarioActualiza == idUsuarioLoggeado).ToList();
            //revisar duplicados
            List<string> codigosDuplicados = Lproductos.Select(x => x.Codigo)
                .GroupBy(i => i)
                .Where(g => g.Count() > 1)
                .Select(g => g.Key).ToList();
            ViewBag.duplicados = codigosDuplicados.Count();
            ViewBag.nuevos = Lproductos.Where(x => x.nuevo).Count();
            ViewBag.actualizacion = Lproductos.Where(x => !x.nuevo).Count();
            return PartialView(db.precioactualizacionexcel.Where(x=>x.idUsuarioActualiza==idUsuarioLoggeado).ToList());
        }

        public ActionResult AvancePrecios()
        {
            var respuesta = new
            {
                total = 0,
                copiados = 0,
                porcentaje = 0,
                label_avance = "ERROR AL CARGAR ARCHIVO"
            };

            int idUsuarioActualiza = Fn.usuario_logeado.id();

            int total = db.precioactualizacionexcel.Where(x => x.idUsuarioActualiza == idUsuarioActualiza).Count();
            if (total != 0)
            {
                int copiados = db.precioactualizacionexcel.Where(x => x.copiado && x.idUsuarioActualiza == idUsuarioActualiza).Count();
                int porcentaje = (copiados * 100) / total;
                string label_avance = "  (" + copiados + "/" + total + ")";
                respuesta = new
                {
                    total = total,
                    copiados = copiados,
                    porcentaje = porcentaje,
                    label_avance = label_avance
                };
            }
            return Json(respuesta, JsonRequestBehavior.AllowGet);
        }

        public ActionResult modalResultadoAnalizarArchivoExcel()
        {
            int idUsuarioLoggeado = Fn.usuario_logeado.id();
            List<nuevo_producto> Lproductos = db.nuevo_producto.Where(x => x.idUsuarioActualiza == idUsuarioLoggeado).ToList();
            //revisar duplicados
            List<string> codigosDuplicados = Lproductos.Select(x => x.codigo)
                .GroupBy(i => i)
                .Where(g => g.Count() > 1)
                .Select(g => g.Key).ToList();
            ViewBag.duplicados = codigosDuplicados.Count();
            ViewBag.nuevos = Lproductos.Where(x => x.nuevo == 1).Count();
            ViewBag.actualizacion = Lproductos.Where(x => x.nuevo == 0).Count();
            return PartialView();
        }

        public ActionResult AvanceProductos()
        {
            var respuesta = new
            {
                total = 0,
                copiados = 0,
                porcentaje = 0,
                label_avance = "ERROR AL CARGAR ARCHIVO"
            };

            int total = db.nuevo_producto.Count();
            if (total != 0)
            {
                int copiados = db.nuevo_producto.Where(x => x.copiadoCatolgoDescripcion != 1).Count();
                int porcentaje = (copiados * 100) / total;
                string label_avance = "  (" + copiados + "/" + total + ")";
                respuesta = new
                {
                    total = total,
                    copiados = copiados,
                    porcentaje = porcentaje,
                    label_avance = label_avance
                };
            }
            return Json(respuesta, JsonRequestBehavior.AllowGet);
        }

        private UploadedFile RetrieveFileFromRequest()
        {
            string filename = null;
            string fileType = null;
            byte[] fileContents = null;
            string ID;

            if (Request.Files.Count > 0)
            { //they're uploading the old way
                var file = Request.Files[0];
                fileContents = new byte[file.ContentLength];
                fileType = file.ContentType;
                filename = file.FileName;

            }
            else if (Request.ContentLength > 0)
            {
                fileContents = new byte[Request.ContentLength];
                Request.InputStream.Read(fileContents, 0, Request.ContentLength);
                filename = Request.Headers["X-File-Name"];
                fileType = Request.Headers["X-File-Type"];
            }

            return new UploadedFile()
            {
                Filename = filename,
                ContentType = fileType,
                FileSize = fileContents != null ? fileContents.Length : 0,
                Contents = fileContents
            };
        }

        public ActionResult listaExcelProductosFinalizado()
        {
            int idUsuarioLoggeado = Fn.usuario_logeado.id();
            List<nuevo_producto> Lproductos = db.nuevo_producto.Where(x => x.idUsuarioActualiza == idUsuarioLoggeado).ToList();
            ViewBag.titulo = "Productos Finalizado";
            return PartialView(Lproductos);
        }

        public ActionResult listaExcelProductosFinalizadoPrecios()
        {
            int idUsuarioLoggeado = Fn.usuario_logeado.id();
            ViewBag.usuario = Fn.usuario_logeado.Nombre();
            List<precioactualizacionexcel> Lproductos = db.precioactualizacionexcel.Where(x => x.idUsuarioActualiza == idUsuarioLoggeado).ToList();
            return PartialView(Lproductos);
        }

        public ActionResult modal_listaExcelProductos(bool nuevos)
        {
            int idUsuarioLoggeado = Fn.usuario_logeado.id();
            List<nuevo_producto> listaProductos = new List<nuevo_producto>();
            if (nuevos)
            {
                listaProductos = db.nuevo_producto.Where(x => x.nuevo == 1 && x.idUsuarioActualiza == idUsuarioLoggeado).ToList();
                ViewBag.titulo = "Productos Nuevo Excel";
            }
            else
            {
                listaProductos = db.nuevo_producto.Where(x => x.nuevo == 0 && x.idUsuarioActualiza == idUsuarioLoggeado).ToList();
                ViewBag.titulo = "Productos Actualizacion Excel";
            }
            return PartialView(listaProductos);
        }

        public ActionResult modal_listaExcelProductosDuplicados()
        {
            int idUsuarioLoggeado = Fn.usuario_logeado.id();
            List<nuevo_producto> listaProductos = new List<nuevo_producto>();
            List<nuevo_producto> Lproductos = db.nuevo_producto.Where(x => x.idUsuarioActualiza == idUsuarioLoggeado).ToList();
            List<string> codigosDuplicados = Lproductos.Select(x => x.codigo)
            .GroupBy(i => i)
            .Where(g => g.Count() > 1)
            .Select(g => g.Key).ToList();

            listaProductos = Lproductos.Where(x => codigosDuplicados.Contains(x.codigo)).ToList();
            ViewBag.titulo = "Productos Duplicados";
            return PartialView(listaProductos);
        }

        public ActionResult modalSeleccionarParametrosActualizacion()
        {
            ViewBag.sucursales = db.sucursal.ToList();
            ViewBag.tipocliente_id = db.tipocliente.ToList();
            //ViewBag.cbx_sucursal = new SelectList(db.sucursal, "id", "nombre_sucursal");
            return PartialView();
        }

        public ActionResult modalSeleccionarTipoPrecioActualizar()
        {
            List<sucursal> sucursales = new List<sucursal>();
            configuracion_general c = Fn.Configuracion.TraerConfiguracionGeneral();
            if (c.centralizacion_precios_sucursales.Value && c.centralizacion_precios_web.Value)
            {

            }
            else if (c.centralizacion_precios_sucursales.Value && !c.centralizacion_precios_web.Value)
            {
                sucursales.Add(db.sucursal.Find(1));
                sucursal s = new sucursal();
                s.id = 2;
                s.nombre_sucursal = "TODAS MENOS WEB";
                sucursales.Add(s);
            }
            else
            {
                sucursales = db.sucursal.ToList();
            }

            ViewBag.sucursales = sucursales;
            ViewBag.tipocliente_id = db.tipocliente.ToList();
            //ViewBag.cbx_sucursal = new SelectList(db.sucursal, "id", "nombre_sucursal");
            return PartialView();
        }

        public PartialViewResult modal_log(String tabla)
        {
            String datos = HttpUtility.UrlDecode(tabla, System.Text.Encoding.Default); ;
            ViewBag.tabla = HttpUtility.UrlDecode(tabla, System.Text.Encoding.Default).Replace('\n', ' ');
            return PartialView();
        }

        public PartialViewResult ModalNuevoPrecioProducto()
        {
            return PartialView();
        }

        public bool VerificarProducto(int id, String codigo)
        {
            bool existe;
            if (id == 0)
                existe = db.producto.Where(x => x.Codigo.ToLower().Equals(codigo.ToLower())).Any();
            else
                existe = db.producto.Where(x => x.id != id && x.Codigo.ToLower().Equals(codigo.ToLower())).Any();
            return existe;
        }

        public bool VerificarMarca(String nombre_marca)
        {
            bool existe;
            existe = db.marca.Where(x => x.nombre.ToLower().Equals(nombre_marca.ToLower())).Any();
            return existe;
        }

        public bool VerificarTipoPrecio(int id, int id_sucursal, String tipo_precio)
        {
            bool existe;
            if (id == 0)
                existe = db.tipocliente.Where(x => x.Tipo.ToLower().Equals(tipo_precio.ToLower())).Any();
            else
                existe = db.tipocliente.Where(x => x.id != id && x.Tipo.ToLower().Equals(tipo_precio.ToLower())).Any();
            return existe;
        }

        public bool VerificarProveedor(int id, String proveedor)
        {
            bool existe;
            if (id == 0)
                existe = db.proveedor.Where(x => x.nombre_proveedor.ToLower().Equals(proveedor.ToLower())).Any();
            else
                existe = db.proveedor.Where(x => x.id != id && x.nombre_proveedor.ToLower().Equals(proveedor.ToLower())).Any();
            return existe;
        }

        public bool VerificarFamilia(int id, String familia)
        {
            bool existe;
            if (id == 0)
                existe = db.familia.Where(x => x.nombre.ToLower().Equals(familia.ToLower())).Any();
            else
                existe = db.familia.Where(x => x.id != id && x.nombre.ToLower().Equals(familia.ToLower())).Any();
            return existe;
        }

        public bool VerificarSubfamilia(int id, int id_familia, String subfamilia)
        {
            bool existe;
            if (id == 0)
                existe = db.subfamilia.Where(x => x.nombre.ToLower().Equals(subfamilia.ToLower()) && x.Familia_id == id_familia).Any();
            else
                existe = db.subfamilia.Where(x => x.id != id && x.nombre.ToLower().Equals(subfamilia.ToLower()) && x.Familia_id == id_familia).Any();
            return existe;
        }

        [HttpPost]
        public void SubirAdjuntos(int id_producto, int whatIs)
        {
            String UploadFolderTiendaOnline = new DirectoryInfo(Server.MapPath("~/")).Parent.FullName + "\\Tienda Online\\UploadFolder";
            UploadedFile file = RetrieveFileFromRequest();
            String extension = System.IO.Path.GetExtension(file.Filename).ToLower();
            if (whatIs == 0)
            {
                imagen imagen = new imagen();
                imagen.id_producto = id_producto;
                imagen.nombre = file.Filename.Replace(" ", "");
                imagen.extension = extension.Substring(1);
                db.imagen.Add(imagen);
                db.SaveChanges();
                System.IO.File.WriteAllBytes(UploadFolderTiendaOnline + "\\imagen_producto\\" + imagen.id + "-" + imagen.nombre, file.Contents);
                imagen = db.imagen.Find(imagen.id);
                if (extension.Substring(1) == "png" || extension.Substring(1) == "bmp" || extension.Substring(1) == "jpg" || extension.Substring(1) == "jpeg" || extension.Substring(1) == "gif")
                {
                    System.Drawing.Image image = System.Drawing.Image.FromFile(UploadFolderTiendaOnline + "\\imagen_producto\\" + imagen.id + "-" + imagen.nombre);
                    int imgWidth = image.Width;
                    int imgHeight = image.Height;
                    image.Dispose();
                    imagen.width = imgWidth;
                    imagen.height = imgHeight;
                }
                else
                {
                    imagen.width = 0;
                    imagen.height = 0;
                }
                imagen.nombre = imagen.id + "-" + file.Filename.Replace(" ", "");
                db.Entry(imagen).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            else if (whatIs == 1)
            {
                pdf pdf = new pdf();
                pdf.id_producto = id_producto;
                pdf.nombre = file.Filename;
                pdf.extension = extension.Substring(1);
                pdf.tamanio = file.FileSize;
                db.pdf.Add(pdf);
                db.SaveChanges();
                System.IO.File.WriteAllBytes(UploadFolderTiendaOnline + "\\descripcion_detalle_producto\\" + pdf.id + "-" + pdf.nombre, file.Contents);
                pdf pdf1 = db.pdf.Find(pdf.id);
                pdf.nombre = pdf.id + "-" + pdf.nombre;
                db.Entry(pdf).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
        }

        public ActionResult descargarLayOut(string nombre)
        {
            string filename = nombre;
            String filepath = Server.MapPath("~/UploadFolder/Layout_Excel/") + filename;

            byte[] filedata = System.IO.File.ReadAllBytes(filepath);
            string contentType = MimeMapping.GetMimeMapping(filepath);

            var cd = new System.Net.Mime.ContentDisposition
            {
                FileName = filename,
                Inline = true,
            };

            Response.AppendHeader("Content-Disposition", cd.ToString());
            return File(filedata, contentType);
        }

        public bool crearInventarioProductosNuevos()
        {
            string path = System.Web.HttpContext.Current.Server.MapPath("").Split('\\').ElementAt(System.Web.HttpContext.Current.Server.MapPath("").Split('\\').Length - 2);
            MySqlConnection dbConn = new MySqlConnection(Fn.ConnDB.getConnString(path));
            String query = "insert into inventario(producto_id,sucursal_id,cantidad)" +
                            "( " +
                            "SELECT p.id as _idProducto, s.id as _idSucursal, 0 " +
                            "FROM producto p, sucursal s " +
                            "WHERE NOT EXISTS(SELECT null " +
                            "FROM inventario i " +
                            "WHERE i.Producto_id = p.id and i.sucursal_id = s.id));";
            using (MySqlCommand com = new MySqlCommand(query, dbConn))
            {
                dbConn.Open();
                com.ExecuteNonQuery();
                dbConn.Close();
            }

            return true;
        }

        public ActionResult VerificarCodigoProducto(int id, String Codigo)
        {
            Boolean existe;
            if (id == 0)
                existe = db.producto.Where(x => x.Codigo.ToLower().Equals(Codigo.ToLower())).Any();
            else
                existe = db.producto.Where(x => x.id != id && x.Codigo.ToLower().Equals(Codigo.ToLower())).Any();
            if (existe)
            {
                var respuesta = new
                {
                    success = false
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var respuesta = new
                {
                    success = true
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
        }

    }
}
