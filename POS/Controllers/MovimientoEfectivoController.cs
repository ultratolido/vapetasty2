﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using POS.Models;
using POS.Fn;
using System.Globalization;

namespace POS.Controllers
{
    public class MovimientoEfectivoController : Controller
    {
        public moragas_pruebasEntities1 db = new moragas_pruebasEntities1();
        CultureInfo IdiomaFecha = new CultureInfo("es-MX");
        // GET: MovimientoEfectivo
        public ActionResult Index()
        {
            //estatus: true = activo, false = cancelado
            List<int> ListaSucursalesPermitidas = usuario_logeado.Sucursales_id().ToList();
            ViewBag.id_sucursal = new SelectList(db.sucursal.Where(x => ListaSucursalesPermitidas.Contains(x.id)).OrderByDescending(y => y.id), "id", "nombre_sucursal");
            ViewBag.tipo_de_movimiento = new SelectList(db.tipo_movimiento_efectivo, "id", "nombre");
            ViewBag.ListaMovimientosEfectivo = db.movimiento_efectivo.Where(x => ListaSucursalesPermitidas.Contains(x.id_sucursal)).ToList();
            ViewBag.HoyInicio = DateTime.Now.ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.HoyFin = DateTime.Now.ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.AyerInicio = DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.AyerFin = DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.AntierInicio = DateTime.Now.AddDays(-2).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.AntierFin = DateTime.Now.AddDays(-2).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.SemanaActualInicio = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.SemanaActualFin = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(6).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.SemanaPasadaInicio = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(-7).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.SemanaPasadaFin = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(-7).AddDays(6).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.SemanaAntepasadaInicio = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(-14).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.SemanaAntepasadaFin = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(-14).AddDays(6).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            DateTime hoy = Convert.ToDateTime(DateTime.Now.ToShortDateString());
            int numero_de_dia = hoy.Day;
            if (numero_de_dia > 15)
            //segunda quincena
            {
                ViewBag.QuincenaActualInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 15).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaActualFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.Month)).ToString("MM/dd/yyyy") + " 11:59:59 PM";
                ViewBag.QuincenaPasadaInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaPasadaFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddDays(15).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
                ViewBag.QuincenaAntepasadaInicio = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, 15).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaAntepasadaFin = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month)).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            }
            else //primera quincena
            {
                ViewBag.QuincenaActualInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaActualFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddDays(15).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
                ViewBag.QuincenaPasadaInicio = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, 15).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaPasadaFin = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month)).ToString("MM/dd/yyyy") + " 11:59:59 PM";
                ViewBag.QuincenaAntepasadaInicio = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaAntepasadaFin = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, 1).AddDays(15).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            }
            ViewBag.MesActualInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.MesActualFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(1).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.MesPasadoInicioFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.MesPasadoFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-1).AddMonths(1).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.MesAntepasadoInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-2).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.MesAntepasadoFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-2).AddMonths(1).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            return View();
        }

        public PartialViewResult _MovimientosEfectivo(int id_sucursal, int opcion_estatus, int opcion_tipo_movimiento, String periodo)
        {
            /* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
            /* Si opcion_estatus == 0 es todos, 1 == activos, 2 == cancelados.                                     */
            /* Si opcion_tipo_movimiento == 0 es todos, 1 == entrada de efectivo, 2 == salida de efectivo,         */
            /* 3 == fondo de caja.                                                                                 */
            /* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
            String[] fecha = Fn.Fechas.CalcularIncioFin(periodo);
            String inicio = fecha[0];
            String fin = fecha[1];
            DateTime _inicio = Convert.ToDateTime(inicio, IdiomaFecha);
            DateTime _fin = Convert.ToDateTime(fin, IdiomaFecha);
            List<movimiento_efectivo> lista_movimiento_efectivo;
            if (opcion_estatus == 1)
            {
                if (opcion_tipo_movimiento == 0)
                    lista_movimiento_efectivo = db.movimiento_efectivo.Where(x => x.id_sucursal == id_sucursal && x.estatus && x.fecha >= _inicio && x.fecha <= _fin).ToList();
                else
                    lista_movimiento_efectivo = db.movimiento_efectivo.Where(x => x.id_sucursal == id_sucursal && x.estatus && x.id_tipo_movimiento_efectivo == opcion_tipo_movimiento && x.fecha >= _inicio && x.fecha <= _fin).ToList();
            }
            else if (opcion_estatus == 2)
            {
                if (opcion_tipo_movimiento == 0)
                    lista_movimiento_efectivo = db.movimiento_efectivo.Where(x => x.id_sucursal == id_sucursal && !x.estatus && ((x.fecha >= _inicio && x.fecha <= _fin) || (x.fecha_cancelacion >= _inicio && x.fecha_cancelacion <= _fin))).ToList();
                else
                    lista_movimiento_efectivo = db.movimiento_efectivo.Where(x => x.id_sucursal == id_sucursal && !x.estatus && x.id_tipo_movimiento_efectivo == opcion_tipo_movimiento && ((x.fecha >= _inicio && x.fecha <= _fin) || (x.fecha_cancelacion >= _inicio && x.fecha_cancelacion <= _fin))).ToList();
            }
            else
            {
                if (opcion_tipo_movimiento == 0)
                    lista_movimiento_efectivo = db.movimiento_efectivo.Where(x => x.id_sucursal == id_sucursal && ((x.fecha >= _inicio && x.fecha <= _fin) || (x.fecha_cancelacion >= _inicio && x.fecha_cancelacion <= _fin))).ToList();
                else
                    lista_movimiento_efectivo = db.movimiento_efectivo.Where(x => x.id_sucursal == id_sucursal && x.id_tipo_movimiento_efectivo == opcion_tipo_movimiento && ((x.fecha >= _inicio && x.fecha <= _fin) || (x.fecha_cancelacion >= _inicio && x.fecha_cancelacion <= _fin))).ToList();
            }
            ViewBag.ListaMovimientosEfectivo = lista_movimiento_efectivo;
            return PartialView();
        }

        public PartialViewResult ModalAgregarMovimiento()
        {
            usuario_punto_venta usuario_logeado = Fn.usuario_logeado.traerUsuario();
            List<int> ListaSucursalesPermitidas = Fn.usuario_logeado.Sucursales_id();
            ViewBag.id_tipo_movimiento_efectivo = new SelectList(db.tipo_movimiento_efectivo, "id", "nombre");
            ViewBag.id_sucursal = new SelectList(db.sucursal.Where(x => ListaSucursalesPermitidas.Contains(x.id)).OrderByDescending(y => y.id), "id", "nombre_sucursal");
            ViewBag.id_usuario = usuario_logeado.id;
            return PartialView();
        }

        public JsonResult AgregarMovimientoEfectivo(movimiento_efectivo movimiento_efectivo)
        {
            try
            {
                usuario_punto_venta usuario_logeado = Fn.usuario_logeado.traerUsuario();
                movimiento_efectivo.id_usuario = usuario_logeado.id;
                movimiento_efectivo.fecha = DateTime.Now;
                movimiento_efectivo.estatus = true;
                db.movimiento_efectivo.Add(movimiento_efectivo);
                db.SaveChanges();
                var respuesta = new
                {
                    success = true
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                var respuesta = new
                {
                    success = false
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
                throw;
            }
        }

        public PartialViewResult ModalConfirmarCancelarMovimiento(int id)
        {
            movimiento_efectivo movimiento_efectivo = db.movimiento_efectivo.Find(id);
            return PartialView(movimiento_efectivo);
        }
        public JsonResult CancelarMovimientoEfectivo(int id, String motivo_cancelacion)
        {
            try
            {
                usuario_punto_venta usuario_logeado = Fn.usuario_logeado.traerUsuario();
                movimiento_efectivo movimiento_efectivo = db.movimiento_efectivo.Find(id);
                movimiento_efectivo.motivo_cancelacion = motivo_cancelacion;
                movimiento_efectivo.id_usuario_cancela = usuario_logeado.id;
                movimiento_efectivo.fecha_cancelacion = DateTime.Now;
                movimiento_efectivo.estatus = false;
                db.Entry(movimiento_efectivo).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                var respuesta = new
                {
                    success = true
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                var respuesta = new
                {
                    success = false
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
                throw;
            }
        }

        public PartialViewResult ModalGenerarFondoCajaAutomatico(int id_sucursal)
        {
            usuario_punto_venta usuario_logeado = Fn.usuario_logeado.traerUsuario();
            List<int> ListaSucursalesPermitidas = Fn.usuario_logeado.Sucursales_id();
            ViewBag.id_tipo_movimiento_efectivo = new SelectList(db.tipo_movimiento_efectivo, "id", "nombre", 3);
            ViewBag.id_sucursal = new SelectList(db.sucursal.Where(x => ListaSucursalesPermitidas.Contains(x.id)).OrderByDescending(y => y.id), "id", "nombre_sucursal", id_sucursal);
            ViewBag.id_usuario = usuario_logeado.id;
            List<object> resultados = calcular_efectivo_en_caja_ultimo_dia(id_sucursal);
            ViewBag.fecha_recuperacion_efectivo = Convert.ToDateTime(resultados.ElementAt(0), IdiomaFecha).ToShortDateString();
            ViewBag.efectivo_fondo_caja = resultados.ElementAt(1);
            return PartialView();
        }

        public List<object> calcular_efectivo_en_caja_ultimo_dia(int id_sucursal)
        {
            List<object> resultados = new List<object>();
            decimal efectivo_actual = 0;
            List<venta> lista_ventas = new List<venta>();
            List<abono> lista_abonos = new List<abono>();
            List<movimiento_efectivo> lista_movimientos_efectivo = new List<movimiento_efectivo>();
            DateTime fechaInicio = DateTime.Today.AddDays(-1);
            DateTime fechaFin = fechaInicio.AddHours(23).AddMinutes(59).AddSeconds(59);

            while (efectivo_actual == 0)
            {
                /* *** *** *** *** *** *** *** *** *** *** *** ***   Lista Ventas  *** *** *** *** *** *** *** *** *** *** *** *** *** */
                /* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
                lista_ventas = db.venta.Where(x => ((x.catalogo_status_id == 1 && x.Fecha_entrega >= fechaInicio && x.Fecha_entrega < fechaFin)
                                                           || (x.catalogo_status_id == 9 && x.fecha_liquidacion >= fechaInicio && x.fecha_liquidacion <= fechaFin)
                                                           || (x.catalogo_status_id == 10 && x.fecha_liquidacion >= fechaInicio && x.fecha_liquidacion <= fechaFin)
                                                           || (x.catalogo_status_id == 11 && x.fecha_liquidacion >= fechaInicio && x.fecha_liquidacion <= fechaFin))
                                                           && x.sucursal_id == id_sucursal).OrderBy(x => x.Folio).ToList();
                if (lista_ventas != null && lista_ventas.Count() > 0)
                {
                    foreach (venta venta in lista_ventas)
                    {
                        foreach (multiple_pago multiple_pago in venta.multiple_pago.ToList())
                        {
                            //Si no es pago a credito y corresponde al cruce
                            if (multiple_pago.id_tipo_pago_venta == 1)
                            {
                                efectivo_actual += Convert.ToDecimal(multiple_pago.cantidad);
                            }
                        }
                    }
                }

                /* *** *** *** *** *** *** *** *** *** *** *** ***   Lista Abonos  *** *** *** *** *** *** *** *** *** *** *** *** *** */
                /* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
                lista_abonos = db.abono.Where(x => (x.estatus == true && x.fecha.Value <= fechaFin && x.fecha.Value >= fechaInicio && x.Tipo_pago_abono_id == 1) && x.venta.sucursal_id == id_sucursal).OrderBy(x => x.id).ToList();

                foreach (abono abono in lista_abonos)
                {
                    efectivo_actual += Convert.ToDecimal(abono.importe);
                }

                /* *** *** *** *** *** *** *** *** *** *** *** Lista Movimientos Efectivo  *** *** *** *** *** *** *** *** *** *** *** */
                /* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
                lista_movimientos_efectivo = db.movimiento_efectivo.Where(x => (x.estatus == true && x.fecha.Value <= fechaFin && x.fecha.Value >= fechaInicio && (x.id_tipo_movimiento_efectivo == 1 || x.id_tipo_movimiento_efectivo == 3)) && x.id_sucursal == id_sucursal).OrderBy(x => x.id).ToList();
                foreach (movimiento_efectivo movimiento_efectivo in lista_movimientos_efectivo)
                {
                    efectivo_actual += movimiento_efectivo.monto;
                }

                lista_movimientos_efectivo = db.movimiento_efectivo.Where(x => (x.estatus == true && x.fecha.Value <= fechaFin && x.fecha.Value >= fechaInicio && x.id_tipo_movimiento_efectivo == 2) && x.id_sucursal == id_sucursal).OrderBy(x => x.id).ToList();
                foreach (movimiento_efectivo movimiento_efectivo in lista_movimientos_efectivo)
                {
                    efectivo_actual -= movimiento_efectivo.monto;
                }

                /* *** *** *** *** Por si el fondo de caja a agregar no es del dia anterio *** *** *** *** */
                if (efectivo_actual == 0)
                {
                    fechaInicio = fechaInicio.AddDays(-1);
                    fechaFin = fechaFin.AddDays(-1);
                }
                else
                {
                    resultados.Add(fechaInicio);
                }
            }
            resultados.Add(efectivo_actual);
            return resultados;
        }
    }
}