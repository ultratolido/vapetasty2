﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using POS.Models;
using SelectPdf;
using System.IO;
using POS.Fn;
using System.Globalization;
using System.Text;
using System.Security.Cryptography;
using MySql.Data.MySqlClient;
using System.Data;

namespace POS.Controllers
{
    public class FormatoPDFController : Controller
    {

        moragas_pruebasEntities1 db = new moragas_pruebasEntities1();
        CultureInfo IdiomaFecha = new CultureInfo("es-MX");

        public PartialViewResult Index(string html)
        {
            ViewBag.html = html;
            return PartialView();
        }

        //Descargar: arma el archivo y lo regresa
        public void traer_pdf_historial_crediticio_cliente(int id, bool solamente_pendientes)
        {
            Uri Request_Url = Request.Url;
            var current_action = RouteData.Values["action"].ToString();
            string URL = "";
            //factura activa
            URL = Request_Url.OriginalString.Replace(current_action, "PDF_historial_crediticio_cliente");
            URL += "&id=" + id + "&solamente_pendientes" + solamente_pendientes;
            PdfPageSize pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize), "A4", true);
            PdfPageOrientation pdfOrientation = (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation), "Portrait", true);
            // instantiate a html to pdf converter object
            HtmlToPdf _converter = new SelectPdf.HtmlToPdf();
            // set converter options
            _converter.Options.PdfPageSize = pageSize;
            _converter.Options.PdfPageOrientation = pdfOrientation;
            _converter.Options.WebPageWidth = 1280;
            _converter.Options.WebPageHeight = 0;
            // create a new pdf document converting an url
            SelectPdf.PdfDocument doc = _converter.ConvertUrl(URL);
            doc.Save(System.Web.HttpContext.Current.Response, false, db.cliente.Find(id).Razon_social.Trim() + DateTime.Now.ToString("dd-MM-yyyy_HH-mm") + ".pdf");
        }

        public PartialViewResult PDF_historial_crediticio_cliente(int id, bool solamente_pendientes)
        {
            cliente c = db.cliente.Find(id);
            List<venta> listaVentasCreditoCliente = new List<venta>();
            List<nota_credito> ListaNotasCredito = new List<nota_credito>();
            if (solamente_pendientes)
            {
                listaVentasCreditoCliente = db.venta.Where(x => x.Cliente_id == id && x.multiple_pago.Where(y => y.id_tipo_pago_venta == 4).Count() > 0 && x.aduedo.Value != 0 && (x.catalogo_status_id == 1 || x.catalogo_status_id == 9 || x.catalogo_status_id == 10 || x.catalogo_status_id == 11)).ToList();
                List<int> ListaIdsVentas = c.venta.Where(x => x.nota_credito.Where(y => y.id_estatus_nota_credito == 1).Any()).Select(x => x.id).ToList();
                ListaNotasCredito = db.nota_credito.Where(x => ListaIdsVentas.Contains(x.id_venta) && x.id_estatus_nota_credito == 1).ToList();
            }
            else
            {
                listaVentasCreditoCliente = db.venta.Where(x => x.Cliente_id == id && x.multiple_pago.Where(y => y.id_tipo_pago_venta == 4).Count() > 0 && (x.catalogo_status_id == 1 || x.catalogo_status_id == 9 || x.catalogo_status_id == 10 || x.catalogo_status_id == 11)).ToList();
                List<int> ListaIdsVentas = c.venta.Where(x => x.nota_credito.Any()).Select(x => x.id).ToList();
                ListaNotasCredito = db.nota_credito.Where(x => ListaIdsVentas.Contains(x.id_venta)).ToList();
            }

            ViewBag.nombre = c.Razon_social;
            ViewBag.ListaNotasCredito = ListaNotasCredito;
            return PartialView(listaVentasCreditoCliente);
        }

        //Descargar: arma el archivo y lo regresa
        public void traer_pdf_historial_crediticio(int id_sucursal, bool solamente_pendientes)
        {
            Uri Request_Url = Request.Url;
            var current_action = RouteData.Values["action"].ToString();
            string URL = "";
            //factura activa
            URL = Request_Url.OriginalString.Replace(current_action, "PDF_historial_creditos");
            URL += "&id_sucursal=" + id_sucursal + "&solamente_pendientes" + solamente_pendientes;
            PdfPageSize pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize), "A4", true);
            PdfPageOrientation pdfOrientation = (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation), "Portrait", true);
            // instantiate a html to pdf converter object
            HtmlToPdf _converter = new HtmlToPdf();
            // set converter options
            _converter.Options.PdfPageSize = pageSize;
            _converter.Options.PdfPageOrientation = pdfOrientation;
            _converter.Options.WebPageWidth = 1280;
            _converter.Options.WebPageHeight = 0;
            // create a new pdf document converting an url
            PdfDocument doc = _converter.ConvertUrl(URL);
            doc.Save(System.Web.HttpContext.Current.Response, false, db.sucursal.Find(id_sucursal).nombre_sucursal + DateTime.Now.ToString("dd-MM-yyyy_HH-mm") + ".pdf");
        }

        public PartialViewResult PDF_historial_creditos(int id_sucursal, bool solamente_pendientes)
        {
            List<venta> listaVentasCreditoCliente = new List<venta>();
            ViewBag.sucursal = db.sucursal.Find(id_sucursal).nombre_sucursal;
            if (solamente_pendientes)
            {
                listaVentasCreditoCliente = db.venta.Where(x => x.multiple_pago.Where(y => y.id_tipo_pago_venta == 4).Count() > 0 && x.aduedo.Value != 0 && x.sucursal_id == id_sucursal && (x.catalogo_status_id == 1 || x.catalogo_status_id == 9 || x.catalogo_status_id == 10 || x.catalogo_status_id == 11)).ToList();
            }
            else
            {
                listaVentasCreditoCliente = db.venta.Where(x => x.multiple_pago.Where(y => y.id_tipo_pago_venta == 4).Count() > 0 && x.sucursal_id == id_sucursal && (x.catalogo_status_id == 1 || x.catalogo_status_id == 9 || x.catalogo_status_id == 10 || x.catalogo_status_id == 11)).ToList();
            }
            return PartialView(listaVentasCreditoCliente);
        }

        //Descargar: arma el archivo y lo regresa
        public void traer_pdf_cotizacion(int id)
        {
            Uri Request_Url = Request.Url;
            var current_action = RouteData.Values["action"].ToString();
            string URL = "";
            //factura activa
            URL = Request_Url.OriginalString.Replace(current_action, "cotizacion");
            //URL += "?id=" + id;
            PdfPageSize pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize), "A4", true);
            PdfPageOrientation pdfOrientation = (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation), "Portrait", true);
            // instantiate a html to pdf converter object
            HtmlToPdf _converter = new HtmlToPdf();
            // set converter options
            _converter.Options.PdfPageSize = pageSize;
            _converter.Options.PdfPageOrientation = pdfOrientation;
            _converter.Options.WebPageWidth = 1280;
            _converter.Options.WebPageHeight = 0;
            // create a new pdf document converting an url
            PdfDocument doc = _converter.ConvertUrl(URL);
            doc.Save(System.Web.HttpContext.Current.Response, false, Fn.Numeros.serie_folio_by_id(id) + DateTime.Now.ToString("dd-MM-yyyy_HH-mm") + ".pdf");
        }

        public PartialViewResult cotizacion(int id)
        {
            venta c = db.venta.Find(id);
            String UploadFolderTiendaOnline = new DirectoryInfo(Server.MapPath("~/")).Parent.FullName + "\\Tienda Online\\UploadFolder";
            ViewBag.UploadFolderTiendaOnline = UploadFolderTiendaOnline;
            ViewBag.tipoCambiario = Math.Round(c.sucursal.precio_dolar.Value, 2);
            ViewBag.venta = c;
            return PartialView();
        }

        //Descargar: arma el archivo y lo regresa
        public void GenerarPDFVenta(int id)
        {
            Uri Request_Url = Request.Url;
            var current_action = RouteData.Values["action"].ToString();
            string URL = "";
            //factura activa
            URL = Request_Url.OriginalString.Replace(current_action, "venta");
            //URL += "?id=" + id;
            PdfPageSize pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize), "A4", true);
            PdfPageOrientation pdfOrientation = (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation), "Portrait", true);
            // instantiate a html to pdf converter object
            HtmlToPdf _converter = new HtmlToPdf();
            // set converter options
            _converter.Options.PdfPageSize = pageSize;
            _converter.Options.PdfPageOrientation = pdfOrientation;
            _converter.Options.WebPageWidth = 1280;
            _converter.Options.WebPageHeight = 0;
            // create a new pdf document converting an url
            PdfDocument doc = _converter.ConvertUrl(URL);
            doc.Save(System.Web.HttpContext.Current.Response, false, Fn.Numeros.serie_folio_by_id(id) + ".pdf");
        }

        public PartialViewResult venta(int id)
        {
            venta venta = db.venta.Find(id);

            List<descuento> ListaDescuentos;
            List<nota_credito> ListaNotasCredito;
            Decimal total_descuentos = 0;
            Decimal total_porcentaje_descuentos = 0;
            ListaNotasCredito = db.nota_credito.Where(x => x.id_venta_aplicada == id).ToList();
            ListaDescuentos = db.descuento.Where(x => x.id_venta == id).ToList();
            total_descuentos = (Decimal)ListaDescuentos.Select(x => x.monto).Sum();
            total_porcentaje_descuentos = (Decimal)ListaDescuentos.Select(x => x.porcentaje).Sum();

            ViewBag.ListaNotasCredito = ListaNotasCredito;
            ViewBag.ListaDescuentos = ListaDescuentos;
            ViewBag.total_descuentos = total_descuentos;
            ViewBag.total_porcentaje_descuentos = total_porcentaje_descuentos;

            String UploadFolderTiendaOnline = new DirectoryInfo(Server.MapPath("~/")).Parent.FullName + "\\Tienda Online\\UploadFolder";
            ViewBag.UploadFolderTiendaOnline = UploadFolderTiendaOnline;
            ViewBag.venta = venta;
            return PartialView();
        }

        public ActionResult EnviarCorreoReporteVentaConsignacion(int id_proveedor, String nombre_proveedor, String correo, int id_sucursal, String periodo, int id_estatus)
        {
            try
            {
                configuracion_general configuracion_general = Fn.Configuracion.TraerConfiguracionGeneral();
                sucursal sucursal = Configuracion.TraerConfiguracionSucursales().First();
                configuracion_sucursal_correo configuracion_sucursal_correo = sucursal.configuracion_sucursal_correo.First();
                Uri Request_Url = Request.Url;
                var current_action = RouteData.Values["action"].ToString();
                string URL = "";
                URL = Request_Url.OriginalString.Replace(current_action, "VistaParcialReporteConsignacion");
                //List<itemProductoCarrito> ListaCarrito = Fn.FnCarrito.TraerLista(System.Web.HttpContext.Current, id_tipo_precio);
                //List<itemProductoCarrito> ListaCarritoParams = new List<itemProductoCarrito>();
                //foreach (itemProductoCarrito lc in ListaCarrito)
                //{
                //    itemProductoCarrito nlc = new itemProductoCarrito();
                //    nlc.Id = lc.Id;
                //    nlc.Cantidad = lc.Cantidad;
                //    ListaCarritoParams.Add(nlc);
                //}
                URL += "?id_proveedor=" + id_proveedor;
                URL += "&id_sucursal=" + id_sucursal;
                URL += "&periodo=" + periodo;
                URL += "&id_estatus=" + id_estatus;

                PdfPageSize pageSize = (PdfPageSize)Enum.Parse(typeof(SelectPdf.PdfPageSize), "A4", true);
                PdfPageOrientation pdfOrientation = (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation), "Portrait", true);
                HtmlToPdf _converter = new HtmlToPdf();
                _converter.Options.PdfPageSize = pageSize;
                _converter.Options.PdfPageOrientation = pdfOrientation;
                _converter.Options.WebPageWidth = 1280;
                _converter.Options.WebPageHeight = 0;
                PdfDocument doc = _converter.ConvertUrl(URL);
                byte[] pdfBuffer;
                String rutaArchivo = Server.MapPath("~/UploadFolder/PDF_email_temp/ReporteConsignacion-") + DateTime.Now.ToString("ddMMyyyy_HHmm") + ".pdf";
                if (true)
                {
                    pdfBuffer = doc.Save();
                    System.IO.File.WriteAllBytes(rutaArchivo, pdfBuffer);
                }
                CDO.Message message = new CDO.Message();
                CDO.IConfiguration configuration = message.Configuration;
                ADODB.Fields fields = configuration.Fields;
                ADODB.Field field = fields["http://schemas.microsoft.com/cdo/configuration/smtpserver"];
                field.Value = configuracion_sucursal_correo.smtp;
                field = fields["http://schemas.microsoft.com/cdo/configuration/smtpserverport"];
                field.Value = configuracion_sucursal_correo.puerto;
                field = fields["http://schemas.microsoft.com/cdo/configuration/sendusing"];
                field.Value = CDO.CdoSendUsing.cdoSendUsingPort;
                field = fields["http://schemas.microsoft.com/cdo/configuration/smtpauthenticate"];
                field.Value = CDO.CdoProtocolsAuthentication.cdoBasic;
                field = fields["http://schemas.microsoft.com/cdo/configuration/sendusername"];
                field.Value = configuracion_sucursal_correo.correo;
                field = fields["http://schemas.microsoft.com/cdo/configuration/sendpassword"];
                field.Value = configuracion_sucursal_correo.contrasena;
                field = fields["http://schemas.microsoft.com/cdo/configuration/smtpusessl"];
                field.Value = configuracion_sucursal_correo.ssl;
                fields.Update();
                message.From = configuracion_sucursal_correo.correo;
                message.Subject = "Ventas A Consignacion - " + configuracion_general.marca;
                message.HTMLBody = "Buenas Tardes <br /><br />Se le ha enviado un reporte de sus productos.<br /><br />Gracias<br /><br /><a href='" + configuracion_general.pagina_web + "'>Visitar " + configuracion_general.pagina_web + "</a><br><br>Saludos!<br><br>" + configuracion_general.marca;
                message.AddAttachment(rutaArchivo);
                String[] correos = correo.Split(';');
                for (int x = 0; x < correos.Length; x++)
                {
                    message.To = correos[x];
                    message.Send();
                }
                System.IO.File.Delete(rutaArchivo);
                var respuesta = new
                {
                    success = true
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                var respuesta = new
                {
                    success = false,
                    proveedor = nombre_proveedor.ToUpper()

                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
        }

        public PartialViewResult VistaParcialReporteConsignacion(int id_proveedor, int id_sucursal, String periodo, int id_estatus)
        {
            String UploadFolderTiendaOnline = new DirectoryInfo(Server.MapPath("~/")).Parent.FullName + "\\Tienda Online\\UploadFolder";
            configuracion_general configuracion_general = Configuracion.TraerConfiguracionGeneral();
            String[] fecha = Fechas.CalcularIncioFin(periodo);
            String inicio = fecha[0];
            String fin = fecha[1];
            List<venta> ListaVentas = new List<venta>();
            List<conceptos_por_venta> ListaConceptos = new List<conceptos_por_venta>();
            proveedor proveedor = new proveedor();
            DateTime _inicio = Convert.ToDateTime(inicio, IdiomaFecha);
            DateTime _fin = Convert.ToDateTime(fin, IdiomaFecha);
            ListaVentas = db.venta.Where(x => ((x.catalogo_status_id == 1 && x.Fecha_entrega >= _inicio && x.Fecha_entrega <= _fin)
                                              || (x.catalogo_status_id == 9 && x.fecha_liquidacion >= _inicio && x.fecha_liquidacion <= _fin)
                                              || (x.catalogo_status_id == 10 && x.fecha_liquidacion >= _inicio && x.fecha_liquidacion <= _fin)
                                              || (x.catalogo_status_id == 11 && x.fecha_liquidacion >= _inicio && x.fecha_liquidacion <= _fin))).ToList();
            if (id_sucursal != 0)
            {
                ListaVentas = ListaVentas.Where(x => x.sucursal_id == id_sucursal).ToList();
            }
            List<int> ListaIdsVentas = ListaVentas.Select(y => y.id).ToList();
            ListaConceptos = db.conceptos_por_venta.Where(x => ListaIdsVentas.Contains(x.Venta_id)).ToList();
            ListaConceptos = ListaConceptos.Where(x => x.producto.consignacion == 1).ToList();
            if (id_proveedor != 0)
            {
                ListaConceptos = ListaConceptos.Where(x => x.producto.Proveedor_id == id_proveedor).ToList();
            }
            ListaConceptos = ListaConceptos.Where(x => x.pagada_consignacion != null).ToList();
            if (id_estatus == 1)
            {
                ListaConceptos = ListaConceptos.Where(x => x.pagada_consignacion == false).ToList();
            }
            else if (id_estatus == 2)
            {
                ListaConceptos = ListaConceptos.Where(x => x.pagada_consignacion == true).ToList();
            }
            proveedor = db.proveedor.Find(id_proveedor);
            ViewBag.ListaVentas = ListaVentas;
            ViewBag.ListaConceptos = ListaConceptos;
            ViewBag.proveedor = proveedor;
            ViewBag.FechaInicio = inicio;
            ViewBag.FechaFin = fin;
            ViewBag.UploadFolderTiendaOnline = UploadFolderTiendaOnline;
            return PartialView();
        }

        public ActionResult EnviarCorreoReporteInventarioConsignacion(int id_sucursal, int id_proveedor, int id_familia, bool con_inventario, String correo, String nombre_proveedor)
        {
            try
            {
                configuracion_general configuracion_general = Fn.Configuracion.TraerConfiguracionGeneral();
                sucursal sucursal = Configuracion.TraerConfiguracionSucursales().First();
                configuracion_sucursal_correo configuracion_sucursal_correo = sucursal.configuracion_sucursal_correo.First();
                Uri Request_Url = Request.Url;
                var current_action = RouteData.Values["action"].ToString();
                string URL = "";
                URL = Request_Url.OriginalString.Replace(current_action, "VistaParcialReporteInventarioConsignacion");
                URL += "?id_sucursal=" + id_sucursal;
                URL += "&id_proveedor=" + id_proveedor;
                URL += "&id_familia=" + id_familia;
                URL += "&con_inventario=" + con_inventario;
                PdfPageSize pageSize = (PdfPageSize)Enum.Parse(typeof(SelectPdf.PdfPageSize), "A4", true);
                PdfPageOrientation pdfOrientation = (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation), "Portrait", true);
                HtmlToPdf _converter = new HtmlToPdf();
                _converter.Options.PdfPageSize = pageSize;
                _converter.Options.PdfPageOrientation = pdfOrientation;
                _converter.Options.WebPageWidth = 1280;
                _converter.Options.WebPageHeight = 0;
                PdfDocument doc = _converter.ConvertUrl(URL);
                byte[] pdfBuffer;
                String rutaArchivo = Server.MapPath("~/UploadFolder/PDF_email_temp/ReporteConsignacion-") + DateTime.Now.ToString("ddMMyyyy_HHmm") + ".pdf";
                if (true)
                {
                    pdfBuffer = doc.Save();
                    System.IO.File.WriteAllBytes(rutaArchivo, pdfBuffer);
                }
                CDO.Message message = new CDO.Message();
                CDO.IConfiguration configuration = message.Configuration;
                ADODB.Fields fields = configuration.Fields;
                ADODB.Field field = fields["http://schemas.microsoft.com/cdo/configuration/smtpserver"];
                field.Value = configuracion_sucursal_correo.smtp;
                field = fields["http://schemas.microsoft.com/cdo/configuration/smtpserverport"];
                field.Value = configuracion_sucursal_correo.puerto;
                field = fields["http://schemas.microsoft.com/cdo/configuration/sendusing"];
                field.Value = CDO.CdoSendUsing.cdoSendUsingPort;
                field = fields["http://schemas.microsoft.com/cdo/configuration/smtpauthenticate"];
                field.Value = CDO.CdoProtocolsAuthentication.cdoBasic;
                field = fields["http://schemas.microsoft.com/cdo/configuration/sendusername"];
                field.Value = configuracion_sucursal_correo.correo;
                field = fields["http://schemas.microsoft.com/cdo/configuration/sendpassword"];
                field.Value = configuracion_sucursal_correo.contrasena;
                field = fields["http://schemas.microsoft.com/cdo/configuration/smtpusessl"];
                field.Value = configuracion_sucursal_correo.ssl;
                fields.Update();
                message.From = configuracion_sucursal_correo.correo;
                message.Subject = "Ventas A Consignacion - " + configuracion_general.marca;
                message.HTMLBody = "Buenas Tardes <br /><br />Se le ha enviado un reporte de sus productos.<br /><br />Gracias<br /><br /><a href='" + configuracion_general.pagina_web + "'>Visitar " + configuracion_general.pagina_web + "</a><br><br>Saludos!<br><br>" + configuracion_general.marca;
                message.AddAttachment(rutaArchivo);
                String[] correos = correo.Split(';');
                for (int x = 0; x < correos.Length; x++)
                {
                    message.To = correos[x];
                    message.Send();
                }
                System.IO.File.Delete(rutaArchivo);
                var respuesta = new
                {
                    success = true
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                var respuesta = new
                {
                    success = false,
                    proveedor = nombre_proveedor.ToUpper()

                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
        }

        public PartialViewResult VistaParcialReporteInventarioConsignacion(int id_sucursal, int id_proveedor, bool con_inventario)
        {
            String UploadFolderTiendaOnline = new DirectoryInfo(Server.MapPath("~/")).Parent.FullName + "\\Tienda Online\\UploadFolder";
            configuracion_general configuracion_general = Configuracion.TraerConfiguracionGeneral();
            List<inventario> ListaInventario = new List<inventario>();
            if (con_inventario)
                ListaInventario = db.inventario.Where(x => x.cantidad > 0).ToList();
            else
                ListaInventario = db.inventario.ToList();
            if (id_sucursal != 0)
                ListaInventario = ListaInventario.Where(x => x.sucursal_id == id_sucursal).ToList();
            List<producto> ListaProductos = db.producto.ToList();
            if (id_proveedor != 0)
            {
                ListaProductos = ListaProductos.Where(x => x.Proveedor_id == id_proveedor).ToList();
            }
            List<int> ListaIdsProductos = ListaProductos.Select(y => y.id).ToList();
            ListaInventario = ListaInventario.Where(x => ListaIdsProductos.Contains(x.Producto_id)).ToList();
            proveedor proveedor = db.proveedor.Find(id_proveedor);
            ViewBag.ListaInventario = ListaInventario;
            ViewBag.proveedor = proveedor;
            ViewBag.id_sucursal = id_sucursal;
            ViewBag.UploadFolderTiendaOnline = UploadFolderTiendaOnline;
            return PartialView();
        }

        /* *** *** *** *** *** *** *** *** *** *** *** *** Reporte/Historial  *** *** *** *** *** *** *** *** *** ***  */
        /* *** *** *** *** --> *** *** *** *** *** *** *** *** --> *** *** *** *** *** *** *** *** --> *** *** *** *** */
        public PartialViewResult PDFListaReporteHistorial(int idSucursal, String periodo)
        {
            String[] fecha = Fn.Fechas.CalcularIncioFin(periodo);
            String inicio = fecha[0];
            String fin = fecha[1];
            List<venta> ListaVentas = new List<venta>();
            DateTime _inicio = Convert.ToDateTime(inicio, IdiomaFecha);
            DateTime _fin = Convert.ToDateTime(fin, IdiomaFecha);
            ListaVentas = db.venta.Where(x => ((x.catalogo_status_id == 1 && x.Fecha_entrega >= _inicio && x.Fecha_entrega <= _fin)
                                              || (x.catalogo_status_id == 9 && x.fecha_liquidacion >= _inicio && x.fecha_liquidacion <= _fin)
                                              || (x.catalogo_status_id == 10 && x.fecha_liquidacion >= _inicio && x.fecha_liquidacion <= _fin)
                                              || (x.catalogo_status_id == 11 && x.fecha_liquidacion >= _inicio && x.fecha_liquidacion <= _fin)
                                              || (x.catalogo_status_id == 2 && x.fecha_cancelacion >= _inicio && x.fecha_cancelacion <= _fin)
                                              || (x.catalogo_status_id == 12 && x.fecha_cancelacion >= _inicio && x.fecha_cancelacion <= _fin && x.fecha_liquidacion != null))
                                              && x.sucursal_id == idSucursal).OrderBy(x => x.Folio).ToList();
            Decimal total_facturado = ListaVentas.Select(x => x.total.Value).Sum();
            Decimal total_cancelado = ListaVentas.Where(x => x.catalogo_status_id == 2 || x.catalogo_status_id == 12).Select(x => x.total.Value).Sum();
            Decimal total = total_facturado - total_cancelado;
            List<pdf_venta> ListaPDFs = db.pdf_venta.ToList();
            ViewBag.ListaPDF = ListaPDFs;
            ViewBag.total_facturado = total_facturado;
            ViewBag.total_cancelado = total_cancelado;
            ViewBag.total = total;
            ViewBag.ListaVentas = ListaVentas;
            ViewBag.sucursal = db.sucursal.Find(idSucursal).nombre_sucursal.ToUpper();
            ViewBag.periodo = periodo;
            return PartialView();
        }

        //Descargar: arma el archivo y lo regresa
        public void TraerPDFListaReporteHistorial(int idSucursal, String periodo)
        {
            Uri Request_Url = Request.Url;
            string URL = Request_Url.OriginalString;
            URL = URL.Replace(Request_Url.PathAndQuery, Url.Action("PDFListaReporteHistorial", "FormatoPDF", new { idSucursal = idSucursal, periodo = periodo }));
            PdfPageSize pageSize;
            pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize), "A4", true);
            PdfPageOrientation pdfOrientation = (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation), "Portrait", true);
            // instantiate a html to pdf converter object
            HtmlToPdf _converter = new HtmlToPdf();
            // set converter options
            _converter.Options.PdfPageSize = pageSize;
            _converter.Options.PdfPageOrientation = pdfOrientation;
            _converter.Options.WebPageWidth = 1280;
            _converter.Options.WebPageHeight = 0;
            _converter.Options.MarginTop = 20;
            _converter.Options.MarginRight = 10;
            _converter.Options.MarginBottom = 20;
            _converter.Options.MarginLeft = 10;
            // create a new pdf document converting an url
            PdfDocument doc = _converter.ConvertUrl(URL);
            doc.Save(System.Web.HttpContext.Current.Response, false, "lista_reporte_historial.pdf");
        }
        /* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
        /* *** *** *** *** <-- *** *** *** *** *** *** *** *** <-- *** *** *** *** *** *** *** *** <-- *** *** *** *** */


        /* *** *** *** *** *** *** *** *** *** *** *** *** Reporte/VentaTipoPrecio  *** *** *** *** *** *** *** *** *** ***  */
        /* *** *** *** *** --> *** *** *** *** *** *** *** *** --> *** *** *** *** *** *** *** *** --> *** *** *** *** */
        public PartialViewResult PDFListaVentasTipoPrecio(int idSucursal, String periodo)
        {
            String[] fecha = Fechas.CalcularIncioFin(periodo);
            String inicio = fecha[0];
            String fin = fecha[1];
            DateTime _inicio = Convert.ToDateTime(inicio, IdiomaFecha);
            DateTime _fin = Convert.ToDateTime(fin, IdiomaFecha);
            List<venta> ListaVentas = new List<venta>();
            List<conceptos_por_venta> ListaConceptos = new List<conceptos_por_venta>();
            List<producto> ListaProductos = new List<producto>();
            ListaVentas = db.venta.Where(x => ((x.catalogo_status_id == 1 && x.Fecha_entrega >= _inicio && x.Fecha_entrega <= _fin)
                                              || (x.catalogo_status_id == 9 && x.fecha_liquidacion >= _inicio && x.fecha_liquidacion <= _fin)
                                              || (x.catalogo_status_id == 10 && x.fecha_liquidacion >= _inicio && x.fecha_liquidacion <= _fin)
                                              || (x.catalogo_status_id == 11 && x.fecha_liquidacion >= _inicio && x.fecha_liquidacion <= _fin))
                                              && x.sucursal_id == idSucursal).OrderBy(x => x.Folio).ToList();
            List<int> ListaIdsVentas = ListaVentas.Select(x => x.id).ToList();
            ListaConceptos = db.conceptos_por_venta.Where(x => ListaIdsVentas.Contains(x.Venta_id)).ToList();
            ListaProductos = ListaConceptos.Select(x => x.producto).Distinct().ToList();
            ViewBag.ListaVentas = ListaVentas;
            ViewBag.ListaConceptos = ListaConceptos;
            ViewBag.ListaProductos = ListaProductos;
            ViewBag.sucursal = db.sucursal.Find(idSucursal).nombre_sucursal.ToUpper();
            ViewBag.periodo = periodo;
            return PartialView();
        }

        //Descargar: arma el archivo y lo regresa
        public void TraerPDFListaVentasTipoPrecio(int idSucursal, String periodo)
        {
            Uri Request_Url = Request.Url;
            string URL = Request_Url.OriginalString;
            URL = URL.Replace(Request_Url.PathAndQuery, Url.Action("PDFListaVentasTipoPrecio", "FormatoPDF", new { idSucursal = idSucursal, periodo = periodo }));
            PdfPageSize pageSize;
            pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize), "A4", true);
            PdfPageOrientation pdfOrientation = (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation), "Portrait", true);
            // instantiate a html to pdf converter object
            HtmlToPdf _converter = new HtmlToPdf();
            // set converter options
            _converter.Options.PdfPageSize = pageSize;
            _converter.Options.PdfPageOrientation = pdfOrientation;
            _converter.Options.WebPageWidth = 1280;
            _converter.Options.WebPageHeight = 0;
            _converter.Options.MarginTop = 20;
            _converter.Options.MarginRight = 10;
            _converter.Options.MarginBottom = 20;
            _converter.Options.MarginLeft = 10;
            // create a new pdf document converting an url
            PdfDocument doc = _converter.ConvertUrl(URL);
            doc.Save(System.Web.HttpContext.Current.Response, false, "lista_reporte_ventas_tipo_precio.pdf");
        }
        /* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
        /* *** *** *** *** <-- *** *** *** *** *** *** *** *** <-- *** *** *** *** *** *** *** *** <-- *** *** *** *** */


        /* *** *** *** *** *** *** *** *** *** *** *** *** Reporte/VentasPorProducto  *** *** *** *** *** *** *** *** *** ***  */
        /* *** *** *** *** --> *** *** *** *** *** *** *** *** --> *** *** *** *** *** *** *** *** --> *** *** *** *** */
        public PartialViewResult PDFListaVentasPorProducto(int idSucursal, String periodo)
        {
            String[] fecha = Fechas.CalcularIncioFin(periodo);
            String inicio = fecha[0];
            String fin = fecha[1];
            List<venta> ventas = new List<venta>();
            DateTime _inicio = Convert.ToDateTime(inicio, IdiomaFecha);
            DateTime _fin = Convert.ToDateTime(fin, IdiomaFecha);
            ventas = db.venta.Where(x => ((x.catalogo_status_id == 1 && x.Fecha_entrega >= _inicio && x.Fecha_entrega <= _fin)
                                              || (x.catalogo_status_id == 9 && x.fecha_liquidacion >= _inicio && x.fecha_liquidacion <= _fin)
                                              || (x.catalogo_status_id == 10 && x.fecha_liquidacion >= _inicio && x.fecha_liquidacion <= _fin)
                                              || (x.catalogo_status_id == 11 && x.fecha_liquidacion >= _inicio && x.fecha_liquidacion <= _fin))
                                              && x.sucursal_id == idSucursal).OrderBy(x => x.Folio).ToList();
            List<int> listaIDventa = ventas.Select(x => x.id).ToList();
            List<conceptos_por_venta> lista_conceptos = db.conceptos_por_venta.Where(x => listaIDventa.Contains(x.Venta_id)).ToList();
            List<producto> productos = lista_conceptos.Select(x => x.producto).Distinct().ToList();
            List<conceptos> ListaConceptos = new List<conceptos>();
            foreach (producto p in productos)
            {
                //tipos de precio por producto
                foreach (tipocliente tprecio in lista_conceptos.Where(x => x.Producto_id == p.id).Select(x => x.tipocliente).Distinct().ToList())
                {
                    //agrupar totales
                    List<conceptos_por_venta> temp_lista_conceptos = lista_conceptos.Where(x => x.Producto_id == p.id && x.TipoCliente_id == tprecio.id).ToList();
                    conceptos c = new conceptos();
                    c.cantidad = temp_lista_conceptos.Select(x => x.cantidad).Sum();
                    c.codigo = p.Codigo;
                    c.descripcion = p.Descripcion;
                    c.precio_unitario_mas_iva = temp_lista_conceptos.First().precio_unitario_mas_iva.Value;
                    c.importe_mas_iva = temp_lista_conceptos.Select(x => x.importe_mas_iva.Value).Sum();
                    ListaConceptos.Add(c);
                }
            }
            ViewBag.ListaConceptos = ListaConceptos;
            ViewBag.sucursal = db.sucursal.Find(idSucursal).nombre_sucursal.ToUpper();
            ViewBag.periodo = periodo;
            return PartialView();
        }

        //Descargar: arma el archivo y lo regresa
        public void TraerPDFListaVentasPorProducto(int idSucursal, String periodo)
        {
            Uri Request_Url = Request.Url;
            string URL = Request_Url.OriginalString;
            URL = URL.Replace(Request_Url.PathAndQuery, Url.Action("PDFListaVentasPorProducto", "FormatoPDF", new { idSucursal = idSucursal, periodo = periodo }));
            PdfPageSize pageSize;
            pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize), "A4", true);
            PdfPageOrientation pdfOrientation = (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation), "Portrait", true);
            // instantiate a html to pdf converter object
            HtmlToPdf _converter = new HtmlToPdf();
            // set converter options
            _converter.Options.PdfPageSize = pageSize;
            _converter.Options.PdfPageOrientation = pdfOrientation;
            _converter.Options.WebPageWidth = 1280;
            _converter.Options.WebPageHeight = 0;
            _converter.Options.MarginTop = 20;
            _converter.Options.MarginRight = 10;
            _converter.Options.MarginBottom = 20;
            _converter.Options.MarginLeft = 10;
            // create a new pdf document converting an url
            PdfDocument doc = _converter.ConvertUrl(URL);
            doc.Save(System.Web.HttpContext.Current.Response, false, "lista_reporte_ventas_por_producto.pdf");
        }
        /* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
        /* *** *** *** *** <-- *** *** *** *** *** *** *** *** <-- *** *** *** *** *** *** *** *** <-- *** *** *** *** */


        /* *** *** *** *** *** *** *** *** *** *** *** *** Producto/Index  *** *** *** *** *** *** *** *** *** ***  */
        /* *** *** *** *** --> *** *** *** *** *** *** *** *** --> *** *** *** *** *** *** *** *** --> *** *** *** *** */
        public PartialViewResult PDFListaProductos(int ordenamiento, int productos_ocultos, String filtro)
        {
            if (productos_ocultos == 1)
                productos_ocultos = 0;
            else
                productos_ocultos = 1;
            String query = "SELECT producto.id AS id_producto, "
                            + "(SELECT nombre FROM familia WHERE familia.id = subfamilia.Familia_id) AS nombre_familia, "
                            + "subfamilia.nombre AS nombre_subfamilia, "
                            + "producto.Codigo AS codigo_producto, "
                            + "producto.Descripcion AS descripcion_producto, "
                            + "producto.descripcion_detallada AS descripcion_detallada, "
                            + "proveedor.nombre_proveedor as proveedor_producto, "
                            + "marca.nombre as marca_producto, "
                            + "producto.precio_dolar as precio_dolar_producto, "
                            + "producto.publicar as publicado_producto, "
                            + "producto.carrusel as destacado_producto, "
                            + "producto.visible as producto_visible, "
                            + "producto.consignacion as producto_consignacion, "
                            + "(SELECT COUNT(*) FROM imagen WHERE imagen.id_producto = producto.id) as imagenes, "
                            + "(SELECT COUNT(*) FROM pdf WHERE pdf.id_producto = producto.id) as pdfs, "
                            + "producto.costo as costo, "
                            + "producto.medida as medida, "
                            + "producto.modelo as modelo "
                            + "FROM producto producto, "
                            + "subfamilia, "
                            + "proveedor, "
                            + "marca "
                            + "WHERE "
                            + "producto.visible = " + productos_ocultos + " and "
                            + "producto.SubFamilia_id = subfamilia.id and "
                            + "producto.Proveedor_id = proveedor.id and "
                            + "producto.id_marca = marca.id "
                            + "AND (producto.codigo like '%" + filtro + "%' OR producto.descripcion like '%" + filtro + "%' OR proveedor.nombre_proveedor like '%" + filtro + "%' OR subfamilia.nombre like '%" + filtro + "%' OR (SELECT nombre FROM familia WHERE familia.id = subfamilia.Familia_id) like '%" + filtro + "%')";
            if (ordenamiento == 1)
                query += " ORDER BY producto.Codigo ASC;";
            else if (ordenamiento == 2)
                query += " ORDER BY producto.Descripcion ASC;";
            else if (ordenamiento == 3)
                query += " (SELECT nombre FROM familia WHERE familia.id = subfamilia.id_familia) ASC;";
            else if (ordenamiento == 4)
                query += " ORDER BY subfamilia.nombre ASC;";
            else if (ordenamiento == 5)
                query += " ORDER BY marca.nombre ASC;";
            else if (ordenamiento == 6)
                query += " ORDER BY producto.precio_dolar DESC;";
            else if (ordenamiento == 7)
                query += " ORDER BY producto.publicar DESC;";
            else if (ordenamiento == 8)
                query += " ORDER BY producto.carrusel DESC;";
            else if (ordenamiento == 9)
                query += " ORDER BY (SELECT COUNT(*) FROM imagen WHERE imagen.id_producto = producto.id) DESC;";
            else if (ordenamiento == 10)
                query += " ORDER BY (SELECT COUNT(*) FROM pdf WHERE pdf.id_producto = producto.id) DESC;";
            DataSet ds = Fn.ConnDB.GetDataSet(query, Server.MapPath("").Split('\\').ElementAt(Server.MapPath("").Split('\\').Length - 2));
            DataTable dt = ds.Tables[0];
            List<ConsultaProducto> ListaProductos = (from DataRow row in dt.Rows
                                                     select new ConsultaProducto
                                                     {
                                                         id = Convert.ToInt32(row["id_producto"]),
                                                         proveedor = row["proveedor_producto"].ToString(),
                                                         marca = row["marca_producto"].ToString(),
                                                         familia = row["nombre_familia"].ToString(),
                                                         subfamilia = row["nombre_subfamilia"].ToString(),
                                                         codigo = row["codigo_producto"].ToString(),
                                                         descripcion = row["descripcion_producto"].ToString(),
                                                         descripcion_detallada = row["descripcion_detallada"].ToString(),
                                                         publicado = Convert.ToInt32(row["publicado_producto"]),
                                                         carrusel = Convert.ToInt32(row["destacado_producto"]),
                                                         visible = Convert.ToInt32(row["producto_visible"]),
                                                         consignacion = Convert.ToInt32(row["producto_consignacion"]),
                                                         precio_dolar = Convert.ToInt32(row["precio_dolar_producto"]),
                                                         imagenes = Convert.ToInt32(row["imagenes"]),
                                                         pdfs = Convert.ToInt32(row["pdfs"]),
                                                         costo = row["costo"] != null ? row["costo"].ToString() : "",
                                                         medida = row["medida"] != null ? row["medida"].ToString() : "",
                                                         modelo = row["modelo"] != null ? row["modelo"].ToString() : ""
                                                     }).ToList();


            ViewBag.ListaProductos = ListaProductos;
            return PartialView();
        }

        //Descargar: arma el archivo y lo regresa
        public void TraerPDFListaProductos(int ordenamiento, int productos_ocultos, String filtro)
        {
            Uri Request_Url = Request.Url;
            string URL = Request_Url.OriginalString;
            URL = URL.Replace(Request_Url.PathAndQuery, Url.Action("PDFListaProductos", "FormatoPDF", new { ordenamiento = ordenamiento, productos_ocultos = productos_ocultos, filtro = filtro }));
            PdfPageSize pageSize;
            pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize), "A4", true);
            PdfPageOrientation pdfOrientation = (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation), "Portrait", true);
            // instantiate a html to pdf converter object
            HtmlToPdf _converter = new HtmlToPdf();
            // set converter options
            _converter.Options.PdfPageSize = pageSize;
            _converter.Options.PdfPageOrientation = pdfOrientation;
            _converter.Options.WebPageWidth = 1280;
            _converter.Options.WebPageHeight = 0;
            _converter.Options.MarginTop = 20;
            _converter.Options.MarginRight = 10;
            _converter.Options.MarginBottom = 20;
            _converter.Options.MarginLeft = 10;
            // create a new pdf document converting an url
            PdfDocument doc = _converter.ConvertUrl(URL);
            doc.Save(System.Web.HttpContext.Current.Response, false, "lista_productos.pdf");
        }
        /* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
        /* *** *** *** *** <-- *** *** *** *** *** *** *** *** <-- *** *** *** *** *** *** *** *** <-- *** *** *** *** */


        /* *** *** *** *** *** *** *** *** *** *** *** *** Proveedor/Index  *** *** *** *** *** *** *** *** *** ***  */
        /* *** *** *** *** --> *** *** *** *** *** *** *** *** --> *** *** *** *** *** *** *** *** --> *** *** *** *** */
        public PartialViewResult PDFListaProveedores()
        {
            return PartialView(db.proveedor);
        }

        //Descargar: arma el archivo y lo regresa
        public void TraerPDFListaProveedores()
        {
            Uri Request_Url = Request.Url;
            string URL = Request_Url.OriginalString;
            URL = URL.Replace(Request_Url.PathAndQuery, Url.Action("PDFListaProveedores", "FormatoPDF"));
            PdfPageSize pageSize;
            pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize), "A4", true);
            PdfPageOrientation pdfOrientation = (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation), "Portrait", true);
            // instantiate a html to pdf converter object
            HtmlToPdf _converter = new HtmlToPdf();
            // set converter options
            _converter.Options.PdfPageSize = pageSize;
            _converter.Options.PdfPageOrientation = pdfOrientation;
            _converter.Options.WebPageWidth = 1280;
            _converter.Options.WebPageHeight = 0;
            _converter.Options.MarginTop = 20;
            _converter.Options.MarginRight = 10;
            _converter.Options.MarginBottom = 20;
            _converter.Options.MarginLeft = 10;
            // create a new pdf document converting an url
            PdfDocument doc = _converter.ConvertUrl(URL);
            doc.Save(System.Web.HttpContext.Current.Response, false, "lista_proveedores.pdf");
        }
        /* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
        /* *** *** *** *** <-- *** *** *** *** *** *** *** *** <-- *** *** *** *** *** *** *** *** <-- *** *** *** *** */


        /* *** *** *** *** *** *** *** *** *** *** *** *** Marcas/Index  *** *** *** *** *** *** *** *** *** ***  */
        /* *** *** *** *** --> *** *** *** *** *** *** *** *** --> *** *** *** *** *** *** *** *** --> *** *** *** *** */
        public PartialViewResult PDFListaMarcas()
        {
            ViewBag.marcas = db.marca.ToList();
            return PartialView();
        }

        //Descargar: arma el archivo y lo regresa
        public void TraerPDFListaMarcas()
        {
            Uri Request_Url = Request.Url;
            string URL = Request_Url.OriginalString;
            URL = URL.Replace(Request_Url.PathAndQuery, Url.Action("PDFListaMarcas", "FormatoPDF"));
            PdfPageSize pageSize;
            pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize), "A4", true);
            PdfPageOrientation pdfOrientation = (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation), "Portrait", true);
            // instantiate a html to pdf converter object
            HtmlToPdf _converter = new HtmlToPdf();
            // set converter options
            _converter.Options.PdfPageSize = pageSize;
            _converter.Options.PdfPageOrientation = pdfOrientation;
            _converter.Options.WebPageWidth = 1280;
            _converter.Options.WebPageHeight = 0;
            _converter.Options.MarginTop = 20;
            _converter.Options.MarginRight = 10;
            _converter.Options.MarginBottom = 20;
            _converter.Options.MarginLeft = 10;
            // create a new pdf document converting an url
            PdfDocument doc = _converter.ConvertUrl(URL);
            doc.Save(System.Web.HttpContext.Current.Response, false, "lista_marcas.pdf");
        }
        /* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
        /* *** *** *** *** <-- *** *** *** *** *** *** *** *** <-- *** *** *** *** *** *** *** *** <-- *** *** *** *** */


        /* *** *** *** *** *** *** *** *** *** *** *** *** Familias/Index  *** *** *** *** *** *** *** *** *** ***  */
        /* *** *** *** *** --> *** *** *** *** *** *** *** *** --> *** *** *** *** *** *** *** *** --> *** *** *** *** */
        public PartialViewResult PDFListaFamilias()
        {
            ViewBag.familias = db.familia.ToList();
            return PartialView();
        }

        //Descargar: arma el archivo y lo regresa
        public void TraerPDFListaFamilias()
        {
            Uri Request_Url = Request.Url;
            string URL = Request_Url.OriginalString;
            URL = URL.Replace(Request_Url.PathAndQuery, Url.Action("PDFListaFamilias", "FormatoPDF"));
            PdfPageSize pageSize;
            pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize), "A4", true);
            PdfPageOrientation pdfOrientation = (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation), "Portrait", true);
            // instantiate a html to pdf converter object
            HtmlToPdf _converter = new HtmlToPdf();
            // set converter options
            _converter.Options.PdfPageSize = pageSize;
            _converter.Options.PdfPageOrientation = pdfOrientation;
            _converter.Options.WebPageWidth = 1280;
            _converter.Options.WebPageHeight = 0;
            _converter.Options.MarginTop = 20;
            _converter.Options.MarginRight = 10;
            _converter.Options.MarginBottom = 20;
            _converter.Options.MarginLeft = 10;
            // create a new pdf document converting an url
            PdfDocument doc = _converter.ConvertUrl(URL);
            doc.Save(System.Web.HttpContext.Current.Response, false, "lista_familias.pdf");
        }
        /* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
        /* *** *** *** *** <-- *** *** *** *** *** *** *** *** <-- *** *** *** *** *** *** *** *** <-- *** *** *** *** */


        /* *** *** *** *** *** *** *** *** *** *** *** *** Subfamilias/Index  *** *** *** *** *** *** *** *** *** ***  */
        /* *** *** *** *** --> *** *** *** *** *** *** *** *** --> *** *** *** *** *** *** *** *** --> *** *** *** *** */
        public PartialViewResult PDFListaSubfamilias()
        {
            ViewBag.subfamilias = db.subfamilia.ToList();
            return PartialView();
        }

        //Descargar: arma el archivo y lo regresa
        public void TraerPDFListaSubfamilias()
        {
            Uri Request_Url = Request.Url;
            string URL = Request_Url.OriginalString;
            URL = URL.Replace(Request_Url.PathAndQuery, Url.Action("PDFListaSubfamilias", "FormatoPDF"));
            PdfPageSize pageSize;
            pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize), "A4", true);
            PdfPageOrientation pdfOrientation = (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation), "Portrait", true);
            // instantiate a html to pdf converter object
            HtmlToPdf _converter = new HtmlToPdf();
            // set converter options
            _converter.Options.PdfPageSize = pageSize;
            _converter.Options.PdfPageOrientation = pdfOrientation;
            _converter.Options.WebPageWidth = 1280;
            _converter.Options.WebPageHeight = 0;
            _converter.Options.MarginTop = 20;
            _converter.Options.MarginRight = 10;
            _converter.Options.MarginBottom = 20;
            _converter.Options.MarginLeft = 10;
            // create a new pdf document converting an url
            PdfDocument doc = _converter.ConvertUrl(URL);
            doc.Save(System.Web.HttpContext.Current.Response, false, "lista_subfamilias.pdf");
        }
        /* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
        /* *** *** *** *** <-- *** *** *** *** *** *** *** *** <-- *** *** *** *** *** *** *** *** <-- *** *** *** *** */


        /* *** *** *** *** *** *** *** *** *** *** *** *** Inventario/Index  *** *** *** *** *** *** *** *** *** ***  */
        /* *** *** *** *** --> *** *** *** *** *** *** *** *** --> *** *** *** *** *** *** *** *** --> *** *** *** *** */
        public PartialViewResult PDFListaInventario(int id_sucursal, bool con_inventario)
        {
            List<inventario> ListaInventario = new List<inventario>();
            if (con_inventario)
                ListaInventario = db.inventario.Where(x => x.sucursal_id == id_sucursal && x.cantidad > 0).ToList();
            else
                ListaInventario = db.inventario.Where(x => x.sucursal_id == id_sucursal).ToList();
            ViewBag.ListaInventario = ListaInventario;
            ViewBag.sucursal = db.sucursal.Find(id_sucursal).nombre_sucursal.ToUpper();
            return PartialView();
        }

        //Descargar: arma el archivo y lo regresa
        public void TraerPDFListaInventario(int id_sucursal, bool con_inventario)
        {
            Uri Request_Url = Request.Url;
            string URL = Request_Url.OriginalString;
            URL = URL.Replace(Request_Url.PathAndQuery, Url.Action("PDFListaInventario", "FormatoPDF", new { id_sucursal = id_sucursal, con_inventario = con_inventario }));
            PdfPageSize pageSize;
            pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize), "A4", true);
            PdfPageOrientation pdfOrientation = (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation), "Portrait", true);
            // instantiate a html to pdf converter object
            HtmlToPdf _converter = new HtmlToPdf();
            // set converter options
            _converter.Options.PdfPageSize = pageSize;
            _converter.Options.PdfPageOrientation = pdfOrientation;
            _converter.Options.WebPageWidth = 1280;
            _converter.Options.WebPageHeight = 0;
            _converter.Options.MarginTop = 20;
            _converter.Options.MarginRight = 10;
            _converter.Options.MarginBottom = 20;
            _converter.Options.MarginLeft = 10;
            // create a new pdf document converting an url
            PdfDocument doc = _converter.ConvertUrl(URL);
            doc.Save(System.Web.HttpContext.Current.Response, false, "lista_inventario.pdf");
        }
        /* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
        /* *** *** *** *** <-- *** *** *** *** *** *** *** *** <-- *** *** *** *** *** *** *** *** <-- *** *** *** *** */


        /* *** *** *** *** *** *** *** *** *** *** *** *** Inventario/HistorialMovimientosInventario  *** *** *** *** *** *** *** *** *** ***  */
        /* *** *** *** *** --> *** *** *** *** *** *** *** *** --> *** *** *** *** *** *** *** *** --> *** *** *** *** */
        public PartialViewResult PDFListaMovimientosInventarios(int idSucursal, int id_tipo_movimiento, String periodo)
        {
            String[] fecha = Fn.Fechas.CalcularIncioFin(periodo);
            String inicio = fecha[0];
            String fin = fecha[1];

            List<movimiento_inventario> movimientos_inventario = new List<movimiento_inventario>();
            DateTime _inicio = Convert.ToDateTime(inicio, IdiomaFecha);
            DateTime _fin = Convert.ToDateTime(fin, IdiomaFecha);
            if (idSucursal == 0)
            {
                if (id_tipo_movimiento == 0)
                    movimientos_inventario = db.movimiento_inventario.Where(x => x.fecha > _inicio && x.fecha < _fin).ToList();
                else
                    movimientos_inventario = db.movimiento_inventario.Where(x => x.fecha > _inicio && x.fecha < _fin && x.tipo_movimiento_inventario_id == id_tipo_movimiento).ToList();
            }
            else
            {
                if (id_tipo_movimiento == 0)
                    movimientos_inventario = db.movimiento_inventario.Where(x => x.fecha > _inicio && x.fecha < _fin && x.sucursal_id == idSucursal).ToList();
                else
                    movimientos_inventario = db.movimiento_inventario.Where(x => x.fecha > _inicio && x.fecha < _fin && x.sucursal_id == idSucursal && x.tipo_movimiento_inventario_id == id_tipo_movimiento).ToList();
            }

            if (id_tipo_movimiento == 0)
                ViewBag.tipo_movimietno = "TODOS";
            else
                ViewBag.tipo_movimietno = db.tipo_movimiento_inventario.Find(id_tipo_movimiento).nombre.ToUpper();
            ViewBag.sucursal = db.sucursal.Find(idSucursal).nombre_sucursal.ToUpper();
            ViewBag.periodo = periodo;
            return PartialView(movimientos_inventario);
        }

        //Descargar: arma el archivo y lo regresa
        public void TraerPDFListaMovimientosInventarios(int idSucursal, int id_tipo_movimiento, String periodo)
        {
            Uri Request_Url = Request.Url;
            string URL = Request_Url.OriginalString;
            URL = URL.Replace(Request_Url.PathAndQuery, Url.Action("PDFListaMovimientosInventarios", "FormatoPDF", new { idSucursal = idSucursal, id_tipo_movimiento = id_tipo_movimiento, periodo = periodo }));
            PdfPageSize pageSize;
            pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize), "A4", true);
            PdfPageOrientation pdfOrientation = (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation), "Portrait", true);
            // instantiate a html to pdf converter object
            HtmlToPdf _converter = new HtmlToPdf();
            // set converter options
            _converter.Options.PdfPageSize = pageSize;
            _converter.Options.PdfPageOrientation = pdfOrientation;
            _converter.Options.WebPageWidth = 1280;
            _converter.Options.WebPageHeight = 0;
            _converter.Options.MarginTop = 20;
            _converter.Options.MarginRight = 10;
            _converter.Options.MarginBottom = 20;
            _converter.Options.MarginLeft = 10;
            // create a new pdf document converting an url
            PdfDocument doc = _converter.ConvertUrl(URL);
            doc.Save(System.Web.HttpContext.Current.Response, false, "lista_movimientos_inventarios.pdf");
        }
        /* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
        /* *** *** *** *** <-- *** *** *** *** *** *** *** *** <-- *** *** *** *** *** *** *** *** <-- *** *** *** *** */



        /* *** *** *** *** *** *** *** *** *** *** *** *** Inventario/HistorialMovimientosInventarioDetalle  *** *** *** *** *** *** *** *** *** ***  */
        /* *** *** *** *** --> *** *** *** *** *** *** *** *** --> *** *** *** *** *** *** *** *** --> *** *** *** *** */
        public PartialViewResult PDFListaMovimientosInventariosDetalle(int idSucursal, int id_tipo_movimiento, String periodo)
        {
            String[] fecha = Fn.Fechas.CalcularIncioFin(periodo);
            String inicio = fecha[0];
            String fin = fecha[1];

            DateTime _inicio = Convert.ToDateTime(inicio, IdiomaFecha);
            DateTime _fin = Convert.ToDateTime(fin, IdiomaFecha);
            List<conceptos_por_movimiento_inventario> cpx = new List<conceptos_por_movimiento_inventario>();
            if (idSucursal == 0)
            {
                if (id_tipo_movimiento == 0)
                    cpx = db.conceptos_por_movimiento_inventario.Where(x => x.movimiento_inventario.fecha > _inicio && x.movimiento_inventario.fecha < _fin).ToList();
                else
                    cpx = db.conceptos_por_movimiento_inventario.Where(x => x.movimiento_inventario.fecha > _inicio && x.movimiento_inventario.fecha < _fin && x.movimiento_inventario.tipo_movimiento_inventario_id == id_tipo_movimiento).ToList();
            }
            else
            {
                if (id_tipo_movimiento == 0)
                    cpx = db.conceptos_por_movimiento_inventario.Where(x => x.movimiento_inventario.fecha > _inicio && x.movimiento_inventario.fecha < _fin && x.movimiento_inventario.sucursal_id == idSucursal).ToList();
                else
                    cpx = db.conceptos_por_movimiento_inventario.Where(x => x.movimiento_inventario.fecha > _inicio && x.movimiento_inventario.fecha < _fin && x.movimiento_inventario.sucursal_id == idSucursal && x.movimiento_inventario.tipo_movimiento_inventario_id == id_tipo_movimiento).ToList();
            }

            if (id_tipo_movimiento == 0)
                ViewBag.tipo_movimietno = "TODOS";
            else
                ViewBag.tipo_movimietno = db.tipo_movimiento_inventario.Find(id_tipo_movimiento).nombre.ToUpper();
            ViewBag.sucursal = db.sucursal.Find(idSucursal).nombre_sucursal.ToUpper();
            ViewBag.periodo = periodo;
            return PartialView(cpx);
        }

        //Descargar: arma el archivo y lo regresa
        public void TraerPDFListaMovimientosInventariosDetalle(int idSucursal, int id_tipo_movimiento, String periodo)
        {
            Uri Request_Url = Request.Url;
            string URL = Request_Url.OriginalString;
            URL = URL.Replace(Request_Url.PathAndQuery, Url.Action("PDFListaMovimientosInventariosDetalle", "FormatoPDF", new { idSucursal = idSucursal, id_tipo_movimiento = id_tipo_movimiento, periodo = periodo }));
            PdfPageSize pageSize;
            pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize), "A4", true);
            PdfPageOrientation pdfOrientation = (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation), "Portrait", true);
            // instantiate a html to pdf converter object
            HtmlToPdf _converter = new HtmlToPdf();
            // set converter options
            _converter.Options.PdfPageSize = pageSize;
            _converter.Options.PdfPageOrientation = pdfOrientation;
            _converter.Options.WebPageWidth = 1280;
            _converter.Options.WebPageHeight = 0;
            _converter.Options.MarginTop = 20;
            _converter.Options.MarginRight = 10;
            _converter.Options.MarginBottom = 20;
            _converter.Options.MarginLeft = 10;
            // create a new pdf document converting an url
            PdfDocument doc = _converter.ConvertUrl(URL);
            doc.Save(System.Web.HttpContext.Current.Response, false, "lista_movimientos_inventarios_detalle.pdf");
        }
        /* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
        /* *** *** *** *** <-- *** *** *** *** *** *** *** *** <-- *** *** *** *** *** *** *** *** <-- *** *** *** *** */


        /* *** *** *** *** *** *** *** *** *** *** *** *** Credito/Index  *** *** *** *** *** *** *** *** *** ***  */
        /* *** *** *** *** --> *** *** *** *** *** *** *** *** --> *** *** *** *** *** *** *** *** --> *** *** *** *** */
        public PartialViewResult PDFListaCreditosPorCliente(int id_sucursal)
        {
            List<int> ListaIdsClientes = db.multiple_pago.Where(x => x.id_tipo_pago_venta == 4).Select(y => y.venta.Cliente_id).ToList();
            List<cliente> ListaCreditosClientes = db.cliente.Where(x => x.venta.Where(y => y.multiple_pago.Where(z => z.id_tipo_pago_venta == 4).Any() && y.catalogo_status_id != 2 && y.catalogo_status_id != 12 && y.sucursal_id == id_sucursal).Count() > 0).ToList();
            ViewBag.ListaCreditosClientes = ListaCreditosClientes;
            ViewBag.sucursal = db.sucursal.Find(id_sucursal).nombre_sucursal.ToUpper();
            return PartialView();
        }

        //Descargar: arma el archivo y lo regresa
        public void TraerPDFListaCreditosPorCliente(int id_sucursal)
        {
            Uri Request_Url = Request.Url;
            string URL = Request_Url.OriginalString;
            URL = URL.Replace(Request_Url.PathAndQuery, Url.Action("PDFListaCreditosPorCliente", "FormatoPDF", new { id_sucursal = id_sucursal }));
            PdfPageSize pageSize;
            pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize), "A4", true);
            PdfPageOrientation pdfOrientation = (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation), "Portrait", true);
            // instantiate a html to pdf converter object
            HtmlToPdf _converter = new HtmlToPdf();
            // set converter options
            _converter.Options.PdfPageSize = pageSize;
            _converter.Options.PdfPageOrientation = pdfOrientation;
            _converter.Options.WebPageWidth = 1280;
            _converter.Options.WebPageHeight = 0;
            _converter.Options.MarginTop = 20;
            _converter.Options.MarginRight = 10;
            _converter.Options.MarginBottom = 20;
            _converter.Options.MarginLeft = 10;
            // create a new pdf document converting an url
            PdfDocument doc = _converter.ConvertUrl(URL);
            doc.Save(System.Web.HttpContext.Current.Response, false, "lista_creditos_por_cliente.pdf");
        }
        /* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
        /* *** *** *** *** <-- *** *** *** *** *** *** *** *** <-- *** *** *** *** *** *** *** *** <-- *** *** *** *** */


        /* *** *** *** *** *** *** *** *** *** *** *** *** Credito/HistorialCreditos  *** *** *** *** *** *** *** *** *** ***  */
        /* *** *** *** *** --> *** *** *** *** *** *** *** *** --> *** *** *** *** *** *** *** *** --> *** *** *** *** */
        public PartialViewResult PDFListaHistorialCreditos(int id_sucursal, bool solamente_pendientes)
        {
            List<venta> ListaVentasCreditoCliente = new List<venta>();
            if (solamente_pendientes)
            {
                ListaVentasCreditoCliente = db.venta.Where(x => x.multiple_pago.Where(y => y.id_tipo_pago_venta == 4).Count() > 0 && x.aduedo.Value != 0 && x.sucursal_id == id_sucursal && (x.catalogo_status_id == 1 || x.catalogo_status_id == 9 || x.catalogo_status_id == 10 || x.catalogo_status_id == 11)).ToList();
            }
            else
            {
                ListaVentasCreditoCliente = db.venta.Where(x => x.multiple_pago.Where(y => y.id_tipo_pago_venta == 4).Count() > 0 && x.sucursal_id == id_sucursal && (x.catalogo_status_id == 1 || x.catalogo_status_id == 9 || x.catalogo_status_id == 10 || x.catalogo_status_id == 11)).ToList();
            }
            ViewBag.ListaVentasCreditoCliente = ListaVentasCreditoCliente;
            ViewBag.sucursal = db.sucursal.Find(id_sucursal).nombre_sucursal.ToUpper();
            if (solamente_pendientes)
                ViewBag.pendientes = "PENDIENTES";
            else
                ViewBag.pendientes = "PENDIENTES / PAGADAS";
            return PartialView();
        }

        //Descargar: arma el archivo y lo regresa
        public void TraerPDFListaHistorialCreditos(int id_sucursal, bool solamente_pendientes)
        {
            Uri Request_Url = Request.Url;
            string URL = Request_Url.OriginalString;
            URL = URL.Replace(Request_Url.PathAndQuery, Url.Action("PDFListaHistorialCreditos", "FormatoPDF", new { id_sucursal = id_sucursal, solamente_pendientes = solamente_pendientes }));
            PdfPageSize pageSize;
            pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize), "A4", true);
            PdfPageOrientation pdfOrientation = (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation), "Portrait", true);
            // instantiate a html to pdf converter object
            HtmlToPdf _converter = new HtmlToPdf();
            // set converter options
            _converter.Options.PdfPageSize = pageSize;
            _converter.Options.PdfPageOrientation = pdfOrientation;
            _converter.Options.WebPageWidth = 1280;
            _converter.Options.WebPageHeight = 0;
            _converter.Options.MarginTop = 20;
            _converter.Options.MarginRight = 10;
            _converter.Options.MarginBottom = 20;
            _converter.Options.MarginLeft = 10;
            // create a new pdf document converting an url
            PdfDocument doc = _converter.ConvertUrl(URL);
            doc.Save(System.Web.HttpContext.Current.Response, false, "lista_historial_creditos.pdf");
        }
        /* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
        /* *** *** *** *** <-- *** *** *** *** *** *** *** *** <-- *** *** *** *** *** *** *** *** <-- *** *** *** *** */


        /* *** *** *** *** *** *** *** *** *** *** *** *** Credito/HistorialAbonosClientes  *** *** *** *** *** *** *** *** *** ***  */
        /* *** *** *** *** --> *** *** *** *** *** *** *** *** --> *** *** *** *** *** *** *** *** --> *** *** *** *** */
        public PartialViewResult PDFListaAbonosClientes(int id_sucursal)
        {
            List<abono> ListaAbonos = db.abono.Where(x => x.venta.sucursal_id == id_sucursal).ToList();
            ViewBag.ListaAbonos = ListaAbonos;
            ViewBag.sucursal = db.sucursal.Find(id_sucursal).nombre_sucursal.ToUpper();
            return PartialView();
        }

        //Descargar: arma el archivo y lo regresa
        public void TraerPDFListaAbonosClientes(int id_sucursal)
        {
            Uri Request_Url = Request.Url;
            string URL = Request_Url.OriginalString;
            URL = URL.Replace(Request_Url.PathAndQuery, Url.Action("PDFListaAbonosClientes", "FormatoPDF", new { id_sucursal = id_sucursal }));
            PdfPageSize pageSize;
            pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize), "A4", true);
            PdfPageOrientation pdfOrientation = (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation), "Portrait", true);
            // instantiate a html to pdf converter object
            HtmlToPdf _converter = new HtmlToPdf();
            // set converter options
            _converter.Options.PdfPageSize = pageSize;
            _converter.Options.PdfPageOrientation = pdfOrientation;
            _converter.Options.WebPageWidth = 1280;
            _converter.Options.WebPageHeight = 0;
            _converter.Options.MarginTop = 20;
            _converter.Options.MarginRight = 10;
            _converter.Options.MarginBottom = 20;
            _converter.Options.MarginLeft = 10;
            // create a new pdf document converting an url
            PdfDocument doc = _converter.ConvertUrl(URL);
            doc.Save(System.Web.HttpContext.Current.Response, false, "lista_abonos_clientes.pdf");
        }
        /* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
        /* *** *** *** *** <-- *** *** *** *** *** *** *** *** <-- *** *** *** *** *** *** *** *** <-- *** *** *** *** */


        /* *** *** *** *** *** *** *** *** *** *** *** *** Credito/MostrarCreditosClientes  *** *** *** *** *** *** *** *** *** ***  */
        /* *** *** *** *** --> *** *** *** *** *** *** *** *** --> *** *** *** *** *** *** *** *** --> *** *** *** *** */
        public PartialViewResult PDFListaResumenCreditosCliente(int id_cliente, bool solamente_pendientes)
        {
            cliente c = db.cliente.Find(id_cliente);
            ViewBag.nombre = c.Razon_social;
            ViewBag.id = id_cliente;

            /* *** *** *** *** *** *** */

            List<venta> listaVentasCreditoCliente = new List<venta>();
            if (solamente_pendientes)
                listaVentasCreditoCliente = db.venta.Where(x => x.Cliente_id == id_cliente && x.multiple_pago.Where(y => y.id_tipo_pago_venta == 4).Count() > 0 && x.aduedo.Value != 0 && (x.catalogo_status_id == 1 || x.catalogo_status_id == 9 || x.catalogo_status_id == 10 || x.catalogo_status_id == 11)).ToList();
            else
                listaVentasCreditoCliente = db.venta.Where(x => x.Cliente_id == id_cliente && x.multiple_pago.Where(y => y.id_tipo_pago_venta == 4).Count() > 0 && (x.catalogo_status_id == 1 || x.catalogo_status_id == 9 || x.catalogo_status_id == 10 || x.catalogo_status_id == 11)).ToList();
            ViewBag.listaVentasCreditoCliente = listaVentasCreditoCliente;

            /* *** *** *** *** *** *** */

            List<nota_credito> listaNotasCredito = new List<nota_credito>();
            if (solamente_pendientes)
            {
                List<int> ListaIdsVentas = c.venta.Where(x => x.nota_credito.Where(y => y.id_estatus_nota_credito == 1).Any()).Select(x => x.id).ToList();
                listaNotasCredito = db.nota_credito.Where(x => ListaIdsVentas.Contains(x.id_venta) && x.id_estatus_nota_credito == 1).ToList();
            }
            else
            {
                List<int> ListaIdsVentas = c.venta.Where(x => x.nota_credito.Any()).Select(x => x.id).ToList();
                listaNotasCredito = db.nota_credito.Where(x => ListaIdsVentas.Contains(x.id_venta)).ToList();
            }
            ViewBag.listaNotasCredito = listaNotasCredito;

            /* *** *** *** *** *** *** */

            List<abono> ListaAbonos = db.abono.Where(XmlReadMode => XmlReadMode.venta.Cliente_id == id_cliente).ToList();
            ViewBag.ListaAbonos = ListaAbonos;

            return PartialView(c);
        }

        //Descargar: arma el archivo y lo regresa
        public void TraerPDFListaResumenCreditosCliente(int id_cliente, bool solamente_pendientes)
        {
            Uri Request_Url = Request.Url;
            string URL = Request_Url.OriginalString;
            URL = URL.Replace(Request_Url.PathAndQuery, Url.Action("PDFListaResumenCreditosCliente", "FormatoPDF", new { id_cliente = id_cliente, solamente_pendientes = solamente_pendientes }));
            PdfPageSize pageSize;
            pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize), "A4", true);
            PdfPageOrientation pdfOrientation = (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation), "Portrait", true);
            // instantiate a html to pdf converter object
            HtmlToPdf _converter = new HtmlToPdf();
            // set converter options
            _converter.Options.PdfPageSize = pageSize;
            _converter.Options.PdfPageOrientation = pdfOrientation;
            _converter.Options.WebPageWidth = 1280;
            _converter.Options.WebPageHeight = 0;
            _converter.Options.MarginTop = 20;
            _converter.Options.MarginRight = 10;
            _converter.Options.MarginBottom = 20;
            _converter.Options.MarginLeft = 10;
            // create a new pdf document converting an url
            PdfDocument doc = _converter.ConvertUrl(URL);
            doc.Save(System.Web.HttpContext.Current.Response, false, "lista_abonos_clientes.pdf");
        }
        /* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
        /* *** *** *** *** <-- *** *** *** *** *** *** *** *** <-- *** *** *** *** *** *** *** *** <-- *** *** *** *** */


        /* *** *** *** *** *** *** *** *** *** *** *** *** NotaCredito/Index  *** *** *** *** *** *** *** *** *** ***  */
        /* *** *** *** *** --> *** *** *** *** *** *** *** *** --> *** *** *** *** *** *** *** *** --> *** *** *** *** */
        public PartialViewResult PDFListaNotasCredito(int id_sucursal, int id_estatus_nota_credito, String periodo)
        {
            List<nota_credito> ListaNotasCredito = db.nota_credito.ToList();
            if (id_sucursal != 0)
                ListaNotasCredito = ListaNotasCredito.Where(x => x.venta.sucursal_id == id_sucursal).ToList();
            if (id_estatus_nota_credito != 0)
                ListaNotasCredito = ListaNotasCredito.Where(x => x.id_estatus_nota_credito == id_estatus_nota_credito).ToList();
            if (!periodo.Equals(""))
            {
                String[] fecha = Fn.Fechas.CalcularIncioFin(periodo);
                String inicio = fecha[0];
                String fin = fecha[1];

                DateTime _inicio = Convert.ToDateTime(inicio, IdiomaFecha);
                DateTime _fin = Convert.ToDateTime(fin, IdiomaFecha);
                ListaNotasCredito = ListaNotasCredito.Where(x => x.fecha_creacion > _inicio && x.fecha_creacion < _fin).ToList();
            }


            ViewBag.estatus_nota_credito = db.estatus_nota_credito.Find(id_estatus_nota_credito).nombre.ToUpper();
            ViewBag.sucursal = db.sucursal.Find(id_sucursal).nombre_sucursal.ToUpper();
            ViewBag.periodo = periodo;
            return PartialView(ListaNotasCredito);
        }

        //Descargar: arma el archivo y lo regresa
        public void TraerPDFListaNotasCredito(int id_sucursal, int id_estatus_nota_credito, String periodo)
        {
            Uri Request_Url = Request.Url;
            string URL = Request_Url.OriginalString;
            URL = URL.Replace(Request_Url.PathAndQuery, Url.Action("PDFListaNotasCredito", "FormatoPDF", new { id_sucursal = id_sucursal, id_estatus_nota_credito = id_estatus_nota_credito, periodo = periodo }));
            PdfPageSize pageSize;
            pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize), "A4", true);
            PdfPageOrientation pdfOrientation = (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation), "Portrait", true);
            // instantiate a html to pdf converter object
            HtmlToPdf _converter = new HtmlToPdf();
            // set converter options
            _converter.Options.PdfPageSize = pageSize;
            _converter.Options.PdfPageOrientation = pdfOrientation;
            _converter.Options.WebPageWidth = 1280;
            _converter.Options.WebPageHeight = 0;
            _converter.Options.MarginTop = 20;
            _converter.Options.MarginRight = 10;
            _converter.Options.MarginBottom = 20;
            _converter.Options.MarginLeft = 10;
            // create a new pdf document converting an url
            PdfDocument doc = _converter.ConvertUrl(URL);
            doc.Save(System.Web.HttpContext.Current.Response, false, "lista_notas_credito.pdf");
        }
        /* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
        /* *** *** *** *** <-- *** *** *** *** *** *** *** *** <-- *** *** *** *** *** *** *** *** <-- *** *** *** *** */


        /* *** *** *** *** *** *** *** *** *** *** *** *** CorteCaja/Index  *** *** *** *** *** *** *** *** *** ***  */
        /* *** *** *** *** --> *** *** *** *** *** *** *** *** --> *** *** *** *** *** *** *** *** --> *** *** *** *** */
        public PartialViewResult PDFListaCortesCaja(int id_sucursal)
        {
            List<corte_caja> ListaCortesCaja = db.corte_caja.Where(x => x.id_sucursal == id_sucursal).ToList();
            ViewBag.ListaCortesCaja = ListaCortesCaja;
            ViewBag.sucursal = db.sucursal.Find(id_sucursal).nombre_sucursal.ToUpper();
            return PartialView();
        }

        //Descargar: arma el archivo y lo regresa
        public void TraerPDFListaCortesCaja(int id_sucursal)
        {
            Uri Request_Url = Request.Url;
            string URL = Request_Url.OriginalString;
            URL = URL.Replace(Request_Url.PathAndQuery, Url.Action("PDFListaCortesCaja", "FormatoPDF", new { id_sucursal = id_sucursal }));
            PdfPageSize pageSize;
            pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize), "A4", true);
            PdfPageOrientation pdfOrientation = (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation), "Portrait", true);
            // instantiate a html to pdf converter object
            HtmlToPdf _converter = new HtmlToPdf();
            // set converter options
            _converter.Options.PdfPageSize = pageSize;
            _converter.Options.PdfPageOrientation = pdfOrientation;
            _converter.Options.WebPageWidth = 1280;
            _converter.Options.WebPageHeight = 0;
            _converter.Options.MarginTop = 20;
            _converter.Options.MarginRight = 10;
            _converter.Options.MarginBottom = 20;
            _converter.Options.MarginLeft = 10;
            // create a new pdf document converting an url
            PdfDocument doc = _converter.ConvertUrl(URL);
            doc.Save(System.Web.HttpContext.Current.Response, false, "lista_cortes_caja.pdf");
        }
        /* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
        /* *** *** *** *** <-- *** *** *** *** *** *** *** *** <-- *** *** *** *** *** *** *** *** <-- *** *** *** *** */


        /* *** *** *** *** *** *** *** *** *** *** *** *** Cliente/Index  *** *** *** *** *** *** *** *** *** ***  */
        /* *** *** *** *** --> *** *** *** *** *** *** *** *** --> *** *** *** *** *** *** *** *** --> *** *** *** *** */
        public PartialViewResult PDFListaClientes(int id_sucursal)
        {
            List<cliente> ListaClientes = db.cliente.Where(y => y.id_sucursal == id_sucursal).OrderBy(x => x.Razon_social).ToList();
            ViewBag.ListaClientes = ListaClientes;
            ViewBag.sucursal = db.sucursal.Find(id_sucursal).nombre_sucursal.ToUpper();
            return PartialView();
        }

        //Descargar: arma el archivo y lo regresa
        public void TraerPDFListaClientes(int id_sucursal)
        {
            Uri Request_Url = Request.Url;
            string URL = Request_Url.OriginalString;
            URL = URL.Replace(Request_Url.PathAndQuery, Url.Action("PDFListaClientes", "FormatoPDF", new { id_sucursal = id_sucursal }));
            PdfPageSize pageSize;
            pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize), "A4", true);
            PdfPageOrientation pdfOrientation = (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation), "Portrait", true);
            // instantiate a html to pdf converter object
            HtmlToPdf _converter = new HtmlToPdf();
            // set converter options
            _converter.Options.PdfPageSize = pageSize;
            _converter.Options.PdfPageOrientation = pdfOrientation;
            _converter.Options.WebPageWidth = 1280;
            _converter.Options.WebPageHeight = 0;
            _converter.Options.MarginTop = 20;
            _converter.Options.MarginRight = 10;
            _converter.Options.MarginBottom = 20;
            _converter.Options.MarginLeft = 10;
            // create a new pdf document converting an url
            PdfDocument doc = _converter.ConvertUrl(URL);
            doc.Save(System.Web.HttpContext.Current.Response, false, "lista_clientes.pdf");
        }
        /* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
        /* *** *** *** *** <-- *** *** *** *** *** *** *** *** <-- *** *** *** *** *** *** *** *** <-- *** *** *** *** */


        /* *** *** *** *** *** *** *** *** *** *** *** *** Usuario/Index  *** *** *** *** *** *** *** *** *** ***  */
        /* *** *** *** *** --> *** *** *** *** *** *** *** *** --> *** *** *** *** *** *** *** *** --> *** *** *** *** */
        public PartialViewResult PDFListaUsuario(int id_sucursal, int id_usuario)
        {
            // nivel usuario
            // 1 - Quianty
            // 2 - Admin    
            // 3 - Usuario
            usuario_punto_venta usuario_logueado = db.usuario_punto_venta.Find(id_usuario);
            int nivelUsuario = usuario_logueado.nivel_usuario_id;
            if (nivelUsuario == 1 || nivelUsuario == 2)
            {
                List<usuario_punto_venta> ListaUsuarios = db.usuario_punto_venta.Where(x => x.nivel_usuario_id >= nivelUsuario && x.sucursales_por_usuario.Select(y => y.sucursal_id).ToList().Contains(id_sucursal)).ToList();
                ViewBag.ListaUsuarios = ListaUsuarios;
            }
            else if (nivelUsuario == 3)
            {
                int id_usuario_logeado = usuario_logueado.id;
                List<usuario_punto_venta> ListaUsuarios = db.usuario_punto_venta.Where(x => x.id == id_usuario_logeado).ToList();
                ViewBag.ListaUsuarios = ListaUsuarios;
            }
            ViewBag.sucursal = db.sucursal.Find(id_sucursal).nombre_sucursal.ToUpper();
            ViewBag.id_usuario_logueado = usuario_logueado.id;
            return PartialView();
        }

        //Descargar: arma el archivo y lo regresa
        public void TraerPDFListaUsuarios(int id_sucursal, int id_usuario)
        {
            Uri Request_Url = Request.Url;
            string URL = Request_Url.OriginalString;
            URL = URL.Replace(Request_Url.PathAndQuery, Url.Action("PDFListaUsuario", "FormatoPDF", new { id_sucursal = id_sucursal, id_usuario = id_usuario }));
            PdfPageSize pageSize;
            pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize), "A4", true);
            PdfPageOrientation pdfOrientation = (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation), "Portrait", true);
            // instantiate a html to pdf converter object
            HtmlToPdf _converter = new HtmlToPdf();
            // set converter options
            _converter.Options.PdfPageSize = pageSize;
            _converter.Options.PdfPageOrientation = pdfOrientation;
            _converter.Options.WebPageWidth = 1280;
            _converter.Options.WebPageHeight = 0;
            _converter.Options.MarginTop = 20;
            _converter.Options.MarginRight = 10;
            _converter.Options.MarginBottom = 20;
            _converter.Options.MarginLeft = 10;
            // create a new pdf document converting an url
            PdfDocument doc = _converter.ConvertUrl(URL);
            doc.Save(System.Web.HttpContext.Current.Response, false, "lista_usuarios.pdf");
        }
        /* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
        /* *** *** *** *** <-- *** *** *** *** *** *** *** *** <-- *** *** *** *** *** *** *** *** <-- *** *** *** *** */


        /* *** *** *** *** *** *** *** *** *** *** *** *** MovimientoEfectivo/Index  *** *** *** *** *** *** *** *** *** ***  */
        /* *** *** *** *** --> *** *** *** *** *** *** *** *** --> *** *** *** *** *** *** *** *** --> *** *** *** *** */
        public PartialViewResult PDFListaMovimientosEfectivo(int id_sucursal, int opcion_estatus, int opcion_tipo_movimiento, String periodo)
        {
            /* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
            /* Si opcion_estatus == 0 es todos, 1 == activos, 2 == cancelados.                                     */
            /* Si opcion_tipo_movimiento == 0 es todos, 1 == entrada de efectivo, 2 == salida de efectivo,         */
            /* 3 == fondo de caja.                                                                                 */
            /* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
            String[] fecha = Fn.Fechas.CalcularIncioFin(periodo);
            String inicio = fecha[0];
            String fin = fecha[1];
            DateTime _inicio = Convert.ToDateTime(inicio, IdiomaFecha);
            DateTime _fin = Convert.ToDateTime(fin, IdiomaFecha);
            List<movimiento_efectivo> lista_movimiento_efectivo;
            if (opcion_estatus == 1)
            {
                if (opcion_tipo_movimiento == 0)
                    lista_movimiento_efectivo = db.movimiento_efectivo.Where(x => x.id_sucursal == id_sucursal && x.estatus && x.fecha >= _inicio && x.fecha <= _fin).ToList();
                else
                    lista_movimiento_efectivo = db.movimiento_efectivo.Where(x => x.id_sucursal == id_sucursal && x.estatus && x.id_tipo_movimiento_efectivo == opcion_tipo_movimiento && x.fecha >= _inicio && x.fecha <= _fin).ToList();
            }
            else if (opcion_estatus == 2)
            {
                if (opcion_tipo_movimiento == 0)
                    lista_movimiento_efectivo = db.movimiento_efectivo.Where(x => x.id_sucursal == id_sucursal && !x.estatus && ((x.fecha >= _inicio && x.fecha <= _fin) || (x.fecha_cancelacion >= _inicio && x.fecha_cancelacion <= _fin))).ToList();
                else
                    lista_movimiento_efectivo = db.movimiento_efectivo.Where(x => x.id_sucursal == id_sucursal && !x.estatus && x.id_tipo_movimiento_efectivo == opcion_tipo_movimiento && ((x.fecha >= _inicio && x.fecha <= _fin) || (x.fecha_cancelacion >= _inicio && x.fecha_cancelacion <= _fin))).ToList();
            }
            else
            {
                if (opcion_tipo_movimiento == 0)
                    lista_movimiento_efectivo = db.movimiento_efectivo.Where(x => x.id_sucursal == id_sucursal && ((x.fecha >= _inicio && x.fecha <= _fin) || (x.fecha_cancelacion >= _inicio && x.fecha_cancelacion <= _fin))).ToList();
                else
                    lista_movimiento_efectivo = db.movimiento_efectivo.Where(x => x.id_sucursal == id_sucursal && x.id_tipo_movimiento_efectivo == opcion_tipo_movimiento && ((x.fecha >= _inicio && x.fecha <= _fin) || (x.fecha_cancelacion >= _inicio && x.fecha_cancelacion <= _fin))).ToList();
            }
            ViewBag.ListaMovimientosEfectivo = lista_movimiento_efectivo;
            ViewBag.sucursal = db.sucursal.Find(id_sucursal).nombre_sucursal.ToUpper();
            if (opcion_tipo_movimiento == 0)
                ViewBag.tipo_movimiento = "TODOS";
            else
                ViewBag.tipo_movimiento = db.tipo_movimiento_efectivo.Find(opcion_tipo_movimiento).nombre.ToUpper();
            ViewBag.periodo = periodo;
            return PartialView();
        }

        //Descargar: arma el archivo y lo regresa
        public void TraerPDFListaMovimientosEfectivo(int id_sucursal, int opcion_estatus, int opcion_tipo_movimiento, String periodo)
        {
            Uri Request_Url = Request.Url;
            string URL = Request_Url.OriginalString;
            URL = URL.Replace(Request_Url.PathAndQuery, Url.Action("PDFListaMovimientosEfectivo", "FormatoPDF", new { id_sucursal = id_sucursal, opcion_estatus = opcion_estatus, opcion_tipo_movimiento = opcion_tipo_movimiento, periodo = periodo }));
            PdfPageSize pageSize;
            pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize), "A4", true);
            PdfPageOrientation pdfOrientation = (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation), "Portrait", true);
            // instantiate a html to pdf converter object
            HtmlToPdf _converter = new HtmlToPdf();
            // set converter options
            _converter.Options.PdfPageSize = pageSize;
            _converter.Options.PdfPageOrientation = pdfOrientation;
            _converter.Options.WebPageWidth = 1280;
            _converter.Options.WebPageHeight = 0;
            _converter.Options.MarginTop = 20;
            _converter.Options.MarginRight = 10;
            _converter.Options.MarginBottom = 20;
            _converter.Options.MarginLeft = 10;
            // create a new pdf document converting an url
            PdfDocument doc = _converter.ConvertUrl(URL);
            doc.Save(System.Web.HttpContext.Current.Response, false, "lista_movimientos_efectivo.pdf");
        }
        /* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
        /* *** *** *** *** <-- *** *** *** *** *** *** *** *** <-- *** *** *** *** *** *** *** *** <-- *** *** *** *** */


        /* *** *** *** *** *** *** *** *** *** *** *** *** Sucursal/Index  *** *** *** *** *** *** *** *** *** ***  */
        /* *** *** *** *** --> *** *** *** *** *** *** *** *** --> *** *** *** *** *** *** *** *** --> *** *** *** *** */
        public PartialViewResult PDFListaSucursales()
        {
            List<sucursal> ListaSucursales = db.sucursal.OrderBy(x => x.id).ToList();
            ViewBag.ListaSucursales = ListaSucursales;
            return PartialView();
        }

        //Descargar: arma el archivo y lo regresa
        public void TraerPDFListaSucursales()
        {
            Uri Request_Url = Request.Url;
            string URL = Request_Url.OriginalString;
            URL = URL.Replace(Request_Url.PathAndQuery, Url.Action("PDFListaSucursales", "FormatoPDF"));
            PdfPageSize pageSize;
            pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize), "A4", true);
            PdfPageOrientation pdfOrientation = (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation), "Portrait", true);
            // instantiate a html to pdf converter object
            HtmlToPdf _converter = new HtmlToPdf();
            // set converter options
            _converter.Options.PdfPageSize = pageSize;
            _converter.Options.PdfPageOrientation = pdfOrientation;
            _converter.Options.WebPageWidth = 1280;
            _converter.Options.WebPageHeight = 0;
            _converter.Options.MarginTop = 20;
            _converter.Options.MarginRight = 10;
            _converter.Options.MarginBottom = 20;
            _converter.Options.MarginLeft = 10;
            // create a new pdf document converting an url
            PdfDocument doc = _converter.ConvertUrl(URL);
            doc.Save(System.Web.HttpContext.Current.Response, false, "lista_sucursales.pdf");
        }
        /* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
        /* *** *** *** *** <-- *** *** *** *** *** *** *** *** <-- *** *** *** *** *** *** *** *** <-- *** *** *** *** */


        /* *** *** *** *** *** *** *** *** *** *** *** *** TipoPrecio/Index  *** *** *** *** *** *** *** *** *** ***  */
        /* *** *** *** *** --> *** *** *** *** *** *** *** *** --> *** *** *** *** *** *** *** *** --> *** *** *** *** */
        public PartialViewResult PDFListaTiposPrecio()
        {
            List<tipocliente> ListaClientes = db.tipocliente.OrderBy(x => x.orden).ToList();
            ViewBag.ListaClientes = ListaClientes;
            return PartialView();
        }

        //Descargar: arma el archivo y lo regresa
        public void TraerPDFListaTiposPrecio()
        {
            Uri Request_Url = Request.Url;
            string URL = Request_Url.OriginalString;
            URL = URL.Replace(Request_Url.PathAndQuery, Url.Action("PDFListaTiposPrecio", "FormatoPDF"));
            PdfPageSize pageSize;
            pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize), "A4", true);
            PdfPageOrientation pdfOrientation = (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation), "Portrait", true);
            // instantiate a html to pdf converter object
            HtmlToPdf _converter = new HtmlToPdf();
            // set converter options
            _converter.Options.PdfPageSize = pageSize;
            _converter.Options.PdfPageOrientation = pdfOrientation;
            _converter.Options.WebPageWidth = 1280;
            _converter.Options.WebPageHeight = 0;
            _converter.Options.MarginTop = 20;
            _converter.Options.MarginRight = 10;
            _converter.Options.MarginBottom = 20;
            _converter.Options.MarginLeft = 10;
            // create a new pdf document converting an url
            PdfDocument doc = _converter.ConvertUrl(URL);
            doc.Save(System.Web.HttpContext.Current.Response, false, "lista_tipos_precio.pdf");
        }
        /* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
        /* *** *** *** *** <-- *** *** *** *** *** *** *** *** <-- *** *** *** *** *** *** *** *** <-- *** *** *** *** */


        /* *** *** *** *** *** *** *** *** *** *** *** *** Configuracion/Permiso  *** *** *** *** *** *** *** *** *** ***  */
        /* *** *** *** *** --> *** *** *** *** *** *** *** *** --> *** *** *** *** *** *** *** *** --> *** *** *** *** */
        public PartialViewResult PDFListaPermisos()
        {
            List<permiso_vista> ListaPermisos = db.permiso_vista.ToList();
            ViewBag.ListaPermisos = ListaPermisos;
            return PartialView();
        }

        //Descargar: arma el archivo y lo regresa
        public void TraerPDFListaPermisos()
        {
            Uri Request_Url = Request.Url;
            string URL = Request_Url.OriginalString;
            URL = URL.Replace(Request_Url.PathAndQuery, Url.Action("PDFListaPermisos", "FormatoPDF"));
            PdfPageSize pageSize;
            pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize), "A4", true);
            PdfPageOrientation pdfOrientation = (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation), "Portrait", true);
            // instantiate a html to pdf converter object
            HtmlToPdf _converter = new HtmlToPdf();
            // set converter options
            _converter.Options.PdfPageSize = pageSize;
            _converter.Options.PdfPageOrientation = pdfOrientation;
            _converter.Options.WebPageWidth = 1280;
            _converter.Options.WebPageHeight = 0;
            _converter.Options.MarginTop = 20;
            _converter.Options.MarginRight = 10;
            _converter.Options.MarginBottom = 20;
            _converter.Options.MarginLeft = 10;
            // create a new pdf document converting an url
            PdfDocument doc = _converter.ConvertUrl(URL);
            doc.Save(System.Web.HttpContext.Current.Response, false, "lista_permisos.pdf");
        }
        /* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
        /* *** *** *** *** <-- *** *** *** *** *** *** *** *** <-- *** *** *** *** *** *** *** *** <-- *** *** *** *** */


        /* *** *** *** *** *** *** *** *** *** *** *** *** Venta/EntregaVenta  *** *** *** *** *** *** *** *** *** ***  */
        /* *** *** *** *** --> *** *** *** *** *** *** *** *** --> *** *** *** *** *** *** *** *** --> *** *** *** *** */
        public PartialViewResult PDFListaEntregaVentas(int idSucursal)
        {
            CultureInfo culture = new CultureInfo("es-MX");
            DateTime hoy = Convert.ToDateTime(DateTime.Now.ToString("dd/MM/yyyy"), culture);
            List<venta> ListaVentas = db.venta.Where(x => x.catalogo_status_id == 3 && x.fecha >= hoy && x.sucursal_id == idSucursal).ToList();
            ViewBag.ListaVentas = ListaVentas;
            ViewBag.sucursal = db.sucursal.Find(idSucursal).nombre_sucursal.ToUpper();
            return PartialView();
        }

        //Descargar: arma el archivo y lo regresa
        public void TraerPDFListaEntregaVentas(int idSucursal)
        {
            Uri Request_Url = Request.Url;
            string URL = Request_Url.OriginalString;
            URL = URL.Replace(Request_Url.PathAndQuery, Url.Action("PDFListaEntregaVentas", "FormatoPDF", new { idSucursal = idSucursal }));
            PdfPageSize pageSize;
            pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize), "A4", true);
            PdfPageOrientation pdfOrientation = (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation), "Portrait", true);
            // instantiate a html to pdf converter object
            HtmlToPdf _converter = new HtmlToPdf();
            // set converter options
            _converter.Options.PdfPageSize = pageSize;
            _converter.Options.PdfPageOrientation = pdfOrientation;
            _converter.Options.WebPageWidth = 1280;
            _converter.Options.WebPageHeight = 0;
            _converter.Options.MarginTop = 20;
            _converter.Options.MarginRight = 10;
            _converter.Options.MarginBottom = 20;
            _converter.Options.MarginLeft = 10;
            // create a new pdf document converting an url
            PdfDocument doc = _converter.ConvertUrl(URL);
            doc.Save(System.Web.HttpContext.Current.Response, false, "lista_entrega_ventas.pdf");
        }
        /* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
        /* *** *** *** *** <-- *** *** *** *** *** *** *** *** <-- *** *** *** *** *** *** *** *** <-- *** *** *** *** */


        /* *** *** *** *** *** *** *** *** *** *** *** *** Venta/Cotizacion  *** *** *** *** *** *** *** *** *** ***  */
        /* *** *** *** *** --> *** *** *** *** *** *** *** *** --> *** *** *** *** *** *** *** *** --> *** *** *** *** */
        public PartialViewResult PDFListaCotizaciones(int idSucursal, String periodo, String filtro)
        {
            CultureInfo culture = new CultureInfo("es-MX");
            List<venta> todas_cotizaciones_sucursal = db.venta.Where(x => x.catalogo_status_id == 4 && x.sucursal_id == idSucursal).OrderBy(x => x.Folio).ToList();
            List<venta> ListaCotizaciones = new List<venta>();
            if (filtro == "" || filtro == null) //filtro por fecha
            {
                String[] fecha = Fn.Fechas.CalcularIncioFin(periodo);
                String inicio = fecha[0];
                String fin = fecha[1];

                DateTime _inicio = Convert.ToDateTime(inicio, culture);
                DateTime _fin = Convert.ToDateTime(fin, culture);
                ListaCotizaciones = todas_cotizaciones_sucursal.Where(x => x.fecha > _inicio && x.fecha < _fin).ToList();
            }
            else//filtro por id_cotizacion
            {
                foreach (venta c in todas_cotizaciones_sucursal)
                {
                    string serie_folio = Fn.Numeros.serie_folio_by_id(c.id);
                    if (serie_folio.Contains(filtro))
                        ListaCotizaciones.Add(c);
                }
            }
            ViewBag.ListaCotizaciones = ListaCotizaciones;
            ViewBag.sucursal = db.sucursal.Find(idSucursal).nombre_sucursal.ToUpper();
            ViewBag.periodo = periodo;
            return PartialView();
        }

        //Descargar: arma el archivo y lo regresa
        public void TraerPDFListaCotizaciones(int idSucursal, String periodo, String filtro)
        {
            Uri Request_Url = Request.Url;
            string URL = Request_Url.OriginalString;
            URL = URL.Replace(Request_Url.PathAndQuery, Url.Action("PDFListaCotizaciones", "FormatoPDF", new { idSucursal = idSucursal, periodo = periodo, filtro = filtro }));
            PdfPageSize pageSize;
            pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize), "A4", true);
            PdfPageOrientation pdfOrientation = (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation), "Portrait", true);
            // instantiate a html to pdf converter object
            HtmlToPdf _converter = new HtmlToPdf();
            // set converter options
            _converter.Options.PdfPageSize = pageSize;
            _converter.Options.PdfPageOrientation = pdfOrientation;
            _converter.Options.WebPageWidth = 1280;
            _converter.Options.WebPageHeight = 0;
            _converter.Options.MarginTop = 20;
            _converter.Options.MarginRight = 10;
            _converter.Options.MarginBottom = 20;
            _converter.Options.MarginLeft = 10;
            // create a new pdf document converting an url
            PdfDocument doc = _converter.ConvertUrl(URL);
            doc.Save(System.Web.HttpContext.Current.Response, false, "lista_cotizaciones.pdf");
        }
        /* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
        /* *** *** *** *** <-- *** *** *** *** *** *** *** *** <-- *** *** *** *** *** *** *** *** <-- *** *** *** *** */


        /* *** *** *** *** *** *** *** *** *** *** *** *** TiendaOnline/Index  *** *** *** *** *** *** *** *** *** ***  */
        /* *** *** *** *** --> *** *** *** *** *** *** *** *** --> *** *** *** *** *** *** *** *** --> *** *** *** *** */
        public PartialViewResult PDFListaVentasTiendaOnline(int idSucursal, string periodo)
        {
            String UploadFolderTiendaOnline = new DirectoryInfo(Server.MapPath("~/")).Parent.FullName + "\\Tienda Online\\UploadFolder";
            CultureInfo culture = new CultureInfo("es-MX");
            String[] fecha = Fn.Fechas.CalcularIncioFin(periodo);
            String inicio = fecha[0];
            String fin = fecha[1];
            DateTime _inicio = Convert.ToDateTime(inicio, IdiomaFecha);
            DateTime _fin = Convert.ToDateTime(fin, IdiomaFecha);
            List<int> ListaIDVentaOnline = new List<int> { 8, 9, 10, 11 };//12 cancelada
            List<venta> ventas_online = db.venta.Where(x => ListaIDVentaOnline.Contains(x.catalogo_status_id) && x.sucursal_id == idSucursal &&
            ((x.fecha >= _inicio && x.fecha <= _fin) || (x.fechaEnvio >= _inicio && x.fechaEnvio <= _fin) ||
            (x.fecha_liquidacion >= _inicio && x.fecha_liquidacion <= _fin) || (x.Fecha_entrega >= _inicio && x.Fecha_entrega <= _fin))).ToList();
            ViewBag.sucursal = db.sucursal.Find(idSucursal).nombre_sucursal.ToUpper();
            ViewBag.periodo = periodo;
            return PartialView(ventas_online);
        }

        //Descargar: arma el archivo y lo regresa
        public void TraerPDFListaVentasTiendaOnline(int idSucursal, string periodo)
        {
            Uri Request_Url = Request.Url;
            string URL = Request_Url.OriginalString;
            URL = URL.Replace(Request_Url.PathAndQuery, Url.Action("PDFListaVentasTiendaOnline", "FormatoPDF", new { idSucursal = idSucursal, periodo = periodo }));
            PdfPageSize pageSize;
            pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize), "A4", true);
            PdfPageOrientation pdfOrientation = (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation), "Portrait", true);
            // instantiate a html to pdf converter object
            HtmlToPdf _converter = new HtmlToPdf();
            // set converter options
            _converter.Options.PdfPageSize = pageSize;
            _converter.Options.PdfPageOrientation = pdfOrientation;
            _converter.Options.WebPageWidth = 1280;
            _converter.Options.WebPageHeight = 0;
            _converter.Options.MarginTop = 20;
            _converter.Options.MarginRight = 10;
            _converter.Options.MarginBottom = 20;
            _converter.Options.MarginLeft = 10;
            // create a new pdf document converting an url
            PdfDocument doc = _converter.ConvertUrl(URL);
            doc.Save(System.Web.HttpContext.Current.Response, false, "lista_ventas_tienda_online.pdf");
        }
        /* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
        /* *** *** *** *** <-- *** *** *** *** *** *** *** *** <-- *** *** *** *** *** *** *** *** <-- *** *** *** *** */


        /* *** *** *** *** *** *** *** *** *** *** *** *** TiendaOnline/CotizacionOnline  *** *** *** *** *** *** *** *** *** ***  */
        /* *** *** *** *** --> *** *** *** *** *** *** *** *** --> *** *** *** *** *** *** *** *** --> *** *** *** *** */
        public PartialViewResult PDFListaCotizacionesOnline(int idSucursal, String periodo, String filtro)
        {
            CultureInfo culture = new CultureInfo("es-MX");
            String[] fecha = Fn.Fechas.CalcularIncioFin(periodo);
            String inicio = fecha[0];
            String fin = fecha[1];
            DateTime _inicio = Convert.ToDateTime(inicio, IdiomaFecha);
            DateTime _fin = Convert.ToDateTime(fin, IdiomaFecha);
            List<venta> todas_cotizaciones_sucursal = new List<venta>();
            if (periodo != null && !periodo.Equals(""))
                todas_cotizaciones_sucursal = db.venta.Where(x => (x.catalogo_status_id == 7) && x.sucursal_id == idSucursal && x.fecha_cotización >= _inicio && x.fecha_cotización <= _fin).OrderBy(x => x.Folio).ToList();
            else
                todas_cotizaciones_sucursal = db.venta.Where(x => (x.catalogo_status_id == 7) && x.sucursal_id == idSucursal).OrderBy(x => x.Folio).ToList();
            if (filtro == null)
                filtro = "";
            List<venta> cotizaciones_filtradas = new List<venta>();
            foreach (venta c in todas_cotizaciones_sucursal)
            {
                string serie_folio = Fn.Numeros.serie_folio_by_id(c.id);
                if (serie_folio.Contains(filtro))
                    cotizaciones_filtradas.Add(c);
            }
            ViewBag.sucursal = db.sucursal.Find(idSucursal).nombre_sucursal.ToUpper();
            ViewBag.periodo = periodo;
            return PartialView(cotizaciones_filtradas);
        }

        //Descargar: arma el archivo y lo regresa
        public void TraerPDFListaCotizacionesOnline(int idSucursal, String periodo, String filtro)
        {
            Uri Request_Url = Request.Url;
            string URL = Request_Url.OriginalString;
            URL = URL.Replace(Request_Url.PathAndQuery, Url.Action("PDFListaCotizacionesOnline", "FormatoPDF", new { idSucursal = idSucursal, periodo = periodo, filtro = filtro }));
            PdfPageSize pageSize;
            pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize), "A4", true);
            PdfPageOrientation pdfOrientation = (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation), "Portrait", true);
            // instantiate a html to pdf converter object
            HtmlToPdf _converter = new HtmlToPdf();
            // set converter options
            _converter.Options.PdfPageSize = pageSize;
            _converter.Options.PdfPageOrientation = pdfOrientation;
            _converter.Options.WebPageWidth = 1280;
            _converter.Options.WebPageHeight = 0;
            _converter.Options.MarginTop = 20;
            _converter.Options.MarginRight = 10;
            _converter.Options.MarginBottom = 20;
            _converter.Options.MarginLeft = 10;
            // create a new pdf document converting an url
            PdfDocument doc = _converter.ConvertUrl(URL);
            doc.Save(System.Web.HttpContext.Current.Response, false, "lista_cotizaciones_online.pdf");
        }
        /* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
        /* *** *** *** *** <-- *** *** *** *** *** *** *** *** <-- *** *** *** *** *** *** *** *** <-- *** *** *** *** */


        /* *** *** *** *** *** *** *** *** *** *** *** ***  Reporte/Consignacion  *** *** *** *** *** *** *** *** ***  */
        /* *** *** *** *** --> *** *** *** *** *** *** *** *** --> *** *** *** *** *** *** *** *** --> *** *** *** *** */
        public PartialViewResult PDFListaConsignaciones(int id_sucursal, int id_proveedor, int id_estatus, String periodo)
        {
            configuracion_general configuracion_general = Configuracion.TraerConfiguracionGeneral();
            String[] fecha = Fn.Fechas.CalcularIncioFin(periodo);
            String inicio = fecha[0];
            String fin = fecha[1];
            List<venta> ListaVentas = new List<venta>();
            List<conceptos_por_venta> ListaConceptos = new List<conceptos_por_venta>();
            DateTime _inicio = Convert.ToDateTime(inicio, IdiomaFecha);
            DateTime _fin = Convert.ToDateTime(fin, IdiomaFecha);
            ListaVentas = db.venta.Where(x => ((x.catalogo_status_id == 1 && x.Fecha_entrega >= _inicio && x.Fecha_entrega <= _fin)
                                              || (x.catalogo_status_id == 9 && x.fecha_liquidacion >= _inicio && x.fecha_liquidacion <= _fin)
                                              || (x.catalogo_status_id == 10 && x.fecha_liquidacion >= _inicio && x.fecha_liquidacion <= _fin)
                                              || (x.catalogo_status_id == 11 && x.fecha_liquidacion >= _inicio && x.fecha_liquidacion <= _fin))).ToList();
            if (id_sucursal != 0)
            {
                ListaVentas = ListaVentas.Where(x => x.sucursal_id == id_sucursal).ToList();
            }
            List<int> ListaIdsVentas = ListaVentas.Select(y => y.id).ToList();
            ListaConceptos = db.conceptos_por_venta.Where(x => ListaIdsVentas.Contains(x.Venta_id)).ToList();
            ListaConceptos = ListaConceptos.Where(x => x.producto.consignacion == 1).ToList();
            if (id_proveedor != 0)
            {
                ListaConceptos = ListaConceptos.Where(x => x.producto.Proveedor_id == id_proveedor).ToList();
            }
            ListaConceptos = ListaConceptos.Where(x => x.pagada_consignacion != null).ToList();
            if (id_estatus == 1)
            {
                ListaConceptos = ListaConceptos.Where(x => x.pagada_consignacion == false).ToList();
            }
            else if (id_estatus == 2)
            {
                ListaConceptos = ListaConceptos.Where(x => x.pagada_consignacion == true).ToList();
            }
            Decimal TotalPagado = 0;
            Decimal TotalDeuda = 0;
            Decimal TotalVendido = 0;
            foreach (conceptos_por_venta concepto_por_venta in ListaConceptos)
            {
                if (concepto_por_venta.producto.costo != null)
                {
                    Decimal total_concepto = 0;
                    total_concepto = Math.Round(concepto_por_venta.cantidad * concepto_por_venta.producto.costo.Value, 2);
                    TotalVendido += total_concepto;
                    if (concepto_por_venta.pagada_consignacion == true)
                    {
                        TotalPagado += total_concepto;
                    }
                    else
                    {
                        TotalDeuda += total_concepto;
                    }
                }
            }
            ViewBag.TotalPagado = TotalPagado;
            ViewBag.TotalDeuda = TotalDeuda;
            ViewBag.TotalVendido = TotalVendido;
            ViewBag.ListaVentas = ListaVentas;
            ViewBag.ListaConceptos = ListaConceptos;

            if (id_estatus == 0)
                ViewBag.tipo_movimietno = "TODOS";
            else
                ViewBag.tipo_movimietno = db.catalogo_status.Find(id_estatus).status.ToUpper();
            ViewBag.sucursal = db.sucursal.Find(id_sucursal).nombre_sucursal.ToUpper();
            ViewBag.periodo = periodo;
            return PartialView();
        }

        //Descargar: arma el archivo y lo regresa
        public void TraerPDFListaConsignaciones(int id_sucursal, int id_proveedor, int id_estatus, String periodo)
        {
            Uri Request_Url = Request.Url;
            string URL = Request_Url.OriginalString;
            URL = URL.Replace(Request_Url.PathAndQuery, Url.Action("PDFListaConsignaciones", "FormatoPDF", new { id_sucursal = id_sucursal, id_proveedor = id_proveedor, id_estatus = id_estatus, periodo = periodo }));
            PdfPageSize pageSize;
            pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize), "A4", true);
            PdfPageOrientation pdfOrientation = (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation), "Portrait", true);
            // instantiate a html to pdf converter object
            HtmlToPdf _converter = new HtmlToPdf();
            // set converter options
            _converter.Options.PdfPageSize = pageSize;
            _converter.Options.PdfPageOrientation = pdfOrientation;
            _converter.Options.WebPageWidth = 1280;
            _converter.Options.WebPageHeight = 0;
            _converter.Options.MarginTop = 20;
            _converter.Options.MarginRight = 10;
            _converter.Options.MarginBottom = 20;
            _converter.Options.MarginLeft = 10;
            // create a new pdf document converting an url
            PdfDocument doc = _converter.ConvertUrl(URL);
            doc.Save(System.Web.HttpContext.Current.Response, false, "lista_consignaciones.pdf");
        }
        /* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
        /* *** *** *** *** <-- *** *** *** *** *** *** *** *** <-- *** *** *** *** *** *** *** *** <-- *** *** *** *** */


        /* *** *** *** *** *** *** *** *** *** *** *** ***  Reporte/ModalConceptosMovimientoInventario  *** *** *** *** *** *** *** *** ***  */
        /* *** *** *** *** --> *** *** *** *** *** *** *** *** --> *** *** *** *** *** *** *** *** --> *** *** *** *** */
        public PartialViewResult PDFListaConceptosMovimientoInventario(int id)
        {
            movimiento_inventario mi = db.movimiento_inventario.Find(id);
            return PartialView(mi);
        }

        //Descargar: arma el archivo y lo regresa
        public void TraerPDFListaConceptosMovimientoInventario(int id)
        {
            Uri Request_Url = Request.Url;
            string URL = Request_Url.OriginalString;
            URL = URL.Replace(Request_Url.PathAndQuery, Url.Action("PDFListaConceptosMovimientoInventario", "FormatoPDF", new { id = id }));
            PdfPageSize pageSize;
            pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize), "A4", true);
            PdfPageOrientation pdfOrientation = (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation), "Portrait", true);
            // instantiate a html to pdf converter object
            HtmlToPdf _converter = new HtmlToPdf();
            // set converter options
            _converter.Options.PdfPageSize = pageSize;
            _converter.Options.PdfPageOrientation = pdfOrientation;
            _converter.Options.WebPageWidth = 1280;
            _converter.Options.WebPageHeight = 0;
            _converter.Options.MarginTop = 20;
            _converter.Options.MarginRight = 10;
            _converter.Options.MarginBottom = 20;
            _converter.Options.MarginLeft = 10;
            // create a new pdf document converting an url
            PdfDocument doc = _converter.ConvertUrl(URL);
            doc.Save(System.Web.HttpContext.Current.Response, false, "lista_conceptos_movimiento_inventario.pdf");
        }
        /* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
        /* *** *** *** *** <-- *** *** *** *** *** *** *** *** <-- *** *** *** *** *** *** *** *** <-- *** *** *** *** */

        /* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
        /* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
        /* *** *** *** *** *** *** *** *** *** *** *** Para exportar a PDF las Tablas  *** *** *** *** *** *** *** *** *** *** */
        /* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
        /* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */

        public ActionResult TraerTablaPDF(List<String> array_tablas, int numero_tablas, string encabezado, string nombre_tabla)
        {
            try
            {
                Uri Request_Url = Request.Url;

                SelectPdf.PdfPageSize pageSize = (SelectPdf.PdfPageSize)Enum.Parse(typeof(SelectPdf.PdfPageSize), "A4", true);
                SelectPdf.PdfPageOrientation pdfOrientation = (SelectPdf.PdfPageOrientation)Enum.Parse(typeof(SelectPdf.PdfPageOrientation), "Portrait", true);
                // instantiate a html to pdf converter object
                SelectPdf.HtmlToPdf _converter = new SelectPdf.HtmlToPdf();
                // set converter options
                _converter.Options.PdfPageSize = pageSize;
                _converter.Options.PdfPageOrientation = pdfOrientation;
                _converter.Options.WebPageWidth = 1280;
                _converter.Options.WebPageHeight = 0;
                _converter.Options.MarginTop = 10;
                _converter.Options.MarginBottom = 10;


                List<String> nombres_pdfs = new List<String>();
                for (int x = 0; x < numero_tablas; x++)
                {
                    StringBuilder result = GenerarCadenaAleatoria();
                    SelectPdf.PdfDocument doc = _converter.ConvertHtmlString(encabezado + array_tablas.ElementAt(x) + "</table></div></div>");
                    doc.Save(Server.MapPath("~/UploadFolder/PDF_email_temp/") + nombre_tabla + "_" + result.ToString() + ".pdf");
                    nombres_pdfs.Add(nombre_tabla + "_" + result.ToString() + ".pdf");
                }

                PdfSharp.Pdf.PdfDocument pdf_con_todas_escalas = new PdfSharp.Pdf.PdfDocument();

                foreach (String x in nombres_pdfs)
                {
                    PdfSharp.Pdf.PdfDocument src = PdfSharp.Pdf.IO.PdfReader.Open(Server.MapPath("~/PDF/") + x, PdfSharp.Pdf.IO.PdfDocumentOpenMode.Import);
                    CopyPages(src, pdf_con_todas_escalas);
                }

                StringBuilder result_final = GenerarCadenaAleatoria();
                pdf_con_todas_escalas.Save(Server.MapPath("~/UploadFolder/PDF_email_temp/") + nombre_tabla + "_" + result_final.ToString() + ".pdf");

                foreach (String x in nombres_pdfs)
                {
                    if (System.IO.File.Exists(Server.MapPath("~/UploadFolder/PDF_email_temp/") + x))
                    {
                        try
                        {
                            System.IO.File.Delete(Server.MapPath("~/UploadFolder/PDF_email_temp/") + x);
                        }
                        catch (System.IO.IOException e)
                        {
                            Console.WriteLine(e.Message);
                        }
                    }
                }

                var respuesta = new
                {
                    success = true,
                    key = result_final.ToString()
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                var respuesta = new
                {
                    success = false
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
        }

        public FileResult DescargarCotizacionDetalle(string key, string nombre_tabla)
        {
            return File(Server.MapPath("~/UploadFolder/PDF_email_temp/") + nombre_tabla + "_" + key + ".pdf", System.Net.Mime.MediaTypeNames.Application.Pdf, "CotizacionDetalle" + "_" + key + ".pdf");
        }

        public PartialViewResult VistaParcialDescargandoPDF()
        {
            return PartialView();
        }

        void CopyPages(PdfSharp.Pdf.PdfDocument from, PdfSharp.Pdf.PdfDocument to)
        {
            for (int i = 0; i < from.PageCount; i++)
            {
                to.AddPage(from.Pages[i]);
            }
        }

        public StringBuilder GenerarCadenaAleatoria()
        {
            char[] chars = new char[62];
            chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray();
            byte[] data = new byte[1];
            using (RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider())
            {
                crypto.GetNonZeroBytes(data);
                data = new byte[8];
                crypto.GetNonZeroBytes(data);
            }
            StringBuilder result = new StringBuilder(8);
            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length)]);
            }
            return result;
        }


    }
}