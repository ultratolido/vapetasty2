﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.Entity.Validation;
using System.Data.Entity.Infrastructure;
using System.IO;
using POS.Models;
using System.Data.Entity;
using POS.Fn;
using System.Web.Script.Serialization;
using MySql.Data.MySqlClient;

namespace POS.Controllers
{
    public class ProductoPadreController : Controller
    {
        moragas_pruebasEntities1 db = new moragas_pruebasEntities1();
        // GET: POS/ProductoPadre
        public ActionResult Index()
        {
            productopadre pP = new productopadre();
            ViewBag.producto = Fn.FnFiles.optionsProveedorTinta();
            return View(pP);
        }
        public ActionResult AgregarPadreHijo(productopadre padre, List<producto> Lhijos)
        {
            string error = "";
            bool success = false;
            try
            {
                db.productopadre.Add(padre);
                db.SaveChanges();
                foreach (var hijo in Lhijos)
                {
                    padre.producto.Add(db.producto.Find(hijo.id));
                }
                db.SaveChanges();
                success = true;
            }
            catch (Exception e)
            {
                success = false;
                error = e.Message.ToString();
            }

            var respuesta = new
            {
                error = error,
                success = success,
            };

            return Json(respuesta, JsonRequestBehavior.AllowGet);
        }
    }

}