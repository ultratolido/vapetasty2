﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using POS.Models;
using Newtonsoft.Json;
using System.Net;
using System.Net.Sockets;

namespace POS.Controllers
{
    public class AccountController : Controller
    {
        moragas_pruebasEntities1 db = new moragas_pruebasEntities1();
        static string cookieName = "logOnPOS";

        public ActionResult LogOn()
        {
            return View();
        }
        
        public double revisar_ip_baneada_segundos()
        {
            string TokenLogin = GuID_logon();
            double bloqueoSegundos = 0;
            if (TokenLogin != null)
            {
                if (db.historial_loggeos.Where(x => x.TokenLogin == TokenLogin && x.status).Any())
                {
                    //TRAER EL ULTIMO BANEO
                    //fecha mas actual baneo activo
                    DateTime maxFechaDesbloqueo = db.historial_loggeos.Where(x => x.TokenLogin == TokenLogin && x.status).Select(x => x.Fecha_fin_desbloqueo).Max();
                    //REVISAR SI LA FECHA DE DESBLOQUEO DE IP ES MENOR A LA FECHA ACTUAL
                    DateTime ahora = DateTime.Now;
                    if (maxFechaDesbloqueo < ahora)
                    {
                        //ya se termino el bloqueo
                        bloqueoSegundos = 0;
                    }
                    else
                    {
                        //LA IP TIENE BANEO ACTIVO
                        TimeSpan diff = maxFechaDesbloqueo - ahora;
                        bloqueoSegundos = diff.TotalSeconds;
                    }
                }
                else
                {
                    //no existe bloqueo
                    bloqueoSegundos = 0;
                }

            }
            else
            {
                //NO EXISTE COOKIE
                bloqueoSegundos = 60;
            }
            return bloqueoSegundos;
        }

        public int crearLogOnCookie(string _iplocal, string _ipPublica)
        {
            string _guid = System.Guid.NewGuid().ToString();
            int intentos = 0;
            //EXISTE ALGUNA COOKIE
            if (Request.Cookies[cookieName] != null)
            {
                //EXISTE LA COOKIE LLAMADA intentos
                dynamic cookieJson = Newtonsoft.Json.JsonConvert.DeserializeObject(Request.Cookies[cookieName].Value);
                intentos = cookieJson["intentos"];
            }
            else
            {
                //CREALA
                var cookieJson = new
                {
                    guid = _guid,
                    intentos = intentos.ToString(),
                    iplocal = _iplocal,
                    ipPublica = _ipPublica
                };

                HttpCookie logOnCookie = new HttpCookie(cookieName);
                logOnCookie.Value = JsonConvert.SerializeObject(cookieJson);
                logOnCookie.Expires = DateTime.Now.AddDays(1d);
                Response.Cookies.Add(logOnCookie);
            }
            return intentos;
        }
        
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult LogOn(LogOnModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                usuario_punto_venta u = db.usuario_punto_venta.Where(x => x.nombre_usuario == model.UserName && x.pw == model.Password).SingleOrDefault();
                if (u != null)
                {
                    limpiar_intentos_cookieLogon();
                    guardarloggeoCorrecto(u, model.localIPS, model.publicIPS);
                    //REVISAR SI EL USUARIO ESTA ACTIVO
                    if (u.id_estatus_usuario == 1)
                    {
                        //////ACTUALIZAR ULTIMA IP LOGGEADA PARA ESTE USUARIO
                        //////actualizarUltimaIPloggeada(u.id, model.localIPS, model.publicIPS);
                        FormsAuthentication.SetAuthCookie(Fn.usuario_logeado.strJson(u), model.RememberMe);

                        FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
                                           1,                                     // ticket version
                                           model.UserName,                              // authenticated username
                                           DateTime.Now,                          // issueDate
                                           DateTime.Now.AddMinutes(30),           // expiryDate
                                           true,                          // true to persist across browser sessions
                                           Fn.usuario_logeado.strJson(u),                              // can be used to store additional user data
                                           FormsAuthentication.FormsCookiePath);  // the path for the cookie

                        // Encrypt the ticket using the machine key
                        string encryptedTicket = FormsAuthentication.Encrypt(ticket);

                        // Add the cookie to the request to save it
                        HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                        cookie.HttpOnly = true;
                        Response.Cookies.Add(cookie);
                        u.token = SingleSessionPreparation.CreateAndStoreSessionToken_returnToken(u).ToString();
                        db.SaveChanges();
                        if (u.nivel_usuario_id == 3)
                        {
                            return RedirectToAction("NuevaVenta", "Venta");
                        }
                        else
                        {
                            return RedirectToAction("Index", "Home");
                        }
                    }
                    else
                    {
                        ViewBag.error = "El usuario esta desactivado.";
                        return View(model);
                    }
                }
                else
                {
                    //LOGGEO INCORRECTO
                    aumentarIntengoLogOnCookie();
                    historial_loggeos hbi = guardarloggeoIncorrecto(model.UserName, model.Password, model.localIPS, model.publicIPS);
                    int intento = hbi.intentos;
                    TimeSpan diff = hbi.Fecha_fin_desbloqueo - DateTime.Now;
                    int segundos = Convert.ToInt32(diff.TotalSeconds);
                    ViewBag.error = "El nombre de usuario o la contraseña son incorrectos.";
                    if (segundos > 0) { ViewBag.error += "<br>El usuario esta bloqueado por " + segundos + " seg."; }
                    return View(model);
                }
            }
            else
            {
                ViewBag.error = "Introduzca el nombre de usuario y la contraseña.";
            }
            return View(model);
        }
        
        public historial_loggeos guardarloggeoCorrecto(usuario_punto_venta u, string localIPS, string publicIPS)
        {
            historial_loggeos hlog = new historial_loggeos();
            localIPS = localIPS == null ? "" : localIPS;
            publicIPS = publicIPS == null ? "" : publicIPS;

            string guid = GuID_logon();
            //SI EXISTE COOKIE
            if (guid != null)
            {
                //EXISTE HISTORIAL BANEO IP ACTIVO?
                if (db.historial_loggeos.Where(x => x.TokenLogin == guid && x.status).Any())
                {
                    hlog = db.historial_loggeos.Where(x => x.TokenLogin == guid && x.status).Single();
                    hlog.intentos++;
                    hlog.Fecha_fin_desbloqueo = DateTime.Now.AddMinutes(-1);
                    hlog.status = false;
                    hlog.usuario_punto_venta_id = u.id;
                    //hlog.PLATAFORMA_id = u.PLATAFORMA_id;
                }
                else
                {
                    //CREAR 
                    hlog.Fecha_fin_desbloqueo = DateTime.Now.AddMinutes(-1);
                    hlog.TokenLogin = guid;
                    hlog.intentos = 1;
                    hlog.IP_local = localIPS;
                    hlog.IP_publica = publicIPS;
                    hlog.status = false;
                    hlog.usuario_punto_venta_id = u.id;
                    db.historial_loggeos.Add(hlog);
                }


                //AGREGAR LOGGEO INCORRECTO
                intento_loggeo li = new intento_loggeo();
                li.Fecha = DateTime.Now;
                li.Nombre = u.nombre_usuario;
                li.Pw = u.pw;
                hlog.intento_loggeo.Add(li);

                try
                {
                    //GUARDAR CAMBIOS
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                }
            }
            else
            {
                //NO EXISTE GUID COOKIE LOGIN
            }
            return hlog;
        }

        public void limpiar_intentos_cookieLogon()
        {
            string TokenLogin = GuID_logon();
            if (TokenLogin != null)
            {
                //Reiniciar intentos en cookie
                dynamic cookieJson = Newtonsoft.Json.JsonConvert.DeserializeObject(Request.Cookies[cookieName].Value);
                cookieJson["intentos"] = 0;

                HttpCookie myCookie = new HttpCookie(cookieName);
                myCookie.Value = JsonConvert.SerializeObject(cookieJson);
                myCookie.Expires = DateTime.Now.AddDays(1d);
                Response.Cookies.Add(myCookie);
                db.SaveChanges();
            }
        }

        public bool revisar_ip_baneada(string localIPS, string publicIPS)
        {
            if (db.historial_loggeos.Where(x => x.IP_local == localIPS && x.IP_publica == publicIPS && x.status).Any())
            {
                //TRAER EL ULTIMO BANEO
                //fecha mas actual baneo activo
                DateTime maxFechaDesbloqueo = db.historial_loggeos.Where(x => x.status).Select(x => x.Fecha_fin_desbloqueo).Max();
                //REVISAR SI LA FECHA DE DESBLOQUEO DE IP ES MENOR A LA FECHA ACTUAL
                DateTime ahora = DateTime.Now;
                if (maxFechaDesbloqueo < ahora)
                {
                    //ya se termino el bloqueo
                    return false;
                }
                else
                {
                    //LA IP TIENE BANEO ACTIVO
                    return true;
                }
            }
            else
            {
                //no existe bloqueo
                return false;
            }
        }

        public void aumentarIntengoLogOnCookie()
        {
            if (Request.Cookies[cookieName] != null)
            {
                //EXISTE
                dynamic cookieJson = Newtonsoft.Json.JsonConvert.DeserializeObject(Request.Cookies[cookieName].Value);
                cookieJson["intentos"] = Convert.ToInt16(cookieJson["intentos"]) + 1;

                HttpCookie myCookie = new HttpCookie(cookieName);
                myCookie.Value = JsonConvert.SerializeObject(cookieJson);
                myCookie.Expires = DateTime.Now.AddDays(1d);
                Response.Cookies.Add(myCookie);
            }
        }

        public string GuID_logon()
        {
            string guid = null;
            if (Request.Cookies[cookieName] != null)
            {
                //EXISTE
                dynamic cookieJson = Newtonsoft.Json.JsonConvert.DeserializeObject(Request.Cookies[cookieName].Value);
                guid = cookieJson["guid"];
            }
            return guid;
        }

        public historial_loggeos guardarloggeoIncorrecto(string nombre, string pw, string localIPS, string publicIPS)
        {
            historial_loggeos hlog = new historial_loggeos();
            localIPS = localIPS == null ? "" : localIPS;
            publicIPS = publicIPS == null ? "" : publicIPS;

            string guid = GuID_logon();
            //SI EXISTE COOKIE
            if (guid != null)
            {
                //EXISTE HISTORIAL BANEO IP ACTIVO?
                if (db.historial_loggeos.Where(x => x.TokenLogin == guid && x.status).Any())
                {
                    hlog = db.historial_loggeos.Where(x => x.TokenLogin == guid && x.status).Single();
                    DateTime ahora = DateTime.Now;
                    if (hlog.Fecha_fin_desbloqueo < ahora)
                    {
                        hlog.intentos++;
                        hlog.Fecha_fin_desbloqueo = DateTime.Now.AddSeconds(-1);
                        //MAS DE 5 INTENTOS?
                        if (hlog.intentos > 5)
                        {
                            //APLICA CASTIGO
                            int castigoSegundos = hlog.intentos * 1;
                            hlog.Fecha_fin_desbloqueo = DateTime.Now.AddSeconds(castigoSegundos);
                        }
                    }

                }
                else
                {
                    //CREAR 
                    hlog.Fecha_fin_desbloqueo = DateTime.Now.AddSeconds(-1);
                    hlog.TokenLogin = guid;
                    hlog.intentos = 1;
                    hlog.IP_local = localIPS;
                    hlog.IP_publica = publicIPS;
                    hlog.status = true;
                    db.historial_loggeos.Add(hlog);
                }


                //AGREGAR LOGGEO INCORRECTO
                intento_loggeo il = new intento_loggeo();
                il.Fecha = DateTime.Now;
                il.Nombre = nombre;
                il.Pw = pw;
                hlog.intento_loggeo.Add(il);

                try
                {
                    //GUARDAR CAMBIOS
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    //error = e.Message;
                }
            }
            else
            {
                //NO EXISTE GUID COOKIE LOGIN
            }
            return hlog;
        }

        public static string getIPlocal()
        {
            string _ip = "0.0.0.0";
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    _ip = ip.ToString();
                }
            }
            return _ip;
        }

        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("LogOn", "Account");
        }

        public ActionResult AccesoDenegadoNivel()
        {
            if (Fn.usuario_logeado.Nombre() != null && Fn.usuario_logeado.Nombre() != "")
            {
                return View();
            }
            else
            {
                return RedirectToAction("LogOn");
            }
        }

        public ActionResult AccesoDenegadoVista()
        {
            if (TempData["ControllerName"] != null)
                ViewBag.ControllerName = TempData["ControllerName"].ToString().ToUpper();
            if (TempData["ActionName"] != null)
                ViewBag.ActionName = TempData["ActionName"].ToString().ToUpper();

            if (Fn.usuario_logeado.Nombre() != null && Fn.usuario_logeado.Nombre() != "")
            {
                return View();
            }
            else
            {
                return RedirectToAction("LogOn");
            }
        }

        #region Status Codes
        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // Vaya a http://go.microsoft.com/fwlink/?LinkID=177550 para
            // obtener una lista completa de códigos de estado.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "El nombre de usuario ya existe. Escriba un nombre de usuario diferente.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "Ya existe un nombre de usuario para esa dirección de correo electrónico. Escriba una dirección de correo electrónico diferente.";

                case MembershipCreateStatus.InvalidPassword:
                    return "La contraseña especificada no es válida. Escriba un valor de contraseña válido.";

                case MembershipCreateStatus.InvalidEmail:
                    return "La dirección de correo electrónico especificada no es válida. Compruebe el valor e inténtelo de nuevo.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "La respuesta de recuperación de la contraseña especificada no es válida. Compruebe el valor e inténtelo de nuevo.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "La pregunta de recuperación de la contraseña especificada no es válida. Compruebe el valor e inténtelo de nuevo.";

                case MembershipCreateStatus.InvalidUserName:
                    return "El nombre de usuario especificado no es válido. Compruebe el valor e inténtelo de nuevo.";

                case MembershipCreateStatus.ProviderError:
                    return "El proveedor de autenticación devolvió un error. Compruebe los datos especificados e inténtelo de nuevo. Si el problema continúa, póngase en contacto con el administrador del sistema.";

                case MembershipCreateStatus.UserRejected:
                    return "La solicitud de creación de usuario se ha cancelado. Compruebe los datos especificados e inténtelo de nuevo. Si el problema continúa, póngase en contacto con el administrador del sistema.";

                default:
                    return "Error desconocido. Compruebe los datos especificados e inténtelo de nuevo. Si el problema continúa, póngase en contacto con el administrador del sistema.";
            }
        }
        #endregion
    }
}