﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using POS.Models;
using System.Data;
using System.Data.Entity.Validation;
using System.Data.Entity.Infrastructure;
using System.Data.Entity;
using POS.Fn;
using System.Reflection;
using System.IO;
using MySql.Data.MySqlClient;

namespace POS.Controllers
{
    public class ConfiguracionController : Controller
    {

        moragas_pruebasEntities1 db = new moragas_pruebasEntities1();

        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName = "Configuracion De La Plataforma")]
        public ActionResult Index()
        {
            configuracion_general configuracion_general = Configuracion.TraerConfiguracionGeneral();
            configuracion_pos configuracion_pos = Configuracion.TraerConfiguracionPOS();
            configuracion_tienda_online configuracion_tienda_online = Configuracion.TraerConfiguracionTiendaOnline();
            List<configuracion_menu> ListaMenus = Configuracion.TraerConfiguracionMenu();
            List<sucursal> ListaSucursales = Configuracion.TraerConfiguracionSucursales();
            List<imagen_carousel> ListaImagenesSlider = Configuracion.TraerImagenesSlider();
            List<int> ListaSucursalesPermitidas = Fn.usuario_logeado.Sucursales_id();
            if (configuracion_general.id_sucursal != null)
                ViewBag.id_sucursal = new SelectList(db.sucursal.Where(x => ListaSucursalesPermitidas.Contains(x.id)).OrderByDescending(y => y.id), "id", "nombre_sucursal", configuracion_general.id_sucursal);
            else
                ViewBag.id_sucursal = new SelectList(db.sucursal.Where(x => ListaSucursalesPermitidas.Contains(x.id)).OrderByDescending(y => y.id), "id", "nombre_sucursal");
            ViewBag.id_banco = new SelectList(db.banco, "id", "nombre");
            ViewBag.configuracion_general = configuracion_general;
            ViewBag.configuracion_pos = configuracion_pos;
            ViewBag.configuracion_tienda_online = configuracion_tienda_online;
            ViewBag.ListaMenus = ListaMenus;
            ViewBag.ListaSucursales = ListaSucursales;
            ViewBag.ListaImagenesSlider = ListaImagenesSlider;
            return View();
        }

        public ActionResult EditarConfiguracion(configuracion_general configuracion_general, configuracion_pos configuracion_pos, configuracion_tienda_online configuracion_tienda_online, configuracion_general configuracion_general_respaldo, List<sucursal> sucursales, List<configuracion_menu> menus)
        {
            try
            {
                if (configuracion_general.centralizacion_precios_sucursales == configuracion_general_respaldo.centralizacion_precios_sucursales
                    && configuracion_general.centralizacion_precios_web == configuracion_general_respaldo.centralizacion_precios_web
                    && configuracion_general.id_sucursal == configuracion_general_respaldo.id_sucursal)
                {
                    if (configuracion_general.centralizacion_precios_sucursales == false)
                        configuracion_general.id_sucursal = null;
                    db.Entry(configuracion_general).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {
                    if (configuracion_general.centralizacion_precios_sucursales == false)
                        configuracion_general.id_sucursal = null;
                    db.Entry(configuracion_general).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    if (configuracion_general.centralizacion_precios_sucursales == true && configuracion_general.centralizacion_precios_web == true)
                    {
                        List<precio_por_tipocliente> ListaPreciosEliminar = db.precio_por_tipocliente.Where(x => x.sucursal_id != configuracion_general.id_sucursal).ToList();
                        db.precio_por_tipocliente.RemoveRange(ListaPreciosEliminar);
                        db.SaveChanges();
                        List<sucursal> ListaSucursales = db.sucursal.Where(x => x.id != configuracion_general.id_sucursal).ToList();
                        List<precio_por_tipocliente> ListaPrecios = db.precio_por_tipocliente.Where(x => x.sucursal_id == configuracion_general.id_sucursal).ToList();
                        foreach (sucursal sucursal in ListaSucursales)
                        {
                            List<precio_por_tipocliente> ListaNuevosPrecios = ListaPrecios.Select(x => new precio_por_tipocliente()
                            {
                                TipoCliente_id = x.TipoCliente_id,
                                Precio = x.Precio,
                                sucursal_id = x.sucursal_id,
                                Producto_id = x.Producto_id
                            }).ToList();
                            ListaNuevosPrecios.All(x => { x.sucursal_id = sucursal.id; return true; });
                            db.precio_por_tipocliente.AddRange(ListaNuevosPrecios);
                            db.SaveChanges();
                        }
                    }
                    else if (configuracion_general.centralizacion_precios_sucursales == true && configuracion_general.centralizacion_precios_web == false)
                    {
                        List<int> ListaSucursalesEliminar = db.sucursal.Where(x => x.id != configuracion_general.id_sucursal && x.id != 1).Select(y => y.id).ToList();
                        List<precio_por_tipocliente> ListaPreciosEliminar = db.precio_por_tipocliente.Where(x => ListaSucursalesEliminar.Contains(x.sucursal_id)).ToList();
                        db.precio_por_tipocliente.RemoveRange(ListaPreciosEliminar);
                        db.SaveChanges();
                        List<sucursal> ListaSucursales = db.sucursal.Where(x => x.id != configuracion_general.id_sucursal && x.id != 1).ToList();
                        List<precio_por_tipocliente> ListaPrecios = db.precio_por_tipocliente.Where(x => x.sucursal_id == configuracion_general.id_sucursal).ToList();
                        foreach (sucursal sucursal in ListaSucursales)
                        {
                            List<precio_por_tipocliente> ListaNuevosPrecios = ListaPrecios.Select(x => new precio_por_tipocliente()
                            {
                                TipoCliente_id = x.TipoCliente_id,
                                Precio = x.Precio,
                                sucursal_id = x.sucursal_id,
                                Producto_id = x.Producto_id
                            }).ToList();
                            ListaNuevosPrecios.All(x => { x.sucursal_id = sucursal.id; return true; });
                            db.precio_por_tipocliente.AddRange(ListaNuevosPrecios);
                            db.SaveChanges();
                        }
                    }
                }
                db.Entry(configuracion_pos).State = System.Data.Entity.EntityState.Modified;
                db.Entry(configuracion_tienda_online).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                foreach (sucursal sucursal in sucursales)
                {
                    if (sucursal.id == 0)
                    {
                        db.sucursal.Add(sucursal);
                        db.SaveChanges();
                        crearInventarioSucursalNueva();
                        //crearInventariosNoExistentesSucursal(sucursal.id);
                    }
                    else
                    {
                        foreach (configuracion_sucursal_telefono configuracion_sucursal_telefono in db.configuracion_sucursal_telefono.Where(x => x.id_sucursal == sucursal.id).ToList())
                        {
                            db.configuracion_sucursal_telefono.Remove(configuracion_sucursal_telefono);
                            db.SaveChanges();
                        }
                        foreach (configuracion_sucursal_telefono configuracion_sucursal_telefono in sucursal.configuracion_sucursal_telefono.ToList())
                        {
                            configuracion_sucursal_telefono.id_sucursal = sucursal.id;
                            db.configuracion_sucursal_telefono.Add(configuracion_sucursal_telefono);
                            db.SaveChanges();
                        }
                        foreach (configuracion_sucursal_correo configuracion_sucursal_correo in db.configuracion_sucursal_correo.Where(x => x.id_sucursal == sucursal.id).ToList())
                        {
                            db.configuracion_sucursal_correo.Remove(configuracion_sucursal_correo);
                            db.SaveChanges();
                        }
                        foreach (configuracion_sucursal_correo configuracion_sucursal_correo in sucursal.configuracion_sucursal_correo.ToList())
                        {
                            configuracion_sucursal_correo.id_sucursal = sucursal.id;
                            db.configuracion_sucursal_correo.Add(configuracion_sucursal_correo);
                            db.SaveChanges();
                        }
                        foreach (configuracion_cuenta_bancaria configuracion_cuenta_bancaria in db.configuracion_cuenta_bancaria.Where(x => x.id_sucursal == sucursal.id).ToList())
                        {
                            db.configuracion_cuenta_bancaria.Remove(configuracion_cuenta_bancaria);
                            db.SaveChanges();
                        }
                        foreach (configuracion_cuenta_bancaria configuracion_cuenta_bancaria in sucursal.configuracion_cuenta_bancaria.ToList())
                        {
                            configuracion_cuenta_bancaria.id_sucursal = sucursal.id;
                            db.configuracion_cuenta_bancaria.Add(configuracion_cuenta_bancaria);
                            db.SaveChanges();
                        }
                        db.Entry(sucursal).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                }
                foreach (configuracion_submenu configuracion_submenu in db.configuracion_submenu.ToList())
                {
                    db.configuracion_submenu.Remove(configuracion_submenu);
                    db.SaveChanges();
                }
                foreach (configuracion_menu configuracion_menu in db.configuracion_menu.ToList())
                {
                    db.configuracion_menu.Remove(configuracion_menu);
                    db.SaveChanges();
                }
                foreach (configuracion_menu configuracion_menu in menus)
                {
                    db.configuracion_menu.Add(configuracion_menu);
                    db.SaveChanges();
                }
                var respuesta = new
                {
                    success = true
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                var respuesta = new
                {
                    success = false
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Bancos()
        {
            try
            {
                String select = "<select class='form-control id_banco' id='id_banco' name='id_banco'>";
                foreach (banco banco in db.banco.ToList())
                {
                    select = select + "<option value='" + banco.id + "'>" + banco.nombre + "</option>";
                }
                select = select + "</select>";
                var respuesta = new
                {
                    success = true,
                    bancos = select
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                var respuesta = new
                {
                    success = false
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
        }

        public PartialViewResult VistaParcialSlider()
        {
            List<imagen_carousel> ListaImagenesSlider = Configuracion.TraerImagenesSlider();
            ViewBag.ListaImagenesSlider = ListaImagenesSlider;
            return PartialView();
        }

        public ActionResult DesactivarSlider(int id)
        {
            try
            {
                imagen_carousel imagen = db.imagen_carousel.Find(id);
                imagen.activa = 0;
                db.Entry(imagen).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                var respuesta = new
                {
                    success = true
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                var respuesta = new
                {
                    success = false
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ActivarSlider(int id)
        {
            try
            {
                imagen_carousel imagen = db.imagen_carousel.Find(id);
                imagen.activa = 1;
                db.Entry(imagen).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                var respuesta = new
                {
                    success = true
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                var respuesta = new
                {
                    success = false
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult EliminarSlider(int id)
        {
            try
            {
                String UploadFolderTiendaOnline = new DirectoryInfo(Server.MapPath("~/")).Parent.FullName + "\\Tienda Online\\UploadFolder";
                imagen_carousel imagen = db.imagen_carousel.Find(id);
                imagen.activa = 0;
                System.IO.File.Delete(UploadFolderTiendaOnline + "\\imagen_slide\\" + imagen.nombre);
                db.imagen_carousel.Remove(imagen);
                db.SaveChanges();
                var respuesta = new
                {
                    success = true
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                var respuesta = new
                {
                    success = false
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
        }

        public void SubirImagenSlider()
        {
            String UploadFolderTiendaOnline = new DirectoryInfo(Server.MapPath("~/")).Parent.FullName + "\\Tienda Online\\UploadFolder";
            UploadedFile file = RetrieveFileFromRequest();
            String extension = Path.GetExtension(file.Filename).ToLower().Substring(1);
            imagen_carousel imagen = new imagen_carousel();
            imagen.nombre = file.Filename;
            imagen.extension = extension;
            imagen.fecha_publicacion = DateTime.Now;
            imagen.activa = 1;
            db.imagen_carousel.Add(imagen);
            db.SaveChanges();
            System.IO.File.WriteAllBytes(UploadFolderTiendaOnline + "\\imagen_slide\\" + imagen.id + "-" + file.Filename, file.Contents);
            imagen_carousel a = db.imagen_carousel.Find(imagen.id);
            if (a.extension == "png" ||
                a.extension == "bmp" ||
                a.extension == "jpg" ||
                a.extension == "jpeg")
            {
                System.Drawing.Image image = System.Drawing.Image.FromFile(UploadFolderTiendaOnline + "\\imagen_slide\\" + imagen.id + "-" + file.Filename);
                int imgWidth = image.Width;
                int imgHeight = image.Height;
                image.Dispose();
                imagen.width = imgWidth;
                imagen.height = imgHeight;
            }
            else
            {
                a.width = 0;
                a.height = 0;
            }
            a.nombre = imagen.id + "-" + file.Filename;
            db.Entry(a).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public PartialViewResult VistaParcialInformacion1()
        {
            configuracion_tienda_online configuracion_tienda_online = Configuracion.TraerConfiguracionTiendaOnline();
            ViewBag.configuracion_tienda_online = configuracion_tienda_online;
            return PartialView();
        }

        public ActionResult ActualizarLinkInformacion1(int id, String link)
        {
            try
            {
                configuracion_tienda_online configuracion_tienda_online = Configuracion.TraerConfiguracionTiendaOnline();
                if (id == 1)
                    configuracion_tienda_online.link_1_info1 = link;
                else if (id == 2)
                    configuracion_tienda_online.link_2_info1 = link;
                else if (id == 3)
                    configuracion_tienda_online.link_3_info1 = link;
                db.Entry(configuracion_tienda_online).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                var respuesta = new
                {
                    success = true
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                var respuesta = new
                {
                    success = false
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ActualizarImagenInformacion1(int id)
        {
            try
            {
                configuracion_tienda_online configuracion_tienda_online = Configuracion.TraerConfiguracionTiendaOnline();
                String UploadFolderTiendaOnline = new DirectoryInfo(Server.MapPath("~/")).Parent.FullName + "\\Tienda Online\\UploadFolder";
                UploadedFile file = RetrieveFileFromRequest();
                if (id == 1)
                {
                    if (configuracion_tienda_online.imagen_1_info1 != null && !configuracion_tienda_online.imagen_1_info1.Equals(""))
                        System.IO.File.Delete(UploadFolderTiendaOnline + "\\imagen_informacion1\\1-" + configuracion_tienda_online.imagen_1_info1);
                    System.IO.File.WriteAllBytes(UploadFolderTiendaOnline + "\\imagen_informacion1\\1-" + file.Filename, file.Contents);
                    configuracion_tienda_online.imagen_1_info1 = "1-" + file.Filename;
                }
                else if (id == 2)
                {
                    if (configuracion_tienda_online.imagen_2_info1 != null && !configuracion_tienda_online.imagen_2_info1.Equals(""))
                        System.IO.File.Delete(UploadFolderTiendaOnline + "\\imagen_informacion1\\2-" + configuracion_tienda_online.imagen_2_info1);
                    System.IO.File.WriteAllBytes(UploadFolderTiendaOnline + "\\imagen_informacion1\\2-" + file.Filename, file.Contents);
                    configuracion_tienda_online.imagen_2_info1 = "2-" + file.Filename;
                }
                else if (id == 3)
                {
                    if (configuracion_tienda_online.imagen_3_info1 != null && !configuracion_tienda_online.imagen_3_info1.Equals(""))
                        System.IO.File.Delete(UploadFolderTiendaOnline + "\\imagen_informacion1\\3-" + configuracion_tienda_online.imagen_3_info1);
                    System.IO.File.WriteAllBytes(UploadFolderTiendaOnline + "\\imagen_informacion1\\3-" + file.Filename, file.Contents);
                    configuracion_tienda_online.imagen_3_info1 = "3-" + file.Filename;
                }
                db.Entry(configuracion_tienda_online).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                var respuesta = new
                {
                    success = true
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                var respuesta = new
                {
                    success = false
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
        }

        public PartialViewResult VistaParcialDestacados()
        {
            return PartialView();
        }

        public PartialViewResult VistaParcialMarcas()
        {
            return PartialView();
        }

        public PartialViewResult VistaParcialInformacion2()
        {
            configuracion_tienda_online configuracion_tienda_online = Configuracion.TraerConfiguracionTiendaOnline();
            ViewBag.configuracion_tienda_online = configuracion_tienda_online;
            return PartialView();
        }

        public ActionResult ActualizarLinkInformacion2(String link)
        {
            try
            {
                configuracion_tienda_online configuracion_tienda_online = Configuracion.TraerConfiguracionTiendaOnline();
                configuracion_tienda_online.link_info2 = link;
                db.Entry(configuracion_tienda_online).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                var respuesta = new
                {
                    success = true
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                var respuesta = new
                {
                    success = false
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ActualizarImagenInformacion2()
        {
            try
            {
                configuracion_tienda_online configuracion_tienda_online = Configuracion.TraerConfiguracionTiendaOnline();
                String UploadFolderTiendaOnline = new DirectoryInfo(Server.MapPath("~/")).Parent.FullName + "\\Tienda Online\\UploadFolder";
                UploadedFile file = RetrieveFileFromRequest();
                if (configuracion_tienda_online.imagen_info2 != null && !configuracion_tienda_online.imagen_info2.Equals(""))
                    System.IO.File.Delete(UploadFolderTiendaOnline + "\\imagen_informacion2\\" + configuracion_tienda_online.imagen_info2);
                System.IO.File.WriteAllBytes(UploadFolderTiendaOnline + "\\imagen_informacion2\\" + file.Filename, file.Contents);
                configuracion_tienda_online.imagen_info2 = file.Filename;
                db.Entry(configuracion_tienda_online).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                var respuesta = new
                {
                    success = true
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                var respuesta = new
                {
                    success = false
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
        }

        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName = "Catalogo De Permisos")]
        public ActionResult Permiso()
        {
            List<permiso_vista> ListaPermisos = db.permiso_vista.ToList();
            return View();
        }

        public PartialViewResult VistaParcialPermiso()
        {
            List<permiso_vista> ListaPermisos = db.permiso_vista.ToList();
            ViewBag.ListaPermisos = ListaPermisos;
            return PartialView();
        }

        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName = "Crear Permiso")]
        public ActionResult NuevoPermiso()
        {
            List<string> Controladores = TraerListaControladores();
            ViewBag.nombreControlador = Controladores.Select(i => new SelectListItem()
            {
                Text = i.ToString(),
                Value = i
            });
            return View();
        }

        [HttpPost]
        public bool NuevoPermiso(string NombreVista, string NombreVistaPublico, string NombreControlador)
        {
            bool resultado = false;
            try
            {
                if (!db.permiso_vista.Where(x => x.nombreVista == NombreVista && x.nombreControlador == NombreControlador).Any())
                {
                    permiso_vista pv = new permiso_vista();
                    pv.nombreControlador = NombreControlador;
                    pv.nombreVista = NombreVista;
                    pv.nombreVistaPublico = NombreVistaPublico;
                    db.permiso_vista.Add(pv);
                    db.SaveChanges();
                }
                resultado = true;
            }
            catch (Exception)
            {
                throw;
            }
            return resultado;
        }

        public List<string> TraerListaControladores()
        {
            Assembly asm = Assembly.GetAssembly(typeof(POS.MvcApplication));
            var controllerlist = asm.GetTypes()
                    .Where(type => typeof(System.Web.Mvc.Controller).IsAssignableFrom(type))
                    .SelectMany(type => type.GetMethods(BindingFlags.Instance | BindingFlags.DeclaredOnly | BindingFlags.Public))
                    .Where(m => m.GetCustomAttributes(typeof(Fn.AutorizacionVistaUsuarioActivo), false).Any())
                    .Select(x => new { Controller = x.DeclaringType.Name, Action = x.Name, ReturnType = x.ReturnType.Name, Attributes = String.Join(",", x.GetCustomAttributes().Select(a => a.GetType().Name.Replace("Attribute", ""))) })
                    .OrderBy(x => x.Controller).ThenBy(x => x.Action).ToList();
            return controllerlist.Select(x => x.Controller.Replace("Controller", "").ToString()).Distinct().ToList();
        }

        public string TraerOptionsListaAccionesPorControladorRegistradas(string NombreControlador)
        {
            List<permiso_vista> listasAccionesRegistradas = db.permiso_vista.Where(x => x.nombreControlador == NombreControlador).ToList();
            string registros = "";
            foreach (permiso_vista accion in listasAccionesRegistradas)
            {
                registros += "<tr>"
                    + "<td>" + accion.nombreControlador + "</td>"
                    + "<td>" + accion.nombreVistaPublico + "</td>"
                    //+ "<td style='text-align:center;'>"
                    //    + "<a href='#' class='eliminar_permiso btn btn-danger' idPermiso='" + accion.id + "'>"
                    //        + "<span class='glyph-icon icon-separator'>"
                    //         + "   <i class='glyph-icon icon-times'></i>"
                    //        + "</span>"
                    //        + "<span class='button-content'>Eliminar</span>"
                    //    + "</a></td>"
                    + "</tr>";
            }
            return registros;
        }

        public string TraerOptionsListaAccionesPorControladorFaltantes(string NombreControlador)
        {
            List<permiso_vista> listaAccionesTodas = TraerListaAccionesPorControlador(NombreControlador);
            List<permiso_vista> listasAccionesRegistradas = db.permiso_vista.Where(x => x.nombreControlador == NombreControlador).ToList();


            var accionesNoRegistradas = listaAccionesTodas.Where(p => !listasAccionesRegistradas.Any(p2 => p2.nombreVista == p.nombreVista));

            string opciones = "";
            foreach (permiso_vista accion in accionesNoRegistradas)
            {
                opciones += "<option value='" + accion.nombreVista + "'>" + accion.nombreVistaPublico + "</option>";
            }
            return opciones;
        }

        public List<permiso_vista> TraerListaAccionesPorControlador(string NombreControlador)
        {
            NombreControlador += "Controller";
            Assembly asm = Assembly.GetAssembly(typeof(POS.MvcApplication));
            var controllerlist = asm.GetTypes()
                    .Where(type => typeof(System.Web.Mvc.Controller).IsAssignableFrom(type))
                    .SelectMany(type => type.GetMethods(BindingFlags.Instance | BindingFlags.DeclaredOnly | BindingFlags.Public))
                    .Where(m => m.GetCustomAttributes(typeof(Fn.AutorizacionVistaUsuarioActivo), false).Any() && m.DeclaringType.Name == NombreControlador)
                    .Select(x => new
                    {
                        Controller = x.DeclaringType.Name,
                        Action = x.Name,
                        ReturnType = x.ReturnType.Name,
                        displayName = x.GetCustomAttributes(true).OfType<Fn.AutorizacionVistaUsuarioActivo>().FirstOrDefault().displayName != null ?
                                        x.GetCustomAttributes(true).OfType<Fn.AutorizacionVistaUsuarioActivo>().FirstOrDefault().displayName :
                                        x.Name
                        ,
                        Attributes = String.Join(",", x.GetCustomAttributes().Select(a => a.GetType().Name.Replace("Attribute", "")))
                    })
                    .OrderBy(x => x.Controller).ThenBy(x => x.Action).ToList();
            return controllerlist.Select(x => new permiso_vista { nombreControlador = x.Controller, nombreVista = x.Action, nombreVistaPublico = x.displayName }).ToList();
        }

        [HttpPost]
        [AutorizacionVistaUsuarioActivo(displayName = "Eliminar Permiso")]
        public ActionResult EliminarPermiso(int id)
        {
            bool success = false;
            try
            {
                List<permiso_vista_por_usuario> eliminarPermisosPorUsuario = db.permiso_vista_por_usuario.Where(x => x.permiso_vista_id == id).ToList();
                foreach (var permisoPorUsuario in eliminarPermisosPorUsuario)
                {
                    db.permiso_vista_por_usuario.Remove(permisoPorUsuario);
                }
                if (db.permiso_vista.Where(x => x.id == id).Any())
                {
                    db.permiso_vista.Remove(db.permiso_vista.Find(id));
                }
                db.SaveChanges();
                var respuesta = new
                {
                    success = true
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                var respuesta = new
                {
                    success = false
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
        }

        private UploadedFile RetrieveFileFromRequest()
        {
            string filename = null;
            string fileType = null;
            byte[] fileContents = null;
            string ID;

            if (Request.Files.Count > 0)
            { //they're uploading the old way
                var file = Request.Files[0];
                fileContents = new byte[file.ContentLength];
                fileType = file.ContentType;
                filename = file.FileName;

            }
            else if (Request.ContentLength > 0)
            {
                fileContents = new byte[Request.ContentLength];
                Request.InputStream.Read(fileContents, 0, Request.ContentLength);
                filename = Request.Headers["X-File-Name"];
                fileType = Request.Headers["X-File-Type"];
            }

            return new UploadedFile()
            {
                Filename = filename,
                ContentType = fileType,
                FileSize = fileContents != null ? fileContents.Length : 0,
                Contents = fileContents
            };
        }

        public void ModificarLogo()
        {
            String UploadFolderTiendaOnline = new DirectoryInfo(Server.MapPath("~/")).Parent.FullName + "\\Tienda Online\\UploadFolder";
            UploadedFile file = RetrieveFileFromRequest();
            String extension = Path.GetExtension(file.Filename).ToLower();
            configuracion_general configuracion_general = db.configuracion_general.ToList().First();
            if (configuracion_general.logo != null && !configuracion_general.logo.Equals(""))
                System.IO.File.Delete(UploadFolderTiendaOnline + "\\logo\\" + configuracion_general.logo);
            configuracion_general.logo = file.Filename;
            System.IO.File.WriteAllBytes(UploadFolderTiendaOnline + "\\logo\\" + configuracion_general.logo, file.Contents);
            db.Entry(configuracion_general).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void ModificarFavicon()
        {
            String UploadFolderTiendaOnline = new DirectoryInfo(Server.MapPath("~/")).Parent.FullName + "\\Tienda Online\\UploadFolder";
            UploadedFile file = RetrieveFileFromRequest();
            String extension = Path.GetExtension(file.Filename).ToLower();
            configuracion_general configuracion_general = db.configuracion_general.ToList().First();
            if (configuracion_general.favicon != null && !configuracion_general.favicon.Equals(""))
                System.IO.File.Delete(UploadFolderTiendaOnline + "\\logo\\" + configuracion_general.favicon);
            configuracion_general.favicon = file.Filename;
            System.IO.File.WriteAllBytes(UploadFolderTiendaOnline + "\\logo\\" + configuracion_general.favicon, file.Contents);
            db.Entry(configuracion_general).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }
        
        public Boolean crearInventariosNoExistentesSucursal(int id_sucursal)
        {
            try
            {
                List<int> lista_id_productos = db.producto.Select(x => x.id).ToList();
                foreach (int x in lista_id_productos)
                {
                    inventario inventario = db.inventario.FirstOrDefault(v => v.Producto_id == x && v.sucursal_id == id_sucursal);
                    // si no existe el inventario entonces crearlo
                    if (inventario == null)
                    {
                        inventario inventario_nuevo = new inventario();
                        inventario_nuevo.Producto_id = x;
                        inventario_nuevo.sucursal_id = id_sucursal;
                        inventario_nuevo.cantidad = 0;
                        db.inventario.Add(inventario_nuevo);
                        db.SaveChanges();
                    }
                }
                return true;
            }
            catch (Exception)
            {
                return false;
                throw;
            }
        }

        public bool crearInventarioSucursalNueva()
        {
            string path = System.Web.HttpContext.Current.Server.MapPath("").Split('\\').ElementAt(System.Web.HttpContext.Current.Server.MapPath("").Split('\\').Length - 2);
            MySqlConnection dbConn = new MySqlConnection(Fn.ConnDB.getConnString(path));
            String query = "insert into inventario(producto_id,sucursal_id,cantidad)" +
                            "( " +
                            "SELECT p.id as _idProducto, s.id as _idSucursal, 0 " +
                            "FROM producto p, sucursal s " +
                            "WHERE NOT EXISTS(SELECT null " +
                            "FROM inventario i " +
                            "WHERE i.Producto_id = p.id and i.sucursal_id = s.id));";
            using (MySqlCommand com = new MySqlCommand(query, dbConn))
            {
                dbConn.Open();
                com.ExecuteNonQuery();
                dbConn.Close();
            }

            return true;
        }

    }
}
