﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using POS.Models;
using System.Data.Entity;
using POS.Fn;

namespace POS.Controllers
{
    public class FamiliaController : Controller
    {

        moragas_pruebasEntities1 db = new moragas_pruebasEntities1();

        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName = "Catalogo De Familias")]
        public ActionResult Index()
        {
            ViewBag.familias = db.familia.ToList();
            return View();
        }

        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName = "Crear Familia")]
        public ActionResult NuevaFamilia()
        {
            return View();
        }

        [HttpPost]
        public ActionResult NuevaFamilia(familia familia)
        {
            db.familia.Add(familia);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName = "Editar Familia")]
        public ActionResult EditarFamilia(int id)
        {
            return View(db.familia.Find(id));
        }

        [HttpPost]
        public ActionResult EditarFamilia(familia familia)
        {
            db.Entry(familia).State =System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public bool VerificarFamilia(int id, String familia)
        {
            bool existe;
            if (id == 0)
                existe = db.familia.Where(x => x.nombre.ToLower().Equals(familia.ToLower())).Any();
            else
                existe = db.familia.Where(x => x.id != id && x.nombre.ToLower().Equals(familia.ToLower())).Any();
            return existe;
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
