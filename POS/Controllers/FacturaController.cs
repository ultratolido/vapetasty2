﻿using POS.Fn;
using POS.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace POS.Controllers
{
    public class FacturaController : Controller
    {
        moragas_pruebasEntities1 db = new moragas_pruebasEntities1();
        CultureInfo IdiomaFecha = new CultureInfo("es-MX");

        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName = "Ver")]
        public void armarFactura()
        {
            WsFactura.WSSoapClient _WsFactura = new WsFactura.WSSoapClient();
            WsFactura.mFactura f = new WsFactura.mFactura();
            //AGREGAR CONCEPTOS
            WsFactura.ArrayOfMConcepto Lc = new WsFactura.ArrayOfMConcepto();
            //foreach (var item in collection)
            //{
            WsFactura.mConcepto c = new WsFactura.mConcepto();
            c.Cantidad = 1;
            c.valorUnitario = 1;
            c.Descripcion = "PRUEBA VENTA";
            c.Unidad = "PIEZA";
            Lc.Add(c);
            //}
            f.conceptos = Lc;

            //AGREGAR TRASLADADOS
            WsFactura.ArrayOfMTraslados Lt = new WsFactura.ArrayOfMTraslados();
            //foreach (var item in collection)
            //{
            WsFactura.mTraslados t = new WsFactura.mTraslados();
            t.importe = .16M;
            t.impuesto = "IVA";
            t.tasa = 16;
            Lt.Add(t);
            //}
            f.traslado = Lt;

            ////AGREGAR RETENCION
            //WsFactura.ArrayOfMRetencion Lr = new WsFactura.ArrayOfMRetencion();
            //foreach (var item in collection)
            //{
            //WsFactura.mRetencion r = new WsFactura.mRetencion();
            //r.importe = 16;
            //r.impuesto = .16M;
            //Lr.Add(r);
            //}
            //f.retencion = Lr;

            f.EmisorRFC = "RARC860607497";
            f.EmisorRazonSocial = "CARLOS MANUEL RAMÍREZ ROSALES";
            f.EmisorRegimenFiscal = "INCORPORACIÓN FISCAL";
            f.EmisorPW = "123";
            f.EmisorCalle = "SANTA TERESA DE JESÚS";
            f.EmisorNumExt = "266";
            f.EmisorNumInt = "B";
            f.EmisorCodigoPostal = "45040";
            f.EmisorMunicipio = "ZAPOPAN";
            f.EmisorEstado = "JALISCO";
            f.EmisorPais = "MÉXICO";

            f.ReceptorRFC = "RARR860607RX0";
            f.ReceptorRazonSocial = "ROBERTO RAMIREZ ROSALES";
            f.ReceptorRegimenFiscal = "RÉGIMEN DE LEY PERSONAS FÍSICAS CON ACTIVIDAD EMPRESARIALES O PROFESIONALES";
            f.ReceptorCalle = "ALCAMO";
            f.ReceptorNumExt = "3058";
            f.ReceptorNumInt = "";
            f.ReceptorCodigoPostal = "44670";
            f.ReceptorMunicipio = "GUADALAJARA";
            f.ReceptorEstado = "JALISCO";
            f.ReceptorPais = "MÉXICO";
            try
            {
                string json = _WsFactura.armarFactura(f);
            }
            catch (Exception e)
            {
                string error = e.Message;
                //TIEMPO
                //RFC NO REGISTRADO
                //CER NO VALIDO
                //XML MAL ARMADO
                throw;
            }
            //string response = f.armarFactura("rfc", "123", "");
        }

        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName = "Historial De Facturas")]
        public ActionResult Index()
        {
            WsFactura.WSSoapClient _WsFactura = new WsFactura.WSSoapClient();
            List<int> ListaSucursalesPermitidas = usuario_logeado.Sucursales_id();
            ViewBag.id_sucursal = new SelectList(db.sucursal.Where(x => ListaSucursalesPermitidas.Contains(x.id)).OrderByDescending(y => y.id), "id", "nombre_sucursal");
            ViewBag.HoyInicio = DateTime.Now.ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.HoyFin = DateTime.Now.ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.AyerInicio = DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.AyerFin = DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.AntierInicio = DateTime.Now.AddDays(-2).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.AntierFin = DateTime.Now.AddDays(-2).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.SemanaActualInicio = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.SemanaActualFin = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(6).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.SemanaPasadaInicio = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(-7).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.SemanaPasadaFin = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(-7).AddDays(6).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.SemanaAntepasadaInicio = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(-14).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.SemanaAntepasadaFin = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(-14).AddDays(6).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            DateTime hoy = Convert.ToDateTime(DateTime.Now.ToShortDateString());
            int numero_de_dia = hoy.Day;
            if (numero_de_dia > 15)
            //segunda quincena
            {
                ViewBag.QuincenaActualInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 15).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaActualFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.Month)).ToString("MM/dd/yyyy") + " 11:59:59 PM";
                ViewBag.QuincenaPasadaInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaPasadaFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddDays(15).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
                ViewBag.QuincenaAntepasadaInicio = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, 15).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaAntepasadaFin = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month)).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            }
            else //primera quincena
            {
                ViewBag.QuincenaActualInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaActualFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddDays(15).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
                ViewBag.QuincenaPasadaInicio = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, 15).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaPasadaFin = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month)).ToString("MM/dd/yyyy") + " 11:59:59 PM";
                ViewBag.QuincenaAntepasadaInicio = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaAntepasadaFin = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, 1).AddDays(15).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            }
            ViewBag.MesActualInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.MesActualFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(1).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.MesPasadoInicioFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.MesPasadoFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-1).AddMonths(1).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.MesAntepasadoInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-2).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.MesAntepasadoFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-2).AddMonths(1).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.id_catalogo_status = new SelectList(db.catalogo_status, "id", "status");
            return View();
        }

        public PartialViewResult VistaParcialFactura(int idSucursal, int id_catalogo_status, String periodo)
        {
            String[] fecha = Fn.Fechas.CalcularIncioFin(periodo);
            String inicio = fecha[0];
            String fin = fecha[1];
            usuario_punto_venta u = Fn.usuario_logeado.traerUsuario();
            List<venta> ListaVentas = new List<venta>();
            DateTime _inicio = Convert.ToDateTime(inicio, IdiomaFecha);
            DateTime _fin = Convert.ToDateTime(fin, IdiomaFecha);
            if (id_catalogo_status == 0)
            {
                ListaVentas = db.venta.Where(x => ((x.catalogo_status_id == 1 && x.Fecha_entrega >= _inicio && x.Fecha_entrega <= _fin)
                                                  || (x.catalogo_status_id == 9 && x.fecha_liquidacion >= _inicio && x.fecha_liquidacion <= _fin)
                                                  || (x.catalogo_status_id == 10 && x.fecha_liquidacion >= _inicio && x.fecha_liquidacion <= _fin)
                                                  || (x.catalogo_status_id == 11 && x.fecha_liquidacion >= _inicio && x.fecha_liquidacion <= _fin)
                                                  || (x.catalogo_status_id == 2 && x.fecha_cancelacion >= _inicio && x.fecha_cancelacion <= _fin)
                                                  || (x.catalogo_status_id == 12 && x.fecha_cancelacion >= _inicio && x.fecha_cancelacion <= _fin && x.fecha_liquidacion != null))
                                                  && x.sucursal_id == idSucursal).OrderBy(x => x.Folio).ToList();
            }
            else
            {
                if (id_catalogo_status == 1)
                {
                    ListaVentas = db.venta.Where(x => ((x.catalogo_status_id == 1 && x.Fecha_entrega >= _inicio && x.Fecha_entrega <= _fin))
                                                        && x.sucursal_id == idSucursal).OrderBy(x => x.Folio).ToList();
                }
                else if (id_catalogo_status == 2)
                {
                    ListaVentas = db.venta.Where(x => ((x.catalogo_status_id == 2 && x.fecha_cancelacion >= _inicio && x.fecha_cancelacion <= _fin))
                                                        && x.sucursal_id == idSucursal).OrderBy(x => x.Folio).ToList();
                }
                else if (id_catalogo_status >= 9 && id_catalogo_status <= 11)
                {
                    ListaVentas = db.venta.Where(x => ((x.catalogo_status_id == id_catalogo_status && x.fecha_liquidacion >= _inicio && x.fecha_liquidacion <= _fin))
                                                        && x.sucursal_id == idSucursal).OrderBy(x => x.Folio).ToList();
                }
                else if (id_catalogo_status == 12)
                {
                    ListaVentas = db.venta.Where(x => ((x.catalogo_status_id == id_catalogo_status && x.fecha_cancelacion >= _inicio
                                                        && x.fecha_cancelacion <= _fin && x.fecha_liquidacion != null))
                                                        && x.sucursal_id == idSucursal).OrderBy(x => x.Folio).ToList();
                }
            }
            Decimal total_facturado = ListaVentas.Select(x => x.total.Value).Sum();
            Decimal total_cancelado = ListaVentas.Where(x => x.catalogo_status_id == 2 || x.catalogo_status_id == 12).Select(x => x.total.Value).Sum();
            Decimal total = total_facturado - total_cancelado;
            List<pdf_venta> ListaPDFs = db.pdf_venta.ToList();
            ViewBag.ListaPDF = ListaPDFs;
            ViewBag.total_facturado = total_facturado;
            ViewBag.total_cancelado = total_cancelado;
            ViewBag.total = total;
            ViewBag.ListaVentas = ListaVentas;
            return PartialView();
        }
    }
}