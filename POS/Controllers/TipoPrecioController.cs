﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using POS.Models;
using System.Data.Entity;
using POS.Fn;

namespace POS.Controllers
{
    public class TipoPrecioController : Controller
    {

        moragas_pruebasEntities1 db = new moragas_pruebasEntities1();

        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName = "Lista De Tipos De Precios")]
        public ActionResult Index()
        {
            return View();
        }

        public PartialViewResult VistaParcialTipoPrecio()
        {
            List<tipocliente> ListaClientes = db.tipocliente.OrderBy(x => x.orden).ToList();
            ViewBag.ListaClientes = ListaClientes;
            return PartialView();
        }

        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName = "Crear Tipo De Precio")]
        public ActionResult NuevoTipoPrecio()
        {
            return View();
        }

        [HttpPost]
        public ActionResult NuevoTipoPrecio(tipocliente tipo_precio)
        {
            db.tipocliente.Add(tipo_precio);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName = "Editar Tipo De Precio")]
        public ActionResult EditarTipoPrecio(int id)
        {
            tipocliente tipo_precio = db.tipocliente.Find(id);
            return View(tipo_precio);
        }

        [HttpPost]
        public ActionResult EditarTipoPrecio(tipocliente tipo_precio)
        {
            db.Entry(tipo_precio).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public bool VerificarTipoPrecio(int id, String tipo_precio)
        {
            bool existe;
            if (id == 0)
                existe = db.tipocliente.Where(x => x.Tipo.ToLower().Equals(tipo_precio.ToLower())).Any();
            else
                existe = db.tipocliente.Where(x => x.id != id && x.Tipo.ToLower().Equals(tipo_precio.ToLower())).Any();
            return existe;
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
