﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using POS.Models;
using System.Data.Entity;
using POS.Fn;

namespace POS.Controllers
{
    public class ProveedorController : Controller
    {
        moragas_pruebasEntities1 db = new moragas_pruebasEntities1();

        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName = "Catalogo De Proveedores")]
        public ActionResult Index()
        {
            return View(db.proveedor);
        }

        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName = "Crear Proveedores")]
        public ViewResult NuevoProveedor()
        {
            return View();
        }

        [HttpPost]
        public ActionResult NuevoProveedor(proveedor proveedor)
        {
            db.proveedor.Add(proveedor);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName = "Editar Proveedores")]
        public ViewResult EditarProveedor(int id)
        {
            proveedor proveedor = db.proveedor.Find(id);
            return View(proveedor);
        }

        [HttpPost]
        public ActionResult EditarProveedor(proveedor proveedor)
        {
            db.Entry(proveedor).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public PartialViewResult _DetalleProveedor(int id)
        {
            proveedor proveedor = db.proveedor.Find(id);
            List<marca> lista_marcas = new List<marca>();
            List<familia> lista_familias = new List<familia>();
            List<producto> lista_productos = db.producto.Where(x => x.Proveedor_id == id).ToList();

            List<int> lista_id_marcas = lista_productos.Where(x => x.Proveedor_id == id).Select(x => x.id_marca).Distinct().ToList();
            List<int> lista_id_familias = lista_productos.Where(x => x.Proveedor_id == id).Select(x => x.subfamilia.Familia_id).Distinct().ToList();

            lista_marcas = db.marca.Where(x => lista_id_marcas.Contains(x.id)).ToList();
            lista_familias = db.familia.Where(x => lista_id_familias.Contains(x.id)).ToList();

            ViewBag.lista_productos = lista_productos;
            ViewBag.lista_marcas = lista_marcas;
            ViewBag.lista_familias = lista_familias;

            return PartialView(proveedor);
        }

        public string traer_proveedores_select()
        {
            string opciones = "";
            foreach (proveedor p in db.proveedor)
            {
                opciones += "<option value='" + p.id + "'>" + p.nombre_proveedor + "</option>";
            }
            return opciones;
        }

        public bool VerificarProveedor(int id, String proveedor)
        {
            bool existe;
            if (id == 0)
                existe = db.proveedor.Where(x => x.nombre_proveedor.ToLower().Equals(proveedor.ToLower())).Any();
            else
                existe = db.proveedor.Where(x => x.id != id && x.nombre_proveedor.ToLower().Equals(proveedor.ToLower())).Any();
            return existe;
        }

    }
}
