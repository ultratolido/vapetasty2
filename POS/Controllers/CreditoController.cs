﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using POS.Models;
using POS.Fn;

namespace POS.Controllers
{
    public class CreditoController : Controller
    {

        moragas_pruebasEntities1 db = new moragas_pruebasEntities1();

        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName = "Creditos Por Cliente")]
        public ViewResult Index()
        {
            List<int> ListaSucursalesPermitidas = Fn.usuario_logeado.Sucursales_id();
            ViewBag.id_sucursal = new SelectList(db.sucursal.Where(x => ListaSucursalesPermitidas.Contains(x.id)).OrderByDescending(y => y.id), "id", "nombre_sucursal");
            return View();
        }

        public PartialViewResult VistaParcialCreditoCliente(int id_sucursal)
        {
            List<int> ListaIdsClientes = db.multiple_pago.Where(x => x.id_tipo_pago_venta == 4).Select(y => y.venta.Cliente_id).ToList();
            List<cliente> ListaCreditosClientes = db.cliente.Where(x => x.venta.Where(y => y.multiple_pago.Where(z => z.id_tipo_pago_venta == 4).Any() && y.catalogo_status_id != 2 && y.catalogo_status_id != 12 && y.sucursal_id == id_sucursal).Count() > 0).ToList();
            ViewBag.ListaCreditosClientes = ListaCreditosClientes;
            return PartialView();
        }

        public ActionResult VistaEditarDiasCredito(int id, int ruta)
        {
            ViewBag.id = id;
            ViewBag.ruta = ruta;
            return View("EditarDiasCredito", db.cliente.Find(id));
        }

        [HttpPost]
        public ActionResult EditarDiasCredito(int id, int dias, int ruta)
        {
            cliente cliente = db.cliente.Find(id);
            cliente.dias_credito = dias;
            db.Entry(cliente).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            if (ruta == 1)
                return RedirectToAction("Index");
            else
                return RedirectToAction("MostrarCreditosClientes", new { id = id });
        }

        [Authorize]
        public ViewResult MostrarCreditosClientes(int id)
        {
            cliente c = db.cliente.Find(id);
            ViewBag.nombre = c.Razon_social;
            ViewBag.id = id;
            return View(c);
        }

        public PartialViewResult MostrarCreditosClientesVistaParcial(int id, bool solamente_pendientes)
        {
            cliente c = db.cliente.Find(id);
            List<venta> listaVentasCreditoCliente = new List<venta>();
            if (solamente_pendientes)
                listaVentasCreditoCliente = db.venta.Where(x => x.Cliente_id == id && x.multiple_pago.Where(y => y.id_tipo_pago_venta == 4).Count() > 0 && x.aduedo.Value != 0 && (x.catalogo_status_id == 1 || x.catalogo_status_id == 9 || x.catalogo_status_id == 10 || x.catalogo_status_id == 11)).ToList();
            else
                listaVentasCreditoCliente = db.venta.Where(x => x.Cliente_id == id && x.multiple_pago.Where(y => y.id_tipo_pago_venta == 4).Count() > 0 && (x.catalogo_status_id == 1 || x.catalogo_status_id == 9 || x.catalogo_status_id == 10 || x.catalogo_status_id == 11)).ToList();
            ViewBag.nombre = c.Razon_social;
            return PartialView(listaVentasCreditoCliente);
        }

        public PartialViewResult MostrarNotasCreditoClientesVistaParcial(int id, bool solamente_pendientes)
        {
            cliente c = db.cliente.Find(id);
            List<nota_credito> listaNotasCredito = new List<nota_credito>();
            if (solamente_pendientes)
            {
                List<int> ListaIdsVentas = c.venta.Where(x => x.nota_credito.Where(y => y.id_estatus_nota_credito == 1).Any()).Select(x => x.id).ToList();
                listaNotasCredito = db.nota_credito.Where(x => ListaIdsVentas.Contains(x.id_venta) && x.id_estatus_nota_credito == 1).ToList();
            }
            else
            {
                List<int> ListaIdsVentas = c.venta.Where(x => x.nota_credito.Any()).Select(x => x.id).ToList();
                listaNotasCredito = db.nota_credito.Where(x => ListaIdsVentas.Contains(x.id_venta)).ToList();
            }
            ViewBag.nombre = c.Razon_social;
            return PartialView(listaNotasCredito);
        }

        public PartialViewResult MostrarCreditosClientesExcelVistaParcial(int id, bool solamente_pendientes)
        {
            cliente c = db.cliente.Find(id);
            List<venta> listaVentasCreditoCliente = new List<venta>();
            List<nota_credito> ListaNotasCredito = new List<nota_credito>();
            if (solamente_pendientes)
            {
                listaVentasCreditoCliente = db.venta.Where(x => x.Cliente_id == id && x.multiple_pago.Where(y => y.id_tipo_pago_venta == 4).Count() > 0 && x.aduedo.Value != 0 && (x.catalogo_status_id == 1 || x.catalogo_status_id == 9 || x.catalogo_status_id == 10 || x.catalogo_status_id == 11)).ToList();
                List<int> ListaIdsVentas = c.venta.Where(x => x.nota_credito.Where(y => y.id_estatus_nota_credito == 1).Any()).Select(x => x.id).ToList();
                ListaNotasCredito = db.nota_credito.Where(x => ListaIdsVentas.Contains(x.id_venta) && x.id_estatus_nota_credito == 1).ToList();
            }
            else
            {
                listaVentasCreditoCliente = db.venta.Where(x => x.Cliente_id == id && x.multiple_pago.Where(y => y.id_tipo_pago_venta == 4).Count() > 0 && (x.catalogo_status_id == 1 || x.catalogo_status_id == 9 || x.catalogo_status_id == 10 || x.catalogo_status_id == 11)).ToList();
                List<int> ListaIdsVentas = c.venta.Where(x => x.nota_credito.Any()).Select(x => x.id).ToList();
                ListaNotasCredito = db.nota_credito.Where(x => ListaIdsVentas.Contains(x.id_venta)).ToList();
            }
            ViewBag.nombre = c.Razon_social;
            ViewBag.ListaNotasCredito = ListaNotasCredito;
            return PartialView(listaVentasCreditoCliente);
        }

        public ActionResult CancelarNotaCredito(int id, int id_cliente)
        {
            nota_credito nota_credito = db.nota_credito.Find(id);
            nota_credito.fecha_cancelacion = DateTime.Now;
            nota_credito.id_estatus_nota_credito = 3;
            db.Entry(nota_credito).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            venta venta = db.venta.Find(nota_credito.id_venta);
            movimiento_inventario movimiento_inventario = new movimiento_inventario();
            movimiento_inventario.fecha = DateTime.Now;
            movimiento_inventario.recibe = venta.cliente.Razon_social;
            movimiento_inventario.usuario_valida_id = Fn.usuario_logeado.traerUsuario().id;
            movimiento_inventario.tipo_movimiento_inventario_id = 6;
            movimiento_inventario.sucursal_id = venta.sucursal_id;
            db.movimiento_inventario.Add(movimiento_inventario);
            db.SaveChanges();
            List<concepto_por_nota_credito> ListaConceptosNotaCredito = db.concepto_por_nota_credito.Where(x => x.id_nota_credito == nota_credito.id).ToList();
            foreach (concepto_por_nota_credito concepto_por_nota_credito in ListaConceptosNotaCredito)
            {
                conceptos_por_venta concepto_venta = db.conceptos_por_venta.Find(concepto_por_nota_credito.id_conceptos_por_venta);
                inventario inventario = db.inventario.Where(x => x.Producto_id == concepto_venta.Producto_id && x.sucursal_id == venta.sucursal_id).First();
                inventario.cantidad = inventario.cantidad - concepto_por_nota_credito.cantidad;
                db.Entry(inventario).State = System.Data.Entity.EntityState.Modified;
                conceptos_por_movimiento_inventario conceptos_por_movimiento_inventario = new conceptos_por_movimiento_inventario();
                conceptos_por_movimiento_inventario.movimiento_inventario_id = movimiento_inventario.id;
                conceptos_por_movimiento_inventario.Inventario_id = inventario.id;
                conceptos_por_movimiento_inventario.cantidad = concepto_por_nota_credito.cantidad.Value;
                db.conceptos_por_movimiento_inventario.Add(conceptos_por_movimiento_inventario);
            }
            db.SaveChanges();
            return RedirectToAction("MostrarCreditosClientes", new { id = id_cliente });
        }

        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName = "Creditos Por Venta")]
        public ViewResult HistorialCreditos()
        {
            List<int> ListaSucursalesPermitidas = Fn.usuario_logeado.Sucursales_id();
            ViewBag.id_sucursal = new SelectList(db.sucursal.Where(x => ListaSucursalesPermitidas.Contains(x.id)).OrderByDescending(y => y.id), "id", "nombre_sucursal");
            return View();
        }

        public PartialViewResult HistorialCreditosVistaParcial(int id_sucursal, bool solamente_pendientes)
        {
            List<venta> ListaVentasCreditoCliente = new List<venta>();
            if (solamente_pendientes)
            {
                ListaVentasCreditoCliente = db.venta.Where(x => x.multiple_pago.Where(y => y.id_tipo_pago_venta == 4).Count() > 0 && x.aduedo.Value != 0 && x.sucursal_id == id_sucursal && (x.catalogo_status_id == 1 || x.catalogo_status_id == 9 || x.catalogo_status_id == 10 || x.catalogo_status_id == 11)).ToList();
            }
            else
            {
                ListaVentasCreditoCliente = db.venta.Where(x => x.multiple_pago.Where(y => y.id_tipo_pago_venta == 4).Count() > 0 && x.sucursal_id == id_sucursal && (x.catalogo_status_id == 1 || x.catalogo_status_id == 9 || x.catalogo_status_id == 10 || x.catalogo_status_id == 11)).ToList();
            }
            ViewBag.ListaVentasCreditoCliente = ListaVentasCreditoCliente;
            return PartialView();
        }

        public PartialViewResult VistaParcialHistorialAbonosXCliente(int id_cliente)
        {
            List<abono> ListaAbonos = db.abono.Where(XmlReadMode => XmlReadMode.venta.Cliente_id == id_cliente).ToList();
            ViewBag.ListaAbonos = ListaAbonos;
            return PartialView();
        }

        [Authorize]
        public ViewResult AbonoVentaCliente(int id)
        {
            venta v = db.venta.Where(x => x.id == id).Single();
            return View(v);
        }

        public ActionResult AplicarAbono(int id, decimal abono, int tipoPagoAbono)
        {
            venta v = db.venta.Where(x => x.id == id).Single();
            v.aduedo = v.aduedo - abono;
            cliente c = db.cliente.Find(v.Cliente_id);
            c.Adeudo_credito = c.Adeudo_credito - abono;
            abono a = new abono();
            a.importe = abono;
            a.fecha = DateTime.Now;
            a.usuario_id_recibe = Fn.usuario_logeado.traerUsuario().id;
            a.usuario_punto_venta1 = db.usuario_punto_venta.Find(a.usuario_id_recibe);
            a.Venta_id = id;
            a.Tipo_pago_abono_id = tipoPagoAbono;
            a.estatus = true;//true = activa, false = cancelada
            db.abono.Add(a);
            db.Entry(c).State = System.Data.Entity.EntityState.Modified;
            db.Entry(v).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("MostrarCreditosClientes", new { id = v.Cliente_id });
        }

        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName = "Historial De Abonos")]
        public ViewResult HistorialAbonosClientes()
        {
            List<int> ListaSucursalesPermitidas = Fn.usuario_logeado.Sucursales_id();
            ViewBag.id_sucursal = new SelectList(db.sucursal.Where(x => ListaSucursalesPermitidas.Contains(x.id)).OrderByDescending(y => y.id), "id", "nombre_sucursal");
            return View();
        }

        public PartialViewResult VistaParcialHistorialAbonosClientes(int id_sucursal)
        {
            List<abono> ListaAbonos = db.abono.Where(x => x.venta.sucursal_id == id_sucursal).ToList();
            ViewBag.ListaAbonos = ListaAbonos;
            return PartialView();
        }

        public JsonResult CancelaAbono(int id)
        {
            try
            {
                abono a = db.abono.Find(id);
                if (a.estatus)
                {
                    venta v = db.venta.Find(a.Venta_id);
                    cliente c = db.cliente.Find(v.Cliente_id);
                    a.fecha_cancelacion = DateTime.Now;
                    a.usuario_id_cancela = Fn.usuario_logeado.traerUsuario().id;
                    a.usuario_punto_venta = db.usuario_punto_venta.Find(a.usuario_id_cancela);
                    a.estatus = false;
                    v.aduedo = v.aduedo + a.importe;
                    c.Adeudo_credito = c.Adeudo_credito + a.importe;
                    db.Entry(a).State = System.Data.Entity.EntityState.Modified;
                    db.Entry(v).State = System.Data.Entity.EntityState.Modified;
                    db.Entry(c).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
                var respuesta = new
                {
                    success = true
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                var respuesta = new
                {
                    success = false
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
        }

        [Authorize]
        public ActionResult CancelarCredito(int id)
        {
            DateTime hoy = DateTime.Now;
            venta v = db.venta.Find(id);
            cliente c = db.cliente.Find(v.Cliente_id);
            List<abono> listaAbonos = v.abono.Where(x => x.estatus == true).ToList();
            foreach (abono a in listaAbonos)
            {
                a.estatus = false;
                a.fecha_cancelacion = hoy;
                a.usuario_id_cancela = Fn.usuario_logeado.traerUsuario().id;
                a.usuario_punto_venta1 = db.usuario_punto_venta.Find(a.usuario_id_cancela);
                c.Adeudo_credito = c.Adeudo_credito + a.importe;
                db.Entry(a).State = System.Data.Entity.EntityState.Modified;
            }
            db.Entry(c).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            Decimal pagos_credito = v.multiple_pago.Where(x => x.id_tipo_pago_venta == 4).ToList().Select(y => y.cantidad).Sum().Value;
            v.fecha_cancelacion = hoy;
            v.usuario_id_cancela = Fn.usuario_logeado.traerUsuario().id;
            v.usuario_punto_venta1 = db.usuario_punto_venta.Find(v.usuario_id_cancela);
            if (v.catalogo_status_id == 1)
            {
            v.catalogo_status_id = 2;
            }
            else
            {
                v.catalogo_status_id = 12;
            }
            c.Adeudo_credito = c.Adeudo_credito - pagos_credito;
            v.aduedo = 0;
            db.Entry(v).State = System.Data.Entity.EntityState.Modified;
            db.Entry(c).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            movimiento_inventario mi = new movimiento_inventario();
            mi.fecha = DateTime.Now;
            mi.recibe = v.cliente.Razon_social;
            mi.usuario_valida_id = Fn.usuario_logeado.traerUsuario().id;
            mi.tipo_movimiento_inventario_id = 3;//3- entrada por cancelación
            mi.sucursal_id = v.sucursal_id;
            db.movimiento_inventario.Add(mi);
            foreach (conceptos_por_venta concepto in v.conceptos_por_venta)
            {
                //regresar inventario
                inventario i = db.inventario.Where(x => x.Producto_id == concepto.Producto_id && x.sucursal_id == v.sucursal_id).First();
                i.cantidad = i.cantidad + concepto.cantidad;
                //agregar entrada de inventario
                conceptos_por_movimiento_inventario cxm = new conceptos_por_movimiento_inventario();
                cxm.movimiento_inventario_id = mi.id;
                cxm.Inventario_id = i.id;
                cxm.cantidad = concepto.cantidad;
                db.conceptos_por_movimiento_inventario.Add(cxm);
            }
            db.SaveChanges();
            return RedirectToAction("HistorialCreditos");
        }

        public String traerTipoPagoCredito()
        {
            String datos = "";
            List<tipo_pago_abono> listaTipoPagoAbono = db.tipo_pago_abono.ToList();
            foreach (tipo_pago_abono tpa in listaTipoPagoAbono)
            {
                datos = datos + "<option value='" + tpa.id + "'>" + tpa.tipo + "</option>";
            }
            return datos;
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}





