﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using POS.Models;
using POS.Fn;
using System.Globalization;

namespace POS.Controllers
{
    public class CorteCajaController : Controller
    {

        moragas_pruebasEntities1 db = new moragas_pruebasEntities1();
        CultureInfo IdiomaFecha = new CultureInfo("es-MX");

        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName = "Historial De Cortes De Caja")]
        public ViewResult Index()
        {
            List<int> ListaSucursalesPermitidas = Fn.usuario_logeado.Sucursales_id();
            ViewBag.id_sucursal = new SelectList(db.sucursal.Where(x => ListaSucursalesPermitidas.Contains(x.id)).OrderByDescending(y => y.id), "id", "nombre_sucursal");
            return View();
        }

        public PartialViewResult VistaParcialCorteCaja(int id_sucursal)
        {
            List<corte_caja> ListaCortesCaja = db.corte_caja.Where(x => x.id_sucursal == id_sucursal).ToList();
            ViewBag.ListaCortesCaja = ListaCortesCaja;
            return PartialView();
        }

        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName = "Crear Corte De Caja")]
        public ViewResult NuevoCorteCaja()
        {
            ViewBag.HoyInicio = DateTime.Now.ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.HoyFin = DateTime.Now.ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.AyerInicio = DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.AyerFin = DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.AntierInicio = DateTime.Now.AddDays(-2).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.AntierFin = DateTime.Now.AddDays(-2).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.SemanaActualInicio = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.SemanaActualFin = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(6).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.SemanaPasadaInicio = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(-7).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.SemanaPasadaFin = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(-7).AddDays(6).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.SemanaAntepasadaInicio = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(-14).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.SemanaAntepasadaFin = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(-14).AddDays(6).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            DateTime hoy = Convert.ToDateTime(DateTime.Now.ToShortDateString());
            int numero_de_dia = hoy.Day;
            if (numero_de_dia > 15)
            //segunda quincena
            {
                ViewBag.QuincenaActualInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 15).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaActualFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.Month)).ToString("MM/dd/yyyy") + " 11:59:59 PM";
                ViewBag.QuincenaPasadaInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaPasadaFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddDays(15).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
                ViewBag.QuincenaAntepasadaInicio = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, 15).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaAntepasadaFin = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month)).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            }
            else //primera quincena
            {
                ViewBag.QuincenaActualInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaActualFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddDays(15).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
                ViewBag.QuincenaPasadaInicio = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, 15).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaPasadaFin = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month)).ToString("MM/dd/yyyy") + " 11:59:59 PM";
                ViewBag.QuincenaAntepasadaInicio = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaAntepasadaFin = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, 1).AddDays(15).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            }
            ViewBag.MesActualInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.MesActualFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(1).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.MesPasadoInicioFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.MesPasadoFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-1).AddMonths(1).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.MesAntepasadoInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-2).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.MesAntepasadoFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-2).AddMonths(1).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            List<int> ListaSucursalesPermitidas = Fn.usuario_logeado.Sucursales_id();
            ViewBag.id_sucursal = new SelectList(db.sucursal.Where(x => ListaSucursalesPermitidas.Contains(x.id)).OrderByDescending(y => y.id), "id", "nombre_sucursal");
            return View();
        }

        [Authorize]
        public PartialViewResult VistaParcialNuevoCorteCaja(int id_sucursal, String periodo)
        {
            String[] fecha = Fn.Fechas.CalcularIncioFin(periodo);
            String inicio = fecha[0];
            String fin = fecha[1];
            DateTime fechaInicio = Convert.ToDateTime(inicio, IdiomaFecha);
            DateTime fechaFin = Convert.ToDateTime(fin, IdiomaFecha);
            DateTime fecha_cierre_inicio = Convert.ToDateTime(DateTime.Now.ToShortDateString());
            DateTime fecha_cierre_fin = fecha_cierre_inicio.AddDays(1).AddSeconds(-1);
            List<corte_caja> Cortes_caja_anteriores = db.corte_caja.Where(x => x.fechaFin.Value >= fecha_cierre_inicio && x.fechaFin.Value <= fecha_cierre_fin && x.id_sucursal == id_sucursal).ToList();
            //cortes de caja anteriores
            ViewBag.cortes_del_dia = Cortes_caja_anteriores;
            configuracion_pos configuracion_pos = db.configuracion_pos.FirstOrDefault();
            bool movimientos_efectivo = false;
            if (configuracion_pos != null)
                movimientos_efectivo = (bool)configuracion_pos.movimientos_efectivo;
            List<Decimal> Totales_Finales = new List<Decimal>();
            Decimal TotalFacturado = 0;
            Decimal TotalCancelado = 0;
            for (int x = 0; x < db.tipo_pago_venta.ToList().Count; x++)
            {
                Totales_Finales.Add(0);
            }
            Decimal total_ltp_01, total_ltp_02, total_ltp_03;
            /*
             *     ClaseAux01
             * Esta lista contiene:
             * val_01 -> VentaActiva
             * val_02 -> VentaCancelada
             * val_03 -> VentaTotal
             */

            /* *** *** *** *** *** *** *** *** *** *** *** ***   Lista Ventas  *** *** *** *** *** *** *** *** *** *** *** *** *** */
            /* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
            List<venta> ListaVentas = db.venta.Where(x => ((x.catalogo_status_id == 1 && x.Fecha_entrega >= fechaInicio && x.Fecha_entrega < fechaFin)
                                                          || (x.catalogo_status_id == 9 && x.fecha_liquidacion >= fechaInicio && x.fecha_liquidacion <= fechaFin)
                                                          || (x.catalogo_status_id == 10 && x.fecha_liquidacion >= fechaInicio && x.fecha_liquidacion <= fechaFin)
                                                          || (x.catalogo_status_id == 11 && x.fecha_liquidacion >= fechaInicio && x.fecha_liquidacion <= fechaFin)
                                                          || (x.catalogo_status_id == 2 && x.fecha_cancelacion >= fechaInicio && x.fecha_cancelacion <= fechaFin)
                                                          || (x.catalogo_status_id == 2 && x.Fecha_entrega >= fechaInicio && x.Fecha_entrega <= fechaFin)
                                                          || (x.catalogo_status_id == 12 && x.fecha_cancelacion >= fechaInicio && x.fecha_cancelacion <= fechaFin && x.fecha_liquidacion != null)
                                                          || (x.catalogo_status_id == 12 && x.fecha_liquidacion >= fechaInicio && x.fecha_liquidacion <= fechaFin))
                                                          && x.sucursal_id == id_sucursal).OrderBy(x => x.Folio).ToList();
            List<ClaseAux01> lista_venta_tipo = new List<ClaseAux01>();
            foreach (tipo_pago_venta item in db.tipo_pago_venta.ToList())
            {
                ClaseAux01 ca = new ClaseAux01(item.tipo);
                lista_venta_tipo.Add(ca);
            }
            List<TotalVenta> TotalesVentas = new List<TotalVenta>();

            foreach (venta venta in ListaVentas)
            {
                foreach (multiple_pago multiple_pago in venta.multiple_pago.ToList())
                {
                    //Recorre todos los tipos de pago venta para hacer un crece con los multiples pagos de la venta
                    for (int x = 0; x < lista_venta_tipo.Count; x++)
                    {
                        //Si no es pago a credito y corresponde al cruce
                        if (multiple_pago.id_tipo_pago_venta != 4 && multiple_pago.id_tipo_pago_venta == (x + 1))
                        {
                            lista_venta_tipo.ElementAt(x).val_01 += multiple_pago.cantidad.Value;
                        }
                    }
                    if ((venta.catalogo_status_id == 2 || venta.catalogo_status_id == 12) && venta.fecha_cancelacion != null)
                    {
                        if (venta.fecha_cancelacion >= fechaInicio && venta.fecha_cancelacion <= fechaFin)
                        {
                            for (int x = 0; x < lista_venta_tipo.Count; x++)
                            {
                                //Si no es pago a credito y corresponde al cruce
                                if (multiple_pago.id_tipo_pago_venta != 4 && multiple_pago.id_tipo_pago_venta == (x + 1))
                                {
                                    lista_venta_tipo.ElementAt(x).val_02 += multiple_pago.cantidad.Value;
                                }
                            }
                        }
                    }
                }
            }
            for (int x = 0; x < lista_venta_tipo.Count; x++)
            {
                lista_venta_tipo.ElementAt(x).val_03 = lista_venta_tipo.ElementAt(x).val_01 - lista_venta_tipo.ElementAt(x).val_02;
            }
            total_ltp_01 = total_ltp_02 = total_ltp_03 = 0;
            for (int x = 0; x < lista_venta_tipo.Count; x++)
            {
                TotalVenta total_item = new TotalVenta();
                total_item.tipo_pago = lista_venta_tipo.ElementAt(x).tipo;
                total_item.facturado = lista_venta_tipo.ElementAt(x).val_01;
                total_item.cancelado = lista_venta_tipo.ElementAt(x).val_02; ;
                total_item.total = lista_venta_tipo.ElementAt(x).val_03;
                total_ltp_01 += total_item.facturado;
                total_ltp_02 += total_item.cancelado;
                total_ltp_03 += total_item.total;
                Totales_Finales[x] += lista_venta_tipo.ElementAt(x).val_03;
                TotalesVentas.Add(total_item);
            }
            /* Para la suma de todos los totales --> ventas */
            TotalVenta total_total = new TotalVenta();
            total_total.tipo_pago = "Total";
            total_total.facturado = total_ltp_01;
            total_total.cancelado = total_ltp_02;
            total_total.total = total_ltp_03;
            TotalFacturado += total_ltp_01;
            TotalCancelado += total_ltp_02;
            TotalesVentas.Add(total_total);
            ViewBag.TotalesVentas = TotalesVentas;
            //List<venta> ListaVentasActivas = db.venta.Where(x => ((x.catalogo_status_id == 1 && x.Fecha_entrega >= fechaInicio && x.Fecha_entrega <= fechaFin)
            //                                                     || (x.catalogo_status_id == 9 && x.fecha_liquidacion >= fechaInicio && x.fecha_liquidacion <= fechaFin)
            //                                                     || (x.catalogo_status_id == 10 && x.fecha_liquidacion >= fechaInicio && x.fecha_liquidacion <= fechaFin)
            //                                                     || (x.catalogo_status_id == 11 && x.fecha_liquidacion >= fechaInicio && x.fecha_liquidacion <= fechaFin))
            //                                                     && x.sucursal_id == id_sucursal).OrderBy(x => x.Folio).ToList();
            //List<venta> ListaVentasCanceladas = db.venta.Where(x => ((x.catalogo_status_id == 2 && x.fecha_cancelacion >= fechaInicio && x.fecha_cancelacion <= fechaFin)
            //                                                        || (x.catalogo_status_id == 12 && x.fecha_cancelacion >= fechaInicio && x.fecha_cancelacion <= fechaFin && x.fecha_liquidacion != null))
            //                                                        && x.sucursal_id == id_sucursal).OrderBy(x => x.Folio).ToList();


            /* *** *** *** *** *** *** *** *** *** *** *** ***   Lista Ventas a Credito  *** *** *** *** *** *** *** *** *** *** *** *** *** */
            /* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */

            ClaseAux01 venta_tipo_credito = new ClaseAux01("Credito");

            TotalVenta TotalVentasACredito = new TotalVenta();

            foreach (venta venta in ListaVentas)
            {
                multiple_pago multiple_pago = venta.multiple_pago.FirstOrDefault(x => x.id_tipo_pago_venta == 4);
                if (multiple_pago != null)
                {
                    venta_tipo_credito.val_01 += Convert.ToDecimal(multiple_pago.cantidad);
                    if ((venta.catalogo_status_id == 2 || venta.catalogo_status_id == 12) && venta.fecha_cancelacion != null)
                    {
                        if (venta.fecha_cancelacion >= fechaInicio && venta.fecha_cancelacion <= fechaFin)
                        {
                            venta_tipo_credito.val_02 += multiple_pago.cantidad.Value;
                        }
                    }
                }
            }

            venta_tipo_credito.val_03 = venta_tipo_credito.val_01 - venta_tipo_credito.val_02;
            total_ltp_01 = total_ltp_02 = total_ltp_03 = 0;
            //for (int x = 0; x < lista_venta_tipo.Count; x++)
            //{
                TotalVentasACredito.tipo_pago = venta_tipo_credito.tipo;
                TotalVentasACredito.facturado = venta_tipo_credito.val_01;
                TotalVentasACredito.cancelado = venta_tipo_credito.val_02;
                TotalVentasACredito.total = venta_tipo_credito.val_03;
                total_ltp_01 += TotalVentasACredito.facturado;
                total_ltp_02 += TotalVentasACredito.cancelado;
                total_ltp_03 += TotalVentasACredito.total;
                //Totales_Finales[x] += lista_venta_tipo.ElementAt(x).val_03;
            //}
            /* Para la suma de todos los totales --> ventas */
            total_total = new TotalVenta();
            total_total.tipo_pago = "Total";
            total_total.facturado = total_ltp_01;
            total_total.cancelado = total_ltp_02;
            total_total.total = total_ltp_03;
            //TotalFacturado += total_ltp_01;
            //TotalCancelado += total_ltp_02;
            //TotalesVentas.Add(total_total);
            ViewBag.TotalesVentasACredito = total_total;

            /* *** *** *** *** *** *** *** *** *** *** *** ***   Lista Abonos  *** *** *** *** *** *** *** *** *** *** *** *** *** */
            /* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
            List<abono> ListaAbonos = db.abono.Where(x => ((x.estatus == true && x.fecha.Value <= fechaFin && x.fecha.Value >= fechaInicio) || (x.estatus == false && x.fecha_cancelacion.Value <= fechaFin && x.fecha_cancelacion.Value >= fechaInicio)) && x.venta.sucursal_id == id_sucursal).OrderBy(x => x.id).ToList();
            List<ClaseAux01> lista_tipo_abono = new List<ClaseAux01>();
            foreach (tipo_pago_abono item in db.tipo_pago_abono.ToList())
            {
                ClaseAux01 ca = new ClaseAux01(item.tipo);
                lista_tipo_abono.Add(ca);
            }
            TotalesVentas = new List<TotalVenta>();
            foreach (abono abono in ListaAbonos)
            {

                for (int x = 0; x < lista_tipo_abono.Count; x++)
                {
                    if (abono.Tipo_pago_abono_id == (x + 1))
                    {
                        lista_tipo_abono.ElementAt(x).val_01 += abono.importe.Value;
                    }
                }
                if (!abono.estatus)
                {
                    for (int x = 0; x < lista_tipo_abono.Count; x++)
                    {
                        if (abono.Tipo_pago_abono_id == (x + 1))
                        {
                            lista_tipo_abono.ElementAt(x).val_02 += abono.importe.Value;
                        }
                    }
                }
            }
            for (int x = 0; x < lista_tipo_abono.Count; x++)
            {
                lista_tipo_abono.ElementAt(x).val_03 = lista_tipo_abono.ElementAt(x).val_01 - lista_tipo_abono.ElementAt(x).val_02;
            }
            total_ltp_01 = total_ltp_02 = total_ltp_03 = 0;
            for (int x = 0; x < lista_tipo_abono.Count; x++)
            {
                TotalVenta total_item = new TotalVenta();
                total_item.tipo_pago = lista_tipo_abono.ElementAt(x).tipo;
                total_item.facturado = lista_tipo_abono.ElementAt(x).val_01;
                total_item.cancelado = lista_tipo_abono.ElementAt(x).val_02; ;
                total_item.total = lista_tipo_abono.ElementAt(x).val_03;
                total_ltp_01 += total_item.facturado;
                total_ltp_02 += total_item.cancelado;
                total_ltp_03 += total_item.total;
                Totales_Finales[x] += lista_tipo_abono.ElementAt(x).val_03;
                TotalesVentas.Add(total_item);
            }
            /* Para la suma de todos los totales --> abonos */
            total_total = new TotalVenta();
            total_total.tipo_pago = "Total";
            total_total.facturado = total_ltp_01;
            total_total.cancelado = total_ltp_02;
            total_total.total = total_ltp_03;
            TotalFacturado += total_ltp_01;
            TotalCancelado += total_ltp_02;
            TotalesVentas.Add(total_total);
            ViewBag.TotalesAbonos = TotalesVentas;
            List<abono> ListaAbonosActivos = db.abono.Where(x => (x.estatus == true && x.fecha.Value <= fechaFin && x.fecha.Value >= fechaInicio) && x.venta.sucursal_id == id_sucursal).OrderBy(x => x.id).ToList();
            List<abono> ListaAbonosCancelados = db.abono.Where(x => (x.estatus == false && x.fecha_cancelacion.Value <= fechaFin && x.fecha_cancelacion.Value >= fechaInicio) && x.venta.sucursal_id == id_sucursal).OrderBy(x => x.id).ToList();
            /* *** *** *** *** *** *** *** *** *** *** *** Lista Movimientos Efectivo  *** *** *** *** *** *** *** *** *** *** *** */
            /* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
            List<movimiento_efectivo> ListaMovimientosEfectivo = null;
            if (movimientos_efectivo)
            {
                ListaMovimientosEfectivo = db.movimiento_efectivo.Where(x => ((x.estatus == true && x.fecha.Value <= fechaFin && x.fecha.Value >= fechaInicio) || (x.estatus == false && x.fecha_cancelacion.Value <= fechaFin && x.fecha_cancelacion.Value >= fechaInicio)) && x.id_sucursal == id_sucursal).OrderBy(x => x.id).ToList();
                List<ClaseAux01> lista_tipo_movimiento_efectivo = new List<ClaseAux01>();
                foreach (tipo_movimiento_efectivo item in db.tipo_movimiento_efectivo.ToList())
                {
                    ClaseAux01 ca = new ClaseAux01(item.nombre);
                    lista_tipo_movimiento_efectivo.Add(ca);
                }
                TotalesVentas = new List<TotalVenta>();
                foreach (movimiento_efectivo movimiento_efectivo in ListaMovimientosEfectivo)
                {

                    for (int x = 0; x < lista_tipo_movimiento_efectivo.Count; x++)
                    {
                        if (movimiento_efectivo.id_tipo_movimiento_efectivo == (x + 1))
                        {
                            lista_tipo_movimiento_efectivo.ElementAt(x).val_01 += movimiento_efectivo.monto;
                        }
                    }
                    if (!movimiento_efectivo.estatus)
                    {
                        for (int x = 0; x < lista_tipo_movimiento_efectivo.Count; x++)
                        {
                            if (movimiento_efectivo.id_tipo_movimiento_efectivo == (x + 1))
                            {
                                lista_tipo_movimiento_efectivo.ElementAt(x).val_02 += movimiento_efectivo.monto;
                            }
                        }
                    }
                }
                for (int x = 0; x < lista_tipo_movimiento_efectivo.Count; x++)
                {
                    lista_tipo_movimiento_efectivo.ElementAt(x).val_03 = lista_tipo_movimiento_efectivo.ElementAt(x).val_01 - lista_tipo_movimiento_efectivo.ElementAt(x).val_02;
                }
                total_ltp_01 = total_ltp_02 = total_ltp_03 = 0;
                for (int x = 0; x < lista_tipo_movimiento_efectivo.Count; x++)
                {
                    TotalVenta total_item = new TotalVenta();
                    total_item.tipo_pago = lista_tipo_movimiento_efectivo.ElementAt(x).tipo;
                    total_item.facturado = lista_tipo_movimiento_efectivo.ElementAt(x).val_01;
                    total_item.cancelado = lista_tipo_movimiento_efectivo.ElementAt(x).val_02;
                    total_item.total = lista_tipo_movimiento_efectivo.ElementAt(x).val_03;
                    if (x == 1)
                    {
                        total_ltp_01 -= total_item.facturado;
                        total_ltp_02 -= total_item.cancelado;
                        total_ltp_03 -= total_item.total;
                        Totales_Finales[0] -= lista_tipo_movimiento_efectivo.ElementAt(x).val_03;
                    }
                    else
                    {
                        total_ltp_01 += total_item.facturado;
                        total_ltp_02 += total_item.cancelado;
                        total_ltp_03 += total_item.total;
                        Totales_Finales[0] += lista_tipo_movimiento_efectivo.ElementAt(x).val_03;
                    }

                    TotalesVentas.Add(total_item);
                }
                /* Para la suma de todos los totales --> movimientos efectivo */
                total_total = new TotalVenta();
                total_total.tipo_pago = "Total";
                total_total.facturado = total_ltp_01;
                total_total.cancelado = total_ltp_02;
                total_total.total = total_ltp_03;
                TotalFacturado += total_ltp_01;
                TotalCancelado += total_ltp_02;
                TotalesVentas.Add(total_total);
                ViewBag.TotalesMovimientosEfectivo = TotalesVentas;
                List<movimiento_efectivo> ListaMovimientosEfectivoActivos = db.movimiento_efectivo.Where(x => (x.estatus == true && x.fecha.Value <= fechaFin && x.fecha.Value >= fechaInicio) && x.id_sucursal == id_sucursal).OrderBy(x => x.id).ToList();
                List<movimiento_efectivo> ListaMovimientosEfectivoCancelados = db.movimiento_efectivo.Where(x => (x.estatus == false && x.fecha_cancelacion.Value <= fechaFin && x.fecha_cancelacion.Value >= fechaInicio) && x.id_sucursal == id_sucursal).OrderBy(x => x.id).ToList();
            }
            List<nota_credito> ListaNotasCredito = db.nota_credito.Where(x => ((x.id_estatus_nota_credito == 1 && x.fecha_creacion <= fechaFin && x.fecha_creacion >= fechaInicio) || (x.id_estatus_nota_credito == 2 && x.fecha_uso <= fechaFin && x.fecha_uso >= fechaInicio) || (x.id_estatus_nota_credito == 3 && x.fecha_cancelacion <= fechaFin && x.fecha_cancelacion >= fechaInicio)) && x.id_sucursal == id_sucursal).ToList();
            List<nota_credito> ListaNotasCreditoCreadas = ListaNotasCredito.Where(x => x.id_estatus_nota_credito == 1 && x.fecha_creacion <= fechaFin && x.fecha_creacion >= fechaInicio && x.id_sucursal == id_sucursal).ToList();
            List<nota_credito> ListaNotasCreditoCobradas = ListaNotasCredito.Where(x => x.id_estatus_nota_credito == 2 && x.fecha_uso <= fechaFin && x.fecha_uso >= fechaInicio && x.id_sucursal == id_sucursal).ToList();
            List<nota_credito> ListaNotasCreditoCanceladas = ListaNotasCredito.Where(x => x.id_estatus_nota_credito == 3 && x.fecha_cancelacion <= fechaFin && x.fecha_cancelacion >= fechaInicio && x.id_sucursal == id_sucursal).ToList();
            ViewBag.NotasCreditoPendientes = ListaNotasCreditoCreadas.Select(x => x.cantidad).Sum();
            ViewBag.NotasCreditoCobradas = ListaNotasCreditoCobradas.Select(x => x.cantidad).Sum();
            ViewBag.NotasCreditocanceladas = ListaNotasCreditoCanceladas.Select(x => x.cantidad).Sum();
            ViewBag.ListaVentas = ListaVentas;
            ViewBag.ListaAbonos = ListaAbonos;
            if (movimientos_efectivo)
                ViewBag.ListaMovimientosEfectivo = ListaMovimientosEfectivo;
            ViewBag.ListaNotasCredito = ListaNotasCredito;
            ViewBag.ListaTiposPagosVenta = db.tipo_pago_venta.Where(x => x.id != 4).ToList();
            ViewBag.Totales_Finales = Totales_Finales;
            Decimal GranTotal = 0;
            for (int x = 0; x < Totales_Finales.Count; x++)
            {
                GranTotal += Totales_Finales.ElementAt(x);
            }
            //ViewBag.GranTotal = GranTotal - ListaNotasCreditoCobradas.Select(x => x.cantidad).Sum();
            ViewBag.GranTotal = GranTotal;
            ViewBag.TotalFacturado = TotalFacturado;
            ViewBag.TotalCancelado = TotalCancelado;
            ViewBag.Total = TotalFacturado - TotalCancelado;
            ViewBag.fechaInicio = fechaInicio;
            ViewBag.fechaFin = fechaFin;
            ViewBag.id_sucursal = id_sucursal;
            ViewBag.movimientos_efectivo = movimientos_efectivo;
            return PartialView();
        }

        [HttpPost]
        public ActionResult NuevoCorteCaja(corte_caja corte_caja)
        {
            try
            {
                db.corte_caja.Add(corte_caja);
                db.SaveChanges();
                var respuesta = new
                {
                    success = true
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                var respuesta = new
                {
                    success = false
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
        }

        public ViewResult DetalleCorteCaja(int id)
        {
            corte_caja c = db.corte_caja.Find(id);
            DateTime fechaInicio = Convert.ToDateTime(c.fechaInicio.Value, IdiomaFecha);
            DateTime fechaFin = Convert.ToDateTime(c.fechaFin.Value, IdiomaFecha);
            configuracion_pos configuracion_pos = db.configuracion_pos.FirstOrDefault();
            bool movimientos_efectivo = false;
            if (configuracion_pos != null)
                movimientos_efectivo = (bool)configuracion_pos.movimientos_efectivo;
            List<venta> ListaVentas = db.venta.Where(x => ((x.catalogo_status_id == 1 && x.Fecha_entrega >= fechaInicio && x.Fecha_entrega < fechaFin)
                                                          || (x.catalogo_status_id == 9 && x.fecha_liquidacion >= fechaInicio && x.fecha_liquidacion <= fechaFin)
                                                          || (x.catalogo_status_id == 10 && x.fecha_liquidacion >= fechaInicio && x.fecha_liquidacion <= fechaFin)
                                                          || (x.catalogo_status_id == 11 && x.fecha_liquidacion >= fechaInicio && x.fecha_liquidacion <= fechaFin)
                                                          || (x.catalogo_status_id == 2 && x.fecha_cancelacion >= fechaInicio && x.fecha_cancelacion <= fechaFin)
                                                          || (x.catalogo_status_id == 12 && x.fecha_cancelacion >= fechaInicio && x.fecha_cancelacion <= fechaFin && x.fecha_liquidacion != null))
                                                          && x.sucursal_id == c.id_sucursal).OrderBy(x => x.Folio).ToList();
            List<Decimal> Totales_Finales = new List<Decimal>();
            Decimal TotalFacturado = 0;
            Decimal TotalCancelado = 0;
            for (int x = 0; x < db.tipo_pago_venta.ToList().Count; x++)
            {
                Totales_Finales.Add(0);
            }
            /*
             * Esta lista contiene:
             * val_01 -> VentaActiva
             * val_02 -> VentaCancelada
             * val_03 -> VentaTotal
             */
            List<ClaseAux01> lista_venta_tipo = new List<ClaseAux01>();
            foreach (tipo_pago_venta item in db.tipo_pago_venta.ToList())
            {
                ClaseAux01 ca = new ClaseAux01(item.tipo);
                lista_venta_tipo.Add(ca);
            }
            List<TotalVenta> TotalesVentas = new List<TotalVenta>();
            foreach (venta venta in ListaVentas)
            {
                foreach (multiple_pago multiple_pago in venta.multiple_pago.ToList())
                {
                    for (int x = 0; x < lista_venta_tipo.Count; x++)
                    {
                        if (multiple_pago.id_tipo_pago_venta == (x + 1))
                        {
                            lista_venta_tipo.ElementAt(x).val_01 += multiple_pago.cantidad.Value;
                        }
                    }
                    if (venta.catalogo_status_id == 2 || venta.catalogo_status_id == 12)
                    {
                        for (int x = 0; x < lista_venta_tipo.Count; x++)
                        {
                            if (multiple_pago.id_tipo_pago_venta == (x + 1))
                            {
                                lista_venta_tipo.ElementAt(x).val_02 += multiple_pago.cantidad.Value;
                            }
                        }
                    }
                }
            }
            for (int x = 0; x < lista_venta_tipo.Count; x++)
            {
                lista_venta_tipo.ElementAt(x).val_03 = lista_venta_tipo.ElementAt(x).val_01 - lista_venta_tipo.ElementAt(x).val_02;
            }
            Decimal total_ltp_01, total_ltp_02, total_ltp_03;
            total_ltp_01 = total_ltp_02 = total_ltp_03 = 0;
            for (int x = 0; x < lista_venta_tipo.Count; x++)
            {
                TotalVenta total_item = new TotalVenta();
                total_item.tipo_pago = lista_venta_tipo.ElementAt(x).tipo;
                total_item.facturado = lista_venta_tipo.ElementAt(x).val_01;
                total_item.cancelado = lista_venta_tipo.ElementAt(x).val_02; ;
                total_item.total = lista_venta_tipo.ElementAt(x).val_03;
                total_ltp_01 += total_item.facturado;
                total_ltp_02 += total_item.cancelado;
                total_ltp_03 += total_item.total;
                Totales_Finales[x] += lista_venta_tipo.ElementAt(x).val_03;
                TotalesVentas.Add(total_item);
            }
            /* Para la suma de todos los totales */
            TotalVenta total_total = new TotalVenta();
            total_total.tipo_pago = "Total";
            total_total.facturado = total_ltp_01;
            total_total.cancelado = total_ltp_02;
            total_total.total = total_ltp_03;
            TotalFacturado += total_ltp_01;
            TotalCancelado += total_ltp_02;
            TotalesVentas.Add(total_total);
            ViewBag.TotalesVentas = TotalesVentas;
            List<venta> ListaVentasActivas = db.venta.Where(x => ((x.catalogo_status_id == 1 && x.Fecha_entrega >= fechaInicio && x.Fecha_entrega <= fechaFin)
                                                                 || (x.catalogo_status_id == 9 && x.fecha_liquidacion >= fechaInicio && x.fecha_liquidacion <= fechaFin)
                                                                 || (x.catalogo_status_id == 10 && x.fecha_liquidacion >= fechaInicio && x.fecha_liquidacion <= fechaFin)
                                                                 || (x.catalogo_status_id == 11 && x.fecha_liquidacion >= fechaInicio && x.fecha_liquidacion <= fechaFin))
                                                                 && x.sucursal_id == c.id_sucursal).OrderBy(x => x.Folio).ToList();
            List<venta> ListaVentasCanceladas = db.venta.Where(x => ((x.catalogo_status_id == 2 && x.fecha_cancelacion >= fechaInicio && x.fecha_cancelacion <= fechaFin)
                                                                    || (x.catalogo_status_id == 12 && x.fecha_cancelacion >= fechaInicio && x.fecha_cancelacion <= fechaFin && x.fecha_liquidacion != null))
                                                                    && x.sucursal_id == c.id_sucursal).OrderBy(x => x.Folio).ToList();
            List<abono> ListaAbonos = db.abono.Where(x => (x.estatus == true && x.fecha.Value <= fechaFin && x.fecha.Value >= fechaInicio) || (x.estatus == false && x.fecha_cancelacion.Value <= fechaFin && x.fecha_cancelacion.Value >= fechaInicio)).OrderBy(x => x.id).ToList();
            List<ClaseAux01> lista_tipo_abono = new List<ClaseAux01>();
            foreach (tipo_pago_abono item in db.tipo_pago_abono.ToList())
            {
                ClaseAux01 ca = new ClaseAux01(item.tipo);
                lista_tipo_abono.Add(ca);
            }
            TotalesVentas = new List<TotalVenta>();
            foreach (abono abono in ListaAbonos)
            {

                for (int x = 0; x < lista_tipo_abono.Count; x++)
                {
                    if (abono.Tipo_pago_abono_id == (x + 1))
                    {
                        lista_tipo_abono.ElementAt(x).val_01 += abono.importe.Value;
                    }
                }
                if (!abono.estatus)
                {
                    for (int x = 0; x < lista_tipo_abono.Count; x++)
                    {
                        if (abono.Tipo_pago_abono_id == (x + 1))
                        {
                            lista_tipo_abono.ElementAt(x).val_02 += abono.importe.Value;
                        }
                    }
                }
            }
            for (int x = 0; x < lista_tipo_abono.Count; x++)
            {
                lista_tipo_abono.ElementAt(x).val_03 = lista_tipo_abono.ElementAt(x).val_01 - lista_tipo_abono.ElementAt(x).val_02;
            }
            total_ltp_01 = total_ltp_02 = total_ltp_03 = 0;
            for (int x = 0; x < lista_tipo_abono.Count; x++)
            {
                TotalVenta total_item = new TotalVenta();
                total_item.tipo_pago = lista_tipo_abono.ElementAt(x).tipo;
                total_item.facturado = lista_tipo_abono.ElementAt(x).val_01;
                total_item.cancelado = lista_tipo_abono.ElementAt(x).val_02; ;
                total_item.total = lista_tipo_abono.ElementAt(x).val_03;
                total_ltp_01 += total_item.facturado;
                total_ltp_02 += total_item.cancelado;
                total_ltp_03 += total_item.total;
                Totales_Finales[x] += lista_tipo_abono.ElementAt(x).val_03;
                TotalesVentas.Add(total_item);
            }
            /* Para la suma de todos los totales */
            total_total = new TotalVenta();
            total_total.tipo_pago = "Total";
            total_total.facturado = total_ltp_01;
            total_total.cancelado = total_ltp_02;
            total_total.total = total_ltp_03;
            TotalFacturado += total_ltp_01;
            TotalCancelado += total_ltp_02;
            TotalesVentas.Add(total_total);
            ViewBag.TotalesAbonos = TotalesVentas;
            List<abono> ListaAbonosActivos = db.abono.Where(x => (x.estatus == true && x.fecha.Value <= fechaFin && x.fecha.Value >= fechaInicio)).OrderBy(x => x.id).ToList();
            List<abono> ListaAbonosCancelados = db.abono.Where(x => (x.estatus == false && x.fecha_cancelacion.Value <= fechaFin && x.fecha_cancelacion.Value >= fechaInicio)).OrderBy(x => x.id).ToList();
            /* *** *** *** *** *** *** *** *** *** *** *** Lista Movimientos Efectivo  *** *** *** *** *** *** *** *** *** *** *** */
            /* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
            List<movimiento_efectivo> ListaMovimientosEfectivo = null;
            if (movimientos_efectivo)
            {
                ListaMovimientosEfectivo = db.movimiento_efectivo.Where(x => ((x.estatus == true && x.fecha.Value <= fechaFin && x.fecha.Value >= fechaInicio) || (x.estatus == false && x.fecha_cancelacion.Value <= fechaFin && x.fecha_cancelacion.Value >= fechaInicio)) && x.id_sucursal == c.id_sucursal).OrderBy(x => x.id).ToList();
                List<ClaseAux01> lista_tipo_movimiento_efectivo = new List<ClaseAux01>();
                foreach (tipo_movimiento_efectivo item in db.tipo_movimiento_efectivo.ToList())
                {
                    ClaseAux01 ca = new ClaseAux01(item.nombre);
                    lista_tipo_movimiento_efectivo.Add(ca);
                }
                TotalesVentas = new List<TotalVenta>();
                foreach (movimiento_efectivo movimiento_efectivo in ListaMovimientosEfectivo)
                {

                    for (int x = 0; x < lista_tipo_movimiento_efectivo.Count; x++)
                    {
                        if (movimiento_efectivo.id_tipo_movimiento_efectivo == (x + 1))
                        {
                            lista_tipo_movimiento_efectivo.ElementAt(x).val_01 += movimiento_efectivo.monto;
                        }
                    }
                    if (!movimiento_efectivo.estatus)
                    {
                        for (int x = 0; x < lista_tipo_movimiento_efectivo.Count; x++)
                        {
                            if (movimiento_efectivo.id_tipo_movimiento_efectivo == (x + 1))
                            {
                                lista_tipo_movimiento_efectivo.ElementAt(x).val_02 += movimiento_efectivo.monto;
                            }
                        }
                    }
                }
                for (int x = 0; x < lista_tipo_movimiento_efectivo.Count; x++)
                {
                    lista_tipo_movimiento_efectivo.ElementAt(x).val_03 = lista_tipo_movimiento_efectivo.ElementAt(x).val_01 - lista_tipo_movimiento_efectivo.ElementAt(x).val_02;
                }
                total_ltp_01 = total_ltp_02 = total_ltp_03 = 0;
                for (int x = 0; x < lista_tipo_movimiento_efectivo.Count; x++)
                {
                    TotalVenta total_item = new TotalVenta();
                    total_item.tipo_pago = lista_tipo_movimiento_efectivo.ElementAt(x).tipo;
                    total_item.facturado = lista_tipo_movimiento_efectivo.ElementAt(x).val_01;
                    total_item.cancelado = lista_tipo_movimiento_efectivo.ElementAt(x).val_02;
                    total_item.total = lista_tipo_movimiento_efectivo.ElementAt(x).val_03;
                    if (x == 1)
                    {
                        total_ltp_01 -= total_item.facturado;
                        total_ltp_02 -= total_item.cancelado;
                        total_ltp_03 -= total_item.total;
                        Totales_Finales[0] -= lista_tipo_movimiento_efectivo.ElementAt(x).val_03;
                    }
                    else
                    {
                        total_ltp_01 += total_item.facturado;
                        total_ltp_02 += total_item.cancelado;
                        total_ltp_03 += total_item.total;
                        Totales_Finales[0] += lista_tipo_movimiento_efectivo.ElementAt(x).val_03;
                    }

                    TotalesVentas.Add(total_item);
                }
                /* Para la suma de todos los totales --> movimientos efectivo */
                total_total = new TotalVenta();
                total_total.tipo_pago = "Total";
                total_total.facturado = total_ltp_01;
                total_total.cancelado = total_ltp_02;
                total_total.total = total_ltp_03;
                TotalFacturado += total_ltp_01;
                TotalCancelado += total_ltp_02;
                TotalesVentas.Add(total_total);
                ViewBag.TotalesMovimientosEfectivo = TotalesVentas;
                List<movimiento_efectivo> ListaMovimientosEfectivoActivos = db.movimiento_efectivo.Where(x => (x.estatus == true && x.fecha.Value <= fechaFin && x.fecha.Value >= fechaInicio) && x.id_sucursal == c.id_sucursal).OrderBy(x => x.id).ToList();
                List<movimiento_efectivo> ListaMovimientosEfectivoCancelados = db.movimiento_efectivo.Where(x => (x.estatus == false && x.fecha_cancelacion.Value <= fechaFin && x.fecha_cancelacion.Value >= fechaInicio) && x.id_sucursal == c.id_sucursal).OrderBy(x => x.id).ToList();
            }
            List<nota_credito> ListaNotasCredito = db.nota_credito.Where(x => (x.id_estatus_nota_credito == 1 && x.fecha_creacion <= fechaFin && x.fecha_creacion >= fechaInicio) || (x.id_estatus_nota_credito == 2 && x.fecha_uso <= fechaFin && x.fecha_uso >= fechaInicio) || (x.id_estatus_nota_credito == 3 && x.fecha_cancelacion <= fechaFin && x.fecha_cancelacion >= fechaInicio)).ToList();
            List<nota_credito> ListaNotasCreditoCreadas = ListaNotasCredito.Where(x => x.id_estatus_nota_credito == 1 && x.fecha_creacion <= fechaFin && x.fecha_creacion >= fechaInicio).ToList();
            List<nota_credito> ListaNotasCreditoCobradas = ListaNotasCredito.Where(x => x.id_estatus_nota_credito == 2 && x.fecha_uso <= fechaFin && x.fecha_uso >= fechaInicio).ToList();
            List<nota_credito> ListaNotasCreditoCanceladas = ListaNotasCredito.Where(x => x.id_estatus_nota_credito == 3 && x.fecha_cancelacion <= fechaFin && x.fecha_cancelacion >= fechaInicio).ToList();
            ViewBag.NotasCreditoPendientes = ListaNotasCreditoCreadas.Select(x => x.cantidad).Sum();
            ViewBag.NotasCreditoCobradas = ListaNotasCreditoCobradas.Select(x => x.cantidad).Sum();
            ViewBag.NotasCreditocanceladas = ListaNotasCreditoCanceladas.Select(x => x.cantidad).Sum();
            ViewBag.ListaVentas = ListaVentas;
            ViewBag.ListaAbonos = ListaAbonos;
            if (movimientos_efectivo)
                ViewBag.ListaMovimientosEfectivo = ListaMovimientosEfectivo;
            ViewBag.ListaNotasCredito = ListaNotasCredito;
            ViewBag.ListaTiposPagosVenta = db.tipo_pago_venta.ToList();
            ViewBag.Totales_Finales = Totales_Finales;
            Decimal GranTotal = 0;
            for (int x = 0; x < Totales_Finales.Count; x++)
            {
                GranTotal += Totales_Finales.ElementAt(x);
            }
            //ViewBag.GranTotal = GranTotal - ListaNotasCreditoCobradas.Select(x => x.cantidad).Sum();
            ViewBag.GranTotal = GranTotal;
            ViewBag.TotalFacturado = TotalFacturado;
            ViewBag.TotalCancelado = TotalCancelado;
            ViewBag.Total = TotalFacturado - TotalCancelado;
            ViewBag.fechaInicio = fechaInicio;
            ViewBag.fechaFin = fechaFin;
            ViewBag.movimientos_efectivo = movimientos_efectivo;
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }

    public class ClaseAux01
    {
        public String tipo { get; set; }
        public Decimal val_01 { get; set; }
        public Decimal val_02 { get; set; }
        public Decimal val_03 { get; set; }
        public ClaseAux01(String tipo)
        {
            this.tipo = tipo;
            this.val_01 = 0;
            this.val_02 = 0;
            this.val_03 = 0;
        }
    }

}