﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using POS.Models;
using System.Data.Entity;
using POS.Fn;
using System.Globalization;

namespace POS.Controllers
{
    public class NotaCreditoController : Controller
    {

        moragas_pruebasEntities1 db = new moragas_pruebasEntities1();
        CultureInfo IdiomaFecha = new CultureInfo("es-MX");

        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName = "Historial De Notas De Credito")]
        public ActionResult Index()
        {
            ViewBag.HoyInicio = DateTime.Now.ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.HoyFin = DateTime.Now.ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.AyerInicio = DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.AyerFin = DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.AntierInicio = DateTime.Now.AddDays(-2).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.AntierFin = DateTime.Now.AddDays(-2).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.SemanaActualInicio = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.SemanaActualFin = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(6).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.SemanaPasadaInicio = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(-7).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.SemanaPasadaFin = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(-7).AddDays(6).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.SemanaAntepasadaInicio = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(-14).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.SemanaAntepasadaFin = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(-14).AddDays(6).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            DateTime hoy = Convert.ToDateTime(DateTime.Now.ToShortDateString());
            int numero_de_dia = hoy.Day;
            if (numero_de_dia > 15)
            //segunda quincena
            {
                ViewBag.QuincenaActualInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 15).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaActualFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.Month)).ToString("MM/dd/yyyy") + " 11:59:59 PM";
                ViewBag.QuincenaPasadaInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaPasadaFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddDays(15).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
                ViewBag.QuincenaAntepasadaInicio = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, 15).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaAntepasadaFin = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month)).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            }
            else //primera quincena
            {
                ViewBag.QuincenaActualInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaActualFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddDays(15).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
                ViewBag.QuincenaPasadaInicio = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, 15).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaPasadaFin = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month)).ToString("MM/dd/yyyy") + " 11:59:59 PM";
                ViewBag.QuincenaAntepasadaInicio = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaAntepasadaFin = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, 1).AddDays(15).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            }
            ViewBag.MesActualInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.MesActualFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(1).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.MesPasadoInicioFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.MesPasadoFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-1).AddMonths(1).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.MesAntepasadoInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-2).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.MesAntepasadoFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-2).AddMonths(1).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.id_sucursal = new SelectList(db.sucursal.OrderByDescending(y => y.id), "id", "nombre_sucursal");
            ViewBag.id_estatus_nota_credito = new SelectList(db.estatus_nota_credito, "id", "nombre");
            return View();
        }

        public PartialViewResult VistaParcialNotaCredito(int id_sucursal, int id_estatus_nota_credito, String periodo)
        {
            List<nota_credito> ListaNotasCredito = db.nota_credito.ToList();
            if (id_sucursal != 0)
                ListaNotasCredito = ListaNotasCredito.Where(x => x.venta.sucursal_id == id_sucursal).ToList();
            if (id_estatus_nota_credito != 0)
                ListaNotasCredito = ListaNotasCredito.Where(x => x.id_estatus_nota_credito == id_estatus_nota_credito).ToList();
            if (!periodo.Equals(""))
            {
                String[] fecha = Fn.Fechas.CalcularIncioFin(periodo);
                String inicio = fecha[0];
                String fin = fecha[1];

                DateTime _inicio = Convert.ToDateTime(inicio, IdiomaFecha);
                DateTime _fin = Convert.ToDateTime(fin, IdiomaFecha);
                ListaNotasCredito = ListaNotasCredito.Where(x => x.fecha_creacion > _inicio && x.fecha_creacion < _fin).ToList();
            }
            return PartialView(ListaNotasCredito);
        }

        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName="Crear Nota De Credito")]
        public ActionResult NuevaNotaCredito()
        {
            return View();
        }

        public PartialViewResult VistaParcialNuevaNotaCredito(String id_venta)
        {
            try
            {
                String[] datos = id_venta.Split('-');
                String serie = datos[0];
                String folio_con_ceros = datos[1];
                String folio_sin_ceros = "";
                Boolean ignorar = false;
                for (int i = 0; i < 5; i++)
                {
                    if (!ignorar)
                    {
                        if (folio_con_ceros.ElementAt(i) != '0')
                        {
                            folio_sin_ceros = folio_sin_ceros + folio_con_ceros.ElementAt(i);
                            ignorar = true;
                        }
                    }
                    else
                        folio_sin_ceros = folio_sin_ceros + folio_con_ceros.ElementAt(i);
                }
                int folio = Convert.ToInt32(folio_sin_ceros);
                venta venta = db.venta.Where(x => x.Folio == folio && x.Serie.ToLower().Equals(serie.ToLower())).Single();
                if (venta.catalogo_status_id == 2 || venta.catalogo_status_id == 12)
                {
                    ViewBag.correcto = -1;
                    return PartialView();
                }
                else
                {
                    List<conceptos_por_venta> ListaConceptosVenta = venta.conceptos_por_venta.ToList();
                    List<nota_credito> ListaNotasCredito = venta.nota_credito.ToList();
                    List<int> ListaIdsNotasCredito = venta.nota_credito.Where(x => x.id_estatus_nota_credito != 3).Select(x => x.id).ToList();
                    List<concepto_por_nota_credito> ListaConceptosNotaCredito = db.concepto_por_nota_credito.Where(x => ListaIdsNotasCredito.Contains(x.id_nota_credito)).ToList();
                    foreach (conceptos_por_venta conceptos_por_venta in ListaConceptosVenta)
                    {
                        foreach (concepto_por_nota_credito concepto_por_nota_credito in ListaConceptosNotaCredito)
                        {
                            if (conceptos_por_venta.id == concepto_por_nota_credito.id_conceptos_por_venta)
                                conceptos_por_venta.cantidad -= concepto_por_nota_credito.cantidad.Value;
                        }
                    }
                    /* *** *** *** *** *** *** *** ***  Buscar si hay algun descuento aplicado a esta venta *** *** *** *** *** *** */
                    descuento descuento = db.descuento.FirstOrDefault(x => x.id_venta == venta.id && x.monto > 0);
                    if (descuento != null)
                    {
                        ViewBag.existe_descuento = true;
                        ViewBag.descuento = descuento;
                    }
                    else
                        ViewBag.existe_descuento = false;



                    ViewBag.Venta = venta;
                    ViewBag.ListaConceptos = ListaConceptosVenta;
                    ViewBag.ListaNotasCredito = ListaNotasCredito;
                    ViewBag.NumeroNotasCredito = ListaNotasCredito.Count;
                    ViewBag.correcto = 1;
                    return PartialView();
                }
            }
            catch
            {
                ViewBag.correcto = 0;
                return PartialView();
            }
        }

        [HttpPost]
        public ActionResult NuevaNotaCredito(nota_credito nota_credito, List<conceptos_por_venta> ListaConceptos)
        {
            Decimal cantidad = 0;
            venta venta = db.venta.Find(nota_credito.id_venta);
            movimiento_inventario movimiento_inventario = new movimiento_inventario();
            movimiento_inventario.fecha = DateTime.Now;
            movimiento_inventario.recibe = venta.sucursal.nombre_sucursal;// Fn.usuario_logeado.traerUsuario().sucursal.nombre_sucursal;
            movimiento_inventario.usuario_valida_id = Fn.usuario_logeado.traerUsuario().id;
            movimiento_inventario.tipo_movimiento_inventario_id = 5;//3- entrada por cancelación
            movimiento_inventario.sucursal_id = venta.sucursal_id;
            db.movimiento_inventario.Add(movimiento_inventario);
            db.SaveChanges();
            foreach (conceptos_por_venta concepto in ListaConceptos)
            {
                conceptos_por_venta concepto_venta = db.conceptos_por_venta.Find(concepto.id);
                inventario inventario = db.inventario.Where(x => x.Producto_id == concepto_venta.Producto_id && x.sucursal_id == venta.sucursal_id).First();
                inventario.cantidad = inventario.cantidad + concepto.cantidad;
                db.Entry(inventario).State = System.Data.Entity.EntityState.Modified;
                conceptos_por_movimiento_inventario conceptos_por_movimiento_inventario = new conceptos_por_movimiento_inventario();
                conceptos_por_movimiento_inventario.movimiento_inventario_id = movimiento_inventario.id;
                conceptos_por_movimiento_inventario.Inventario_id = inventario.id;
                conceptos_por_movimiento_inventario.cantidad = concepto.cantidad;
                db.conceptos_por_movimiento_inventario.Add(conceptos_por_movimiento_inventario);
                cantidad = cantidad + Math.Round(((concepto.cantidad * concepto_venta.precio_unitario_mas_iva.Value)), 2);
                db.SaveChanges();
            }
            string Serie_de_nota_de_credito = Config.Serie_de_nota_de_credito();
            nota_credito.fecha_creacion = DateTime.Now;
            nota_credito.id_estatus_nota_credito = 1;
            nota_credito.serie = Config.Serie_de_nota_de_credito();
            nota_credito.folio = Convert.ToInt32(db.nota_credito.Where(x => x.serie.Equals(Serie_de_nota_de_credito) && x.venta.sucursal_id == nota_credito.id_sucursal).Select(x => x.folio).Max()) + 1;
            nota_credito.cantidad = cantidad;
            nota_credito.id_usuario_punto_venta = Fn.usuario_logeado.traerUsuario().id;
            db.nota_credito.Add(nota_credito);
            db.SaveChanges();
            foreach (conceptos_por_venta concepto in ListaConceptos)
            {
                conceptos_por_venta concepto_venta = db.conceptos_por_venta.Find(concepto.id);
                concepto_por_nota_credito concepto_por_nota_credito = new concepto_por_nota_credito();
                concepto_por_nota_credito.id_nota_credito = nota_credito.id;
                concepto_por_nota_credito.id_conceptos_por_venta = concepto_venta.id;
                concepto_por_nota_credito.cantidad = concepto.cantidad;
                db.concepto_por_nota_credito.Add(concepto_por_nota_credito);
            }
            db.SaveChanges();
            var respuesta = new
            {
                id = nota_credito.id
            };
            return Json(respuesta, JsonRequestBehavior.AllowGet);
        }
        
        [AutorizacionVistaUsuarioActivo(displayName = "Cancelar Notas De Credito")]
        public void CancelarNotaCredito(int id)
        {
            nota_credito nota_credito = db.nota_credito.Find(id);
            if (nota_credito.id_estatus_nota_credito != 3)
            {
                nota_credito.fecha_cancelacion = DateTime.Now;
                nota_credito.id_estatus_nota_credito = 3;
                db.Entry(nota_credito).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                venta venta = db.venta.Find(nota_credito.id_venta);
                movimiento_inventario movimiento_inventario = new movimiento_inventario();
                movimiento_inventario.fecha = DateTime.Now;
                movimiento_inventario.recibe = venta.cliente.Razon_social;
                movimiento_inventario.usuario_valida_id = Fn.usuario_logeado.traerUsuario().id;
                movimiento_inventario.tipo_movimiento_inventario_id = 6;
                movimiento_inventario.sucursal_id = venta.sucursal_id;
                db.movimiento_inventario.Add(movimiento_inventario);
                db.SaveChanges();
                List<concepto_por_nota_credito> ListaConceptosNotaCredito = db.concepto_por_nota_credito.Where(x => x.id_nota_credito == nota_credito.id).ToList();
                foreach (concepto_por_nota_credito concepto_por_nota_credito in ListaConceptosNotaCredito)
                {
                    conceptos_por_venta concepto_venta = db.conceptos_por_venta.Find(concepto_por_nota_credito.id_conceptos_por_venta);
                    inventario inventario = db.inventario.Where(x => x.Producto_id == concepto_venta.Producto_id && x.sucursal_id == venta.sucursal_id).First();
                    inventario.cantidad = inventario.cantidad - concepto_por_nota_credito.cantidad;
                    db.Entry(inventario).State = System.Data.Entity.EntityState.Modified;
                    conceptos_por_movimiento_inventario conceptos_por_movimiento_inventario = new conceptos_por_movimiento_inventario();
                    conceptos_por_movimiento_inventario.movimiento_inventario_id = movimiento_inventario.id;
                    conceptos_por_movimiento_inventario.Inventario_id = inventario.id;
                    conceptos_por_movimiento_inventario.cantidad = concepto_por_nota_credito.cantidad.Value;
                    db.conceptos_por_movimiento_inventario.Add(conceptos_por_movimiento_inventario);
                }
                db.SaveChanges();
            }
        }

        public PartialViewResult VistaParcialGenerarTicketNotaCredito(int id)
        {
            nota_credito nota = db.nota_credito.Find(id);
            return PartialView(db.nota_credito.Find(id));
        }

        public PartialViewResult ModalCargandoPDF()
        {
            return PartialView();
        }

        public void GenerarPDFNotaCredito(int id)
        {
            Uri Request_Url = Request.Url;
            var current_action = RouteData.Values["action"].ToString();

            string URL = "";

            //factura activa
            URL = Request_Url.OriginalString.Replace(current_action, "VistaParcialPDFNotaCredito");
            URL += "?id=" + id;

            SelectPdf.PdfPageSize pageSize = (SelectPdf.PdfPageSize)Enum.Parse(typeof(SelectPdf.PdfPageSize), "A4", true);
            SelectPdf.PdfPageOrientation pdfOrientation = (SelectPdf.PdfPageOrientation)Enum.Parse(typeof(SelectPdf.PdfPageOrientation), "Portrait", true);
            // instantiate a html to pdf converter object
            SelectPdf.HtmlToPdf _converter = new SelectPdf.HtmlToPdf();
            // set converter options
            _converter.Options.PdfPageSize = pageSize;
            _converter.Options.PdfPageOrientation = pdfOrientation;
            _converter.Options.WebPageWidth = 1280;
            _converter.Options.WebPageHeight = 0;
            // create a new pdf document converting an url
            SelectPdf.PdfDocument doc = _converter.ConvertUrl(URL);
            doc.Save(System.Web.HttpContext.Current.Response, false, Fn.Numeros.serie_folio_by_id_nota_credito(id) + ".pdf");
            
        }

        public PartialViewResult VistaParcialPDFNotaCredito(int id)
        {
            nota_credito nota_credito = db.nota_credito.Find(id);
            return PartialView(nota_credito);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
