﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using POS.Models;
using System.Data.Entity;
using POS.Fn;

namespace POS.Controllers
{
    public class SubfamiliaController : Controller
    {

        moragas_pruebasEntities1 db = new moragas_pruebasEntities1();

        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName = "Catalogo De Subfamilias")]
        public ActionResult Index()
        {
            ViewBag.subfamilias = db.subfamilia.ToList();
            return View();
        }

        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName = "Crear Subfamilia")]
        public ActionResult NuevaSubfamilia()
        {
            ViewBag.Familia_id = new SelectList(db.familia, "id", "nombre");
            return View();
        }

        [HttpPost]
        public ActionResult NuevaSubfamilia(subfamilia subfamilia)
        {
            db.subfamilia.Add(subfamilia);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName = "Editar Subfamilia")]
        public ActionResult EditarSubfamilia(int id)
        {
            subfamilia subfamilia = db.subfamilia.Find(id);
            ViewBag.Familia_id = new SelectList(db.familia, "id", "nombre", subfamilia.Familia_id);
            return View(subfamilia);
        }

        [HttpPost]
        public ActionResult EditarSubfamilia(subfamilia subfamilia)
        {
            db.Entry(subfamilia).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public bool VerificarSubfamilia(int id, int id_familia, String subfamilia)
        {
            bool existe;
            if (id == 0)
                existe = db.subfamilia.Where(x => x.nombre.ToLower().Equals(subfamilia.ToLower()) && x.Familia_id == id_familia).Any();
            else
                existe = db.subfamilia.Where(x => x.id != id && x.nombre.ToLower().Equals(subfamilia.ToLower()) && x.Familia_id == id_familia).Any();
            return existe;
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
