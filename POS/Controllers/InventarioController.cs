﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using POS.Models;
using System.Web.Script.Serialization;
using System.Text.RegularExpressions;
using System.Data.Entity;
using POS.Fn;
using System.Globalization;

namespace POS.Controllers
{
    public class InventarioController : Controller
    {
        moragas_pruebasEntities1 db = new moragas_pruebasEntities1();
        CultureInfo IdiomaFecha = new CultureInfo("es-MX");

        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName = "Inventario")]
        public ActionResult Index()
        {
            List<int> ListaSucursalesPermitidas = Fn.usuario_logeado.Sucursales_id();
            ViewBag.id_sucursal = new SelectList(db.sucursal.Where(x => ListaSucursalesPermitidas.Contains(x.id)).OrderByDescending(y => y.id), "id", "nombre_sucursal");
            return View();
        }

        public PartialViewResult VistaParcialInventario(int id_sucursal, bool con_inventario)
        {
            List<inventario> ListaInventario = new List<inventario>();
            if(con_inventario)
                ListaInventario = db.inventario.Where(x => x.sucursal_id == id_sucursal && x.cantidad > 0).ToList();
            else
                ListaInventario = db.inventario.Where(x => x.sucursal_id == id_sucursal).ToList();
            ViewBag.ListaInventario = ListaInventario;
            return PartialView();
        }

        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName = "Nueva Salida De Inventario")]
        public ActionResult NuevaSalida()
        {
            List<int> ListaSucursalesPermitidas = Fn.usuario_logeado.Sucursales_id();
            ViewBag.id_sucursal = new SelectList(db.sucursal.Where(x => ListaSucursalesPermitidas.Contains(x.id)).OrderByDescending(y => y.id), "id", "nombre_sucursal");
            ViewBag.tipo_movimiento_inventario_id = new SelectList(db.tipo_movimiento_inventario.Where(x => x.id == 2), "id", "nombre");
            return View();
        }

        [HttpPost]
        public ActionResult NuevaSalida(movimiento_inventario MovimientoInventario, List<concepto_movimiento_inventario> ConceptoMovimientoInventario)
        {
            bool success = false;
            try
            {
                List<concepto_movimiento_inventario> ListaConceptosMovimientoInventario = new List<concepto_movimiento_inventario>();
                //DISTINTOS CODIGOS POR VENTA
                foreach (int id_producto in ConceptoMovimientoInventario.Select(x => x.id_producto).Distinct())
                {
                    List<concepto_movimiento_inventario> ListaTemporal = ConceptoMovimientoInventario.Where(x => x.id_producto == id_producto).ToList();
                    concepto_movimiento_inventario concepto_movimiento_inventario = new concepto_movimiento_inventario();
                    concepto_movimiento_inventario.cantidad = ListaTemporal.Select(x => x.cantidad).Sum();
                    concepto_movimiento_inventario.id_inventario = ListaTemporal.First().id_inventario;
                    concepto_movimiento_inventario.id_producto = ListaTemporal.First().id_producto;
                    ListaConceptosMovimientoInventario.Add(concepto_movimiento_inventario);
                }
                MovimientoInventario.fecha = DateTime.Now;
                MovimientoInventario.usuario_valida_id = Fn.usuario_logeado.traerUsuario().id;
                db.movimiento_inventario.Add(MovimientoInventario);
                foreach (concepto_movimiento_inventario concepto_movimiento_inventario in ListaConceptosMovimientoInventario)
                {
                    inventario inventario = new inventario();
                    if (concepto_movimiento_inventario.id_inventario == 0)
                    {
                        //crear inventario para producto-sucursal
                        inventario.Producto_id = concepto_movimiento_inventario.id_producto;
                        inventario.sucursal_id = MovimientoInventario.sucursal_id;
                        inventario.cantidad = 0;
                        db.inventario.Add(inventario);
                        //bajar inventario
                        inventario.cantidad = inventario.cantidad - concepto_movimiento_inventario.cantidad;
                    }
                    else
                    {
                        inventario = db.inventario.Where(x => x.Producto_id == concepto_movimiento_inventario.id_producto && x.sucursal_id == MovimientoInventario.sucursal_id).Single();
                        inventario.cantidad = inventario.cantidad - concepto_movimiento_inventario.cantidad;
                    }
                    conceptos_por_movimiento_inventario conceptos_por_movimiento_inventario = new conceptos_por_movimiento_inventario();
                    conceptos_por_movimiento_inventario.movimiento_inventario_id = MovimientoInventario.id;
                    conceptos_por_movimiento_inventario.Inventario_id = inventario.id;
                    conceptos_por_movimiento_inventario.cantidad = concepto_movimiento_inventario.cantidad;
                    db.conceptos_por_movimiento_inventario.Add(conceptos_por_movimiento_inventario);
                }
                db.SaveChanges();
                success = true;
            }
            catch (Exception)
            {
                throw;
            }
            var respuesta = new
            {
                success = success
            };
            return Json(respuesta, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName = "Nueva Entrada De Inventario")]
        public ActionResult NuevaEntrada()
        {
            List<int> ListaSucursalesPermitidas = Fn.usuario_logeado.Sucursales_id();
            ViewBag.id_sucursal = new SelectList(db.sucursal.Where(x => ListaSucursalesPermitidas.Contains(x.id)).OrderByDescending(y => y.id), "id", "nombre_sucursal");
            ViewBag.tipo_movimiento_inventario_id = new SelectList(db.tipo_movimiento_inventario.Where(x => x.id == 1 || x.id == 7), "id", "nombre");
            return View();
        }

        [HttpPost]
        public ActionResult NuevaEntrada(movimiento_inventario MovimientoInventario, List<concepto_movimiento_inventario> ConceptoMovimientoInventario)
        {
            bool success = false;
            try
            {
                List<concepto_movimiento_inventario> ListaConceptosMovimientoInventario = new List<concepto_movimiento_inventario>();
                //DISTINTOS CODIGOS POR VENTA
                foreach (int id_producto in ConceptoMovimientoInventario.Select(x => x.id_producto).Distinct())
                {
                    List<concepto_movimiento_inventario> ListaTemporal = ConceptoMovimientoInventario.Where(x => x.id_producto == id_producto).ToList();
                    concepto_movimiento_inventario concepto_movimiento_inventario = new concepto_movimiento_inventario();
                    concepto_movimiento_inventario.cantidad = ListaTemporal.Select(x => x.cantidad).Sum();
                    concepto_movimiento_inventario.id_inventario = ListaTemporal.First().id_inventario;
                    concepto_movimiento_inventario.id_producto = ListaTemporal.First().id_producto;
                    ListaConceptosMovimientoInventario.Add(concepto_movimiento_inventario);
                }
                MovimientoInventario.fecha = DateTime.Now;
                MovimientoInventario.usuario_valida_id = Fn.usuario_logeado.traerUsuario().id;
                db.movimiento_inventario.Add(MovimientoInventario);
                foreach (concepto_movimiento_inventario concepto_movimiento_inventario in ListaConceptosMovimientoInventario)
                {
                    inventario inventario = new inventario();
                    if (concepto_movimiento_inventario.id_inventario == 0)
                    {
                        //crear inventario para producto-sucursal
                        inventario.Producto_id = concepto_movimiento_inventario.id_producto;
                        inventario.sucursal_id = MovimientoInventario.sucursal_id;
                        inventario.cantidad = 0;
                        db.inventario.Add(inventario);
                        //bajar inventario
                        inventario.cantidad = inventario.cantidad + concepto_movimiento_inventario.cantidad;
                    }
                    else
                    {
                        inventario = db.inventario.Where(x => x.Producto_id == concepto_movimiento_inventario.id_producto && x.sucursal_id == MovimientoInventario.sucursal_id).Single();
                        inventario.cantidad = inventario.cantidad + concepto_movimiento_inventario.cantidad;
                    }
                    conceptos_por_movimiento_inventario conceptos_por_movimiento_inventario = new conceptos_por_movimiento_inventario();
                    conceptos_por_movimiento_inventario.movimiento_inventario_id = MovimientoInventario.id;
                    conceptos_por_movimiento_inventario.Inventario_id = inventario.id;
                    conceptos_por_movimiento_inventario.cantidad = concepto_movimiento_inventario.cantidad;
                    db.conceptos_por_movimiento_inventario.Add(conceptos_por_movimiento_inventario);
                }
                db.SaveChanges();
                success = true;
            }
            catch (Exception)
            {
                throw;
            }
            var respuesta = new
            {
                success = success
            };
            return Json(respuesta, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName = "Resumen De Entradas - Salidas")]
        public ActionResult HistorialMovimientosInventarioDetalle(String periodo)
        {
            List<int> idsucursales = Fn.usuario_logeado.Sucursales_id();
            ViewBag.id_sucursal = new SelectList(db.sucursal.Where(x => idsucursales.Contains(x.id)).OrderByDescending(y => y.id), "id", "nombre_sucursal");
            ViewBag.id_tipo_movimiento_inventario = new SelectList(db.tipo_movimiento_inventario, "id", "nombre");

            if (periodo != null)
            {
                ViewBag.inicio = periodo.Split('-')[0].Substring(0, periodo.Split('-')[0].Length - 1);
                ViewBag.fin = periodo.Split('-')[1].Substring(1);
            }
            ViewBag.HoyInicio = DateTime.Now.ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.HoyFin = DateTime.Now.ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.AyerInicio = DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.AyerFin = DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.AntierInicio = DateTime.Now.AddDays(-2).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.AntierFin = DateTime.Now.AddDays(-2).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.SemanaActualInicio = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.SemanaActualFin = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(6).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.SemanaPasadaInicio = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(-7).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.SemanaPasadaFin = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(-7).AddDays(6).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.SemanaAntepasadaInicio = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(-14).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.SemanaAntepasadaFin = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(-14).AddDays(6).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            DateTime hoy = Convert.ToDateTime(DateTime.Now.ToShortDateString());
            int numero_de_dia = hoy.Day;
            if (numero_de_dia > 15)
            //segunda quincena
            {
                ViewBag.QuincenaActualInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 15).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaActualFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.Month)).ToString("MM/dd/yyyy") + " 11:59:59 PM";
                ViewBag.QuincenaPasadaInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaPasadaFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddDays(15).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
                ViewBag.QuincenaAntepasadaInicio = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, 15).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaAntepasadaFin = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month)).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            }
            else //primera quincena
            {
                ViewBag.QuincenaActualInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaActualFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddDays(15).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
                ViewBag.QuincenaPasadaInicio = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, 15).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaPasadaFin = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month)).ToString("MM/dd/yyyy") + " 11:59:59 PM";
                ViewBag.QuincenaAntepasadaInicio = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaAntepasadaFin = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, 1).AddDays(15).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            }
            ViewBag.MesActualInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.MesActualFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(1).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.MesPasadoInicioFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.MesPasadoFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-1).AddMonths(1).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.MesAntepasadoInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-2).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.MesAntepasadoFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-2).AddMonths(1).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            return View();
        }

        [Authorize]
        [AutorizacionVistaUsuarioActivo(displayName = "Historial De Movimientos")]
        public ActionResult HistorialMovimientosInventario(String periodo)
        {
            List<int> idsucursales = Fn.usuario_logeado.Sucursales_id();
            ViewBag.id_sucursal = new SelectList(db.sucursal.Where(x => idsucursales.Contains(x.id)).OrderByDescending(y => y.id), "id", "nombre_sucursal");
            ViewBag.id_tipo_movimiento_inventario = new SelectList(db.tipo_movimiento_inventario, "id", "nombre");

            ViewBag.HoyFin = DateTime.Now.ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.AyerInicio = DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.AyerFin = DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.AntierInicio = DateTime.Now.AddDays(-2).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.AntierFin = DateTime.Now.AddDays(-2).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.SemanaActualInicio = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.SemanaActualFin = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(6).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.SemanaPasadaInicio = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(-7).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.SemanaPasadaFin = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(-7).AddDays(6).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.SemanaAntepasadaInicio = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(-14).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.SemanaAntepasadaFin = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + 1).AddDays(-14).AddDays(6).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            DateTime hoy = Convert.ToDateTime(DateTime.Now.ToShortDateString());
            int numero_de_dia = hoy.Day;
            if (numero_de_dia > 15)
            //segunda quincena
            {
                ViewBag.QuincenaActualInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 15).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaActualFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.Month)).ToString("MM/dd/yyyy") + " 11:59:59 PM";
                ViewBag.QuincenaPasadaInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaPasadaFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddDays(15).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
                ViewBag.QuincenaAntepasadaInicio = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, 15).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaAntepasadaFin = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month)).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            }
            else //primera quincena
            {
                ViewBag.QuincenaActualInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaActualFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddDays(15).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
                ViewBag.QuincenaPasadaInicio = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, 15).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaPasadaFin = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month)).ToString("MM/dd/yyyy") + " 11:59:59 PM";
                ViewBag.QuincenaAntepasadaInicio = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
                ViewBag.QuincenaAntepasadaFin = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, 1).AddDays(15).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            }
            ViewBag.MesActualInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.MesActualFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(1).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.MesPasadoInicioFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-1).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.MesPasadoFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-1).AddMonths(1).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            ViewBag.MesAntepasadoInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-2).ToString("MM/dd/yyyy") + " 12:00:00 AM";
            ViewBag.MesAntepasadoFin = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-2).AddMonths(1).AddSeconds(-1).ToString("MM/dd/yyyy") + " 11:59:59 PM";
            return View();
        }

        public PartialViewResult VistaParcialHistorialMovimientosInventario(int idSucursal, int id_tipo_movimiento, String periodo)
        {
            String[] fecha = Fn.Fechas.CalcularIncioFin(periodo);
            String inicio = fecha[0];
            String fin = fecha[1];

            List<movimiento_inventario> movimientos_inventario = new List<movimiento_inventario>();
            DateTime _inicio = Convert.ToDateTime(inicio, IdiomaFecha);
            DateTime _fin = Convert.ToDateTime(fin, IdiomaFecha);
            if(idSucursal == 0)
            {
                if(id_tipo_movimiento == 0)
                    movimientos_inventario = db.movimiento_inventario.Where(x => x.fecha > _inicio && x.fecha < _fin).ToList();
                else
                    movimientos_inventario = db.movimiento_inventario.Where(x => x.fecha > _inicio && x.fecha < _fin && x.tipo_movimiento_inventario_id == id_tipo_movimiento).ToList();
            }
            else
            {
                if(id_tipo_movimiento == 0)
                    movimientos_inventario = db.movimiento_inventario.Where(x => x.fecha > _inicio && x.fecha < _fin && x.sucursal_id == idSucursal).ToList();
                else
                    movimientos_inventario = db.movimiento_inventario.Where(x => x.fecha > _inicio && x.fecha < _fin && x.sucursal_id == idSucursal && x.tipo_movimiento_inventario_id == id_tipo_movimiento).ToList();
            }
            
            ViewBag.periodo = periodo;
            return PartialView(movimientos_inventario);
        }

        public PartialViewResult VistaParcialHistorialMovimientosInventarioDetalle(int idSucursal, int id_tipo_movimiento, String periodo)
        {
            String[] fecha = Fn.Fechas.CalcularIncioFin(periodo);
            String inicio = fecha[0];
            String fin = fecha[1];

            DateTime _inicio = Convert.ToDateTime(inicio, IdiomaFecha);
            DateTime _fin = Convert.ToDateTime(fin, IdiomaFecha);
            List<conceptos_por_movimiento_inventario> cpx = new List<conceptos_por_movimiento_inventario>();
            if (idSucursal == 0)
            {
                if(id_tipo_movimiento == 0)
                    cpx = db.conceptos_por_movimiento_inventario.Where(x => x.movimiento_inventario.fecha > _inicio && x.movimiento_inventario.fecha < _fin).ToList();
                else
                    cpx = db.conceptos_por_movimiento_inventario.Where(x => x.movimiento_inventario.fecha > _inicio && x.movimiento_inventario.fecha < _fin && x.movimiento_inventario.tipo_movimiento_inventario_id == id_tipo_movimiento).ToList();
            }
            else
            {
                if(id_tipo_movimiento == 0)
                    cpx = db.conceptos_por_movimiento_inventario.Where(x => x.movimiento_inventario.fecha > _inicio && x.movimiento_inventario.fecha < _fin && x.movimiento_inventario.sucursal_id == idSucursal).ToList();
                else
                    cpx = db.conceptos_por_movimiento_inventario.Where(x => x.movimiento_inventario.fecha > _inicio && x.movimiento_inventario.fecha < _fin && x.movimiento_inventario.sucursal_id == idSucursal && x.movimiento_inventario.tipo_movimiento_inventario_id == id_tipo_movimiento).ToList();
            }
            return PartialView(cpx);
        }

        public PartialViewResult ModalConceptosMovimientoInventario(int id)
        {
            movimiento_inventario mi = db.movimiento_inventario.Find(id);
            return PartialView(mi);
        }

        public PartialViewResult ModalConfirmarMovimientoInventario()
        {
            return PartialView();
        }

        public PartialViewResult ModalLimpiarMovimientoInventario()
        {
            return PartialView();
        }

        public string armarRenglonInventario(int idInventario)
        {
            inventario i = db.inventario.Find(idInventario);
            return "<tr>"
                + "<td>"
                + "    <label class='titulo_mediano'>" + db.sucursal.Find(i.sucursal_id).nombre_sucursal + "</label>"
                + "</td>"
                + "<td>"
                + "    <label class='titulo_mediano'>" + db.producto.Find(i.Producto_id).Codigo + "</label>"
                + "</td>"
                + "<td>"
                + "    <label class='titulo_mediano'>" + db.producto.Find(i.Producto_id).Descripcion + "</label>"
                + "</td>"
                + "<td>"
                + "    <label id='cantidad_146' class='titulo_mediano'>" + i.cantidad + "</label>"
                + "</td>"
                + "<td>"
                + "    <label class='titulo_mediano'><a class='editar_inventario' href='#' name='" + i.id + "'>editar</a></label>"
                + "</td>"
            + "</tr>";
        }

        public ActionResult DatosProducto(int id_sucursal, string codigoProducto)
        {
            Decimal disponible = 0;
            int idProducto = 0;
            int idInventario = 0;
            string descripcion = "";
            if (db.producto.Where(x => x.Codigo == codigoProducto).Any())
            {
                producto p = db.producto.Where(x => x.Codigo == codigoProducto).SingleOrDefault();
                if (p.inventario.Where(x => x.sucursal_id == id_sucursal).Any())
                {
                    disponible = p.inventario.Where(x => x.sucursal_id == id_sucursal).SingleOrDefault().cantidad.Value;
                    idInventario = p.inventario.Where(x => x.sucursal_id == id_sucursal).SingleOrDefault().id;
                }
                idProducto = p.id;
                descripcion = p.Descripcion;
            }
            var respuesta = new
            {
                disponible = disponible,
                idProducto = idProducto,
                idInventario = idInventario,
                descripcion = descripcion
            };
            return Json(respuesta, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult ModalMovimientoInventarioVacio()
        {
            return PartialView();
        }

        public ActionResult VerificarProducto(String codigo)//Para nuevo inventario
        {
            bool existe = false;
            if (db.producto.Where(x => x.Codigo.ToLower().Equals(codigo.ToLower())).Any())
                existe = true;
            var respuesta = new
            {
                existe = existe
            };
            return Json(respuesta, JsonRequestBehavior.AllowGet);
        }

        public ActionResult VerificarInventario(String codigo, int id_sucursal)//Para nuevo inventario
        {
            bool existe = false;
            bool esta_registrado = false;
            if (db.producto.Where(x => x.Codigo.ToLower().Equals(codigo.ToLower())).Any())
            {
                producto producto = db.producto.Where(x => x.Codigo.ToLower().Equals(codigo.ToLower())).FirstOrDefault();
                if (producto != null)
                {
                    esta_registrado = true;
                    if (db.inventario.Where(x => x.Producto_id == producto.id && x.sucursal_id == id_sucursal).Any())
                        existe = true;
                }
                else
                {
                    esta_registrado = false;
                }
            }
            var respuesta = new
            {
                existe = existe,
                esta_registrado = esta_registrado
            };
            return Json(respuesta, JsonRequestBehavior.AllowGet);
        }

        public void SubirArchivoInventario(string new_fileName)
        {

            //RECUPERAR ARCHIVO A SUBIR
            UploadedFile file = RetrieveFileFromRequest();
            file.Filename = new_fileName;

            string rootPath = AppDomain.CurrentDomain.BaseDirectory;
            String savedFilePath = Server.MapPath("~/ArchivosExcel/") + new_fileName;
            System.IO.File.WriteAllBytes(savedFilePath, file.Contents);
        }

        public ActionResult ModalCargaArchivoExcelInventario()// CargandoProductosVistaParcial()
        {
            return PartialView();
        }

        private UploadedFile RetrieveFileFromRequest()
        {
            string filename = null;
            string fileType = null;
            byte[] fileContents = null;
            string ID;

            if (Request.Files.Count > 0)
            { //they're uploading the old way
                var file = Request.Files[0];
                fileContents = new byte[file.ContentLength];
                fileType = file.ContentType;
                filename = file.FileName;

            }
            else if (Request.ContentLength > 0)
            {
                fileContents = new byte[Request.ContentLength];
                Request.InputStream.Read(fileContents, 0, Request.ContentLength);
                filename = Request.Headers["X-File-Name"];
                fileType = Request.Headers["X-File-Type"];
            }

            return new UploadedFile()
            {
                Filename = filename,
                ContentType = fileType,
                FileSize = fileContents != null ? fileContents.Length : 0,
                Contents = fileContents
            };
        }

        public PartialViewResult ModalLog(String tabla)
        {
            String datos = HttpUtility.UrlDecode(tabla, System.Text.Encoding.Default); ;
            ViewBag.tabla = HttpUtility.UrlDecode(tabla, System.Text.Encoding.Default).Replace('\n', ' ');
            return PartialView();
        }

        public ActionResult ModalResultadoInventarioAnalizarArchivoExcel()
        {

            //ViewBag.descripcion = db.C_tempdetallesajusteinventario.ToList();
            List<C_subirinvenarioexcel> listaInventarioActualizara = db.C_subirinvenarioexcel.Where(x => x.Producto_id!="").ToList();
            return PartialView(listaInventarioActualizara);
        }

        public ActionResult ModalListaExcelInventario(bool existe)
        {
            List<temp_movimientos_actualiza_inventario> listaMovInventario = new List<temp_movimientos_actualiza_inventario>();
            if (existe)
            {
                listaMovInventario = db.temp_movimientos_actualiza_inventario.Where(x => x.existeBool == true).ToList();
                ViewBag.titulo = "Productos No existen";
            }
            else
            {
                listaMovInventario = db.temp_movimientos_actualiza_inventario.Where(x => x.existeBool == false).ToList();
                ViewBag.titulo = "Productos Si existe";
            }
            return PartialView(listaMovInventario);
        }

        public ActionResult ModalSeleccionarParametrosActualizacion()
        {
            ViewBag.sucursales = db.sucursal.ToList();
            //ViewBag.cbx_sucursal = new SelectList(db.sucursal, "id", "nombre_sucursal");
            return PartialView();
        }

    }
}
