﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace POS.Models
{
    public class concepto_movimiento_inventario
    {
        public int id_producto { get; set; }
        public Decimal cantidad { get; set; }
        public string codigo { get; set; }
        public int id_inventario { get; set; }
    }
}