﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace POS.Models
{
    public class ConsultaProducto
    {
        public int id { get; set; }
        public String proveedor { get; set; }
        public String marca { get; set; }
        public String familia { get; set; }
        public String subfamilia { get; set; } 
        public String codigo { get; set; }
        public String descripcion { get; set; }
        public String descripcion_detallada { get; set; }
        public String costo { get; set; }
        public String medida { get; set; }
        public String modelo { get; set; }
        public int precio_dolar { get; set; }
        public int publicado { get; set; }
        public int carrusel { get; set; }
        public int imagenes { get; set; }
        public int pdfs { get; set; }
        public int visible { get; set; }
        public int consignacion { get; set; }
    }
}