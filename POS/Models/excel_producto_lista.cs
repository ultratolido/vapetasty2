﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace POS.Models
{
    public class excel_producto_lista
    {
        //public Nullable<producto> producto { get; set; }
        public string codigo { get; set; }
        public string proveedor { get; set; }
        public int idResultado { get; set; }
        public string Familia { get; set; }
        public string subFamilia { get; set; }
        public string descripcion { get; set; }
        public int precio_dolar { get; set; }
        public decimal precio { get; set; }
        public string modelo { get; set; }
        public string medida { get; set; }
        public List<string> cambios { get; set; }

        /*
         idResultado:
         * 1-agregado exitosamente
         * 2-duplicado
         * 3-error
         */
    }
}