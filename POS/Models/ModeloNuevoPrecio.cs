﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace POS.Models
{
    public class ModeloNuevoPrecio
    {
        public string codigo { get; set; }
        public string descripcion { get; set; }
        public Nullable<decimal> precio { get; set; }
        public Nullable<int> precio_dolar { get; set; }
        public string estatus { get; set; }
    }
}
