//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace POS.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class historial_loggeos
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public historial_loggeos()
        {
            this.intento_loggeo = new HashSet<intento_loggeo>();
        }
    
        public int id { get; set; }
        public string TokenLogin { get; set; }
        public string IP_local { get; set; }
        public string IP_publica { get; set; }
        public string hostname { get; set; }
        public int intentos { get; set; }
        public System.DateTime Fecha_fin_desbloqueo { get; set; }
        public bool status { get; set; }
        public Nullable<int> usuario_punto_venta_id { get; set; }
    
        public virtual usuario_punto_venta usuario_punto_venta { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<intento_loggeo> intento_loggeo { get; set; }
    }
}
