﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace POS.Models
{
      
    public class conceptos
    {
        public int id_producto { get; set; }
        public string descripcion { get; set; }
        public string tipoPrecio { get; set; }
        public Decimal cantidad { get; set; }
        public string codigo { get; set; }
        public decimal precio_unitario { get; set; }
        public decimal precio_unitario_original { get; set; }
        public decimal iva_unitario { get; set; }
        public decimal precio_unitario_mas_iva { get; set; }
        public decimal importe { get; set; }
        public decimal iva_importe { get; set; }
        public decimal importe_mas_iva { get; set; }
        public int tipoCliente_id { get; set; }
        public decimal precio_en_dolares { get; set; }
        public decimal precio_en_dolares_original { get; set; }
    }
   
}