﻿using System;

namespace POS.Models
{ 
    public class TotalVenta
    {
        public String tipo_pago { get; set; }
        public Decimal facturado { get; set; }
        public Decimal cancelado { get; set; }
        public Decimal total { get; set; }
    }
}