﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace POS.Models
{
    public class ModeloNuevoProducto
    {
        public string proveedor { get; set; }
        public string codigo { get; set; }
        public string descripcion { get; set; }
        public string familia { get; set; }
        public string subfamilia { get; set; }
        public string estatus { get; set; }
    }
}
