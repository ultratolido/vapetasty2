﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using POS.Models;

namespace POS.Models
{
    public class EditarProd
    {
        public static void aumentarPrecio(Decimal porcentaje,List<precio_por_tipocliente> Lista_precios_por_tipoCliente)
        {
            moragas_pruebasEntities1 db = new moragas_pruebasEntities1();
            porcentaje = 1 + (porcentaje / 100);
            foreach (precio_por_tipocliente pptc in Lista_precios_por_tipoCliente)
            {
                precio_por_tipocliente pptc_cambios= db.precio_por_tipocliente.Find(pptc.id);
                pptc_cambios.Precio = pptc_cambios.Precio * porcentaje;
            }
            db.SaveChanges();
        }

        public static void bajarPrecio(Decimal porcentaje)
        {
            moragas_pruebasEntities1 db = new moragas_pruebasEntities1();
            porcentaje = 1 + porcentaje;
            foreach (precio_por_tipocliente pptc in db.precio_por_tipocliente)
            {
                pptc.Precio = pptc.Precio / porcentaje;
            }
            db.SaveChanges();
        }
    }
}