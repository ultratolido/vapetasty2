﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pos.Models
{
    public class DatoGrafica
    {
        public Decimal nombre { get; set; }
        public Decimal columna1 { get; set; }
        public Decimal columna2 { get; set; }
        public Decimal columna3 { get; set; }
    }
}