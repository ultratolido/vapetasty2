﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace POS.Models
{
    public class cambio_precio
    {
        public string codigo { get; set; }
        public Decimal precio_anterior { get; set; }
        public Decimal precio_actual { get; set; }
        public Decimal diferencia { get; set; }
    }
}