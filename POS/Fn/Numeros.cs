﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using POS.Models;

namespace POS.Fn
{
    public class Numeros
    {
        public static string agregarCeros(int num)
        {
            string cadena_con_ceros = num.ToString();
            int total_digitos = 5;
            int diferencia = total_digitos - num.ToString().Length;
            for (int i = 0; i < diferencia; i++)
            {
                cadena_con_ceros = "0" + cadena_con_ceros;
            }
            return cadena_con_ceros;
        }

        public static string serie_folio_by_id(int idVenta)
        {
            moragas_pruebasEntities1 db = new moragas_pruebasEntities1();
            string serie = db.venta.Find(idVenta).Serie;
            string folio = agregarCeros(db.venta.Find(idVenta).Folio.Value);
            return serie + "-" + folio;
        }

        public static string serie_folio_by_id_nota_credito(int id)
        {
            moragas_pruebasEntities1 db = new moragas_pruebasEntities1();
            string serie = db.nota_credito.Find(id).serie;
            string folio = agregarCeros(db.nota_credito.Find(id).folio.Value);
            return serie + "-" + folio;
        }

        //public static Decimal iva_configurado()
        //{
        //    return Config.iva();
        //}

        public static Decimal cantidad_mas_iva(Decimal cantidad, int id_producto)
        {
            return Math.Round(cantidad * (Config.iva(id_producto)), 2);
        }

        public static Decimal cantidad_menos_iva(Decimal cantidad, int id_producto)
        {
            return Math.Round(cantidad / (Config.iva(id_producto)), 2);
        }

        public static Decimal calcula_iva(Decimal cantidad, int id_producto)
        {
            return Math.Round(cantidad * Config.iva(id_producto), 2);
        }

        public static String[] calcularInicioFin(String periodo)
        {
            String inicio = "", fin = "";

            if (periodo.Split('-')[0].Split(' ')[2].Equals("PM"))
            {
                if (periodo.Split('-')[0].Split(' ')[1].Split(':')[0].Equals("12"))
                    inicio = periodo.Split('-')[0].Split(' ')[0].Split('/')[1] + "/" + periodo.Split('-')[0].Split(' ')[0].Split('/')[0] + "/" + periodo.Split('-')[0].Split(' ')[0].Split('/')[2] + " " + (Convert.ToInt32(periodo.Split('-')[0].Split(' ')[1].Split(':')[0])) + ":" + periodo.Split('-')[0].Split(' ')[1].Split(':')[1] + ":" + periodo.Split('-')[0].Split(' ')[1].Split(':')[2];
                else
                    inicio = periodo.Split('-')[0].Split(' ')[0].Split('/')[1] + "/" + periodo.Split('-')[0].Split(' ')[0].Split('/')[0] + "/" + periodo.Split('-')[0].Split(' ')[0].Split('/')[2] + " " + (Convert.ToInt32(periodo.Split('-')[0].Split(' ')[1].Split(':')[0]) + 12) + ":" + periodo.Split('-')[0].Split(' ')[1].Split(':')[1] + ":" + periodo.Split('-')[0].Split(' ')[1].Split(':')[2];

            }
            else
            {
                if (periodo.Split('-')[0].Split(' ')[1].Split(':')[0].Equals("12"))
                    inicio = periodo.Split('-')[0].Split(' ')[0].Split('/')[1] + "/" + periodo.Split('-')[0].Split(' ')[0].Split('/')[0] + "/" + periodo.Split('-')[0].Split(' ')[0].Split('/')[2] + " " + (Convert.ToInt32(periodo.Split('-')[0].Split(' ')[1].Split(':')[0]) - 12) + ":" + periodo.Split('-')[0].Split(' ')[1].Split(':')[1] + ":" + periodo.Split('-')[0].Split(' ')[1].Split(':')[2];
                else
                    inicio = periodo.Split('-')[0].Split(' ')[0].Split('/')[1] + "/" + periodo.Split('-')[0].Split(' ')[0].Split('/')[0] + "/" + periodo.Split('-')[0].Split(' ')[0].Split('/')[2] + " " + (Convert.ToInt32(periodo.Split('-')[0].Split(' ')[1].Split(':')[0])) + ":" + periodo.Split('-')[0].Split(' ')[1].Split(':')[1] + ":" + periodo.Split('-')[0].Split(' ')[1].Split(':')[2];
            }

            if (periodo.Split('-')[1].Split(' ')[3].Equals("PM"))
            {
                if (periodo.Split('-')[1].Split(' ')[2].Split(':')[0].Equals("12"))
                    fin = periodo.Split('-')[1].Split(' ')[1].Split('/')[1] + "/" + periodo.Split('-')[1].Split(' ')[1].Split('/')[0] + "/" + periodo.Split('-')[1].Split(' ')[1].Split('/')[2] + " " + (Convert.ToInt32(periodo.Split('-')[1].Split(' ')[2].Split(':')[0])) + ":" + periodo.Split('-')[1].Split(' ')[2].Split(':')[1] + ":" + periodo.Split('-')[1].Split(' ')[2].Split(':')[2];
                else
                    fin = periodo.Split('-')[1].Split(' ')[1].Split('/')[1] + "/" + periodo.Split('-')[1].Split(' ')[1].Split('/')[0] + "/" + periodo.Split('-')[1].Split(' ')[1].Split('/')[2] + " " + (Convert.ToInt32(periodo.Split('-')[1].Split(' ')[2].Split(':')[0]) + 12) + ":" + periodo.Split('-')[1].Split(' ')[2].Split(':')[1] + ":" + periodo.Split('-')[1].Split(' ')[2].Split(':')[2];
            }
            else
            {
                if (periodo.Split('-')[1].Split(' ')[2].Split(':')[0].Equals("12"))
                    fin = periodo.Split('-')[1].Split(' ')[1].Split('/')[1] + "/" + periodo.Split('-')[1].Split(' ')[1].Split('/')[0] + "/" + periodo.Split('-')[1].Split(' ')[1].Split('/')[2] + " " + (Convert.ToInt32(periodo.Split('-')[1].Split(' ')[2].Split(':')[0]) - 12) + ":" + periodo.Split('-')[1].Split(' ')[2].Split(':')[1] + ":" + periodo.Split('-')[1].Split(' ')[2].Split(':')[2];
                else
                    fin = periodo.Split('-')[1].Split(' ')[1].Split('/')[1] + "/" + periodo.Split('-')[1].Split(' ')[1].Split('/')[0] + "/" + periodo.Split('-')[1].Split(' ')[1].Split('/')[2] + " " + (Convert.ToInt32(periodo.Split('-')[1].Split(' ')[2].Split(':')[0])) + ":" + periodo.Split('-')[1].Split(' ')[2].Split(':')[1] + ":" + periodo.Split('-')[1].Split(' ')[2].Split(':')[2];
            }
            String[] fecha = { inicio, fin };
            return fecha;
        }

    }
}