﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using POS.Models;
using Newtonsoft.Json;

namespace POS.Fn
{
    public class usuario_logeado
    {
        public int _idUsuario { get; set; }
        public string _nombre { get; set; }
        public int _nivel_id { get; set; }
        public string _nivel { get; set; }
        public string _logoPathUsuario { get; set; }
        public List<int> _permisosID { get; set; }
        public List<int> _sucursales_id { get; set; }

        public static usuario_punto_venta traerUsuario()
        {
            usuario_punto_venta u = new usuario_punto_venta();
            moragas_pruebasEntities1 db = new moragas_pruebasEntities1();
            usuario_logeado ul = JsonConvert.DeserializeObject<usuario_logeado>(HttpContext.Current.User.Identity.Name);
            if (ul._nombre != "")
            {
                u = db.usuario_punto_venta.Where(x => x.nombre_usuario == ul._nombre).SingleOrDefault();
            }
            return u;
        }

        public static List<int> PermisosID()
        {
            usuario_logeado ul = JsonConvert.DeserializeObject<usuario_logeado>(HttpContext.Current.User.Identity.Name);
            return ul._permisosID;
        }


        public static bool revisarPermisoAccesoVista(string nombreControlador,string nombreAccion)
        {
            moragas_pruebasEntities1 db=new moragas_pruebasEntities1();
            bool existeReglaPermiso = db.permiso_vista.Where(x => x.nombreVista == nombreAccion && x.nombreControlador == nombreControlador).Any();
            bool accesoPermitido = true;
            if (existeReglaPermiso)
            {
                usuario_punto_venta u = Fn.usuario_logeado.traerUsuario();
                accesoPermitido =db.permiso_vista_por_usuario.Where(x=>x.usuario_punto_venta_id==u.id).Select(x => x.permiso_vista).Where(x => x.nombreVista == nombreAccion && x.nombreControlador == nombreControlador).Any() ? true : false;
            }
            return accesoPermitido;
        }
        public static bool revisarPermisoInternoVista(string nombreControlador, string nombreAccion, string permisoInterno)
        {
            usuario_punto_venta u = Fn.usuario_logeado.traerUsuario();
            bool permiso = u.permiso_vista_por_usuario.Select(x => x.permiso_vista).Where(x => x.nombreVista == nombreAccion && x.nombreControlador == nombreControlador).Any() ? true : false;
            return permiso;
        }

        public static List<int> Sucursales_id()
        {
            usuario_logeado ul = JsonConvert.DeserializeObject<usuario_logeado>(HttpContext.Current.User.Identity.Name);
            return ul._sucursales_id;
        }

        public static int id()
        {
            usuario_logeado ul = JsonConvert.DeserializeObject<usuario_logeado>(HttpContext.Current.User.Identity.Name);
            return ul._idUsuario;
        }
        public static string nivel()
        {
            usuario_logeado ul = JsonConvert.DeserializeObject<usuario_logeado>(HttpContext.Current.User.Identity.Name);
            return ul._nivel.ToUpper();
        }
        public static int nivel_id()
        {
            usuario_logeado ul = JsonConvert.DeserializeObject<usuario_logeado>(HttpContext.Current.User.Identity.Name);
            return ul._nivel_id;
            //return ul._nivel_id != null ? ul._nivel_id : 0;
        }

        public static string logoPathUsuario()
        {
            usuario_logeado ul = JsonConvert.DeserializeObject<usuario_logeado>(HttpContext.Current.User.Identity.Name);
            return ul._logoPathUsuario;
        }

        public static string Nombre()
        {
            usuario_logeado ul = new usuario_logeado();
            if (HttpContext.Current.User.Identity.Name != "")
            {
                ul = JsonConvert.DeserializeObject<usuario_logeado>(HttpContext.Current.User.Identity.Name);
            }
            return ul._nombre != null ? ul._nombre.ToUpper() : "";
        }
        public static string strJson(usuario_punto_venta u)
        {
            usuario_logeado ul = new usuario_logeado();
            ul._idUsuario = u.id;
            ul._nombre = u.nombre_usuario;
            ul._nivel = u.nivel_usuario.nombre;
            ul._nivel_id = u.nivel_usuario_id;
            ul._permisosID = u.permiso_vista_por_usuario.Select(x => x.permiso_vista).Select(x=>x.id).ToList();
            ul._sucursales_id = u.sucursales_por_usuario.Select(x => x.sucursal.id).ToList();
            return JsonConvert.SerializeObject(ul);
        }

    }
}