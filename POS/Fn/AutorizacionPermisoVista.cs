﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using POS.Models;

namespace POS.Fn
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class AutorizacionVistaUsuarioActivo : AuthorizeAttribute
    {
        public string codigoPermiso { get; set; }
        public string nombreControlador { get; set; }
        public string nombreAction { get; set; }
        public string displayName{ get; set; }

        private bool _isAuthorized;

        protected override bool AuthorizeCore(System.Web.HttpContextBase httpContext)
        {
            var rd = httpContext.Request.RequestContext.RouteData;
            nombreControlador = rd.GetRequiredString("controller");
            nombreAction = rd.GetRequiredString("action");
            _isAuthorized = base.AuthorizeCore(httpContext);

            if (!_isAuthorized)
            {
                return false;
            }
            else
            {
                if (HttpContext.Current.User.Identity.Name != "")
                {
                    if (Fn.usuario_logeado.revisarPermisoAccesoVista(nombreControlador, nombreAction))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Controller.TempData["ControllerName"] = nombreControlador;
            filterContext.Controller.TempData["ActionName"] = nombreAction;
            // Returns HTTP 401 - see comment in HttpUnauthorizedResult.cs.
            filterContext.Result = new RedirectToRouteResult(
                                       new RouteValueDictionary 
                                   {
                                       { "action", "AccesoDenegadoVista" },
                                       { "controller", "Account" }
                                   });
        }
    }
}