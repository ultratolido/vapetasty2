﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using POS.Models;

namespace POS.Fn
{
    public class Configuracion
    {

        public static configuracion_general TraerConfiguracionGeneral()
        {
            moragas_pruebasEntities1 db = new moragas_pruebasEntities1();
            configuracion_general configuracion_general = db.configuracion_general.ToList().First();
            return configuracion_general;
        }

        public static configuracion_pos TraerConfiguracionPOS()
        {
            moragas_pruebasEntities1 db = new moragas_pruebasEntities1();
            configuracion_pos configuracion_pos = db.configuracion_pos.ToList().First();
            return configuracion_pos;
        }

        public static configuracion_tienda_online TraerConfiguracionTiendaOnline()
        {
            moragas_pruebasEntities1 db = new moragas_pruebasEntities1();
            configuracion_tienda_online configuracion_tienda_online = db.configuracion_tienda_online.ToList().First();
            return configuracion_tienda_online;
        }

        public static List<configuracion_menu> TraerConfiguracionMenu()
        {
            moragas_pruebasEntities1 db = new moragas_pruebasEntities1();
            List<configuracion_menu> ListaMenus = db.configuracion_menu.ToList();
            return ListaMenus;
        }

        public static List<sucursal> TraerConfiguracionSucursales()
        {
            moragas_pruebasEntities1 db = new moragas_pruebasEntities1();
            List<sucursal> ListaSucursales = db.sucursal.ToList();
            return ListaSucursales;
        }

        public static List<imagen_carousel> TraerImagenesSlider()
        {
            moragas_pruebasEntities1 db = new moragas_pruebasEntities1();
            List<imagen_carousel> ListaImagenesSlider = db.imagen_carousel.ToList();
            return ListaImagenesSlider;
        }

    }
}