﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using POS.Models;

namespace POS.Fn
{
    public class Fechas
    {
        public static String[] CalcularIncioFin(String periodo)
        {
            String inicio = "", fin = "";
            if (periodo.Split('-')[0].Split(' ')[2].Equals("PM"))
            {
                if (periodo.Split('-')[0].Split(' ')[1].Split(':')[0].Equals("12"))
                    inicio = periodo.Split('-')[0].Split(' ')[0].Split('/')[1] + "/" + periodo.Split('-')[0].Split(' ')[0].Split('/')[0] + "/" + periodo.Split('-')[0].Split(' ')[0].Split('/')[2] + " " + (Convert.ToInt32(periodo.Split('-')[0].Split(' ')[1].Split(':')[0])) + ":" + periodo.Split('-')[0].Split(' ')[1].Split(':')[1] + ":" + periodo.Split('-')[0].Split(' ')[1].Split(':')[2];
                else
                    inicio = periodo.Split('-')[0].Split(' ')[0].Split('/')[1] + "/" + periodo.Split('-')[0].Split(' ')[0].Split('/')[0] + "/" + periodo.Split('-')[0].Split(' ')[0].Split('/')[2] + " " + (Convert.ToInt32(periodo.Split('-')[0].Split(' ')[1].Split(':')[0]) + 12) + ":" + periodo.Split('-')[0].Split(' ')[1].Split(':')[1] + ":" + periodo.Split('-')[0].Split(' ')[1].Split(':')[2];
            }
            else
            {
                if (periodo.Split('-')[0].Split(' ')[1].Split(':')[0].Equals("12"))
                    inicio = periodo.Split('-')[0].Split(' ')[0].Split('/')[1] + "/" + periodo.Split('-')[0].Split(' ')[0].Split('/')[0] + "/" + periodo.Split('-')[0].Split(' ')[0].Split('/')[2] + " " + (Convert.ToInt32(periodo.Split('-')[0].Split(' ')[1].Split(':')[0]) - 12) + ":" + periodo.Split('-')[0].Split(' ')[1].Split(':')[1] + ":" + periodo.Split('-')[0].Split(' ')[1].Split(':')[2];
                else
                    inicio = periodo.Split('-')[0].Split(' ')[0].Split('/')[1] + "/" + periodo.Split('-')[0].Split(' ')[0].Split('/')[0] + "/" + periodo.Split('-')[0].Split(' ')[0].Split('/')[2] + " " + (Convert.ToInt32(periodo.Split('-')[0].Split(' ')[1].Split(':')[0])) + ":" + periodo.Split('-')[0].Split(' ')[1].Split(':')[1] + ":" + periodo.Split('-')[0].Split(' ')[1].Split(':')[2];
            }
            if (periodo.Split('-')[1].Split(' ')[3].Equals("PM"))
            {
                if (periodo.Split('-')[1].Split(' ')[2].Split(':')[0].Equals("12"))
                    fin = periodo.Split('-')[1].Split(' ')[1].Split('/')[1] + "/" + periodo.Split('-')[1].Split(' ')[1].Split('/')[0] + "/" + periodo.Split('-')[1].Split(' ')[1].Split('/')[2] + " " + (Convert.ToInt32(periodo.Split('-')[1].Split(' ')[2].Split(':')[0])) + ":" + periodo.Split('-')[1].Split(' ')[2].Split(':')[1] + ":" + periodo.Split('-')[1].Split(' ')[2].Split(':')[2];
                else
                    fin = periodo.Split('-')[1].Split(' ')[1].Split('/')[1] + "/" + periodo.Split('-')[1].Split(' ')[1].Split('/')[0] + "/" + periodo.Split('-')[1].Split(' ')[1].Split('/')[2] + " " + (Convert.ToInt32(periodo.Split('-')[1].Split(' ')[2].Split(':')[0]) + 12) + ":" + periodo.Split('-')[1].Split(' ')[2].Split(':')[1] + ":" + periodo.Split('-')[1].Split(' ')[2].Split(':')[2];
            }
            else
            {
                if (periodo.Split('-')[1].Split(' ')[2].Split(':')[0].Equals("12"))
                    fin = periodo.Split('-')[1].Split(' ')[1].Split('/')[1] + "/" + periodo.Split('-')[1].Split(' ')[1].Split('/')[0] + "/" + periodo.Split('-')[1].Split(' ')[1].Split('/')[2] + " " + (Convert.ToInt32(periodo.Split('-')[1].Split(' ')[2].Split(':')[0]) - 12) + ":" + periodo.Split('-')[1].Split(' ')[2].Split(':')[1] + ":" + periodo.Split('-')[1].Split(' ')[2].Split(':')[2];
                else
                    fin = periodo.Split('-')[1].Split(' ')[1].Split('/')[1] + "/" + periodo.Split('-')[1].Split(' ')[1].Split('/')[0] + "/" + periodo.Split('-')[1].Split(' ')[1].Split('/')[2] + " " + (Convert.ToInt32(periodo.Split('-')[1].Split(' ')[2].Split(':')[0])) + ":" + periodo.Split('-')[1].Split(' ')[2].Split(':')[1] + ":" + periodo.Split('-')[1].Split(' ')[2].Split(':')[2];
            }
            String[] fechas = { inicio, fin };
            return fechas;
        }

        public static int TraerDiasRestantes(DateTime dia_venta, int dias_credito)
        {
            dia_venta = dia_venta.AddDays(dias_credito);
            //Sacamos la diferencia
            TimeSpan ts = dia_venta - DateTime.Now;
            return ts.Days;
        }
    }
}