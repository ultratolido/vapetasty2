﻿using System;
using System.Web;
using System.Web.Security;
using POS.Models;
/// <summary>
/// Enforces a single login session
/// Needs an entry in Web.Config, exactly where depends on the version of IIS, but you
/// can safely put it in both places.
/// 1:
///  <system.web>
///     <httpModules>
///      <add name="SingleSessionEnforcement" type="SingleSessionEnforcement" />
///    </httpModules>
///  </system.web>
/// 2:
///  <system.webServer>
///    <modules runAllManagedModulesForAllRequests="true">
///      <add name="SingleSessionEnforcement" type="SingleSessionEnforcement" />
///    </modules>
///  </system.webServer>
/// Also, slidingExpiration for the forms must be set to false, also set a 
/// suitable timeout period (in minutes)
///  <authentication mode="Forms">
///   <forms protection="All" slidingExpiration="false" loginUrl="login.aspx" timeout="600" />
///  </authentication>
/// </summary>
/// 
public class SingleSessionEnforcement : IHttpModule
{
    public static string guid { get; set; }

    public SingleSessionEnforcement()
    {
        // No construction needed
    }

    
    private void OnPostAuthenticate(Object sender, EventArgs e)
    {


        moragas_pruebasEntities1 db = new moragas_pruebasEntities1();

        Guid sessionToken;

        HttpApplication httpApplication = (HttpApplication)sender;
        HttpContext httpContext = httpApplication.Context;

        // Check user's session token
        if (httpContext.User.Identity.IsAuthenticated)
        {
            FormsAuthenticationTicket authenticationTicket =
                ((FormsIdentity)httpContext.User.Identity).Ticket;

          
            if (authenticationTicket.UserData != "")
            {
                sessionToken = new Guid(authenticationTicket.UserData);
            }
            else
            {
                //SIN LOGEO
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                return;
            }

            //  int idUsuarioLoggeado = POS.Fn.usuario_logeado.id();
            //usuario_punto_venta u = db.usuario_punto_venta.Find(idUsuarioLoggeado);

            guid = sessionToken.ToString();

            //if (sessionToken.ToString() != u.token)
            //{
            //    // el token almacenado para este usuario no es el mismo que el actual
            //    // avisa que se ha iniciado sesión desde otro equipo y da la opcion de recuperar el control del usuario.
            //    FormsAuthentication.SignOut();
            //    FormsAuthentication.RedirectToLoginPage();
            //}
        }
    }

    public void Dispose()
    {
        // Nothing to dispose
    }

    public void Init(HttpApplication context)
    {
        context.PostAuthenticateRequest += new EventHandler(OnPostAuthenticate);
    }
}