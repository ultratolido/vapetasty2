﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using POS.Models;

namespace POS.Fn
{
    public class Impuestos
    {
        public static Decimal iva(int id_producto, Decimal precio)
        {
            moragas_pruebasEntities1 db = new moragas_pruebasEntities1();
            producto producto = db.producto.Find(id_producto);
            Decimal iva = precio * producto.iva.Value;
            return iva;
        }
    }
}