﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using POS.Models;

namespace POS.Fn
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class AutoriacionNivel : AuthorizeAttribute
    {
        public string nombreNivel { get; set; }

        private bool _isAuthorized;

        protected override bool AuthorizeCore(System.Web.HttpContextBase httpContext)
        {
            _isAuthorized = base.AuthorizeCore(httpContext);
            if (!_isAuthorized)
            {
                return false;
            }
            else
            {
                if (HttpContext.Current.User.Identity.Name != "")
                {
                    string Nivelusuario = POS.Fn.usuario_logeado.nivel();
                    string[] NivelesPermitidos = this.nombreNivel.Split(',');
                    bool existeRol = false;
                    foreach (string rolPermitido in NivelesPermitidos)
                    {
                        if (rolPermitido.ToUpper().Trim() == Nivelusuario.ToUpper().Trim())
                            existeRol = true;
                    }
                    if (existeRol)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {

            // Returns HTTP 401 - see comment in HttpUnauthorizedResult.cs.
            filterContext.Result = new RedirectToRouteResult(
                                       new RouteValueDictionary 
                                   {
                                       { "action", "AccesoDenegadoNivel" },
                                       { "controller", "Account" }
                                   });
        }
    }
}