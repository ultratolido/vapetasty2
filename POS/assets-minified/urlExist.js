﻿function urlExists(url, callback) {
    $.ajax({
        type: 'HEAD',
        url: url,
        success: function () {
            callback(true);
        },
        error: function () {
            callback(false);
        }
    });
}