﻿
keyDown = false;
ctrl = 17;
vKey = 86; Vkey = 118;
xkey = 120; XKey = 88;
ckey = 99; CKey = 67;

$(document).keydown(function (e) {
    if (e.keyCode == ctrl) keyDown = true;
}).keyup(function (e) {
    if (e.keyCode == ctrl) keyDown = false;
});

///////////////////////////////////////////////////////
///////////////////////////////////////////////////////
////// LIMITAR INPUT TXT
function limitMaxNumber(element) {
    var max_chars = 2;
    valor = parseFloat(element.value);
    max = parseFloat(element.getAttribute("max"));
    if (valor > max) {
        element.value = element.value.substring(0, element.value.length - 1);
    }
}

///////////////////////////////////////////////////////
///////////////////////////////////////////////////////
////// E N T E R O     S I N    D E C I M A L E S
function InputEnteroSinDecimales(elementID) {
    keyDown = false;
    ctrl = 17;
    vKey = 86; Vkey = 118;
    xkey = 120; XKey = 88;
    ckey = 99; CKey = 67;

    $(elementID).on('keypress', function (e) {
        if (!e) var e = window.event;
        if (e.keyCode > 0 && e.which == 0) return true;
        if (e.keyCode) code = e.keyCode;
        else if (e.which) code = e.which;
        var character = String.fromCharCode(code);
        if (character == '\b' /*|| character == ' ' */ || character == '\t') return true;
        if (keyDown && (code == vKey || code == Vkey)) return (character);
        if (keyDown && (code == xkey || code == XKey)) return (character);
        if (keyDown && (code == ckey || code == CKey)) return (character);
        else return (/[0-9]$/.test(character));
    }).on('focusout', function (e) {
        var $this = $(this);
        $this.val($this.val().replace(/[^0-9]/g, ''));
    }).on('paste', function (e) {
        var $this = $(this);
        setTimeout(function () {
            $this.val($this.val().replace(/[^0-9]/g, ''));
        }, 5);
    });
}



///////////////////////////////////////////////////////
///////////////////////////////////////////////////////
////// E N T E R O     C O N    D O S     2   D E C I M A L E S 
function InputEnteroDosDecimales(elementID) {
    keyDown = false;
    ctrl = 17;
    vKey = 86; Vkey = 118;
    xkey = 120; XKey = 88;
    ckey = 99; CKey = 67;
    punto = 46;

    $(elementID).on('keypress', function (e) {
        if (!e) var e = window.event;
        if (e.keyCode > 0 && e.which == 0) return true;
        if (e.keyCode) code = e.keyCode;
        else if (e.which) code = e.which;
        var character = String.fromCharCode(code);
        contarPuntos = ($(this).val().split(".").length) - 1;
        if (character == '\b' /*|| character == ' ' */ || character == '\t') return true;
        if (keyDown && (code == vKey || code == Vkey)) return (character);
        if (keyDown && (code == xkey || code == XKey)) return (character);
        if (keyDown && (code == ckey || code == CKey)) return (character);
        if (contarPuntos == 0 && code == punto) return (character);
        else return (/^\d{0,8}(\.\d{1,2})?$/.test(character));
    }).on('focusout', function (e) {
        var $this = $(this);
        contarPuntos = ($(this).val().split(".").length) - 1;
        value = $this.val();
        if (contarPuntos > 1) {
            splitArray = $(this).val().split(".");
            value = splitArray[0] + "." + splitArray[1];
            $this.val(value.replace(/[^0-9.]/g, ""));
        }
        else {
            $this.val(value.replace(/[^0-9.]/g, ""));
        }
    }).on('paste', function (e) {
        var $this = $(this);
        setTimeout(function () {
            value = $this.val();
            contarPuntos = (value.split(".").length) - 1;
            if (contarPuntos > 1) {
                splitArray = value.split(".");
                value = splitArray[0] + "." + splitArray[1];
                //$this.val(value.replace(/[^0-9.]/g, ""));
                //redondear value a 2 decimales
                $this.val(value.replace(/[^0-9.]/g, ""));
            }
            else {
                $this.val(value.replace(/[^0-9.]/g, ""));
            }
        }, 5);
    });
}



///////////////////////////////////////////////////////
///////////////////////////////////////////////////////
////// E N T E R O     C O N    D O S     2   D E C I M A L E S  MAX VALUE
function InputEnteroDosDecimalesMaxValue(elementID) {
    keyDown = false;
    ctrl = 17;
    vKey = 86; Vkey = 118;
    xkey = 120; XKey = 88;
    ckey = 99; CKey = 67;
    punto = 46;

    $(elementID).on('keypress', function (e) {
        if (!e) var e = window.event;
        if (e.keyCode > 0 && e.which == 0) return true;
        if (e.keyCode) code = e.keyCode;
        else if (e.which) code = e.which;
        var character = String.fromCharCode(code);
        contarPuntos = ($(this).val().split(".").length) - 1;
        if (character == '\b' /*|| character == ' ' */ || character == '\t') return true;
        if (keyDown && (code == vKey || code == Vkey)) return (character);
        if (keyDown && (code == xkey || code == XKey)) return (character);
        if (keyDown && (code == ckey || code == CKey)) return (character);
        if (contarPuntos == 0 && code == punto) return (character);
        else return (/^\d{0,8}(\.\d{1,2})?$/.test(character));
    }).on('focusout', function (e) {
        var $this = $(this);
        contarPuntos = ($(this).val().split(".").length) - 1;
        value = $this.val();
        if (contarPuntos > 1) {
            splitArray = $(this).val().split(".");
            value = splitArray[0] + "." + splitArray[1];
            $this.val(value.replace(/[^0-9.]/g, ""));
        }
        else {
            $this.val(value.replace(/[^0-9.]/g, ""));
        }
    }).on('paste', function (e) {
        var $this = $(this);
        setTimeout(function () {
            value = $this.val();
            contarPuntos = (value.split(".").length) - 1;
            if (contarPuntos > 1) {
                splitArray = value.split(".");
                value = splitArray[0] + "." + splitArray[1];
                $this.val(value.replace(/[^0-9.]/g, ""));
            }
            else {
                $this.val(value.replace(/[^0-9.]/g, ""));
            }
        }, 5);
    });
}


///////////////////////////////////////////////////////
///////////////////////////////////////////////////////
////// E N T E R O     C O N    D E C I M A L E S     I N F I N I T O S
function InputEnteroConDecimales(elementID) {
    keyDown = false;
    ctrl = 17;
    vKey = 86; Vkey = 118;
    xkey = 120; XKey = 88;
    ckey = 99; CKey = 67;
    punto = 46;

    $(elementID).on('keypress', function (e) {
        if (!e) var e = window.event;
        if (e.keyCode > 0 && e.which == 0) return true;
        if (e.keyCode) code = e.keyCode;
        else if (e.which) code = e.which;
        var character = String.fromCharCode(code);
        contarPuntos = ($(this).val().split(".").length) - 1;
        if (character == '\b' /*|| character == ' ' */ || character == '\t') return true;
        if (keyDown && (code == vKey || code == Vkey)) return (character);
        if (keyDown && (code == xkey || code == XKey)) return (character);
        if (keyDown && (code == ckey || code == CKey)) return (character);
        if (contarPuntos == 0 && code == punto) return (character);
        else return (/^\d{0,8}(\.\d{1,2})?$/.test(character));
    }).on('focusout', function (e) {
        var $this = $(this);
        contarPuntos = ($(this).val().split(".").length) - 1;
        value = $this.val();
        if (contarPuntos > 1) {
            splitArray = $(this).val().split(".");
            value = splitArray[0] + "." + splitArray[1];
            $this.val(value.replace(/[^0-9.]/g, ""));
        }
        else {
            $this.val(value.replace(/[^0-9.]/g, ""));
        }
    }).on('paste', function (e) {
        var $this = $(this);
        setTimeout(function () {
            value = $this.val();
            contarPuntos = (value.split(".").length) - 1;
            if (contarPuntos > 1) {
                splitArray = value.split(".");
                value = splitArray[0] + "." + splitArray[1];
                $this.val(value.replace(/[^0-9.]/g, ""));
            }
            else {
                $this.val(value.replace(/[^0-9.]/g, ""));
            }
        }, 5);
    });
}