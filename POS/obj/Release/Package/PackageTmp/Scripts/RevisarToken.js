﻿
function RevisarToken(UrlRevisarToken, PathLogOn) {
    $.ajax({
        type: "post",
        url: UrlRevisarToken,
        success: function (result) {
            if (result.success) {
                if (!result.mismoToken) {
                    //SI NO ES LA MISMA IP REGISTRADA
                    //MANDA A LA VISTA DE LOGIN

                    alertaIP = "<table class='table' style='width: 90%;margin:5%'>"
                        + "<tbody>"
                          + "<tr>"
                          + " <td style='text-align: center; border: 0px;'>"
                          + "<div style='color: orange; text-align: center'>"
                                + "<i class='fa fa-exclamation-triangle' aria-hidden='true' style='font-size: 5em; padding: 2%;'></i>"
                           + "</div>"
                          + "</td>"
                          + " </tr>"
                          + "<tr>"
                          + "    <td style='text-align: left; border: 0px;'><label>¿Porque pasa esto?</label></td>"
                          + "</tr>"
                          + "<tr>"
                          + "    <td style='text-align: left; border: 0px;'><label>Mi usuario:</label></td>"
                          + "</tr>"
                            + "<tr>"
                          + "    <td style='text-align: left; border: 0px;'>1) Aún no ha sido loggeado.</td>"
                          + "</tr>"
                          + "</tr>"
                            + "<tr>"
                          + "    <td style='text-align: left; border: 0px;'>2) Ha sido desactivado por el administrador.</td>"
                          + "</tr>"
                          + "<tr>"
                          + "    <td style='text-align: left; border: 0px;'>3) Ha sido utilizado en este mismo equipo pero desde otro navegador.</td>"
                          + "</tr>"
                          + "<tr>"
                          + "    <td style='text-align: left; border: 0px;'>4) Ha sido utilizado desde otro equipo.</td>"
                          + "</tr>"
                          + "<tr>"
                              + "<td>&nbsp;"
                              + "</td>"
                          + "</tr>"
                          + "<tr>"
                          + "    <td style='text-align: center; border: 0px;'><label>Favor de autenticarse de nuevo para recuperar el permiso.</label></td>"
                          + "</tr>"
                        + "</tbody>"
                 + "</table>";

                    var bootboxAutenticarse = bootbox.dialog({
                        message: alertaIP,
                        title: "Es necesario ingresar de nuevo a su cuenta",
                        onEscape: function () {
                            window.location.href = PathLogOn;
                        },
                        onClose: function () {
                            window.location.href = PathLogOn;
                        },
                        buttons: {
                            success: {
                                label: "AUTENTICARSE DE NUEVO",
                                callback: function () { window.location.href = PathLogOn; }// rootPath + "/Account/LogOff"; }
                            }
                        }
                    });

                }
            }
        },
        async: false
    });
}