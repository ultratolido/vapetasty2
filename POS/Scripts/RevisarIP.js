﻿
function RevisarIP(PathRevisarIP, PathLogOn) {
    getLocalIP();
    var _localIPS = "";
    function getLocalIP() {
        _localIPS = "";
        //insert IP addresses into the page
        getIPs(function (ip) {
            //local IPs
            if (ip.match(/^(192\.168\.|169\.254\.|10\.|172\.(1[6-9]|2\d|3[01]))/)) {
                var dt = new Date();
                var time = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
                $("#lblIPS").append(ip + "-");
                //alert(ip + "- " + time);
                _localIPS = _localIPS + "-" + ip;
            }
        });

        //get the IP addresses associated with an account
        function getIPs(callback) {
            var ip_dups = {};
            //compatibility for firefox and chrome
            var RTCPeerConnection = window.RTCPeerConnection
                || window.mozRTCPeerConnection
                || window.webkitRTCPeerConnection;
            var useWebKit = !!window.webkitRTCPeerConnection;

            //bypass naive webrtc blocking using an iframe
            if (!RTCPeerConnection) {
                //NOTE: you need to have an iframe in the page right above the script tag
                //
                //<iframe id="iframe" sandbox="allow-same-origin" style="display: none"></iframe>
                //<script>...getIPs called in here...
                //
                var win = iframe.contentWindow;
                RTCPeerConnection = win.RTCPeerConnection
                    || win.mozRTCPeerConnection
                    || win.webkitRTCPeerConnection;
                useWebKit = !!win.webkitRTCPeerConnection;
            }

            //minimal requirements for data connection
            var mediaConstraints = {
                optional: [{ RtpDataChannels: true }]
            };

            var servers = { iceServers: [{ urls: "stun:stun.services.mozilla.com" }] };

            //construct a new RTCPeerConnection
            var pc = new RTCPeerConnection(servers, mediaConstraints);

            function handleCandidate(candidate) {
                //match just the IP address
                var ip_regex = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/
                var ip_addr = ip_regex.exec(candidate)[1];

                //remove duplicates
                if (ip_dups[ip_addr] === undefined)
                    callback(ip_addr);

                ip_dups[ip_addr] = true;
            }

            //listen for candidate events
            pc.onicecandidate = function (ice) {

                //skip non-candidate events
                if (ice.candidate)
                    handleCandidate(ice.candidate.candidate);
            };

            //create a bogus data channel
            pc.createDataChannel("");

            //create an offer sdp
            pc.createOffer(function (result) {

                //trigger the stun server request
                pc.setLocalDescription(result, function () { }, function () { });

            }, function () { });

            //wait for a while to let everything done
            setTimeout(function () {
                //read candidate info from local description
                var lines = pc.localDescription.sdp.split('\n');

                lines.forEach(function (line) {
                    if (line.indexOf('a=candidate:') === 0)
                        handleCandidate(line);
                });
            }, 1000);
        }
    }

    setTimeout(function () {
        var urlIP = PathRevisarIP;// rootPath + "/IP/revisarIP";
        if (_localIPS != "") {
            $.ajax({
                url: urlIP,
                data: { localIPS: _localIPS },
                success: function (result) {
                    if (result.success) {
                        if (!result.mismaIP) {
                            //SI NO ES LA MISMA IP REGISTRADA
                            //MANDA A LA VISTA DE LOGIN

                            alertaIP = "<table class='table' style='width: 100%;'>"
                                +"<tbody>"
                                  + "<tr>"
                                  + " <td style='text-align: center; border: 0px;'>"
                                  +"<div style='color: orange; text-align: center'>"
                                        + "<i class='fa fa-exclamation-triangle' aria-hidden='true' style='font-size: 5em; padding: 2%;'></i>"
                                   + "</div>"
                                  + "</td>"
                                  + " </tr>"
                                  + "<tr>"
                                  + "    <td style='text-align: center; border: 0px;'>Su usuario ha sido loggeado en otro equipo.</td>"
                                  + "</tr>"
                                  + "<tr>"
                                  + "    <td style='text-align: center; border: 0px;'>Favor de autenticarse de nuevo para recuperar el permiso.</td>"
                                  + "</tr>"
                                + "</tbody>"
                         + "</table>";

                            bootbox.dialog({
                                message: alertaIP,
                                title: "LA DIRECCIÓN 'IP' HA CAMBIADO.'",
                                onClose: false,
                                onEscape:false,
                                buttons: {
                                    success: {
                                        label: "AUTENTICARSE DE NUEVO",
                                        callback: function () { window.location.href = PathLogOn; }// rootPath + "/Account/LogOff"; }
                                    }
                                }
                            });

                        //bootbox.alert(alerta, function () {
                        //    //window.location.href = "/Account/LogOff"
                        //});
        
                           
                    }
                }
            },
                async: false
        });
    }
    }, 1000);
}
