﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tienda_Online.Models;
using System.IO;

namespace Tienda_Online.Controllers
{
    public class ProductoController : Controller
    {
        POSEntities db = new POSEntities();

        public PartialViewResult VistaParcialProductosDestacados(int pagina, int numeroProductos)
        {
            /** 
             * Conslta para traer el inventario para la tienda online:
             * select  sum(inv.cantidad) as inventario
		     *  from inventario inv
             *  where
             *  (inv.producto_id, inv.sucursal_id) in 
             *  (select inventario.Producto_id, inventario.sucursal_id 
             *  from inventario, sucursal 
             *  where inventario.Producto_id = 1 and
             *  inventario.sucursal_id in (select sucursal.id from sucursal where sucursal.alimenta_tienda_online = 1));
             * **/
            configuracion_general configuracion_general = Fn.Configuracion.TraerConfiguracionGeneral();
            configuracion_tienda_online configuracion_tienda_online = Fn.Configuracion.TraerConfiguracionTiendaOnline();
            int id_tipo_precio = 1;
            if (Fn.cliente_logeado.TraerCliente().id != 0)
                id_tipo_precio = Fn.cliente_logeado.TraerCliente().TipoCliente_id;
            String query = "select distinct(pro.id), pro.Codigo, pro.descripcion, pro.descripcion_detallada, pro.precio_dolar, pro.id_marca, pro.SubFamilia_id, pre.Precio, pro.costo, pro.iva"
                            + " from producto pro,"
                            + " precio_por_tipocliente pre"
                            + " where "
                            + " pro.id=pre.Producto_id"
                            + " and pre.precio>0 "
                            + " and pro.carrusel = 1"
                            + " and pre.sucursal_id=1 and pre.TipoCliente_id=" + id_tipo_precio + " AND pro.publicar = 1";
            DataSet ds = Fn.ConnDB.GetDataSet(query, Server.MapPath("").Split('\\').ElementAt(3));
            DataTable dt = ds.Tables[0];
            List<itemproductocarrito2> ListaProductos = (from DataRow row in dt.Rows
                                                         select new itemproductocarrito2
                                                        {
                                                            Id = Convert.ToInt32(row["Id"]),
                                                            Codigo = row["Codigo"].ToString(),
                                                            Descripcion = row["Descripcion"].ToString(),
                                                            Precio_Dolar = Convert.ToInt32(row["Precio_Dolar"]),
                                                            id_marca = Convert.ToInt32(row["id_marca"]),
                                                            SubFamilia_id = Convert.ToInt32(row["SubFamilia_id"]),
                                                            Precio_Unitario = Convert.ToDecimal(row["Precio"]),
                                                            iva = Convert.ToDecimal(row["iva"]),
                                                            imagen = ""
                                                        }).ToList();
            int totalProductos = ListaProductos.Count();
            ListaProductos = ListaProductos.Skip(((pagina - 1) * numeroProductos)).Take(numeroProductos).ToList();
            foreach (itemproductocarrito2 producto in ListaProductos)
                if (db.imagen.Where(x => x.id_producto == producto.Id).Any())
                    producto.imagen = db.imagen.Where(x => x.id_producto == producto.Id).FirstOrDefault().nombre;
            /**
             * Para traer los inventario por producto
             * **/
            if (configuracion_tienda_online.mostrar_inventario == true)
            {
                List<int> lista_id_sucursales = db.sucursal.Where(x => x.alimenta_tienda_online == true).Select(y => y.id).ToList();
                foreach (itemproductocarrito2 producto in ListaProductos)
                {
                    Decimal cantidad_en_inventario = Convert.ToDecimal(db.inventario.Where(x => x.Producto_id == producto.Id && lista_id_sucursales.Contains(x.sucursal_id)).Select(x => x.cantidad).Sum());
                    producto.Inventario = cantidad_en_inventario;

                }
            }

            foreach (itemproductocarrito2 producto in ListaProductos)
            {
                producto.cantidad_iva_unitario = 0;
                Decimal total_porcentaje = 100 + producto.iva;
                Decimal precioActual = db.precio_por_tipocliente.Where(x => x.Producto_id == producto.Id && x.TipoCliente_id == id_tipo_precio && x.sucursal_id == 1).Single().Precio.Value;
                if (configuracion_general.precios_netos == true)
                {
                    producto.Precio_Unitario = (precioActual / total_porcentaje) * 100;
                    if (total_porcentaje > 100)
                        producto.cantidad_iva_unitario = Math.Round((precioActual / total_porcentaje) * (total_porcentaje - 100), 2);
                }
                else
                {
                    if (total_porcentaje > 100)
                        producto.cantidad_iva_unitario = Math.Round((producto.Precio_Unitario / 100) * (total_porcentaje - 100), 2);
                }
            }

            ViewBag.ListaProductos = ListaProductos;
            ViewBag.totalProductos = totalProductos;
            if (totalProductos == 0)
                ViewBag.desde = 0;
            else
                ViewBag.desde = ((pagina - 1) * numeroProductos) + 1;
            if (totalProductos < ((pagina - 1) * numeroProductos) + numeroProductos)
                ViewBag.hasta = totalProductos;
            else
                ViewBag.hasta = ((pagina - 1) * numeroProductos) + numeroProductos;
            ViewBag.pagina = pagina;
            ViewBag.numeroProductos = numeroProductos;
            return PartialView();
        }

        public ActionResult DetalleProducto(string id_producto)
        {
            Fn.FnIva.establecerValoresIniciales();
            configuracion_general configuracion_general = Fn.Configuracion.TraerConfiguracionGeneral();
            configuracion_tienda_online configuracion_tienda_online = Fn.Configuracion.TraerConfiguracionTiendaOnline();
            int id_tipo_precio = 1;
            Decimal cantidad_en_inventario;
            if (Fn.cliente_logeado.TraerCliente().id != 0)
                id_tipo_precio = Fn.cliente_logeado.TraerCliente().TipoCliente_id;

            producto producto = db.producto.Find(id_producto);
            ViewBag.ListaHijos = new SelectList((from s in db.producto.Where(x => x.productopadre_id == producto.productopadre_id).ToList() select new { ID = s.id.ToString(), text = s.modelo.ToUpper() }), "ID", "text");
            Decimal iva_actual_producto = Convert.ToDecimal(producto.iva);
            Decimal precio_unitario_producto = 0;
            Decimal cantidad_iva_unitario = 0;
            Decimal total_porcentaje = 100 + iva_actual_producto;
            Decimal precioActual = db.precio_por_tipocliente.Where(x => x.Producto_id == producto.id && x.TipoCliente_id == id_tipo_precio && x.sucursal_id == 1).Single().Precio.Value;
            
            if (configuracion_general.precios_netos == true)
            {
                precio_unitario_producto = (precioActual / total_porcentaje) * 100;
                if (total_porcentaje > 100)
                    cantidad_iva_unitario = Math.Round((precioActual / total_porcentaje) * (total_porcentaje - 100), 2);
            }
            else
            {
                precio_unitario_producto = precioActual;
                if (total_porcentaje > 100)
                    cantidad_iva_unitario = Math.Round((precioActual / 100) * (total_porcentaje - 100), 2);
            }
            List<int> lista_id_sucursales = db.sucursal.Where(x => x.alimenta_tienda_online == true).Select(y => y.id).ToList();
            List<inventario> lista_inventario = db.inventario.Where(x => x.Producto_id == producto.id && lista_id_sucursales.Contains(x.sucursal_id)).ToList();
            if (lista_inventario != null && lista_inventario.Count > 0)
                cantidad_en_inventario = Convert.ToDecimal(lista_inventario.Select(x => x.cantidad).Sum());
            else
                cantidad_en_inventario = 0;

            List<itemproductocarrito2> ListaProductosRelacionados = new List<itemproductocarrito2>();
            List<int> listaIdProductos = new List<int>();
            List<Int32> ListaIdsProductoSeleccionadosContar = new List<int>();
            int id_familia = producto.subfamilia.Familia_id;
            int id_subFamilia = producto.SubFamilia_id;

            string query = "select distinct(pro.id), pro.Codigo, pro.descripcion, pro.descripcion_detallada, pro.precio_dolar, pro.id_marca, pro.SubFamilia_id, pre.Precio, pro.costo, pro.iva"
                        + " FROM producto pro"
                        + " ,precio_por_tipocliente pre"
                        + " WHERE"
                        + " pro.id=pre.Producto_id"
                        + " and pro.subfamilia_id=" + id_subFamilia
                        + " and pro.subfamilia_id!=1"
                        + " and pre.TipoCliente_id=" + id_tipo_precio
                        + " and pre.sucursal_id=1"
                        + " and pro.publicar=1"
                        + " and pre.precio>0";
            DataSet ds = Fn.ConnDB.GetDataSet(query, Server.MapPath("").Split('\\').ElementAt(3));
            DataTable dt = ds.Tables[0];
            ListaProductosRelacionados = (from DataRow row in dt.Rows
                                          select new itemproductocarrito2
                                          {
                                              Id = Convert.ToInt32(row["Id"]),
                                              Codigo = row["Codigo"].ToString(),
                                              Descripcion = row["Descripcion"].ToString(),
                                              Precio_Dolar = Convert.ToInt32(row["Precio_Dolar"]),
                                              id_marca = Convert.ToInt32(row["id_marca"]),
                                              SubFamilia_id = Convert.ToInt32(row["SubFamilia_id"]),
                                              Precio_Unitario = Convert.ToDecimal(row["Precio"]),
                                              iva = Convert.ToDecimal(row["iva"])
                                          }).ToList();

            if (configuracion_tienda_online.mostrar_inventario == true)
            {
                foreach (itemproductocarrito2 producto1 in ListaProductosRelacionados)
                    if (db.imagen.Where(x => x.id_producto == producto1.Id).Any())
                        producto1.imagen = db.imagen.Where(x => x.id_producto == producto1.Id).FirstOrDefault().nombre; 
            }
            /**
             * Para traer los inventario por producto
             * **/
            lista_id_sucursales = db.sucursal.Where(x => x.alimenta_tienda_online == true).Select(y => y.id).ToList();
            foreach (itemproductocarrito2 producto2 in ListaProductosRelacionados)
            {
                Decimal cantidad_en_inventario2 = Convert.ToDecimal(db.inventario.Where(x => x.Producto_id == producto2.Id && lista_id_sucursales.Contains(x.sucursal_id)).Select(x => x.cantidad).Sum());
                producto2.Inventario = cantidad_en_inventario2;
            }
            foreach (itemproductocarrito2 producto_individual in ListaProductosRelacionados)
            {
                producto_individual.cantidad_iva_unitario = 0;
                total_porcentaje = 100 + producto_individual.iva;
                precioActual = db.precio_por_tipocliente.Where(x => x.Producto_id == producto_individual.Id && x.TipoCliente_id == id_tipo_precio && x.sucursal_id == 1).Single().Precio.Value;
                if (configuracion_general.precios_netos == true)
                {
                    producto_individual.Precio_Unitario = (precioActual / total_porcentaje) * 100;
                    if (total_porcentaje > 100)
                        producto_individual.cantidad_iva_unitario = Math.Round((precioActual / total_porcentaje) * (total_porcentaje - 100), 2);
                }
                else
                {
                    if (total_porcentaje > 100)
                        producto_individual.cantidad_iva_unitario = Math.Round((producto_individual.Precio_Unitario / 100) * (total_porcentaje - 100), 2);
                }
            }
            //List<pdf> catalogos = db.pdf.Where(x => x.id_producto == id_producto).ToList();
            ViewBag.producto = producto;
            ViewBag.precio_unitario_producto = precio_unitario_producto;
            ViewBag.cantidad_iva_unitario = cantidad_iva_unitario;
            ViewBag.total_inventario = cantidad_en_inventario;
            ViewBag.ListaProductosRelacionados = ListaProductosRelacionados.OrderByDescending(x => x.imagen).ToList();
           // ViewBag.ListaCatalogos = catalogos;

            return View();
        }
        
        public FileStreamResult ObtenerPDF(int id)
        {
            pdf archivo = db.pdf.Find(id);
            if ((System.IO.File.Exists(Server.MapPath("~/UploadFolder/descripcion_detalle_producto/" + archivo.nombre))))
            {
                FileStream fs = new FileStream(Server.MapPath("~/UploadFolder/descripcion_detalle_producto/") + archivo.nombre, FileMode.Open, FileAccess.Read);
                return File(fs, "application/pdf");
            }
            return null;
        }

        public FileResult DescargarAdjunto(String nombre)
        {
            return File(Server.MapPath("~/UploadFolder/descripcion_detalle_producto/") + nombre, System.Net.Mime.MediaTypeNames.Application.Octet, nombre);
        }

        public String SiguienteAdjunto(int id, int IdProducto)
        {
            List<pdf> la = db.pdf.Where(x => x.id_producto == IdProducto && (x.extension.ToLower().Equals("pdf") || x.extension.ToLower().Equals("png") || x.extension.ToLower().Equals("bmp") || x.extension.ToLower().Equals("jpg") || x.extension.ToLower().Equals("jpeg"))).ToList();
            int actual = -1;
            int siguiente = -1;
            for (int i = 0; i < la.Count; i++)
            {
                if (la.ElementAt(i).id == id)
                {
                    actual = i;
                    break;
                }
            }
            if ((actual + 1) >= la.Count)
            {
                siguiente = 0;
            }
            else
            {
                siguiente = actual + 1;
            }
            pdf a = la.ElementAt(siguiente);
            return a.nombre + "|" + a.id + "|" + a.extension;
        }

        public String AnteriorAdjunto(int id, int IdProducto)
        {
            List<pdf> la = db.pdf.Where(x => x.id_producto == IdProducto && (x.extension.ToLower().Equals("pdf") || x.extension.ToLower().Equals("png") || x.extension.ToLower().Equals("bmp") || x.extension.ToLower().Equals("jpg") || x.extension.ToLower().Equals("jpeg"))).ToList();
            int actual = -1;
            for (int i = 0; i < la.Count; i++)
            {
                if (la.ElementAt(i).id == id)
                {
                    actual = i;
                    break;
                }
            }
            if ((actual - 1) < 0)
            {
                actual = la.Count - 1;
            }
            else
            {
                actual = actual - 1;
            }
            pdf a = la.ElementAt(actual);
            return a.nombre + "|" + a.id + "|" + a.extension;
        }

        //BUSCADOR PRODUCTOS
        /*
        * 0 Busqueda Normal
        * 1 Familia
        * 2 Subfamilia
        * 3 Marca
        */

        public ActionResult busqueda(String filtro, int tipo)
        {
            List<itemproductocarrito2> ListaProductos = new List<itemproductocarrito2>();// db.marca.ToList();
            List<marca> ListaMarcas = new List<marca>();
            String BuscarProductos = "";
            //BUSQUEDA TODO
            if (tipo == 0)
            {
                BuscarProductos += "SELECT distinct(pro.id), pro.codigo,pro.descripcion FROM "
                                        + " producto pro"
                                        + " ,marca mar "
                                        + " ,subfamilia sub "
                                        + " ,familia fam "
                                        + " where "
                                        + " pro.id_marca=mar.id "
                                        + " and pro.publicar=1"
                                        + " and pro.subfamilia_id=sub.id"
                                        + " and fam.id=sub.Familia_id;"
                                        + " and pro.id in (select precio_por_tipocliente.Producto_id from precio_por_tipocliente Where Precio > 0)";
            }
            //BUSQUEDA FAMILIA
            else if (tipo == 1)
            {
                BuscarProductos +=
                          "SELECT distinct( pro.id), pro.codigo,pro.descripcion FROM "
                          + " producto pro"
                          + " ,marca mar "
                          + " ,subfamilia sub "
                          + " ,familia fam "
                          + " where "
                          + " pro.id_marca=mar.id "
                          + " and pro.publicar=1"
                          + " and pro.subfamilia_id=sub.id"
                          + " and fam.id=sub.Familia_id"
                          + " and pro.id in (select precio_por_tipocliente.Producto_id from precio_por_tipocliente Where Precio > 0)";

                //BuscarProductos = "select distinct(pro.id), pro.Codigo, pro.descripcion, pro.precio_dolar, pro.id_marca, pro.SubFamilia_id,"
                //+ " (select precio from precio_por_tipocliente where Producto_id = pro.id and sucursal_id = 1 and TipoCliente_id = 1 and precio> 0) as precio,"
                //+ " pro.iva"
                //+ " from producto pro"
                //+ " where pro.publicar = 1"
                //+ " and pro.id in (4, 113, 264, 389, 390, 391, 392, 393, 394, 395, 400, 407, 408, 409, 428, 450, 452, 453, 454, 455, 456, 460, 461, 462, 463, 464, 466, 467, 468, 469, 471, 472, 473, 475, 476, 478, 764, 765, 766, 767, 768, 769, 770, 771, 777, 820, 821, 822, 831, 832, 834, 863, 887, 888, 894, 895, 907, 908, 909, 911, 913, 916, 917, 918, 930, 931, 932, 934, 935, 936, 937, 1207, 1216, 1218, 1260, 1261, 1264, 1277, 1278, 1279, 1280, 1281, 1282, 1284, 1306, 1317, 1318, 1319, 1320, 1321, 1561, 1562, 1563, 1564, 1565, 1566, 1568, 1569, 1571, 1585, 1586, 1587, 1588, 1589, 1593, 1594, 1597, 1633, 1634, 1891, 1893, 1894, 1928, 1929, 1930, 1931, 1932, 1933, 1934, 1935, 1936, 1938, 1939, 1940, 1941, 1942, 1943, 1945, 1946, 1964, 1975, 1976, 1977, 1978, 1979, 1982, 1989, 1990, 1994, 2497, 2506, 2638, 2667, 2668, 2714, 2715, 2723, 2724, 2725, 2726, 2741, 2742, 2743, 2744, 2752, 2767, 2773, 2786, 2787, 2788, 2806, 2807, 2808, 2809, 2845, 2846, 2847, 2856, 2857, 2858, 2859, 2860, 2861, 2862, 2863, 2864, 2865, 2866, 2868, 2870, 2871, 2872, 2873, 2875, 2876, 2877, 2881, 2882, 2883, 2884, 2885, 2886, 2887, 2888, 2889, 2893, 2907, 2920, 2981, 2982, 2988, 2990, 3031, 3032, 3033, 3034, 3035, 3036, 3037, 3038, 3042, 3043, 3044, 3049, 3050, 3051, 3052, 3053, 3055, 3057, 3058, 3059, 3061, 3097, 3099, 3104, 3105, 3120, 3123, 3124, 3131, 3147, 3149, 3150, 3331, 3332, 3333, 3334, 3335, 3336, 3337, 3338, 3339, 3340, 3341, 3345, 3403, 3404, 3468, 5621, 5622, 5623, 5624, 5625, 5626, 5627, 5628, 5629, 5630, 5631, 5632, 5633, 5634, 5635, 5636, 5637, 5638, 5639, 5640, 5641, 5642, 5643, 5644, 5645, 5670, 5671, 5672, 5673, 5674, 5675, 5676, 5677, 5678, 5684, 5685, 5686, 5687, 5688, 5694, 5695, 5696, 6442, 6487, 6498, 6512, 6610, 6613, 6614, 6622, 6626, 7792, 7795, 7900, 7951, 7978, 8035, 8036, 8037, 8038, 8042, 8043, 8044, 8045, 8046, 8048, 8067, 8069, 8070, 8071, 8077, 8078, 8094, 8097, 8100, 8101, 8102, 8103, 8105, 8106, 8107, 8108, 8109, 8110, 8112, 8114, 8115, 8116, 8122, 8125, 8126, 8127, 8131, 8139, 8142, 8154, 8155, 8156, 8157, 8163, 8164, 8176, 8184, 8371, 8377, 8417, 8418, 8424, 8428, 8534, 8761, 8762, 8763, 8764, 8765, 8766, 8767, 8768, 8769, 8770, 8771, 8772, 8773, 8774, 8775, 8776, 8777, 8778, 8779, 8780, 8781, 8782, 8783, 8784, 8785, 8786, 8787, 8788, 8790, 8791, 8792, 8793, 8794, 8795, 8796, 8797, 8798, 8799, 8800, 8801, 8802, 8806, 8807, 8808, 8809, 8810, 8811, 8812, 8813, 8814, 8815, 8816, 8817, 8818, 8819, 8820, 8821, 8822, 8823, 13146, 13147, 13148, 13149, 13152, 13153, 13154, 13155, 13156, 13158, 13160, 13161, 13162, 13163, 13164, 13167, 13168, 13169, 13170, 13171, 13172, 13174, 13178, 13179, 13180, 13182, 13183, 13184, 13185, 13186, 13187, 13188, 13189, 13190, 13191, 13192, 13193, 13194, 13200, 13209, 13210, 13211, 13212, 13213, 13214, 13219, 13230, 13906, 13907, 13908, 13909, 14801, 14802, 14803, 14804, 14805, 14806, 14807, 14808, 14809, 14810, 14811, 14812, 14813, 14814, 14815, 14816, 14817, 14818, 14819, 14820, 14821, 14822, 14823, 14824, 14825, 14826)"
                //+ " order by pro.Codigo asc";


                if (filtro != "")
                {
                    BuscarProductos += " and pro.subfamilia_id in (select id from subfamilia where Familia_id= " + filtro + " )";//filtro es el id de familia para el tipo 1
                }
                BuscarProductos += ";";
            }
            //BUSQUEDA SUBFAMILIA
            else if (tipo == 2)
            {
                BuscarProductos +=
                          "SELECT distinct( pro.id), pro.codigo,pro.descripcion FROM "
                          + " producto pro"
                          + " ,marca mar "
                          + " ,subfamilia sub "
                          + " ,familia fam "
                          + " where "
                          + " pro.id_marca=mar.id "
                          + " and pro.publicar=1"
                          + " and pro.subfamilia_id=sub.id"
                          + " and fam.id=sub.Familia_id"
                          + " and pro.id in (select precio_por_tipocliente.Producto_id from precio_por_tipocliente Where Precio > 0)";
                if (filtro != "")
                {
                    BuscarProductos += " and pro.subfamilia_id=" + filtro;//filtro es el id de subfamilia para el tipo 2
                }
                BuscarProductos += ";";
            }
            //BUSQUEDA NORMAL
            else
            {
                //busqueda desde filtro
                BuscarProductos +=
                          "SELECT distinct( pro.id), pro.codigo,pro.descripcion FROM "
                          + " producto pro"
                          + " ,marca mar "
                          + " ,subfamilia sub "
                          + " ,familia fam "
                          + " where "
                          + " pro.id_marca=mar.id "
                          + " and pro.publicar=1"
                          + " and pro.subfamilia_id=sub.id"
                          + " and fam.id=sub.Familia_id"
                          + " and pro.id in (select precio_por_tipocliente.Producto_id from precio_por_tipocliente Where Precio > 0)";
                if (filtro != "" && filtro != null)
                {
                    BuscarProductos += " and(";
                    int x = 1;
                    //tipo 2 es una subfailia - no se separan las palabras
                    if (tipo == 2)
                    {
                        BuscarProductos += " ( pro.codigo like '%" + filtro + "%'"
                        + " or pro.descripcion like '%" + filtro + "%'"
                        + " or fam.nombre like '%" + filtro + "%'"
                        + " or  sub.nombre like '%" + filtro + "%'"
                        + " or mar.nombre like '%" + filtro + "%'  COLLATE utf8_general_ci )";
                    }
                    else
                    {
                        BuscarProductos += " ( pro.codigo like '%" + filtro + "%'"
                            + " or pro.descripcion like '%" + filtro + "%'"
                            + " or fam.nombre like '%" + filtro + "%'"
                            + " or  sub.nombre like '%" + filtro + "%'"
                            + " or mar.nombre like '%" + filtro + "%'  COLLATE utf8_general_ci )";
                    }
                    BuscarProductos += ");";
                }

            }

            DataSet ds = Fn.ConnDB.GetDataSet(BuscarProductos, Server.MapPath("").Split('\\').ElementAt(3));
            DataTable dt = ds.Tables[0];
            ListaProductos = (from DataRow row in dt.Rows select new itemproductocarrito2 { Id = Convert.ToInt32(row["Id"]) }).ToList();
            if (ListaProductos.Count() == 1)
            {
                return RedirectToAction("DetalleProducto", "Producto", new { id_producto = ListaProductos.First().Id });
            }
            else
            {
                ViewBag.ListaIdProductos = ListaProductos.Select(x => x.Id).ToList();
                List<marca> Marcas = new List<marca>();
                if (ListaProductos.Count() != 0)
                {
                    String BuscarMarcas =
                               "SELECT distinct( mar.id), mar.nombre FROM "
                               + " producto pro"
                               + ", marca mar "
                               + " ,subfamilia sub "
                               + " ,familia fam "
                               + " where "
                               + " pro.id_marca=mar.id "
                               + " and pro.subfamilia_id=sub.id"
                               + " and fam.id=sub.Familia_id ";
                    itemproductocarrito2 last = ListaProductos.Last();
                    BuscarMarcas += " and pro.id in (";
                    foreach (itemproductocarrito2 producto in ListaProductos)
                    {
                        BuscarMarcas += producto.Id;
                        if (producto != last)
                        {
                            BuscarMarcas += ",";
                        }

                    }
                    BuscarMarcas += ")";
                    BuscarMarcas += ";";
                    ds = Fn.ConnDB.GetDataSet(BuscarMarcas, Server.MapPath("").Split('\\').ElementAt(3));
                    dt = ds.Tables[0];
                    Marcas = (from DataRow row in dt.Rows select new marca { id = Convert.ToInt32(row["id"]), nombre = Convert.ToString(row["nombre"]) }).ToList();
                }
                ViewBag.ListaMarcas = Marcas;
                return View();
            }
        }

        public PartialViewResult VistaParcialProductos(int pagina, int numeroProductos, String filtro, int ordenamiento, List<int> Marcas, List<int> ListaIdProductos /*, List<int> Familias, List<int> Subfamilias*/)
        {
            Fn.FnIva.establecerValoresIniciales();
            configuracion_general configuracion_general = Fn.Configuracion.TraerConfiguracionGeneral();
            configuracion_tienda_online configuracion_tienda_online = Fn.Configuracion.TraerConfiguracionTiendaOnline();
            List<itemproductocarrito2> ListaProductos = new List<itemproductocarrito2>();
            int id_tipo_precio = 1;
            if (Fn.cliente_logeado.TraerCliente().id != 0)
                id_tipo_precio = Fn.cliente_logeado.TraerCliente().TipoCliente_id;
            if (ListaIdProductos != null)
            {
                string query = "select distinct(pro.id), pro.Codigo, pro.descripcion, pro.precio_dolar, pro.id_marca, pro.SubFamilia_id, (select precio from precio_por_tipocliente where Producto_id=pro.id and sucursal_id=1 and TipoCliente_id=" + id_tipo_precio + " and precio>0) as precio, pro.iva"
                            + " from producto pro"
                            + " where "
                            + " (select precio from precio_por_tipocliente where Producto_id=pro.id and sucursal_id=1 and TipoCliente_id=1 and precio>0) is not null and "
                            + " pro.publicar = 1";


                //FILTRAR POR MARCAS SELECCIONADAS
                if (Marcas != null)
                {
                    int last = Marcas.Last();
                    query += " and pro.id_marca in (";
                    foreach (int idMarca in Marcas)
                    {
                        query += idMarca;
                        query = idMarca != last ? query += "," : query;
                    }
                    query += ") ";
                }
                //FILTRO SECUNDARIO
                if (filtro != "")
                {
                    foreach (string _filtro in filtro.Replace("|", " ").Split(' '))
                    {
                        query += " and( pro.codigo like '%" + _filtro + "%'"
                            + " or pro.descripcion like '%" + _filtro + "%'"
                            + " or pro.descripcion like '%" + _filtro + "%' COLLATE utf8_general_ci )";
                    }
                }
                //FILTRAR POR LOS IDS DE PRODUCTO FILTRADOS EN EL CAMPO BUSQUEDA PRINCIPAL
                if (ListaIdProductos.Count != 0)
                {
                    int last = ListaIdProductos.Last();
                    query += " and pro.id in (";
                    foreach (int idProducto in ListaIdProductos)
                    {
                        query += idProducto;
                        query = idProducto != last ? query += "," : query;
                    }
                    query += ")";
                }
                //ORDENA
                if (ordenamiento == 1)
                    query += " order by pro.Codigo asc";
                if (ordenamiento == 2)
                    query += " order by pro.descripcion asc";
                if (ordenamiento == 3)
                    query += " order by pre.Precio asc";
                if (ordenamiento == 4)
                    query += " order by inv.cantidad asc";
                DataSet ds = Fn.ConnDB.GetDataSet(query, Server.MapPath("").Split('\\').ElementAt(3));
                DataTable dt = ds.Tables[0];
                ListaProductos = (from DataRow row in dt.Rows
                                  select new itemproductocarrito2
                                  {
                                      Id = Convert.ToInt32(row["Id"]),
                                      Codigo = row["Codigo"].ToString(),
                                      Descripcion = row["Descripcion"].ToString(),
                                      Precio_Dolar = Convert.ToInt32(row["Precio_Dolar"]),
                                      id_marca = Convert.ToInt32(row["id_marca"]),
                                      SubFamilia_id = Convert.ToInt32(row["SubFamilia_id"]),
                                      Precio_Unitario = Convert.ToDecimal(row["Precio"]),
                                      iva = Convert.ToDecimal(row["iva"]),
                                      imagen = ""
                                  }).ToList();
                List<int> ListaSubfamilias = new List<int>();
                if (Marcas != null)
                    ListaProductos = ListaProductos.Where(x => Marcas.Contains(x.id_marca)).ToList();
            }
            int totalProductos = ListaProductos.Count();
            ListaProductos = ListaProductos.Skip(((pagina - 1) * numeroProductos)).Take(numeroProductos).ToList();
            foreach (itemproductocarrito2 producto in ListaProductos)
                if (db.imagen.Where(x => x.id_producto == producto.Id).Any())
                    producto.imagen = db.imagen.Where(x => x.id_producto == producto.Id).FirstOrDefault().nombre;
            /**
             * Para traer los inventario por producto
             * **/
            if (configuracion_tienda_online.mostrar_inventario == true)
            {
                List<int> lista_id_sucursales = db.sucursal.Where(x => x.alimenta_tienda_online == true).Select(y => y.id).ToList();
                foreach (itemproductocarrito2 producto in ListaProductos)
                {
                    Decimal cantidad_en_inventario = Convert.ToDecimal(db.inventario.Where(x => x.Producto_id == producto.Id && lista_id_sucursales.Contains(x.sucursal_id)).Select(x => x.cantidad).Sum());
                    producto.Inventario = cantidad_en_inventario;
                } 
            }
            foreach (itemproductocarrito2 producto in ListaProductos)
            {
                producto.cantidad_iva_unitario = 0;
                Decimal total_porcentaje = 100 + producto.iva;
                Decimal precioActual = db.precio_por_tipocliente.Where(x => x.Producto_id == producto.Id && x.TipoCliente_id == id_tipo_precio && x.sucursal_id == 1).Single().Precio.Value;
                if (configuracion_general.precios_netos == true)
                {
                    producto.Precio_Unitario = (precioActual / total_porcentaje) * 100;
                    if (total_porcentaje > 100)
                        producto.cantidad_iva_unitario = Math.Round((precioActual / total_porcentaje) * (total_porcentaje - 100), 2);
                }
                else
                {
                    if (total_porcentaje > 100)
                        producto.cantidad_iva_unitario = Math.Round((producto.Precio_Unitario / 100) * (total_porcentaje - 100), 2);
                }

                producto.cantidad_iva_unitario = Fn.FnIva.obtenerIvaUnitarioProducto(producto.Id);
            }
            ViewBag.ListaProductos = ListaProductos.OrderByDescending(x => x.imagen).ToList();
            ViewBag.totalProductos = totalProductos;
            if (totalProductos == 0)
                ViewBag.desde = 0;
            else
                ViewBag.desde = ((pagina - 1) * numeroProductos) + 1;
            if (totalProductos < ((pagina - 1) * numeroProductos) + numeroProductos)
                ViewBag.hasta = totalProductos;
            else
                ViewBag.hasta = ((pagina - 1) * numeroProductos) + numeroProductos;
            ViewBag.pagina = pagina;
            ViewBag.numeroProductos = numeroProductos;
            return PartialView();
        }

        public JsonResult traer_cantidad_inventario(String codigo_producto)
        {
            Decimal cantidad_en_inventario = 0;
            try
            {
                List<int> lista_id_sucursales = db.sucursal.Where(x => x.alimenta_tienda_online == true).Select(y => y.id).ToList();
                producto pro = db.producto.FirstOrDefault(x => x.Codigo.Equals(codigo_producto));
                List<inventario> lista_inventario = db.inventario.Where(x => x.Producto_id == pro.id && lista_id_sucursales.Contains(x.sucursal_id)).ToList();
                if (lista_inventario != null && lista_inventario.Count > 0)
                    cantidad_en_inventario = Convert.ToDecimal(lista_inventario.Select(x => x.cantidad).Sum());
                var respuesta = new
                {
                    success = true,
                    cantidad_en_inventario = cantidad_en_inventario
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);

            }
            catch (Exception)
            {
                var respuesta = new
                {
                    success = false,
                    cantidad_en_inventario = 0
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
                throw;
            }
        }

    }
}
