﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Tienda_Online.Models;
using System.Data;
using System.Data.Entity;
using System.IO;
using Tienda_Online.Fn;

namespace Tienda_Online.Controllers
{
    public class CuentaController : Controller
    {

        POSEntities db = new POSEntities();

        public ActionResult Registro()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Registro(cliente cliente)
        {
            Boolean exito;
            try
            {
                cliente.id_estatus_cliente = 2;
                cliente.Limite_credito = 0;
                cliente.Adeudo_credito = 0;
                cliente.dias_credito = 0;
                cliente.TipoCliente_id = 1;
                cliente.id_sucursal = 1;
                db.cliente.Add(cliente);
                db.SaveChanges();
                exito = true;
                var respuesta = new
                {
                    success = exito,
                    id_cliente = cliente.id
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                exito = false;
                var respuesta = new
                {
                    success = exito,
                    id_cliente = 0
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult LogOn()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LogOn(LogOnModel model)
        {
            if (ModelState.IsValid)
            {
                cliente cliente = db.cliente.Where(x => x.correo == model.UserName && x.contrasena == model.Password).SingleOrDefault();
                if (cliente != null)
                {
                    if (cliente.id_estatus_cliente == 1)
                    {
                        FormsAuthentication.SetAuthCookie(model.UserName, model.RememberMe);
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        ViewBag.error = "El cliente no ha sido activado. Por favor contacte al administrador.";
                        return View(model);
                    }
                }
                else
                {
                    ViewBag.error = "El correo o la contraseña son incorrectos.";
                    return View(model);
                }
            }
            ViewBag.error = "Introduzca el correo y la contraseña.";
            return View(model);
        }

        public PartialViewResult VistaParcialLogOn()
        {
            return PartialView();
        }
        [Authorize]
        public ActionResult MiCuenta()
        {
            cliente cliente = Fn.cliente_logeado.TraerCliente();
            ViewBag.cliente = cliente;
            ViewBag.tipoCambiario = cliente.id_sucursal != null ?
                Math.Round(cliente.sucursal.precio_dolar.Value, 2)
                : Math.Round(db.sucursal.First().precio_dolar.Value, 2);
            return View();
        }

        public PartialViewResult VistaParcialHistorialCotizaciones(int id_cliente)
        {
            List<venta> ListaCotizaciones = db.venta.Where(x => (x.catalogo_status_id == 7 || x.catalogo_status_id == 4) && x.Cliente_id == id_cliente).ToList();
            ViewBag.ListaCotizaciones = ListaCotizaciones;
            return PartialView();
        }

        public PartialViewResult VistaParcialHistorialCompras(int id_cliente)
        {
            List<int> ListaIDVentaOnline = new List<int> { 8, 9, 10, 11 };
            List<venta> ListaVentas = db.venta.Where(x => ListaIDVentaOnline.Contains(x.catalogo_status_id) && x.Cliente_id == id_cliente).ToList();
            ViewBag.ListaVentas = ListaVentas;
            return PartialView();
        }

        public PartialViewResult ModalAnexosVenta(int id)
        {
            ViewBag.venta = db.venta.Find(id);
            return PartialView(Fn.FnFiles.BuscarCoincidenciasArchivos("UploadFolder/adjuntos_cliente_compra", id + "-"));
        }

        public PartialViewResult ModalConfirmarRecibido(int id)
        {
            ViewBag.id_cotizacion = id;
            cliente cliente = Fn.cliente_logeado.TraerCliente();
            ViewBag.tipoCambiario = cliente.id_sucursal != null ?
                Math.Round(cliente.sucursal.precio_dolar.Value, 2)
                : Math.Round(db.sucursal.First().precio_dolar.Value, 2);
            return PartialView(db.venta.Find(id));
        }

        public ActionResult confirmar_venta_recibido(int id)
        {
            try
            {
                venta venta = db.venta.Find(id);
                venta.catalogo_status_id = 11;
                venta.Fecha_entrega = DateTime.Now;
                db.Entry(venta).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                var respuesta = new
                {
                    success = true
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var respuesta = new
                {
                    success = false,
                    error = e.Message
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
        }

        public ViewResult EditarCuenta(int id)
        {
            cliente cliente = null;
            if (Fn.cliente_logeado.TraerCliente().id == id)
            {
                cliente = Fn.cliente_logeado.TraerCliente();
            }
            ViewBag.cliente = cliente;
            return View();
        }

        [HttpPost]
        public ActionResult EditarCuenta(cliente cliente)
        {
            Boolean success = false;
            try
            {
                cliente clienteActualizar = db.cliente.Find(cliente.id);
                clienteActualizar.Razon_social = cliente.Razon_social;
                clienteActualizar.RFC = cliente.RFC;
                clienteActualizar.contrasena = cliente.contrasena;
                clienteActualizar.calle = cliente.calle;
                clienteActualizar.exterior = cliente.exterior;
                clienteActualizar.interior = cliente.interior;
                clienteActualizar.colonia = cliente.colonia;
                clienteActualizar.municipio = cliente.municipio;
                clienteActualizar.estado = cliente.estado;
                clienteActualizar.cp = cliente.cp;
                clienteActualizar.correo = cliente.correo;
                clienteActualizar.telefono = cliente.telefono;
                clienteActualizar.calle_embarque = cliente.calle_embarque;
                clienteActualizar.exterior_embarque = cliente.exterior_embarque;
                clienteActualizar.interior_embarque = cliente.interior_embarque;
                clienteActualizar.colonia_embarque = cliente.colonia_embarque;
                clienteActualizar.municipio_embarque = cliente.municipio_embarque;
                clienteActualizar.estado_embarque = cliente.estado_embarque;
                clienteActualizar.cp_embarque = cliente.cp_embarque;
                clienteActualizar.domicilio_embarque_igual = cliente.domicilio_embarque_igual;
                clienteActualizar.interior_embarque = cliente.interior_embarque;
                db.Entry(clienteActualizar).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                success = true;
            }
            catch (Exception e)
            {
                success = false;
            }
            var respuesta = new
            {
                success = success,
                id_cliente = 0
            };
            return Json(respuesta, JsonRequestBehavior.AllowGet);
        }

        public ViewResult EditarCotizacion(int id_cotizacion)
        {
            venta cotizacion = db.venta.Find(id_cotizacion);
            ViewBag.cotizacion = cotizacion;
            return View();
        }

        public ViewResult verCotizacion(int id)
        {
            cliente cliente = Fn.cliente_logeado.TraerCliente();
            ViewBag.tipoCambiario = cliente.id_sucursal != null ?
                Math.Round(cliente.sucursal.precio_dolar.Value, 2)
                : Math.Round(db.sucursal.First().precio_dolar.Value, 2);
            ViewBag.serieFolio = Fn.Numeros.serie_folio_by_id(id);
            venta v = db.venta.Find(id);
            return View(v);
        }
        public ViewResult verVenta(int id)
        {
            venta v = db.venta.Find(id);
            ViewBag.serieFolio = Fn.Numeros.serie_folio_by_id(id);
            ViewBag.tipoCambiario = Math.Round(v.precio_dolar_venta.Value, 2);
            ViewBag.venta = v;
            return View();
        }

        [HttpPost]
        public ActionResult EditarCotizacion(int id_cotizacion, List<conceptos_por_venta> conceptos)
        {
            Boolean success;
            try
            {
                Fn.FnIva.establecerValoresIniciales();
                decimal subtotal = 0;
                decimal iva = 0;
                decimal total = 0;
                venta cotizacion = db.venta.Find(id_cotizacion);
                foreach (conceptos_por_venta concepto in cotizacion.conceptos_por_venta.ToList())
                {
                    decimal subtotal_inidvidual;
                    decimal iva_individual;
                    decimal total_individual;
                    if (conceptos.Select(x => x.id).Contains(concepto.id))
                    {
                        conceptos_por_venta nuevo_concepto_por_venta = new conceptos_por_venta();
                        Decimal cantidad = conceptos.Where(x => x.id == concepto.id).Single().cantidad;
                        concepto.cantidad = cantidad;
                        nuevo_concepto_por_venta = Fn.FnIva.obtenerConceptoVenta(concepto);
                        concepto.iva_unitario = nuevo_concepto_por_venta.iva_unitario;
                        concepto.precio_unitario_mas_iva = nuevo_concepto_por_venta.precio_unitario_mas_iva;
                        concepto.importe = nuevo_concepto_por_venta.importe;
                        concepto.iva_importe = nuevo_concepto_por_venta.iva_importe;
                        concepto.importe_mas_iva = nuevo_concepto_por_venta.importe_mas_iva;
                        db.Entry(concepto).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        subtotal_inidvidual = Fn.FnIva.obtenerSubtotalConcepto(Convert.ToDecimal(concepto.precio_unitario), concepto.cantidad);
                        iva_individual = Fn.FnIva.obtenerIvaConcepto(Convert.ToDecimal(concepto.iva_unitario), concepto.cantidad);
                        total_individual = Fn.FnIva.obtenerTotalConcepto((subtotal_inidvidual + iva_individual), concepto.cantidad);
                        subtotal += subtotal_inidvidual;
                        iva += iva_individual;
                        total += total_individual;
                    }
                    else
                    {
                        db.conceptos_por_venta.Remove(concepto);
                        db.SaveChanges();
                    }
                }
                cotizacion.subtotal = subtotal;
                cotizacion.iva = iva;
                cotizacion.total = total;
                db.Entry(cotizacion).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                success = true;
                var respuesta = new
                {
                    success = success,
                    idventa = cotizacion.id,
                    id_cotizacion = Fn.Numeros.serie_folio_by_id(cotizacion.id)
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                success = false;
                var respuesta = new
                {
                    success = success
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
        }

        public PartialViewResult ModalConfirmarCotizacion(int id_cotizacion)
        {
            ViewBag.id_cotizacion = id_cotizacion;
            cliente cliente = Fn.cliente_logeado.TraerCliente();
            ViewBag.tipoCambiario = cliente.id_sucursal != null ?
                Math.Round(cliente.sucursal.precio_dolar.Value, 2)
                : Math.Round(db.sucursal.First().precio_dolar.Value, 2);
            return PartialView(db.venta.Find(id_cotizacion));
        }
        public PartialViewResult TraerArchivosOrdenesCompra(int id_cotizacion)
        {
            ViewBag.id_cotizacion = id_cotizacion;
            return PartialView(Fn.FnFiles.BuscarCoincidenciasArchivos("UploadFolder/adjuntos_cliente_compra", id_cotizacion + "-"));
        }

        public ActionResult eliminarArchivo(string FileName, string carpeta)
        {
            bool success = false;
            try
            {

                if (Fn.FnFiles.existe(carpeta, FileName))
                    Fn.FnFiles.elimina(carpeta, FileName);
                success = true;
            }
            catch (Exception)
            {
                success = false;
            }
            var respuesta = new
            {
                success = success
            };
            return Json(respuesta, JsonRequestBehavior.AllowGet);
        }

        public ActionResult TerminarCotizacion(int id_cotizacion)
        {
            bool success = false;
            string error = "";
            try
            {
                decimal subtotal = 0;
                decimal iva = 0;
                decimal total = 0;
                venta v = db.venta.Find(id_cotizacion);
                /* Para actulizar la cotizacion en caso de que exista un cambo con 
                 * respecto al inventario  */
                configuracion_general configuracion_general = Fn.Configuracion.TraerConfiguracionGeneral();
                if (configuracion_general.respetar_inventario == true)
                {
                    Decimal cantidad_en_inventario = 0;
                    List<int> lista_id_sucursales = db.sucursal.Where(x => x.alimenta_tienda_online == true).Select(y => y.id).ToList();
                    List<conceptos_por_venta> lista_conceptos_por_venta = db.conceptos_por_venta.Where(x => x.Venta_id == v.id).ToList();
                    List<int> lista_cpv_borrar = new List<int>();
                    List<int> lista_cpv_actualizar = new List<int>();
                    
                    foreach (conceptos_por_venta cpv in lista_conceptos_por_venta)
                    {
                        producto pro = db.producto.Find(cpv.Producto_id);
                        List<inventario> lista_inventario = db.inventario.Where(x => x.Producto_id == pro.id && lista_id_sucursales.Contains(x.sucursal_id)).ToList();

                        if (lista_inventario != null && lista_inventario.Count > 0)
                            cantidad_en_inventario = Convert.ToDecimal(lista_inventario.Select(x => x.cantidad).Sum());
                        //Si no existe inventario procedemos a eliminra el concepto por venta
                        if (cantidad_en_inventario <= 0)
                        {
                            lista_cpv_borrar.Add(cpv.id);
                        }
                        //si la cantidad en inventario es menor que la cantidad guardada procedemos a descontar la cantidad 
                        else if (cantidad_en_inventario < cpv.cantidad)
                        {
                            lista_cpv_actualizar.Add(cpv.id);
                        }
                    }
                    if (lista_cpv_borrar != null && lista_cpv_borrar.Count > 0)
                    {
                        foreach (int x in lista_cpv_borrar)
                        {
                            db.conceptos_por_venta.Remove(db.conceptos_por_venta.Find(x));
                        }
                        db.SaveChanges();
                    }
                    if (lista_cpv_actualizar != null && lista_cpv_actualizar.Count > 0)
                    {
                        foreach (int x in lista_cpv_actualizar)
                        {
                            conceptos_por_venta cpv = db.conceptos_por_venta.Find(x);
                            producto pro = db.producto.Find(cpv.Producto_id);
                            List<inventario> lista_inventario = db.inventario.Where(a => a.Producto_id == pro.id && lista_id_sucursales.Contains(a.sucursal_id)).ToList();

                            if (lista_inventario != null && lista_inventario.Count > 0)
                                cantidad_en_inventario = Convert.ToDecimal(lista_inventario.Select(y => y.cantidad).Sum());
                            cpv.cantidad = cantidad_en_inventario;
                            cpv = Fn.FnIva.obtenerConceptoVenta(cpv);
                            db.Entry(cpv).State = System.Data.Entity.EntityState.Modified;
                        }
                        db.SaveChanges();
                    }
                    
                    v.subtotal = Fn.FnIva.obtenerSubtotalVenta(v);
                    v.iva = Fn.FnIva.obtenerIvaVenta(v);
                    v.total = Fn.FnIva.obtenerTotalVenta(v);
                    db.Entry(v).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
                string Serie_de_venta_online = Models.Config.Serie_de_venta_online();
                if (v.Serie != Serie_de_venta_online)//  "T"=esta venta ya se ha entregado
                {
                    v.catalogo_status_id = 13;
                    int tipoCliente_id = Fn.cliente_logeado.TraerCliente().TipoCliente_id;
                    Decimal precioDolar = Math.Round(v.sucursal.precio_dolar.Value, 2);
                    foreach (conceptos_por_venta c in v.conceptos_por_venta)
                    {
                        producto p = db.producto.Find(c.Producto_id);
                        conceptos_por_venta nuevo_concepto_por_venta = new conceptos_por_venta();
                        nuevo_concepto_por_venta = Fn.FnIva.obtenerConceptoVenta(c);
                        c.TipoCliente_id = nuevo_concepto_por_venta.TipoCliente_id;
                        c.precio_unitario = nuevo_concepto_por_venta.precio_unitario;
                        c.precio_unitario_original = nuevo_concepto_por_venta.precio_unitario_original;
                        c.precio_en_dolares = nuevo_concepto_por_venta.precio_en_dolares;
                        c.precio_en_dolares_original = nuevo_concepto_por_venta.precio_en_dolares_original;
                        c.iva_unitario = nuevo_concepto_por_venta.iva_unitario;
                        c.precio_unitario_mas_iva = nuevo_concepto_por_venta.precio_unitario_mas_iva;
                        c.importe = nuevo_concepto_por_venta.importe;
                        c.iva_importe = nuevo_concepto_por_venta.iva_importe;
                        c.importe_mas_iva = nuevo_concepto_por_venta.importe_mas_iva;
                        subtotal += c.importe.Value;
                        iva += c.iva_importe.Value;
                        total += c.importe_mas_iva.Value;
                    }
                    db.SaveChanges();
                    v.Order_de_compra_filename = "";
                    v.precio_dolar_venta = precioDolar;
                    v.subtotal = subtotal;
                    v.iva = iva;
                    v.total = total;
                    db.SaveChanges();
                    venta venta_confirmada = new venta();
                    venta_confirmada.Cliente_id = v.Cliente_id;
                    venta_confirmada.fecha = DateTime.Now;
                    venta_confirmada.catalogo_status_id = 8;
                    venta_confirmada.sucursal_id = v.sucursal_id;
                    venta_confirmada.usuario_id = v.usuario_id;
                    venta_confirmada.Folio = Convert.ToInt16(db.venta.Where(x => x.Serie == Serie_de_venta_online && x.sucursal_id == v.sucursal_id).Select(x => x.Folio).Max()) + 1;
                    venta_confirmada.Serie = Models.Config.Serie_de_venta_online();//"W"
                    venta_confirmada.aduedo = v.aduedo;
                    venta_confirmada.subtotal = v.subtotal;
                    venta_confirmada.iva = v.iva;
                    venta_confirmada.total = v.total;
                    venta_confirmada.precio_dolar_venta = precioDolar;
                    venta_confirmada.observaciones = v.observaciones;
                    db.venta.Add(venta_confirmada);
                    db.SaveChanges();
                    List<String> ListaArchivos = Fn.FnFiles.BuscarCoincidenciasArchivos("UploadFolder/adjuntos_cliente_compra", v.id + "-");
                    foreach (String archivo in ListaArchivos)
                    {
                        String archivo_nuevo = venta_confirmada.id +  archivo.Substring(archivo.Split('-')[0].Length);
                        DirectoryInfo hdDirectoryInWhichToSearch = new DirectoryInfo(Server.MapPath("~/UploadFolder/adjuntos_cliente_compra/"));
                        String ruta = hdDirectoryInWhichToSearch.ToString();
                        System.IO.File.Move(ruta + archivo, ruta + archivo_nuevo);
                    }
                    foreach (conceptos_por_venta concepto in v.conceptos_por_venta.ToList())
                    {
                        conceptos_por_venta conceptos_por_venta = new conceptos_por_venta();
                        conceptos_por_venta.Venta_id = venta_confirmada.id;
                        conceptos_por_venta.Producto_id = concepto.Producto_id;
                        conceptos_por_venta.TipoCliente_id = concepto.TipoCliente_id;
                        conceptos_por_venta.cantidad = concepto.cantidad;
                        conceptos_por_venta.precio_unitario = concepto.precio_unitario;
                        conceptos_por_venta.precio_unitario_original = concepto.precio_unitario_original;
                        conceptos_por_venta.iva_unitario = concepto.iva_unitario;
                        conceptos_por_venta.precio_unitario_mas_iva = concepto.precio_unitario_mas_iva;
                        conceptos_por_venta.importe = concepto.importe;
                        conceptos_por_venta.iva_importe = concepto.iva_importe;
                        conceptos_por_venta.importe_mas_iva = concepto.importe_mas_iva;
                        conceptos_por_venta.precio_en_dolares = concepto.precio_en_dolares;
                        conceptos_por_venta.precio_en_dolares_original = concepto.precio_en_dolares_original;
                        db.conceptos_por_venta.Add(conceptos_por_venta);
                    }
                    db.SaveChanges();
                    success = true;
                }
                else
                {
                    success = false;
                    error = "La venta ya ha sido confirmada anteriormente";
                }
            }
            catch (Exception)
            {
                success = false;
            }
            var respuesta = new
            {
                success = success,
                error = error
            };
            return Json(respuesta, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public void SubirArchivoCotizacionFinal(int id)
        {
            UploadedFile file = RetrieveFileFromRequest();
            String extension = System.IO.Path.GetExtension(file.Filename).ToLower();
            String FileName = id + "-" + file.Filename;
            if (Fn.FnFiles.subirArchivo(file, "UploadFolder/adjuntos_cliente_compra", FileName))
            {
                venta v = db.venta.Find(id);
                try
                {
                    v.Order_de_compra_filename = FileName;
                    db.SaveChanges();
                }
                catch (Exception)
                {
                    v.Order_de_compra_filename = null;
                    db.SaveChanges();
                }
            }
        }

        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

        public void LogOffLast()
        {
            FormsAuthentication.SignOut();
        }

        public PartialViewResult ModalEliminarCotización(int id_cotizacion)
        {
            ViewBag.id_cotizacion = id_cotizacion;
            return PartialView();
        }

        public ActionResult eliminarCotizacion(int id_cotizacion)
        {
            bool success = false;
            try
            {
                db.venta.Find(id_cotizacion).catalogo_status_id = 10;
                db.SaveChanges();
                success = true;
            }
            catch (Exception)
            {
                success = false;
            }
            var respuesta = new
            {
                success = success
            };
            return Json(respuesta, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult ModalGuardarCotizacion(String id_cotizacion, int id_venta)
        {
            ViewBag.id_cotizacion = id_cotizacion;
            ViewBag.id_venta = id_venta;
            return PartialView();
        }

        public PartialViewResult ModalErrorCotizacion()
        {
            return PartialView();
        }

        public PartialViewResult ModalRegistroExitoso()
        {
            return PartialView();
        }

        public PartialViewResult ModalRegistroErroneo()
        {
            return PartialView();
        }

        [HttpPost]
        public void UploadFile_IMG(int idCliente)
        {
            UploadedFile file = RetrieveFileFromRequest();
            string nombre_imagen = idCliente + "-" + file.Filename.Trim();
            string rootPath = AppDomain.CurrentDomain.BaseDirectory;
            String savedFileIMG = rootPath + "/UploadFolder/imagen_cliente/" + nombre_imagen;
            //guardar imagen
            System.IO.File.WriteAllBytes(Server.MapPath("~/UploadFolder/imagen_cliente/") + nombre_imagen, file.Contents);
            String extension = System.IO.Path.GetExtension(file.Filename).ToLower();
            cliente cliente = db.cliente.Find(idCliente);
            //si el cliente tiene una imagen registrada buscarla y si existe eliminarla
            if (System.IO.File.Exists(rootPath + "/UploadFolder/imagen_cliente/" + cliente.imagen))
            {
                System.IO.File.Delete(rootPath + "/UploadFolder/imagen_cliente/" + cliente.imagen);
            }
            //guardar nuevo nombre en en el registro de cliente
            cliente.imagen = cliente.id + "-" + file.Filename;
            db.Entry(cliente).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        [HttpPost]
        public void UploadOrdenCompra(int idCotizacion)
        {
            UploadedFile file = RetrieveFileFromRequest();
            string nombre_archivo = idCotizacion + "-" + file.Filename.Trim().Replace("&", "");
            string rootPath = AppDomain.CurrentDomain.BaseDirectory;
            String savedFileIMG = rootPath + "/UploadFolder/adjuntos_cliente_compra/" + nombre_archivo;
            String extension = System.IO.Path.GetExtension(file.Filename).ToLower();
            //si el cliente tiene una imagen registrada buscarla y si existe eliminarla
            if (System.IO.File.Exists(savedFileIMG))
            {
                //si ya existe entonces agrega la fecha antes de la extensión para que tenga un nombre único
                string fecha = DateTime.Now.ToString("yyyyMMddHHmmss");
                nombre_archivo = nombre_archivo.Replace(extension, "") + fecha + extension;
            }
            //guardar archivo
            System.IO.File.WriteAllBytes(rootPath + "/UploadFolder/adjuntos_cliente_compra/" + nombre_archivo, file.Contents);
            //guardar nuevo nombre en en el registro de cliente
            venta v = db.venta.Find(idCotizacion);
            v.Order_de_compra_filename = nombre_archivo;
            db.Entry(v).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        [HttpPost]
        public void SubirArchivo(int id)
        {
            UploadedFile file = RetrieveFileFromRequest();
            String extension = System.IO.Path.GetExtension(file.Filename).ToLower();
            cliente cliente = db.cliente.Find(id);
            cliente.imagen = cliente.id + "-" + file.Filename;
            db.Entry(cliente).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            System.IO.File.WriteAllBytes(Server.MapPath("~/UploadFolder/imagen_cliente/") + cliente.imagen, file.Contents);
        }

        private UploadedFile RetrieveFileFromRequest()
        {
            string filename = null;
            string fileType = null;
            byte[] fileContents = null;
            string ID;

            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];
                fileContents = new byte[file.ContentLength];
                fileType = file.ContentType;
                filename = file.FileName;

            }
            else if (Request.ContentLength > 0)
            {
                fileContents = new byte[Request.ContentLength];
                Request.InputStream.Read(fileContents, 0, Request.ContentLength);
                filename = Request.Headers["X-File-Name"];
                fileType = Request.Headers["X-File-Type"];
            }
            return new UploadedFile()
            {
                Filename = filename,
                ContentType = fileType,
                FileSize = fileContents != null ? fileContents.Length : 0,
                Contents = fileContents
            };
        }

        public bool VerificarRazonSocial(int id, String Razon_social)
        {
            bool existe;
            if (id == 0)
                existe = db.cliente.Where(x => x.Razon_social.ToLower().Equals(Razon_social.ToLower())).Any();
            else
                existe = db.cliente.Where(x => x.id != id && x.Razon_social.ToLower().Equals(Razon_social.ToLower())).Any();
            return existe;
        }

        public bool VerificarRFC(int id, String RFC)
        {
            bool existe;
            if (id == 0)
                existe = db.cliente.Where(x => x.RFC.ToLower().Equals(RFC.ToLower())).Any();
            else
                existe = db.cliente.Where(x => x.id != id && x.RFC.ToLower().Equals(RFC.ToLower())).Any();
            return existe;
        }

        public bool VerificarCorreo(int id, String correo)
        {
            bool existe;
            if (id == 0)
                existe = db.cliente.Where(x => x.correo.ToLower().Equals(correo.ToLower())).Any();
            else
                existe = db.cliente.Where(x => x.id != id && x.correo.ToLower().Equals(correo.ToLower())).Any();
            return existe;
        }
        
        public ActionResult archivosAnexadosCount(int id_cotizacion)
        {
            bool success = false;
            int count = 0;
            try
            {
                count = Fn.FnFiles.BuscarCoincidenciasArchivos("UploadFolder/adjuntos_cliente_compra", id_cotizacion + "-").Count();
                success = true;
            }
            catch (Exception)
            {
                success = false;
                throw;
            }
            var respuesta = new
            {
                success = success,
                count = count
            };
            return Json(respuesta, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ExisteDocumento(string FileName, string carpeta)
        {
            bool success = Fn.FnFiles.existe(carpeta, FileName);
            var respuesta = new
            {
                success = success
            };
            return Json(respuesta, JsonRequestBehavior.AllowGet);
        }
        
        public void descargarDocumento(string FileName, string carpeta)
        {
            Fn.FnFiles.DescargarArchivo(Response, carpeta, FileName);
        }

    }
}
