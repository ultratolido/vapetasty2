﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tienda_Online.Models;

namespace Tienda_Online.Controllers
{
    public class CarritoController : Controller
    {

        POSEntities db = new POSEntities();

        public ActionResult Index()
        {
            ViewBag.isLogin = 0;
            int id_tipo_precio = 1;
            if (Request.IsAuthenticated)
            {
                cliente cliente = Fn.cliente_logeado.TraerCliente();
                id_tipo_precio = cliente.TipoCliente_id;
                sucursal sucursal = cliente.sucursal;
                ViewBag.tipoCambiario = Math.Round(sucursal.precio_dolar.Value, 2);
                if (cliente.id != 0)
                {
                    ViewBag.isLogin = 1;
                }
            }
            else
            {
                ViewBag.tipoCambiario = Math.Round(db.sucursal.First().precio_dolar.Value, 2);
            }
            List<itemproductocarrito2> ListaCarrito = Fn.FnCarrito.TraerLista(System.Web.HttpContext.Current, id_tipo_precio);
            if (ListaCarrito != null)
            {
                Fn.FnCarrito.actualizarNombrePrimeraImagenListaProducto(ListaCarrito);
                Fn.FnCarrito.actualizarDatosListaProducto(ListaCarrito, id_tipo_precio);
            }
            ViewBag.ListaCarrito = ListaCarrito;
            return View();
        }


        public ActionResult agregarCotizacionACarrito(int id_cotizacion)
        {
            List<itemproductocarrito2> ListaProductosCarritos = new List<itemproductocarrito2>();
            venta v = db.venta.Find(id_cotizacion);
            try
            {
                //revisar si es una cotizacion online
                if (v.catalogo_status_id == 7)
                {
                    List<itemproductocarrito2> productosCotizacion = new List<itemproductocarrito2>();
                    foreach (conceptos_por_venta concepto in v.conceptos_por_venta)
                    {
                        //ir por los datos actuales de este concepto
                        producto _p = db.producto.Find(concepto.producto.id);
                        itemproductocarrito2 p = new itemproductocarrito2();
                        p.ProductoID = _p.id;
                        p.Codigo = concepto.producto.Codigo;
                        p.Cantidad = concepto.cantidad;
                        p.Precio_Dolar = _p.precio_dolar.Value;
                        p.Precio_Unitario = _p.precio_por_tipocliente.Where(x => x.TipoCliente_id == v.cliente.TipoCliente_id && x.sucursal_id == 1).Single().Precio.Value;
                        productosCotizacion.Add(p);
                    }
                    ListaProductosCarritos = Fn.FnCarrito.agregarProductos(productosCotizacion, System.Web.HttpContext.Current);
                }
                var respuesta = new
                {
                    success = true,
                    conceptos = ListaProductosCarritos.Count,
                    cantidad = v.conceptos_por_venta.Count(),
                    codigo = ""
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                var respuesta = new
                {
                    success = false
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult AgregarProductoCarrito(itemproductocarrito2 ProductoAgregar)
        {
            Fn.FnIva.establecerValoresIniciales();
            configuracion_general configuracion_general = Fn.Configuracion.TraerConfiguracionGeneral();
            try
            {
                int idTipoCliente = new int();
                if (Request.IsAuthenticated)
                {
                    idTipoCliente = Fn.cliente_logeado.TraerCliente().TipoCliente_id;
                }
                else
                {
                    idTipoCliente = 1;
                }
                producto p = db.producto.Find(ProductoAgregar.ProductoID);
                ProductoAgregar = Fn.FnIva.obtenerItemVenta(ProductoAgregar,idTipoCliente);
                ProductoAgregar.SubFamilia_id = p.SubFamilia_id;
                ProductoAgregar.Proveedor_id = p.Proveedor_id;
                ProductoAgregar.id_marca = p.id_marca;
                ProductoAgregar.ProductoID = p.id;
                ProductoAgregar.Codigo = p.Codigo;
                List<itemproductocarrito2> ListaProductos = new List<itemproductocarrito2>();
                ListaProductos.Add(ProductoAgregar);
                List<itemproductocarrito2> ListaProductosCarritos = Fn.FnCarrito.agregarProductos(ListaProductos, System.Web.HttpContext.Current);
                var respuesta = new
                {
                    success = true,
                    conceptos = ListaProductosCarritos.Count,
                    cantidad = ProductoAgregar.Cantidad,
                    codigo = p.Codigo
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                var respuesta = new
                {
                    success = false
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
        }

        public Boolean LimpiarCarrito()
        {
            try
            {
                HttpContext _Context = System.Web.HttpContext.Current;
                //EXISTE COOKIE?
                if (_Context.Request.Cookies["CarritoCompras"] != null)
                {
                    if (_Context.Request.Cookies["CarritoCompras"].Value != null)
                    {

                        _Context.Response.Cookies["CarritoCompras"].Value = null;
                    }
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public PartialViewResult VistaParcialPreviaCotizacion()
        {
            Fn.FnIva.establecerValoresIniciales();
            int id_tipo_precio = 1;
            if (Request.IsAuthenticated)
            {
                cliente cliente = Fn.cliente_logeado.TraerCliente();
                id_tipo_precio = cliente.TipoCliente_id;
            }
            Decimal subtotal = 0;
            Decimal iva = 0;
            Decimal total = 0;
            Decimal subtotal_individual, iva_individual, total_individual;
            List<itemproductocarrito2> ListaCarrito = Fn.FnCarrito.TraerLista(System.Web.HttpContext.Current, id_tipo_precio);
            if (ListaCarrito != null)
            {
                foreach (itemproductocarrito2 producto in ListaCarrito)
                {
                    if (db.imagen.Where(x => x.id_producto == producto.Id).Any())
                        producto.imagen = db.imagen.Where(x => x.id_producto == producto.Id).FirstOrDefault().nombre;
                    subtotal_individual = Fn.FnIva.obtenerSubtotalConcepto(producto.Precio_Unitario, producto.Cantidad);
                    iva_individual = Fn.FnIva.obtenerIvaConcepto(producto.cantidad_iva_unitario, producto.Cantidad);
                    total_individual = Fn.FnIva.obtenerTotalConcepto((subtotal_individual + iva_individual), producto.Cantidad);
                    subtotal += subtotal_individual;
                    iva += iva_individual;
                    total += total_individual;
                }
            }
            ViewBag.ListaCarritoPrevio = ListaCarrito;
            ViewBag.subtotal = Math.Round(subtotal, 2);
            ViewBag.iva = Math.Round(iva, 2);
            ViewBag.total = Math.Round(total, 2);
            return PartialView();
        }

        public void cambiarCantidadProductoCarrito(int id_producto, Decimal cantidad)
        {
            Fn.FnCarrito.cambiarCantidadProducto(id_producto, cantidad, System.Web.HttpContext.Current);
        }

        public ActionResult comprararCarrito_con_Cookie(List<itemproductocarrito2> listaCarrito)
        {
            int id_tipo_precio = 1;
            if (Request.IsAuthenticated)
            {
                cliente cliente = Fn.cliente_logeado.TraerCliente();
                id_tipo_precio = cliente.TipoCliente_id;
            }
            bool success = true;
            bool ok = true;
            try
            {
                List<itemproductocarrito2> productoscookie = Fn.FnCarrito.TraerLista(System.Web.HttpContext.Current, id_tipo_precio);
                foreach (itemproductocarrito2 _p in productoscookie)
                {
                    int cantidad_cookie = Convert.ToInt16(_p.Cantidad);
                    int cantidad_carrito = Convert.ToInt16(listaCarrito.Where(x => x.Codigo == _p.Codigo).Single().Cantidad);
                    if (cantidad_carrito != cantidad_cookie) { ok = false; }
                }
            }
            catch (Exception)
            { ok = false; success = false; }

            var respuesta = new
            {
                success = success,
                ok = ok
            };
            return Json(respuesta, JsonRequestBehavior.AllowGet);
        }


        public ActionResult eliminarProductoCarrito(int id_producto)
        {
            try
            {
                List<itemproductocarrito2> ListaProductosCarrito = Fn.FnCarrito.eliminarProductosID(id_producto, System.Web.HttpContext.Current);
                var respuesta = new
                {
                    success = true,
                    conceptos = ListaProductosCarrito.Count
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                var respuesta = new
                {
                    success = false
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult cantidadProductosCarrito()
        {
            try
            {
                Decimal cantidad = Fn.FnCarrito.TraerCantidadProductos(System.Web.HttpContext.Current);
                var respuesta = new
                {
                    success = true,
                    cantidad = cantidad
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                var respuesta = new
                {
                    success = false,
                    cantidad = 0
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GuardarCotizacion()
        {
            bool success = false;
            int idventa = 0;
            string serie_folio = "";
            decimal subtotal = 0;
            decimal iva = 0;
            decimal total = 0;
            int id_cliente = 0;
            int id_tipo_precio = 1;
            cliente cliente = null;
            if (Request.IsAuthenticated)
            {
                cliente = Fn.cliente_logeado.TraerCliente();
                id_cliente = cliente.id;
                id_tipo_precio = cliente.TipoCliente_id;
            }
            //guardar venta
            venta v = new venta();
            try
            {
                string serie_cotizacion = Config.Serie_de_cotizacion_online();
                v.Serie = serie_cotizacion;  // "WT"
                v.Cliente_id = id_cliente;
                v.catalogo_status_id = 7;
                v.sucursal_id = 1;
                v.Folio = Convert.ToInt16(db.venta.Where(x => x.Serie == serie_cotizacion && x.sucursal_id == v.sucursal_id).Select(x => x.Folio).Max()) + 1;
                v.usuario_id = 1;
                v.fecha = DateTime.Now;
                v.fecha_cotización = DateTime.Now;
                v.subtotal = 0;
                v.iva = 0;
                v.guiaEnvio = "";
                v.total = 0;
                v.aduedo = 0;
                v.precio_dolar_venta = db.sucursal.Find(v.sucursal_id).precio_dolar;
                db.venta.Add(v);
                db.SaveChanges();
                
                Decimal precioDolar = db.sucursal.Find(v.sucursal_id).precio_dolar.Value;
                //guardar conceptos de venta
                foreach (itemproductocarrito2 producto in Fn.FnCarrito.TraerLista(System.Web.HttpContext.Current, id_tipo_precio).ToList())
                {
                    //Fn.FnIva.establecerValoresIniciales();
                    conceptos_por_venta cpv = new conceptos_por_venta();
                    cpv.cantidad = producto.Cantidad;
                    cpv.Producto_id = producto.Id;
                    cpv.Venta_id = v.id;
                    cpv = Fn.FnIva.obtenerConceptoVenta(cpv);
                    db.conceptos_por_venta.Add(cpv);
                    subtotal += cpv.importe.Value;
                    iva += cpv.iva_importe.Value;
                    total += cpv.importe_mas_iva.Value;
                }
                db.SaveChanges();
                venta vTemp = db.venta.Find(v.id);
                vTemp.subtotal = subtotal;
                vTemp.iva = iva;
                vTemp.total = total;
                db.Entry(vTemp).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                success = true;
                idventa = v.id;
                serie_folio = Fn.Numeros.serie_folio_by_id(idventa);
                Fn.FnCarrito.VaciarCarrito(System.Web.HttpContext.Current);
            }
            catch (Exception e)
            {
                success = false;
                db.SaveChanges();
                if (v.id != 0)
                {
                    List<conceptos_por_venta> conceptos_p_venta = db.conceptos_por_venta.Where(x => x.Venta_id == v.id).ToList();
                    foreach (conceptos_por_venta c in conceptos_p_venta)
                    {
                        //ELIMINAR CONCEPTOS
                        db.conceptos_por_venta.Remove(c);
                    }
                    db.SaveChanges();
                    db.venta.Remove(v);
                    db.SaveChanges();
                }
            }
            var respuesta = new
            {
                success = success,
                idventa = idventa,
                id_cotizacion = serie_folio
            };
            return Json(respuesta, JsonRequestBehavior.AllowGet);
        }
        public PartialViewResult ModalCrearEnviarPDFCotizacionActual()
        {
            return PartialView();
        }

        public PartialViewResult ModalGuardarCotizacion(String id_cotizacion, int id_venta)
        {
            ViewBag.id_cotizacion = id_cotizacion;
            ViewBag.id_venta = id_venta;
            return PartialView();
        }

        public PartialViewResult ModalErrorCotizacion()
        {
            return PartialView();
        }


        public PartialViewResult ModalenviarCotizacionACarrito(int id_cotizacion)
        {
            ViewBag.id_cotizacion = id_cotizacion;
            return PartialView();
        }
    }
}
