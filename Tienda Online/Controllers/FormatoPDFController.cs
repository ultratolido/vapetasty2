﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tienda_Online.Models;
using SelectPdf;
using Newtonsoft.Json;

namespace Tienda_Online.Controllers
{
    public class FormatoPDFController : Controller
    {

        POSEntities db = new POSEntities();

        public void GenerarPDFVenta(int id_venta)
        {
            Uri Request_Url = Request.Url;
            var current_action = RouteData.Values["action"].ToString();
            string URL = "";
            //factura activa
            URL = Request_Url.OriginalString.Replace(current_action, "VistaParcialPDFVenta");
            URL += "&id=" + id_venta;

            SelectPdf.PdfPageSize pageSize = (SelectPdf.PdfPageSize)Enum.Parse(typeof(SelectPdf.PdfPageSize), "A4", true);
            SelectPdf.PdfPageOrientation pdfOrientation = (SelectPdf.PdfPageOrientation)Enum.Parse(typeof(SelectPdf.PdfPageOrientation), "Portrait", true);
            // instantiate a html to pdf converter object
            SelectPdf.HtmlToPdf _converter = new SelectPdf.HtmlToPdf();
            // set converter options
            _converter.Options.PdfPageSize = pageSize;
            _converter.Options.PdfPageOrientation = pdfOrientation;
            _converter.Options.WebPageWidth = 1280;
            _converter.Options.WebPageHeight = 0;
            // create a new pdf document converting an url
            SelectPdf.PdfDocument doc = _converter.ConvertUrl(URL);
            doc.Save(System.Web.HttpContext.Current.Response, false, Fn.Numeros.serie_folio_by_id(id_venta) + ".pdf; size={1}");
        }

        public void decargarPDFCotizacionActual()
        {
            int id_tipo_precio = 1;
            int idSucursal = 1;
            if (Request.IsAuthenticated)
            {
                cliente cliente = Fn.cliente_logeado.TraerCliente();
                id_tipo_precio = cliente.TipoCliente_id;
                idSucursal = cliente.id_sucursal;
            }

            //
            //List<itemproductocarrito2> ListaCarrito = Fn.FnCarrito.TraerLista(System.Web.HttpContext.Current, id_tipo_precio);
            //List<itemCarritoResumen> ListaCarritoParams = new List<itemCarritoResumen>();

            //string ListaCarritostring = JsonConvert.SerializeObject(ListaCarritoParams);
            Uri Request_Url = Request.Url;
            var current_action = RouteData.Values["action"].ToString();

            //URL += "?ListaCarritostring=" + ListaCarritostring;

            //foreach (itemproductocarrito2 lc in ListaCarrito)
            //{
            //    itemCarritoResumen icr = new itemCarritoResumen();
            //    icr.id = lc.Id;
            //    icr.cantidad = lc.Cantidad;
            //    ListaCarritoParams.Add(icr);
            //}

            string URL = "";
            URL = Request_Url.OriginalString.Replace(current_action, "VistaParcialPDFCotizacionActual");
            URL += "?cookieCarrito=" + Fn.FnCarrito.traerCookieCarrito(System.Web.HttpContext.Current)
                + "&id_tipo_precio=" + id_tipo_precio
                + "&idSucursal=" + idSucursal;


            SelectPdf.PdfPageSize pageSize = (SelectPdf.PdfPageSize)Enum.Parse(typeof(SelectPdf.PdfPageSize), "A4", true);
            SelectPdf.PdfPageOrientation pdfOrientation = (SelectPdf.PdfPageOrientation)Enum.Parse(typeof(SelectPdf.PdfPageOrientation), "Portrait", true);
            // instantiate a html to pdf converter object
            SelectPdf.HtmlToPdf _converter = new SelectPdf.HtmlToPdf();
            // set converter options
            _converter.Options.PdfPageSize = pageSize;
            _converter.Options.PdfPageOrientation = pdfOrientation;
            _converter.Options.WebPageWidth = 1280;
            _converter.Options.WebPageHeight = 0;
            // create a new pdf document converting an url
            SelectPdf.PdfDocument doc = _converter.ConvertUrl(URL);
            doc.Save(System.Web.HttpContext.Current.Response, false, DateTime.Now.ToString("ddMMyyyy_HHmm") + ".pdf; size={1}");
        }
        public bool EnviarPDFCotizacionActual(string email)
        {
            try
            {
                configuracion_general configuracion_general = Fn.Configuracion.TraerConfiguracionGeneral();
                sucursal sucursal = Fn.Configuracion.TraerConfiguracionSucursales().First();
                int idSucursal = 1;
                configuracion_sucursal_correo configuracion_sucursal_correo = sucursal.configuracion_sucursal_correo.First();
                int id_tipo_precio = 1;
                if (Request.IsAuthenticated)
                {
                    cliente cliente = Fn.cliente_logeado.TraerCliente();
                    id_tipo_precio = cliente.TipoCliente_id;
                    idSucursal = cliente.id_sucursal;
                }
                Uri Request_Url = Request.Url;
                var current_action = RouteData.Values["action"].ToString();
                string URL = "";
                //factura activa
                URL = Request_Url.OriginalString.Replace(current_action, "VistaParcialPDFCotizacionActual");
                URL += "?cookieCarrito=" + Fn.FnCarrito.traerCookieCarrito(System.Web.HttpContext.Current)
                          + "&id_tipo_precio=" + id_tipo_precio
                            + "&idSucursal=" + idSucursal;


                //List<itemproductocarrito2> ListaCarrito = Fn.FnCarrito.TraerLista(System.Web.HttpContext.Current, id_tipo_precio);
                //List<itemproductocarrito2> ListaCarritoParams = new List<itemproductocarrito2>();
                //foreach (itemproductocarrito2 lc in ListaCarrito)
                //{
                //    itemproductocarrito2 nlc = new itemproductocarrito2();
                //    nlc.Id = lc.Id;
                //    nlc.Cantidad = lc.Cantidad;
                //    ListaCarritoParams.Add(nlc);
                //}
                //string ListaCarritostring = JsonConvert.SerializeObject(ListaCarritoParams);
                //URL += "?ListaCarritostring=" + ListaCarritostring;

                SelectPdf.PdfPageSize pageSize = (SelectPdf.PdfPageSize)Enum.Parse(typeof(SelectPdf.PdfPageSize), "A4", true);
                SelectPdf.PdfPageOrientation pdfOrientation = (SelectPdf.PdfPageOrientation)Enum.Parse(typeof(SelectPdf.PdfPageOrientation), "Portrait", true);
                // instantiate a html to pdf converter object
                SelectPdf.HtmlToPdf _converter = new SelectPdf.HtmlToPdf();
                // set converter options
                _converter.Options.PdfPageSize = pageSize;
                _converter.Options.PdfPageOrientation = pdfOrientation;
                _converter.Options.WebPageWidth = 1280;
                _converter.Options.WebPageHeight = 0;
                // create a new pdf document converting an url
                SelectPdf.PdfDocument doc = _converter.ConvertUrl(URL);
                byte[] pdfBuffer;
                String rutaArchivo = Server.MapPath("~/UploadFolder/PDF_email_temp/Cotizacion-") + DateTime.Now.ToString("ddMMyyyy_HHmm") + ".pdf";
                if (true)//convertir a memoria
                {
                    pdfBuffer = doc.Save();
                    System.IO.File.WriteAllBytes(rutaArchivo, pdfBuffer);
                }
                CDO.Message message = new CDO.Message();
                CDO.IConfiguration configuration = message.Configuration;
                ADODB.Fields fields = configuration.Fields;
                ADODB.Field field = fields["http://schemas.microsoft.com/cdo/configuration/smtpserver"];
                field.Value = configuracion_sucursal_correo.smtp;
                field = fields["http://schemas.microsoft.com/cdo/configuration/smtpserverport"];
                field.Value = configuracion_sucursal_correo.puerto;
                field = fields["http://schemas.microsoft.com/cdo/configuration/sendusing"];
                field.Value = CDO.CdoSendUsing.cdoSendUsingPort;
                field = fields["http://schemas.microsoft.com/cdo/configuration/smtpauthenticate"];
                field.Value = CDO.CdoProtocolsAuthentication.cdoBasic;
                field = fields["http://schemas.microsoft.com/cdo/configuration/sendusername"];
                field.Value = configuracion_sucursal_correo.correo;
                field = fields["http://schemas.microsoft.com/cdo/configuration/sendpassword"];
                field.Value = configuracion_sucursal_correo.contrasena;
                field = fields["http://schemas.microsoft.com/cdo/configuration/smtpusessl"];
                field.Value = configuracion_sucursal_correo.ssl;
                fields.Update();
                message.From = configuracion_sucursal_correo.correo;
                message.To = email;
                message.Subject = "¡Gracias por utilizar el servicio de cotizaciones Moragas.mx!";// +configuracion_general.marca;
                message.HTMLBody = "<br />Su Cotizacion fue realizada con exito.<br />Cotización:<br /><br />Gracias<br /><br /><a href='" + configuracion_general.pagina_web + "'>Visitar " + configuracion_general.pagina_web + "</a><br><br>Saludos!<br><br>" + configuracion_general.marca
                                    + "<br />Para colocar el pedido de esta cotización Comunícate con nosotros al 1800 4275 769 En  pedidos mayores a 2500.00 pesos  y menores a 5kg él envió es Gratis  "
                            + "<br /><br /><strong>Datos de transferencia: </strong>"
                            + "<br />Moragas de Puerto Valarta SA de CV "
                            + "<br />Banco: Bancomer "
                            + "<br />Número de Cuenta: 0110551570 "
                            + "<br />CLABE: 012375001105515701 ";
                message.AddAttachment(rutaArchivo);
                message.Send();
                System.IO.File.Delete(rutaArchivo);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


        public void GenerarEnviarPDFCotizacion(int id_venta, String correo)
        {
            configuracion_general configuracion_general = Fn.Configuracion.TraerConfiguracionGeneral();
            sucursal sucursal = Fn.Configuracion.TraerConfiguracionSucursales().First();
            configuracion_sucursal_correo configuracion_sucursal_correo = sucursal.configuracion_sucursal_correo.First();
            Uri Request_Url = Request.Url;
            var current_action = RouteData.Values["action"].ToString();
            string URL = "";
            //factura activa
            URL = Request_Url.OriginalString.Replace(current_action, "VistaParcialPDFCotizacion");
            URL += "&id=" + id_venta;

            SelectPdf.PdfPageSize pageSize = (SelectPdf.PdfPageSize)Enum.Parse(typeof(SelectPdf.PdfPageSize), "A4", true);
            SelectPdf.PdfPageOrientation pdfOrientation = (SelectPdf.PdfPageOrientation)Enum.Parse(typeof(SelectPdf.PdfPageOrientation), "Portrait", true);
            // instantiate a html to pdf converter object
            SelectPdf.HtmlToPdf _converter = new SelectPdf.HtmlToPdf();
            // set converter options
            _converter.Options.PdfPageSize = pageSize;
            _converter.Options.PdfPageOrientation = pdfOrientation;
            _converter.Options.WebPageWidth = 1280;
            _converter.Options.WebPageHeight = 0;

            SelectPdf.PdfDocument doc = _converter.ConvertUrl(URL);
            byte[] pdfBuffer;
            String rutaArchivo = Server.MapPath("~/UploadFolder/PDF_email_temp/Cotizacion-") + Fn.Numeros.serie_folio_by_id(id_venta) + ".pdf";
            if (true)//convertir a memoria
            {
                pdfBuffer = doc.Save();
                System.IO.File.WriteAllBytes(rutaArchivo, pdfBuffer);
            }

            Response.AddHeader("Content-Type", "application/pdf");
            Response.AddHeader("Content-Disposition", String.Format("{0}; filename=" + Fn.Numeros.serie_folio_by_id(id_venta) + ".pdf; size={1}",
                 "attachment", pdfBuffer.Length.ToString()));
            Response.BinaryWrite(pdfBuffer);
            Response.End();
            //Response.WriteFile(Server.MapPath("~/PDF/Cotizacion-") + Fn.Numeros.serie_folio_by_id(id_venta) + ".pdf");
            String nombreUsurio = "-";
            if (correo.Equals("correo@correo.com"))
            {
                String correoReal = Fn.cliente_logeado.TraerCliente().correo;
                correo = correoReal;

            }
            CDO.Message message = new CDO.Message();
            CDO.IConfiguration configuration = message.Configuration;
            ADODB.Fields fields = configuration.Fields;
            ADODB.Field field = fields["http://schemas.microsoft.com/cdo/configuration/smtpserver"];
            field.Value = configuracion_sucursal_correo.smtp;
            field = fields["http://schemas.microsoft.com/cdo/configuration/smtpserverport"];
            field.Value = configuracion_sucursal_correo.puerto;
            field = fields["http://schemas.microsoft.com/cdo/configuration/sendusing"];
            field.Value = CDO.CdoSendUsing.cdoSendUsingPort;
            field = fields["http://schemas.microsoft.com/cdo/configuration/smtpauthenticate"];
            field.Value = CDO.CdoProtocolsAuthentication.cdoBasic;
            field = fields["http://schemas.microsoft.com/cdo/configuration/sendusername"];
            field.Value = configuracion_sucursal_correo.correo;
            //field.Value = "contacto@moragas.com";
            field = fields["http://schemas.microsoft.com/cdo/configuration/sendpassword"];
            field.Value = configuracion_sucursal_correo.contrasena;
            //field.Value = "nuevaContraseniaMoragas";
            field = fields["http://schemas.microsoft.com/cdo/configuration/smtpusessl"];
            field.Value = configuracion_sucursal_correo.ssl;
            fields.Update();
            message.From = configuracion_sucursal_correo.correo;
            message.To = correo;
            message.Subject = "¡Gracias por utilizar el servicio de cotizaciones Moragas.mx!";// +configuracion_general.marca;
            message.HTMLBody = "<br />" + nombreUsurio + "<br /><br />Su Cotizacion fue realizada con exito.<br />Cotizaciòn: Folio#" + Fn.Numeros.serie_folio_by_id(id_venta) + "<br /><br />Gracias<br /><br /><a href='" + configuracion_general.pagina_web + "'>Visitar " + configuracion_general.pagina_web + "</a><br><br>Saludos!<br><br>" + configuracion_general.marca
                + "<br />Para colocar el pedido de esta cotización Comunícate con nosotros al 1800 4275 769 En  pedidos mayores a 2500.00 pesos  y menores a 5kg él envió es Gratis  "
                + "<br /><br /><strong>Datos de transferencia: </strong>"
                + "<br />Moragas de Puerto Valarta SA de CV "
                + "<br />Banco: Bancomer "
                + "<br />Numero de Cuenta: 0110551570 "
                + "<br />CLABE: 012375001105515701 ";
            message.AddAttachment(rutaArchivo);
            message.Send();
            System.IO.File.Delete(rutaArchivo);

        }

        public void GenerarPDFCotizacion(int id_venta)
        {
            Uri Request_Url = Request.Url;
            var current_action = RouteData.Values["action"].ToString();
            string URL = "";
            //factura activa
            URL = Request_Url.OriginalString.Replace(current_action, "VistaParcialPDFCotizacion");
            URL += "&id=" + id_venta;
            SelectPdf.PdfPageSize pageSize = (SelectPdf.PdfPageSize)Enum.Parse(typeof(SelectPdf.PdfPageSize), "A4", true);
            SelectPdf.PdfPageOrientation pdfOrientation = (SelectPdf.PdfPageOrientation)Enum.Parse(typeof(SelectPdf.PdfPageOrientation), "Portrait", true);
            // instantiate a html to pdf converter object
            SelectPdf.HtmlToPdf _converter = new SelectPdf.HtmlToPdf();
            // set converter options
            _converter.Options.PdfPageSize = pageSize;
            _converter.Options.PdfPageOrientation = pdfOrientation;
            _converter.Options.WebPageWidth = 1280;
            _converter.Options.WebPageHeight = 0;
            // create a new pdf document converting an url
            SelectPdf.PdfDocument doc = _converter.ConvertUrl(URL);
            doc.Save(System.Web.HttpContext.Current.Response, false, Fn.Numeros.serie_folio_by_id(id_venta) + ".pdf; size={1}");
        }

        public PartialViewResult VistaParcialPDFCotizacion(int id)
        {
            venta c = db.venta.Find(id);
            ViewBag.tipoCambiario = Math.Round(c.sucursal.precio_dolar.Value, 2);
            ViewBag.venta = c;
            return PartialView();
        }

        public PartialViewResult VistaParcialPDFCotizacionActual(string cookieCarrito, int id_tipo_precio, int idSucursal)
        {
            sucursal s = new sucursal();
            if (idSucursal != null)
                s = db.sucursal.Find(idSucursal);
            else
                s = db.sucursal.ToList().First();

            ViewBag.tipoCambiario = Math.Round(s.precio_dolar.Value, 2);

            List<itemproductocarrito2> ListaCarrito = Fn.FnCarrito.TraerListaGUID(cookieCarrito, id_tipo_precio);
            if (ListaCarrito != null)
            {
                if (ListaCarrito.Count() != 0)
                {
                    Fn.FnCarrito.actualizarNombrePrimeraImagenListaProducto(ListaCarrito);
                    Fn.FnCarrito.actualizarDatosListaProducto(ListaCarrito, id_tipo_precio);
                }
            }

            ViewBag.isLogin = 0;
            if (Request.IsAuthenticated)
            {
                if (Fn.cliente_logeado.TraerCliente() != null)
                {
                    ViewBag.isLogin = 1;
                }
            }
            ViewBag.ListaCarrito = ListaCarrito;
            return PartialView();
        }


        public PartialViewResult VistaParcialPDFVenta(int id)
        {
            venta venta = db.venta.Find(id);
            ViewBag.venta = venta;
            return PartialView();
        }

        public void html_to_pdf()
        {

            // read parameters from the webpage
            var url = this.Url.Action("VistaParcialPDFVenta", "FormatoPDF", new { id = 8780 }, this.Request.Url.Scheme);
            SelectPdf.PdfPageSize pageSize = (SelectPdf.PdfPageSize)Enum.Parse(typeof(SelectPdf.PdfPageSize),
                "A4", true);

            SelectPdf.PdfPageOrientation pdfOrientation =
                (SelectPdf.PdfPageOrientation)Enum.Parse(typeof(SelectPdf.PdfPageOrientation),
                "Portrait", true);

            // instantiate a html to pdf converter object
            SelectPdf.HtmlToPdf _converter = new SelectPdf.HtmlToPdf();

            // set converter options
            _converter.Options.PdfPageSize = pageSize;
            _converter.Options.PdfPageOrientation = pdfOrientation;
            _converter.Options.WebPageWidth = 1280;
            _converter.Options.WebPageHeight = 0;

            // create a new pdf document converting an url
            SelectPdf.PdfDocument doc = _converter.ConvertUrl(url);


            byte[] pdfBuffer = doc.Save();
            Response.AddHeader("Content-Type", "application/pdf");
            Response.AddHeader("Content-Disposition", String.Format("{0}; filename=" + "__id__" + ".pdf; size={1}",
                 "attachment", pdfBuffer.Length.ToString()));
            Response.BinaryWrite(pdfBuffer);
            Response.End();
        }

    }
}
