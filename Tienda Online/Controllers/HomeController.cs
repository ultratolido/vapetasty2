﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Tienda_Online.Models;

namespace Tienda_Online.Controllers
{
    public class HomeController : Controller
    {

        POSEntities db = new POSEntities();

        public ActionResult Index()
        {
            List<imagen_carousel> ListaSliders = db.imagen_carousel.Where(x => x.activa == 1).ToList();
            List<marca> ListaMarcas = db.marca.Where(x => x.imagen != null && !x.imagen.Equals("")).ToList();
            ViewBag.ListaSliders = ListaSliders;
            ViewBag.ListaMarcas = ListaMarcas;
            return View();
        }

        public ActionResult Contacto()
        {
            return View();
        }
        public ActionResult aviso()
        {
            return View();
        }
        public ActionResult enviarContacto(string Nombre, string Apellidos, string Email, string Telefono, string Mensaje)
        {
            string Fecha = DateTime.Now.ToString("dd/MM/yyyy hh:mm");
            try
            {
                configuracion_general configuracion_general = Fn.Configuracion.TraerConfiguracionGeneral();
                sucursal sucursal = Fn.Configuracion.TraerConfiguracionSucursales().First();
                configuracion_sucursal_correo configuracion_sucursal_correo = sucursal.configuracion_sucursal_correo.First();
                CDO.Message message = new CDO.Message();
                CDO.IConfiguration configuration = message.Configuration;
                ADODB.Fields fields = configuration.Fields;
                ADODB.Field field = fields["http://schemas.microsoft.com/cdo/configuration/smtpserver"];
                field.Value = configuracion_sucursal_correo.smtp;
                field = fields["http://schemas.microsoft.com/cdo/configuration/smtpserverport"];
                field.Value = configuracion_sucursal_correo.puerto;
                field = fields["http://schemas.microsoft.com/cdo/configuration/sendusing"];
                field.Value = CDO.CdoSendUsing.cdoSendUsingPort;
                field = fields["http://schemas.microsoft.com/cdo/configuration/smtpauthenticate"];
                field.Value = CDO.CdoProtocolsAuthentication.cdoBasic;
                field = fields["http://schemas.microsoft.com/cdo/configuration/sendusername"];
                field.Value = configuracion_sucursal_correo.correo;
                field = fields["http://schemas.microsoft.com/cdo/configuration/sendpassword"];
                field.Value = configuracion_sucursal_correo.contrasena;
                field = fields["http://schemas.microsoft.com/cdo/configuration/smtpusessl"];
                field.Value = configuracion_sucursal_correo.ssl;
                fields.Update();
                message.From = configuracion_sucursal_correo.correo;
                message.To = configuracion_sucursal_correo.correo;
                message.Subject = "Contacto - " + configuracion_general.marca;
                message.HTMLBody = "<link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>"
                    + "<div style='font-family: 'Montserrat', sans-serif;'>"
                    + "<br>"
                    + "<br>"
                        + "Formulario de contacto"
                        + "<br>"
                        + "<br>"
                        + "<table>"
                         + "<tr>"
                                + "<td>Fecha</td>"
                                + "<td>" + Fecha + "</td>"
                            + "</tr>"
                            + "<tr>"
                                + "<td>Nombre</td>"
                                + "<td>" + Nombre + "</td>"
                            + "</tr>"
                            + "<tr>"
                                + "<td>Apellidos</td>"
                                + "<td>" + Apellidos + "</td>"
                            + "</tr>"
                            + "<tr>"
                                + "<td>Email</td>"
                                + "<td>" + Email + "</td>"
                            + "</tr>"
                            + "<tr>"
                                + "<td>Teléfono</td>"
                                + "<td>" + Telefono + "</td>"
                            + "</tr>"
                            + "<tr>"
                                + "<td>Mensaje</td>"
                                + "<td>" + Mensaje + "</td>"
                            + "</tr>"
                        + "</table>"
                        + "</br>"
                    + "</div>";
                message.Send();
                var respuesta = new
                {
                    success = true,
                    error = ""
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var respuesta = new
                {
                    success = false,
                    error = e.Message
                };
                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ExisteDocumentoTecnico(int id)
        {
            bool success = false;
            string error = "";
            string pdfFileName = "";
            string path = "";
            try
            {
                pdf p = new pdf();
                if (db.pdf.Where(x => x.id == id).Any())
                {
                    pdfFileName = db.pdf.Find(id).nombre;
                    path = Server.MapPath("~/UploadFolder/descripcion_detalle_producto/" + pdfFileName);
                    if ((System.IO.File.Exists(path)))
                    {
                        success = true;
                    }
                    else
                    {
                        error = "Archivo no existe";
                        success = false;
                    }
                }
                else
                {
                    error = "PDF id incorrecto";
                    success = false;
                }
            }
            catch (Exception e)
            {
                error = e.Message;
                success = false;
                throw;
            }
            var respuesta = new
            {
                success = success,
                error = error,
                pdfFileName = pdfFileName,
                path = path
            };
            return Json(respuesta, JsonRequestBehavior.AllowGet);

        }

        public void DocumentoTecnico(string pdfFileName, string path)
        {
            Response.ContentType = "application/pdf";
            Response.AppendHeader("content-disposition", "attachment;filename=" + pdfFileName);
            Response.TransmitFile(path);
            Response.End();

        }

        public ActionResult DownloadPDF(int idFile)
        {
            string pdfFileName = db.pdf.Find(idFile).nombre;
            try
            {
                string path = Server.MapPath("~/UploadFolder/descripcion_detalle_producto/" + pdfFileName);
                if ((System.IO.File.Exists(path)))
                {
                    var fileStream = new FileStream(path, FileMode.Open);
                    var mimeType = "application/pdf";
                    var fileDownloadName = pdfFileName;
                    return File(fileStream, mimeType, fileDownloadName);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
        public PartialViewResult menuCategorias()
        {
            List<subfamilia> ListaSubfamiliasProductosPublicar = db.producto.Where(x => x.publicar == 1).Select(x => x.subfamilia).Distinct().ToList();
            List<familia> ListaFamilias = ListaSubfamiliasProductosPublicar.Select(x => x.familia).OrderBy(x => x.id).Distinct().ToList();
            List<marca> ListaMarcas = db.marca.ToList();
            ViewBag.ListaFamilias = ListaFamilias;
            ViewBag.ListaSubfamilias = ListaSubfamiliasProductosPublicar;
            ViewBag.ListaMarcas = ListaMarcas;
            return PartialView();
        }

        public PartialViewResult VistaParcialCampoBusqueda()
        {
            var jsonSerialiser = new JavaScriptSerializer();
            var JsonProductos = jsonSerialiser.Serialize(db.producto.Where(x => x.publicar == 1).Select(x => string.Concat(x.Codigo, " | ", x.Descripcion)));
            ViewBag.JsonProductos = JsonProductos;
            return PartialView();
        }

        public PartialViewResult VistaParcialMenu()
        {

            List<subfamilia> ListaSubfamiliasProductosPublicar = db.producto.Where(x => x.publicar == 1).Select(x => x.subfamilia).Distinct().ToList();
            List<familia> ListaFamilias = ListaSubfamiliasProductosPublicar.Select(x => x.familia).OrderBy(x=>x.id).Distinct().ToList();
            List<marca> ListaMarcas = db.marca.ToList();
            ViewBag.ListaFamilias = ListaFamilias;
            ViewBag.ListaSubfamilias = ListaSubfamiliasProductosPublicar;
            ViewBag.ListaMarcas = ListaMarcas;
            return PartialView();
        }

        public ActionResult RealizarBusqueda(String filtro)
        {
            String[] campos = filtro.Split('|');
            if (campos.Length == 1)
            {
                String campo1 = campos[0].TrimEnd();
                String BuscarProductos = "select id"
                                + " from producto pro"
                                + " where "
                                + " (pro.Codigo like '%" + campo1 + "%' or pro.Descripcion like '%" + campo1 + "%' COLLATE utf8_general_ci) AND pro.publicar = 1"
                                + " and pro.id in (select precio_por_tipocliente.Producto_id from precio_por_tipocliente)";
                DataSet ds = Fn.ConnDB.GetDataSet(BuscarProductos, Server.MapPath("").Split('\\').ElementAt(3));
                DataTable dt = ds.Tables[0];
                List<itemproductocarrito2> ListaProductos = (from DataRow row in dt.Rows select new itemproductocarrito2 { Id = Convert.ToInt32(row["Id"]) }).ToList();

                if (ListaProductos.Count == 1)
                {
                    if (ListaProductos.First().Codigo == filtro)
                    {
                        return RedirectToAction("DetalleProducto", "Producto", new { id_producto = ListaProductos.First().Id });
                    }
                    else
                    {
                        return RedirectToAction("busqueda", "Producto", new { filtro = filtro, tipo = 3 });
                    }
                }
                else
                {
                    return RedirectToAction("busqueda", "Producto", new { filtro = filtro, tipo = 3 });
                }
            }
            else
            {
                String campo1 = campos[0].TrimEnd();
                String campo2 = campos[1].TrimStart();
                String BuscarProductos = "select id"
                                + " from producto pro"
                                + " where "
                                + " (pro.Codigo like '%" + campo1 + "%' or pro.Descripcion like '%" + campo2 + "%' COLLATE utf8_general_ci ) AND pro.publicar = 1";
                DataSet ds = Fn.ConnDB.GetDataSet(BuscarProductos, Server.MapPath("").Split('\\').ElementAt(3));
                DataTable dt = ds.Tables[0];
                List<itemproductocarrito2> ListaProductos = (from DataRow row in dt.Rows select new itemproductocarrito2 { Id = Convert.ToInt32(row["Id"]) }).ToList();

                if (ListaProductos.Count == 1)
                {
                    if (ListaProductos.First().Codigo == filtro)
                    {
                        return RedirectToAction("DetalleProducto", "Producto", new { id_producto = ListaProductos.First().Id });
                    }
                    else
                    {
                        return RedirectToAction("busqueda", "Producto", new { filtro = filtro, tipo = 0 });
                    }
                }
                else
                {
                    return RedirectToAction("busqueda", "Producto", new { filtro = filtro, tipo = 0 });
                }
            }
        }

        public ActionResult nuestraEmpresa()
        {
            return View();
        }

        public ActionResult Ayuda()
        {
            return View();
        }
        public ActionResult Compras()
        {
            return View();
        }

        public ActionResult Chat()
        {
            return View();
        }

        public ActionResult VentanaChat(String nombre)
        {
            ViewBag.nombre = nombre;
            return View();
        }

    }
}
