//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Tienda_Online.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class cascos
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public cascos()
        {
            this.cascos_por_ventas = new HashSet<cascos_por_ventas>();
            this.cascosxcompracascos = new HashSet<cascosxcompracascos>();
            this.inventariocascos = new HashSet<inventariocascos>();
        }
    
        public int id { get; set; }
        public string Nombre { get; set; }
        public decimal PrecioCompra { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cascos_por_ventas> cascos_por_ventas { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cascosxcompracascos> cascosxcompracascos { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<inventariocascos> inventariocascos { get; set; }
    }
}
