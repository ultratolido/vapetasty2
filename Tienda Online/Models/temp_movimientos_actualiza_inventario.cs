//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Tienda_Online.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class temp_movimientos_actualiza_inventario
    {
        public int id { get; set; }
        public string Codigo { get; set; }
        public string desc_movimiento { get; set; }
        public string ubicacion { get; set; }
        public Nullable<decimal> cantidad { get; set; }
        public Nullable<int> copiado { get; set; }
        public int sucursal_id { get; set; }
        public Nullable<bool> existeBool { get; set; }
        public Nullable<decimal> ajusteENT { get; set; }
        public Nullable<decimal> ajusteSAL { get; set; }
        public Nullable<decimal> inventarioActual { get; set; }
        public Nullable<decimal> invetarioFinal { get; set; }
    }
}
