﻿using System;

namespace Tienda_Online.Models
{
    public class Config
    {
        //************************
        //************************
        //aqui se configuran los parametros estaticos
        public static Decimal iva()
        {
            return .16m;
        }
        public static string Serie_de_venta_confirmada()
        {
            return "T";
        }
        public static string Serie_de_cotizacion()
        {
            return "TC";
        }
        public static string Serie_de_venta_por_entregar()
        {
            return "TN";
        }
        public static string Serie_de_nota_de_credito()
        {
            return "NC";
        }
        public static string Serie_de_venta_online()
        {
            return "W";
        }
        public static string Serie_de_cotizacion_online()
        {
            return "WT";
        }

    }
}