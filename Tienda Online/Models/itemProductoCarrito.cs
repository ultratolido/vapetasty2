﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Tienda_Online.Models
{
    public class itemProductoCarrito
    {
        public Int32 Id { get; set; }
        public String Codigo { get; set; }
        public String Descripcion { get; set; }
        public String DescripcionDetallada { get; set; }
        public Int32 Precio_Dolar { get; set; }
        public Int32 Proveedor_id { get; set; }
        public Int32 id_marca { get; set; }
        public Int32 SubFamilia_id { get; set; }
        public Decimal Inventario { get; set; }
        public Decimal Cantidad { get; set; }
        public Decimal Precio_Unitario { get; set; }
        public Decimal Precio_Unitario_actualizado { get; set; }
        public Decimal Importe { get; set; }
        public Decimal Importe_actualizado { get; set; }
        public Decimal iva { get; set; }
        public Decimal cantidad_iva_unitario { get; set; }
        public String imagen { get; set; }

    }
}