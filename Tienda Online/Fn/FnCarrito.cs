﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tienda_Online.Models;

namespace Tienda_Online.Fn
{
    public class FnCarrito
    {
        public DateTime hora { get; set; }
        public List<itemproductocarrito2> carritoComprasProductos { get; set; }
        public static List<itemproductocarrito2> ListaCarrito = new List<itemproductocarrito2>();

        public static void actualizarNombrePrimeraImagenListaProducto(List<itemproductocarrito2> ListaCarrito)
        {
            POSEntities db = new POSEntities();
            foreach (itemproductocarrito2 producto in ListaCarrito)
            {
                if (db.imagen.Where(x => x.id_producto == producto.ProductoID).Any())
                    producto.imagen = db.imagen.Where(x => x.id_producto == producto.ProductoID).FirstOrDefault().nombre;
            }
        }
        public static void actualizarDatosListaProducto(List<itemproductocarrito2> ListaCarrito, int id_tipo_precio)
        {
            Fn.FnIva.establecerValoresIniciales();
            POSEntities db = new POSEntities();
            int id_cliente = 1;
            if (Fn.cliente_logeado.TraerCliente().id != 0)
                id_cliente = Fn.cliente_logeado.TraerCliente().id;
            configuracion_tienda_online configuracion_tienda_online = Fn.Configuracion.TraerConfiguracionTiendaOnline();
            foreach (itemproductocarrito2 p in ListaCarrito)
            {
                Decimal cantidad_en_inventario = 0;
                producto _p = db.producto.Find(p.ProductoID);
                p.Precio_Dolar = _p.precio_dolar.Value;
                p.Descripcion = _p.Descripcion;
                p.DescripcionDetallada = _p.descripcion_detallada;
                p.Codigo = _p.Codigo;
                p.ProductoID = _p.id;
                itemproductocarrito2 itemProductoCarrito = Fn.FnIva.obtenerItemVenta(p, id_tipo_precio);
                p.Precio_Dolar = itemProductoCarrito.Precio_Dolar;
                p.iva = itemProductoCarrito.iva;
                p.cantidad_iva_unitario = itemProductoCarrito.cantidad_iva_unitario;
                p.Precio_Unitario = itemProductoCarrito.Precio_Unitario;
                List<int> lista_id_sucursales = db.sucursal.Where(x => x.alimenta_tienda_online == true).Select(y => y.id).ToList();
                producto pro = db.producto.FirstOrDefault(x => x.Codigo.Equals(_p.Codigo));
                if (configuracion_tienda_online.mostrar_inventario == true)
                {
                    List<inventario> lista_inventario = db.inventario.Where(x => x.Producto_id == pro.id && lista_id_sucursales.Contains(x.sucursal_id)).ToList();
                    if (lista_inventario != null && lista_inventario.Count > 0)
                        cantidad_en_inventario = Convert.ToDecimal(lista_inventario.Select(x => x.cantidad).Sum());
                    p.Inventario = cantidad_en_inventario;
                }
            }
        }

        public static List<itemproductocarrito2> TraerLista(HttpContext _Context, int id_tipo_precio)
        {
            Fn.FnIva.establecerValoresIniciales();
            configuracion_tienda_online configuracion_tienda_online = Fn.Configuracion.TraerConfiguracionTiendaOnline();
            POSEntities db = new POSEntities();
            //EXISTE COOKIE?
            itemcookie2 cookie = new itemcookie2();
            List<itemproductocarrito2> listaCarrito = new List<itemproductocarrito2>();
            if (existeCookieCarrito(_Context))
            {
                if (existeCookieCarritoDB(_Context))
                {
                    string cookieCarrito = traerCookieCarrito(_Context);
                    if (db.itemcookie2.Where(x => x.temp_carritoCookie == cookieCarrito).Any())
                    {
                        listaCarrito = db.itemcookie2.Where(x => x.temp_carritoCookie == cookieCarrito).Single().itemproductocarrito2.ToList();
                        int id_cliente = 1;
                        if (Fn.cliente_logeado.TraerCliente().id != 0)
                            id_cliente = Fn.cliente_logeado.TraerCliente().id;
                        foreach (itemproductocarrito2 p in listaCarrito)
                        {
                            Decimal cantidad_en_inventario = 0;
                            producto _p = db.producto.Find(p.ProductoID);
                            p.Descripcion = _p.Descripcion;
                            p.DescripcionDetallada = _p.descripcion_detallada;
                            p.Codigo = _p.Codigo;
                            itemproductocarrito2 itemProductoCarrito = Fn.FnIva.obtenerItemVenta(p, id_tipo_precio);
                            p.Precio_Dolar = itemProductoCarrito.Precio_Dolar;
                            p.iva = itemProductoCarrito.iva;
                            p.cantidad_iva_unitario = itemProductoCarrito.cantidad_iva_unitario;
                            p.Precio_Unitario = itemProductoCarrito.Precio_Unitario;
                            List<int> lista_id_sucursales = db.sucursal.Where(x => x.alimenta_tienda_online == true).Select(y => y.id).ToList();
                            producto pro = db.producto.FirstOrDefault(x => x.Codigo.Equals(_p.Codigo));
                            if (configuracion_tienda_online.mostrar_inventario == true)
                            {
                                List<inventario> lista_inventario = db.inventario.Where(x => x.Producto_id == pro.id && lista_id_sucursales.Contains(x.sucursal_id)).ToList();
                                if (lista_inventario != null && lista_inventario.Count > 0)
                                    cantidad_en_inventario = Convert.ToDecimal(lista_inventario.Select(x => x.cantidad).Sum());
                                p.Inventario = cantidad_en_inventario;
                            }
                        }
                    }
                }
                else
                {
                    generaCookie(_Context);
                }
            }
            else
            {
                generaCookie(_Context);
            }

            return listaCarrito;
        }


        public static List<itemproductocarrito2> TraerListaGUID(string cookieCarrito, int id_tipo_precio)
        {
            Fn.FnIva.establecerValoresIniciales();
            configuracion_tienda_online configuracion_tienda_online = Fn.Configuracion.TraerConfiguracionTiendaOnline();
            POSEntities db = new POSEntities();
            //EXISTE COOKIE?
            itemcookie2 cookie = new itemcookie2();
            List<itemproductocarrito2> listaCarrito = new List<itemproductocarrito2>();

            if (db.itemcookie2.Where(x => x.temp_carritoCookie == cookieCarrito).Any())
            {
                listaCarrito = db.itemcookie2.Where(x => x.temp_carritoCookie == cookieCarrito).Single().itemproductocarrito2.ToList();
                int id_cliente = 1;
                if (Fn.cliente_logeado.TraerCliente().id != 0)
                    id_cliente = Fn.cliente_logeado.TraerCliente().id;
                foreach (itemproductocarrito2 p in listaCarrito)
                {
                    Decimal cantidad_en_inventario = 0;
                    producto _p = db.producto.Find(p.ProductoID);
                    p.Descripcion = _p.Descripcion;
                    p.DescripcionDetallada = _p.descripcion_detallada;
                    p.Codigo = _p.Codigo;
                    itemproductocarrito2 itemProductoCarrito = Fn.FnIva.obtenerItemVenta(p, id_tipo_precio);
                    p.Precio_Dolar = itemProductoCarrito.Precio_Dolar;
                    p.iva = itemProductoCarrito.iva;
                    p.cantidad_iva_unitario = itemProductoCarrito.cantidad_iva_unitario;
                    p.Precio_Unitario = itemProductoCarrito.Precio_Unitario;
                    List<int> lista_id_sucursales = db.sucursal.Where(x => x.alimenta_tienda_online == true).Select(y => y.id).ToList();
                    producto pro = db.producto.FirstOrDefault(x => x.Codigo.Equals(_p.Codigo));
                    if (configuracion_tienda_online.mostrar_inventario == true)
                    {
                        List<inventario> lista_inventario = db.inventario.Where(x => x.Producto_id == pro.id && lista_id_sucursales.Contains(x.sucursal_id)).ToList();
                        if (lista_inventario != null && lista_inventario.Count > 0)
                            cantidad_en_inventario = Convert.ToDecimal(lista_inventario.Select(x => x.cantidad).Sum());
                        p.Inventario = cantidad_en_inventario;
                    }

                }
            }
            return listaCarrito;
        }


        public static Decimal TraerCantidadProductos(HttpContext _Context)
        {
            POSEntities db = new POSEntities();
            //EXISTE COOKIE?
            //HttpContext _Context2 = System.Web.HttpContext.Current;
            List<itemproductocarrito2> listaActual_CarritoCompras = new List<itemproductocarrito2>();
            if (_Context.Request.Cookies["CarritoComprasCookie"] != null)
            {
                if (_Context.Request.Cookies["CarritoComprasCookie"].Value != null)
                {
                    if (_Context.Request.Cookies["CarritoComprasCookie"].Value != "")
                    {
                        string cookieCarrito = _Context.Request.Cookies["CarritoComprasCookie"].Value;

                        return db.itemcookie2.Where(x => x.temp_carritoCookie == cookieCarrito).Single().itemproductocarrito2.Select(x => x.Cantidad).Sum();
                    }
                    else
                        return 0;
                }
                else
                    return 0;
            }
            else
                return 0;
        }


        public static string traerCookieCarrito(HttpContext _Context)
        {
            //EXISTE COOKIE?
            string cookietemp = "";
            if (_Context.Request.Cookies["CarritoComprasCookie"] != null)
            {
                if (_Context.Request.Cookies["CarritoComprasCookie"].Value != null && _Context.Request.Cookies["CarritoComprasCookie"].Value != "")
                {
                    cookietemp = _Context.Request.Cookies["CarritoComprasCookie"].Value;
                }
            }
            return cookietemp;
        }

        public static List<itemproductocarrito2> traerProductoCookie(HttpContext _Context)
        {
            POSEntities db = new POSEntities();
            List<itemproductocarrito2> productosCookie = new List<itemproductocarrito2>();
            //EXISTE COOKIE?
            if (_Context.Request.Cookies["CarritoComprasCookie"] != null)
            {
                if (_Context.Request.Cookies["CarritoComprasCookie"].Value != null && _Context.Request.Cookies["CarritoComprasCookie"].Value != "")
                {
                    string cookieCarrito = _Context.Request.Cookies["CarritoComprasCookie"].Value;
                    if (db.itemcookie2.Where(x => x.temp_carritoCookie == cookieCarrito).Any())
                    {
                        productosCookie = db.itemcookie2.Where(x => x.temp_carritoCookie == cookieCarrito).Single().itemproductocarrito2.ToList();
                    }
                }
            }
            return productosCookie;
        }

        public static int traerDBidCookie(HttpContext _Context)
        {
            POSEntities db = new POSEntities();
            int id = 0;
            if (existeCookieCarrito(_Context))
            {
                if (existeCookieCarritoDB(_Context))
                {
                    string cookieCarrito = traerCookieCarrito(_Context);
                    id = db.itemcookie2.Where(x => x.temp_carritoCookie == cookieCarrito).Single().id;
                }
            }
            return id;
        }

        public static bool existeCookieCarrito(HttpContext _Context)
        {
            //EXISTE COOKIE?
            bool existe = false;
            if (_Context.Request.Cookies["CarritoComprasCookie"] != null)
            {
                if (_Context.Request.Cookies["CarritoComprasCookie"].Value != null && _Context.Request.Cookies["CarritoComprasCookie"].Value != "")
                {
                    // cookietemp = _Context.Request.Cookies["CarritoComprasCookie"].Value;
                    existe = true;
                }
            }
            return existe;
        }

        public static bool existeCookieCarritoDB(HttpContext _Context)
        {
            POSEntities db = new POSEntities();
            bool existe = false;
            if (existeCookieCarrito(_Context))
            {
                string cookieTemp = traerCookieCarrito(_Context);
                if (db.itemcookie2.Where(x => x.temp_carritoCookie == cookieTemp).Any())
                    existe = true;
            }
            return existe;

        }
        public static void generaCookie(HttpContext _Context)
        {
            POSEntities db = new POSEntities();
            string cookieTemp = Guid.NewGuid().ToString();
            HttpCookie myCookie = new HttpCookie("CarritoComprasCookie");
            myCookie.Value = cookieTemp;
            myCookie.Expires = DateTime.Now.AddDays(1d);
            _Context.Response.Cookies.Add(myCookie);
            itemcookie2 tempCookie = new itemcookie2();
            tempCookie.temp_carritoCookie = cookieTemp;
            db.itemcookie2.Add(tempCookie);
            db.SaveChanges();
        }


        public static List<itemproductocarrito2> agregarProductos(List<itemproductocarrito2> listaProductosAgregar, HttpContext _Context)
        {
            POSEntities db = new POSEntities();
            List<itemproductocarrito2> ListaFinalProductos = new List<itemproductocarrito2>();
            //EXISTE COOKIE CLIENTE?
            if (existeCookieCarrito(_Context))
            {
                //EXISTE EN DB?
                if (existeCookieCarritoDB(_Context))
                {
                    List<itemproductocarrito2> ListaProductosCarrito = traerProductoCookie(_Context);

                    //ACTUALIZA CARRITO COMPRA
                    foreach (itemproductocarrito2 p in listaProductosAgregar)
                    {
                        p.DescripcionDetallada = "";
                        p.Descripcion = "";
                        p.imagen = "";
                        //PRODUCTO EXISTE EN LISTA??
                        if (ListaProductosCarrito.Where(x => x.ProductoID == p.ProductoID).Any())
                        {
                            //ListaProductosCarrito.Where(x => x.ProductoID == p.ProductoID).SingleOrDefault().Cantidad += p.Cantidad;
                            int idcookie=traerDBidCookie(_Context);
                            db.itemcookie2.Find(idcookie).itemproductocarrito2.Where(x => x.ProductoID == p.ProductoID).Single().Cantidad += p.Cantidad;
                            //db.itemproductocarrito2.Find(p.Id).Cantidad += p.Cantidad;
                            //db.SaveChanges();
                        }
                        //SI NO EXISTE AGREGA EL REGISTRO
                        else
                        {
                            p.itemCookie2_id = traerDBidCookie(_Context);
                            db.itemproductocarrito2.Add(p);
                            // db.itemproductocarrito2.Add(p);
                            // ListaProductosCarrito.Add(p);
                            //db.SaveChanges();
                        }

                    }
                    db.SaveChanges();
                }
                else
                {
                    generaCookie(_Context);
                }
            }
            else
            {
                generaCookie(_Context);
            }
            string cookieTemp = traerCookieCarrito(_Context);
            ListaFinalProductos.AddRange(db.itemcookie2.Where(x => x.temp_carritoCookie == cookieTemp).Single().itemproductocarrito2);
            return ListaFinalProductos;
        }

        public static void VaciarCarrito(HttpContext _Context)
        {
            ListaCarrito.Clear();
            POSEntities db = new POSEntities();
            dynamic cookieJson = "";
            //EXISTE COOKIE?
            if (_Context.Request.Cookies["CarritoComprasCookie"] != null)
            {
                if (_Context.Request.Cookies["CarritoComprasCookie"].Value != null)
                {
                    _Context.Response.Cookies["CarritoComprasCookie"].Value = "";
                }
            }
        }

        //FUNCION QUE ELIMINA TODOS LOS PRODUCTOS DE LA LISTA CON EL ID ENVIADO
        public static List<itemproductocarrito2> eliminarProductosID(int idProducto, HttpContext _Context)
        {
            POSEntities db = new POSEntities();
            List<itemproductocarrito2> listaActual_CarritoCompras = new List<itemproductocarrito2>();
            List<itemproductocarrito2> listaFinal = new List<itemproductocarrito2>();
            itemcookie2 cookie = new itemcookie2();

            List<itemproductocarrito2> ListaFinalProductos = new List<itemproductocarrito2>();
            //EXISTE COOKIE CLIENTE?
            if (existeCookieCarrito(_Context))
            {
                //EXISTE EN DB?
                if (existeCookieCarritoDB(_Context))
                {
                    listaActual_CarritoCompras = traerProductoCookie(_Context);
                    //ID PRODUCTO EXISTE EN LISTA??
                    if (listaActual_CarritoCompras.Select(x => x.ProductoID).Contains(idProducto))
                    {
                        itemproductocarrito2 productoCarrito = db.itemproductocarrito2.Find(listaActual_CarritoCompras.Where(x => x.ProductoID == idProducto).Single().Id);
                        db.itemproductocarrito2.Remove(productoCarrito);
                        listaActual_CarritoCompras.Remove(listaActual_CarritoCompras.Where(x => x.ProductoID == idProducto).SingleOrDefault());
                        db.SaveChanges();
                    }
                }
                else
                {
                    generaCookie(_Context);
                }
            }
            else
            {
                generaCookie(_Context);
            }



            string cookieTemp2 = traerCookieCarrito(_Context);
            ListaFinalProductos.AddRange(db.itemcookie2.Where(x => x.temp_carritoCookie == cookieTemp2).Single().itemproductocarrito2);
            return ListaFinalProductos;
        }

        //FUNCION QUE BAJA EN UNA UNIDAD EL PRODUCTO ENVIADO. SI ES QUE LO ENCUENTRA, EN CASO CONTRARIO NO HACE NADA
        public static List<itemproductocarrito2> cambiarCantidadProducto(int idProducto, Decimal cantidad, HttpContext _Context)
        {
            POSEntities db = new POSEntities();
            List<itemproductocarrito2> listaActual_CarritoCompras = new List<itemproductocarrito2>();
            string cookieTemp = "";
            itemcookie2 cookie = new itemcookie2();

            //EXISTE COOKIE?
            if (_Context.Request.Cookies["CarritoComprasCookie"] != null)
            {
                if (_Context.Request.Cookies["CarritoComprasCookie"].Value != null)
                {
                    cookieTemp = _Context.Request.Cookies["CarritoComprasCookie"].Value;
                    listaActual_CarritoCompras = db.itemcookie2.Where(x => x.temp_carritoCookie == cookieTemp).Single().itemproductocarrito2.ToList();
                }

                //ID PRODUCTO EXISTE EN LISTA??
                if (listaActual_CarritoCompras.Select(x => x.ProductoID).Contains(idProducto))
                {
                    listaActual_CarritoCompras.Where(x => x.ProductoID == idProducto).SingleOrDefault().Cantidad = cantidad;
                    db.SaveChanges();

                }
                //foreach (itemproductocarrito2 p in listaActual_CarritoCompras)
                //{
                //    p.DescripcionDetallada = "";
                //    p.Descripcion = "";
                //}
                //cookie.Carrito = listaActual_CarritoCompras;
                //ListaCarrito = listaActual_CarritoCompras;
                ////GUARDAR ESTA LISTA ACTUALIZADA AL COOKIE "CarritoComprasCookie"
                //_Context.Response.Cookies["CarritoComprasCookie"].Value = JsonConvert.SerializeObject(cookie);
            }
            return listaActual_CarritoCompras;
        }

        //FUNCION QUE BAJA EN UNA UNIDAD EL PRODUCTO ENVIADO. SI ES QUE LO ENCUENTRA, EN CASO CONTRARIO NO HACE NADA
        public static List<itemproductocarrito2> subirCantidadProducto(int idProducto, HttpContext _Context)
        {
            POSEntities db = new POSEntities();
            List<itemproductocarrito2> listaActual_CarritoCompras = new List<itemproductocarrito2>();
            string cookieTemp = "";
            itemcookie2 cookie = new itemcookie2();
            //EXISTE COOKIE?
            if (_Context.Request.Cookies["CarritoComprasCookie"] != null)
            {
                if (_Context.Request.Cookies["CarritoComprasCookie"].Value != null)
                {
                    cookieTemp = _Context.Request.Cookies["CarritoComprasCookie"].Value;
                }

                //ID PRODUCTO EXISTE EN LISTA??
                if (listaActual_CarritoCompras.Select(x => x.ProductoID).Contains(idProducto))
                {
                    //CANTIDAD ACTUAL?
                    Decimal cantidadActual = listaActual_CarritoCompras.Where(x => x.ProductoID == idProducto).SingleOrDefault().Cantidad;
                    //SI CANTIDAD ACTUAL-1 MENOR/IGUAL A 0 ENTONCES ELIMINA EL REGISTRO
                    if ((cantidadActual - 1) <= 0)
                    {
                        //ELIMINA REGISTRO DE PRODUCTO
                        listaActual_CarritoCompras.Remove(listaActual_CarritoCompras.Where(x => x.ProductoID == idProducto).SingleOrDefault());
                    }
                    else
                    {
                        //BAJA UNA UNIDAD
                        listaActual_CarritoCompras.Where(x => x.ProductoID == idProducto).SingleOrDefault().Cantidad = cantidadActual + 1;
                    }

                }
            }
            //CREA LA COOKIE
            else
            {
                //AGREGALA
                cookieTemp = Guid.NewGuid().ToString();
                HttpCookie myCookie = new HttpCookie("CarritoComprasCookie");
                myCookie.Value = cookieTemp;
                myCookie.Expires = DateTime.Now.AddDays(1d);
                _Context.Response.Cookies.Add(myCookie);
                itemcookie2 tempCookie = new itemcookie2();
                tempCookie.temp_carritoCookie = cookieTemp;
                db.itemcookie2.Add(tempCookie);
                db.SaveChanges();
            }
            return db.itemcookie2.Where(x => x.temp_carritoCookie == cookieTemp).Single().itemproductocarrito2.ToList();
        }

        public static decimal traerSubtotalConcepto(Decimal precio_unitario, Decimal cantidad)
        {
            return Math.Round(precio_unitario * cantidad, 2);
        }

        public static decimal traerIvaConcepto(Decimal iva_unitario, Decimal cantidad)
        {
            return Math.Round(iva_unitario * cantidad, 2);
        }

        public static decimal traerTotalConcepto(Decimal precio_unitario, Decimal iva_unitario, Decimal cantidad)
        {
            return Math.Round((precio_unitario + iva_unitario) * cantidad, 2);
        }
    }
}