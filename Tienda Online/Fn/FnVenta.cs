﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tienda_Online.Models;

namespace Tienda_Online.Fn
{
    public class FnVenta
    {
        public static decimal traerDiffMXN(int id)
        {
            POSEntities db = new POSEntities();
            configuracion_general configuracion_general = Configuracion.TraerConfiguracionGeneral();
            venta v = db.venta.Find(id);
            int tipoCliente_id = v.cliente.TipoCliente_id;
            decimal totalRecalculado = 0;
            sucursal s = v.sucursal == null ? db.sucursal.First() : v.sucursal;
            Decimal precioDolar = Math.Round(s.precio_dolar.Value, 2);
            foreach (conceptos_por_venta concepto in v.conceptos_por_venta)
            {
                Decimal precioConcepto = 0;
                if (configuracion_general.precios_netos == true)
                {
                    precioConcepto = db.precio_por_tipocliente.Where(x => x.Producto_id == concepto.Producto_id && x.TipoCliente_id == tipoCliente_id && x.sucursal_id == v.sucursal_id).Single().Precio.Value;
                }
                else
                {
                    precioConcepto = db.precio_por_tipocliente.Where(x => x.Producto_id == concepto.Producto_id && x.TipoCliente_id == tipoCliente_id && x.sucursal_id == v.sucursal_id).Single().Precio.Value * Config.iva(concepto.Producto_id);
                }
                if (concepto.producto.precio_dolar == 1)
                    precioConcepto = precioConcepto * precioDolar * concepto.cantidad;
                else
                    precioConcepto = precioConcepto * concepto.cantidad;
                totalRecalculado += precioConcepto;
            }
            return (Math.Round(totalRecalculado - v.total.Value, 2));
        }

        public static decimal calcularNuevoSubtotalSumPesos(int id)
        {
            POSEntities db = new POSEntities();
            configuracion_general configuracion_general = Configuracion.TraerConfiguracionGeneral();
            venta v = db.venta.Find(id);
            int tipoCliente_id = v.cliente.TipoCliente_id;
            decimal totalRecalculado = 0;
            sucursal s = v.sucursal == null ? db.sucursal.First() : v.sucursal;
            Decimal precioDolar = Math.Round(s.precio_dolar.Value, 2);
            foreach (conceptos_por_venta concepto in v.conceptos_por_venta)
            {
                Decimal precioActual = 0;
                if (configuracion_general.precios_netos == true)
                {
                    precioActual = db.precio_por_tipocliente.Where(x => x.Producto_id == concepto.Producto_id && x.TipoCliente_id == tipoCliente_id && x.sucursal_id == s.id).Single().Precio.Value / Config.iva(concepto.Producto_id);
                }
                else
                {
                    precioActual = db.precio_por_tipocliente.Where(x => x.Producto_id == concepto.Producto_id && x.TipoCliente_id == tipoCliente_id && x.sucursal_id == s.id).Single().Precio.Value;
                }
                if (concepto.producto.precio_dolar == 1)
                    precioActual = precioActual * precioDolar * concepto.cantidad;
                else
                    precioActual = precioActual * concepto.cantidad;
                totalRecalculado += precioActual;
            }
            return Math.Round(totalRecalculado, 2);
        }

        public static decimal calcularNuevoIvaSumPesos(int id)
        {
            POSEntities db = new POSEntities();
            configuracion_general configuracion_general = Configuracion.TraerConfiguracionGeneral();
            venta v = db.venta.Find(id);
            int tipoCliente_id = v.cliente.TipoCliente_id;
            decimal totalRecalculado = 0;
            sucursal s = v.sucursal == null ? db.sucursal.First() : v.sucursal;
            Decimal precioDolar = Math.Round(s.precio_dolar.Value, 2);
            foreach (conceptos_por_venta concepto in v.conceptos_por_venta)
            {
                Decimal precioActual = 0;
                if (configuracion_general.precios_netos == true)
                {
                    precioActual = (db.precio_por_tipocliente.Where(x => x.Producto_id == concepto.Producto_id && x.TipoCliente_id == tipoCliente_id && x.sucursal_id == s.id).Single().Precio.Value / Config.iva(concepto.Producto_id)) * (Config.iva(concepto.Producto_id) - 1);
                }
                else
                {
                    precioActual = db.precio_por_tipocliente.Where(x => x.Producto_id == concepto.Producto_id && x.TipoCliente_id == tipoCliente_id && x.sucursal_id == s.id).Single().Precio.Value * (Config.iva(concepto.Producto_id) - 1);
                }
                if (concepto.producto.precio_dolar == 1)
                    precioActual = precioActual * precioDolar * concepto.cantidad;
                else
                    precioActual = precioActual * concepto.cantidad;
                totalRecalculado += precioActual;
            }
            return Math.Round(totalRecalculado, 2);
        }

        public static decimal calcularNuevoTotalSumPesos(int id)
        {
            POSEntities db = new POSEntities();
            configuracion_general configuracion_general = Configuracion.TraerConfiguracionGeneral();
            venta v = db.venta.Find(id);
            int tipoCliente_id = v.cliente.TipoCliente_id;
            decimal totalRecalculado = 0;
            sucursal s = v.sucursal == null ? db.sucursal.First() : v.sucursal;
            Decimal precioDolar = Math.Round(s.precio_dolar.Value, 2);
            foreach (conceptos_por_venta concepto in v.conceptos_por_venta)
            {
                Decimal precioActual = 0;
                if (configuracion_general.precios_netos == true)
                {
                    precioActual = db.precio_por_tipocliente.Where(x => x.Producto_id == concepto.Producto_id && x.TipoCliente_id == tipoCliente_id && x.sucursal_id == v.sucursal_id).Single().Precio.Value;
                }
                else
                {
                    precioActual = db.precio_por_tipocliente.Where(x => x.Producto_id == concepto.Producto_id && x.TipoCliente_id == tipoCliente_id && x.sucursal_id == v.sucursal_id).Single().Precio.Value *
                        Config.iva(concepto.Producto_id);
                }
                if (concepto.producto.precio_dolar == 1)
                    precioActual = precioActual * precioDolar * concepto.cantidad;
                else
                    precioActual = precioActual * concepto.cantidad;
                totalRecalculado += precioActual;
            }
            return Math.Round(totalRecalculado, 2);
        }

        public static decimal calcularNuevoImporteConcepto(int idProducto, int id_cliente, int id_sucursal)
        {
            POSEntities db = new POSEntities();
            configuracion_general configuracion_general = Configuracion.TraerConfiguracionGeneral();
            int tipoCliente_id = 1;
            cliente cliente = db.cliente.Find(id_cliente);
            tipoCliente_id = cliente.TipoCliente_id;
            producto producto = db.producto.Find(idProducto);
            Decimal precioActual = 0;
            if (configuracion_general.precios_netos == true)
            {
                precioActual = db.precio_por_tipocliente.Where(x => x.Producto_id == idProducto && x.TipoCliente_id == tipoCliente_id && x.sucursal_id == id_sucursal).Single().Precio.Value / Config.iva(idProducto);
            }
            else
            {
                precioActual = db.precio_por_tipocliente.Where(x => x.Producto_id == idProducto && x.TipoCliente_id == tipoCliente_id && x.sucursal_id == id_sucursal).Single().Precio.Value;
            }
            return (Math.Round(precioActual, 2));
        }

        public static decimal calcularNuevoImporteConceptoSinRedondeo(int idProducto, int id_cliente, int id_sucursal)
        {
            POSEntities db = new POSEntities();
            configuracion_general configuracion_general = Configuracion.TraerConfiguracionGeneral();
            int tipoCliente_id = 1;
            cliente cliente = db.cliente.Find(id_cliente);
            tipoCliente_id = cliente.TipoCliente_id;
            producto producto = db.producto.Find(idProducto);
            Decimal precioActual = 0;
            if (configuracion_general.precios_netos == true)
            {
                precioActual = db.precio_por_tipocliente.Where(x => x.Producto_id == idProducto && x.TipoCliente_id == tipoCliente_id && x.sucursal_id == id_sucursal).Single().Precio.Value / Config.iva(idProducto);
            }
            else
            {
                precioActual = db.precio_por_tipocliente.Where(x => x.Producto_id == idProducto && x.TipoCliente_id == tipoCliente_id && x.sucursal_id == id_sucursal).Single().Precio.Value;
            }
            return (precioActual);
        }

        public static decimal calcularNuevoSubtotalConceptoEnPesos(int id_concepto, int id_cliente, int id_sucursal)
        {
            POSEntities db = new POSEntities();
            configuracion_general configuracion_general = Configuracion.TraerConfiguracionGeneral();
            int tipoCliente_id = 1;
            cliente cliente = db.cliente.Find(id_cliente);
            tipoCliente_id = cliente.TipoCliente_id;
            sucursal s = db.sucursal.Find(id_sucursal);
            conceptos_por_venta concepto = db.conceptos_por_venta.Find(id_concepto);
            producto producto = db.producto.Find(concepto.Producto_id);
            Decimal precioDolar = Math.Round(s.precio_dolar.Value, 2);
            Decimal precioActual = 0;
            if (configuracion_general.precios_netos == true)
            {
                precioActual = db.precio_por_tipocliente.Where(x => x.Producto_id == concepto.Producto_id && x.TipoCliente_id == tipoCliente_id && x.sucursal_id == id_sucursal).Single().Precio.Value / Config.iva(concepto.Producto_id);
            }
            else
            {
                precioActual = db.precio_por_tipocliente.Where(x => x.Producto_id == concepto.Producto_id && x.TipoCliente_id == tipoCliente_id && x.sucursal_id == id_sucursal).Single().Precio.Value;
            }
            if (concepto.producto.precio_dolar == 1)
                precioActual = precioActual * precioDolar * concepto.cantidad;
            else
                precioActual = precioActual * concepto.cantidad;
            return (Math.Round(precioActual, 2));
        }

        public static decimal calcularNuevoIvaConceptoEnPesos(int id_concepto, int id_cliente, int id_sucursal)
        {
            POSEntities db = new POSEntities();
            configuracion_general configuracion_general = Configuracion.TraerConfiguracionGeneral();
            int tipoCliente_id = 1;
            cliente cliente = db.cliente.Find(id_cliente);
            tipoCliente_id = cliente.TipoCliente_id;
            sucursal s = db.sucursal.Find(id_sucursal);
            conceptos_por_venta concepto = db.conceptos_por_venta.Find(id_concepto);
            producto producto = db.producto.Find(concepto.Producto_id);
            Decimal precioDolar = Math.Round(s.precio_dolar.Value, 2);
            Decimal precioActual = 0;
            if (configuracion_general.precios_netos == true)
            {
                precioActual = (db.precio_por_tipocliente.Where(x => x.Producto_id == concepto.Producto_id && x.TipoCliente_id == tipoCliente_id && x.sucursal_id == id_sucursal).Single().Precio.Value / Config.iva(concepto.Producto_id)) * (Config.iva(concepto.Producto_id) - 1);
            }
            else
            {
                precioActual = db.precio_por_tipocliente.Where(x => x.Producto_id == concepto.Producto_id && x.TipoCliente_id == tipoCliente_id && x.sucursal_id == id_sucursal).Single().Precio.Value * (Config.iva(concepto.Producto_id) - 1);
            }
            if (concepto.producto.precio_dolar == 1)
                precioActual = precioActual * precioDolar * concepto.cantidad;
            else
                precioActual = precioActual * concepto.cantidad;
            return (Math.Round(precioActual, 2));
        }

        public static decimal calcularNuevoTotalConceptoEnPesos(int id_concepto, int id_cliente, int id_sucursal)
        {
            POSEntities db = new POSEntities();
            configuracion_general configuracion_general = Configuracion.TraerConfiguracionGeneral();
            int tipoCliente_id = 1;
            cliente cliente = db.cliente.Find(id_cliente);
            tipoCliente_id = cliente.TipoCliente_id;
            sucursal s = db.sucursal.Find(id_sucursal);
            conceptos_por_venta concepto = db.conceptos_por_venta.Find(id_concepto);
            producto producto = db.producto.Find(concepto.Producto_id);
            Decimal precioDolar = Math.Round(s.precio_dolar.Value, 2);
            Decimal precioActual = 0;
            if (configuracion_general.precios_netos == true)
            {
                precioActual = db.precio_por_tipocliente.Where(x => x.Producto_id == concepto.Producto_id && x.TipoCliente_id == tipoCliente_id && x.sucursal_id == id_sucursal).Single().Precio.Value;
            }
            else
            {
                precioActual = db.precio_por_tipocliente.Where(x => x.Producto_id == concepto.Producto_id && x.TipoCliente_id == tipoCliente_id && x.sucursal_id == id_sucursal).Single().Precio.Value * Config.iva(concepto.Producto_id);
            }
            if (concepto.producto.precio_dolar == 1)
                precioActual = precioActual * precioDolar * concepto.cantidad;
            else
                precioActual = precioActual * concepto.cantidad;
            return (Math.Round(precioActual, 2));
        }

        public static decimal calcularIvaUnitarioProducto(int id_tipo_cliente, int id_sucursal, int id_producto)
        {
            POSEntities db = new POSEntities();
            configuracion_general configuracion_general = Configuracion.TraerConfiguracionGeneral();
            Decimal precioActual;
            Decimal iva = 0;
            Decimal total_porcentaje = 100;
            producto producto = db.producto.Find(id_producto);
            precioActual = db.precio_por_tipocliente.Where(x => x.Producto_id == id_producto && x.TipoCliente_id == id_tipo_cliente && x.sucursal_id == id_sucursal).Single().Precio.Value;
            if (producto.iva == null)
            {
                total_porcentaje += Configuracion.TraerConfiguracionGeneral().iva.Value;
            }
            else
            {
                total_porcentaje += producto.iva.Value;
            }
            if (configuracion_general.precios_netos == true)
            {
                if (total_porcentaje > 100)
                {
                    iva = (precioActual / total_porcentaje) * (total_porcentaje - 100);
                }
            }
            else
            {
                iva = precioActual * Config.iva(id_producto);
            }
            return (Math.Round(iva, 2));
        }

        /*  *** *** ***  *** *** ***  *** *** ***
         *  PRODUCTO
         */
        public static decimal obtenerPrecioUnitarioProducto(int id_producto, int id_tipo_cliente, int id_sucursal)
        {
            POSEntities db = new POSEntities();
            if (id_tipo_cliente == 0)
                id_tipo_cliente = obtenerIdTipoCliente();
            Decimal precio_dolar = db.sucursal.Find(id_sucursal).precio_dolar.Value;
            producto producto = db.producto.Find(id_producto);
            precio_por_tipocliente precio_por_tipo_cliente = db.precio_por_tipocliente.Where(x => x.Producto_id == id_producto && x.sucursal_id == 1 && x.TipoCliente_id == id_tipo_cliente).SingleOrDefault();
            decimal precio_unitario;
            if (obtenerVenderPrecioNeto())
            {
                decimal uno_por_ciento_cantidad;
                decimal total_porcentaje;

                total_porcentaje = 100 + Convert.ToDecimal(producto.iva);
                uno_por_ciento_cantidad = Convert.ToDecimal(precio_por_tipo_cliente.Precio) / total_porcentaje;
                precio_unitario = uno_por_ciento_cantidad * 100;
            }
            else
            {
                precio_unitario = Convert.ToDecimal(precio_por_tipo_cliente.Precio);
            }
            if (producto.precio_dolar != 0)
            {
                precio_unitario = precio_unitario * precio_dolar;
            }
            return precio_unitario;
        }

        public static decimal obtenerIvaUnitarioProducto(int id_producto, int id_tipo_cliente, int id_sucursal)
        {
            POSEntities db = new POSEntities();
            if (id_tipo_cliente == 0)
                id_tipo_cliente = obtenerIdTipoCliente();
            Decimal precio_dolar = db.sucursal.Find(id_sucursal).precio_dolar.Value;
            producto producto = db.producto.Find(id_producto);
            precio_por_tipocliente precio_por_tipo_cliente = db.precio_por_tipocliente.Where(x => x.Producto_id == id_producto && x.sucursal_id == 1 && x.TipoCliente_id == id_tipo_cliente).SingleOrDefault();
            decimal porcentaje_iva_producto = Convert.ToDecimal(producto.iva);
            decimal total_porcentaje = 100 + porcentaje_iva_producto;
            decimal iva_unitario = 0;
            if (obtenerVenderPrecioNeto())
            {
                decimal uno_por_ciento_cantidad;

                uno_por_ciento_cantidad = Convert.ToDecimal(precio_por_tipo_cliente.Precio) / total_porcentaje;
                if (porcentaje_iva_producto > 0)
                {
                    iva_unitario = uno_por_ciento_cantidad * (porcentaje_iva_producto);
                }
            }
            else
            {
                if (porcentaje_iva_producto > 0)
                {
                    decimal uno_porciento_cantidad_sin_iva;
                    uno_porciento_cantidad_sin_iva = Convert.ToDecimal(precio_por_tipo_cliente.Precio) / 100;
                    iva_unitario = uno_porciento_cantidad_sin_iva * porcentaje_iva_producto;
                }
            }
            if (producto.precio_dolar != 0)
            {
                iva_unitario = iva_unitario * precio_dolar;
            }
            return iva_unitario;
        }

        public static decimal obtenerTotalProducto(int id_producto, int id_tipo_cliente, int id_sucursal)
        {
            POSEntities db = new POSEntities();
            if (id_tipo_cliente == 0)
                id_tipo_cliente = obtenerIdTipoCliente();
            Decimal precio_dolar = db.sucursal.Find(id_sucursal).precio_dolar.Value;
            producto producto = db.producto.Find(id_producto);
            precio_por_tipocliente precio_por_tipo_cliente = db.precio_por_tipocliente.Where(x => x.Producto_id == id_producto && x.sucursal_id == 1 && x.TipoCliente_id == id_tipo_cliente).SingleOrDefault();
            decimal porcentaje_iva_producto = Convert.ToDecimal(producto.iva);
            decimal total_porcentaje = 100 + porcentaje_iva_producto;
            decimal precio_total_producto;
            if (obtenerVenderPrecioNeto())
            {
                precio_total_producto = Convert.ToDecimal(precio_por_tipo_cliente.Precio);
            }
            else
            {
                decimal uno_porciento_cantidad_sin_iva;
                uno_porciento_cantidad_sin_iva = Convert.ToDecimal(precio_por_tipo_cliente.Precio) / 100;
                precio_total_producto = uno_porciento_cantidad_sin_iva * total_porcentaje;
            }
            if (producto.precio_dolar != 0)
            {
                precio_total_producto = precio_total_producto * precio_dolar;
            }
            return precio_total_producto;
        }

        public static int obtenerIdTipoCliente()
        {
            cliente cliente = Fn.cliente_logeado.TraerCliente();
            return cliente.TipoCliente_id;
        }

        public static bool obtenerVenderPrecioNeto()
        {
            configuracion_general configuracion_general = Fn.Configuracion.TraerConfiguracionGeneral();
            return Convert.ToBoolean(configuracion_general.precios_netos);
        }
    }
}