﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tienda_Online.Models;

namespace Tienda_Online.Fn
{
    public class cliente_logeado
    {
        public static cliente TraerCliente()
        {
            cliente cliente = new cliente();
            POSEntities db = new POSEntities();
            string correo = HttpContext.Current.User.Identity.Name;
            if (correo != "")
            {
                cliente = db.cliente.Where(x => x.correo.Equals(correo)).SingleOrDefault();
            }
            return cliente;
        }
    }
}