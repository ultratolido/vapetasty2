﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tienda_Online.Models;

namespace Tienda_Online.Fn
{
    public class Configuracion
    {

        public static configuracion_general TraerConfiguracionGeneral()
        {
            POSEntities db = new POSEntities();
            configuracion_general configuracion_general = db.configuracion_general.ToList().First();
            return configuracion_general;
        }

        public static configuracion_pos TraerConfiguracionPOS()
        {
            POSEntities db = new POSEntities();
            configuracion_pos configuracion_pos = db.configuracion_pos.ToList().First();
            return configuracion_pos;
        }

        public static configuracion_tienda_online TraerConfiguracionTiendaOnline()
        {
            POSEntities db = new POSEntities();
            configuracion_tienda_online configuracion_tienda_online = db.configuracion_tienda_online.ToList().First();
            return configuracion_tienda_online;
        }

        public static List<configuracion_menu> TraerConfiguracionMenu()
        {
            POSEntities db = new POSEntities();
            List<configuracion_menu> ListaMenus = db.configuracion_menu.ToList();
            return ListaMenus;
        }

        public static List<sucursal> TraerConfiguracionSucursales()
        {
            POSEntities db = new POSEntities();
            List<sucursal> ListaSucursales = db.sucursal.ToList();
            return ListaSucursales;
        }

        public static List<imagen_carousel> TraerImagenesSlider()
        {
            POSEntities db = new POSEntities();
            List<imagen_carousel> ListaImagenesSlider = db.imagen_carousel.ToList();
            return ListaImagenesSlider;
        }

    }
}