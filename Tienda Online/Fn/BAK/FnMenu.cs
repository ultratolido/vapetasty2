﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tienda_Online.Models;

namespace Tienda_Online.Fn
{
    public class FnMenu
    {
        public static List<familia> TraerListaFamilias()
        {
            POSEntities db = new POSEntities();
            List<subfamilia> ListaSubfamiliasProductosPublicar = db.producto.Where(x => x.publicar == 1).Select(x => x.subfamilia).Distinct().ToList();
            List<familia> ListaFamilias = ListaSubfamiliasProductosPublicar.Select(x => x.familia).OrderBy(x => x.id).Distinct().ToList();
            List<familia> ListaFamiliasFiltrada = ListaFamilias;
            foreach (var f in ListaFamilias)
            {
                if (!ListaSubfamiliasProductosPublicar.Where(x => x.Familia_id == f.id).Any())
                {
                    ListaFamiliasFiltrada.Remove(f);
                }
            }
            return ListaFamiliasFiltrada;
        }



    }
}