﻿using System;
using Tienda_Online.Models;

namespace Tienda_Online.Fn
{
    public class Config
    {
        //************************
        //************************
        //aqui se configuran los parametros estaticos
        public static Decimal iva(int id_producto)
        {
            POSEntities db = new POSEntities();
            Decimal iva = 1;
            if (db.producto.Find(id_producto).iva == null)
            {
                iva += Math.Round(Configuracion.TraerConfiguracionGeneral().iva.Value / 100, 2);
            }
            else
            {
                iva += Math.Round(db.producto.Find(id_producto).iva.Value / 100, 2);
            }
            return iva;
        }
        public static string Serie_de_venta_confirmada()
        {
            return "T";
        }
        public static string Serie_de_cotizacion()
        {
            return "TC";
        }
        public static string Serie_de_venta_por_entregar()
        {
            return "TN";
        }
        public static string Serie_de_nota_de_credito()
        {
            return "NC";
        }
        public static string Serie_de_venta_online()
        {
            return "W";
        }
        public static string Serie_de_cotizacion_online()
        {
            return "WT";
        }
        //************************
        //************************
        //************************
        //aqui se configuran los permisos de usuario
        public static bool acceso()
        {
            bool acceso = false;
            //int id_nivel_usuaruio = Fn.usuario_logeado.traerUsuario().usuario_rol_id;
            return acceso;
        }
        //************************
        //************************

    }
}