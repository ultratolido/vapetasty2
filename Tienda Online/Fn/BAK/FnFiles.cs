﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Tienda_Online.Models;

namespace Tienda_Online.Fn
{
    public class FnFiles
    {
        public static bool existe_orden_de_compra(int id)
        {
            POSEntities db = new POSEntities();
            string fileName = db.venta.Find(id).Order_de_compra_filename;
            return existe("UploadFolder/adjuntos_cliente_compra", fileName);
        }

        public static string TraerIcono(string fileName)
        {
            POSEntities db = new POSEntities();
            string ext = System.IO.Path.GetExtension(fileName).ToLower();
            string icono = "";
            switch (ext)
            {
                case ".pdf":
                    {
                        icono = "<i class='fa fa-file-pdf-o fa-lg' aria-hidden='true' style='margin:5px;color:red'></i>";
                        break;
                    }
                case ".xls":
                    {
                        icono = "<i class='fa fa-file-excel-o fa-lg' aria-hidden='true' style='margin-right: 10px;color:green'></i>";
                        break;
                    }
                case ".xlsx":
                    {
                        icono = "<i class='fa fa-file-excel-o fa-lg' aria-hidden='true' style='margin-right: 10px;color:green'></i>";
                        break;
                    }
                case ".txt":
                    {
                        icono = "<i class='fa fa-file-text-o fa-lg' aria-hidden='true' style='margin-right: 10px;color:gray'></i>";
                        break;
                    }
                default:
                    {
                        icono = "<i class='fa fa-picture-o fa-lg' aria-hidden='true' style='margin-right: 10px;color:gray'></i>";
                        break;
                    }
            }
            return icono;
        }

        public static string CortarNombre(int maxlengt, string fileName, string cadena_union)
        {
            POSEntities db = new POSEntities();
            String extension = System.IO.Path.GetExtension(fileName).ToLower();
            string fileName_no_ext = fileName.Replace(extension, "");
            int length = fileName_no_ext.Length;
            if (length > maxlengt)
            {
                string fileNameTrim = fileName_no_ext.Substring(0, maxlengt);
                return fileNameTrim + cadena_union + extension;
            }
            else
            {
                return fileName;
            }
        }

        public static List<string> BuscarCoincidenciasArchivos(string carpeta, string cadenaBusqueda)
        {
            POSEntities db = new POSEntities();
            List<string> ListaArchivos = new List<string>();
            DirectoryInfo hdDirectoryInWhichToSearch = new DirectoryInfo(HttpContext.Current.Server.MapPath("~/" + carpeta));
            FileInfo[] filesInDir = hdDirectoryInWhichToSearch.GetFiles("*" + cadenaBusqueda + "*.*");
            foreach (FileInfo foundFile in filesInDir)
            {
                ListaArchivos.Add(foundFile.Name);
            }
            return ListaArchivos;
        }


        public static bool subirArchivo(UploadedFile file, string carpeta, string FileName)
        {
            POSEntities db = new POSEntities();
            bool success = false;
            String extension = System.IO.Path.GetExtension(file.Filename).ToLower();
            try
            {
                System.IO.File.WriteAllBytes(HttpContext.Current.Server.MapPath("~/" + carpeta + "/") + FileName, file.Contents);
                success = true;
            }
            catch (Exception)
            {
                throw;
            }
            return success;

        }
        public static void elimina(string carpeta, string fileName)
        {
            POSEntities db = new POSEntities();
            string rootPath = AppDomain.CurrentDomain.BaseDirectory;
            System.IO.File.Delete(rootPath + "/" + carpeta + "/" + fileName);
        }


        public static void DescargarArchivo(HttpResponseBase Response, string carpeta, string fileName)
        {
            POSEntities db = new POSEntities();
            string rootPath = AppDomain.CurrentDomain.BaseDirectory;
            string path = rootPath + "/" + carpeta + "/" + fileName;
            String extension = System.IO.Path.GetExtension(fileName).ToLower();
            switch (extension)
            {
                case ".pdf":
                    {
                        Response.ContentType = "application/pdf";
                        break;
                    }
                case ".xls":
                    {
                        Response.ContentType = "application/excel";
                        break;
                    }
                case ".xlsx":
                    {
                        Response.ContentType = "application/excel";
                        break;
                    }
                case ".txt":
                    {
                        Response.ContentType = "text/plain";
                        break;
                    }
                case ".png":
                    {
                        Response.ContentType = "image/png";
                        break;
                    }
                default:
                    {
                        Response.ContentType = "image/jpeg";
                        break;
                    }
            }
            Response.AppendHeader("content-disposition", "attachment;filename=" + fileName);
            Response.TransmitFile(path);
            Response.End();
        }

        public static bool existe(string carpeta, string fileName)
        {
            POSEntities db = new POSEntities();
            bool existe = false;
            if (carpeta != "")
            {
                if ((System.IO.File.Exists(HttpContext.Current.Server.MapPath("~/" + carpeta + "/" + fileName))))
                {
                    existe = true;
                }
            }
            else
            {
                if ((System.IO.File.Exists(HttpContext.Current.Server.MapPath("~/" + fileName))))
                {
                    existe = true;
                }
            }
            return existe;
        }
    }
}