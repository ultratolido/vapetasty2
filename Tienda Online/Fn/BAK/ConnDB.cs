﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
using System.Data.SqlClient;

namespace Tienda_Online.Fn
{
    public class ConnDB
    {
        public static DataSet GetDataSet(String SQL, String ruta)
        {
            MySqlConnection conn = new MySqlConnection(getConnString(ruta));
            MySqlDataAdapter da = new MySqlDataAdapter();
            MySqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = SQL;
            da.SelectCommand = cmd;
            DataSet ds = new DataSet();
            conn.Open();
            da.Fill(ds);
            conn.Close();
            return ds;
        }

        public static string getConnString(String ruta)
        {
            string ConnectionStringDB = "";
            System.Configuration.Configuration rootWebConfig =
               System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("/" + ruta);
            if (rootWebConfig.ConnectionStrings.ConnectionStrings.Count > 0)
            {
                foreach (System.Configuration.ConnectionStringSettings conn in rootWebConfig.ConnectionStrings.ConnectionStrings)
                {
                    if (ConnectionStringDB == "")
                    {
                        string nomConn = conn.Name.ToString().ToUpper();
                        if (nomConn.Contains("ENTITIES")) { ConnectionStringDB = conn.ConnectionString; }
                    }
                }
            }
            ConnectionStringDB = ConnectionStringDB.Split(new string[] { "provider connection string=\"" }, StringSplitOptions.RemoveEmptyEntries)[1];
            ConnectionStringDB = remplazarUltimaCoincidencia(ConnectionStringDB, "\"", "");
            return ConnectionStringDB;
        }

        public static string remplazarUltimaCoincidencia(string CadenaInicial, string CoincidenciaBuscada, string CadenRemplazo)
        {
            int place = CadenaInicial.LastIndexOf(CoincidenciaBuscada);

            if (place == -1)
                return CadenaInicial;

            string result = CadenaInicial.Remove(place, CoincidenciaBuscada.Length).Insert(place, CadenRemplazo);
            return result;
        }
    }
}