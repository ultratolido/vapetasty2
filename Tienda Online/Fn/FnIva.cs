﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft.Json;
using Tienda_Online.Models;

namespace Tienda_Online.Fn
{
    public class FnIva
    {
        /*  *** *** ***  *** *** ***  *** *** ***   
         *  Variables que son utilizadas muy 
         *  Frecuentemente en esta clase.
        */
        private static decimal iva_general = 16;
        private static int id_tipo_cliente = 1;
        private static decimal precio_dolar_general;
        private static bool vender_precio_neto;

        /*  *** *** ***  *** *** ***  *** *** ***   
         *  Construnctor, par inicializar variables
        */
        static FnIva()
        {
            establecerIvaGeneral();
            establecerIdTipoCliente();
            establecerPrecioDolarGeneral();
            establecerVenderPrecioNeto();
        }

        /*  *** *** ***  *** *** ***  *** *** ***
         *  Conjunto de metodos ESTABLECER 
        */
        public static void establecerValoresIniciales()
        {
            establecerIvaGeneral();
            establecerIdTipoCliente();
            establecerPrecioDolarGeneral();
            establecerVenderPrecioNeto();
        }
        public static decimal establecerIvaGeneral()
        {
            configuracion_general configuracion_general = Fn.Configuracion.TraerConfiguracionGeneral();
            iva_general = Convert.ToDecimal(configuracion_general.iva);
            return iva_general;
        }

        public static int establecerIdTipoCliente(int id_tipo_cliente = 0)
        {
            if (id_tipo_cliente == 0)
            {
                POSEntities db = new POSEntities();
                cliente cliente = Fn.cliente_logeado.TraerCliente();
                if (cliente.id != 0)
                    id_tipo_cliente = cliente.TipoCliente_id;
            }
            else
            {
                FnIva.id_tipo_cliente = id_tipo_cliente;
            }
            return id_tipo_cliente;
        }

        public static decimal establecerPrecioDolarGeneral()
        {
            POSEntities db = new POSEntities();
            sucursal sucursal = db.sucursal.Find(1);
            precio_dolar_general = Convert.ToDecimal(sucursal.precio_dolar);
            return precio_dolar_general;
        }

        public static bool establecerVenderPrecioNeto()
        {
            configuracion_general configuracion_general = Fn.Configuracion.TraerConfiguracionGeneral();
            vender_precio_neto = Convert.ToBoolean(configuracion_general.precios_netos);
            return vender_precio_neto;
        }

        /*  *** *** ***  *** *** ***  *** *** ***
         *  Conjunto de metodos OBTENER 
        */
        public static decimal obtenerIvaGeneral()
        {
            return iva_general;
        }

        public static int obtenerIdTipoCliente()
        {
            return id_tipo_cliente;
        }

        public static decimal obtenerPrecioDolarGeneral()
        {
            return precio_dolar_general;
        }

        public static bool obtenerVenderPrecioNeto()
        {
            return vender_precio_neto;
        }
        /*  *** *** ***  *** *** ***  *** *** ***
         *  Conjunto de metodos para CALCULOS y operaciones
         *  Por Producto - Concepto - Venta
        */

        /*  *** *** ***  *** *** ***  *** *** ***
         *  PRODUCTO
         */
        public static decimal obtenerPrecioUnitarioProducto(int id_producto, int id_tipo_cliente = 0)
        {
            if (id_tipo_cliente == 0)
                id_tipo_cliente = FnIva.id_tipo_cliente;
            POSEntities db = new POSEntities();
            producto producto = db.producto.Find(id_producto);
            precio_por_tipocliente precio_por_tipo_cliente = db.precio_por_tipocliente.Where(x => x.Producto_id == id_producto && x.sucursal_id == 1 && x.TipoCliente_id == id_tipo_cliente).SingleOrDefault();
            decimal precio_unitario;
            if (vender_precio_neto)
            {
                decimal uno_por_ciento_cantidad;
                decimal total_porcentaje;

                total_porcentaje = 100 + Convert.ToDecimal(producto.iva);
                uno_por_ciento_cantidad = Convert.ToDecimal(precio_por_tipo_cliente.Precio) / total_porcentaje;
                precio_unitario = uno_por_ciento_cantidad * 100;
            }
            else
            {
                precio_unitario = Convert.ToDecimal(precio_por_tipo_cliente.Precio);
            }
            return precio_unitario;
        }

        public static decimal obtenerIvaUnitarioProducto(int id_producto, int id_tipo_cliente = 0)
        {
            if (id_tipo_cliente == 0)
                id_tipo_cliente = FnIva.id_tipo_cliente;
            POSEntities db = new POSEntities();
            producto producto = db.producto.Find(id_producto);
            precio_por_tipocliente precio_por_tipo_cliente = db.precio_por_tipocliente.Where(x => x.Producto_id == id_producto && x.sucursal_id == 1 && x.TipoCliente_id == id_tipo_cliente).SingleOrDefault();
            decimal porcentaje_iva_producto = Convert.ToDecimal(producto.iva);
            decimal total_porcentaje = 100 + porcentaje_iva_producto;
            decimal iva_unitario = 0;
            if (vender_precio_neto)
            {
                decimal uno_por_ciento_cantidad;

                uno_por_ciento_cantidad = Convert.ToDecimal(precio_por_tipo_cliente.Precio) / total_porcentaje;
                if (porcentaje_iva_producto > 0)
                {
                    iva_unitario = uno_por_ciento_cantidad * (porcentaje_iva_producto);
                }
            }
            else
            {
                if (porcentaje_iva_producto > 0)
                {
                    decimal uno_porciento_cantidad_sin_iva;
                    uno_porciento_cantidad_sin_iva = Convert.ToDecimal(precio_por_tipo_cliente.Precio) / 100;
                    iva_unitario = uno_porciento_cantidad_sin_iva * porcentaje_iva_producto;
                }
            }
            return iva_unitario;
        }

        public static decimal obtenerTotalProducto(int id_producto, int id_tipo_cliente = 0)
        {
            if (id_tipo_cliente == 0)
                id_tipo_cliente = FnIva.id_tipo_cliente;
            POSEntities db = new POSEntities();
            producto producto = db.producto.Find(id_producto);
            precio_por_tipocliente precio_por_tipo_cliente = db.precio_por_tipocliente.Where(x => x.Producto_id == id_producto && x.sucursal_id == 1 && x.TipoCliente_id == id_tipo_cliente).SingleOrDefault();
            decimal porcentaje_iva_producto = Convert.ToDecimal(producto.iva);
            decimal total_porcentaje = 100 + porcentaje_iva_producto;
            decimal precio_total_producto;
            if (vender_precio_neto)
            {
                precio_total_producto = Convert.ToDecimal(precio_por_tipo_cliente.Precio);
            }
            else
            {
                decimal uno_porciento_cantidad_sin_iva;
                uno_porciento_cantidad_sin_iva = Convert.ToDecimal(precio_por_tipo_cliente.Precio) / 100;
                precio_total_producto = uno_porciento_cantidad_sin_iva * total_porcentaje;
            }
            return precio_total_producto;
        }

        /*  *** *** ***  *** *** ***  *** *** ***
         *  CONCEPTO
         */
        public static decimal obtenerSubtotalConcepto(decimal precio_unitario_producto, decimal cantidad)
        {
            return precio_unitario_producto * cantidad;
        }

        public static decimal obtenerIvaConcepto(decimal iva_unitario_producto, decimal cantidad)
        {
            return iva_unitario_producto * cantidad;
        }

        public static decimal obtenerTotalConcepto(decimal total_producto, decimal cantidad)
        {
            return total_producto * cantidad;
        }

        /*  *** *** ***  *** *** ***  *** *** ***
         *  VENTA
         */
        public static decimal obtenerSubtotalVenta(venta venta)
        {
            decimal subtotal_venta = Convert.ToDecimal(venta.conceptos_por_venta.ToList().Select(x => x.importe).Sum());
            return subtotal_venta;
        }

        public static decimal obtenerIvaVenta(venta venta)
        {
            decimal iva_venta = Convert.ToDecimal(venta.conceptos_por_venta.ToList().Select(x => x.iva_importe).Sum());
            return iva_venta;
        }

        public static decimal obtenerTotalVenta(venta venta)
        {
            decimal total_venta = Convert.ToDecimal(venta.conceptos_por_venta.ToList().Select(x => x.importe_mas_iva).Sum());
            return total_venta;
        }
        /*  *** *** ***  *** *** ***  *** *** ***
         *  Metodo para calcular todos los campos
         *  de un concepto
         */
        public static conceptos_por_venta obtenerConceptoVenta(conceptos_por_venta concepto_por_venta)
        {
            establecerValoresIniciales();
            POSEntities db = new POSEntities();
            int id_tipo_cliente = establecerIdTipoCliente();
            decimal precio_unitario;
            decimal precio_unitario_original;
            decimal iva_unitario;
            decimal precio_unitario_mas_iva;
            decimal importe;
            decimal iva_importe;
            decimal importe_mas_iva;
            decimal precio_en_dolares;
            decimal precio_en_dolares_original;

            producto producto = db.producto.Find(concepto_por_venta.Producto_id);

            precio_unitario = obtenerPrecioUnitarioProducto(concepto_por_venta.Producto_id, id_tipo_cliente);
            precio_unitario_original = obtenerPrecioUnitarioProducto(concepto_por_venta.Producto_id, id_tipo_cliente);
            iva_unitario = obtenerIvaUnitarioProducto(concepto_por_venta.Producto_id, id_tipo_cliente);
            precio_unitario_mas_iva = precio_unitario + iva_unitario;
            importe = obtenerSubtotalConcepto(precio_unitario_original, concepto_por_venta.cantidad);
            iva_importe = obtenerIvaConcepto(iva_unitario, concepto_por_venta.cantidad);
            importe_mas_iva = importe + iva_importe;
            if (producto.precio_dolar != 0)
            {
                precio_en_dolares = precio_unitario;
                precio_en_dolares_original = precio_unitario_original;
                precio_unitario = precio_unitario * precio_dolar_general;
                precio_unitario_original = precio_unitario_original * precio_dolar_general;
                iva_unitario = iva_unitario * precio_dolar_general;
                precio_unitario_mas_iva = precio_unitario_mas_iva * precio_dolar_general;
                importe = importe * precio_dolar_general;
                iva_importe = iva_importe * precio_dolar_general;
                iva_importe = iva_importe * precio_dolar_general;
                importe_mas_iva = importe_mas_iva * precio_dolar_general;
            }
            else
            {
                precio_en_dolares = 0;
                precio_en_dolares_original = 0;
            }

            concepto_por_venta.TipoCliente_id = id_tipo_cliente;
            concepto_por_venta.precio_unitario = Math.Round(precio_unitario, 2);
            concepto_por_venta.precio_unitario_original = precio_unitario_original;
            concepto_por_venta.iva_unitario = Math.Round(iva_unitario, 2);
            concepto_por_venta.precio_unitario_mas_iva = Math.Round(precio_unitario_mas_iva, 2);
            concepto_por_venta.importe = Math.Round(importe, 2);
            concepto_por_venta.iva_importe = Math.Round(iva_importe, 2);
            concepto_por_venta.importe_mas_iva = Math.Round(importe_mas_iva, 2);
            concepto_por_venta.precio_en_dolares = Math.Round(precio_en_dolares, 2);
            concepto_por_venta.precio_en_dolares_original = precio_en_dolares_original;

            return concepto_por_venta;
        }

        public static conceptos_por_venta obtenerConceptoVenta(itemproductocarrito2 concepto_por_venta)
        {
            establecerValoresIniciales();
            int id_tipo_cliente = establecerIdTipoCliente();
            conceptos_por_venta nuevo_concepto_por_venta = new conceptos_por_venta();
            decimal precio_unitario;
            decimal precio_unitario_original;
            decimal iva_unitario;
            decimal precio_unitario_mas_iva;
            decimal importe;
            decimal iva_importe;
            decimal importe_mas_iva;
            decimal precio_en_dolares;
            decimal precio_en_dolares_original;

            precio_unitario = obtenerPrecioUnitarioProducto(concepto_por_venta.Id, id_tipo_cliente);
            precio_unitario_original = obtenerPrecioUnitarioProducto(concepto_por_venta.Id, id_tipo_cliente);
            iva_unitario = obtenerIvaUnitarioProducto(concepto_por_venta.Id, id_tipo_cliente);
            precio_unitario_mas_iva = precio_unitario + iva_unitario;
            importe = obtenerSubtotalConcepto(precio_unitario_original, concepto_por_venta.Cantidad);
            iva_importe = obtenerIvaConcepto(iva_unitario, concepto_por_venta.Cantidad);
            importe_mas_iva = importe + iva_importe;
            if (concepto_por_venta.Precio_Dolar != 0)
            {
                precio_en_dolares = precio_unitario;
                precio_en_dolares_original = precio_unitario_original;
                precio_unitario = precio_unitario * precio_dolar_general;
                precio_unitario_original = precio_unitario_original * precio_dolar_general;
                iva_unitario = iva_unitario * precio_dolar_general;
                precio_unitario_mas_iva = precio_unitario_mas_iva * precio_dolar_general;
                importe = importe * precio_dolar_general;
                iva_importe = iva_importe * precio_dolar_general;
                iva_importe = iva_importe * precio_dolar_general;
                importe_mas_iva = importe_mas_iva * precio_dolar_general;
            }
            else
            {
                precio_en_dolares = 0;
                precio_en_dolares_original = 0;
            }

            nuevo_concepto_por_venta.TipoCliente_id = id_tipo_cliente;
            nuevo_concepto_por_venta.Producto_id = concepto_por_venta.Id;
            nuevo_concepto_por_venta.cantidad = concepto_por_venta.Cantidad;
            nuevo_concepto_por_venta.TipoCliente_id = id_tipo_cliente;
            nuevo_concepto_por_venta.precio_unitario = Math.Round(precio_unitario, 2);
            nuevo_concepto_por_venta.precio_unitario_original = precio_unitario_original;
            nuevo_concepto_por_venta.iva_unitario = Math.Round(iva_unitario, 2);
            nuevo_concepto_por_venta.precio_unitario_mas_iva = Math.Round(precio_unitario_mas_iva, 2);
            nuevo_concepto_por_venta.importe = Math.Round(importe, 2);
            nuevo_concepto_por_venta.iva_importe = Math.Round(iva_importe, 2);
            nuevo_concepto_por_venta.importe_mas_iva = Math.Round(importe_mas_iva, 2);
            nuevo_concepto_por_venta.precio_en_dolares = Math.Round(precio_en_dolares, 2);
            nuevo_concepto_por_venta.precio_en_dolares_original = precio_en_dolares_original;

            return nuevo_concepto_por_venta;
        }

        public static itemproductocarrito2 obtenerItemVenta(itemproductocarrito2 item, int id_tipo_precio)
        {
            establecerValoresIniciales();
            //int id_tipo_cliente = establecerIdTipoCliente();
            POSEntities db = new POSEntities();
            decimal precio_unitario;
            decimal precio_unitario_original;
            decimal iva_unitario;
            decimal importe;
            decimal iva_importe;
            decimal precio_unitario_mas_iva;
            decimal importe_mas_iva;
            decimal precio_en_dolares;
            decimal precio_en_dolares_original;

            producto producto = db.producto.Find(item.ProductoID);
            decimal porcentaje_iva_producto = Convert.ToDecimal(producto.iva);
            item.Precio_Dolar = Convert.ToInt16(producto.precio_dolar);

            precio_unitario = obtenerPrecioUnitarioProducto(item.ProductoID, id_tipo_precio);
            precio_unitario_original = obtenerPrecioUnitarioProducto(item.ProductoID, id_tipo_precio);
            iva_unitario = obtenerIvaUnitarioProducto(item.ProductoID, id_tipo_precio);
            precio_unitario_mas_iva = precio_unitario + iva_unitario;
            importe = obtenerSubtotalConcepto(precio_unitario_original, item.Cantidad);
            iva_importe = obtenerIvaConcepto(iva_unitario, item.Cantidad);
            importe_mas_iva = importe + iva_importe;
            if (item.Precio_Dolar != 0)
            {
                precio_en_dolares = precio_unitario;
                precio_en_dolares_original = precio_unitario_original;
                precio_unitario = precio_unitario * precio_dolar_general;
                precio_unitario_original = precio_unitario_original * precio_dolar_general;
                iva_unitario = iva_unitario * precio_dolar_general;
                precio_unitario_mas_iva = precio_unitario_mas_iva * precio_dolar_general;
                importe = importe * precio_dolar_general;
                iva_importe = iva_importe * precio_dolar_general;
                iva_importe = iva_importe * precio_dolar_general;
                importe_mas_iva = importe_mas_iva * precio_dolar_general;
            }
            else
            {
                precio_en_dolares = 0;
                precio_en_dolares_original = 0;
            }
            item.iva = porcentaje_iva_producto;
            item.cantidad_iva_unitario = iva_unitario;
            item.Precio_Unitario = precio_unitario_original;

            return item;
        }
    }
}
