﻿//using Newtonsoft.Json;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using Tienda_Online.Models;

//namespace Tienda_Online.Fn
//{
//    public class FnCarrito
//    {
//        public DateTime hora { get; set; }
//        public List<itemproductocarrito2> carritoComprasProductos { get; set; }
//        public static List<itemproductocarrito2> ListaCarrito = new List<itemproductocarrito2>();

//        public static void actualizarNombrePrimeraImagenListaProducto(List<itemproductocarrito2> ListaCarrito)
//        {
//            POSEntities db = new POSEntities();
//            foreach (itemProductoCarrito producto in ListaCarrito)
//            {
//                if (db.imagen.Where(x => x.id_producto == producto.Id).Any())
//                    producto.imagen = db.imagen.Where(x => x.id_producto == producto.Id).FirstOrDefault().nombre;
//            }
//        }
//        public static void actualizarDatosListaProducto(List<itemproductocarrito2> ListaCarrito, int id_tipo_precio)
//        {
//            Fn.FnIva.establecerValoresIniciales();
//            POSEntities db = new POSEntities();
//            int id_cliente = 1;
//            if (Fn.cliente_logeado.TraerCliente().id != 0)
//                id_cliente = Fn.cliente_logeado.TraerCliente().id;
//            configuracion_tienda_online configuracion_tienda_online = Fn.Configuracion.TraerConfiguracionTiendaOnline();
//            foreach (itemProductoCarrito p in ListaCarrito)
//            {
//                Decimal cantidad_en_inventario = 0;
//                producto _p = db.producto.Find(p.Id);
//                p.Precio_Dolar = _p.precio_dolar.Value;
//                p.Descripcion = _p.Descripcion;
//                p.DescripcionDetallada = _p.descripcion_detallada;
//                p.Codigo = _p.Codigo;
//                itemProductoCarrito itemProductoCarrito = Fn.FnIva.obtenerItemVenta(p);
//                p.Precio_Dolar = itemProductoCarrito.Precio_Dolar;
//                p.iva = itemProductoCarrito.iva;
//                p.cantidad_iva_unitario = itemProductoCarrito.cantidad_iva_unitario;
//                p.Precio_Unitario = itemProductoCarrito.Precio_Unitario;
//                List<int> lista_id_sucursales = db.sucursal.Where(x => x.alimenta_tienda_online == true).Select(y => y.id).ToList();
//                producto pro = db.producto.FirstOrDefault(x => x.Codigo.Equals(_p.Codigo));
//                if (configuracion_tienda_online.mostrar_inventario == true)
//                {
//                    List<inventario> lista_inventario = db.inventario.Where(x => x.Producto_id == pro.id && lista_id_sucursales.Contains(x.sucursal_id)).ToList();
//                    if (lista_inventario != null && lista_inventario.Count > 0)
//                        cantidad_en_inventario = Convert.ToDecimal(lista_inventario.Select(x => x.cantidad).Sum());
//                    p.Inventario = cantidad_en_inventario;
//                }
//            }
//        }

//        public static List<itemproductocarrito2> TraerLista(HttpContext _Context, int id_tipo_precio)
//        {
//            Fn.FnIva.establecerValoresIniciales();
//            configuracion_tienda_online configuracion_tienda_online = Fn.Configuracion.TraerConfiguracionTiendaOnline();
//            POSEntities db = new POSEntities();
//            //EXISTE COOKIE?
//            itemCookie cookie = new itemCookie();
//            List<itemproductocarrito2> listaCarrito = new List<itemproductocarrito2>();
//            if (_Context.Request.Cookies["CarritoCompras"] != null)
//            {
//                if (_Context.Request.Cookies["CarritoCompras"].Value != null)
//                {
//                    dynamic cookieJson = _Context.Request.Cookies["CarritoCompras"].Value;
//                    if (cookieJson != "")
//                    {
//                        cookie = JsonConvert.DeserializeObject<itemCookie>(cookieJson);
//                        listaCarrito = cookie.Carrito;
//                    }
//                    int id_cliente = 1;
//                    if (Fn.cliente_logeado.TraerCliente().id != 0)
//                        id_cliente = Fn.cliente_logeado.TraerCliente().id;
//                    foreach (itemProductoCarrito p in listaCarrito)
//                    {
//                        Decimal cantidad_en_inventario = 0;
//                        producto _p = db.producto.Find(p.Id);
//                        p.Descripcion = _p.Descripcion;
//                        p.DescripcionDetallada = _p.descripcion_detallada;
//                        p.Codigo = _p.Codigo;
//                        itemProductoCarrito itemProductoCarrito = Fn.FnIva.obtenerItemVenta(p);
//                        p.Precio_Dolar = itemProductoCarrito.Precio_Dolar;
//                        p.iva = itemProductoCarrito.iva;
//                        p.cantidad_iva_unitario = itemProductoCarrito.cantidad_iva_unitario;
//                        p.Precio_Unitario = itemProductoCarrito.Precio_Unitario;
//                        List<int> lista_id_sucursales = db.sucursal.Where(x => x.alimenta_tienda_online == true).Select(y => y.id).ToList();
//                        producto pro = db.producto.FirstOrDefault(x => x.Codigo.Equals(_p.Codigo));
//                        if (configuracion_tienda_online.mostrar_inventario == true)
//                        {
//                            List<inventario> lista_inventario = db.inventario.Where(x => x.Producto_id == pro.id && lista_id_sucursales.Contains(x.sucursal_id)).ToList();
//                            if (lista_inventario != null && lista_inventario.Count > 0)
//                                cantidad_en_inventario = Convert.ToDecimal(lista_inventario.Select(x => x.cantidad).Sum());
//                            p.Inventario = cantidad_en_inventario;
//                        }
//                    }
//                }
//            }
//            return listaCarrito;
//        }


//        public static Decimal TraerCantidadProductos(HttpContext _Context)
//        {
//            POSEntities db = new POSEntities();
//            //EXISTE COOKIE?
//            //HttpContext _Context2 = System.Web.HttpContext.Current;
//            List<itemproductocarrito2> listaActual_CarritoCompras = new List<itemproductocarrito2>();
//            if (_Context.Request.Cookies["CarritoCompras"] != null)
//            {
//                if (_Context.Request.Cookies["CarritoCompras"].Value != null)
//                {
//                    if (_Context.Request.Cookies["CarritoCompras"].Value != "")
//                    {
//                        dynamic cookieJson = _Context.Request.Cookies["CarritoCompras"].Value;
//                        itemCookie cookie = JsonConvert.DeserializeObject<itemCookie>(cookieJson);
//                        listaActual_CarritoCompras = cookie.Carrito;
//                        return listaActual_CarritoCompras.Select(x => x.Cantidad).Sum();
//                    }
//                    else
//                        return 0;
//                }
//                else
//                    return 0;
//            }
//            else
//                return 0;
//        }

//        public static itemCookie traerCookie(HttpContext _Context)
//        {
//            POSEntities db = new POSEntities();
//            itemCookie cookie = new itemCookie();
//            dynamic cookieJson = "";
//            //EXISTE COOKIE?
//            if (_Context.Request.Cookies["CarritoCompras"] != null)
//            {
//                if (_Context.Request.Cookies["CarritoCompras"].Value != null && _Context.Request.Cookies["CarritoCompras"].Value != "")
//                {
//                    cookieJson = _Context.Request.Cookies["CarritoCompras"].Value;
//                    cookie = JsonConvert.DeserializeObject<itemCookie>(cookieJson);
//                }
//            }
//            return cookie;
//        }

//        public static List<itemproductocarrito2> agregarProductos(List<itemproductocarrito2> listaProductosAgregar, HttpContext _Context)
//        {
//            POSEntities db = new POSEntities();
//            //HttpContext _Context = System.Web.HttpContext.Current;
//            itemCookie cookie = traerCookie(_Context);
//            //EXISTE COOKIE?
//            if (cookie.Carrito != null)
//            {
//                foreach (itemProductoCarrito p in listaProductosAgregar)
//                {
//                    p.DescripcionDetallada = "";
//                    p.Descripcion = "";
//                    //PRODUCTO EXISTE EN LISTA??
//                    if (cookie.Carrito.Select(x => x.Id).Contains(p.Id))
//                    {
//                        ListaCarrito.Where(x => x.Id == p.Id).SingleOrDefault().Cantidad += p.Cantidad;
//                        cookie.Carrito.Where(x => x.Id == p.Id).SingleOrDefault().Cantidad += p.Cantidad;
//                    }
//                    //SI NO EXISTE AGREGA EL REGISTRO
//                    else
//                        cookie.Carrito.Add(p);
//                    ListaCarrito.Add(p);
//                }
//                //GUARDAR ESTA LISTA ACTUALIZADA AL COOKIE "carritoCompras"
//                _Context.Response.Cookies["CarritoCompras"].Value = JsonConvert.SerializeObject(cookie);
//            }
//            //CREA LA COOKIE
//            else
//            {
//                foreach (itemProductoCarrito p in listaProductosAgregar)
//                {
//                    p.DescripcionDetallada = "";
//                    p.Descripcion = "";
//                }
//                //AGREGALA
//                cookie.Carrito = listaProductosAgregar;
//                ListaCarrito = listaProductosAgregar;
//                HttpCookie myCookie = new HttpCookie("CarritoCompras");
//                myCookie.Value = JsonConvert.SerializeObject(cookie);
//                myCookie.Expires = DateTime.Now.AddDays(1d);
//                _Context.Response.Cookies.Add(myCookie);
//            }
//            return cookie.Carrito;
//        }

//        public static void VaciarCarrito(HttpContext _Context)
//        {
//            ListaCarrito.Clear();
//            POSEntities db = new POSEntities();
//            dynamic cookieJson = "";
//            //EXISTE COOKIE?
//            if (_Context.Request.Cookies["CarritoCompras"] != null)
//            {
//                if (_Context.Request.Cookies["CarritoCompras"].Value != null)
//                {
//                    _Context.Response.Cookies["CarritoCompras"].Value = "";
//                }
//            }
//        }

//        //FUNCION QUE ELIMINA TODOS LOS PRODUCTOS DE LA LISTA CON EL ID ENVIADO
//        public static List<itemproductocarrito2> eliminarProductosID(int idProducto, HttpContext _Context)
//        {
//            POSEntities db = new POSEntities();
//            List<itemproductocarrito2> listaActual_CarritoCompras = new List<itemproductocarrito2>();
//            List<itemproductocarrito2> listaFinal = new List<itemproductocarrito2>();
//            itemCookie cookie = new itemCookie();
//            dynamic cookieJson = "";
//            //EXISTE COOKIE?
//            if (_Context.Request.Cookies["CarritoCompras"] != null)
//            {
//                if (_Context.Request.Cookies["CarritoCompras"].Value != null)
//                {
//                    cookieJson = _Context.Request.Cookies["CarritoCompras"].Value;
//                    cookie = JsonConvert.DeserializeObject<itemCookie>(cookieJson);
//                    listaActual_CarritoCompras = cookie.Carrito;
//                }




                

//                //COOKIE
//                //ID PRODUCTO EXISTE EN LISTA??
//                if (listaActual_CarritoCompras.Select(x => x.Id).Contains(idProducto))
//                {
//                    listaActual_CarritoCompras.Remove(listaActual_CarritoCompras.Where(x => x.Id == idProducto).SingleOrDefault());
//                }
//                //SI NO EXISTE NO HAGAS NADA
//                foreach (itemProductoCarrito p in listaActual_CarritoCompras)
//                {
//                    p.DescripcionDetallada = "";
//                    p.Descripcion = "";
//                }
//                cookie.Carrito = listaActual_CarritoCompras;
//                ListaCarrito = listaActual_CarritoCompras;
//                //GUARDAR ESTA LISTA ACTUALIZADA AL COOKIE "carritoCompras"
//                _Context.Response.Cookies["CarritoCompras"].Value = JsonConvert.SerializeObject(cookie);
//                //_Context.Response.Cookies["CarritoCompras"].Value = JsonConvert.SerializeObject(cookieJson);
//            }
//            //CREA LA COOKIE
//            else
//            {
//                //AGREGALA
//                HttpCookie myCookie = new HttpCookie("CarritoCompras");
//                myCookie.Value = JsonConvert.SerializeObject("");
//                myCookie.Expires = DateTime.Now.AddDays(1d);
//                _Context.Response.Cookies.Add(myCookie);
//            }

//            return listaActual_CarritoCompras;
//        }

//        //FUNCION QUE BAJA EN UNA UNIDAD EL PRODUCTO ENVIADO. SI ES QUE LO ENCUENTRA, EN CASO CONTRARIO NO HACE NADA
//        public static List<itemproductocarrito2> cambiarCantidadProducto(int idProducto, Decimal cantidad, HttpContext _Context)
//        {
//            POSEntities db = new POSEntities();
//            List<itemproductocarrito2> listaActual_CarritoCompras = new List<itemproductocarrito2>();
//            dynamic cookieJson = "";
//            itemCookie cookie = new itemCookie();

//            //EXISTE COOKIE?
//            if (_Context.Request.Cookies["CarritoCompras"] != null)
//            {
//                if (_Context.Request.Cookies["CarritoCompras"].Value != null)
//                {
//                    cookieJson = _Context.Request.Cookies["CarritoCompras"].Value;
//                    cookie = JsonConvert.DeserializeObject<itemCookie>(cookieJson);
//                    listaActual_CarritoCompras = cookie.Carrito;
//                }

//                //ID PRODUCTO EXISTE EN LISTA??
//                if (listaActual_CarritoCompras.Select(x => x.Id).Contains(idProducto))
//                {
//                    listaActual_CarritoCompras.Where(x => x.Id == idProducto).SingleOrDefault().Cantidad = cantidad;

//                }
//                foreach (itemProductoCarrito p in listaActual_CarritoCompras)
//                {
//                    p.DescripcionDetallada = "";
//                    p.Descripcion = "";
//                }
//                cookie.Carrito = listaActual_CarritoCompras;
//                ListaCarrito = listaActual_CarritoCompras;
//                //GUARDAR ESTA LISTA ACTUALIZADA AL COOKIE "carritoCompras"
//                _Context.Response.Cookies["CarritoCompras"].Value = JsonConvert.SerializeObject(cookie);
//            }
//            return listaActual_CarritoCompras;
//        }

//        //FUNCION QUE BAJA EN UNA UNIDAD EL PRODUCTO ENVIADO. SI ES QUE LO ENCUENTRA, EN CASO CONTRARIO NO HACE NADA
//        public static List<itemproductocarrito2> subirCantidadProducto(int idProducto, HttpContext _Context)
//        {
//            POSEntities db = new POSEntities();
//            List<itemproductocarrito2> listaActual_CarritoCompras = new List<itemproductocarrito2>();
//            dynamic cookieJson = "";
//            itemCookie cookie = new itemCookie();
//            //EXISTE COOKIE?
//            if (_Context.Request.Cookies["CarritoCompras"] != null)
//            {
//                if (_Context.Request.Cookies["CarritoCompras"].Value != null)
//                {
//                    cookieJson = _Context.Request.Cookies["CarritoCompras"].Value;
//                    cookie = JsonConvert.DeserializeObject<itemCookie>(cookieJson);
//                    listaActual_CarritoCompras = cookie.Carrito;
//                }

//                //ID PRODUCTO EXISTE EN LISTA??
//                if (listaActual_CarritoCompras.Select(x => x.Id).Contains(idProducto))
//                {
//                    //CANTIDAD ACTUAL?
//                    Decimal cantidadActual = listaActual_CarritoCompras.Where(x => x.Id == idProducto).SingleOrDefault().Cantidad;
//                    //SI CANTIDAD ACTUAL-1 MENOR/IGUAL A 0 ENTONCES ELIMINA EL REGISTRO
//                    if ((cantidadActual - 1) <= 0)
//                    {
//                        //ELIMINA REGISTRO DE PRODUCTO
//                        listaActual_CarritoCompras.Remove(listaActual_CarritoCompras.Where(x => x.Id == idProducto).SingleOrDefault());
//                    }
//                    else
//                    {
//                        //BAJA UNA UNIDAD
//                        listaActual_CarritoCompras.Where(x => x.Id == idProducto).SingleOrDefault().Cantidad = cantidadActual + 1;
//                    }

//                }
//                foreach (itemProductoCarrito p in listaActual_CarritoCompras)
//                {
//                    p.DescripcionDetallada = "";
//                    p.Descripcion = "";
//                }
//                cookie.Carrito = listaActual_CarritoCompras;
//                ListaCarrito = listaActual_CarritoCompras;
//                //GUARDAR ESTA LISTA ACTUALIZADA AL COOKIE "carritoCompras"
//                _Context.Response.Cookies["CarritoCompras"].Value = JsonConvert.SerializeObject(cookie);
//            }
//            //CREA LA COOKIE
//            else
//            {
//                //AGREGALA
//                HttpCookie myCookie = new HttpCookie("CarritoCompras");
//                myCookie.Value = JsonConvert.SerializeObject("");
//                myCookie.Expires = DateTime.Now.AddDays(1d);
//                _Context.Response.Cookies.Add(myCookie);
//            }
//            return listaActual_CarritoCompras;
//        }

//        public static decimal traerSubtotalConcepto(Decimal precio_unitario, Decimal cantidad)
//        {
//            return Math.Round(precio_unitario * cantidad, 2);
//        }

//        public static decimal traerIvaConcepto(Decimal iva_unitario, Decimal cantidad)
//        {
//            return Math.Round(iva_unitario * cantidad, 2);
//        }

//        public static decimal traerTotalConcepto(Decimal precio_unitario, Decimal iva_unitario, Decimal cantidad)
//        {
//            return Math.Round((precio_unitario + iva_unitario) * cantidad, 2);
//        }
//    }
//}